/*
 ** xkaapi
 ** 
 ** Created on Tue Mar 31 15:19:14 2009
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 ** thierry.gautier@inrialpes.fr
 ** fabien.lementec@imag.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include "kaapif.h"
#include "kaapif_inc.h"
#include "libkomp.h"
#include "kaapi_error.h"
#include <stdarg.h>
#include <stddef.h>
#include <alloca.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>


/* closure for the body of the for each */
typedef struct kaapif_body_arg_t {
  void (*f_f)(int32_t*, int32_t*, int32_t*, ...);
  long long           first;
  unsigned int        nargs;
  void*               args[];
} kaapif_body_arg_t;



#define FATAL()						\
do {							\
  printf("fatal error @ %s::%d\n", __FILE__, __LINE__);	\
  kaapi_abort(__LINE__, __FILE__, "");\
} while(0)


/** Used by old API that use global variable for set_grain
*/
static __thread komp_foreach_attr_t kaapif_default_attr = { .init = 0 };

/**
*/
int32_t kaapif_get_grains_(int32_t* par_grain, int32_t* seq_grain)
{
  unsigned long long sgrain;
  unsigned long long pgrain;
  int err =  komp_foreach_attr_get_grains( &kaapif_default_attr, &sgrain, &pgrain);
  *par_grain = *seq_grain = 0;
  if (err !=0)
    return err;

  if ((sgrain >= (unsigned long long)INT_MAX) || (pgrain >= (unsigned long long)INT_MAX))
    return EINVAL;

  *par_grain = (int32_t)pgrain;
  *seq_grain = (int32_t)sgrain;
  return 0;
}

/**
*/
int32_t kaapif_set_grains_(int32_t* par_grain, int32_t* seq_grain)
{
  return komp_foreach_attr_set_grains( &kaapif_default_attr, *seq_grain, *par_grain);
}

/**
*/
int32_t kaapif_set_default_grains_(void)
{
  return komp_foreach_attr_set_grains( &kaapif_default_attr, 1, 0);
}

/**
*/
int32_t kaapif_set_distribution_(int32_t* level, int32_t* strict)
{
  return komp_foreach_attr_set_distribution(&kaapif_default_attr, (komp_hws_levelid_t)*level, *strict);
}

/**
*/
int32_t kaapif_set_affinity_(int32_t* level, int32_t* strict)
{
  return komp_foreach_attr_set_localitydomain(&kaapif_default_attr, (komp_hws_levelid_t)*level, *strict);
}

/**
*/
int32_t kaapif_set_policy_(int32_t* policy )
{
  return komp_foreach_attr_set_policy(&kaapif_default_attr, (komp_foreach_attr_policy_t)*policy);
}

/**
*/
int32_t kaapif_foreach_attr_init_( int64_t* fattr )
{
  komp_foreach_attr_t** attr = (komp_foreach_attr_t**)fattr;
  *attr = malloc( sizeof(komp_foreach_attr_t));
  return komp_foreach_attr_init(*attr);
}

/**
*/
int32_t kaapif_foreach_attr_destroy_(int64_t* fattr)
{
  komp_foreach_attr_t** attr = (komp_foreach_attr_t**)fattr;
  komp_foreach_attr_destroy(*attr);
  free(*attr);
  *attr = 0;
  return 0;
}

/**
*/
int32_t kaapif_foreach_attr_set_distribution_(int64_t* fattr, int32_t* level, int32_t* strict)
{
  komp_foreach_attr_t** attr = (komp_foreach_attr_t**)fattr;
  return komp_foreach_attr_set_distribution(*attr, (komp_hws_levelid_t)*level, *strict);
}

/**
*/
int32_t kaapif_foreach_attr_set_affinity_(int64_t* fattr, int32_t* level, int32_t* strict)
{
  komp_foreach_attr_t** attr = (komp_foreach_attr_t**)fattr;
  return komp_foreach_attr_set_localitydomain(*attr, (komp_hws_levelid_t)*level, *strict);
}



/**
*/
int32_t kaapif_foreach_attr_set_default_grains_(int64_t* fattr)
{
  komp_foreach_attr_t** attr = (komp_foreach_attr_t**)fattr;
  return komp_foreach_attr_set_grains( *attr, 1, 0);
}

/**
*/
int32_t kaapif_foreach_attr_set_grains_(int64_t* fattr, int32_t* par_grain, int32_t* seq_grain)
{
  komp_foreach_attr_t** attr = (komp_foreach_attr_t**)fattr;
  return komp_foreach_attr_set_grains( *attr, *seq_grain, *par_grain);
}

/**
*/
int32_t kaapif_foreach_attr_set_policy_(
  int64_t*,  /* attr */
  int32_t*   /* policy */
);


/* main entry point */
static void kaapif_foreach_body2user(
  unsigned long long ifirst,
  unsigned long long ilast,
  void* arg
)
{
  /* change [first, last[ to [first,last-1] as assumed in fortran.
  */
  kaapif_body_arg_t* call  = (kaapif_body_arg_t*)arg;
  int32_t first = (int32_t)(call->first + ifirst);
  int32_t last  = (int32_t)(call->first + ilast);
  int32_t tid = komp_get_thread_num();
  --last;
#include "kaapif_adaptive_switch.h"
  KAAPIF_ADAPTIVE_SWITCH(call, first, last, tid);
}


/**
*/
int32_t kaapif_foreach_with_attr_(
  int32_t* first, int32_t* last, 
  int64_t* fattr,
  int32_t* nargs, 
  void (*f)(int32_t*, int32_t*, int32_t*, ...), 
  ...
)
{
  int32_t k;
  kaapif_body_arg_t* body_arg;
  va_list va_args;

  komp_foreach_attr_t** attr = (komp_foreach_attr_t**)fattr;

  if (*nargs >= KAAPIF_MAX_ARGS)
    return KAAPIF_ERR_EINVAL;

  if (!(first <last))
    return KAAPIF_SUCCESS;

  body_arg = (kaapif_body_arg_t*)alloca(
    offsetof(kaapif_body_arg_t, args) + *nargs * sizeof(void*)
  ); 
  va_start(va_args, f);
  
  /* format of each effective parameter is a list of tuple:
       @
  */
  body_arg->f_f = f;
  body_arg->first = *first;
  body_arg->nargs = *nargs;

  for (k = 0; k < *nargs; ++k)
    body_arg->args[k] = va_arg(va_args, void*);
  va_end(va_args);
  
  unsigned long long bmax;
  if (*first <= *last) 
    bmax = (*last - *first)+1;
  else
    bmax = (*first - *last)+1;
  /* init global thread specific kaapif attribut */
  komp_foreach_attr_init(&kaapif_default_attr);
  if (komp_foreach( 0, bmax, 1, 0, *attr, kaapif_foreach_body2user, body_arg) ==0)
    return KAAPIF_SUCCESS;
  return KAAPIF_ERR_FAILURE;
}


/** Copy of previous code, because it requires to pass va_args if we want to call it
*/
int32_t kaapif_foreach_(
  int32_t* first, int32_t* last, 
  int32_t* nargs,
  void (*f)(int32_t*, int32_t*, int32_t*, ...), 
  ...
)
{
  int32_t k;
  kaapif_body_arg_t* body_arg;
  va_list va_args;

  if (*nargs >= KAAPIF_MAX_ARGS)
    return KAAPIF_ERR_EINVAL;

  if (!(*first <*last))
    return KAAPIF_SUCCESS;

  body_arg = (kaapif_body_arg_t*)alloca(
    offsetof(kaapif_body_arg_t, args) + *nargs * sizeof(void*)
  ); 
  va_start(va_args, f);
  
  /* format of each effective parameter is a list of tuple:
       @
  */
  body_arg->f_f = f;
  body_arg->first = *first;
  body_arg->nargs = *nargs;

  for (k = 0; k < *nargs; ++k)
    body_arg->args[k] = va_arg(va_args, void*);
  va_end(va_args);
  
  unsigned long long bmax;
  if (*first <= *last) 
    bmax = (*last - *first)+1;
  else
    bmax = (*first - *last)+1;
  
  komp_foreach_attr_t attr = kaapif_default_attr;
  komp_foreach_attr_init(&kaapif_default_attr);
  if (komp_foreach( 0, bmax, 1, 0, &attr, kaapif_foreach_body2user, body_arg) ==0)
    return KAAPIF_SUCCESS;
  return KAAPIF_ERR_FAILURE;
}

