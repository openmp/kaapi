/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012, 2013, 2014 INRIA.
**
** Contributors :
**
** francois.broquedis@imag.fr
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include <libiomp.h>


/* Always returns true. (see kmp_csupport.c:173) */
kmp_int32 __kmpc_ok_to_fork(ident_t *loc)
{
  TRACE_ENTER();
  TRACE_LEAVE();

  return 1;
}

void __kmpc_begin_push_init(int kind)
{
  komp_begin_push_init((komp_push_init_t)kind);
}

void __kmpc_end_push_init()
{
  komp_end_push_init();
}

/* Returns the id of the current thread. */
kmp_int32 __kmpc_global_thread_num(ident_t *loc)
{
  return 0; /* should return either pointer to kprocessor, but cannot be cast without
        guarantee to kmp_int32. Return kid to the team structure ? So not use
     */
}
#if 0
{
  kmp_int32 retval;
  TRACE_ENTER();
  kaapi_processor_t* kproc = kaapi_self_processor();
  if (kproc == 0)
    retval = -1;
  else
    retval = (kmp_int32)kproc->kid;
  TRACE_LEAVE();
  return retval;
}
#endif

#if !KOMP_USE_ICV
 __thread kmp_int32 tls_num_threads = 0;
#endif

void __kmpc_push_num_threads (ident_t *loc, kmp_int32 global_tid, kmp_int32 num_threads)
{
  TRACE_ENTER();
#if KOMP_USE_ICV
  komp_set_num_threads( num_threads );
#else
  tls_num_threads = num_threads;
#endif
  TRACE_LEAVE();
}

struct microtask_arg {
  microtask_t microtask;
  kmp_int32 argc;
  void** argv;
  komp_dataenv_icv_t* icvs;
};


static void __kmpc_wrapper_parallel_kaapi( void* _a )
{
  struct microtask_arg* arg= (struct microtask_arg*)_a;
  int gtid = kaapi_self_kid(); 

  __kmp_invoke_microtask( arg->microtask, gtid, gtid, (int)arg->argc, (void**)arg->argv );
}


/*
*/
void __kmpc_fork_call (ident_t *loc, kmp_int32 argc, kmpc_micro microtask, ...) 
{
  TRACE_ENTER();

#if KOMP_USE_ICV
  kmp_int32 num_threads = 0; /* let komp_fork_call determine it using icvs */
#else
  kmp_int32 num_threads = tls_num_threads;
#endif

  struct microtask_arg arg;
  void  **argv;
  va_list ap;
  int i;

  /* file args for the microtask_arg */
  arg.microtask = (microtask_t)microtask;
  arg.argv = alloca( sizeof(void*)*argc);
  arg.argc = argc;
  arg.icvs = komp_self_icvs();
  argv = arg.argv;
  va_start(ap, microtask);

  for( i=argc-1; i >= 0; --i )
    /* TODO: revert workaround for Intel(R) 64 tracker #96 */
#if (KMP_ARCH_X86_64 || KMP_ARCH_ARM) && KMP_OS_LINUX
    *argv++ = va_arg( *ap, void * );
#else
    *argv++ = va_arg( ap, void * );
#endif
  va_end(ap);

  komp_fork_call(num_threads, komp_proc_bind_default, __kmpc_wrapper_parallel_kaapi, &arg);

#if !KOMP_USE_ICV
  /* reset num_threads clause for next region */
  tls_num_threads = 0;
#endif

  TRACE_LEAVE();
}

/*
*/
void __kmpc_serialized_parallel(ident_t *loc, kmp_int32 global_tid)
{
  /* TODO: Est-ce qu'on doit créer une team dans ce cas-là? (accès
   * aux ICVs etc) */
  TRACE_ENTER();
  fprintf (stderr, "%s is deprecated!\n", __FUNCTION__);
  exit (-1);
  TRACE_LEAVE();
}

/*
*/
void __kmpc_end_serialized_parallel(ident_t *loc, kmp_int32 global_tid)
{
  TRACE_ENTER();
  fprintf (stderr, "%s is deprecated!\n", __FUNCTION__);
  exit (-1);
  TRACE_LEAVE();
}

/*
*/
kmp_int32 __kmpc_single(ident_t *loc, kmp_int32 global_tid)
{
  TRACE_ENTER();
  TRACE_LEAVE();
  return kaapi_team_single(kaapi_self_team(), 0);
}

/*
*/
void __kmpc_end_single(ident_t *loc, kmp_int32 global_tid)
{
  TRACE_ENTER();
  TRACE_LEAVE();
}

/*
*/
kmp_int32 __kmpc_master(ident_t *loc, kmp_int32 global_tid)
{
  TRACE_ENTER();
  TRACE_LEAVE();
  return kaapi_team_master(kaapi_self_team(), 0);
}

/*
*/
void __kmpc_end_master(ident_t *loc, kmp_int32 global_tid)
{
  TRACE_ENTER();
  TRACE_LEAVE();
}



/*
*/
void
__kmpc_threadprivate_register(ident_t *loc, void *data, kmpc_ctor ctor, kmpc_cctor cctor, kmpc_dtor dtor)
{
  TRACE_ENTER();
  kaapi_assert(0);
  TRACE_LEAVE();
}

/*
*/
void
__kmpc_threadprivate_register_vec( ident_t *loc, void *data, kmpc_ctor_vec ctor,
                                   kmpc_cctor_vec cctor, kmpc_dtor_vec dtor,
                                   size_t vector_length )
{
}

/*
*/
typedef struct kmpc_cache_t {
  kaapi_hashmap_t          data_khm; /* to store task & data visited */
  kaapi_hashentries_t*     mapentries[1<<KAAPI_SIZE_DFGCTXT];
  kaapi_hashentries_bloc_t mapbloc;
} kmpc_cache_t;

static __thread kmpc_cache_t* cache = 0;

/* Return the thread copy of data
*/
void*
__kmpc_threadprivate_cached( ident_t * loc, kmp_int32 global_tid,
                             void * data, size_t size, void *** dummy_cache )
{
  TRACE_ENTER();
  if (cache ==0)
  {
    cache = malloc( sizeof(kmpc_cache_t) );
    kaapi_hashmap_init(&cache->data_khm, cache->mapentries, KAAPI_SIZE_DFGCTXT, &cache->mapbloc);
  }
  kaapi_hashentries_t* entry = kaapi_hashmap_findinsert(&cache->data_khm, data);
  void* handle = KAAPI_HASHENTRIES_GET(entry, void*);
  if (handle ==0)
  {
    handle = malloc( size );
    memcpy( handle, data, size );
    KAAPI_HASHENTRIES_SET(entry, handle, void*);
  }
  TRACE_LEAVE();
  return handle;
}

/* Undocumented function ?
*/
void * __kmpc_threadprivate( ident_t * loc, kmp_int32 global_tid, void * data, size_t size)
{
  TRACE_ENTER();
  kaapi_assert(0);
  TRACE_LEAVE();
  return 0;
}


/* Master do reduction
*/
kmp_int32
__kmpc_reduce(  ident_t *loc, kmp_int32 global_tid,
                kmp_int32 num_vars, size_t reduce_size,
                void *reduce_data, void (*reduce_func)(void *lhs_data, void *rhs_data),
                kmp_critical_name *lck )
{
  TRACE_ENTER();
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_context_t* context = kproc->context;
  kaapi_team_t* team = kproc->team;
  context->reduce_data = reduce_data;
  kaapi_team_barrier_wait( team, kproc, KAAPI_BARRIER_FLAG_DEFAULT );
  if (kproc->rsrc.kid ==0)
  {
    int i;
    for (i=1; i<team->count; ++i)
      reduce_func( reduce_data, team->all_kprocessors[i]->context->reduce_data );
  }
  TRACE_LEAVE();
  return 1;
}

/*
*/
void __kmpc_end_reduce( ident_t *loc, kmp_int32 global_tid, kmp_critical_name *lck )
{
  TRACE_ENTER();
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_team_t* team = kproc->team;
  kaapi_team_barrier_wait( team, kproc, KAAPI_BARRIER_FLAG_DEFAULT );
  TRACE_LEAVE();
}

/*
*/
kmp_int32
__kmpc_reduce_nowait(  ident_t *loc, kmp_int32 global_tid,
                       kmp_int32 num_vars, size_t reduce_size,
                       void *reduce_data, void (*reduce_func)(void *lhs_data, void *rhs_data),
                       kmp_critical_name *lck )
{
  return __kmpc_reduce( loc, global_tid, num_vars, reduce_size, reduce_data, reduce_func, lck );
}

/*
*/
void __kmpc_end_reduce_nowait( ident_t *loc, kmp_int32 global_tid, kmp_critical_name *lck )
{
  __kmpc_end_reduce(loc, global_tid, lck );
} 

