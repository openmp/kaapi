/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "libgomp.h"

//#define TRACE 1

#ifdef TRACE
#include <stdio.h>
#define KOMP_TRACE_ENTER \
  printf("%i:: enter '%s'\n", kaapi_self_kid(), __PRETTY_FUNCTION__); fflush(stdout);
#define KOMP_TRACE_EXIT \
  printf("%i:: exit '%s'\n", kaapi_self_kid(), __PRETTY_FUNCTION__); fflush(stdout);
#else
#define KOMP_TRACE_ENTER 
#define KOMP_TRACE_EXIT
#endif

typedef struct komp_wrapper_loop_t {
    void (*fncpy) (void *, void*);
    void (*fn) (void *);
    void *data;
    long arg_size;
    long arg_align;
    long start;
    long end;
    long incr;
    int priority;
} komp_wrapper_loop_t;


/* entry point */
static void komp_bopy_taskloop(
    unsigned long long start,  /* first */
    unsigned long long end,   /* last */
    void* a)
{
  komp_wrapper_loop_t* arg = (komp_wrapper_loop_t*)a;
  long s = arg->start + start * arg->incr;
  long e = arg->start + end * arg->incr;
  char buffer[arg->arg_size+arg->arg_align];
  char* data = (char *) (((uintptr_t) buffer + arg->arg_align - 1) & ~(uintptr_t) (arg->arg_align - 1));
  arg->fncpy(data, arg->data);
  ((long*)data)[0] = s;
  ((long*)data)[0] = e;
  arg->fn( data );
}

/*
*/
void
GOMP_taskloop (void (*fn) (void *), void *data, void (*cpyfn) (void *, void *),
               long arg_size, long arg_align, unsigned flags,
               unsigned long num_tasks, int priority,
               long start, long end, long incr
)
{
  kaapi_processor_t* kproc = komp_self_processor();
  komp_thread_t* thread = (komp_thread_t*)kaapi_kproc2thread( kproc );
  komp_foreach_attr_t attr;

  kaapi_task_t* currtask = (kaapi_task_t*)komp_current_task(thread);
  komp_taskdata_t* currtask_data = (komp_taskdata_t*)currtask->arg;

  if ((currtask_data->task_group !=0) && (currtask_data->task_group->cancelled !=0)) return;

//  komp_dataenv_icv_t* save_icvs = komp_get_dataenv_icv( kproc );

  /* verify inputs */
  if (((incr > 0) && (start >= end)) || ((incr <0) && (start <=end)))
    return;

  if ((flags & GOMP_TASK_FLAG_NOGROUP) ==0)
    komp_taskgroup_begin();

  komp_wrapper_loop_t* arg = komp_data_push( thread, sizeof(komp_wrapper_loop_t) );
  arg->start = start;
  arg->end   = end;
  arg->incr  = incr;
  arg->priority = 0;
  arg->fncpy = cpyfn;
  arg->fn    = fn;
  arg->data  = data;
  arg->arg_size = arg_size;
  arg->arg_align = arg_align;

  komp_foreach_attr_init( &attr );
  long iter_size;
  if (incr >0)
    iter_size = (end - start + incr-1)/incr;
  else
    iter_size = (start - end - incr-1)/-incr;

  if (flags & GOMP_TASK_FLAG_GRAINSIZE)
  {
    komp_foreach_attr_set_grains(&attr, num_tasks, num_tasks);
    komp_foreach_attr_set_policy(&attr, KOMP_FOREACH_SCHED_ADAPTIVE);
  }
  else if (num_tasks !=0)
  {
    komp_foreach_attr_set_grains(&attr, iter_size/num_tasks, iter_size/num_tasks);
    komp_foreach_attr_set_policy(&attr, KOMP_FOREACH_SCHED_STATIC);
  }
  else {
    komp_foreach_attr_set_grains(&attr, 1, 1);
    komp_foreach_attr_set_policy(&attr, KOMP_FOREACH_SCHED_ADAPTIVE);
  }

  if (flags & GOMP_TASK_FLAG_PRIORITY)
    arg->priority = priority;
  else
    arg->priority = 0;


  if (0== komp_loop_workinit( thread, 0, &attr, 0, iter_size, 1, komp_bopy_taskloop, arg))
    return;

  long long s;
  long long e;
  while (komp_loop_worknext(thread, &s, &e) != 0)
    komp_bopy_taskloop( s, e, arg);
  komp_loop_workfini( thread );

  if ((flags & GOMP_TASK_FLAG_NOGROUP) == 0)
    komp_taskgroup_end();
}
