/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012, 2013, 2014 INRIA.
**
** Contributors :
**
** francois.broquedis@imag.fr
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef LIBIOMP_H
#define LIBIOMP_H

#include <stdio.h>
#include <stdint.h>

#include "libkomp_impl.h"

#define kmp_int32 int32_t
#define kmp_uint32 uint32_t
#define kmp_int64 int64_t
#define kmp_uint64 uint64_t

#ifdef TRACE_LIB
#define TRACE_ENTER(...) fprintf(stderr, "[debug] {%s} Entering %s.\n", __FILE__, __FUNCTION__);
#define TRACE_LEAVE(...) fprintf(stderr, "[debug] {%s} Leaving %s.\n", __FILE__, __FUNCTION__);
#else
#define TRACE_ENTER(...)
#define TRACE_LEAVE(...)
#endif


typedef struct ident {
    kmp_int32 reserved_1;
    kmp_int32 flags;
    kmp_int32 reserved_2;
    kmp_int32 reserved_3;
    char *psource;
} ident_t;

// Some forward declarations.

typedef union  kmp_team      kmp_team_t;
typedef union  kmp_task_team kmp_task_team_t;
typedef union  kmp_team      kmp_team_p;
typedef union  kmp_info      kmp_info_p;
typedef union  kmp_root      kmp_root_p;


typedef kmp_int32 (* kmp_routine_entry_t)( kmp_int32, void * );
typedef void (*kmpc_micro)(kmp_int32 *global_tid, kmp_int32 *bound_tid, ...);
typedef void (*kmpc_micro_bound)        ( kmp_int32 * bound_tid, kmp_int32 * bound_nth, ... );
typedef void (*microtask_t)( int *gtid, int *npr, ... );

typedef void *(*kmpc_ctor)    (void *);
typedef void (*kmpc_dtor)     (void * /*, size_t */);
typedef void *(*kmpc_cctor)   (void *, void *);
typedef void *(*kmpc_ctor_vec)  (void *, size_t);
typedef void (*kmpc_dtor_vec)   (void *, size_t);
typedef void *(*kmpc_cctor_vec) (void *, void *, size_t);



typedef struct kmp_tasking_flags {          /* Total struct must be exactly 32 bits */
    /* Compiler flags */                    /* Total compiler flags must be 16 bits */
    unsigned tiedness    : 1;               /* task is either tied (1) or untied (0) */
    unsigned final       : 1;               /* task is final(1) so execute immediately */
    unsigned merged_if0  : 1;               /* no __kmpc_task_{begin/complete}_if0 calls in if0 code path */
#if 1//OMP_40_ENABLED
    unsigned destructors_thunk : 1;         /* set if the compiler creates a thunk to invoke destructors from the runtime */
#if 1//OMP_41_ENABLED
    unsigned proxy       : 1;               /* task is a proxy task (it will be executed outside the context of the RTL) */
    unsigned reserved    : 11;              /* reserved for compiler use */
#else
    unsigned reserved    : 12;              /* reserved for compiler use */
#endif
#else // OMP_40_ENABLED
    unsigned reserved    : 13;              /* reserved for compiler use */
#endif // OMP_40_ENABLED

    /* Library flags */                     /* Total library flags must be 16 bits */
    unsigned tasktype    : 1;               /* task is either explicit(1) or implicit (0) */
    unsigned task_serial : 1;               /* this task is executed immediately (1) or deferred (0) */
    unsigned tasking_ser : 1;               /* all tasks in team are either executed immediately (1) or may be deferred (0) */
    unsigned team_serial : 1;               /* entire team is serial (1) [1 thread] or parallel (0) [>= 2 threads] */
                                            /* If either team_serial or tasking_ser is set, task team may be NULL */
    /* Task State Flags: */
    unsigned started     : 1;               /* 1==started, 0==not started     */
    unsigned executing   : 1;               /* 1==executing, 0==not executing */
    unsigned complete    : 1;               /* 1==complete, 0==not complete   */
    unsigned freed       : 1;               /* 1==freed, 0==allocateed        */
    unsigned native      : 1;               /* 1==gcc-compiled task, 0==intel */
    unsigned reserved31  : 7;               /* reserved for library use */

} kmp_tasking_flags_t;


typedef struct kmp_task_t {         /* GEH: Shouldn't this be aligned somehow? */
    void *              shareds;    /**< pointer to block of pointers to shared vars   */
    kmp_routine_entry_t routine;    /**< pointer to routine to call for executing task */
    kmp_int32           part_id;    /**< part id for the task                          */
#if 1//OMP_40_ENABLED
    kmp_routine_entry_t destructors;        /* pointer to function to invoke deconstructors of firstprivate C++ objects */
#endif // OMP_40_ENABLED
    /*  private vars  */
} kmp_task_t;


//Task structures
//FIXME cleanup useless stuff

typedef komp_taskdata_t kmp_taskdata_t;

typedef struct kmp_depend_info {
     void *               base_addr;
     size_t               len;
     struct {
         bool             in:1;
         bool             out:1;
     } flags;
} kmp_depend_info_t;


enum sched_type {
    kmp_sch_lower                     = 32,   /**< lower bound for unordered values */
    kmp_sch_static_chunked            = 33,
    kmp_sch_static                    = 34,   /**< static unspecialized */
    kmp_sch_dynamic_chunked           = 35,
    kmp_sch_guided_chunked            = 36,   /**< guided unspecialized */
    kmp_sch_runtime                   = 37,
    kmp_sch_auto                      = 38,   /**< auto */
    kmp_sch_trapezoidal               = 39,

    /* accessible only through KMP_SCHEDULE environment variable */
    kmp_sch_static_greedy             = 40,
    kmp_sch_static_balanced           = 41,
    /* accessible only through KMP_SCHEDULE environment variable */
    kmp_sch_guided_iterative_chunked  = 42,
    kmp_sch_guided_analytical_chunked = 43,

    kmp_sch_steal                     = 44,   /**< accessible only through KMP_SCHEDULE environment variable */

    /* accessible only through KMP_SCHEDULE environment variable */
    kmp_sch_upper                     = 45,   /**< upper bound for unordered values */

    kmp_sch_adaptive                  = 46,

    kmp_ord_lower                     = 64,   /**< lower bound for ordered values, must be power of 2 */
    kmp_ord_static_chunked            = 65,
    kmp_ord_static                    = 66,   /**< ordered static unspecialized */
    kmp_ord_dynamic_chunked           = 67,
    kmp_ord_guided_chunked            = 68,
    kmp_ord_runtime                   = 69,
    kmp_ord_auto                      = 70,   /**< ordered auto */
    kmp_ord_trapezoidal               = 71,
    kmp_ord_upper                     = 72,   /**< upper bound for ordered values */

//#if OMP_40_ENABLED
    /* Schedules for Distribute construct */
    kmp_distribute_static_chunked     = 91,   /**< distribute static chunked */
    kmp_distribute_static             = 92,   /**< distribute static unspecialized */
//#endif

    /*
     * For the "nomerge" versions, kmp_dispatch_next*() will always return
     * a single iteration/chunk, even if the loop is serialized.  For the
     * schedule types listed above, the entire iteration vector is returned
     * if the loop is serialized.  This doesn't work for gcc/gcomp sections.
     */
    kmp_nm_lower                      = 160,  /**< lower bound for nomerge values */

    kmp_nm_static_chunked             = (kmp_sch_static_chunked - kmp_sch_lower + kmp_nm_lower),
    kmp_nm_static                     = 162,  /**< static unspecialized */
    kmp_nm_dynamic_chunked            = 163,
    kmp_nm_guided_chunked             = 164,  /**< guided unspecialized */
    kmp_nm_runtime                    = 165,
    kmp_nm_auto                       = 166,  /**< auto */
    kmp_nm_trapezoidal                = 167,

    /* accessible only through KMP_SCHEDULE environment variable */
    kmp_nm_static_greedy              = 168,
    kmp_nm_static_balanced            = 169,
    /* accessible only through KMP_SCHEDULE environment variable */
    kmp_nm_guided_iterative_chunked   = 170,
    kmp_nm_guided_analytical_chunked  = 171,
    kmp_nm_static_steal               = 172,  /* accessible only through OMP_SCHEDULE environment variable */

    kmp_nm_ord_static_chunked         = 193,
    kmp_nm_ord_static                 = 194,  /**< ordered static unspecialized */
    kmp_nm_ord_dynamic_chunked        = 195,
    kmp_nm_ord_guided_chunked         = 196,
    kmp_nm_ord_runtime                = 197,
    kmp_nm_ord_auto                   = 198,  /**< auto */
    kmp_nm_ord_trapezoidal            = 199,
    kmp_nm_upper                      = 200,  /**< upper bound for nomerge values */

    kmp_sch_default = kmp_sch_static  /**< default scheduling algorithm */
};

static inline void print_ident(const ident_t *loc) {
    fprintf(stderr, "ident_t *loc: %p\n", loc);
    fprintf(stderr, "\tloc->reserved_1 = %i\n", loc->reserved_1);
    fprintf(stderr, "\tloc->flags \t= %i\n", loc->flags);
    fprintf(stderr, "\tloc->reserved_2 = %i\n", loc->reserved_2);
    fprintf(stderr, "\tloc->reserved_3 = %i\n", loc->reserved_3);
    fprintf(stderr, "\tloc->psource \t= %s\n", loc->psource);
}





/* see Intel omp.h for the definition of the types : */
typedef struct komp_lock_t
{
  unsigned char _x[64] 
    __attribute__((__aligned__(8)));
} komp_lock_t;

typedef komp_lock_t omp_lock_t;

typedef struct komp_nest_lock_t
{
  unsigned char _x[80] 
    __attribute__((__aligned__(8)));
} komp_nest_lock_t;

typedef komp_nest_lock_t omp_nest_lock_t;

//Full list of libomp exported entry point
#define KMP_EXPORT extern

//NOTE: these begin/end are *not* generated by clang ! But may be useful, icc ?
KMP_EXPORT void   __kmpc_begin                ( ident_t *, kmp_int32 flags );
KMP_EXPORT void   __kmpc_end                  ( ident_t * );

KMP_EXPORT void   __kmpc_threadprivate_register_vec ( ident_t *, void * data, kmpc_ctor_vec ctor,
                                                      kmpc_cctor_vec cctor, kmpc_dtor_vec dtor, size_t vector_length );
KMP_EXPORT void   __kmpc_threadprivate_register     ( ident_t *, void * data, kmpc_ctor ctor, kmpc_cctor cctor, kmpc_dtor dtor );
KMP_EXPORT void * __kmpc_threadprivate              ( ident_t *, kmp_int32 global_tid, void * data, size_t size );
KMP_EXPORT void*
__kmpc_threadprivate_cached( ident_t * loc, kmp_int32 global_tid,
                             void * data, size_t size, void *** cache );

/*NOTE: parallel.c */

extern int __kmp_invoke_microtask( microtask_t pkfn, int gtid, int npr, int argc, void *argv[] );
KMP_EXPORT kmp_int32  __kmpc_global_thread_num  ( ident_t * );
//KMP_EXPORT kmp_int32  __kmpc_global_num_threads ( ident_t * );
//KMP_EXPORT kmp_int32  __kmpc_bound_thread_num   ( ident_t * );
//KMP_EXPORT kmp_int32  __kmpc_bound_num_threads  ( ident_t * );

//NOTE: never generated by clang's codegen
KMP_EXPORT kmp_int32  __kmpc_ok_to_fork     ( ident_t * );
KMP_EXPORT void       __kmpc_fork_call          ( ident_t *, kmp_int32 nargs, kmpc_micro microtask, ... );

KMP_EXPORT void   __kmpc_serialized_parallel     ( ident_t *, kmp_int32 global_tid );
KMP_EXPORT void   __kmpc_end_serialized_parallel ( ident_t *, kmp_int32 global_tid );

/* NOTE: barrier.c */

//KMP_EXPORT void   __kmpc_flush              ( ident_t *);
KMP_EXPORT void   __kmpc_barrier            ( ident_t *, kmp_int32 global_tid );
KMP_EXPORT kmp_int32  __kmpc_master         ( ident_t *, kmp_int32 global_tid );
KMP_EXPORT void   __kmpc_end_master         ( ident_t *, kmp_int32 global_tid );
//KMP_EXPORT void   __kmpc_ordered            ( ident_t *, kmp_int32 global_tid );
//KMP_EXPORT void   __kmpc_end_ordered        ( ident_t *, kmp_int32 global_tid );
typedef kmp_int32 kmp_critical_name[8];
KMP_EXPORT void   __kmpc_critical           ( ident_t *, kmp_int32 global_tid, kmp_critical_name * );
KMP_EXPORT void   __kmpc_end_critical       ( ident_t *, kmp_int32 global_tid, kmp_critical_name * );

#if OMP_41_ENABLED
//KMP_EXPORT void   __kmpc_critical_with_hint ( ident_t *, kmp_int32 global_tid, kmp_critical_name *, uintptr_t hint );
#endif

//KMP_EXPORT kmp_int32  __kmpc_barrier_master ( ident_t *, kmp_int32 global_tid );
//KMP_EXPORT void   __kmpc_end_barrier_master ( ident_t *, kmp_int32 global_tid );

//KMP_EXPORT kmp_int32  __kmpc_barrier_master_nowait ( ident_t *, kmp_int32 global_tid );

KMP_EXPORT kmp_int32  __kmpc_single         ( ident_t *, kmp_int32 global_tid );
KMP_EXPORT void   __kmpc_end_single         ( ident_t *, kmp_int32 global_tid );

//KMP_EXPORT void KMPC_FOR_STATIC_INIT    ( ident_t *loc, kmp_int32 global_tid, kmp_int32 schedtype, kmp_int32 *plastiter,
                                          //kmp_int *plower, kmp_int *pupper, kmp_int *pstride, kmp_int incr, kmp_int chunk );

KMP_EXPORT void __kmpc_for_static_fini  ( ident_t *loc, kmp_int32 global_tid );

//KMP_EXPORT void __kmpc_copyprivate( ident_t *loc, kmp_int32 global_tid, size_t cpy_size, void *cpy_data, void(*cpy_func)(void*,void*), kmp_int32 didit );


/* --------------------------------------------------------------------------- */

/*
 * Taskq interface routines
 */

//KMP_EXPORT kmpc_thunk_t * __kmpc_taskq (ident_t *loc, kmp_int32 global_tid, kmpc_task_t taskq_task, size_t sizeof_thunk,
                                        //size_t sizeof_shareds, kmp_int32 flags, kmpc_shared_vars_t **shareds);
//KMP_EXPORT void __kmpc_end_taskq (ident_t *loc, kmp_int32 global_tid, kmpc_thunk_t *thunk);
//KMP_EXPORT kmp_int32 __kmpc_task (ident_t *loc, kmp_int32 global_tid, kmpc_thunk_t *thunk);
//KMP_EXPORT void __kmpc_taskq_task (ident_t *loc, kmp_int32 global_tid, kmpc_thunk_t *thunk, kmp_int32 status);
//KMP_EXPORT void __kmpc_end_taskq_task (ident_t *loc, kmp_int32 global_tid, kmpc_thunk_t *thunk);
//KMP_EXPORT kmpc_thunk_t * __kmpc_task_buffer (ident_t *loc, kmp_int32 global_tid, kmpc_thunk_t *taskq_thunk, kmpc_task_t task);

/* ------------------------------------------------------------------------ */

/*
 * OMP 3.0 tasking interface routines
 */

/* NOTE: task.c */
KMP_EXPORT kmp_int32
__kmpc_omp_task( ident_t *loc_ref, kmp_int32 gtid, kmp_task_t * new_task );
KMP_EXPORT kmp_task_t*
__kmpc_omp_task_alloc( ident_t *loc_ref, kmp_int32 gtid, kmp_int32 flags,
                       size_t sizeof_kmp_task_t, size_t sizeof_shareds,
                       kmp_routine_entry_t task_entry );
//KMP_EXPORT void
//__kmpc_omp_task_begin_if0( ident_t *loc_ref, kmp_int32 gtid, kmp_task_t * task );
//KMP_EXPORT void
//__kmpc_omp_task_complete_if0( ident_t *loc_ref, kmp_int32 gtid, kmp_task_t *task );
//KMP_EXPORT kmp_int32
//__kmpc_omp_task_parts( ident_t *loc_ref, kmp_int32 gtid, kmp_task_t * new_task );
KMP_EXPORT kmp_int32
__kmpc_omp_taskwait( ident_t *loc_ref, kmp_int32 gtid );

KMP_EXPORT kmp_int32 
__kmpc_omp_taskyield( ident_t *loc_ref, kmp_int32 gtid, int end_part );

#if TASK_UNUSED
//void __kmpc_omp_task_begin( ident_t *loc_ref, kmp_int32 gtid, kmp_task_t * task );
//void __kmpc_omp_task_complete( ident_t *loc_ref, kmp_int32 gtid, kmp_task_t *task );
#endif // TASK_UNUSED

/* ------------------------------------------------------------------------ */


KMP_EXPORT void __kmpc_omp_set_task_name(const char *name);

KMP_EXPORT void __kmpc_omp_set_task_affinity(kmp_uint32 kind, kmp_uint64 affinity, kmp_uint32 strict);

KMP_EXPORT void __kmpc_begin_push_init(int kind);
KMP_EXPORT void __kmpc_end_push_init();

//#if OMP_40_ENABLED

//KMP_EXPORT void __kmpc_taskgroup( ident_t * loc, int gtid );
//KMP_EXPORT void __kmpc_end_taskgroup( ident_t * loc, int gtid );

KMP_EXPORT kmp_int32 __kmpc_omp_task_with_deps ( ident_t *loc_ref, kmp_int32 gtid, kmp_task_t * new_task,
                                                 kmp_int32 ndeps, kmp_depend_info_t *dep_list,
                                                 kmp_int32 ndeps_noalias, kmp_depend_info_t *noalias_dep_list );
//KMP_EXPORT void __kmpc_omp_wait_deps ( ident_t *loc_ref, kmp_int32 gtid, kmp_int32 ndeps, kmp_depend_info_t *dep_list,
                                          //kmp_int32 ndeps_noalias, kmp_depend_info_t *noalias_dep_list );
//extern void __kmp_release_deps ( kmp_int32 gtid, kmp_taskdata_t *task );

//extern kmp_int32 __kmp_omp_task( kmp_int32 gtid, kmp_task_t * new_task, bool serialize_immediate );

//KMP_EXPORT kmp_int32 __kmpc_cancel(ident_t* loc_ref, kmp_int32 gtid, kmp_int32 cncl_kind);
//KMP_EXPORT kmp_int32 __kmpc_cancellationpoint(ident_t* loc_ref, kmp_int32 gtid, kmp_int32 cncl_kind);
//KMP_EXPORT kmp_int32 __kmpc_cancel_barrier(ident_t* loc_ref, kmp_int32 gtid);
//KMP_EXPORT int __kmp_get_cancellation_status(int cancel_kind);

#if OMP_41_ENABLED

//KMP_EXPORT void __kmpc_proxy_task_completed( kmp_int32 gtid, kmp_task_t *ptask );
//KMP_EXPORT void __kmpc_proxy_task_completed_ooo ( kmp_task_t *ptask );

#endif

//#endif


/*
 * Lock interface routines (fast versions with gtid passed in)
 */
KMP_EXPORT void __kmpc_init_lock( ident_t *loc, kmp_int32 gtid,  void **user_lock );
//KMP_EXPORT void __kmpc_init_nest_lock( ident_t *loc, kmp_int32 gtid, void **user_lock );
KMP_EXPORT void __kmpc_destroy_lock( ident_t *loc, kmp_int32 gtid, void **user_lock );
//KMP_EXPORT void __kmpc_destroy_nest_lock( ident_t *loc, kmp_int32 gtid, void **user_lock );

KMP_EXPORT void __kmpc_set_lock( ident_t *loc, kmp_int32 gtid, void **user_lock );
//KMP_EXPORT void __kmpc_set_nest_lock( ident_t *loc, kmp_int32 gtid, void **user_lock );
KMP_EXPORT void __kmpc_unset_lock( ident_t *loc, kmp_int32 gtid, void **user_lock );
//KMP_EXPORT void __kmpc_unset_nest_lock( ident_t *loc, kmp_int32 gtid, void **user_lock );
KMP_EXPORT int __kmpc_test_lock( ident_t *loc, kmp_int32 gtid, void **user_lock );
//KMP_EXPORT int __kmpc_test_nest_lock( ident_t *loc, kmp_int32 gtid, void **user_lock );

#if OMP_41_ENABLED
//KMP_EXPORT void __kmpc_init_lock_with_hint( ident_t *loc, kmp_int32 gtid, void **user_lock, uintptr_t hint );
//KMP_EXPORT void __kmpc_init_nest_lock_with_hint( ident_t *loc, kmp_int32 gtid, void **user_lock, uintptr_t hint );
#endif

/* ------------------------------------------------------------------------ */

/*
 * Interface to fast scalable reduce methods routines
 */

KMP_EXPORT kmp_int32 __kmpc_reduce_nowait( ident_t *loc, kmp_int32 global_tid,
                                           kmp_int32 num_vars, size_t reduce_size,
                                           void *reduce_data, void (*reduce_func)(void *lhs_data, void *rhs_data),
                                           kmp_critical_name *lck );
KMP_EXPORT void __kmpc_end_reduce_nowait( ident_t *loc, kmp_int32 global_tid, kmp_critical_name *lck );
KMP_EXPORT kmp_int32 __kmpc_reduce( ident_t *loc, kmp_int32 global_tid,
                                    kmp_int32 num_vars, size_t reduce_size,
                                    void *reduce_data, void (*reduce_func)(void *lhs_data, void *rhs_data),
                                    kmp_critical_name *lck );
KMP_EXPORT void __kmpc_end_reduce( ident_t *loc, kmp_int32 global_tid, kmp_critical_name *lck );

/*
 * internal fast reduction routines
 */

//extern PACKED_REDUCTION_METHOD_T
//__kmp_determine_reduction_method( ident_t *loc, kmp_int32 global_tid,
                                  //kmp_int32 num_vars, size_t reduce_size,
                                  //void *reduce_data, void (*reduce_func)(void *lhs_data, void *rhs_data),
                                  //kmp_critical_name *lck );

// this function is for testing set/get/determine reduce method
//KMP_EXPORT kmp_int32 __kmp_get_reduce_method( void );

//KMP_EXPORT kmp_uint64 __kmpc_get_taskid();
//KMP_EXPORT kmp_uint64 __kmpc_get_parent_taskid();

// this function exported for testing of KMP_PLACE_THREADS functionality
//KMP_EXPORT void __kmpc_place_threads(int,int,int,int,int);

/* ------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------ */

// C++ port
// missing 'extern "C"' declarations

//KMP_EXPORT kmp_int32 __kmpc_in_parallel( ident_t *loc );
//KMP_EXPORT void __kmpc_pop_num_threads(  ident_t *loc, kmp_int32 global_tid );
KMP_EXPORT void __kmpc_push_num_threads( ident_t *loc, kmp_int32 global_tid, kmp_int32 num_threads );

#if OMP_40_ENABLED
//KMP_EXPORT void __kmpc_push_proc_bind( ident_t *loc, kmp_int32 global_tid, int proc_bind );
//KMP_EXPORT void __kmpc_push_num_teams( ident_t *loc, kmp_int32 global_tid, kmp_int32 num_teams, kmp_int32 num_threads );
//KMP_EXPORT void __kmpc_fork_teams(ident_t *loc, kmp_int32 argc, kmpc_micro microtask, ...);

#endif

#endif /* LIBIOMP_H */

