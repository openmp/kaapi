/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** francois.broquedis@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#if defined(__linux__)
#define _GNU_SOURCE
#include <sched.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <errno.h>

#include "libkomp_impl.h"
#include "kaapi_util.h"

#if defined(KAAPI_USE_PERFCOUNTER)

#if defined(__linux__) 
#define _GNU_SOURCE
#endif
#include <dlfcn.h>
#endif

#if defined(KAAPI_USE_PERFCOUNTER)
/* Returns 0 if is able to return a name associated to the address (function pointer).
   IF EEXIST is returned then, the address cannot be found to be associated
   to a name. Else returns error during initialization of sub-library.
*/
static int komp_get_taskname(void *address, char* buffer, size_t sizebuffer);
#endif

#if defined(KAAPI_USE_PERFCOUNTER)
static kaapi_lock_t lock_format = KAAPI_LOCK_INITIALIZER;
#endif
// DEBUG
#include "kaapi_foreach_impl.h"

#define KOMP_DECLARE_FMT(name) \
  struct komp_format_t* komp_##name##_format;

#define KOMP_INIT_FMT(name) \
  komp_##name##_format = (struct komp_format_t*)kaapi_##name##_format;

KOMP_DECLARE_FMT(schar)
KOMP_DECLARE_FMT(char)
KOMP_DECLARE_FMT(shrt)
KOMP_DECLARE_FMT(int)
KOMP_DECLARE_FMT(long)
KOMP_DECLARE_FMT(llong)
KOMP_DECLARE_FMT(int8)
KOMP_DECLARE_FMT(int16)
KOMP_DECLARE_FMT(int32)
KOMP_DECLARE_FMT(int64)
KOMP_DECLARE_FMT(uchar)
KOMP_DECLARE_FMT(ushrt)
KOMP_DECLARE_FMT(uint)
KOMP_DECLARE_FMT(ulong)
KOMP_DECLARE_FMT(ullong)
KOMP_DECLARE_FMT(uint8)
KOMP_DECLARE_FMT(uint16)
KOMP_DECLARE_FMT(uint32)
KOMP_DECLARE_FMT(uint64)
KOMP_DECLARE_FMT(flt)
KOMP_DECLARE_FMT(dbl)
KOMP_DECLARE_FMT(ldbl)
KOMP_DECLARE_FMT(voidp)


static komp_device_icv_t komp_global_device_icv = {
  .def_sched         = komp_sched_static,
  .stacksize         = 0,  /* means system default */
  .waitpolicy        = komp_wait_policy_active, /* no choice in this version ! */
  .max_active_levels = 256,
  .cancel            = 0
};

komp_device_icv_t* komp_get_global_device_icv(void)
{
  return &komp_global_device_icv;
}

__thread komp_globalenv_icv_t komp_global_dataenv_icv = {
  .icvs.h_nthreads    = 0, /* runtime selection */
  .icvs.run_sched     = komp_sched_static,
  .icvs.run_sched_modifier = 0,
  .icvs.dyn           = 0,
  .icvs.nest          = 0,
  .icvs.h_bind        = komp_proc_bind_false,
  .icvs.prio          = 0,
  .icvs.flag          = 0,
  .icvs.task_name     = 0,
  .nthreads           = 0,
  .binds              = 0,
  .thread_limit       = 8192 /* sens on cpu */
};


static bool
komp_parse_stacksize (const char *name, unsigned long *pvalue)
{
  char* env = getenv(name);
  if (env ==0) return false;
  if (kaapi_parse_size( &env, pvalue) && (*env == 0))
    return true;

  fprintf (stderr, "[libKOMP] Wrong stacksize value '%s' for '%s'!\n", getenv(name), name);
  return false;
}


static bool
komp_parse_unsigned_int( char* name, unsigned int* pvalue )
{
  char* env = getenv(name);
  if (env ==0) return false;
  if (kaapi_parse_unsigned_int( &env, pvalue) && (*env == 0))
    return true;

  fprintf (stderr, "[libKOMP] Wrong value '%s' for '%s'!\n", getenv(name), name);
  return false;
}

__attribute__((unused))
static bool
komp_parse_unsigned_short( char* name, unsigned short* pvalue )
{
  char* env = getenv(name);
  if (env ==0) return false;
  if (kaapi_parse_unsigned_short( &env, pvalue) && (*env == 0))
    return true;

  fprintf (stderr, "[libKOMP] Wrong value '%s' for '%s'!\n", getenv(name), name);
  return false;
}


static bool
komp_parse_int( char* name, int* pvalue )
{
  char* env = getenv(name);
  if (env ==0) return false;
  if (kaapi_parse_int( &env, pvalue) && (*env == 0))
    return true;

  fprintf (stderr, "[libKOMP] Wrong value for '%s'!\n", name);
  return false;
}


static bool
komp_parse_list_unsigned_int (char* name, unsigned int **pvalue)
{
  char* env = getenv(name);
  unsigned int count;
  if (env ==0) return false;
  if (kaapi_parse_list_unsigned_int( &env, &count, pvalue) && (*env == 0))
    return true;

  fprintf (stderr, "[libKOMP] Wrong value for '%s'!\n", name);
  return false;
}

__attribute__((unused))
static bool
komp_parse_list_unsigned_short (char* name, unsigned short **pvalue)
{
  char* env = getenv(name);
  unsigned short count;
  if (env ==0) return false;
  if (kaapi_parse_list_unsigned_short( &env, &count, pvalue) && (*env == 0))
    return true;

  fprintf (stderr, "[libKOMP] Wrong value for '%s'!\n", name);
  return false;
}


static bool
komp_parse_list_procbind (char* name, komp_proc_bind_t **pvalue)
{
  char* env = getenv(name);
  unsigned int count;
  if (env ==0) return false; 
  if (kaapi_parse_list_procbind( &env, &count, (kaapi_procbind_t**)pvalue) && (*env == 0))
  {
    /* alway one more place free in the list */
    (*pvalue)[count] = komp_proc_bind_end_list;
    return true;
  }
  *pvalue = 0;
  fprintf (stderr, "[libKOMP] Wrong value for '%s'!\n", name);
  return false;
}


static const char* komp_unparse_procbind(komp_proc_bind_t value)
{
  switch (value) {
    case komp_proc_bind_false: return "false";
    case komp_proc_bind_true: return "true";
    case komp_proc_bind_master: return "master";
    case komp_proc_bind_close: return "close";
    case komp_proc_bind_spread: return "spread";

    default:
      fprintf(stderr,"[libKOMP] bad value: %i for procbind code\n", value);
      exit(1);
  }
}

char komp_unparse_procbind_list_buffer[4096];
static const char* komp_unparse_procbind_list(
    komp_proc_bind_t h_bind,
    komp_proc_bind_t* bindproc_list
)
{
  ;
  char* pos;
  int cnt;

  pos = &komp_unparse_procbind_list_buffer[0];
  komp_unparse_procbind_list_buffer[0] = 0;
  cnt = sprintf(pos,"%s", komp_unparse_procbind(h_bind));
  kaapi_assert(cnt >0);
  pos += cnt;

  while ((bindproc_list !=0) && (*bindproc_list !=0))
  {
    cnt = sprintf(pos,",%s",komp_unparse_procbind(*bindproc_list));
    pos += cnt;
    kaapi_assert(cnt >0);
    ++bindproc_list;
  }
  return komp_unparse_procbind_list_buffer;
}


static bool
komp_parse_bool( char* name, int8_t* pvalue )
{
  char* env = getenv(name);
  *pvalue = 0;
  if (env ==0)
  { /* undef -> false */
    *pvalue = false;
    return false;
  }
  if (kaapi_parse_bool( &env, pvalue) && (*env == 0))
    return true;

  fprintf (stderr, "[libKOMP] Wrong value for '%s'! Should be set to one of (true|false).\n", name);
  return false;
}


/* Parse the active|passive environment variable. */
static bool
parse_waitpolicy (char* env, bool* pvalue)
{
  *pvalue = false;
  if (env == NULL)
    return false;

  if (strcasecmp (env, "passive") == 0)
  {
    *pvalue = true;
    return true;
  }
  if (strcasecmp (env, "active") == 0)
  {
    *pvalue = false;
    return true;
  }
  return false;
}

static bool
komp_parse_waitpolicy( const char* name, bool* pvalue )
{
  char* env = getenv(name);
  *pvalue = false;
  if (env == 0)
     return true;
  if (!parse_waitpolicy( env, pvalue))
  {
    fprintf (stderr, "[libKOMP] Wrong value for '%s'! Should be set to one of (passive | active).\n",
                   name);
    return false;
  }
  return true;
}

#if defined (KAAPI_USE_PERFCOUNTER)
static bool
komp_parse_perfcounter( const char* name, int* pvalue )
{
  char* env = getenv(name);
  if (env ==0) return false; 
  if (kaapi_parse_perfcounter( &env, pvalue) && (*env ==0))
    return true;

  fprintf (stderr, "[libKOMP] Wrong value for '%s'! Should be set to one of (no | full | resume | gplot).\n",
                 name);
  return false;
}
#endif


static bool
komp_parse_stealprotocol( const char* name, kaapi_rtparam_t* rt_param )
{
  char* env = getenv(name);
  if (env ==0) return false; 
  if (kaapi_parse_stealprotocol( &env, rt_param) && (*env ==0))
    return true;

  fprintf (stderr, "[libKOMP] Wrong value for '%s'! Should be set to one of (ccsync | qlock | null).\n",
                   name);
  return false;
}

static inline const char *
komp_env_defined (char* name)
{
  if (getenv(name) != 0)
    return "user-defined";
  return "libKOMP default";
}

static const char* komp_nthreads2str( unsigned int h_nthread, unsigned int* nthreads)
{
  static char buffer[1024];
  memset(buffer,0, 1024);
  char* data = buffer;
  int first = 1;
  if (h_nthread ==0) return "<empty>";
  data += snprintf(data, 1024-(data-buffer), "%u", h_nthread );
  if (nthreads ==0) return buffer;
  do {
    if (first)
    {
      data += snprintf(data, 1024-(data-buffer), "%u", *nthreads);
      first = 0;
    }
    else
      data += snprintf(data, 1024-(data-buffer), ",%u", *nthreads);
    ++nthreads;
  } while (*nthreads != 0);
  return buffer;
}

static const char* komp_unparse_schedule(komp_sched_t sched, unsigned long run_sched_modifier)
{
  static char buffer[1024];
  char* data = buffer;
  switch (sched) {
    case komp_sched_auto:
      data += sprintf(data, "auto");
      break;
    case komp_sched_dynamic:
      data += sprintf(data, "auto");
      break;
    case komp_sched_static:
      data += sprintf(data, "auto");
      break;
    case komp_sched_guided:
      data += sprintf(data, "auto");
      break;
    case komp_sched_adaptive:
      data += sprintf(data, "auto");
      break;
    case komp_sched_steal:
      data += sprintf(data, "auto");
      break;
  }
  if (run_sched_modifier !=0)
  {
    switch (sched) {
      case komp_sched_auto:
      case komp_sched_dynamic:
      case komp_sched_static:
      case komp_sched_guided:
      case komp_sched_adaptive:
      case komp_sched_steal:
        printf(data,",%lu",run_sched_modifier);
        break;
    }
  }
  return buffer;
}



/* Display runtime-related information as a banner printed before
   running the OpenMP application. */
static void
komp_display_env (int mode)
{
  if (!mode) return;

  fprintf(stdout, "OPENMP DISPLAY ENVIRONMENT BEGIN\n");
  fprintf (stdout, "  OMP_NUM_THREADS\t=\t%s\t(%s)\n",
	   komp_nthreads2str(komp_global_dataenv_icv.icvs.h_nthreads, komp_global_dataenv_icv.nthreads),
     komp_env_defined("OMP_NUM_THREADS")
  );
  fprintf (stdout, "  OMP_THREAD_LIMIT\t=\t%u\t(%s)\n",
	   komp_global_dataenv_icv.thread_limit,
     komp_env_defined("OMP_THREAD_LIMIT")
  );
  fprintf (stdout, "  OMP_STACKSIZE\t\t=\t%i\t(%s)\n",
	   (int)komp_global_device_icv.stacksize,
     komp_env_defined("OMP_STACKSIZE")
  );
  fprintf (stdout, "  OMP_MAX_ACTIVE_LEVELS\t=\t%i\t(%s)\n",
	   (int)komp_global_device_icv.max_active_levels,
     komp_env_defined("OMP_MAX_ACTIVE_LEVELS")
  );
  fprintf (stdout, "  OMP_NESTED\t\t=\t%s\t(%s)\n",
	   (komp_global_dataenv_icv.icvs.nest ? "true" : "false"),
     komp_env_defined("OMP_NESTED")
  );
  fprintf (stdout, "  OMP_DYNAMIC\t\t=\t%s\t(%s)\n",
	   (komp_global_dataenv_icv.icvs.dyn ? "true" : "false"),
     komp_env_defined("OMP_DYNAMIC")
  );
  fprintf (stdout, "  OMP_PROC_BIND\t\t=\t%s\t(%s)\n",
	   komp_unparse_procbind_list(komp_global_dataenv_icv.icvs.h_bind, komp_global_dataenv_icv.binds),
     komp_env_defined("OMP_PROC_BIND")
  );
  fprintf (stdout, "  OMP_PLACES\t\t=\t%s\t(%s)\n",
	   kaapi_unparse_places( (unsigned int)kaapi_default_param.places.len, kaapi_default_param.places.first ),
     komp_env_defined("OMP_PLACES")
  );
  fprintf (stdout, "  OMP_SCHEDULE\t\t=\t%s\t(%s)\n",
	   komp_unparse_schedule(komp_global_dataenv_icv.icvs.run_sched,
                        komp_global_dataenv_icv.icvs.run_sched_modifier),
     komp_env_defined("OMP_SCHEDULE")
  );
  fprintf (stdout, "  OMP_CANCELLATION\t=\t%s\t(%s)\n",
	   (komp_global_device_icv.cancel ? "true" : "false"),
     komp_env_defined("OMP_NESTED")
  );
#if defined (KAAPI_USE_PERFCOUNTER)
  fprintf (stdout, "  KOMP_DISPLAY_PERF\t=\t%s\t(%s)\n",
	   (   kaapi_tracelib_param.display_perfcounter == KAAPI_NO_DISPLAY_PERF ? "no"
       : kaapi_tracelib_param.display_perfcounter == KAAPI_DISPLAY_PERF_FULL ? "full"
       : kaapi_tracelib_param.display_perfcounter == KAAPI_DISPLAY_PERF_RESUME ? "resume"
       : kaapi_tracelib_param.display_perfcounter == KAAPI_DISPLAY_PERF_GPLOT ? "gplot"
       : "bad value" ),
     komp_env_defined("KOMP_DISPLAY_PERF")
  );
#endif
  fprintf (stdout, "  KOMP_STEAL_PROTOCOL\t=\t%s\t(%s)\n",
	   (   kaapi_default_param.request_post_fnc == kaapi_sched_ccsync_post_request ? "ccsync"
       : kaapi_default_param.request_post_fnc == kaapi_sched_qlock_post_request ? "qlock"
       : "bad value"
     ),
     komp_env_defined("KOMP_STEAL_PROTOCOL")
  );


  /* wspush_init/wspush/wsselect */
  {
    int i;
    int found=0;
    for (i=0; ; ++i)
    {
      if (kaapi_default_pushinit[i].entrypoint ==0) break; /* end of list */
      if (kaapi_default_pushinit[i].entrypoint == kaapi_default_param.wspush_init)
      {
        found = 1;
        break;
      }
    }
    if (!found)
    {
      fprintf(stderr, "***Kaapi: bad value for variable KAAPI_WSPUSH_INIT\n" );
      kaapi_assert(0);
    }
    fprintf( stdout, "  KAAPI_WSPUSH_INIT\t=\t%s\n", kaapi_default_pushinit[i].name );
  }


  {
    int i;
    int found=0;
    for (i=0; ; ++i)
    {
      if (kaapi_default_push[i].entrypoint ==0) break; /* end of list */
      if (kaapi_default_push[i].entrypoint == kaapi_default_param.wspush)
      {
        found = 1;
        break;
      }
    }
    if (!found)
    {
      fprintf(stderr, "***Kaapi: bad value for variable KAAPI_WSPUSH\n" );
      kaapi_assert(0);
    }
    fprintf( stdout, "  KAAPI_WSPUSH\t=\t%s\n", kaapi_default_push[i].name );
  }

  {
    int i;
    int found=0;
    for (i=0; ; ++i)
    {
      if (kaapi_default_selectvictim[i].entrypoint ==0) break; /* end of list */
      if (kaapi_default_selectvictim[i].entrypoint == kaapi_default_param.wsselect)
      {
        found = 1;
        break;
      }
    }
    if (!found)
    {
      fprintf(stderr, "***Kaapi: bad value for variable KAAPI_WSSELECT\n" );
      kaapi_assert(0);
    }
    fprintf( stdout, "  KAAPI_WSSELECT\t=\t%s\n", kaapi_default_selectvictim[i].name );
  }

  fprintf(stdout, "OPENMP DISPLAY ENVIRONMENT END\n");
}

/* Parse the OMP_DIPLAY_ENV environment variable.
   This feature comes from OpenMP 4 RC2.

   As described in section 4.12, OMP_DISPLAY_ENV can be set to one of
   these values: 
   true | false | verbose

   and display runtime-related information accordingly (ICVs, version
   number, ...).

   Note that, as opposed to the current version of the 4RC2 specs,
   lowercase characters have been chosen to fit the style of other
   OpenMP variables like OMP_NESTED.
 */
static bool
komp_parse_display_env (void)
{
  char *env = getenv ("OMP_DISPLAY_ENV");
  if (env == NULL)
    return false;

  if (kaapi_parse_display_env(&env, &kaapi_default_param.display_env) && (*env == 0))
    return true;

  fprintf (stderr, "[libKOMP] Wrong value for OMP_DISPLAY_ENV (%s)! "
                     "Should be set to one of (true | false | verbose).\n", env);
  return false;
}


static bool
komp_parse_schedule (char* name, komp_sched_t* prun_sched, int* prun_sched_modifier)
{
  char* env = getenv (name);
  int run_sched_modifier = 0;
  if (env == NULL)
    return false;

  kaapi_foreach_attr_policy_t kaapi_run_sched;
  if (kaapi_parse_schedule(&env, &kaapi_run_sched, &run_sched_modifier) && (*env ==0))
  {
    *prun_sched_modifier = run_sched_modifier;
    *prun_sched = (kaapi_run_sched == KAAPI_FOREACH_SCHED_AUTO ?
                      komp_sched_auto :
                      (komp_sched_t)kaapi_run_sched
    );
    return true;
  }
  fprintf (stderr, "[libKOMP] Wrong value for %s! "
               "Should be set to one of (static|dynamic|auto|guided|adaptive|steal)[,chunksize]\n", name);
  return false;
}


/* to free dynamic memory */
unsigned int* l_alloc1 = 0;

/*
*/
struct komp_thread_t* komp_self_thread(void)
{ 
  komp_thread_t* thread = (komp_thread_t*)kaapi_self_thread(); 
  if (thread !=0) return thread;
  kaapi_processor_t* kproc = kaapi_create_orphan_kprocessor();
  return (komp_thread_t*)kaapi_context2thread(kproc->context);
}


/*
*/
struct komp_team_t* komp_self_team(void)
{
  kaapi_processor_t* kproc = kaapi_self_processor();
  if (kproc ==0) return 0;
  return (struct komp_team_t*)kproc->team;
}


/*
*/
komp_dataenv_icv_t* komp_self_icvs(void)
{
#if KOMP_USE_ICV
  kaapi_processor_t* kproc = kaapi_self_processor();
  komp_dataenv_icv_t* icv = komp_get_dataenv_icv( kproc );
#else
  komp_dataenv_icv_t* icv = 0;
#endif
  return icv;
}


/*
*/
komp_dataenv_icv_t* komp_get_dataenv_icv(kaapi_processor_t* proc)
{
#if KOMP_USE_ICV
  struct kaapi_context_t* thread;
  if ((proc ==0) || (proc->context ==0))
    return &komp_global_dataenv_icv.icvs;

  thread = proc->context;
  if (thread->komp_icvs == 0)
  {
    thread->komp_icvs = &komp_global_dataenv_icv;
    return (komp_dataenv_icv_t*)thread->komp_icvs;
  }
  return (komp_dataenv_icv_t*)thread->komp_icvs;
#else
  return &komp_global_dataenv_icv.icvs;
#endif
}


/* for task
*/
komp_dataenv_icv_t* komp_swap_dataenv_icvs(
  struct komp_thread_t* th,
  komp_dataenv_icv_t* newicvs
)
{
  struct kaapi_thread_t* thread = (kaapi_thread_t*)th;
  kaapi_context_t* context = kaapi_thread2context(thread);
  komp_dataenv_icv_t* old = (komp_dataenv_icv_t*)context->komp_icvs;
  context->komp_icvs = newicvs;
  return old;
}


/* create a copy of the ICVS
*/
void komp_newdataenv_icvs(
  kaapi_processor_t* kproc,
  komp_dataenv_icv_t* parent_icvs,
  komp_dataenv_icv_t* task_icvs
)
{
  task_icvs->h_nthreads = parent_icvs->h_nthreads;
  task_icvs->run_sched  = parent_icvs->run_sched;
  task_icvs->run_sched_modifier = parent_icvs->run_sched_modifier;
  task_icvs->dyn        = parent_icvs->dyn;
  task_icvs->nest       = parent_icvs->nest;
  task_icvs->h_bind     = parent_icvs->h_bind;
  task_icvs->task_name  = 0;
  task_icvs->flag       = 0;
  task_icvs->affinity   = 0;
  task_icvs->prio       = 0;
  task_icvs->extra_deps = 0;
}


/* kaapi_data_push
*/
void* komp_data_push( komp_thread_t* th, unsigned long size )
{
  return kaapi_data_push((kaapi_thread_t*)th, size);
}

/* format definition for any libKOMP task */
static size_t
komp_task_format_get_size(const struct komp_format_t* fmt, const void* sp)
{
  /*sp points to a kmp_task_t, the komp_taskdata_t is right before*/
  komp_taskdata_t *task = (komp_taskdata_t *)sp;
  return task->size;
}

static void
komp_task_format_task_copy(const struct komp_format_t* fmt, void* sp_dest, const void* sp_src)
{
  komp_taskdata_t *task_src = ((komp_taskdata_t *)sp_src)-1;
  memcpy( sp_dest, task_src, task_src->size );
}

static
unsigned long komp_task_format_get_count_params(const struct komp_format_t* fmt, const void* sp)
{
  komp_taskdata_t *arg = (komp_taskdata_t *)sp;
  return arg->n_depend
#if KOMP_USE_ICV
   +arg->extra_n_depend
#endif
  ;
}

static
komp_access_mode_t komp_task_format_get_mode_param(
  const struct komp_format_t* fmt, unsigned int i, const void* sp
)
{
  komp_taskdata_t *arg = (komp_taskdata_t *)sp;
  if (i < arg->n_depend)
    return (komp_access_mode_t)arg->depends[i].mode;
#if KOMP_USE_ICV
  else
    return (komp_access_mode_t)arg->extra_depends[i-arg->n_depend].mode;
#else
   return KAAPI_ACCESS_MODE_VOID;
#endif
}

static
void* komp_task_format_get_data_param(
  const struct komp_format_t* fmt, unsigned int i, const void* sp
)
{
  komp_taskdata_t *arg = (komp_taskdata_t *)sp;
  if (i < arg->n_depend)
    return arg->depends[i].access.data;
#if KOMP_USE_ICV
  else
    return arg->extra_depends[i-arg->n_depend].access.data;
#else
   return 0;
#endif
}

static
komp_access_t* komp_task_format_get_access_param(
  const struct komp_format_t* fmt, unsigned int i, const void* sp
)
{
  komp_taskdata_t *arg = (komp_taskdata_t *)sp;
  if (i < arg->n_depend)
    return &arg->depends[i].access;
#if KOMP_USE_ICV
  else
    return &arg->extra_depends[i-arg->n_depend].access;
#else
   return 0;
#endif
}

static
void komp_task_format_set_access_param(
  const struct komp_format_t* fmt, unsigned int i, void* sp, const komp_access_t* a
)
{
  komp_taskdata_t *arg = (komp_taskdata_t *)sp;
  if (i < arg->n_depend)
    arg->depends[i].access = *a;
#if KOMP_USE_ICV
  else
    arg->extra_depends[i-arg->n_depend].access = *a;
#endif
}

static
const struct komp_format_t* komp_task_format_get_fmt_param(
  const struct komp_format_t* fmt, unsigned int i, const void* sp
)
{
  return komp_char_format;
}

static
void komp_task_format_get_view_param(
  const struct komp_format_t* fmt, unsigned int i, const void* sp,
  komp_memory_view_t* view
)
{
  /*komp_taskdata_t *arg = (komp_taskdata_t *)sp;*/
  /*kaapi_memory_view_make1d( view, 0, arg->depends[i].dep_len, sizeof(char) );*/
  kaapi_memory_view_make1d( (kaapi_memory_view_t*)view, 0, 1, sizeof(char) );
}

static
void komp_task_format_set_view_param(
  const struct komp_format_t* fmt, unsigned int i, void* sp, const komp_memory_view_t* view
)
{
  abort();
}

static
void komp_task_format_reducor(
  const struct komp_format_t* fmt, unsigned int i, void* sp, const void* v
)
{
  abort();
}

static
void komp_task_format_redinit(
  const struct komp_format_t* fmt, unsigned int i, const void* sp, void* v
)
{
  abort();
}

static komp_adaptivetask_splitter_t	komp_task_fnc_get_splitter(
  const struct komp_format_t* fmt, const void* sp
)
{
  return 0;
}

static int komp_task_fnc_get_affinity(
  const struct komp_format_t* fmt, const void* sp, struct komp_task_t* t, uint16_t flag
)
{
  kaapi_task_t* task = (kaapi_task_t*)t;
  komp_taskdata_t *arg = (komp_taskdata_t *)sp;
  if ((flag & KAAPI_TASK_FLAG_MASK_AFFINITY) == KAAPI_TASK_FLAG_AFF_NODE)
    return arg->node_id;
  if ((flag & KAAPI_TASK_FLAG_MASK_AFFINITY) == KAAPI_TASK_FLAG_AFF_CORE)
    return arg->core_id;
  if ((flag & KAAPI_TASK_FLAG_MASK_AFFINITY) == KAAPI_TASK_FLAG_AFF_ADDR)
    return komp_get_locality_domain_num_for(arg->affinity);
  /* if OCR, ldid is the index of access parameter to look to push task */
  if ((flag & KAAPI_TASK_FLAG_MASK_AFFINITY) == KAAPI_TASK_FLAG_AFF_OCR)
  {
    if (fmt == 0)
      return -1;
    int ldid = task->u.s.site -1;

    unsigned int count_params = kaapi_format_get_count_params( (kaapi_format_t*)fmt, sp );
    if (ldid >= count_params)
      return ldid;

    kaapi_access_mode_t mode            = kaapi_format_get_mode_param((kaapi_format_t*)fmt, ldid, sp);
    if (mode == KAAPI_ACCESS_MODE_V)
      return -1;

    kaapi_access_t* access = kaapi_format_get_access_param((kaapi_format_t*)fmt, ldid, sp);
    return kaapi_numa_getpage_id(access->data);
  }

  return 0;
}

#if !defined(KAAPI_USE_PERFCOUNTER)
/* One format for all tasks
*/
static struct komp_format_t* komp_task_format = 0;
#endif

/* Manage ICS and Dispatch call to task to the correct wrapper depending if gcc or iomp
*/
static void komp_trampoline_taskcall( struct komp_task_t* task, struct komp_thread_t* thread )
{
  komp_taskdata_t* taskdata = ((kaapi_task_t*)task)->arg;
  kaapi_context_t* context  = kaapi_thread2context(thread);
  komp_task_t* running = context->task;

#if KOMP_USE_ICV
  komp_dataenv_icv_t* save_icvs = context->komp_icvs;
  context->komp_icvs = &taskdata->icvs;
#endif
  context->task = (kaapi_task_t*)task;
  taskdata->wrapper( task, thread );
  context->task = running;

#if KOMP_USE_ICV
  context->komp_icvs = save_icvs;
#endif
}


/* kaapi_task_push common to libGOMP and libOMP
*/
int komp_task_push(
  komp_thread_t* th,
  komp_task_body_t body,
  void (*user_routine) (),
  komp_taskdata_t* arg,
  int ifclause, int finalclause
)
{
  kaapi_thread_t* thread = (kaapi_thread_t*)th;
  kaapi_processor_t* kproc = kaapi_thread2kproc(thread);

  kaapi_task_t* currtask = (kaapi_task_t*)komp_current_task(th);
  if (currtask !=0)
  {
    komp_taskdata_t* currtask_data = (komp_taskdata_t*)(currtask->arg);
    arg->task_group = currtask_data->task_group;
    if ((currtask_data->task_group !=0) && (currtask_data->task_group->cancelled !=0)) return 0;
  }
  else
    arg->task_group = 0;

  kaapi_team_t* team = kproc->team;

  kaapi_task_flag_t kaapi_flag = KAAPI_TASK_FLAG_DEFAULT;

#if KOMP_USE_ICV
  komp_dataenv_icv_t* icvs = komp_get_dataenv_icv( kproc );
  komp_newdataenv_icvs( kproc, icvs, &arg->icvs );
  size_t extra_n_depend = 0;
  komp_extradep_icv_t* icvs_ed = icvs->extra_deps;
  arg->extra_depends  = 0;
  arg->extra_n_depend = 0;

  if (icvs_ed !=0)
  {
    extra_n_depend += icvs_ed->sizeext_mode_r + icvs_ed->sizeext_mode_rw + icvs_ed->sizeext_mode_w;

    /* extra dependencies passed through icvs */
    if (extra_n_depend !=0)
    {
      arg->extra_depends = (komp_depend_info_t*)kaapi_data_push((kaapi_thread_t*)thread,
          sizeof(komp_depend_info_t) * extra_n_depend
      );
      kaapi_assert( arg->extra_depends !=0);
      komp_depend_info_t* deps = arg->extra_depends;
      arg->extra_n_depend = (int)extra_n_depend;
      size_t i;
      /* extre deps _R */
      for (i=0; i<icvs_ed->sizeext_mode_r; ++i, ++deps)
      {
        deps->access.data    = icvs_ed->ext_mode_r[i];
        deps->access.version = 0;
        deps->mode           = KOMP_ACCESS_MODE_R;
  #if defined(KAAPI_DEBUG)
        deps->dep_len        = 0;
        deps->access.version = 0;
        deps->access.task    = 0;
        deps->access.next    = deps->access.next_out = 0;
  #endif
      }

      /* extre deps _RW */
      for (i=0; i<icvs_ed->sizeext_mode_rw; ++i, ++deps)
      {
        deps->access.data    = icvs_ed->ext_mode_rw[i];
        deps->access.version = 0;
        deps->mode           = KOMP_ACCESS_MODE_RW;
  #if defined(KAAPI_DEBUG)
        deps->dep_len        = 0;
        deps->access.version = 0;
        deps->access.task    = 0;
        deps->access.next    = deps->access.next_out = 0;
  #endif
      }

      /* extre deps _W */
      for (i=0; i<icvs_ed->sizeext_mode_w; ++i,++deps)
      {
        deps->access.data    = icvs_ed->ext_mode_w[i];
        deps->access.version = 0;
        deps->mode           = KOMP_ACCESS_MODE_W;
  #if defined(KAAPI_DEBUG)
        deps->dep_len        = 0;
        deps->access.version = 0;
        deps->access.task    = 0;
        deps->access.next    = deps->access.next_out = 0;
  #endif
      }

      /* reset icv about dependencies */
      icvs_ed->sizeext_mode_r = icvs_ed->sizeext_mode_rw = icvs_ed->sizeext_mode_w = 0;
      icvs_ed->ext_mode_r = icvs_ed->ext_mode_rw = icvs_ed->ext_mode_w = 0;
    }
  }
#endif  // icvs


#if defined(KAAPI_USE_PERFCOUNTER) // register format for each different instances
  const kaapi_format_t* fmt;
  fmt = kaapi_format_resolve_bykey( (void*)user_routine );
  if (fmt ==0)
  {
    kaapi_atomic_lock(&lock_format);
    fmt = kaapi_format_resolve_bykey( (void*)user_routine );
    if (fmt == 0)
    {
      const char* name =0;

#if KOMP_USE_ICV
      if (icvs->task_name !=0)
      {
        name = strdup(icvs->task_name);
        icvs->task_name = 0;
      }
#endif
      if (name ==0)
      {
        char* buffer = malloc(128);
        int err = komp_get_taskname( user_routine, buffer, 128);
        if (err ==0) name = buffer;
        else name = "komptask";
      }

      struct komp_format_t* newfmt = (struct komp_format_t*)kaapi_format_allocate();
      komp_format_taskregister_func(
                                     newfmt,
                                     (void*)user_routine, /* key */
                                     body, /* body CPU */
                                     0,    /* body GPU */
                                     name,
                                     0,
                                     komp_task_format_get_size,
                                     komp_task_format_task_copy,
                                     komp_task_format_get_count_params,
                                     komp_task_format_get_mode_param,
                                     komp_task_format_get_data_param,
                                     komp_task_format_get_access_param,
                                     komp_task_format_set_access_param,
                                     komp_task_format_get_fmt_param,
                                     komp_task_format_get_view_param,
                                     komp_task_format_set_view_param,
                                     komp_task_format_reducor,
                                     komp_task_format_redinit,
                                     komp_task_fnc_get_splitter,
                                     komp_task_fnc_get_affinity
      );
      fmt = (kaapi_format_t*)newfmt;
    }
    kaapi_atomic_unlock(&lock_format);
  }
#endif


#if LOG
{
  int i;
  printf("[DFG] task:%p name:%s, #deps: %i\n", (void*)thread->sp, fmt->name, arg->n_depend);
  for (i=0; i<arg->n_depend; ++i)
  {
    printf("\t data: %p, access: %p, mode: %c\n", (void*)arg->depends[i].access.data, (void*)&arg->depends[i], kaapi_getmodename(arg->depends[i].mode));
  }
}
#endif

  /* add indirection to manage ICVS - may be avoid id ICVS not used !
     Also used to avoid lookup of format if no perfcounter
  */
  arg->wrapper      = body;
  body              = komp_trampoline_taskcall;

#if LOG
{
  int i;
  printf("[DFG] task:%p name:%s, #deps: %i\n", (void*)thread->sp, fmt->name, arg->n_depend);
  for (i=0; i<arg->n_depend; ++i)
  {
    printf("\t data: %p, access: %p, mode: %c\n", (void*)arg->depends[i].access.data, (void*)&arg->depends[i], kaapi_getmodename(arg->depends[i].mode));
  }
}
#endif

  komp_task_t* task = (komp_task_t*)thread->sp;
  if (!_kaapi_has_enough_taskspace(thread))
    task = kaapi_thread_slow_pushtask(thread);
  task->arg         = arg;
  task->body        = (kaapi_task_body_t)body;
  task->state.steal = (kaapi_task_body_t)body;
  KAAPI_ATOMIC_WRITE( &task->wc, 0);
  task->u.dummy     = 0;
#if defined(KAAPI_USE_PERFCOUNTER) // use registered format
  task->fmt         = fmt;
#else
  task->fmt         = (const kaapi_format_t*)komp_task_format;
#endif
#if !defined(NDEBUG) && defined(KAAPI_LINKED_LIST)
  task->next        = 0;
  task->prev        = 0;
#endif

  if ( !ifclause
    || (team ==0) /* orphan */
    || ((currtask !=0) && (currtask->u.s.flag & KAAPI_TASK_FLAG_IN_FINAL))
  )
  {
    /* required if dependencies exists with created task and the previous tasks */
    kaapi_sched_sync(thread);
    body((struct komp_task_t*)task, (struct komp_thread_t*)thread);
#if KOMP_USE_ICV
    kproc->context->komp_icvs = icvs;
#endif
    return 0;
  }

  if (arg->n_depend
#if KOMP_USE_ICV
    + arg->extra_n_depend 
#endif
   == 0)
    kaapi_flag = KAAPI_TASK_FLAG_INDPENDENT;

#if KOMP_USE_ICV
  if (icvs->prio)
  {
    task->u.s.priority = icvs->prio;
    kaapi_flag |= KAAPI_TASK_FLAG_PRIORITY;
    icvs->prio = 0; /* reset for next task */
//printf("%s:: Task:%p prio:%i\n", task->fmt->name, task, (int)task->u.s.priority );
  }
  if (icvs->flag)
  {
    if ((icvs->flag & KAAPI_TASK_FLAG_MASK_AFFINITY) == KAAPI_TASK_FLAG_AFF_ADDR) {
      arg->affinity = icvs->affinity;
      kaapi_flag |= KAAPI_TASK_FLAG_AFF_ADDR;
    } else if ((icvs->flag & KAAPI_TASK_FLAG_MASK_AFFINITY) == KAAPI_TASK_FLAG_AFF_NODE) {
      arg->node_id = icvs->node_id;
      kaapi_flag |= KAAPI_TASK_FLAG_AFF_NODE;
    } else if ((icvs->flag & KAAPI_TASK_FLAG_MASK_AFFINITY) == KAAPI_TASK_FLAG_AFF_CORE) {
      arg->core_id = icvs->core_id;
      kaapi_flag |= KAAPI_TASK_FLAG_AFF_CORE;
    } else if ((icvs->flag & KAAPI_TASK_FLAG_MASK_AFFINITY) == KAAPI_TASK_FLAG_AFF_OCR) {
      task->u.s.site = 1+(uint8_t)icvs->param_id;
      kaapi_flag |= KAAPI_TASK_FLAG_AFF_OCR;
    }
    if (icvs->flag & KAAPI_TASK_FLAG_LD_BOUND) 
      kaapi_flag |= KAAPI_TASK_FLAG_LD_BOUND;

    /* reset for next task */
    icvs->flag = 0;
    icvs->affinity = 0;
  }
#endif
  if (finalclause)
    kaapi_flag |= KAAPI_TASK_FLAG_IN_FINAL;
  task->u.s.flag = kaapi_flag;
  kaapi_task_commit(thread);
  return 0;
}


/*
*/
void komp_taskgroup_begin(void)
{
  kaapi_thread_t* self_thread = kaapi_self_thread();
  kaapi_task_t* currtask = (kaapi_task_t*)komp_current_task((komp_thread_t*)self_thread);
  komp_taskdata_t* currtask_data = (komp_taskdata_t*)(currtask->arg);

  komp_taskgroup_t* task_group
    = (komp_taskgroup_t*)kaapi_data_push( self_thread, sizeof(komp_taskgroup_t)
  );
  task_group->cancelled = 0;
  task_group->prev = currtask_data->task_group;
  currtask_data->task_group = task_group;
}


/*
*/
void komp_taskgroup_end (void)
{
  kaapi_thread_t* self_thread = kaapi_self_thread();
  kaapi_sched_sync( self_thread );

  kaapi_task_t* currtask = (kaapi_task_t*)komp_current_task((komp_thread_t*)self_thread);
  komp_taskdata_t* currtask_data = (komp_taskdata_t*)(currtask->arg);

  komp_taskgroup_t* tg = currtask_data->task_group;
  currtask_data->task_group = tg->prev;
}


/*
*/
void komp_atomic_initlock( struct komp_mutex_t* l)
{
  l->ptr = malloc( sizeof(kaapi_lock_t) );
  kaapi_atomic_initlock(l->ptr);
}

/*
*/
void komp_atomic_destroylock( struct komp_mutex_t* l)
{
  kaapi_atomic_destroylock(l->ptr);
  free(l->ptr);
}

void komp_atomic_lock( struct komp_mutex_t* l )
{
    kaapi_atomic_lock(l->ptr);
}

void komp_atomic_unlock( struct komp_mutex_t* l )
{
    kaapi_atomic_unlock(l->ptr);
}

/*
*/
void komp_barrier( struct komp_team_t* team )
{
  if (team ==0) return;
  kaapi_team_barrier_wait((kaapi_team_t*)team, kaapi_self_processor(), KAAPI_BARRIER_FLAG_DEFAULT);
}

/*
*/
void komp_sched_sync( struct komp_thread_t* thread)
{
  if (thread ==0) return;
  kaapi_sched_sync((kaapi_thread_t*)thread);
}

/*
*/
int komp_lock ( struct komp_team_t* team )
{ return kaapi_team_lock((kaapi_team_t*)team); }

/*
*/
int komp_unlock ( struct komp_team_t* team )
{ return kaapi_team_unlock((kaapi_team_t*)team); }


/*
*/
int komp_master ( struct komp_team_t* team )
{
  if (team ==0) return 1;
  return kaapi_team_master((kaapi_team_t*)team,0);
}

/*
*/
__thread uint64_t count_parallel_id = 0;

/* Execute a parallel region with body the body of the region
   - similar interface than in Intel IOMP
*/
typedef struct trampoline_forkcall_arg {
  void                          (*user_body)(void*);
  void*                           user_arg;
  komp_dataenv_icv_t*             parent_icvs;
  struct trampoline_forkcall_arg* next;
} trampoline_forkcall_arg_t;

/*
*/
static void trampoline_forkcall_wrapper(void* a)
{
#if (KOMP_USE_ICV|| defined(KAAPI_USE_PERFCOUNTER))
  kaapi_processor_t* kproc = kaapi_self_processor();
#if defined(KAAPI_USE_PERFCOUNTER)
  ++count_parallel_id;
  KAAPI_EVENT_PUSH1( kproc->rsrc.evtkproc, 0, KAAPI_EVT_PARALLEL_BEG, count_parallel_id);
#endif
#endif

  trampoline_forkcall_arg_t* arg = (trampoline_forkcall_arg_t*)a;
#if KOMP_USE_ICV
  komp_taskdata_t omp_defaultarg; /* implicit omp task */
  omp_defaultarg.extra_n_depend = 0;
  omp_defaultarg.depends  = 0;
  omp_defaultarg.n_depend = 0;
  omp_defaultarg.task_group = 0;

  kaapi_context_t* context = kproc->context;

  komp_newdataenv_icvs( kproc, arg->parent_icvs, &omp_defaultarg.icvs );
  komp_update_implicit_icvs(kproc->team->levels, &omp_defaultarg.icvs);
  context->komp_icvs = &omp_defaultarg.icvs;
#endif

  arg->user_body(arg->user_arg);

#if KOMP_USE_ICV
  context->komp_icvs = 0;
#endif
}


int komp_get_thread_parallel_region(
    int num_threads,
    int ifclause,
    komp_dataenv_icv_t* icv
)
{
  /* OpenMP spec algorithm 2.1 to compute the number of threads */
  int threadBusy   = kaapi_get_running_threads();
  int activeRegion = komp_get_active_level();
  int threadReq = (num_threads !=0) ? num_threads : icv->h_nthreads;
  int threadAvail = komp_global_dataenv_icv.thread_limit - threadBusy +1;
  
  if (!ifclause)
    num_threads = 1;
  else if ((activeRegion >=1) && !icv->nest)
    num_threads = 1;
  else if (activeRegion == komp_get_max_active_levels())
    num_threads = 1;
  else if (icv->dyn && (threadReq <= threadAvail))
    num_threads = threadReq; /* fix upper bound BUT can be between 1 and threadReq */
  else if (icv->dyn && (threadReq > threadAvail))
    num_threads = threadAvail; /* fix upper bound BUT can be between 1 and threadAvail */
  else if (!icv->dyn && (threadReq <= threadAvail))
    num_threads = threadReq;
  else if (!icv->dyn && (threadReq > threadAvail))
    /* implementation defined */
    num_threads = threadReq;
  
  else
    kaapi_abort(__LINE__,__FILE__, "Invalid case");
  return num_threads;
}


/*
*/
int komp_begin_parallel(
  unsigned int     num_threads,
  komp_proc_bind_t procbind,
  void (*body)(void*),
  void* data
)
{
#if KOMP_USE_ICV || defined(KAAPI_USE_PERFCOUNTER)
  kaapi_processor_t* kproc = komp_self_processor();
#endif
#if KOMP_USE_ICV
  komp_dataenv_icv_t* icvs = komp_get_dataenv_icv( kproc );
  num_threads = komp_get_thread_parallel_region( num_threads, 1, icvs );
  if (procbind == komp_proc_bind_default)
    procbind = icvs->h_bind;
#endif
  /* what does means true here ? convert to default that has mapping to kaapi */
  if (procbind == komp_proc_bind_true)
    procbind = komp_proc_bind_default;

#if defined (KAAPI_USE_PERFCOUNTER)
  ++count_parallel_id;
  KAAPI_EVENT_PUSH1( kproc->rsrc.evtkproc, 0, KAAPI_EVT_PARALLEL_BEG, count_parallel_id );
#endif

  trampoline_forkcall_arg_t* arg = 0;
  if (body)
  {
    arg = (trampoline_forkcall_arg_t*)kaapi_thread_pushdata(kaapi_self_thread(), sizeof(trampoline_forkcall_arg_t));
    arg->user_body = body;
    arg->user_arg  = data;
#if KOMP_USE_ICV
    arg->parent_icvs = icvs;
#endif
  }

  kaapi_team_t* team = kaapi_begin_parallel(
      num_threads,
      (kaapi_procbind_t)procbind,
      body == 0 ? 0 : body,
      body == 0 ? 0 : trampoline_forkcall_wrapper,
      arg
  );

  /* start parallel team, except the current context */
  kaapi_start_parallel(team, 0);

  if (body)
    trampoline_forkcall_wrapper(arg);

  return 0;
}


/**
*/
int komp_end_parallel(void)
{
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_context_t* context = kproc->context;


  if (context->ws.count_for_end != context->ws.count_for)
    komp_loop_workfini( (komp_thread_t*)kaapi_context2thread(context) );
  //kaapi_assert_debug(context->ws.count_for_end == context->ws.count_for);

  kaapi_end_parallel(kproc->team);
#if defined (KAAPI_USE_PERFCOUNTER)
  KAAPI_EVENT_PUSH1( kproc->rsrc.evtkproc, 0, KAAPI_EVT_PARALLEL_END, count_parallel_id );
#endif
  return 0;
}


/** Keep it until begin/end debug.
    == begin_parallel(num_threads, procbind, body, data); end_parallel();
*/
void komp_fork_call(
  unsigned int num_threads,
  komp_proc_bind_t procbind,
  void (*body)(void*),
  void* data
)
{
  kaapi_processor_t* kproc = komp_self_processor();
#if KOMP_USE_ICV
  komp_dataenv_icv_t* icvs = komp_get_dataenv_icv( kproc );
  num_threads = komp_get_thread_parallel_region( num_threads, 1, icvs );
  if (procbind == komp_proc_bind_default)
    procbind = icvs->h_bind;
#else
  if (num_threads ==0) num_threads = kaapi_default_param.syscpucount;
#endif
  /* what does means true here ? convert to default that has mapping to kaapi */
  if (procbind == komp_proc_bind_true)
    procbind = komp_proc_bind_default;

#if defined (KAAPI_USE_PERFCOUNTER)
  ++count_parallel_id;
  KAAPI_EVENT_PUSH1( kproc->rsrc.evtkproc, 0, KAAPI_EVT_PARALLEL_BEG, count_parallel_id );
#endif

  trampoline_forkcall_arg_t* arg = kproc->context->freebloc_fj;
  if (arg ==0)
    arg = (trampoline_forkcall_arg_t*)kaapi_processor_alloc_data(
          kproc,
          sizeof(trampoline_forkcall_arg_t)
    );
  else
    kproc->context->freebloc_fj = arg->next;

  arg->user_body = body;
  arg->user_arg  = data;
#if KOMP_USE_ICV
  arg->parent_icvs = icvs;
#endif
  arg->next = 0;

  kaapi_team_t* team = kaapi_begin_parallel(
      num_threads,
      (kaapi_procbind_t)procbind,
      body,
      trampoline_forkcall_wrapper,
      arg
  );
  kproc = kaapi_self_processor();


  /* start parallel team, except the current context */
  kaapi_start_parallel(team, 1 );

  kaapi_context_t* context = kproc->context;
  if (context->ws.count_for_end != context->ws.count_for)
    komp_loop_workfini( (komp_thread_t*)kaapi_context2thread(context) );
  kaapi_assert_debug(context->ws.count_for_end == context->ws.count_for);

  kaapi_end_parallel(team);

  kproc = kaapi_self_processor();
  arg->next = kproc->context->freebloc_fj;
  kproc->context->freebloc_fj = arg;

#if defined (KAAPI_USE_PERFCOUNTER)
  KAAPI_EVENT_PUSH1( kproc->rsrc.evtkproc, 0, KAAPI_EVT_PARALLEL_END, count_parallel_id );
#endif
}


/*
*/
int komp_single (struct komp_team_t* team)
{
  if (team ==0) return 1;
  return kaapi_team_single((kaapi_team_t*)team,0);
}


/*
*/
void* komp_single_copy_start( struct komp_team_t* team )
{
  if ((team ==0) || kaapi_team_single((kaapi_team_t*)team, 0))
    return 0;

  kaapi_team_barrier_wait((kaapi_team_t*)team, kaapi_self_processor(), KAAPI_BARRIER_FLAG_DEFAULT);
  return ((kaapi_team_t*)team)->ws.single_data;
}


/*
*/
void komp_single_copy_end( struct komp_team_t* team, void* data)
{
  if (!team) return;
  ((kaapi_team_t*)team)->ws.single_data = data;
  kaapi_team_barrier_wait((kaapi_team_t*)team, kaapi_self_processor(), KAAPI_BARRIER_FLAG_NOSCHEDULE);
  /* unlink threads blocked in komp_single_copy_start */
}



/*
*/
void komp_mem_barrier(void)
{
  kaapi_mem_barrier();
}


/* kaapi_format_allocate
*/
struct komp_format_t* komp_format_allocate(void)
{
  return (struct komp_format_t*) kaapi_format_allocate();
}


/* kaapi_format_structregister
*/
int komp_format_structregister(
  struct komp_format_t*        fmt,
  const char*                  name,
  unsigned long                size,
  void                       (*cstor)( void* ),
  void                       (*dstor)( void* ),
  void                       (*cstorcopy)( void*, const void*),
  void                       (*copy)( void*, const void*),
  void                       (*assign)( void*, const komp_memory_view_t*, const void*, const komp_memory_view_t*)
)
{
  return kaapi_format_structregister( (struct kaapi_format_t*)fmt,
        name,
        size,
        cstor,
        dstor,
        cstorcopy,
        copy,
        (void (*)( void*, const kaapi_memory_view_t*, const void*, const kaapi_memory_view_t*)
) assign
  );
}

/* kaapi_format_taskregister_func
*/
int komp_format_taskregister_func(
  struct komp_format_t*         fmt,
  void*                         key,
  komp_task_body_t              body,
  komp_task_body_t              bodygpu,
  const char*                   name,
  komp_fmt_fnc_get_name         get_name,
  komp_fmt_fnc_get_size         get_size,
  komp_fmt_fnc_task_copy        task_copy,
  komp_fmt_fnc_get_count_params get_count_params,
  komp_fmt_fnc_get_mode_param   get_mode_param,
  komp_fmt_fnc_get_data_param   get_data_param,
  komp_fmt_fnc_get_access_param get_access_param,
  komp_fmt_fnc_set_access_param set_access_param,
  komp_fmt_fnc_get_fmt_param    get_fmt_param,
  komp_fmt_fnc_get_view_param   get_view_param,
  komp_fmt_fnc_set_view_param   set_view_param,
  komp_fmt_fnc_reducor          reducor,
  komp_fmt_fnc_redinit          redinit,
  komp_fmt_fnc_get_splitter	    get_splitter,
  komp_fmt_fnc_get_affinity	    get_affinity
)
{
  kaapi_format_id_t fmtid =  kaapi_format_taskregister_func(
    (struct kaapi_format_t*)         fmt,
    (void*)                          key,
    (kaapi_task_body_t)              body,
                                     name,
    (kaapi_fmt_fnc_get_name)         get_name,
    (kaapi_fmt_fnc_get_size)         get_size,
    (kaapi_fmt_fnc_task_copy)        task_copy,
    (kaapi_fmt_fnc_get_count_params) get_count_params,
    (kaapi_fmt_fnc_get_mode_param)   get_mode_param,
    (kaapi_fmt_fnc_get_data_param)   get_data_param,
    (kaapi_fmt_fnc_get_access_param) get_access_param,
    (kaapi_fmt_fnc_set_access_param) set_access_param,
    (kaapi_fmt_fnc_get_fmt_param)    get_fmt_param,
    (kaapi_fmt_fnc_get_view_param)   get_view_param,
    (kaapi_fmt_fnc_set_view_param)   set_view_param,
    (kaapi_fmt_fnc_reducor)          reducor,
    (kaapi_fmt_fnc_redinit)          redinit,
    (kaapi_fmt_fnc_get_splitter)     get_splitter,
    (kaapi_fmt_fnc_get_affinity)     get_affinity
  );
  if (!fmtid) return EINVAL;
  if (bodygpu)
    if (0 == kaapi_format_taskregister_body( (struct kaapi_format_t*)fmt, (kaapi_task_body_t)bodygpu, KAAPI_PROC_TYPE_GPU))
      return EINVAL;; 
  return 0;
}

int komp_format_taskregister_body(
    struct komp_format_t*         fmt,
    komp_task_body_t              body,
    int                           arch
)
{
  if ((arch !=KOMP_PROC_TYPE_CPU) &&
      (arch != KOMP_PROC_TYPE_CUDA) &&
      (arch != KOMP_PROC_TYPE_MIC))
    return EINVAL;
  kaapi_format_taskregister_body(
    (struct kaapi_format_t*)fmt, (kaapi_task_body_t)body, arch);
  return 0;
}

/*
*/
int komp_foreach_attr_set_policy(
  komp_foreach_attr_t* attr, 
  komp_foreach_attr_policy_t policy
)
{
  return
    kaapi_foreach_attr_set_policy((kaapi_foreach_attr_t*)attr, (kaapi_foreach_attr_policy_t)policy);
}

/*
*/
int komp_foreach_attr_get_policy(
  const komp_foreach_attr_t* attr,
  komp_foreach_attr_policy_t* policy
)
{
  return
    kaapi_foreach_attr_get_policy((kaapi_foreach_attr_t*)attr, (kaapi_foreach_attr_policy_t*)policy);
}


/*
*/
int komp_foreach_attr_set_grains(
  komp_foreach_attr_t* attr, 
  unsigned long long s_grain,
  unsigned long long p_grain
)
{
  return
    kaapi_foreach_attr_set_grains((kaapi_foreach_attr_t*)attr, s_grain, p_grain);
}


/*
*/
int komp_foreach_attr_get_grains(
  const komp_foreach_attr_t* attr,
  unsigned long long* s_grain,
  unsigned long long* p_grain
)
{
  return
    kaapi_foreach_attr_get_grains((kaapi_foreach_attr_t*)attr, s_grain, p_grain);
}


/*
*/
int komp_foreach_attr_set_localitydomain(
  komp_foreach_attr_t* attr, 
  komp_hws_levelid_t  level,
  int                  strictness
)
{
  return
    kaapi_foreach_attr_set_localitydomain((kaapi_foreach_attr_t*)attr, (kaapi_hws_levelid_t)level, strictness);
}


/*
*/
int komp_foreach_attr_get_localitydomain(
  const komp_foreach_attr_t* attr,
  komp_hws_levelid_t*       level,
  int*                       strictness
)
{
  return
    kaapi_foreach_attr_get_localitydomain((kaapi_foreach_attr_t*)attr, (kaapi_hws_levelid_t*)level, strictness);
}


/*
*/
int komp_foreach_attr_set_distribution(
  komp_foreach_attr_t* attr, 
  komp_hws_levelid_t   level,
  int                  strict
)
{
  return kaapi_foreach_attr_set_distribution(
        (kaapi_foreach_attr_t*)attr,
        (kaapi_hws_levelid_t)level,
        strict );
}

/*
*/
int komp_foreach_attr_get_distribution(
  const komp_foreach_attr_t* attr,
  komp_hws_levelid_t*        level,
  int*                       strict
)
{
  return kaapi_foreach_attr_get_distribution(
        (kaapi_foreach_attr_t*)attr,
        (kaapi_hws_levelid_t*)level,
        strict );
}


/*
*/
int komp_foreach(
  unsigned long long         first,
  unsigned long long         last,
  unsigned long long         incr,
  int                        sync,
  const komp_foreach_attr_t* attr,
  komp_foreach_body_t body,
  void* arg
)
{
  if (last <= first)
    return 0;

  /* sync / or not */
  
  /* is_format true if called from kaapif_foreach_with_format */
  kaapi_processor_t* const kproc = kaapi_self_processor();
  if ((kproc ==0) || (kproc->team ==0))
  {
    body(first, last, arg);
    return 0;
  }
  komp_thread_t* const self_thread = (komp_thread_t*)kaapi_context2thread(kproc->context);

//  KAAPI_EVENT_PUSH0(kproc, 0, KAAPI_EVT_FOREACH_BEG );

  if (0== komp_loop_workinit_ull( self_thread, sync, attr, 1, first, last, 1, body, arg))
    return -1;

  while (komp_loop_worknext_ull(self_thread, &first, &last) != 0)
    body( first, last, arg);

  komp_loop_workfini( self_thread );
//  KAAPI_EVENT_PUSH0(kproc, 0, KAAPI_EVT_FOREACH_END );

  return 0;
}


/* loop init 
*/
int komp_loop_workinit(
  komp_thread_t* thread,
  int sync,
  const komp_foreach_attr_t* attr,
  long long start,
  long long end,
  long long incr,
  komp_foreach_body_t body,
  void* arg
)
{
  unsigned long long iter_size;
  kaapi_context_t* context = kaapi_thread2context(thread);

  if (incr >0)
    iter_size = (end - start + incr-1)/incr;
  else
    iter_size = (start - end - incr-1)/-incr;

  void* retval = kaapi_foreach_workinit( context, sync,
                    (const kaapi_foreach_attr_t*)attr,
                    iter_size,
                    body, arg
  );

  if (retval !=0) 
  {
    context->ws.for_data.first   = (unsigned long long)start;
    context->ws.for_data.last    = (unsigned long long)end;
    context->ws.for_data.incr    = (unsigned long long)incr;
    return 1;
  }
  return 0;
}

/*
*/
int komp_loop_workinit_ull(
  komp_thread_t*           thread,
  int                        sync,
  const komp_foreach_attr_t* attr,
  int                        up,
  unsigned long long         start,
  unsigned long long         end,
  unsigned long long         incr,
  komp_foreach_body_t        body,
  void*                      arg
)
{
  unsigned long long iter_size;
  kaapi_context_t* context = kaapi_thread2context(thread);

  kaapi_assert_debug( !((up && (start > end)) || (!up && (start < end))) );

  if (up)
    iter_size = (end - start + incr-1)/incr;
  else
    iter_size = (start - end - incr-1)/-incr;

  void* retval = kaapi_foreach_workinit( context, sync,
                    (const kaapi_foreach_attr_t*)attr,
                    iter_size,
                    body, arg
  );
  if (retval !=0) 
  {
    context->ws.for_data.first   = start;
    context->ws.for_data.last    = end;
    context->ws.for_data.incr    = incr;
    return 1;
  }
  return 0;
}


/* return !=0 value if success */
int komp_loop_worknext(
  komp_thread_t* thread,
  long long *istart,
  long long *iend
)
{
  unsigned long long ristart;
  unsigned long long riend;
  kaapi_context_t* context = kaapi_thread2context(thread);
  kaapi_foreach_work_t* lwork = context->ws.for_data.rt_ctxt;

  /* pop next range and start execution (on return...) */
  if ( (lwork !=0) &&
       (0  == kaapi_foreach_worknext(
          lwork,
          &ristart,
          &riend
          )
        )
      )
  {
    context->ws.for_data.curr_start = (context->ws.for_data.first + context->ws.for_data.incr * ristart);
    context->ws.for_data.curr_end   = (context->ws.for_data.first + context->ws.for_data.incr * riend);
    *istart = (long long)context->ws.for_data.curr_start;
    *iend   = (long long)context->ws.for_data.curr_end;
    return 1;
  }
  return 0;
}

/* return !=0 value if success */
int komp_loop_worknext_ull(
  komp_thread_t* thread,
  unsigned long long *istart,
  unsigned long long *iend
)
{
  kaapi_context_t* context = kaapi_thread2context(thread);
  kaapi_foreach_work_t* lwork = context->ws.for_data.rt_ctxt;

  /* pop next range and start execution (on return...) */
  if ( (lwork !=0) &&
       (0  == kaapi_foreach_worknext( lwork, istart, iend) )
     )
  {
    context->ws.for_data.curr_start = (context->ws.for_data.first + context->ws.for_data.incr * *istart);
    context->ws.for_data.curr_end   = (context->ws.for_data.first + context->ws.for_data.incr * *iend);
    *istart = context->ws.for_data.curr_start;
    *iend   = context->ws.for_data.curr_end;
    return 1;
  }
  return 0;
}


/* return !=0 value if success */
int komp_loop_workfini(
  komp_thread_t* thread
)
{
  /* pop next range and start execution (on return...) */
  kaapi_context_t* context = kaapi_thread2context(thread);
  kaapi_foreach_work_t* lwork = context->ws.for_data.rt_ctxt;
  if ( (lwork !=0) &&
       (0  == kaapi_foreach_workfini( lwork )) )
  {
    return 1;
  }
  return 0;
}


/*
*/
int komp_loop_policy_start (
  komp_thread_t* thread,
  const komp_foreach_attr_t* attr,
  long long start,
  long long end,
  long long incr,
  long long *istart,
  long long *iend
)
{
  if (0 == komp_loop_workinit( thread, 1, attr, start, end, incr, 0, 0))
    return 0;

  if ((istart !=0) && (iend !=0))
    return komp_loop_worknext( thread, istart, iend );
  return 1;
}


/*
*/
int komp_loop_policy_start_ull (
  komp_thread_t* thread,
  const komp_foreach_attr_t* attr,
  int up,
  unsigned long long start,
  unsigned long long end,
  unsigned long long incr,
  unsigned long long *istart,
  unsigned long long *iend
)
{
  if (0 == komp_loop_workinit_ull( thread, 1, attr, up, start, end, incr, 0, 0))
    return 0;

  if ((istart !=0) && (iend !=0))
    return komp_loop_worknext_ull( thread, istart, iend );
  return 1;
}


/*
*/
void komp_ordered_start (void)
{
  kaapi_processor_t* kproc = kaapi_self_processor();

  kaapi_context_t* thread = kproc->context;
  kaapi_team_t* team = kproc->team;

  if (team ==0)
    return;

  kaapi_workqueue_index_t ordered_it;
  kaapi_workqueue_index_t curr_ordered_index = KAAPI_ATOMIC_READ(&team->ws.ordered_index);
  
  /* my iteration bounds */
  kaapi_workqueue_index_t it_first = thread->ws.for_data.curr_start;
  kaapi_workqueue_index_t it_last  = thread->ws.for_data.curr_end;

do_test:
  ordered_it = thread->ws.for_data.first + thread->ws.for_data.incr * curr_ordered_index;

  if (thread->ws.for_data.incr >0)
  {
    if ((ordered_it >= it_first) && (ordered_it < it_last))
      goto return_value;
  }
  else
  {
    if ((ordered_it <= it_first) && (ordered_it > it_last))
      goto return_value;
  }

  /* wait until current_ordered_index change */
  while (curr_ordered_index == KAAPI_ATOMIC_READ(&team->ws.ordered_index))
    kaapi_slowdown_cpu();

  /* read it again */
  curr_ordered_index = KAAPI_ATOMIC_READ(&team->ws.ordered_index);
  goto do_test;
  
return_value:
  //printf("Out: Ordered: %llu\n", curr_ordered_index);
  return;
}


/*
*/
void komp_ordered_end (void)
{
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_team_t* team = kproc->team;
  if (kproc ==0)
    return;

  /* assume atomic */
  kaapi_assert_debug(
      __kaapi_isaligned( &team->ws.ordered_index, sizeof(team->ws.ordered_index)
      )
  );
  //printf("Out: Ordered_end: %llu\n", KAAPI_ATOMIC_READ(&team->ws.ordered_index));
  kaapi_writemem_barrier();
  KAAPI_ATOMIC_INCR(&team->ws.ordered_index);
}


__attribute__((constructor))
static void komp_runtime_abi_constructor(void)
{
    komp_init();
}

__attribute__((destructor))
static void komp_runtime_abi_destructor(void)
{
    komp_finalize();
}


/* guard against multiple initialization */
static kaapi_atomic_t komp_isinit = { 0 };

/*
*/
void komp_init (void)
{
  if (KAAPI_ATOMIC_INCR(&komp_isinit) !=1) 
    return;

  int err;
  int line;
  char* msg;
  /* check if public komp data structure differs */

  /* Parse OMP_DISPLAY_ENV and display informations. Before init to enable some output */
  komp_parse_display_env ();

  /* Parse KOMP_STEAL_PROTOCOL env variables */
  komp_parse_stealprotocol("KOMP_STEAL_PROTOCOL", &kaapi_default_param);

  /* */
  err = kaapi_mt_hwdetect();
  if ((err !=0) && (err != EALREADY))
  {
    line = __LINE__ -3;
    msg = "kaapi_mt_hwdetect";
    goto error_label;
  }

  /* KOMP_CPU_AFFINITY has higher priority with GOMP_CPU_AFFINITY */
  if (0 != getenv("KOMP_CPU_AFFINITY"))
    kaapi_default_param.cpuset_str = getenv("KOMP_CPU_AFFINITY");

  /* OMP_NUM_THREADS. */
  komp_parse_list_unsigned_int("OMP_NUM_THREADS", &komp_global_dataenv_icv.nthreads);
  kaapi_default_param.cpucount_str = getenv("OMP_NUM_THREADS");

  l_alloc1 = komp_global_dataenv_icv.nthreads;

  /* Turn GOMP_STACKSIZE and OMP_STACKSIZE to control the pthread stack size */
  if (komp_parse_stacksize ("OMP_STACKSIZE", &komp_global_device_icv.stacksize)
      || komp_parse_stacksize ("GOMP_STACKSIZE", &komp_global_device_icv.stacksize))
  {
    kaapi_default_param.threadstacksize = komp_global_device_icv.stacksize;
  }
  else {
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_getstacksize( &attr, &komp_global_device_icv.stacksize );
    kaapi_default_param.threadstacksize = komp_global_device_icv.stacksize;
  }

  /* Parse OMP_MAX_ACTIVE_LEVELS. */
  komp_parse_int ("OMP_MAX_ACTIVE_LEVELS", &komp_global_device_icv.max_active_levels);

  /* Parse OMP_NESTED. */
  int8_t bvalue;
  komp_parse_bool("OMP_NESTED", &bvalue );
  komp_global_dataenv_icv.icvs.nest = bvalue ? 1 : 0;

  /* Parse OMP_DYNAMIC. */
  komp_parse_bool("OMP_DYNAMIC", &bvalue );
  komp_global_dataenv_icv.icvs.dyn = bvalue ? 1 : 0;

  /* Parse OMP_DYNAMIC. */
  komp_parse_bool("OMP_CANCELLATION", &bvalue);
  komp_global_device_icv.cancel = bvalue ? 1 : 0;

  /* Parse OMP_PROC_BIND */
  komp_parse_list_procbind("OMP_PROC_BIND", &komp_global_dataenv_icv.binds);
  if (komp_global_dataenv_icv.binds ==0)
    komp_global_dataenv_icv.icvs.h_bind = komp_proc_bind_false;
  else
  {
    komp_global_dataenv_icv.icvs.h_bind = *komp_global_dataenv_icv.binds;
    ++komp_global_dataenv_icv.binds;
    if (*komp_global_dataenv_icv.binds == komp_proc_bind_end_list)
      komp_global_dataenv_icv.binds = 0;
  }

  /* FIXEME : currently KAAPI should be used with OMP_PLACES defined ... 
  */
  if (0 == getenv("OMP_PLACES"))
  {
#if defined(__linux__)
    cpu_set_t syscpuset;
    CPU_ZERO(&syscpuset);
    sched_getaffinity(0, sizeof(syscpuset), &syscpuset);
    /*FIXME : dirty quickfix to take default affinity into account*/
    if (CPU_COUNT(&syscpuset) > 0)
    {
      char places[CPU_SETSIZE*2+2];
      char *curr = places;
      int i = 0;
      *curr++ = '{';
      int nparsed = 0;
      while (nparsed < CPU_COUNT(&syscpuset))
      {
        if (CPU_ISSET(i, &syscpuset))
        {
          if (nparsed++ > 0)
            *curr++ = ',';
          sprintf(curr, "%d", i);
          while(*++curr)
            ;
        }
        i++;
      }
      *curr++ = '}';
      *curr++ = '\0';
      setenv("OMP_PLACES",places, 1);
    } 
    else
#endif
    {
      int nthreads = ( komp_global_dataenv_icv.nthreads == 0 ?
          kaapi_default_param.syscpucount
        : *komp_global_dataenv_icv.nthreads );
      kaapi_assert( nthreads >0 );
      char tmp[32];
      snprintf(tmp, 32, "threads(%i)", nthreads);
      setenv("OMP_PLACES",tmp, 1);
    }
  }

  /* OMP_PLACES has lower priority than KOMP_CPU_AFFINITY or GOMP_CPU_AFFINITY */
  if ((0 != getenv("OMP_PLACES")) && (kaapi_default_param.cpuset_str ==0))
  {
    kaapi_default_param.cpuset_str = getenv("OMP_PLACES");
    if (getenv("OMP_PROC_BIND")==0) komp_global_dataenv_icv.icvs.h_bind = komp_proc_bind_true;
  }

  bool activeon = true;
  komp_parse_waitpolicy("OMP_WAIT_POLICY", &activeon );
  if (activeon)
    komp_global_device_icv.waitpolicy = komp_wait_policy_active;
  else
    komp_global_device_icv.waitpolicy = komp_wait_policy_passive;
  
  /* OMP_THREAD_LIMIT: unsigned int */
  komp_parse_unsigned_int("OMP_THREAD_LIMIT", &komp_global_dataenv_icv.thread_limit);

  /* Parse OMP_SCHEDULE */
  komp_sched_t run_sched;
  komp_parse_schedule ("OMP_SCHEDULE",
    &run_sched,
    &komp_global_dataenv_icv.icvs.run_sched_modifier);
  komp_global_dataenv_icv.icvs.run_sched = (unsigned char)run_sched;

#if defined (KAAPI_USE_PERFCOUNTER)
  /* Parse KOMP env variables */
  int display_perfcounter_value;
  komp_parse_perfcounter("KOMP_DISPLAY_PERF", &display_perfcounter_value);
  kaapi_tracelib_param.display_perfcounter = display_perfcounter_value;
#endif

  /* init kaapi without parsing KAAPI_XXX environement variables */
  if (0 != (err = kaapi_mt_init(KAAPI_START_ONLY_MAIN)))
  {
    line = __LINE__ -2;
    msg = "kaapi_mt_init";
    goto error_label;
  }

  KOMP_INIT_FMT(schar)
  KOMP_INIT_FMT(char)
  KOMP_INIT_FMT(shrt)
  KOMP_INIT_FMT(int)
  KOMP_INIT_FMT(long)
  KOMP_INIT_FMT(llong)
  KOMP_INIT_FMT(int8)
  KOMP_INIT_FMT(int16)
  KOMP_INIT_FMT(int32)
  KOMP_INIT_FMT(int64)
  KOMP_INIT_FMT(uchar)
  KOMP_INIT_FMT(ushrt)
  KOMP_INIT_FMT(uint)
  KOMP_INIT_FMT(ulong)
  KOMP_INIT_FMT(ullong)
  KOMP_INIT_FMT(uint8)
  KOMP_INIT_FMT(uint16)
  KOMP_INIT_FMT(uint32)
  KOMP_INIT_FMT(uint64)
  KOMP_INIT_FMT(flt)
  KOMP_INIT_FMT(dbl)
  KOMP_INIT_FMT(ldbl)
  KOMP_INIT_FMT(voidp)

  if (komp_global_dataenv_icv.nthreads ==0)
    komp_global_dataenv_icv.icvs.h_nthreads = kaapi_default_param.cpucount;
  else
  {
    komp_global_dataenv_icv.icvs.h_nthreads = *komp_global_dataenv_icv.nthreads;
    ++komp_global_dataenv_icv.nthreads;
    if (komp_global_dataenv_icv.nthreads[0] ==0)
      komp_global_dataenv_icv.nthreads = 0;
  }

  if (kaapi_default_param.display_env)
    komp_display_env(kaapi_default_param.display_env);

#if !defined(KAAPI_USE_PERFCOUNTER)
  komp_task_format = (struct komp_format_t*)kaapi_format_allocate();
  komp_format_taskregister_func(
                                   komp_task_format,
                                   (void*)komp_trampoline_taskcall, /* key */
                                   komp_trampoline_taskcall, /* body CPU */
                                   0,    /* body GPU */
                                   "komp_task",
                                   0,
                                   komp_task_format_get_size,
                                   komp_task_format_task_copy,
                                   komp_task_format_get_count_params,
                                   komp_task_format_get_mode_param,
                                   komp_task_format_get_data_param,
                                   komp_task_format_get_access_param,
                                   komp_task_format_set_access_param,
                                   komp_task_format_get_fmt_param,
                                   komp_task_format_get_view_param,
                                   komp_task_format_set_view_param,
                                   komp_task_format_reducor,
                                   komp_task_format_redinit,
                                   komp_task_fnc_get_splitter,
                                   komp_task_fnc_get_affinity
    );
#endif

  return;

error_label:
  fprintf (stderr, "[libKOMP] call to Kaapi function '%s' at line %i return error, code= %d, name= %s.", msg, line, err, strerror(err));
  abort();
  return;
}


void komp_finalize (void)
{
  if (KAAPI_ATOMIC_DECR(&komp_isinit) !=0) 
    return;

  kaapi_mt_finalize ();

  if (l_alloc1 !=0)
  {
    free(l_alloc1);
    l_alloc1 = 0;
  }

#if KAAPI_KOMP_TRACE
  printf("KOMP: time start parallel: %f\n", 1e6*kaapi_komp_start_parallel/kaapi_komp_start_parallel_count); 
  printf("KOMP: time end parallel: %f\n", 1e6*kaapi_komp_end_parallel/kaapi_komp_end_parallel_count); 
#endif
}



/* get a task name */
#if defined(KAAPI_USE_PERFCOUNTER)
#if defined(__linux__) && defined(HAVE_BFD_LIB)
#include <stdio.h>
#include <stdlib.h>

#include <execinfo.h>
#include <signal.h>
#include <bfd.h>
#include <unistd.h>

/* globals retained across calls to resolve. */
static bfd* abfd = 0;
static asymbol **syms = 0;
static asection *text = 0;
static kaapi_lock_t bfd_lock = KAAPI_LOCK_INITIALIZER;

static int komp_get_taskname(void *address, char* buffer, size_t sizebuffer)
{
  if ((buffer ==0) || (sizebuffer ==0))
    return 0;

  kaapi_atomic_lock( &bfd_lock );
  if (!abfd)
  {
    char ename[1024];
    int l = readlink("/proc/self/exe",ename,sizeof(ename));
    if (l == -1) {
      perror("failed to find executable\n");
      kaapi_atomic_unlock( &bfd_lock );
      return ENOEXEC;
    }
    ename[l] = 0;

    bfd_init();

    abfd = bfd_openr(ename, 0);
    if (!abfd) {
      perror("bfd_openr failed: ");
      kaapi_atomic_unlock( &bfd_lock );
      return EPERM;
    }

    /* oddly, this is required for it to work... */
    bfd_check_format(abfd,bfd_object);

    unsigned storage_needed = bfd_get_symtab_upper_bound(abfd);
    syms = (asymbol **) malloc(storage_needed);
    if (syms ==0)
    {
      kaapi_atomic_unlock( &bfd_lock );
      return ENOMEM;
    }
    unsigned cSymbols = bfd_canonicalize_symtab(abfd, syms);

    text = bfd_get_section_by_name(abfd, ".text");
  }
  kaapi_atomic_unlock( &bfd_lock );

  long offset = ((long)address) - text->vma;
  if (offset > 0)
  {
    const char *file;
    const char *func;
    unsigned line;
    if (bfd_find_nearest_line(abfd, text, syms, offset, &file, &func, &line) && file)
    {
      snprintf(buffer, sizebuffer, "%s, file:%s line: %u\n",func,file,line);
      return 0;
    }
  }
  return EEXIST;
}

#else /* if defined(__linux__) */
#include <dlfcn.h>
static int komp_get_taskname(void *address, char* buffer, size_t sizebuffer)
{
  Dl_info info;
  if (dladdr(address, &info) ==0) return EEXIST;
  if (info.dli_sname !=0)
    strncpy( buffer, info.dli_sname, sizebuffer );
  return 0;
}
#endif
#endif // #if defined(KAAPI_USE_PERFCOUNTER)

