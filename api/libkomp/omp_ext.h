/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_OMP_EXT_H_
#define _KAAPI_OMP_EXT_H_

#include <stddef.h>

#if defined(__cplusplus)
extern "C" {
#endif


/** Set the affinity mask of the icv for the next created task.
    Strict parameter, if !=0 allows to enforce the execution on the given localitydomain.
    Returns 0 in case of success.
    Returns !=0 in case of failure and set errno to the error code.
    Possible error is:
    * EINVAL: bad value
 
*/
enum {
  OMP_AFFINITY_DEFAULT_LD   = 0xFF,
  OMP_AFFINITY_ANY_LD       = 0xFF
};
extern int omp_set_task_affinity(int numa_id, int strict );

extern void omp_begin_numa_init(void **select, void **push, void **push_init);
extern void omp_end_numa_init(void *select, void *push, void *push_init);

/** Set the priority for the next task spawn by the current thread
    One the task is spawned, the icv prio is reset to its default value.
    The minimum priority is 0.
    The maximal priority is !=0, but positive.
*/
extern int omp_set_task_priority(int prio);


/** Set flag for the next tasks spawn by the current thread
*/
enum {
  OMP_FLAG_DEFAULT      = 0,
  OMP_FLAG_NUMA_AWARE   = 1,
  OMP_FLAG_LD_BOUND     = 0x80,
  OMP_FLAG_OCR          = 0x40
};
extern int omp_set_task_flag(int flag);

/* Used to give the name of the next explicit generating task.
   Information re-used at runtime when tasks counters are dump or data flow graph is dumped.
*/
extern void omp_set_task_name( const char* name );

/* Locality Domain interface for OMP
*/

/** Return the number of locality domains
    This number can be changed using taskset.
*/
extern int omp_get_num_locality_domains(void);

/* Return the current locality domain identifier
   Or -1 if undefined.
*/
extern int omp_get_locality_domain_num(void);

/* Return the locality domain that stores address.
   Or -1 if undefined.
*/
extern int omp_get_locality_domain_num_for(void* addr);

/** Return the number of procs inside a locality domain.
 */
extern int omp_locality_domain_get_num_cpu(int ldid);

/* Bind data to a bloc cyclic distribution with bloc size B over the omp_get_num_locality_domains()
   locality domains assigned to the process.
   The blocsize, as well as the size, must be a multiple of a page size.
   The addresse 'addr' should be aligned to page boundary.

   Returns 0 in case of success
   Returns -1 in case of failure and set errno to the error code.
   Possible error is:
    * ENOENT: function not available on this configuration
    * EINVAL: invalid argument blocsize
    * EFAULT: invalid address addr
*/
extern int omp_locality_domain_bind_bloc1dcyclic(const void* addr, size_t size, size_t blocsize);

/** Bind data to a 2D bloc cyclic distribution with bloc size Bx x By over the omp_get_num_locality_domains()
    locality domains assigned to the process. The number of locality domains is assumed to be
    a square nldx x nldy.
    The blocsizes bsx and bst, as well as the size, must be a multiple of a page size.
    The addresse 'addr' should be aligned to page boundary.
    Moreover, bsy must divide n and bsy must divide m.

   Returns 0 in case of success
   Returns -1 in case of failure and set errno to the error code.
   Possible error is:
    * ENOENT: function not available on this configuration
    * EINVAL: invalid arguments
    * EFAULT: non aligned or null address addr
*/
extern int omp_locality_domain_bind_bloc2dcyclic(
  int nldx, int nldy,
  const void* addr, size_t n, size_t m, size_t ld, size_t wordsize,
  size_t bsx, size_t bsy
);

/** Alloc pages of an array of size bytes on all locality domains
    The pages are allocated using a bloc cyclic strategy of bloc size equals to blocsize. 
    blocsize should be multiple of the page size.
    Returns !=0 in case of success
    Returns 0 in case of failure and set errno to the error code.
    Possible error is:
    * ENOENT: function not available on this configuration
    * EINVAL: invalid argument blocsize
*/
extern void* omp_locality_domain_allocate_bloc1dcyclic(size_t size, size_t blocsize);

/** Alloc pages of an array of size bytes on all locality domains
    The pages are allocated using a 2D bloc cyclic strategy of bloc size equals to bsx x bsy.
    The blocsizes bsx and bst, as well as the size, must be a multiple of a page size.
    Moreover, bsy must divide n and bsy must divide m.
    Returns !=0 in case of success
    Returns 0 in case of failure and set errno to the error code.
    Possible error is:
    * ENOENT: function not available on this configuration
    * EINVAL: invalid argument blocsize
*/
extern void* omp_locality_domain_allocate_bloc2dcyclic(
  int nldx, int nldy,
  size_t n, size_t m, size_t wordsize,
  size_t bsx, size_t bsy
);

/** Free the bloc allocated with alignment constraint compatible for bind_XX method.
    The size of allocated memory is rounded to the smallest multiple of a page size greather than size.
    Returns 0 in case of success
    Returns !=0 in case of failure and set errno to the error code.
*/
extern void* omp_locality_domain_alloc(size_t size);

/** Free the bloc allocated with omp_locality_domain_alloc or omp_locality_domain_allocate_bloc1dcyclic
    Returns 0 in case of success
    Returns !=0 in case of failure and set errno to the error code.
    Possible error is:
    * EFAULT: invalid address addr
*/
extern int omp_locality_domain_free(void* addr, size_t size);


/** Extra API to declare variable number of dependencies on OpenMP tasks.
    Applied to the next created task.
    The scope of array must be valid up to the next task creation.
    It was to the responsability of the caller to manage the storage of 'array' after
    the next task creation.
*/
extern int omp_task_declare_dependencies( int mode, int count, void** array);

#define OMPEXT_MODE_READ      1
#define OMPEXT_MODE_WRITE     2
#define OMPEXT_MODE_READWRITE 3

#if defined(__cplusplus)
}
#endif
#endif // #ifndef _KAAPI_OMP_EXT_H_
