/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "libgomp.h"

static kaapi_lock_t thecritical = KAAPI_LOCK_INITIALIZER;

/*
*/
void GOMP_critical_start (void)
{
  kaapi_atomic_lock(&thecritical);
}

/*
*/
void GOMP_critical_end (void)
{
  kaapi_atomic_unlock(&thecritical);
}


static kaapi_lock_t theatomic = KAAPI_LOCK_INITIALIZER;

/*
*/
void GOMP_atomic_start (void)
{
  kaapi_atomic_lock(&theatomic);
}

/*
*/
void GOMP_atomic_end (void)
{
  kaapi_atomic_unlock(&theatomic);
}


void GOMP_critical_name_start (void ** pptr)
{
  kaapi_lock_t* lock;
  if (sizeof(kaapi_lock_t) <= sizeof(void*))
    lock = (kaapi_lock_t*)pptr;
  else if ((lock = (kaapi_lock_t*)*pptr) ==0)
  {
    /* lock is not garbaded */
    lock = malloc(sizeof(kaapi_lock_t));
    kaapi_atomic_initlock(lock);
    int retval =
#if (__SIZEOF_POINTER__ == 8)
        KAAPI_ATOMIC_CAS64((kaapi_atomic64_t*)pptr, 0, (intptr_t)lock );
#else
        KAAPI_ATOMIC_CAS((kaapi_atomic_t*)pptr, 0, lock );
#endif
    if (!retval)
    {
      kaapi_atomic_destroylock(lock);
      lock = (kaapi_lock_t*)*pptr;
    }
  }

  kaapi_atomic_lock(lock);
}

void GOMP_critical_name_end (void ** pptr)
{
  kaapi_lock_t* lock;
  if (sizeof(kaapi_lock_t) <= sizeof(void*))
    lock = (kaapi_lock_t*)pptr;
  else
    lock = (kaapi_lock_t*)*pptr;
  kaapi_atomic_unlock(lock);
}

