/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012, 2013, 2014 INRIA.
**
** Contributors :
**
** francois.broquedis@imag.fr
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/

#include <libiomp.h>

void omp_init_lock(struct komp_lock_t *lock)
{
  komp_init_lock( lock );
}

void omp_destroy_lock(struct komp_lock_t *lock)
{
  komp_destroy_lock(lock);
}

void omp_set_lock(struct komp_lock_t *lock)
{
  komp_set_lock( lock );
}

void omp_unset_lock(struct komp_lock_t *lock)
{
  komp_unset_lock(lock);
}

int omp_test_lock(struct komp_lock_t *lock)
{
  return komp_test_lock(lock);
}

void __kmpc_init_lock(ident_t * loc, kmp_int32 gtid,  void **user_lock) {
    TRACE_ENTER();
    komp_lock_t *lock = (komp_lock_t *)user_lock;
    komp_init_lock(lock);
    TRACE_LEAVE();
}

void __kmpc_destroy_lock( ident_t *loc, kmp_int32 gtid, void **user_lock ) {
    TRACE_ENTER();
    komp_lock_t *lock = (komp_lock_t *)user_lock;
    komp_destroy_lock(lock);
    TRACE_LEAVE();
}

int __kmpc_test_lock(ident_t * loc, kmp_int32 gtid, void **user_lock) {
    TRACE_ENTER();
    komp_lock_t *lock = (komp_lock_t *)user_lock;
    int retval = komp_test_lock(lock);
    TRACE_LEAVE();
    return retval;
}

void __kmpc_set_lock(ident_t * loc, kmp_int32 gtid, void **user_lock) {
    TRACE_ENTER();
    komp_lock_t *lock = (komp_lock_t *)user_lock;
    komp_set_lock(lock);
    TRACE_LEAVE();
}

void __kmpc_unset_lock(ident_t *loc, kmp_int32 gtid, void **user_lock) {
    TRACE_ENTER();
    komp_lock_t *lock = (komp_lock_t *)user_lock;
    komp_unset_lock(lock);
    TRACE_LEAVE();
}

typedef kmp_int32 kmp_critical_name[8];

static kaapi_lock_t global_lock = KAAPI_LOCK_INITIALIZER;

void __kmpc_critical(ident_t *loc, kmp_int32 global_tid, kmp_critical_name *crit) {
    TRACE_ENTER();
    kaapi_atomic_lock(&global_lock);
    TRACE_LEAVE();
}

void __kmpc_end_critical(ident_t *loc, kmp_int32 global_tid, kmp_critical_name *crit) {
    TRACE_ENTER();
    kaapi_atomic_unlock(&global_lock);
    TRACE_LEAVE();
}
