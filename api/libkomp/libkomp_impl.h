/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** francois.broquedis@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_LIBOMP_IMPL_H_
#define _KAAPI_LIBOMP_IMPL_H_


/* Set to 0 this macro in order to compiler libkomp without support for icv
*/
#define KOMP_USE_ICV 1

#define KOMP_TRACE 0
#include "kaapi.h"
#include "kaapi_impl.h"

#include "libkomp.h"

/* THE definitions 
*/
typedef kaapi_task_t komp_task_t;

#define __KOMP_NOTHROW __attribute__((__nothrow__))

/** Set flag for the next tasks spawn by the current thread
    See omp_ext.h, value should be the same.
*/
enum {
  KOMP_FLAG_DEFAULT      = 0,
  KOMP_FLAG_NUMA_AWARE   = 1,
  KOMP_FLAG_OCR          = 2
};

/* Return the current kprocessor 
*/
static inline struct kaapi_processor_t* komp_self_processor(void)
{
  kaapi_processor_t* kproc = kaapi_self_processor();
  if (kproc) return kproc;
  return kaapi_create_orphan_kprocessor();
}

/* host device ICV */
extern komp_device_icv_t* komp_get_global_device_icv(void);

/* global data env ICV */
extern __thread komp_globalenv_icv_t komp_global_dataenv_icv;

/*
*/
extern komp_dataenv_icv_t* komp_get_dataenv_icv(kaapi_processor_t* proc);

/* loop init
*/
extern int komp_loop_workinit(
  komp_thread_t*             thread,
  int sync,
  const komp_foreach_attr_t* attr,
  long long start,
  long long end,
  long long incr,
  komp_foreach_body_t body,
  void* arg
);

/*
*/
extern int komp_loop_workinit_ull(
  komp_thread_t*             thread,
  int                        sync,
  const komp_foreach_attr_t* attr,
  int                        up,
  unsigned long long         start,
  unsigned long long         end,
  unsigned long long         incr,
  komp_foreach_body_t        body,
  void*                      arg
);

/*
*/
static inline struct komp_task_t* komp_current_task(komp_thread_t* thread)
{
  return (struct komp_task_t*)kaapi_thread2context(thread)->task;
}

/*
*/
static inline komp_taskdata_t* komp_current_taskdata(komp_thread_t* thread)
{
  return (komp_taskdata_t*)kaapi_thread2context(thread)->task->arg;
}


#if KOMP_USE_ICV
/*
*/
extern void komp_newdataenv_icvs(
  struct kaapi_processor_t* kproc,
  komp_dataenv_icv_t* parent_icvs,
  komp_dataenv_icv_t* task_icvs
);


/*
*/
static inline
void komp_update_implicit_icvs(
  int level,
  komp_dataenv_icv_t* icvs
)
{
  if (komp_global_dataenv_icv.nthreads !=0)
  {
    if (komp_global_dataenv_icv.nthreads[0] == 0)
      icvs->h_nthreads = 0;
    else
      icvs->h_nthreads = komp_global_dataenv_icv.nthreads[level];
  }

  if (komp_global_dataenv_icv.binds !=0)
  {
    if (komp_global_dataenv_icv.binds[0] == 0)
      icvs->h_bind = 0;
    else
      icvs->h_bind = komp_global_dataenv_icv.binds[level];
  }
}


/*
*/
extern int komp_get_thread_parallel_region(
    int num_threads,
    int ifclause,
    komp_dataenv_icv_t* icv
);
#endif


#ifdef HAVE_VERSION_SYMBOL

# define strong_alias(fn, al) \
  extern __typeof (fn) al __attribute__ ((alias (#fn)));

#endif


/* Function in OpenMP-4.0  RC 2, p288 */
typedef komp_sched_t omp_sched_t;
typedef komp_proc_bind_t omp_proc_bind_t;
struct komp_lock_t;
struct komp_nest_lock_t;

extern void omp_set_num_threads (int) __KOMP_NOTHROW;
extern int omp_get_num_threads (void) __KOMP_NOTHROW;
extern int omp_get_max_threads (void) __KOMP_NOTHROW;
extern int omp_get_thread_num (void) __KOMP_NOTHROW;
extern int omp_get_num_procs (void) __KOMP_NOTHROW;

extern int omp_in_parallel (void) __KOMP_NOTHROW;

extern void omp_set_dynamic (int) __KOMP_NOTHROW;
extern int omp_get_dynamic (void) __KOMP_NOTHROW;

extern void omp_set_nested (int) __KOMP_NOTHROW;
extern int omp_get_nested (void) __KOMP_NOTHROW;

extern int omp_get_cancellation(void) __KOMP_NOTHROW;

extern void omp_set_schedule (omp_sched_t, int) __KOMP_NOTHROW;
extern void omp_get_schedule (omp_sched_t *, int *) __KOMP_NOTHROW;

extern int omp_get_thread_limit (void) __KOMP_NOTHROW;
extern void omp_set_max_active_levels (int) __KOMP_NOTHROW;
extern int omp_get_max_active_levels (void) __KOMP_NOTHROW;
extern int omp_get_level (void) __KOMP_NOTHROW;
extern int omp_get_ancestor_thread_num (int) __KOMP_NOTHROW;
extern int omp_get_team_size (int) __KOMP_NOTHROW;
extern int omp_get_active_level (void) __KOMP_NOTHROW;
extern int omp_in_final(void);


extern omp_proc_bind_t omp_get_proc_bind(void) __KOMP_NOTHROW;

extern void omp_set_default_device(int device_num) __KOMP_NOTHROW;
extern int omp_get_default_device(void) __KOMP_NOTHROW;
extern int omp_get_num_devices(void) __KOMP_NOTHROW;
extern int omp_get_num_teams(void) __KOMP_NOTHROW;
extern int omp_get_team_num(void) __KOMP_NOTHROW;
extern int omp_is_initial_device(void) __KOMP_NOTHROW;

#if 0
extern void omp_init_lock (struct komp_lock_t *) __KOMP_NOTHROW;
extern void omp_destroy_lock (struct komp_lock_t *) __KOMP_NOTHROW;
extern void omp_set_lock (struct komp_lock_t *) __KOMP_NOTHROW;
extern void omp_unset_lock (struct komp_lock_t *) __KOMP_NOTHROW;
extern int omp_test_lock (struct komp_lock_t *) __KOMP_NOTHROW;

extern void omp_init_nest_lock (struct komp_nest_lock_t *) __KOMP_NOTHROW;
extern void omp_destroy_nest_lock (struct komp_nest_lock_t *) __KOMP_NOTHROW;
extern void omp_set_nest_lock (struct komp_nest_lock_t *) __KOMP_NOTHROW;
extern void omp_unset_nest_lock (struct komp_nest_lock_t *) __KOMP_NOTHROW;
extern int omp_test_nest_lock (struct komp_nest_lock_t *) __KOMP_NOTHROW;
#endif

extern double omp_get_wtime (void) __KOMP_NOTHROW;
extern double omp_get_wtick (void) __KOMP_NOTHROW;


#endif // #ifndef _KAAPI_LIBGOMP_
