/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "libgomp.h"

//#define TRACE 1

#ifdef TRACE
#include <stdio.h>
#define KOMP_TRACE_ENTER \
  printf("%i:: enter '%s'\n", kaapi_self_kid(), __PRETTY_FUNCTION__); fflush(stdout);
#define KOMP_TRACE_EXIT \
  printf("%i:: exit '%s'\n", kaapi_self_kid(), __PRETTY_FUNCTION__); fflush(stdout);
#else
#define KOMP_TRACE_ENTER 
#define KOMP_TRACE_EXIT
#endif

typedef struct komp_trampoline_parallel_loop_start_t {
    void (*fn) (void *);
    void *data;
    long start;
    long end;
    long incr;
    long chunk_size;
    komp_foreach_attr_policy_t policy;
#if KOMP_USE_ICV
    komp_dataenv_icv_t* parent_icvs;
#endif
} komp_trampoline_parallel_loop_start_t;

static void komp_trampoline_parallel_fn( void* a )
{
  komp_trampoline_parallel_loop_start_t* arg = (komp_trampoline_parallel_loop_start_t*)a;
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_context_t* context = kproc->context;
  komp_foreach_attr_t attr;
  komp_foreach_attr_init( &attr );
  komp_foreach_attr_set_policy( &attr, arg->policy );
  komp_foreach_attr_set_grains( &attr, arg->chunk_size, 0 );

  komp_loop_workinit( (komp_thread_t*)kaapi_context2thread(context), 1, &attr, arg->start, arg->end, arg->incr, 0, 0);

#if KOMP_USE_ICV
  /* implicit task ! */
  komp_dataenv_icv_t implicttask_icvs;
  komp_newdataenv_icvs( kproc, arg->parent_icvs, &implicttask_icvs );
  context->komp_icvs = &implicttask_icvs;
#endif
  arg->fn( arg->data );
#if KOMP_USE_ICV
  context->komp_icvs = 0;
#endif
  komp_loop_workfini( (komp_thread_t*)kaapi_context2thread(context) );
}


/* Same as GOMP_parallel_start except that:
   - workshare is initialized before creating sub task
   - the created task is a komp_trampoline_task_parallelfor
   that execute the second branch of GOMP_loop_dynamic_start
*/
static void komp_parallel_loop_policy_start (
  komp_foreach_attr_policy_t policy,
  void (*fn) (void *),
  void *data,
  unsigned num_threads,
  komp_proc_bind_t procbind,
  long start, 
  long end, 
  long incr, 
  long chunk_size
)
{
  kaapi_processor_t* kproc = kaapi_self_processor();
#if KOMP_USE_ICV
  komp_dataenv_icv_t* env = komp_get_dataenv_icv( kproc );
  num_threads = komp_get_thread_parallel_region( num_threads, 1, env );
#endif
  komp_foreach_attr_t attr;
  komp_foreach_attr_init( &attr );
  komp_foreach_attr_set_policy( &attr, policy );
  komp_foreach_attr_set_grains( &attr, chunk_size, 0 );

  /* what does means true here ? convert to default that has mapping to kaapi */
  if (procbind == komp_proc_bind_true)
    procbind = komp_proc_bind_default;

  kaapi_context_t* context = (kproc == 0 ? 0 : kproc->context);
  komp_trampoline_parallel_loop_start_t* arg;
  if (context == 0)
     arg = (komp_trampoline_parallel_loop_start_t*)malloc(
        sizeof(komp_trampoline_parallel_loop_start_t)
      );
  else
     arg = (komp_trampoline_parallel_loop_start_t*)kaapi_data_push(
        kaapi_context2thread(context), sizeof(komp_trampoline_parallel_loop_start_t)
      );
  arg->fn         = fn;
  arg->data       = data;
  arg->start      = start;
  arg->end        = end;
  arg->incr       = incr;
  arg->chunk_size = chunk_size;
  arg->policy     = policy;
#if KOMP_USE_ICV
  arg->parent_icvs= env;
#endif

  struct kaapi_team_t* team = kaapi_begin_parallel(
      num_threads,
      (kaapi_procbind_t)procbind,
      fn,
      komp_trampoline_parallel_fn,
      arg
  );

  kaapi_start_parallel( team, 0 );

  kproc = kaapi_self_processor();
  context = kproc->context;

#if KOMP_USE_ICV
  /* set the ICV to the newly created implicit task icvs*/
  komp_dataenv_icv_t implicttask_icvs;
  komp_newdataenv_icvs( kproc, env, &implicttask_icvs );
  context->komp_icvs = &implicttask_icvs;
#endif

  komp_loop_policy_start( (komp_thread_t*)kaapi_context2thread(context),
      &attr, start, end, incr, 0, 0 );
}

void GOMP_loop_end (void)
{
  KOMP_TRACE_ENTER
  kaapi_processor_t* kproc = kaapi_self_processor();
  if (kproc ==0) 
  {
    KOMP_TRACE_EXIT
    return;
  }
  kaapi_context_t* context = kproc->context;
  komp_loop_workfini( (komp_thread_t*)kaapi_context2thread(context) );
  kaapi_assert_debug(context->ws.count_for_end == context->ws.count_for);
  KOMP_TRACE_EXIT
}

void GOMP_loop_end_nowait (void)
{
  KOMP_TRACE_ENTER
  kaapi_processor_t* kproc = kaapi_self_processor();
  if (kproc ==0) 
  {
    KOMP_TRACE_EXIT
    return;
  }
  kaapi_context_t* context = kproc->context;
  komp_loop_workfini( (komp_thread_t*)kaapi_context2thread(context) );
  kaapi_assert_debug(context->ws.count_for_end == context->ws.count_for);
  KOMP_TRACE_EXIT
}


#if KOMP_USE_ICV
#define GOMP_PARALLEL_LOOP_ICVS \
  kaapi_processor_t* kproc = kaapi_self_processor(); \
  komp_dataenv_icv_t* env = komp_get_dataenv_icv( kproc ); \
  komp_proc_bind_t procbind = env->h_bind; \
  komp_proc_bind_t bind_var = flags & 7; \
  if ((procbind != komp_proc_bind_false) && (bind_var != komp_proc_bind_false)) \
    procbind = env->h_bind = bind_var;
#define GOMP_PARALLEL_LOOP_START_ICVS \
  kaapi_processor_t* kproc = kaapi_self_processor(); \
  komp_dataenv_icv_t* env = komp_get_dataenv_icv( kproc ); \
  komp_proc_bind_t procbind = env->h_bind;
#else
#define GOMP_PARALLEL_LOOP_ICVS \
  komp_proc_bind_t procbind =komp_proc_bind_false;
#define GOMP_PARALLEL_LOOP_START_ICVS \
  komp_proc_bind_t procbind =komp_proc_bind_false;
#endif

/*
*/
#define GOMP_LOOP(GS, KS) \
bool GOMP_loop_##GS##_start ( \
  long start, \
  long end, \
  long incr, \
  long chunk_size,\
  long *istart, \
  long *iend\
)\
{\
  int retval; \
  long long ristart; \
  long long riend; \
  KOMP_TRACE_ENTER \
  if (((incr >0) && (end <= start)) || ((incr <0) && (start <= end))) { KOMP_TRACE_EXIT return 0; }\
  komp_foreach_attr_t attr;\
  komp_foreach_attr_init( &attr );\
  komp_foreach_attr_set_policy( &attr, KS );\
  komp_foreach_attr_set_grains( &attr, chunk_size, 0 );\
  retval = komp_loop_policy_start( komp_self_thread(), &attr, start, end, incr, &ristart, &riend ); \
  if (retval ==0) { KOMP_TRACE_EXIT return 0; }\
  *istart = (long)ristart; \
  *iend   = (long)riend; \
  KOMP_TRACE_EXIT\
  return 1; \
} \
bool GOMP_loop_ordered_##GS##_start ( \
          long start, \
          long end, \
          long incr, \
          long chunk_size, \
          long *istart, \
          long *iend \
) \
{ \
  KOMP_TRACE_ENTER \
  bool retval = GOMP_loop_##GS##_start(start,end, incr, chunk_size, istart, iend); \
  KOMP_TRACE_EXIT \
  return retval; \
} \
void GOMP_parallel_loop_##GS##_start ( \
          void (*fn) (void *), \
          void *data, \
          unsigned num_threads, \
          long start, \
          long end, \
          long incr,  \
          long chunk_size \
) \
{ \
  KOMP_TRACE_ENTER \
  if (((incr >0) && (end <= start)) || ((incr <0) && (start <= end))) { KOMP_TRACE_EXIT return;}\
  GOMP_PARALLEL_LOOP_START_ICVS \
  komp_parallel_loop_policy_start( KS, fn, data, num_threads, procbind, start, end, incr, chunk_size ); \
  KOMP_TRACE_EXIT \
} \
/* GOMP_4.0 */ \
void GOMP_parallel_loop_##GS (void (*fn) (void *), void *data,\
                           unsigned num_threads, long start, long end,\
                           long incr, long chunk_size, unsigned flags)\
{ \
  KOMP_TRACE_ENTER \
  if (((incr >0) && (end <= start)) || ((incr <0) && (start <= end))) { KOMP_TRACE_EXIT return;}\
  GOMP_PARALLEL_LOOP_ICVS \
  komp_parallel_loop_policy_start( KS, fn, data, num_threads, procbind, start, end, incr, chunk_size ); \
  fn (data); \
  GOMP_parallel_end(); \
  KOMP_TRACE_EXIT \
} \
bool GOMP_loop_##GS##_next (long *istart, long *iend) \
{ \
  KOMP_TRACE_ENTER \
  long long ristart; \
  long long riend;\
  int retval = komp_loop_worknext( komp_self_thread(), &ristart, &riend ); \
  if (retval ==0) { KOMP_TRACE_EXIT return 0; }\
  *istart = (long)ristart; \
  *iend   = (long)riend; \
  KOMP_TRACE_EXIT \
  return 1;\
} \
bool GOMP_loop_ordered_##GS##_next (long *istart, long *iend) \
{ \
  KOMP_TRACE_ENTER \
  bool retval = GOMP_loop_##GS##_next( istart, iend); \
  KOMP_TRACE_EXIT \
  return retval; \
}


GOMP_LOOP(static,   KOMP_FOREACH_SCHED_STATIC)
GOMP_LOOP(dynamic,  KOMP_FOREACH_SCHED_DYNAMIC)
GOMP_LOOP(guided,   KOMP_FOREACH_SCHED_GUIDED)
GOMP_LOOP(steal,    KOMP_FOREACH_SCHED_STEAL)
GOMP_LOOP(adaptive, KOMP_FOREACH_SCHED_ADAPTIVE)


bool GOMP_loop_runtime_start (
  long start, long end, long incr, 
  long *istart, 
  long *iend
)
{
  KOMP_TRACE_ENTER 
  bool retval;
  komp_dataenv_icv_t* icv = komp_get_dataenv_icv( kaapi_self_processor() );
  switch (icv->run_sched)
  {
    case komp_sched_static:
      retval = GOMP_loop_static_start(
                  start, end, incr,
                  icv->run_sched_modifier,
                  istart, 
                  iend );
      break;
    case komp_sched_dynamic:
      retval = GOMP_loop_dynamic_start(
                  start, end, incr,
                  icv->run_sched_modifier,
                  istart, 
                  iend );
      break;
    case komp_sched_steal:
      retval = GOMP_loop_steal_start(
                  start, end, incr,
                  icv->run_sched_modifier,
                  istart, 
                  iend );
      break;
    case komp_sched_guided:
      retval = GOMP_loop_guided_start(
                  start, end, incr,
                  icv->run_sched_modifier,
                  istart, 
                  iend );
      break;
    case komp_sched_auto:
    case komp_sched_adaptive:
      retval = GOMP_loop_adaptive_start(
                  start, end, incr,
                  icv->run_sched_modifier,
                  istart, 
                  iend );
    default:
      abort();
  }
  KOMP_TRACE_EXIT 
  return retval;
}


bool GOMP_loop_ordered_runtime_start (
  long start, long end, long incr, 
  long *istart, 
  long *iend
)
{
  KOMP_TRACE_ENTER
  komp_dataenv_icv_t* icv = komp_get_dataenv_icv( kaapi_self_processor() );
  bool retval;
  switch (icv->run_sched)
  {
    case komp_sched_static:
      retval = GOMP_loop_ordered_static_start(
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier,
                  istart,
                  iend );
      break;
    case komp_sched_dynamic:
      retval = GOMP_loop_ordered_dynamic_start(
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier,
                  istart,
                  iend );
      break;
    case komp_sched_steal:
      retval = GOMP_loop_ordered_steal_start(
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier,
                  istart, 
                  iend );
      break;
    case komp_sched_guided:
      retval = GOMP_loop_ordered_guided_start(
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier,
                  istart,
                  iend );
      break;
    case komp_sched_auto:
    case komp_sched_adaptive:
      retval = GOMP_loop_ordered_adaptive_start(
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier,
                  istart,
                  iend );
      break;
    default:
      abort();
  }
  KOMP_TRACE_EXIT
  return retval;
}


void GOMP_parallel_loop_runtime_start (
    void (*fn) (void *), 
    void *data,
    unsigned num_threads, 
    long start, long end, long incr
)
{
  KOMP_TRACE_ENTER
  komp_dataenv_icv_t* icv = komp_get_dataenv_icv( kaapi_self_processor() );
  switch (icv->run_sched)
  {
    case komp_sched_static:
      GOMP_parallel_loop_static_start(
                  fn, data,
                  num_threads, 
                  start, 
                  end,
                  incr,
                  icv->run_sched_modifier );
      break;
    case komp_sched_dynamic:
      GOMP_parallel_loop_dynamic_start(
                  fn, data,
                  num_threads, 
                  start, 
                  end,
                  incr,
                  icv->run_sched_modifier );
      break;
    case komp_sched_steal:
      GOMP_parallel_loop_steal_start(
                  fn, data,
                  num_threads, 
                  start, 
                  end,
                  incr,
                  icv->run_sched_modifier );
      break;
    case komp_sched_guided:
      GOMP_parallel_loop_guided_start(
                  fn, data,
                  num_threads, 
                  start, 
                  end,
                  incr,
                  icv->run_sched_modifier );
      break;
    case komp_sched_auto:
    case komp_sched_adaptive:
      GOMP_parallel_loop_adaptive_start(
                  fn, data,
                  num_threads, 
                  start, 
                  end,
                  incr,
                  icv->run_sched_modifier );
      break;
    default:
      abort();
  }
  KOMP_TRACE_EXIT
}

void GOMP_parallel_loop_runtime(
    void (*fn) (void *),
    void *data,
    unsigned num_threads,
    long start, long end, long incr, 
    unsigned flags
)
{
  KOMP_TRACE_ENTER
  komp_dataenv_icv_t* icv = komp_get_dataenv_icv( kaapi_self_processor() );
  switch (icv->run_sched)
  {
    case komp_sched_static:
      GOMP_parallel_loop_static(
                  fn, data,
                  num_threads,
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier, flags );
      break;
    case komp_sched_dynamic:
      GOMP_parallel_loop_dynamic(
                  fn, data,
                  num_threads,
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier, flags );
      break;
    case komp_sched_steal:
      GOMP_parallel_loop_steal(
                  fn, data,
                  num_threads,
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier, flags );
      break;
    case komp_sched_guided:
      GOMP_parallel_loop_guided(
                  fn, data,
                  num_threads,
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier, flags );
      break;
    case komp_sched_auto:
    case komp_sched_adaptive:
      GOMP_parallel_loop_adaptive(
                  fn, data,
                  num_threads,
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier, flags );
      break;
    default:
      abort();
  }
  KOMP_TRACE_EXIT
}


bool GOMP_loop_runtime_next (
  long *istart, 
  long *iend
)
{
  KOMP_TRACE_ENTER
  komp_dataenv_icv_t* icv = komp_get_dataenv_icv( kaapi_self_processor() );
  bool retval;
  switch (icv->run_sched)
  {
    case komp_sched_static:
      retval = GOMP_loop_static_next(
                  istart,
                  iend );
      break;
    case komp_sched_dynamic:
      retval = GOMP_loop_dynamic_next(
                  istart,
                  iend );
      break;
    case komp_sched_steal:
      retval = GOMP_loop_steal_next(
                  istart,
                  iend );
      break;
    case komp_sched_guided:
      retval = GOMP_loop_guided_next(
                  istart,
                  iend );
      break;
    case komp_sched_auto:
    case komp_sched_adaptive:
      retval = GOMP_loop_adaptive_next(
                  istart,
                  iend );
      break;
    default:
      abort();
  }
  KOMP_TRACE_EXIT
  return retval;
}


bool GOMP_loop_ordered_runtime_next (
  long *istart, 
  long *iend
)
{
  KOMP_TRACE_ENTER
  komp_dataenv_icv_t* icv = komp_get_dataenv_icv( kaapi_self_processor() );
  bool retval;
  switch (icv->run_sched)
  {
    case komp_sched_static:
      retval = GOMP_loop_ordered_static_next(
                  istart,
                  iend );
      break;
    case komp_sched_dynamic:
      retval = GOMP_loop_ordered_dynamic_next(
                  istart,
                  iend );
      break;
    case komp_sched_steal:
      retval = GOMP_loop_ordered_steal_next(
                  istart,
                  iend );
      break;
    case komp_sched_guided:
      retval = GOMP_loop_ordered_guided_next(
                  istart,
                  iend );
      break;
    case komp_sched_auto:
    case komp_sched_adaptive:
      retval = GOMP_loop_ordered_adaptive_next(
                  istart,
                  iend );
      break;
    default:
      abort();
  }
  KOMP_TRACE_EXIT
  return retval;
}

