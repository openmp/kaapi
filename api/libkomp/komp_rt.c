/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** francois.broquedis@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef __KOMP_RT_C_
#define __KOMP_RT_C_

#include "libkomp_impl.h"
#include "omp_ext.h"
#include <stdio.h>

void
komp_set_num_threads (int n)
{
  if (n<=0) return;
  komp_dataenv_icv_t* env = komp_get_dataenv_icv(kaapi_self_processor());
  env->h_nthreads = (unsigned int)n;
}

int
komp_get_num_threads (void)
{
  return kaapi_get_concurrency();
}

int
komp_get_thread_num (void)
{
  kaapi_processor_t* kproc = kaapi_self_processor();
  if (kproc ==0) return 0; /* master of an orphan construct */
  return (int)kproc->rsrc.kid;
}

/*
*/
int 
komp_get_max_threads (void)
{
  komp_dataenv_icv_t* env = komp_get_dataenv_icv(kaapi_self_processor());
  return (int)env->h_nthreads;
}

int komp_get_num_procs (void)
{
  return kaapi_default_param.syscpucount;
}

/*
*/
int 
komp_in_parallel(void)
{
  kaapi_team_t* team = (kaapi_team_t*)komp_self_team();
  if (team ==0) return 0;
  return 1;
}

/*
*/
void 
komp_set_dynamic(int dynamic_threads )
{
  komp_get_dataenv_icv(kaapi_self_processor())->dyn = dynamic_threads;
}

/*
*/
int 
komp_get_dynamic(void)
{
  return komp_get_dataenv_icv(kaapi_self_processor())->dyn;
}

/*
*/
void 
komp_set_nested(int nested)
{
  komp_get_dataenv_icv(kaapi_self_processor())->nest = nested;
}

/*
*/
int 
komp_get_nested(void)
{
  return komp_get_dataenv_icv(kaapi_self_processor())->nest;
}

/*
*/
int komp_get_cancellation(void)
{
  return komp_get_global_device_icv()->cancel != 0;
}


/*
*/
void 
komp_set_schedule(komp_sched_t kind, int modifier )
{
  komp_dataenv_icv_t* env;
  if ((kind <= 0) && (kind >6))
  {
    fprintf (stderr, "[libKOMP] omp_set_schedule: invalid argument '%i' ignored\n", kind);
    return;
  }
  if (kind == komp_sched_auto)
    modifier = 0;
  else
  { /* only valid modifier (>=0, 0: default, >0: user defined) is accepted */
    if (modifier <0)
    {
      fprintf (stderr, "[libKOMP] omp_set_schedule: invalid negative modifier '%i' ignored\n",
          modifier
      );
      modifier = 0; /* default value */
    }
  }
  env = komp_get_dataenv_icv(kaapi_self_processor());
  env->run_sched = kind;
  env->run_sched_modifier = modifier;
}


/*
*/
void 
komp_get_schedule(komp_sched_t * kind, int * modifier )
{
  komp_dataenv_icv_t* env = komp_get_dataenv_icv(kaapi_self_processor());
  *kind = env->run_sched;
  *modifier = (int)env->run_sched_modifier;
}


/* Should depend on the default device: OMP-4.0 spec
*/
int 
komp_get_thread_limit(void)
{ 
  return komp_global_dataenv_icv.thread_limit;
}


/*
*/
void 
komp_set_max_active_levels (int max_levels )
{
  if (max_levels <0)
  {
    fprintf(stderr, "[libKOMP] omp_set_max_active_levels, ignoring non positive value\n");
    return;
  }
  komp_get_global_device_icv()->max_active_levels = max_levels;
}


/*
*/
int 
komp_get_max_active_levels(void)
{
  return komp_get_global_device_icv()->max_active_levels;
}


/*
*/
int
komp_get_level(void)
{
  kaapi_team_t* team = (kaapi_team_t*)komp_self_team();
  if (team ==0) return 0;
  return team->levels;
}


/* Returns the id of the level-th ancestor task/thread of the calling
task. */
int 
komp_get_ancestor_thread_num (int level)
{
  int k;

  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_team_t* team = kproc->team;
  if (team ==0) return -1;

  int max_levels = team->levels;
  if (level == 0) 
    return 0;
  if ((level <0) || (level > max_levels))
    return -1;
  if (level == max_levels)
    return (int)kproc->rsrc.kid;

  int maxk = max_levels-level-1;
  for (k=0; k < maxk; ++k)
  {
    team = team->parent_team;
  }
  return team->tid;
}


/*
*/
int 
komp_get_team_size (int level)
{
  if (level == 0) return 1;
  int k;
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_team_t* team = (kaapi_team_t*)kproc->team;
  int max_levels = team->levels;
  if ((level <0) || (level > max_levels))
    return -1;

  if (level == max_levels)
    return team->count;

  int maxk = max_levels-level;
  for (k=0; k < maxk; ++k)
  {
    team = team->parent_team;
  }
  return team->count;
}

/*
*/
int 
komp_get_active_level(void)
{
  kaapi_team_t* team = (kaapi_team_t*)komp_self_team();
  if (team ==0) return 0;
  return team->active_levels;
}

/*
*/
int 
komp_in_final(void)
{
  kaapi_task_t* task = (kaapi_task_t*)komp_current_task(komp_self_thread());
  if (!task) return 0;
  return (task->u.s.flag & KAAPI_TASK_FLAG_IN_FINAL) != 0;
}

komp_proc_bind_t komp_get_proc_bind(void)
{
  komp_dataenv_icv_t* env = komp_get_dataenv_icv(kaapi_self_processor());
  return env->h_bind;
}

double komp_get_wtime(void)
{
  return kaapi_get_elapsedtime();
}

double komp_get_wtick(void)
{
  return 1e-6; /* elapsed time is assumed to be in micro second ?? */
}

void komp_set_default_device(int device_num)
{
  fprintf (stderr, "[libKOMP] Error: komp_set_default_device() not implemented!\n");
  exit (-1);
}

int komp_get_default_device(void)
{
  return komp_global_dataenv_icv.def_device;;
}

int komp_get_num_devices(void)
{
  return 1;
}

int komp_get_num_teams(void)
{
  return 1;
}

int komp_get_team_num(void)
{
  return 0;
}

int komp_is_initial_device(void)
{
  return 1;
}

/*
*/
void
komp_set_datadistribution_bloccyclic( unsigned long long size, unsigned int length )
{
#if defined(KAAPI_USE_FOREACH_WITH_DATADISTRIBUTION)
  printf("In komp_set_datadistribution_bloccyclic\n");
  kompctxt_t* ctxt = komp_get_ctxt();
  kaapic_foreach_attr_set_bloccyclic_datadistribution( &ctxt->icv.attr, size, length );
#endif
}

/*
*/
int komp_set_task_affinity(komp_affinity_kind_t kind, unsigned long long affinity, unsigned int strict)
{
#if defined(KAAPI_USE_SCHED_AFFINITY)
  komp_dataenv_icv_t* icvs = komp_get_dataenv_icv(kaapi_self_processor());
  icvs->flag &= ~KAAPI_TASK_FLAG_LD_BOUND;
  icvs->flag |= (strict ? KAAPI_TASK_FLAG_LD_BOUND : 0);
  switch (kind) {
    case komp_affinity_numa:
      if ((affinity <0) && (affinity >=kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count))
      {
        fprintf(stderr,"*** invalid lid:%i\n", (int)affinity );
        return EINVAL;
      }
      kaapi_assert_debug( affinity < USHRT_MAX );
      icvs->flag |= KAAPI_TASK_FLAG_AFF_NODE;
      icvs->node_id = (int)affinity;
      break;
    case komp_affinity_depend:
      icvs->affinity = (void*)affinity;
      icvs->flag |= KAAPI_TASK_FLAG_AFF_ADDR;
      break;
    case komp_affinity_core:
      /*FIXME check boudaries*/
      icvs->core_id = (int)affinity;
      icvs->flag |= KAAPI_TASK_FLAG_AFF_CORE;
      return EINVAL;
    default:
      fprintf(stderr, "*** Unknown affinity kind.\n");
      return EINVAL;
      break;
  }
#endif
  return 0;
}


/*
*/
int komp_set_task_priority(int prio)
{
  if (prio <0)
    return EINVAL;
  komp_dataenv_icv_t* icvs = komp_get_dataenv_icv(kaapi_self_processor());
  icvs->prio = (uint8_t)prio;
  return 0;
}


/*
*/
void komp_set_task_name(const char* name)
{
  if (name ==0) return;
  komp_dataenv_icv_t* icvs = komp_get_dataenv_icv(kaapi_self_processor());
  icvs->task_name = name;
}


/*
*/
int komp_task_declare_dependencies( int mode, int count, void** array)
{
  if (count ==0) return 0;
#if KOMP_USE_ICV
  void*** ref = 0;
  int *size;
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_thread_t* thread = kaapi_kproc2thread(kproc);
  komp_dataenv_icv_t* icvs = komp_get_dataenv_icv(kproc);
  komp_extradep_icv_t* icvs_ed = icvs->extra_deps;
  if (icvs_ed == 0)
  {
    icvs_ed = kaapi_thread_pushdata(thread, sizeof(komp_extradep_icv_t));
    icvs_ed->ext_mode_r = icvs_ed->ext_mode_w = icvs_ed->ext_mode_rw = 0;
    icvs_ed->sizeext_mode_r = icvs_ed->sizeext_mode_w = icvs_ed->sizeext_mode_rw = 0;
    icvs->extra_deps = icvs_ed;
  }

  switch (mode) {
    case OMPEXT_MODE_READ:
      ref = &icvs_ed->ext_mode_r;
      size = &icvs_ed->sizeext_mode_r;
      break;
    case OMPEXT_MODE_WRITE:
      ref = &icvs_ed->ext_mode_w;
      size = &icvs_ed->sizeext_mode_w;
      break;
    case OMPEXT_MODE_READWRITE:
      ref = &icvs_ed->ext_mode_rw;
      size = &icvs_ed->sizeext_mode_rw;
      break;
    default:
      return EINVAL;
  }
  if (*size !=0) return EBUSY; /* already in use */
  *size = count;
  *ref  = array;
#else
  return EINVAL;
#endif
  return 0;
}


/* TO RE_IMPLEMENT: place in OMP is not places in kaapi (== harware places masked by used ressources)
*/
int komp_get_num_places(void)
{
  return kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count;
}

/* TO RE_IMPLEMENT: place in OMP is not places in kaapi (== harware places masked by used ressources)
*/
int komp_get_place_num_procs(int place_num)
{
  if ((place_num <0)
   || (place_num >= kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count))
    return 0;
  return KAAPI_CPUSET_COUNT( &kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].cpuset[place_num] );
}

/* TO RE_IMPLEMENT: place in OMP is not places in kaapi (== harware places masked by used ressources)
*/
void komp_get_place_proc_ids(int place_num, int *ids)
{
  if ((place_num <0)
   || (place_num >= kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count))
    return;

  kaapi_cpuset_t cpuset = kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].cpuset[place_num];
  while (KAAPI_CPUSET_COUNT(&cpuset) !=0)
  {
    int idx = KAAPI_CPUSET_FIRSTONE(&cpuset) -1;
    KAAPI_CPUSET_CLR( idx-1, &cpuset);
    *ids++ = idx;
  }
}

/* TO RE_IMPLEMENT: place in OMP is not places in kaapi (== harware places masked by used ressources)
*/
int komp_get_place_num(void)
{
  return 0;
}

/* TO RE_IMPLEMENT: place in OMP is not places in kaapi (== harware places masked by used ressources)
*/
int komp_get_partition_num_places(void)
{
  return 1;
}

/* TO RE_IMPLEMENT: place in OMP is not places in kaapi (== harware places masked by used ressources)
*/
void komp_get_partition_place_nums(int *place_nums)
{
  *place_nums = 0;
}

/* TO RE_IMPLEMENT: place in OMP is not places in kaapi (== harware places masked by used ressources)
*/
int komp_get_max_task_priority(void)
{
  return 1;
}

/*
*/
int komp_get_num_locality_domains(void)
{
  return kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count;
}

/*
*/
int komp_get_locality_domain_num(void)
{
  return (int)kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].count;
}


/*
*/
int komp_get_locality_domain_num_for(void* addr)
{
  return kaapi_numa_getpage_id(addr);
}


int komp_locality_domain_get_num_cpu(int ldid)
{
  return (int)kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].count;
}

/*
*/
int komp_locality_domain_bind_bloc1dcyclic(const void* addr, unsigned long size, unsigned long blocsize)
{
  kaapi_numaset_t set = (1U << kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count)-1;
  return kaapi_numa_bind_bloc1dcyclic( set, addr, size, blocsize );
}

/*
*/
void* komp_locality_domain_allocate_bloc1dcyclic(unsigned long size, unsigned long blocsize)
{
  kaapi_numaset_t set = (1U << kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count)-1;
  return kaapi_numa_allocate_bloc1dcyclic( set, size, blocsize );
}

/*
*/
int komp_locality_domain_bind_bloc2dcyclic(
  int nldx, int nldy,
  const void* addr, unsigned long n, unsigned long m, unsigned long ld, unsigned long wordsize,
  unsigned long bsx, unsigned long bsy
)
{
  kaapi_numaset_t set = (1U << kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count)-1;
  return kaapi_numa_bind_bloc2dcyclic(set, nldx, nldy, addr, n, m, ld, wordsize, bsx, bsy);
}

/*
*/
void* komp_locality_domain_allocate_bloc2dcyclic(
  int nldx, int nldy,
  unsigned long n, unsigned long m, unsigned long wordsize,
  unsigned long bsx, unsigned long bsy
)
{
  kaapi_numaset_t set = (1U << kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count)-1;
  return kaapi_numa_allocate_bloc2dcyclic( set, nldx, nldy, n, m, wordsize,  bsx, bsy);
}

/*
*/
void* komp_locality_domain_alloc(unsigned long size)
{
  return kaapi_numa_alloc(size);
}

/*
*/
int komp_locality_domain_free(void* addr, unsigned long size)
{
  return kaapi_numa_free(addr, size);
}

/*FIXME find a better place for this*/
void *tmp_select = 0;
void *tmp_push = 0;
void *tmp_pushinit = 0;

void komp_begin_push_init(komp_push_init_t kind)
{
  tmp_select = kaapi_default_param.wsselect;
  tmp_push = kaapi_default_param.wspush;
  tmp_pushinit = kaapi_default_param.wspush_init;
#if defined(KAAPI_USE_NUMA)
  kaapi_default_param.wsselect = &kaapi_sched_select_victim_strict_push;
  kaapi_default_param.wspush   = &kaapi_sched_select_queue_localnuma;
#else
  kaapi_default_param.wsselect = &kaapi_sched_select_victim_rand;
  kaapi_default_param.wspush   = &kaapi_sched_select_queue_cyclic;
#endif
  /* Use the specified push_init distrib */
  switch (kind) {
    /*komp_init_runtime          = 0,*/
    /*komp_init_rand             = 1,*/
    /*komp_init_cyclic           = 2,*/
    /*komp_init_randnuma         = 3,*/
    /*komp_init_cyclicnuma       = 4,*/
    /*komp_init_cyclicnumastrict = 5,*/
    case komp_init_runtime:
      /*FIXME verify interaction with environment variable is ok */
      kaapi_default_param.wspush_init = &kaapi_sched_push_init_queue_wspush;
      break;
    case komp_init_rand:
      kaapi_default_param.wspush_init = &kaapi_sched_push_init_queue_randkproc;
      break;
    case komp_init_cyclic:
      kaapi_default_param.wspush_init = &kaapi_sched_push_init_queue_cyclickproc;
      break;
#if defined(KAAPI_USE_NUMA)
    case komp_init_cyclicnuma:
      kaapi_default_param.wspush_init = &kaapi_sched_push_init_queue_cyclicnuma;
      break;
    case komp_init_cyclicnumastrict:
      kaapi_default_param.wspush_init = &kaapi_sched_push_init_queue_cyclicnumastrict;
      break;
    case komp_init_randnuma:
      kaapi_default_param.wspush_init = &kaapi_sched_push_init_queue_randnuma;
      break;
#else
    default:
      fprintf(stderr, "Unsupported value for push_init.\n");
      break;
#endif
  }
}

void komp_end_push_init()
{
  kaapi_default_param.wsselect = tmp_select;
  kaapi_default_param.wspush = tmp_push;
  /*Restore default push after init, to guarantee compatibility between select/push_init
   * outside initialization */
  kaapi_default_param.wspush_init = tmp_pushinit;
}

void komp_begin_numa_init(void **select, void **push, void **push_init)
{
  *select = kaapi_default_param.wsselect;
  *push = kaapi_default_param.wspush;
  *push_init = kaapi_default_param.wspush_init;
#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
  kaapi_default_param.wsselect = &kaapi_sched_select_victim_strict_push;
  kaapi_default_param.wspush = &kaapi_sched_select_queue_localnuma;
  /* Use the specified push_init distrib */
  kaapi_default_param.wspush_init = kaapi_default_param.wspush_init_distrib;
#endif
}

void komp_end_numa_init(void *select, void *push, void *push_init)
{
  kaapi_default_param.wsselect = select;
  kaapi_default_param.wspush = push;
  /*Restore default push after init, to guarantee compatibility between select/push_init
   * outside initialization */
  kaapi_default_param.wspush_init = push_init;
}


#if defined(HAVE_VERSION_SYMBOL) 
strong_alias (komp_set_num_threads,        omp_set_num_threads)
strong_alias (komp_get_num_threads,        omp_get_num_threads)
strong_alias (komp_get_max_threads,        omp_get_max_threads)
strong_alias (komp_get_thread_num,         omp_get_thread_num)
strong_alias (komp_get_num_procs,          omp_get_num_procs)
strong_alias (komp_in_parallel,            omp_in_parallel)
strong_alias (komp_set_dynamic,            omp_set_dynamic)
strong_alias (komp_get_dynamic,            omp_get_dynamic)
strong_alias (komp_set_nested,             omp_set_nested)
strong_alias (komp_get_nested,             omp_get_nested)
strong_alias (komp_get_cancellation,       omp_get_cancellation)
strong_alias (komp_set_schedule,           omp_set_schedule)
strong_alias (komp_get_schedule,           omp_get_schedule)
strong_alias (komp_get_thread_limit,       omp_get_thread_limit)
strong_alias (komp_set_max_active_levels,  omp_set_max_active_levels)
strong_alias (komp_get_max_active_levels,  omp_get_max_active_levels)
strong_alias (komp_get_level,              omp_get_level)

strong_alias (komp_get_ancestor_thread_num, omp_get_ancestor_thread_num)
strong_alias (komp_get_team_size,          omp_get_team_size)
strong_alias (komp_get_active_level,       omp_get_active_level)
strong_alias (komp_in_final,               omp_in_final)
strong_alias (komp_get_proc_bind,          omp_get_proc_bind)
strong_alias (komp_set_default_device,     omp_set_default_device)
strong_alias (komp_get_default_device,     omp_get_default_device)

strong_alias (komp_get_num_devices,        omp_get_num_devices)
strong_alias (komp_get_num_teams,          omp_get_num_teams)
strong_alias (komp_get_team_num,           omp_get_team_num)
strong_alias (komp_is_initial_device,      omp_is_initial_device)

strong_alias (komp_get_wtime,              omp_get_wtime)
strong_alias (komp_get_wtick,              omp_get_wtick)


strong_alias (komp_get_num_places,         omp_get_num_places)
strong_alias (komp_get_place_num_procs,    omp_get_place_num_procs)
strong_alias (komp_get_place_proc_ids,     omp_get_place_proc_ids)
strong_alias (komp_get_place_num,          omp_get_place_num)
strong_alias (komp_get_partition_num_places, omp_get_partition_num_places)
strong_alias (komp_get_partition_place_nums, omp_get_partition_place_nums)
strong_alias (komp_get_max_task_priority,  omp_get_max_task_priority)

#else

void omp_set_num_threads (int n)
{ komp_set_num_threads(n); }

int omp_get_num_threads (void)
{ return komp_get_num_threads(); }

int omp_get_thread_num (void)
{ return komp_get_thread_num(); }

int omp_get_max_threads (void)
{ return komp_get_max_threads(); }

int omp_get_num_procs (void)
{ return komp_get_num_procs(); }

int omp_in_parallel(void)
{ return komp_in_parallel(); }

void omp_set_dynamic(int dynamic_threads )
{ komp_set_dynamic(dynamic_threads); }

int omp_get_dynamic(void)
{ return komp_get_dynamic(); }

void omp_set_nested(int nested)
{ komp_set_nested(nested); }

int omp_get_nested(void)
{ return komp_get_nested(); }

int omp_get_cancellation(void)
{ return komp_get_cancellation(); }

void omp_set_schedule(komp_sched_t kind, int modifier )
{ komp_set_schedule(kind,modifier); }

void omp_get_schedule(komp_sched_t * kind, int * modifier )
{ komp_get_schedule(kind,modifier); }

int omp_get_thread_limit(void)
{ return komp_get_thread_limit(); }

void omp_set_max_active_levels (int max_levels )
{ komp_set_max_active_levels(max_levels); }

int omp_get_max_active_levels(void)
{ return komp_get_max_active_levels(); }

int omp_get_level(void)
{ return komp_get_level(); }

int omp_get_ancestor_thread_num (int level)
{ return komp_get_ancestor_thread_num(level); }

int omp_get_team_size (int level)
{ return komp_get_team_size(level); }

int omp_get_active_level(void)
{ return komp_get_active_level(); }

int omp_in_final(void)
{ return komp_in_final(); }

komp_proc_bind_t omp_get_proc_bind(void)
{ return komp_get_proc_bind(); }

double omp_get_wtime(void)
{ return komp_get_wtime(); }

double omp_get_wtick(void)
{ return komp_get_wtick(); }

void omp_set_default_device(int device_num)
{ komp_set_default_device( device_num ); }

int omp_get_default_device(void)
{ return komp_get_default_device(); }

int omp_get_num_devices(void)
{ return komp_get_num_devices(); }

int omp_get_num_teams(void)
{ return komp_get_num_teams(); }

int omp_get_team_num(void)
{ return komp_get_team_num(); }

int omp_is_initial_device(void)
{ return komp_is_initial_device(); }

int omp_get_num_places(void)
{ return komp_get_num_places(); }

int omp_get_place_num_procs(int place_num)
{ return komp_get_place_num_procs( place_num ); }

void omp_get_place_proc_ids(int place_num, int *ids)
{ komp_get_place_proc_ids( place_num, ids ); }

int omp_get_place_num(void)
{ return komp_get_place_num(); }

int omp_get_partition_num_places(void)
{ return komp_get_partition_num_places(); }

void omp_get_partition_place_nums(int *place_nums)
{ komp_get_partition_place_nums( place_nums ); }

int omp_get_max_task_priority(void)
{ return komp_get_max_task_priority(); }

#endif


#endif /* #define __KOMP_RT_C_ */
