/*
 ** xkaapi
 ** 
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** francois.broquedis@imag.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include "libgomp.h"


/*
   which= GOMP_CANCEL_LOOP | GOMP_CANCEL_SECTIONS | GOMP_CANCEL_TASKGROUP | GOMP_CANCEL_PARALLEL
*/

bool
GOMP_cancellation_point (int which)
{
#if KOMP_USE_ICV
  if (!komp_get_global_device_icv()->cancel)
    return false;
  if (which & GOMP_CANCEL_TASKGROUP)
  {
    kaapi_task_t* currtask = (kaapi_task_t*)komp_current_task(komp_self_thread());
    komp_taskdata_t* currtask_data = (komp_taskdata_t*)(currtask->arg);
    if (currtask_data!=0)
    {
      komp_taskgroup_t* tg = currtask_data->task_group;
      if (tg && tg->cancelled)
        return true;
    }
  }
#endif
  return false;
}

bool
GOMP_cancel (int which, bool do_cancel)
{
  if (!komp_get_global_device_icv()->cancel)
    return false;

  if (!do_cancel)
    GOMP_cancellation_point(which);

#if KOMP_USE_ICV
  if (which & GOMP_CANCEL_TASKGROUP)
  {
    kaapi_task_t* currtask = (kaapi_task_t*)komp_current_task(komp_self_thread());
    komp_taskdata_t* currtask_data = (komp_taskdata_t*)(currtask->arg);
    if (currtask_data!=0)
    {
      komp_taskgroup_t* tg = currtask_data->task_group;
      if (tg)
        tg->cancelled = 1;
    }
  }
#endif
  
  return true;
}

