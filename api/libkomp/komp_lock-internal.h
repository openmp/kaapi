/*
** xkaapi
** 
**
** Copyright 2012 INRIA.
**
** Contributors :
**
** vincent.danjean@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_LOCK_INTERNAL_H_
#define _KAAPI_LOCK_INTERNAL_H_

#include "kaapi.h"

/* Simple lock: map omp_lock_t to the atomic type in kaapi...
*/
typedef struct komp_lock_t {
  kaapi_atomic_t lock;
} komp_lock_t;

typedef struct komp_nest_lock_t {
	komp_lock_t lock;
	int count;
	void* owner;
} komp_nest_lock_t;

/* Warning: as "omp.h" system header does not defined the following types
 * we can not automatically check they are compatible
 */
typedef kaapi_atomic_t komp_lock_25_t;
typedef struct {
	int owner;
	int count;
} komp_nest_lock_25_t;


#ifdef HAVE_VERSION_SYMBOL
extern void komp_init_lock_30 (struct komp_lock_t *) __KOMP_NOTHROW;
extern void komp_destroy_lock_30 (struct komp_lock_t *) __KOMP_NOTHROW;
extern void komp_set_lock_30 (struct komp_lock_t *) __KOMP_NOTHROW;
extern void komp_unset_lock_30 (struct komp_lock_t *) __KOMP_NOTHROW;
extern int  komp_test_lock_30 (struct komp_lock_t *) __KOMP_NOTHROW;
extern void komp_init_nest_lock_30 (struct komp_nest_lock_t *) __KOMP_NOTHROW;
extern void komp_destroy_nest_lock_30 (struct komp_nest_lock_t *) __KOMP_NOTHROW;
extern void komp_set_nest_lock_30 (struct komp_nest_lock_t *) __KOMP_NOTHROW;
extern void komp_unset_nest_lock_30 (struct komp_nest_lock_t *) __KOMP_NOTHROW;
extern int  komp_test_nest_lock_30 (struct komp_nest_lock_t *) __KOMP_NOTHROW;

#if 0 /* not yet implemented */
extern void komp_init_lock_25 (struct komp_lock_25_t *) __KOMP_NOTHROW;
extern void komp_destroy_lock_25 (struct komp_lock_25_t *) __KOMP_NOTHROW;
extern void komp_set_lock_25 (struct komp_lock_25_t *) __KOMP_NOTHROW;
extern void komp_unset_lock_25 (struct komp_lock_25_t *) __KOMP_NOTHROW;
extern int  komp_test_lock_25 (struct komp_lock_25_t *) __KOMP_NOTHROW;
extern void komp_init_nest_lock_25 (struct komp_nest_lock_25_t *) __KOMP_NOTHROW;
extern void komp_destroy_nest_lock_25 (struct komp_nest_lock_25_t *) __KOMP_NOTHROW;
extern void komp_set_nest_lock_25 (struct komp_nest_lock_25_t *) __KOMP_NOTHROW;
extern void komp_unset_nest_lock_25 (struct komp_nest_lock_25_t *) __KOMP_NOTHROW;
extern int  komp_test_nest_lock_25 (struct komp_nest_lock_25_t *) __KOMP_NOTHROW;
#endif


# define komp_lock_symver30(fn) \
  __asm (".symver k" #fn "_30, k" #fn "@@KOMP_1.0");\
  __asm (".symver k" #fn ", " #fn "@@OMP_3.0");
# define komp_lock_symver(fn) \
  komp_lock_symver30(fn) \
  __asm (".symver k" #fn "_25, " #fn "@OMP_1.0");

#else
# define komp_init_lock_30 omp_init_lock
# define komp_destroy_lock_30 omp_destroy_lock
# define komp_set_lock_30 omp_set_lock
# define komp_unset_lock_30 omp_unset_lock
# define komp_test_lock_30 omp_test_lock
# define komp_init_nest_lock_30 omp_init_nest_lock
# define komp_destroy_nest_lock_30 omp_destroy_nest_lock
# define komp_set_nest_lock_30 omp_set_nest_lock
# define komp_unset_nest_lock_30 omp_unset_nest_lock
# define komp_test_nest_lock_30 omp_test_nest_lock

#define komp_lock_symver30(fn)
#define komp_lock_symver(fn)

#endif

#endif
