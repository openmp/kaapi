/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012, 2013, 2014 INRIA.
**
** Contributors :
**
** francois.broquedis@imag.fr
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "libiomp.h"
#include <string.h>

#if KMP_ARCH_X86
# define KMP_SIZE_T_MAX (0xFFFFFFFF)
#else
# define KMP_SIZE_T_MAX (0xFFFFFFFFFFFFFFFF)
#endif

// Round up a size to a power of two specified by val
// Used to insert padding between structures co-allocated using a single malloc() call
static size_t
__kmp_round_up_to_val( size_t size, size_t val ) {
    if ( size & ( val - 1 ) ) {
        size &= ~ ( val - 1 );
        if ( size <= KMP_SIZE_T_MAX - val ) {
            size += val;    // Round up if there is no overflow.
        }; // if
    }; // if
    return size;
} // __kmp_round_up_to_va


/*FIXME: use an enum for strict, or at least kmp_uint8*/
void __kmpc_omp_set_task_affinity(kmp_uint32 kind, kmp_uint64 affinity, kmp_uint32 strict)
{
  komp_set_task_affinity((komp_affinity_kind_t)kind, affinity, strict);
}

void __kmpc_omp_set_task_name(const char *name)
{
  komp_set_task_name(name);
}


/*
*/
kmp_task_t *__kmpc_omp_task_alloc(ident_t *loc_ref,
                                  kmp_int32 gtid,
                                  kmp_int32 flags,
                                  size_t sizeof_kmp_task_t,
                                  size_t sizeof_shareds,
                                  kmp_routine_entry_t task_entry) 
{
    TRACE_ENTER();
    kaapi_thread_t* thread = kaapi_self_thread();

    size_t shareds_offset = sizeof( kmp_taskdata_t ) + sizeof_kmp_task_t;
    shareds_offset = __kmp_round_up_to_val( shareds_offset, sizeof( void * ));

    size_t size_task = shareds_offset + sizeof_shareds;
    /*We want kaapi data which look like this :*/
    /*data {
     * - kmp_taskdata_t
     * - kmp_task_t
     * - private_vars
     * - shared section
    }*/
    kmp_taskdata_t *new_taskdata = kaapi_data_push_align(thread, (uint32_t)size_task, sizeof(void *));
    kmp_task_t     *new_task = (kmp_task_t*)(new_taskdata+1);
    new_taskdata->size    = (uint32_t)size_task;
    new_taskdata->flags    = flags;
    new_taskdata->n_depend = 0;
    new_taskdata->depends  = 0;
    /*Sizeof(kmp_task_t) != sizeof_kmp_task_t, because of (first)private vars*/
    new_task->shareds = (void*)((uintptr_t)new_taskdata + shareds_offset);
    new_task->routine = task_entry;
    new_task->part_id = 0;

    kaapi_assert_debug( ( ((uintptr_t)new_taskdata) & (sizeof(double)-1) ) == 0 );
    kaapi_assert_debug( ( ((uintptr_t)new_task) & (sizeof(double)-1) ) == 0 );
    kaapi_assert_debug( ( ((uintptr_t)new_task->shareds) & (sizeof(double)-1) ) == 0 );

    TRACE_LEAVE();

    return new_task;
}


/*
*/
static void __kmpc_wrapper_task( struct komp_task_t* t, struct komp_thread_t* thread )
{
  kaapi_task_t* task          = (kaapi_task_t*)t;
  kmp_taskdata_t *kmptaskdata = (kmp_taskdata_t*)task->arg;
  kmp_task_t* kmptask         = (kmp_task_t*)(kmptaskdata+1);
  kaapi_context_t* context    = kaapi_thread2context(thread);
  void* padding[8];
  kmptask->routine( (kmp_int32)context->proc->rsrc.kid, kmptask );
}


/*
*/
kmp_int32 __kmpc_omp_task_with_deps(
    ident_t *loc_ref,
    kmp_int32 gtid,
    kmp_task_t * new_task,
    kmp_int32 ndeps,
    kmp_depend_info_t *dep_list,
    kmp_int32 ndeps_noalias,
    kmp_depend_info_t *noalias_dep_list
)
{
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_thread_t* thread = kaapi_kproc2thread(kproc);
  kmp_taskdata_t *kmptaskdata = ((kmp_taskdata_t*)new_task) - 1;
#if KOMP_USE_ICV
  kmp_tasking_flags_t *input_flags = (kmp_tasking_flags_t *) & kmptaskdata->flags;
#endif

  size_t fulldep_size = sizeof(komp_depend_info_t) *(ndeps + ndeps_noalias);
  void *data_dep = kaapi_data_push(thread, fulldep_size);
  kmptaskdata->n_depend = ndeps + ndeps_noalias;

  if (kmptaskdata->n_depend)
  {
    komp_depend_info_t *task_deps = data_dep;
    kmptaskdata->depends = task_deps;

    int i, shift_noalias = 0;
    for (i = 0; i < ndeps; i++)
    {
      komp_access_mode_t mode;
      kmp_depend_info_t *cur_kmp  = &dep_list[i];
      if (cur_kmp->flags.in && cur_kmp->flags.out)
        mode = KOMP_ACCESS_MODE_RW;
      else if (cur_kmp->flags.in)
        mode = KOMP_ACCESS_MODE_R;
      else 
        mode = KOMP_ACCESS_MODE_RW; // in place of W
      task_deps[i].access.data    = dep_list[i].base_addr;
      task_deps[i].access.version = 0;
      task_deps[i].mode           = mode;
#if defined(KAAPI_DEBUG)
      task_deps[i].dep_len        = 0;
      task_deps[i].access.version = 0;
      task_deps[i].access.task    = 0;
      task_deps[i].access.next    = task_deps[i].access.next_out = 0;
#endif
    }
    shift_noalias+=ndeps;

    for (i = 0; i < ndeps_noalias; i++)
    {
      komp_depend_info_t *cur_komp = &task_deps[shift_noalias + i];
      kmp_depend_info_t *cur_kmp = &noalias_dep_list[shift_noalias + i];
      cur_komp->access.data = cur_kmp->base_addr;
#if defined(KAAPI_DEBUG)
      cur_komp->dep_len        = 0;
      cur_komp->access.version = 0;
      cur_komp->access.task    = 0;
      cur_komp->access.next    = cur_komp->access.next_out = 0;
#endif
      if (cur_kmp->flags.in && cur_kmp->flags.out)
        cur_komp->mode = KOMP_ACCESS_MODE_RW;
      else if (cur_kmp->flags.in)
        cur_komp->mode = KOMP_ACCESS_MODE_R;
      else if (cur_kmp->flags.out)
        cur_komp->mode = KOMP_ACCESS_MODE_W;
      else
        kaapi_assert(0);
    }
  }

  int retval = komp_task_push(
    (struct komp_thread_t*)thread,
    __kmpc_wrapper_task,
    (void (*)())new_task->routine,
    kmptaskdata,
    1,
#if KOMP_USE_ICV
    input_flags->final
#else 
    0
#endif
  );
  TRACE_LEAVE();

  return retval;
}

/*
*/
kmp_int32 __kmpc_omp_task(ident_t *loc_ref,
                          kmp_int32 gtid,
                          kmp_task_t *new_task)
{
  TRACE_ENTER();
  int retval = __kmpc_omp_task_with_deps(
    loc_ref,
    gtid,
    new_task,
    0, 0, 0, 0 
  );
  TRACE_LEAVE();
  return retval;
}


/*
*/
kmp_int32 __kmpc_omp_taskwait(ident_t *loc_ref, kmp_int32 gtid)
{
  TRACE_ENTER();
  
  kaapi_sched_sync(kaapi_self_thread());
  
  TRACE_LEAVE();

  return 0;
}


/*
*/
kmp_int32 __kmpc_omp_taskyield( ident_t *loc_ref, kmp_int32 gtid, int end_part )
{
  TRACE_ENTER();
  TRACE_LEAVE();
  return 0;
}


/*
*/
__attribute__((constructor))
static void kmpc_kaapi_task_format_constructor(void)
{
  TRACE_ENTER();
  TRACE_LEAVE();
}

void __kmpc_omp_task_begin_if0(ident_t *loc_ref, kmp_int32 gtid, kmp_task_t * task)
{
  TRACE_ENTER();
  task->routine(kaapi_self_kid(), task);
  TRACE_LEAVE();
}

void __kmpc_omp_task_complete_if0(ident_t *loc_ref, kmp_int32 gtid, kmp_task_t *task)
{
  TRACE_ENTER();
  /* Nothing to do here. */
  TRACE_LEAVE();
}

