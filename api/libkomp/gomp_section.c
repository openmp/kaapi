/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "libgomp.h"

//#define TRACE 1

#ifdef TRACE
#include <stdio.h>
#define KOMP_TRACE_ENTER \
  printf("%i:: enter '%s'\n", kaapi_self_kid(), __PRETTY_FUNCTION__); fflush(stdout);
#define KOMP_TRACE_EXIT \
  printf("%i:: exit '%s'\n", kaapi_self_kid(), __PRETTY_FUNCTION__); fflush(stdout);
#else
#define KOMP_TRACE_ENTER
#define KOMP_TRACE_EXIT
#endif


unsigned int GOMP_sections_start (unsigned maxsec)
{
  unsigned int retval;
  long first, last;
  KOMP_TRACE_ENTER
  if (GOMP_loop_dynamic_start( 0, maxsec, 1, 1, &first, &last ))
    retval = (unsigned int)(1+first);
  else
    retval = 0;
  KOMP_TRACE_EXIT
  return retval;
}

unsigned int GOMP_sections_next (void)
{
  unsigned int retval;
  long first, last;
  KOMP_TRACE_ENTER
  if (GOMP_loop_dynamic_next( &first, &last ))
    retval = (unsigned int)(1+first);
  else
    retval = 0;
  KOMP_TRACE_EXIT
  return retval;
}

void GOMP_sections_end (void)
{
  KOMP_TRACE_ENTER
  GOMP_loop_end();
  KOMP_TRACE_EXIT
}

void GOMP_sections_end_nowait (void)
{
  KOMP_TRACE_ENTER
  GOMP_loop_end_nowait();
  KOMP_TRACE_EXIT
}


void GOMP_parallel_sections_start (
    void (*fn) (void *), 
    void *data,
    unsigned num_threads, 
    unsigned count
)
{
  KOMP_TRACE_ENTER
  GOMP_parallel_loop_dynamic_start(
    fn, 
    data,
    num_threads,
    0, 
    count,
    1,
    1
  );
  KOMP_TRACE_EXIT
}

void
GOMP_parallel_sections (void (*fn) (void *), void *data,
                        unsigned num_threads, unsigned count, unsigned flags)
{
  KOMP_TRACE_ENTER
  GOMP_parallel_sections_start(fn, data, num_threads, count );
  fn(data);
  GOMP_sections_end();
  GOMP_parallel_end(); 
  KOMP_TRACE_EXIT
}
