/*
 ** xkaapi
 ** 
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */

#include "libgomp.h"
#include <string.h>



/* This version pass to each created task a pointer to the parent context.
   It is correct because:
   - the parent thread will always waits for completion of the child tasks (XKaapi style of 
   execution)
   - a context has a scope at least equal to the task that creates it.
*/
typedef struct GOMP_trampoline_task_arg {
  komp_taskdata_t           data;
  void                     (*fn) (void *);
  void*                     fn_data;
} GOMP_trampoline_task_arg;



static void GOMP_trampoline_task_body(
  struct komp_task_t* task, komp_thread_t* thread
)
{
  GOMP_trampoline_task_arg* taskarg = (GOMP_trampoline_task_arg*)((komp_taskdata_t*)((kaapi_task_t*)task)->arg);
  taskarg->fn(taskarg->fn_data);
}

#define RDTSC_PUSH 0

#if RDTSC_PUSH
/*
*/
static inline uint64_t rdtsc(void)
{
  uint32_t lo,hi;
  __asm__ volatile ( "rdtsc" : "=a" ( lo ) , "=d" ( hi ) );
  return (uint64_t)hi << 32UL | lo;
}
extern uint64_t gomp_task_rdtsc;
uint64_t gomp_task_rdtsc = 0;
#if (RDTSC_PUSH>1)
extern uint64_t gomp_task_rdtsc_kaapi;
extern uint64_t gomp_task_rdtsc_komp;
extern uint64_t gomp_task_rdtsc_alloc;
extern uint64_t gomp_task_rdtsc_depend;
extern double gomp_task_delay;
extern double gomp_task_count;
double gomp_task_delay = 0.0;
double gomp_task_count = 0.0;
uint64_t gomp_task_rdtsc_kaapi = 0;
uint64_t gomp_task_rdtsc_alloc = 0;
uint64_t gomp_task_rdtsc_depend = 0;
uint64_t gomp_task_rdtsc_komp = 0;
#endif
#endif

//static int counter = 0;
void GOMP_task(
  void (*fn) (void *),
  void *data, 
  void (*cpyfn) (void *, void *),
  long arg_size, 
  long arg_align, 
  bool if_clause,
  unsigned flags,
  void **depend, /* new GOMP-4.0 */
  int priority   /* new GOMP-4.5 */
)
{
#if RDTSC_PUSH
double delay = - kaapi_get_elapsedtime();
uint64_t rd = rdtsc();
#endif
  size_t count_depend;
  size_t clo_size;
  kaapi_processor_t* kproc = komp_self_processor();
  kaapi_context_t* context = kproc->context;

  if (flags & GOMP_TASK_FLAG_DEPEND) /* depend clauses */
  {
    /* see libgomp, task.c  */
    count_depend = (uintptr_t)depend[0];
    /* depend[i+2] is the address on which to compute dependency */
  }
  else
    count_depend = 0;

  clo_size = sizeof(GOMP_trampoline_task_arg)+ arg_size + (sizeof(komp_depend_info_t)) * count_depend;

  GOMP_trampoline_task_arg* arg = kaapi_data_push_align( kaapi_context2thread(context),
                    (uint32_t)clo_size, (uintptr_t)arg_align);
  void* userarg = arg+1;

  if (cpyfn)
    cpyfn(userarg, data);
  else
    memcpy(userarg, data, arg_size);

#if (RDTSC_PUSH>1)
uint64_t rd0 = rdtsc();
#endif
  arg->fn             = fn;
  arg->fn_data        = userarg;

  arg->data.n_depend = 0;
  arg->data.depends  = 0;
  if (count_depend !=0) /* #pragma omp depend */
  {
    size_t i;
    size_t count_out = (uintptr_t) depend[1];
    arg->data.depends  = (komp_depend_info_t*)(arg_size + ((char*)userarg));
    arg->data.n_depend = (int)count_depend;
    for (i=0; i<count_depend; ++i)
    {
      komp_access_mode_t mode;
      if (i < count_out)
        mode = KOMP_ACCESS_MODE_RW; /* it is a READ WRITE in the sens of Kaapi */
      else
        mode = KOMP_ACCESS_MODE_R;
      arg->data.depends[i].access.data      = depend[i+2];
      arg->data.depends[i].access.version   = 0;
      arg->data.depends[i].mode             = mode; /* mode */
#if defined(KAAPI_DEBUG)
      arg->data.depends[i].dep_len        = 0;
      arg->data.depends[i].access.version = 0;
      arg->data.depends[i].access.task    = 0;
      arg->data.depends[i].access.next    = arg->data.depends[i].access.next_out = 0;
#endif
    }
  }

#if (RDTSC_PUSH>1)
uint64_t rd1 = rdtsc();
#endif
  komp_task_push(
    (komp_thread_t*)kaapi_context2thread(context),
    GOMP_trampoline_task_body,
    fn,
    &arg->data,
    1,
    flags & GOMP_TASK_FLAG_FINAL ? 1: 0
  );
#if RDTSC_PUSH
uint64_t rd2 = rdtsc();
gomp_task_rdtsc += rd2 - rd;
#if (RDTSC_PUSH >1)
gomp_task_rdtsc_kaapi += rd2 - rd1;
gomp_task_rdtsc_alloc += rd0 - rd;
gomp_task_rdtsc_depend += rd0 - rd1;
gomp_task_rdtsc_komp += rd2 - rd1;
delay += kaapi_get_elapsedtime();
gomp_task_delay += delay;
++gomp_task_count;
#endif
#endif
  return;
}

void GOMP_taskwait (void)
{
  kaapi_thread_t* thread = kaapi_self_thread();
  if (thread)
    kaapi_sched_sync(thread);
}

/* GOMP3.0 */
void GOMP_taskyield(void)
{
}


/* GOMP4.0 */
void
GOMP_taskgroup_start (void)
{
  komp_taskgroup_begin();
}


void
GOMP_taskgroup_end (void)
{
  komp_taskgroup_end();
}
