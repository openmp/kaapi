#include <stdint.h>
/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** francois.broquedis@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_LIBOMP_
#define _KAAPI_LIBOMP_

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
#include <stddef.h>

struct komp_team_t;
struct komp_thread_t;
struct komp_task_t;
struct komp_access_t;
struct komp_format_t;
struct kaapi_thread_t;
struct kaapi_context_t;

/** Initialization of the OpenMP common library
*/
extern void komp_init (void);

/** Finalisation of the OpenMP common library
*/
extern void komp_finalize (void);


/* Leave to concrete runtime to define lock (libgomp or libiomp)
*/
struct komp_lock_t;
struct komp_nest_lock_t;

/* Standard Open 4.0
   - loop scheduler. 5 & 6 are provided by Kaapi runtime 
   - same value as for kaapi_foreach_attr_policy_t
*/
typedef enum komp_sched_t
{
  komp_sched_static   = 1,
  komp_sched_dynamic  = 2,
  komp_sched_guided   = 3,
  komp_sched_auto     = 4,
  komp_sched_steal    = 5,
  komp_sched_adaptive = 6
} komp_sched_t;

/* Standard definition for proc_bind
*/
typedef enum komp_proc_bind_t {
  komp_proc_bind_end_list= -1, /* komp marker */
  komp_proc_bind_false  = 0,   /* kaapi/omp value */
  komp_proc_bind_true   = 1,   /* kaapi/omp value */
  komp_proc_bind_master = 2,   /* kaapi/omp value */
  komp_proc_bind_close  = 3,   /* kaapi/omp value */
  komp_proc_bind_spread = 4,   /* kaapi/omp value */

  komp_proc_bind_default= 5,   /* kaapi value */
} komp_proc_bind_t;

typedef enum komp_push_init_t {
  komp_init_runtime          = 0,
  komp_init_rand             = 1,
  komp_init_cyclic           = 2,
  komp_init_randnuma         = 3,
  komp_init_cyclicnuma       = 4,
  komp_init_cyclicnumastrict = 5,
} komp_push_init_t;

typedef enum komp_affinity_kind_t {
  komp_affinity_none        = 0,
  komp_affinity_depend      = 1,
  komp_affinity_numa        = 2,
  komp_affinity_core        = 3,
} komp_affinity_kind_t;

/* Intenal definition
*/
typedef enum komp_wait_policy_t {
  komp_wait_policy_active  = 0,
  komp_wait_policy_passive = 1
} komp_wait_policy_t;


/*
*/
typedef struct komp_taskgroup_t {
  struct komp_taskgroup_t* prev;
  void*                    frame;
  int                      cancelled;
} komp_taskgroup_t;


/* ICVs of scope "Device"
*/
typedef struct komp_device_icv_t {
  komp_sched_t       def_sched;
  unsigned long      stacksize;
  komp_wait_policy_t waitpolicy;
  int                max_active_levels;
  int8_t             cancel;
  komp_proc_bind_t*  binds;             /* list */
  unsigned int*      nthreads;          /* list defined by nthreads-var icv */
  int                max_task_priority;
} komp_device_icv_t;


/* libkomp extension for A. Roussel */
typedef struct komp_extradep_icv_t {
  int                 sizeext_mode_r;
  void**              ext_mode_r;
  int                 sizeext_mode_w;
  void**              ext_mode_w;
  int                 sizeext_mode_rw;
  void**              ext_mode_rw;
} komp_extradep_icv_t;

/* ICVs of scope "Data environment"
   The following fields are inherited at task creation time (explicit or implicit)
*/
typedef struct komp_dataenv_icv_t {
  unsigned int       h_nthreads; /* head of the list nthreads-var, depending of the level */
  komp_sched_t       run_sched;
  int                run_sched_modifier;
  int8_t             dyn;
  int8_t             nest;
  komp_proc_bind_t   h_bind;      /* head of the list */

  /* XKaapi extension */
  const char*         task_name;     /* omp_set_task_name */
  uint16_t            flag;          /* flag */
  union {
    void             *affinity;
    int               node_id;
    int               core_id;
    int               param_id;
  };
  uint8_t             prio;          /* support it as per task icv for omp_set_task_priority */

  /* libkomp extension for A. Roussel */
  komp_extradep_icv_t *extra_deps;
} komp_dataenv_icv_t;


/* to avoid global data definition */
typedef struct komp_globalenv_icv_t {
  komp_dataenv_icv_t icvs;
  unsigned int       thread_limit;
  int                def_device;
  unsigned int       *nthreads; /* list nthreads-var */
  komp_proc_bind_t   *binds;    /* list binds-var */
} komp_globalenv_icv_t;


/*
*/
extern komp_dataenv_icv_t* komp_self_icvs(void);

/*
*/
struct komp_thread_t* komp_self_thread(void);

/*
*/
struct komp_team_t* komp_self_team(void);

/* kaapi_memory_view_t
*/
typedef struct komp_memory_view_t {
  int           type;
  unsigned long offset;
  unsigned long size[2];
  unsigned long lda;
  unsigned long wordsize;
} komp_memory_view_t;


/* kaapi_memory_view_make1d
*/
static inline void komp_memory_view_make1d(
    komp_memory_view_t* view,
    unsigned long       offset,
    unsigned long       size,
    unsigned long       wordsize
)
{
  view->type     = 1;
  view->offset   = offset;
  view->size[0]  = size;
  view->wordsize = wordsize;
  view->size[1]  = 0;
  view->lda      = 0;
}

/* kaapi_memory_view_make2d
*/
static inline void komp_memory_view_make2d(
  komp_memory_view_t* view,
  unsigned long offset,
  unsigned long n,
  unsigned long m,
  unsigned long lda,
  unsigned long wordsize
)
{
  view->type     = 2;
  view->offset   = offset;
  view->size[0]  = n;
  view->size[1]  = m;
  view->lda      = lda;
  view->wordsize = wordsize;
}


/* kaapi_access_mode_t
*/
typedef enum komp_access_mode_t {
  KOMP_ACCESS_MODE_VOID= 0,        /* 0000 0000 : */
  KOMP_ACCESS_MODE_V   = 1,        /* 0000 0001 : */
  KOMP_ACCESS_MODE_R   = 2,        /* 0000 0010 : */
  KOMP_ACCESS_MODE_W   = 4,        /* 0000 0100 : */
  KOMP_ACCESS_MODE_CW  = 8,        /* 0000 1000 : */
  KOMP_ACCESS_MODE_S   = 16,       /* 0001 0000 : stack data */
  KOMP_ACCESS_MODE_T   = 32,       /* 0010 0000 : for Quark support: scratch mode or temporary */
  KOMP_ACCESS_MODE_P   = 64,       /* 0100 0000 : */
  KOMP_ACCESS_MODE_IP  = 128,      /* 1000 0000 : in place, for CW only */

  KOMP_ACCESS_MODE_RW  = KOMP_ACCESS_MODE_R|KOMP_ACCESS_MODE_W,
  KOMP_ACCESS_MODE_STACK = KOMP_ACCESS_MODE_S|KOMP_ACCESS_MODE_RW,
  KOMP_ACCESS_MODE_SCRATCH = KOMP_ACCESS_MODE_T|KOMP_ACCESS_MODE_V,
  KOMP_ACCESS_MODE_CWP = KOMP_ACCESS_MODE_P|KOMP_ACCESS_MODE_CW,
  KOMP_ACCESS_MODE_ICW = KOMP_ACCESS_MODE_IP|KOMP_ACCESS_MODE_CW
} komp_access_mode_t;


/* kaapi_access_t
*/
typedef struct komp_access_t {
  void*                            data;    /* global data */
  struct komp_access_t*            next;    /* next access */
  struct komp_task_t*              task;    /* the owner task */
  void*                            version; /* used from breaking dependencies */
  struct komp_access_t*            next_out;/* next access to activate */
//  double                           Tinf;     /* computed only if KAAPI_USE_PERFCOUNTER is set  */
//  void*                            perf;    /* unused except if KAAPI_USE_PERFCOUNTER is set */
} komp_access_t;


static inline void komp_access_init( komp_access_t* a, void* b)
{
  a->data = b;
  a->version = 0;
  a->task = 0;
}

static inline void* komp_access_getdata( komp_access_t* a )
{
  return a->data;
}


/* kaapi_atomic_t
*/
typedef struct komp_atomic_t {
  volatile int _counter;
} komp_atomic_t;

/* pointer to a kaapi_lock_t
*/
typedef struct komp_mutex_t {
  void* ptr;
} komp_mutex_t;

/*
*/
extern void komp_atomic_initlock( struct komp_mutex_t* );

/*
*/
extern void komp_atomic_destroylock( struct komp_mutex_t* );

/*
*/
extern void komp_atomic_lock( struct komp_mutex_t* l );

/*
*/
extern void komp_atomic_unlock( struct komp_mutex_t* l );

/* kaapi_task_body_t
*/
typedef void (*komp_task_body_t)( struct komp_task_t* /* task */,
                                  struct komp_thread_t* /* thread */);


/* Frame: where tasks are stored !
*/
typedef struct komp_thread_t {
  struct komp_task_t*   sp;
  char*                 sp_data;
} komp_thread_t;


/*
*/
extern komp_dataenv_icv_t* komp_swap_dataenv_icvs(
  struct komp_thread_t* thread,
  komp_dataenv_icv_t* newicvs
);


/* kaapi_data_push
*/
extern void* komp_data_push( komp_thread_t* th, unsigned long size );

/* Common taskdata type for both libgomp and libomp
*/
typedef struct {
  komp_access_t            access;
  komp_access_mode_t       mode;
  size_t                   dep_len; /* not yet used */
} komp_depend_info_t;

/*
*/
typedef struct {
#if KOMP_USE_ICV
  komp_dataenv_icv_t        icvs;
#endif
  uint32_t                  flags;   /* implementation iomp task_alloc/task */
  struct komp_taskgroup_t*  task_group;
  komp_task_body_t          wrapper; /* depend if gcc or iomp */
  uintptr_t                 size;

  union {
    void *                  affinity;
    int                     node_id;
    int                     core_id;
  };

  int                       n_depend;
  komp_depend_info_t*       depends;
#if KOMP_USE_ICV
  int                       extra_n_depend;
  komp_depend_info_t*       extra_depends;
#endif
} komp_taskdata_t;


/* kaapi_task_push
*/
extern int komp_task_push(
    komp_thread_t*   thread,
    komp_task_body_t body,
    void (*user_routine) (),
    komp_taskdata_t* data,
    int ifclause,
    int finalclause
);


/*
*/
extern void komp_taskgroup_begin(void);

/*
*/
extern void komp_taskgroup_end(void);

/*
*/
void komp_sched_sync( struct komp_thread_t* thread);

/*
*/
extern struct komp_format_t* komp_schar_format;
extern struct komp_format_t* komp_char_format;
extern struct komp_format_t* komp_shrt_format;
extern struct komp_format_t* komp_int_format;
extern struct komp_format_t* komp_long_format;
extern struct komp_format_t* komp_llong_format;
extern struct komp_format_t* komp_int8_format;
extern struct komp_format_t* komp_int16_format;
extern struct komp_format_t* komp_int32_format;
extern struct komp_format_t* komp_int64_format;
extern struct komp_format_t* komp_uchar_format;
extern struct komp_format_t* komp_ushrt_format;
extern struct komp_format_t* komp_uint_format;
extern struct komp_format_t* komp_ulong_format;
extern struct komp_format_t* komp_ullong_format;
extern struct komp_format_t* komp_uint8_format;
extern struct komp_format_t* komp_uint16_format;
extern struct komp_format_t* komp_uint32_format;
extern struct komp_format_t* komp_uint64_format;
extern struct komp_format_t* komp_flt_format;
extern struct komp_format_t* komp_dbl_format;
extern struct komp_format_t* komp_ldbl_format;
extern struct komp_format_t* komp_voidp_format;

/*
*/
struct komp_format_t* komp_format_allocate(void);

/*
*/
extern int komp_format_structregister(
  struct komp_format_t* fmt,
  const char*           name,
  unsigned long         size,
  void                 (*cstor)( void* ),
  void                 (*dstor)( void* ),
  void                 (*cstorcopy)( void*, const void*),
  void                 (*copy)( void*, const void*),
  void                 (*assign)( void*, const komp_memory_view_t*, const void*, const komp_memory_view_t*)
);

typedef void (*komp_fmt_fnc_get_name)(
  const struct komp_format_t*, const void*, char* buffer, int size
);

typedef int (*komp_fmt_fnc_get_affinity)(
  const struct komp_format_t*, const void*, struct komp_task_t*, uint16_t
);

typedef unsigned long (*komp_fmt_fnc_get_size)(const struct komp_format_t*, const void*);

typedef void (*komp_fmt_fnc_task_copy)(const struct komp_format_t*, void*, const void*);

typedef unsigned long (*komp_fmt_fnc_get_count_params)(const struct komp_format_t*, const void*);

typedef komp_access_mode_t (*komp_fmt_fnc_get_mode_param)(
    const struct komp_format_t*, unsigned int, const void*
);

typedef void* (*komp_fmt_fnc_get_data_param)(
    const struct komp_format_t*, unsigned int, const void*
);

typedef struct komp_access_t* (*komp_fmt_fnc_get_access_param)(
    const struct komp_format_t*, unsigned int, const void*
);

typedef void (*komp_fmt_fnc_set_access_param)(
    const struct komp_format_t*, unsigned int, void*, const struct komp_access_t*
);

typedef const struct komp_format_t*(*komp_fmt_fnc_get_fmt_param)(
    const struct komp_format_t*, unsigned int, const void*
);

typedef void (*komp_fmt_fnc_get_view_param)(
    const struct komp_format_t*, unsigned int, const void*, komp_memory_view_t*
);

typedef void (*komp_fmt_fnc_set_view_param)(
    const struct komp_format_t*, unsigned int, void*, const komp_memory_view_t*);

typedef void (*komp_fmt_fnc_reducor)(
    const struct komp_format_t*, unsigned int, void*, const void*);

typedef void (*komp_fmt_fnc_redinit)(
    const struct komp_format_t*, unsigned int, const void* sp, void*);

/* TO DO
*/
typedef int komp_adaptivetask_splitter_t;
typedef komp_adaptivetask_splitter_t	(*komp_fmt_fnc_get_splitter)(
    const struct komp_format_t*, const void*
);

/* kaapi_format_taskregister_func
*/
extern int komp_format_taskregister_func(
  struct komp_format_t*         fmt,
  void*                         key,
  komp_task_body_t              body,
  komp_task_body_t              bodywh,
  const char*                   name,
  komp_fmt_fnc_get_name         get_name,
  komp_fmt_fnc_get_size         get_size,
  komp_fmt_fnc_task_copy        task_copy,
  komp_fmt_fnc_get_count_params get_count_params,
  komp_fmt_fnc_get_mode_param   get_mode_param,
  komp_fmt_fnc_get_data_param   get_data_param,
  komp_fmt_fnc_get_access_param get_access_param,
  komp_fmt_fnc_set_access_param set_access_param,
  komp_fmt_fnc_get_fmt_param    get_fmt_param,
  komp_fmt_fnc_get_view_param   get_view_param,
  komp_fmt_fnc_set_view_param   set_view_param,
  komp_fmt_fnc_reducor          reducor,
  komp_fmt_fnc_redinit          redinit,
  komp_fmt_fnc_get_splitter	    get_splitter,
  komp_fmt_fnc_get_affinity	    get_affinity
);
    

/* Do not change value - see kaapi.h
*/
#define KOMP_PROC_TYPE_CPU     0x1
#define KOMP_PROC_TYPE_CUDA    0x2
#define KOMP_PROC_TYPE_MIC     0x3
extern int komp_format_taskregister_body(
    struct komp_format_t*         fmt,
    komp_task_body_t              body,
    int                           arch
);

/* Execute a parallel region with body the body of the region
   - similar interface than in Intel IOMP
*/
extern void komp_fork_call(
  unsigned int num_threads,
  komp_proc_bind_t procbind,
  void (*body)(void*),
  void* arg
);

/*
*/
extern void komp_mem_barrier(void);


/* Single
*/
extern int komp_single (struct komp_team_t* team);

/* Master
*/
extern int komp_master ( struct komp_team_t* team );

/* for #atomic
*/
extern int komp_lock ( struct komp_team_t* team );

/*
*/
extern int komp_unlock ( struct komp_team_t* team );

/* barrier 
*/
extern void komp_barrier( struct komp_team_t* team );

/* Loop support: definitions must correspond to kaapi_foreach_attr_policy_t definitions
*/
typedef enum {
  KOMP_FOREACH_SCHED_DEFAULT    = 0x00,  /* default: implementation defined */
  KOMP_FOREACH_SCHED_STATIC     = 0x01,  /* static distribution as OpenMP */
  KOMP_FOREACH_SCHED_DYNAMIC    = 0x02,  /* dynamic distribution as OpenMP */
  KOMP_FOREACH_SCHED_GUIDED     = 0x03,  /* guided distribution as OpenMP */
  KOMP_FOREACH_SCHED_AUTO       = 0x04,  /* auto strategy as OpenMP */
  KOMP_FOREACH_SCHED_STEAL      = 0x05,  /* steal half strategy */
  KOMP_FOREACH_SCHED_ADAPTIVE   = 0x06,  /* add to the steal strategy a distribution step */
  KOMP_FOREACH_SCHED_RUNTIME    = 0x10,  /* runtime strategy as OpenMP */
} komp_foreach_attr_policy_t;

/** \ingroup HWS
    hierarchy level identifiers
 */
typedef enum komp_hws_levelid
{
  KOMP_HWS_LEVELID_THREAD  = 0,
  KOMP_HWS_LEVELID_CORE    = 1,
  KOMP_HWS_LEVELID_NUMA    = 2,
  KOMP_HWS_LEVELID_SOCKET  = 3,
  KOMP_HWS_LEVELID_BOARD   = 4,
  KOMP_HWS_LEVELID_MACHINE = 5,
  KOMP_HWS_LEVELID_OFFLOAD_HOST = 6,
  KOMP_HWS_LEVELID_OFFLOAD_CUDA = 7,
  KOMP_HWS_LEVELID_OFFLOAD_MIC  = 8,
  KOMP_HWS_LEVELID_MAX
} komp_hws_levelid_t;

typedef struct komp_foreach_attr_t {
  unsigned int                init;      /* flags for each attributes == 1 iff initialize */
  unsigned long long          s_grain;   /* default = 1 */
  unsigned long long          p_grain;   /* default = 1 */
  komp_foreach_attr_policy_t  policy;    /* choose the policy for splitting */
  komp_hws_levelid_t          dist;      /* default == CORE */
  komp_hws_levelid_t          affinity;  /* default == MACHINE */
  int                         flag;      /* default == 0, bit0 = dist, bit1 = affinity */
} komp_foreach_attr_t;

/*
*/
static inline int komp_foreach_attr_init(komp_foreach_attr_t* attr)
{
  attr->init  = 0;
  return 0;
}

/*
*/
extern int komp_foreach_attr_set_policy(
  komp_foreach_attr_t* attr, 
  komp_foreach_attr_policy_t policy
);

/*
*/
extern int komp_foreach_attr_get_policy(
  const komp_foreach_attr_t* attr,
  komp_foreach_attr_policy_t* policy
);

/*
*/
extern int komp_foreach_attr_set_grains(
  komp_foreach_attr_t* attr, 
  unsigned long long s_grain,
  unsigned long long p_grain
);

/*
*/
extern int komp_foreach_attr_get_grains(
  const komp_foreach_attr_t* attr,
  unsigned long long* s_grain,
  unsigned long long* p_grain
);

/* Core of the given locality domain 'level' of the running thread steal iterations in priority.
   If strictness !=0 then only those cores can steal iterations.
*/
extern int komp_foreach_attr_set_localitydomain(
  komp_foreach_attr_t* attr, 
  komp_hws_levelid_t   level,
  int                  strict
);


/* Core of the given locality domain 'level' of the running thread steal iterations in priority.
   If strictness !=0 then only those cores can steal iterations.
*/
extern int komp_foreach_attr_get_localitydomain(
  const komp_foreach_attr_t* attr,
  komp_hws_levelid_t*        level,
  int*                       strict
);


/* Distribute the iteration over the localitydomain of the given 'level' :
   * distribute
*/
extern int komp_foreach_attr_set_distribution(
  komp_foreach_attr_t* attr, 
  komp_hws_levelid_t   level,
  int                  strict
);

/* Distribute the iteration over the localitydomain of the given 'level' :
   * distribute
*/
extern int komp_foreach_attr_get_distribution(
  const komp_foreach_attr_t* attr,
  komp_hws_levelid_t*        level,
  int*                       strict  
);



/*
*/
static inline int komp_foreach_attr_destroy(komp_foreach_attr_t* attr __attribute__((unused)))
{ return 0; }

/* Signature of foreach body 
   Called with (first, last, arg) in order to do
   computation over the range [first,last[.
*/
typedef void (*komp_foreach_body_t)(
    unsigned long long,  /* first */
    unsigned long long,  /* last */
    void* );

/** begin parallel region. Each thread start body(data). If body is 0, then 
    the main thread return will other threads start work stealer.
    if sync !=0 then wait for thread creation before returning
*/
extern int komp_begin_parallel(
  unsigned int num_threads,
  komp_proc_bind_t procbind,
  void (*body)(void*),
  void* data
);

/** End of parallel region
*/
extern int komp_end_parallel(void);

/* High level for each function.
   Execute body on each iteration i from lb up to ub (not included).
   In case of concurrency, then execution is performed in parallel.
   See documentation
   \retval 0: in case of success
   \retval EINVAL: invalid arguments, bounds may represent empty range, null function
*/
extern int komp_foreach(
  unsigned long long lb,
  unsigned long long ub,
  unsigned long long incr,
  int                sync,
  const komp_foreach_attr_t* attr,
  komp_foreach_body_t body,
  void* arg
);

/*
*/
extern int komp_loop_policy_start (
  komp_thread_t* thread,
  const komp_foreach_attr_t* attr,
  long long start,
  long long end,
  long long incr,
  long long *istart,
  long long *iend
);

extern int komp_loop_policy_start_ull (
  komp_thread_t* thread,
  const komp_foreach_attr_t* attr,
  int up,
  unsigned long long start,
  unsigned long long end,
  unsigned long long incr,
  unsigned long long *istart,
  unsigned long long *iend
);

/*
*/
extern int komp_loop_worknext(
  komp_thread_t* thread,
  long long *istart,
  long long *iend
);

/*
 */
extern int komp_loop_worknext_ull(
  komp_thread_t* thread,
  unsigned long long *istart,
  unsigned long long *iend
);

/*
*/
extern int komp_loop_workfini(
  komp_thread_t* thread
);


/* Ordered rt support
*/
extern void komp_ordered_start (void);
extern void komp_ordered_end (void);


/* For copy private
   - similar interface than in libGOMP
*/
extern void* komp_single_copy_start( struct komp_team_t* team );
extern void komp_single_copy_end( struct komp_team_t* team, void* data);


/* Function in OpenMP-4.0, p288 */
extern void komp_set_num_threads (int);
extern int komp_get_num_threads (void);
extern int komp_get_max_threads (void);
extern int komp_get_thread_num (void);
extern int komp_get_num_procs (void);

extern int komp_in_parallel (void);

extern void komp_set_dynamic (int);
extern int komp_get_dynamic (void);

extern void komp_set_nested (int);
extern int komp_get_nested (void);

extern int komp_get_cancellation(void);

extern void komp_set_schedule (komp_sched_t, int);
extern void komp_get_schedule (komp_sched_t *, int *);

extern int komp_get_thread_limit (void);
extern void komp_set_max_active_levels (int);
extern int komp_get_max_active_levels (void);
extern int komp_get_level (void);
extern int komp_get_ancestor_thread_num (int);
extern int komp_get_team_size (int);
extern int komp_get_active_level (void);
extern int komp_in_final(void);

extern komp_proc_bind_t komp_get_proc_bind(void);

extern void komp_set_default_device(int device_num);
extern int komp_get_default_device(void);
extern int komp_get_num_devices(void);
extern int komp_get_num_teams(void);
extern int komp_get_team_num(void);
extern int komp_is_initial_device(void);

extern void komp_init_lock(struct komp_lock_t *lock);
extern void komp_destroy_lock(struct komp_lock_t *lock);
extern void komp_set_lock(struct komp_lock_t *lock);
extern void komp_unset_lock(struct komp_lock_t *lock);
extern int  komp_test_lock(struct komp_lock_t *lock);

extern void komp_init_nest_lock(struct komp_nest_lock_t *lock);
extern void komp_destroy_nest_lock(struct komp_nest_lock_t *lock);
extern void komp_set_nest_lock(struct komp_nest_lock_t *lock);
extern void komp_unset_nest_lock(struct komp_nest_lock_t *lock);
extern int  komp_test_nest_lock(struct komp_nest_lock_t *lock);

extern double komp_get_wtime (void);
extern double komp_get_wtick (void);

extern void komp_set_datadistribution_bloccyclic( unsigned long long size, unsigned int length );
extern int komp_set_task_affinity(komp_affinity_kind_t kind, unsigned long long affinit, unsigned int strict);
extern void komp_begin_push_init(komp_push_init_t kind);
extern void komp_end_push_init();
extern void komp_begin_numa_init(void **select, void **push, void **push_init);
extern void komp_end_numa_init(void *select, void *push, void *push_init);
extern int komp_set_task_priority(int prio);
extern void komp_set_task_name( const char* name );

/* add dependencies onto next created task */
extern int komp_task_declare_dependencies( int mode, int count, void** array);


/* new to OpenMP 4.5 */
extern int komp_get_num_places(void);
extern int komp_get_place_num_procs(int place_num);
extern void komp_get_place_proc_ids(int place_num, int *ids);
extern int komp_get_place_num(void);
extern int komp_get_partition_num_places(void);
extern void komp_get_partition_place_nums(int *place_nums);
extern int komp_get_max_task_priority(void);

/* extension for libkomp */
extern int komp_get_num_locality_domains(void);
extern int komp_get_locality_domain_num(void);
extern int komp_get_locality_domain_num_for(void* addr);
extern int komp_locality_domain_get_num_cpu(int ldid);
extern int komp_locality_domain_bind_bloc1dcyclic(const void* addr, unsigned long size, unsigned long blocsize);
extern void* komp_locality_domain_allocate_bloc1dcyclic(unsigned long size, unsigned long blocsize);
extern int komp_locality_domain_bind_bloc2dcyclic(
  int nldx, int nldy,
  const void* addr, unsigned long n, unsigned long m, unsigned long ld, unsigned long wordsize,
  unsigned long bsx, unsigned long bsy
);
extern void* komp_locality_domain_allocate_bloc2dcyclic(
  int nldx, int nldy,
  unsigned long n, unsigned long m, unsigned long wordsize,
  unsigned long bsx, unsigned long bsy
);
extern void* komp_locality_domain_alloc(unsigned long size);
extern int komp_locality_domain_free(void* addr, unsigned long size);

#ifdef __cplusplus
}
#endif

#endif // #ifndef _KAAPI_LIBGOMP_
