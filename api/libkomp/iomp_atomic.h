/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012, 2013, 2014 INRIA.
**
** Contributors :
**
** francois.broquedis@imag.fr
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/

#ifndef ATOMIC_H
#define ATOMIC_H

#include <libiomp.h>
#include <stdio.h>

/* Used to implement operations kaapi (or gcc) does not provide
 * natively. */
static kaapi_lock_t global_atomic_lock = KAAPI_LOCK_INITIALIZER;

#define __mul(lhs, rhs)                          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) *= (rhs);                             \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __div(lhs, rhs)                          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) /= (rhs);                             \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __shl(lhs, rhs)                          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) = *(lhs) << (rhs);                    \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __shr(lhs, rhs)                          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) = *(lhs) >> (rhs);                    \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __andl(lhs, rhs)                         \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) = *(lhs) && (rhs);                    \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __andb(lhs, rhs)                         \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) = *(lhs) & (rhs);                     \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __orl(lhs, rhs)                          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) = *(lhs) || (rhs);                    \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __orb(lhs, rhs)                          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) = *(lhs) | (rhs);                     \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __xor(lhs, rhs)                          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) = ~(rhs);                             \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __min(lhs, rhs)                          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) = *(lhs) < (rhs) ? *(lhs) : (rhs);    \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __max(lhs, rhs)                          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) = *(lhs) > (rhs) ? *(lhs) : (rhs);    \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __eqv(lhs, rhs)                          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) = (*(lhs) == (rhs));                  \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __neqv(lhs, rhs)                         \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    *(lhs) = (*(lhs) != (rhs));                  \
    kaapi_atomic_unlock(&global_atomic_lock);    \
}                                                \

#define __mul_cpt(type, lhs, rhs, flag)          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) *= (rhs);                             \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __div_cpt(type, lhs, rhs, flag)          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) /= (rhs);                             \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __andl_cpt(type, lhs, rhs, flag)         \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = *(lhs) && (rhs);                    \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __andb_cpt(type, lhs, rhs, flag)         \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = *(lhs) & (rhs);                     \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __shl_cpt(type, lhs, rhs, flag)          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = *(lhs) << (rhs);                    \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __shr_cpt(type, lhs, rhs, flag)          \
    {                                            \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = *(lhs) >> (rhs);                    \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __orl_cpt(type, lhs, rhs, flag)          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = *(lhs) || (rhs);                    \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __orb_cpt(type, lhs, rhs, flag)          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = *(lhs) | (rhs);                     \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __xor_cpt(type, lhs, rhs, flag)          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = ~(rhs);                             \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __min_cpt(type, lhs, rhs, flag)          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = *(lhs) < (rhs) ? *(lhs) : (rhs);    \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __max_cpt(type, lhs, rhs, flag)          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = *(lhs) > (rhs) ? *(lhs) : (rhs);    \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __eqv_cpt(type, lhs, rhs, flag)          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = (*(lhs) == (rhs));                  \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __neqv_cpt(type, lhs, rhs, flag)         \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = (*(lhs) != (rhs));                  \
    if ((flag))                                  \
        ret = *(lhs);                            \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#define __swap(type, lhs, rhs)          \
{                                                \
    kaapi_atomic_lock(&global_atomic_lock);      \
    type ret = *(lhs);                           \
    *(lhs) = (rhs);                             \
    kaapi_atomic_unlock(&global_atomic_lock);    \
    return ret;                                  \
}                                                \

#endif /* ATOMIC_H */
