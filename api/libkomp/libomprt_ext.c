/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "omp_ext.h"
#include <stdio.h>
#include <stdlib.h>

# define komp_symver(fn) \
  __asm (".symver " #fn ", " #fn "@@KOMP_1.0");

#if defined(__linux__)
#  define WEAK_SYMBOL __attribute__((weak))
#elif defined(__APPLE__)
#  define WEAK_SYMBOL __attribute__((weak_import))
#else
#  warning "Unkown how to define weak symbol"
#endif

WEAK_SYMBOL void komp_set_datadistribution_bloccyclic( unsigned long long size, unsigned int length );

void 
omp_set_datadistribution_bloccyclic( unsigned long long size, unsigned int length )
{
  printf("In omp_set_datadistribution_bloccyclic\n");
  if (komp_set_datadistribution_bloccyclic != 0)
  {
    printf("In omp_set_datadistribution_bloccyclic: found symbol !!!\n");
    komp_set_datadistribution_bloccyclic( size, length );
  }
}


/*
*/
WEAK_SYMBOL int komp_set_task_affinity(int kind, unsigned long long affinit, unsigned int strict );
int omp_set_task_affinity(int ldid, int strict )
{
  if (komp_set_task_affinity !=0)
    return komp_set_task_affinity(2 /*komp_affinity_numa*/, ldid, strict);
  return 0;
}

/*
*/
WEAK_SYMBOL void komp_begin_numa_init(void **select, void **push, void **push_init);
void omp_begin_numa_init(void **select, void **push, void **push_init)
{
  if (komp_begin_numa_init != 0)
	komp_begin_numa_init(select, push, push_init);
}

/*
*/
WEAK_SYMBOL void komp_end_numa_init(void *select, void *push, void *push_init);
void omp_end_numa_init(void *select, void *push, void *push_init)
{
  if (komp_end_numa_init != 0)
	komp_end_numa_init(select, push, push_init);
}

/*
*/
WEAK_SYMBOL int komp_set_task_priority(int prio);
int omp_set_task_priority(int prio)
{
  if (komp_set_task_priority !=0)
    return komp_set_task_priority(prio);
  return 0;
}



/*
*/
WEAK_SYMBOL void komp_set_task_name(const char* name);
void omp_set_task_name(const char* name)
{
  if (komp_set_task_name !=0)
    komp_set_task_name(name);
}

/*
*/
WEAK_SYMBOL int komp_get_num_locality_domains(void);
int omp_get_num_locality_domains(void)
{
  if (komp_get_num_locality_domains !=0)
    return komp_get_num_locality_domains();
  return 1;
}

/*
*/
WEAK_SYMBOL int komp_get_locality_domain_num(void);
int omp_get_locality_domain_num(void)
{
  if (komp_get_locality_domain_num !=0)
    return komp_get_locality_domain_num();

  return 0;
}


/*
*/
WEAK_SYMBOL int komp_get_locality_domain_num_for(void*);
int omp_get_locality_domain_num_for(void* addr)
{
  if ( komp_get_locality_domain_num_for!=0)
    return komp_get_locality_domain_num_for(addr);
  return 0;
}

/*
*/
WEAK_SYMBOL int komp_locality_domain_get_num_cpu(int ldid);
int omp_locality_domain_get_num_cpu(int ldid)
{
  if (komp_locality_domain_get_num_cpu !=0)
    return komp_locality_domain_get_num_cpu(ldid);

  return -1;
}

/*
*/
WEAK_SYMBOL int komp_locality_domain_bind_bloc1dcyclic(const void* addr, size_t size, size_t blocsize);
int omp_locality_domain_bind_bloc1dcyclic(const void* addr, size_t size, size_t blocsize)
{
  if (komp_locality_domain_bind_bloc1dcyclic !=0)
    return komp_locality_domain_bind_bloc1dcyclic(addr, size, blocsize);
  return 0;
}

/*
*/
WEAK_SYMBOL void* komp_locality_domain_allocate_bloc1dcyclic(size_t size, size_t blocsize);
void* omp_locality_domain_allocate_bloc1dcyclic(size_t size, size_t blocsize)
{
  if (komp_locality_domain_allocate_bloc1dcyclic !=0)
    return komp_locality_domain_allocate_bloc1dcyclic(size, blocsize);
  return malloc(size);
}

/*
*/
WEAK_SYMBOL int komp_locality_domain_bind_bloc2dcyclic(
  int nldx, int nldy,
  const void* addr, size_t n, size_t m, size_t ld, size_t wordsize,
  size_t bsx, size_t bsy
);
int omp_locality_domain_bind_bloc2dcyclic(
  int nldx, int nldy,
  const void* addr, size_t n, size_t m, size_t ld, size_t wordsize,
  size_t bsx, size_t bsy
)
{
  if (komp_locality_domain_bind_bloc2dcyclic !=0)
    return komp_locality_domain_bind_bloc2dcyclic( nldx, nldy, addr, n, m, ld, wordsize, bsx, bsy );
  return 0;
}
//komp_symver(omp_locality_domain_bind_bloc2dcyclic)

/*
*/
WEAK_SYMBOL void* komp_locality_domain_allocate_bloc2dcyclic(
  int nldx, int nldy,
  size_t n, size_t m, size_t wordsize,
  size_t bsx, size_t bsy
);
void* omp_locality_domain_allocate_bloc2dcyclic(
  int nldx, int nldy,
  size_t n, size_t m, size_t wordsize,
  size_t bsx, size_t bsy
)
{
  if (komp_locality_domain_allocate_bloc2dcyclic !=0)
    return komp_locality_domain_allocate_bloc2dcyclic( nldx, nldy, n, m, wordsize, bsx, bsy );
  return malloc(n*m*wordsize);
}

/*
*/
WEAK_SYMBOL void* komp_locality_domain_alloc(size_t size);
void* omp_locality_domain_alloc(size_t size)
{
  if (komp_locality_domain_alloc !=0)
    return komp_locality_domain_alloc(size);
  return malloc(size);
}

/*
*/
WEAK_SYMBOL int komp_locality_domain_free(void* addr, size_t size);
int omp_locality_domain_free(void* addr, size_t size)
{
  if (komp_locality_domain_free !=0)
    return komp_locality_domain_free(addr, size);
  free(addr);
  return 0;
}


WEAK_SYMBOL int komp_task_declare_dependencies( int mode, int count, void** array);
int omp_task_declare_dependencies( int mode, int count, void** array)
{
  if (komp_task_declare_dependencies !=0)
    return komp_task_declare_dependencies( mode, count, array );
  return 0;
}



