/*
** xkaapi
**
**
** Copyright 2009,2010,2011,2012, 2013, 2014 INRIA.
**
** Contributors :
**
** francois.broquedis@imag.fr
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
**
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL http://www.cecill.info.
**
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
**
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
**
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
**
*/

#include "iomp_atomic.h"

/* These functions are executed only when the compiler is not able to
 * inline atomic operations. */

void __kmpc_atomic_fixed1_add(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_ADD((kaapi_atomic8_t *)lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_add_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    int8_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_ADD((kaapi_atomic8_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_ADD_ORIG((kaapi_atomic8_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed1_sub(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_SUB((kaapi_atomic8_t *)lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_sub_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    int8_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_SUB((kaapi_atomic8_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_SUB_ORIG((kaapi_atomic8_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed1_mul(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __mul(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_mul_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __mul_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_div(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __div(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_div_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __div_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_max(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __max(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_max_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __max_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_min(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __min(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_min_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __min_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_eqv(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __eqv(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_eqv_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __eqv_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_neqv(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __neqv(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_neqv_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __neqv_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_andb(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __andb(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_andb_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andb_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_shl(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __shl(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_shl_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shl_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_shr(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __shr(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_shr_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shr_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_orb(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __orb(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_orb_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orb_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_xor(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __xor(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_xor_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __xor_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_andl(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __andl(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_andl_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andl_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1_orl(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    __orl(lhs, rhs);
    TRACE_LEAVE();
}

int8_t __kmpc_atomic_fixed1_orl_cpt(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orl_cpt(int8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_add(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_ADD((kaapi_atomic8_t *)lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_add_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    uint8_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_ADD((kaapi_atomic8_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_ADD_ORIG((kaapi_atomic8_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed1u_sub(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_SUB((kaapi_atomic8_t *)lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_sub_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    uint8_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_SUB((kaapi_atomic8_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_SUB_ORIG((kaapi_atomic8_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed1u_mul(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __mul(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_mul_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __mul_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_div(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __div(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_div_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __div_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_max(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __max(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_max_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __max_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_min(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __min(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_min_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __min_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_eqv(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __eqv(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_eqv_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __eqv_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_neqv(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __neqv(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_neqv_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __neqv_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_andb(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __andb(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_andb_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andb_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_shl(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __shl(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_shl_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shl_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_shr(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __shr(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_shr_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shr_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_orb(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __orb(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_orb_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orb_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_xor(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __xor(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_xor_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __xor_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_andl(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __andl(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_andl_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andl_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed1u_orl(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    __orl(lhs, rhs);
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_orl_cpt(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orl_cpt(uint8_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_add(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_ADD((kaapi_atomic16_t *)lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_add_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    int16_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_ADD((kaapi_atomic16_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_ADD_ORIG((kaapi_atomic16_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed2_sub(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_SUB((kaapi_atomic16_t *)lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_sub_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    int16_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_SUB((kaapi_atomic16_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_SUB_ORIG((kaapi_atomic16_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed2_mul(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __mul(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_mul_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __mul_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_div(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __div(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_div_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __div_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_max(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __max(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_max_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __max_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_min(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __min(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_min_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __min_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_eqv(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __eqv(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_eqv_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __eqv_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_neqv(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __neqv(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_neqv_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __neqv_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_andb(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __andb(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_andb_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andb_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_shl(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __shl(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_shl_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shl_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_shr(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __shr(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_shr_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shr_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_orb(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __orb(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_orb_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orb_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_xor(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __xor(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_xor_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __xor_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_andl(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __andl(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_andl_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andl_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2_orl(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    __orl(lhs, rhs);
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_orl_cpt(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orl_cpt(int16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_add(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_ADD((kaapi_atomic16_t *)lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_add_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    uint16_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_ADD((kaapi_atomic16_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_ADD_ORIG((kaapi_atomic16_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed2u_sub(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_SUB((kaapi_atomic16_t *)lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_sub_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    uint16_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_SUB((kaapi_atomic16_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_SUB_ORIG((kaapi_atomic16_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed2u_mul(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __mul(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_mul_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __mul_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_div(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __div(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_div_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __div_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_max(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __max(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_max_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __max_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_min(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __min(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_min_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __min_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_eqv(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __eqv(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_eqv_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __eqv_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_neqv(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __neqv(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_neqv_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __neqv_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_andb(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __andb(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_andb_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andb_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_shl(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __shl(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_shl_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shl_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_shr(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __shr(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_shr_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shr_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_orb(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __orb(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_orb_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orb_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_xor(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __xor(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_xor_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __xor_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_andl(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __andl(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_andl_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andl_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed2u_orl(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    __orl(lhs, rhs);
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_orl_cpt(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orl_cpt(uint16_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_add(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_ADD((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_add_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    int32_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_ADD((kaapi_atomic32_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_ADD_ORIG((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed4_sub(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_SUB((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_sub_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    int32_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_SUB((kaapi_atomic32_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_SUB_ORIG((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed4_mul(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __mul(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_mul_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __mul_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_div(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __div(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_div_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __div_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_max(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __max(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_max_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __max_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_min(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __min(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_min_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __min_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_eqv(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __eqv(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_eqv_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __eqv_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_neqv(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __neqv(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_neqv_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __neqv_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_andb(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __andb(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_andb_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andb_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_shl(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __shl(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_shl_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shl_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_shr(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __shr(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_shr_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shr_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_orb(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __orb(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_orb_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orb_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_xor(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __xor(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_xor_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __xor_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_andl(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __andl(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_andl_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andl_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4_orl(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    __orl(lhs, rhs);
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_orl_cpt(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orl_cpt(int32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_add(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_ADD((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_add_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    uint32_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_ADD((kaapi_atomic32_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_ADD_ORIG((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed4u_sub(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_SUB((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_sub_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    uint32_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_SUB((kaapi_atomic32_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_SUB_ORIG((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed4u_mul(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __mul(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_mul_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __mul_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_div(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __div(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_div_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __div_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_max(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __max(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_max_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __max_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_min(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __min(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_min_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __min_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_eqv(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __eqv(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_eqv_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __eqv_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_neqv(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __neqv(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_neqv_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __neqv_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_andb(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __andb(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_andb_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andb_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_shl(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __shl(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_shl_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shl_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_shr(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __shr(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_shr_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shr_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_orb(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __orb(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_orb_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orb_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_xor(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __xor(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_xor_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __xor_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_andl(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __andl(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_andl_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andl_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed4u_orl(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    __orl(lhs, rhs);
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_orl_cpt(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orl_cpt(uint32_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_add(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_ADD((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_add_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    int64_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_ADD((kaapi_atomic64_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_ADD_ORIG((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed8_sub(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_SUB((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_sub_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    int64_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_SUB((kaapi_atomic64_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_SUB_ORIG((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed8_mul(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __mul(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_mul_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __mul_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_div(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __div(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_div_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __div_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_max(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __max(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_max_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __max_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_min(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __min(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_min_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __min_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_eqv(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __eqv(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_eqv_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __eqv_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_neqv(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __neqv(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_neqv_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __neqv_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_andb(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __andb(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_andb_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andb_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_shl(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __shl(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_shl_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shl_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_shr(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __shr(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_shr_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shr_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_orb(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __orb(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_orb_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orb_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_xor(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __xor(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_xor_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __xor_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_andl(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __andl(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_andl_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andl_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8_orl(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    __orl(lhs, rhs);
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_orl_cpt(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orl_cpt(int64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_add(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_ADD((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_add_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    uint64_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_ADD((kaapi_atomic64_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_ADD_ORIG((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed8u_sub(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_SUB((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_sub_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    uint64_t ret;
    if (flag)
        ret = KAAPI_ATOMIC_SUB((kaapi_atomic64_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_SUB_ORIG((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_fixed8u_mul(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __mul(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_mul_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __mul_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_div(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __div(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_div_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __div_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_max(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __max(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_max_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __max_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_min(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __min(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_min_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __min_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_eqv(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __eqv(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_eqv_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __eqv_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_neqv(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __neqv(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_neqv_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __neqv_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_andb(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __andb(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_andb_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andb_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_shl(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __shl(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_shl_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shl_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_shr(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __shr(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_shr_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __shr_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_orb(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __orb(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_orb_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orb_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_xor(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __xor(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_xor_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __xor_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_andl(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __andl(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_andl_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __andl_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_fixed8u_orl(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    __orl(lhs, rhs);
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_orl_cpt(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __orl_cpt(uint64_t, lhs, rhs, flag);
}

void __kmpc_atomic_float4_add(ident_t *id_ref, int gtid, float *lhs, float rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_ADD((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
}

float __kmpc_atomic_float4_add_cpt(ident_t *id_ref, int gtid, float *lhs, float rhs, int flag) {
    TRACE_ENTER();
    float ret;
    if (flag)
        ret = KAAPI_ATOMIC_ADD((kaapi_atomic32_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_ADD_ORIG((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_float4_sub(ident_t *id_ref, int gtid, float *lhs, float rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_SUB((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
}

float __kmpc_atomic_float4_sub_cpt(ident_t *id_ref, int gtid, float *lhs, float rhs, int flag) {
    TRACE_ENTER();
    float ret;
    if (flag)
        ret = KAAPI_ATOMIC_SUB((kaapi_atomic32_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_SUB_ORIG((kaapi_atomic32_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_float4_mul(ident_t *id_ref, int gtid, float *lhs, float rhs) {
    TRACE_ENTER();
    __mul(lhs, rhs);
    TRACE_LEAVE();
}

float __kmpc_atomic_float4_mul_cpt(ident_t *id_ref, int gtid, float *lhs, float rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __mul_cpt(float, lhs, rhs, flag);
}

void __kmpc_atomic_float4_div(ident_t *id_ref, int gtid, float *lhs, float rhs) {
    TRACE_ENTER();
    __div(lhs, rhs);
    TRACE_LEAVE();
}

float __kmpc_atomic_float4_div_cpt(ident_t *id_ref, int gtid, float *lhs, float rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __div_cpt(float, lhs, rhs, flag);
}

void __kmpc_atomic_float4_max(ident_t *id_ref, int gtid, float *lhs, float rhs) {
    TRACE_ENTER();
    __max(lhs, rhs);
    TRACE_LEAVE();
}

float __kmpc_atomic_float4_max_cpt(ident_t *id_ref, int gtid, float *lhs, float rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __max_cpt(float, lhs, rhs, flag);
}

void __kmpc_atomic_float4_min(ident_t *id_ref, int gtid, float *lhs, float rhs) {
    TRACE_ENTER();
    __min(lhs, rhs);
    TRACE_LEAVE();
}

float __kmpc_atomic_float4_min_cpt(ident_t *id_ref, int gtid, float *lhs, float rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __min_cpt(float, lhs, rhs, flag);
}

void __kmpc_atomic_float4_eqv(ident_t *id_ref, int gtid, float *lhs, float rhs) {
    TRACE_ENTER();
    __eqv(lhs, rhs);
    TRACE_LEAVE();
}

float __kmpc_atomic_float4_eqv_cpt(ident_t *id_ref, int gtid, float *lhs, float rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __eqv_cpt(float, lhs, rhs, flag);
}

void __kmpc_atomic_float4_neqv(ident_t *id_ref, int gtid, float *lhs, float rhs) {
    TRACE_ENTER();
    __neqv(lhs, rhs);
    TRACE_LEAVE();
}

float __kmpc_atomic_float4_neqv_cpt(ident_t *id_ref, int gtid, float *lhs, float rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __neqv_cpt(float, lhs, rhs, flag);
}

void __kmpc_atomic_float8_add(ident_t *id_ref, int gtid, double *lhs, double rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_ADD((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
}

double __kmpc_atomic_float8_add_cpt(ident_t *id_ref, int gtid, double *lhs, double rhs, int flag) {
    TRACE_ENTER();
    double ret;
    if (flag)
        ret = KAAPI_ATOMIC_ADD((kaapi_atomic64_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_ADD_ORIG((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_float8_sub(ident_t *id_ref, int gtid, double *lhs, double rhs) {
    TRACE_ENTER();
    KAAPI_ATOMIC_SUB((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
}

double __kmpc_atomic_float8_sub_cpt(ident_t *id_ref, int gtid, double *lhs, double rhs, int flag) {
    TRACE_ENTER();
    double ret;
    if (flag)
        ret = KAAPI_ATOMIC_SUB((kaapi_atomic64_t *)lhs, rhs);
    else
        ret = KAAPI_ATOMIC_SUB_ORIG((kaapi_atomic64_t *)lhs, rhs);
    TRACE_LEAVE();
    return ret;
}
void __kmpc_atomic_float8_mul(ident_t *id_ref, int gtid, double *lhs, double rhs) {
    TRACE_ENTER();
    __mul(lhs, rhs);
    TRACE_LEAVE();
}

double __kmpc_atomic_float8_mul_cpt(ident_t *id_ref, int gtid, double *lhs, double rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __mul_cpt(double, lhs, rhs, flag);
}

void __kmpc_atomic_float8_div(ident_t *id_ref, int gtid, double *lhs, double rhs) {
    TRACE_ENTER();
    __div(lhs, rhs);
    TRACE_LEAVE();
}

double __kmpc_atomic_float8_div_cpt(ident_t *id_ref, int gtid, double *lhs, double rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __div_cpt(double, lhs, rhs, flag);
}

void __kmpc_atomic_float8_max(ident_t *id_ref, int gtid, double *lhs, double rhs) {
    TRACE_ENTER();
    __max(lhs, rhs);
    TRACE_LEAVE();
}

double __kmpc_atomic_float8_max_cpt(ident_t *id_ref, int gtid, double *lhs, double rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __max_cpt(double, lhs, rhs, flag);
}

void __kmpc_atomic_float8_min(ident_t *id_ref, int gtid, double *lhs, double rhs) {
    TRACE_ENTER();
    __min(lhs, rhs);
    TRACE_LEAVE();
}

double __kmpc_atomic_float8_min_cpt(ident_t *id_ref, int gtid, double *lhs, double rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __min_cpt(double, lhs, rhs, flag);
}

void __kmpc_atomic_float8_eqv(ident_t *id_ref, int gtid, double *lhs, double rhs) {
    TRACE_ENTER();
    __eqv(lhs, rhs);
    TRACE_LEAVE();
}

double __kmpc_atomic_float8_eqv_cpt(ident_t *id_ref, int gtid, double *lhs, double rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __eqv_cpt(double, lhs, rhs, flag);
}

void __kmpc_atomic_float8_neqv(ident_t *id_ref, int gtid, double *lhs, double rhs) {
    TRACE_ENTER();
    __neqv(lhs, rhs);
    TRACE_LEAVE();
}

double __kmpc_atomic_float8_neqv_cpt(ident_t *id_ref, int gtid, double *lhs, double rhs, int flag) {
    TRACE_ENTER();
    TRACE_LEAVE();
    __neqv_cpt(double, lhs, rhs, flag);
}

/* Be carrefull: all this implementations assume right alignment of data to be atomic
*/
int8_t __kmpc_atomic_fixed1_rd(ident_t *id_ref, int gtid, int8_t *loc) {
    TRACE_ENTER();
    return *loc;
    TRACE_LEAVE();
}

void __kmpc_atomic_fixed1_wr(ident_t *id_ref, int gtid, int8_t *lhs, int8_t rhs) {
    TRACE_ENTER();
    *lhs = rhs;
    TRACE_LEAVE();
}

uint8_t __kmpc_atomic_fixed1u_rd(ident_t *id_ref, int gtid, uint8_t *loc) {
    TRACE_ENTER();
    return *loc;
    TRACE_LEAVE();
}

void __kmpc_atomic_fixed1u_wr(ident_t *id_ref, int gtid, uint8_t *lhs, uint8_t rhs) {
    TRACE_ENTER();
    *lhs = rhs;
    TRACE_LEAVE();
}

int16_t __kmpc_atomic_fixed2_rd(ident_t *id_ref, int gtid, int16_t *loc) {
    TRACE_ENTER();
    return *loc;
    TRACE_LEAVE();
}

void __kmpc_atomic_fixed2_wr(ident_t *id_ref, int gtid, int16_t *lhs, int16_t rhs) {
    TRACE_ENTER();
    *lhs = rhs;
    TRACE_LEAVE();
}

uint16_t __kmpc_atomic_fixed2u_rd(ident_t *id_ref, int gtid, uint16_t *loc) {
    TRACE_ENTER();
    return *loc;
    TRACE_LEAVE();
}

void __kmpc_atomic_fixed2u_wr(ident_t *id_ref, int gtid, uint16_t *lhs, uint16_t rhs) {
    TRACE_ENTER();
    *lhs = rhs;
    TRACE_LEAVE();
}

int32_t __kmpc_atomic_fixed4_rd(ident_t *id_ref, int gtid, int32_t *loc) {
    TRACE_ENTER();
    return *loc;
    TRACE_LEAVE();
}

void __kmpc_atomic_fixed4_wr(ident_t *id_ref, int gtid, int32_t *lhs, int32_t rhs) {
    TRACE_ENTER();
    *lhs = rhs;
    TRACE_LEAVE();
}

uint32_t __kmpc_atomic_fixed4u_rd(ident_t *id_ref, int gtid, uint32_t *loc) {
    TRACE_ENTER();
    return *loc;
    TRACE_LEAVE();
}

void __kmpc_atomic_fixed4u_wr(ident_t *id_ref, int gtid, uint32_t *lhs, uint32_t rhs) {
    TRACE_ENTER();
    *lhs = rhs;
    TRACE_LEAVE();
}

int64_t __kmpc_atomic_fixed8_rd(ident_t *id_ref, int gtid, int64_t *loc) {
    TRACE_ENTER();
    return *loc;
    TRACE_LEAVE();
}

void __kmpc_atomic_fixed8_wr(ident_t *id_ref, int gtid, int64_t *lhs, int64_t rhs) {
    TRACE_ENTER();
    *lhs = rhs;
    TRACE_LEAVE();
}

uint64_t __kmpc_atomic_fixed8u_rd(ident_t *id_ref, int gtid, uint64_t *loc) {
    TRACE_ENTER();
    return *loc;
    TRACE_LEAVE();
}

void __kmpc_atomic_fixed8u_wr(ident_t *id_ref, int gtid, uint64_t *lhs, uint64_t rhs) {
    TRACE_ENTER();
    *lhs = rhs;
    TRACE_LEAVE();
}

float __kmpc_atomic_float4_rd(ident_t *id_ref, int gtid, float *loc) {
    TRACE_ENTER();
    return *loc;
    TRACE_LEAVE();
}

void __kmpc_atomic_float4_wr(ident_t *id_ref, int gtid, float *lhs, float rhs) {
    TRACE_ENTER();
    *lhs = rhs;
    TRACE_LEAVE();
}

double __kmpc_atomic_float8_rd(ident_t *id_ref, int gtid, double *loc) {
    TRACE_ENTER();
    return *loc;
    TRACE_LEAVE();
}

void __kmpc_atomic_float8_wr(ident_t *id_ref, int gtid, double *lhs, double rhs) {
    TRACE_ENTER();
    *lhs = rhs;
    TRACE_LEAVE();
}


char __kmpc_atomic_fixed1_swp(  ident_t *id_ref, int gtid, char        * lhs, char        rhs )
{
    TRACE_ENTER();
    __swap( char, lhs, rhs );
    TRACE_LEAVE();
}

short __kmpc_atomic_fixed2_swp(  ident_t *id_ref, int gtid, short       * lhs, short       rhs )
{
    TRACE_ENTER();
    __swap( short, lhs, rhs );
    TRACE_LEAVE();
}

kmp_int32 __kmpc_atomic_fixed4_swp(  ident_t *id_ref, int gtid, kmp_int32   * lhs, kmp_int32   rhs )
{
    TRACE_ENTER();
    __swap( kmp_int32, lhs, rhs );
    TRACE_LEAVE();
}

kmp_int64 __kmpc_atomic_fixed8_swp(  ident_t *id_ref, int gtid, kmp_int64   * lhs, kmp_int64   rhs )
{
    TRACE_ENTER();
    __swap( kmp_int64, lhs, rhs );
    TRACE_LEAVE();
}

float __kmpc_atomic_float4_swp(  ident_t *id_ref, int gtid, float       * lhs, float  rhs )
{
    TRACE_ENTER();
    __swap( float, lhs, rhs );
    TRACE_LEAVE();
}

double __kmpc_atomic_float8_swp(  ident_t *id_ref, int gtid, double      * lhs, double  rhs )
{
    TRACE_ENTER();
    __swap( double, lhs, rhs );
    TRACE_LEAVE();
}



