/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "libgomp.h"


/* 
*/
#define GOMP_LOOP(GS, KS) \
bool GOMP_loop_ull_##GS##_start ( \
          bool up, \
          unsigned long long start, \
          unsigned long long end, \
          unsigned long long incr, \
          unsigned long long chunk_size,\
          unsigned long long *istart, \
          unsigned long long *iend\
)\
{\
  if ((up && (start > end)) || (!up && (start < end))) return 0;\
  komp_foreach_attr_t attr;\
  komp_foreach_attr_init( &attr );\
  komp_foreach_attr_set_policy( &attr, KS );\
  komp_foreach_attr_set_grains( &attr, chunk_size, 0 );\
  return komp_loop_policy_start_ull( komp_self_thread(), &attr, up, start, end, incr, istart, iend ); \
} \
bool GOMP_loop_ull_ordered_##GS##_start ( \
          bool up, \
          unsigned long long start, \
          unsigned long long end, \
          unsigned long long incr, \
          unsigned long long chunk_size, \
          unsigned long long *istart, \
          unsigned long long *iend \
) \
{ \
  if ((up && (start > end)) || (!up && (start < end))) return 0;\
  komp_foreach_attr_t attr;\
  komp_foreach_attr_init( &attr );\
  komp_foreach_attr_set_policy( &attr, KS );\
  komp_foreach_attr_set_grains( &attr, chunk_size, 0 );\
  return komp_loop_policy_start_ull( komp_self_thread(), &attr, up, start, end, incr, istart, iend ); \
} \
bool GOMP_loop_ull_##GS##_next (unsigned long long *istart, unsigned long long *iend) \
{ \
  unsigned long long ristart; \
  unsigned long long riend;\
  kaapi_processor_t*   kproc = kaapi_self_processor(); \
  kaapi_context_t* thread = kproc->context; \
  if (0== kaapi_foreach_worknext( thread->ws.for_data.rt_ctxt, &ristart, &riend) ) \
  {\
    *istart = thread->ws.for_data.first + thread->ws.for_data.incr * ristart;\
    *iend   = thread->ws.for_data.first + thread->ws.for_data.incr * riend;\
    return 1; \
  }\
  return 0; \
} \
bool GOMP_loop_ull_ordered_##GS##_next (unsigned long long *istart, unsigned long long *iend) \
{ \
  unsigned long long ristart; \
  unsigned long long riend;\
  kaapi_processor_t*   kproc = kaapi_self_processor(); \
  kaapi_context_t* thread = kproc->context; \
  if (0== kaapi_foreach_worknext( thread->ws.for_data.rt_ctxt, &ristart, &riend) ) \
  { \
    *istart = thread->ws.for_data.first + thread->ws.for_data.incr * ristart;\
    *iend   = thread->ws.for_data.first + thread->ws.for_data.incr * riend;\
    thread->ws.for_data.curr_start = *istart; \
    thread->ws.for_data.curr_end   = *iend; \
    return 1; \
  }\
  return 0; \
} \


GOMP_LOOP(static,   KOMP_FOREACH_SCHED_STATIC)
GOMP_LOOP(dynamic,  KOMP_FOREACH_SCHED_DYNAMIC)
GOMP_LOOP(guided,   KOMP_FOREACH_SCHED_GUIDED)
GOMP_LOOP(steal,    KOMP_FOREACH_SCHED_STEAL)
GOMP_LOOP(adaptive, KOMP_FOREACH_SCHED_ADAPTIVE)


bool GOMP_loop_ull_runtime_start (
  bool up,
  unsigned long long start, unsigned long long end, unsigned long long incr,
  unsigned long long *istart,
  unsigned long long *iend
)
{
  komp_dataenv_icv_t* icv = komp_get_dataenv_icv( kaapi_self_processor() );
  switch (icv->run_sched)
  {
    case komp_sched_static:
      return GOMP_loop_ull_static_start(
                  up,
                  start, end, incr,
                  icv->run_sched_modifier,
                  istart, 
                  iend );
    case komp_sched_dynamic:
      return GOMP_loop_ull_dynamic_start(
                  up,
                  start, end, incr,
                  icv->run_sched_modifier,
                  istart,
                  iend );

    case komp_sched_auto:
    case komp_sched_adaptive:
      return GOMP_loop_ull_adaptive_start(
                  up,
                  start, end, incr,
                  icv->run_sched_modifier,
                  istart, 
                  iend );

    case komp_sched_steal:
      return GOMP_loop_ull_steal_start(
                  up,
                  start, end, incr,
                  icv->run_sched_modifier,
                  istart, 
                  iend );

    case komp_sched_guided:
      return GOMP_loop_ull_guided_start(
                  up,
                  start, end, incr,
                  icv->run_sched_modifier,
                  istart, 
                  iend );
    default:
      abort();
  }
}


bool GOMP_loop_ull_ordered_runtime_start (
  bool up,
  unsigned long long start, unsigned long long end, unsigned long long incr,
  unsigned long long *istart,
  unsigned long long *iend
)
{
  komp_dataenv_icv_t* icv = komp_get_dataenv_icv( kaapi_self_processor() );
  switch (icv->run_sched)
  {
    case komp_sched_static:
      return GOMP_loop_ull_ordered_static_start(
                  up,
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier,
                  istart,
                  iend );
    case komp_sched_dynamic:
      return GOMP_loop_ull_ordered_dynamic_start(
                  up,
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier,
                  istart,
                  iend );

    case komp_sched_auto:
    case komp_sched_adaptive:
      return GOMP_loop_ull_ordered_adaptive_start(
                  up,
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier,
                  istart,
                  iend );

    case komp_sched_steal:
      return GOMP_loop_ull_ordered_steal_start(
                  up,
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier,
                  istart, 
                  iend );

    case komp_sched_guided:
      return GOMP_loop_ull_ordered_guided_start(
                  up,
                  start,
                  end,
                  incr,
                  icv->run_sched_modifier,
                  istart,
                  iend );
    default:
      abort();
  }
}


bool GOMP_loop_ull_runtime_next (
  unsigned long long *istart,
  unsigned long long *iend
)
{
  komp_dataenv_icv_t* icv = komp_get_dataenv_icv( kaapi_self_processor() );
  switch (icv->run_sched)
  {
    case komp_sched_static:
      return GOMP_loop_ull_static_next(
                  istart,
                  iend );
    case komp_sched_dynamic:
      return GOMP_loop_ull_dynamic_next(
                  istart,
                  iend );
    case komp_sched_auto:
    case komp_sched_adaptive:
      return GOMP_loop_ull_adaptive_next(
                  istart,
                  iend );
    case komp_sched_steal:
      return GOMP_loop_ull_steal_next(
                  istart,
                  iend );
    case komp_sched_guided:
      return GOMP_loop_ull_guided_next(
                  istart,
                  iend );
    default:
      abort();
  }
}


bool GOMP_loop_ull_ordered_runtime_next (
  unsigned long long *istart,
  unsigned long long *iend
)
{
  komp_dataenv_icv_t* icv = komp_get_dataenv_icv( kaapi_self_processor() );
  switch (icv->run_sched)
  {
    case komp_sched_static:
      return GOMP_loop_ull_ordered_static_next(
                  istart,
                  iend );
    case komp_sched_dynamic:
      return GOMP_loop_ull_ordered_dynamic_next(
                  istart,
                  iend );
    case komp_sched_auto:
    case komp_sched_adaptive:
      return GOMP_loop_ull_ordered_adaptive_next(
                  istart,
                  iend );
    case komp_sched_steal:
      return GOMP_loop_ull_ordered_steal_next(
                  istart,
                  iend );
    case komp_sched_guided:
      return GOMP_loop_ull_ordered_guided_next(
                  istart,
                  iend );
    default:
      abort();
  }
}
