#!/bin/bash

INTTYPES="fixed1 fixed1u fixed2 fixed2u fixed4 fixed4u fixed8 fixed8u"
FLOATTYPES="float4 float8"
OPS="add sub mul div max min eqv neqv"
BINOPS="andb shl shr orb xor andl orl"

function print_header() {
    echo -e "/*"
    echo -e "** xkaapi"
    echo -e "**"
    echo -e "**"
    echo -e "** Copyright 2009,2010,2011,2012, 2013, 2014 INRIA."
    echo -e "**"
    echo -e "** Contributors :"
    echo -e "**"
    echo -e "** francois.broquedis@imag.fr"
    echo -e "** thierry.gautier@inrialpes.fr"
    echo -e "**"
    echo -e "** This software is a computer program whose purpose is to execute"
    echo -e "** multithreaded computation with data flow synchronization between"
    echo -e "** threads."
    echo -e "**"
    echo -e "** This software is governed by the CeCILL-C license under French law"
    echo -e "** and abiding by the rules of distribution of free software.  You can"
    echo -e "** use, modify and/ or redistribute the software under the terms of"
    echo -e "** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the"
    echo -e "** following URL http://www.cecill.info."
    echo -e "**"
    echo -e "** As a counterpart to the access to the source code and rights to"
    echo -e "** copy, modify and redistribute granted by the license, users are"
    echo -e "** provided only with a limited warranty and the software's author,"
    echo -e "** the holder of the economic rights, and the successive licensors"
    echo -e "** have only limited liability."
    echo -e "**"
    echo -e "** In this respect, the user's attention is drawn to the risks"
    echo -e "** associated with loading, using, modifying and/or developing or"
    echo -e "** reproducing the software by the user in light of its specific"
    echo -e "** status of free software, that may mean that it is complicated to"
    echo -e "** manipulate, and that also therefore means that it is reserved for"
    echo -e "** developers and experienced professionals having in-depth computer"
    echo -e "** knowledge. Users are therefore encouraged to load and test the"
    echo -e "** software's suitability as regards their requirements in conditions"
    echo -e "** enabling the security of their systems and/or data to be ensured"
    echo -e "** and, more generally, to use and operate it in the same conditions"
    echo -e "** as regards security."
    echo -e "**"
    echo -e "** The fact that you are presently reading this means that you have"
    echo -e "** had knowledge of the CeCILL-C license and that you accept its"
    echo -e "** terms."
    echo -e "**"
    echo -e "*/"
    echo -e ""
    echo -e "#include <atomic.h>"
    echo -e ""
    echo -e "/* These functions are executed only when the compiler is not able to"
    echo -e " * inline atomic operations. */"
    echo -e ""
}

function name2type() {
    case "$1" in
        "fixed1")
            echo "int8_t"
            ;;
        "fixed1u")
            echo "uint8_t"
            ;;
        "fixed2")
            echo "int16_t"
            ;;
        "fixed2u")
            echo "uint16_t"
            ;;
        "fixed4")
            echo "int32_t"
            ;;
        "fixed4u")
            echo "uint32_t"
            ;;
        "fixed8")
            echo "int64_t"
            ;;
        "fixed8u")
            echo "uint64_t"
            ;;
        "float4")
            echo "float"
            ;;
        "float8")
            echo "double"
            ;;
    esac
}

function type2atomic() {
    case "$1" in
        "int8_t" | "uint8_t")
            echo "kaapi_atomic8_t"
            ;;
        "int16_t" | "uint16_t")
            echo "kaapi_atomic16_t"
            ;;
        "int32_t" | "uint32_t")
            echo "kaapi_atomic32_t"
            ;;
        "int64_t" | "uint64_t")
            echo "kaapi_atomic64_t"
            ;;
        "float")
            echo "kaapi_atomic32_t"
            ;;
        "double")
            echo "kaapi_atomic64_t"
            ;;
    esac
}

print_header

for type in $INTTYPES;
do
    for op in $OPS $BINOPS;
    do
        ctype=$(name2type $type)

        case "$op" in
            "add")
                atomictype=$(type2atomic $ctype)
                # add
                echo -e "void __kmpc_atomic_${type}_${op}(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    KAAPI_ATOMIC_ADD((${atomictype} *)lhs, rhs);"
                echo -e "    TRACE_LEAVE();"
                echo -e "}"
                echo -e ""

                # add capture
                echo -e "$ctype __kmpc_atomic_${type}_${op}_cpt(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs, int flag) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    ${ctype} ret;"
                echo -e "    if (flag)"
                echo -e "        ret = KAAPI_ATOMIC_ADD((${atomictype} *)lhs, rhs);"
                echo -e "    else"
                echo -e "        ret = KAAPI_ATOMIC_ADD_ORIG((${atomictype} *)lhs, rhs);"
                echo -e "    TRACE_LEAVE();"
                echo -e "    return ret;"
                echo -e "}"

                ;;

            "sub")
                atomictype=$(type2atomic $ctype)
                # sub
                echo -e "void __kmpc_atomic_${type}_${op}(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    KAAPI_ATOMIC_SUB((${atomictype} *)lhs, rhs);"
                echo -e "    TRACE_LEAVE();"
                echo -e "}"
                echo -e ""

                # sub capture
                echo -e "$ctype __kmpc_atomic_${type}_${op}_cpt(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs, int flag) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    ${ctype} ret;"
                echo -e "    if (flag)"
                echo -e "        ret = KAAPI_ATOMIC_SUB((${atomictype} *)lhs, rhs);"
                echo -e "    else"
                echo -e "        ret = KAAPI_ATOMIC_SUB_ORIG((${atomictype} *)lhs, rhs);"
                echo -e "    TRACE_LEAVE();"
                echo -e "    return ret;"
                echo -e "}"

                ;;

            *)
                # update
                echo -e "void __kmpc_atomic_${type}_${op}(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    __${op}(lhs, rhs);"
                echo -e "    TRACE_LEAVE();"
                echo -e "}"
                echo -e ""

                # capture
                echo -e "$ctype __kmpc_atomic_${type}_${op}_cpt(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs, int flag) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    TRACE_LEAVE();"
                echo -e "    __${op}_cpt(${ctype}, lhs, rhs, flag);"
                echo -e "}"
                echo -e ""

                ;;
        esac
    done
done

for type in $FLOATTYPES;
do
    ctype=$(name2type $type)

    for op in $OPS;
    do
        case "$op" in
            "add")
                atomictype=$(type2atomic $ctype)
                # add
                echo -e "void __kmpc_atomic_${type}_${op}(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    KAAPI_ATOMIC_ADD((${atomictype} *)lhs, rhs);"
                echo -e "    TRACE_LEAVE();"
                echo -e "}"
                echo -e ""

                # add capture
                echo -e "$ctype __kmpc_atomic_${type}_${op}_cpt(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs, int flag) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    ${ctype} ret;"
                echo -e "    if (flag)"
                echo -e "        ret = KAAPI_ATOMIC_ADD((${atomictype} *)lhs, rhs);"
                echo -e "    else"
                echo -e "        ret = KAAPI_ATOMIC_ADD_ORIG((${atomictype} *)lhs, rhs);"
                echo -e "    TRACE_LEAVE();"
                echo -e "    return ret;"
                echo -e "}"

                ;;

            "sub")
                atomictype=$(type2atomic $ctype)
                # sub
                echo -e "void __kmpc_atomic_${type}_${op}(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    KAAPI_ATOMIC_SUB((${atomictype} *)lhs, rhs);"
                echo -e "    TRACE_LEAVE();"
                echo -e "}"
                echo -e ""

                # sub capture
                echo -e "$ctype __kmpc_atomic_${type}_${op}_cpt(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs, int flag) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    ${ctype} ret;"
                echo -e "    if (flag)"
                echo -e "        ret = KAAPI_ATOMIC_SUB((${atomictype} *)lhs, rhs);"
                echo -e "    else"
                echo -e "        ret = KAAPI_ATOMIC_SUB_ORIG((${atomictype} *)lhs, rhs);"
                echo -e "    TRACE_LEAVE();"
                echo -e "    return ret;"
                echo -e "}"

                ;;

            *)
                # update
                echo -e "void __kmpc_atomic_${type}_${op}(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    __${op}(lhs, rhs);"
                echo -e "    TRACE_LEAVE();"
                echo -e "}"
                echo -e ""

                # capture
                echo -e "$ctype __kmpc_atomic_${type}_${op}_cpt(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs, int flag) {"
                echo -e "    TRACE_ENTER();"
                echo -e "    TRACE_LEAVE();"
                echo -e "    __${op}_cpt(${ctype}, lhs, rhs, flag);"
                echo -e "}"
                echo -e ""

                ;;

        esac
    done
done

for type in $INTTYPES $FLOATTYPES;
do
    ctype=$(name2type $type)

    #read
    echo -e "$ctype __kmpc_atomic_${type}_rd(ident_t *id_ref, int gtid, $ctype *loc) {"
    echo -e "    TRACE_ENTER();"
    echo -e "    fprintf(stderr, \"Function %s not implemented!\", __FUNCTION__);"
    echo -e "    abort();"
    echo -e "    TRACE_LEAVE();"
    echo -e "}"
    echo -e ""

    #write
    echo -e "void __kmpc_atomic_${type}_wr(ident_t *id_ref, int gtid, $ctype *lhs, $ctype rhs) {"
    echo -e "    TRACE_ENTER();"
    echo -e "    fprintf(stderr, \"Function %s not implemented!\", __FUNCTION__);"
    echo -e "    abort();"
    echo -e "    TRACE_LEAVE();"
    echo -e "}"
    echo -e ""

done
