/*
** xkaapi
**
**
** Copyright 2009,2010,2011,2012, 2013, 2014 INRIA.
**
** Contributors :
**
** francois.broquedis@imag.fr
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
**
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
**
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
**
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
**
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
**
*/
#include <assert.h>
#include <stdarg.h>
#include <stdbool.h>

#include <libiomp.h>

#include <kaapi_impl.h>


static bool __loop_worknext_32(kaapi_context_t *thread, int32_t *istart, int32_t *iend) {
    unsigned long long ristart;
    unsigned long long riend;

    if ( (thread->ws.for_data.rt_ctxt != 0)
         && (0 == kaapi_foreach_worknext(thread->ws.for_data.rt_ctxt, &ristart, &riend)) )
    {
        *istart = (int32_t)(thread->ws.for_data.first + thread->ws.for_data.incr * ristart);
        *iend   = (int32_t)(thread->ws.for_data.first + thread->ws.for_data.incr * (riend - 1));
        thread->ws.for_data.curr_start = *istart;
        thread->ws.for_data.curr_end   = *iend;
        return 1;
    }
    return 0;
}

static bool __loop_worknext_64(kaapi_context_t *thread, int64_t *istart, int64_t *iend) {
    unsigned long long ristart;
    unsigned long long riend;

    if ( (thread->ws.for_data.rt_ctxt != 0)
         && (0 == kaapi_foreach_worknext(thread->ws.for_data.rt_ctxt, &ristart, &riend)) )
    {
        *istart = thread->ws.for_data.first + thread->ws.for_data.incr * ristart;
        *iend   = thread->ws.for_data.first + thread->ws.for_data.incr * (riend - 1);
        thread->ws.for_data.curr_start = *istart;
        thread->ws.for_data.curr_end   = *iend;
        return 1;
    }
    return 0;
}

static bool __loop_policy_start_32(komp_foreach_attr_policy_t policy,
                                   int32_t start,
                                   int32_t end,
                                   int32_t incr,
                                   int32_t chunk_size,
                                   int32_t *pstride,
                                   int32_t *istart,
                                   int32_t *iend) {
    kaapi_processor_t *kproc = kaapi_self_processor();
    kaapi_context_t *context = kproc->context;
    komp_foreach_attr_t attr;
    komp_foreach_attr_init( &attr );
    komp_foreach_attr_set_policy( &attr, policy );
    komp_foreach_attr_set_grains( &attr, chunk_size, 0 );

    if (0 == komp_loop_workinit( (komp_thread_t*)kaapi_context2thread(context),
                                  1, &attr,
                                  (int64_t)start, (int64_t)end, (int64_t)incr,
                                  0, 0))
        return 0;

    *pstride = chunk_size * kaapi_self_team()->count;

    return __loop_worknext_32(context, istart, iend);
}

static bool __loop_policy_start_64(komp_foreach_attr_policy_t policy,
                                   int64_t start,
                                   int64_t end,
                                   int64_t incr,
                                   int64_t chunk_size,
                                   int64_t *pstride,
                                   int64_t *istart,
                                   int64_t *iend) {
    kaapi_processor_t *kproc = kaapi_self_processor();
    kaapi_context_t *context = kproc->context;
    komp_foreach_attr_t attr;
    komp_foreach_attr_init( &attr );
    komp_foreach_attr_set_policy( &attr, policy );
    komp_foreach_attr_set_grains( &attr, chunk_size, 0 );

    if (0 == komp_loop_workinit( (komp_thread_t*)kaapi_context2thread(context),
                                  1, &attr,
                                  start, end, incr,
                                  0, 0))
        return 0;

    *pstride = chunk_size * kaapi_self_team()->count;

    return __loop_worknext_64(context, istart, iend);
}

static inline void __for_static_init_32(int32_t schedtype, int32_t *plastiter, int32_t *plower,
                                        int32_t *pupper, int32_t *pstride, int32_t incr, int32_t chunk) {
    int32_t start = *plower;
    int32_t end = *pupper + 1;

    if (((incr > 0) && (end <= start)) || ((incr < 0) && (start <= end)))
        return;

    int32_t chunk_size = schedtype == kmp_sch_static ? 0 : chunk;
    __loop_policy_start_32(KOMP_FOREACH_SCHED_STATIC, start, end, incr, chunk_size, pstride, plower, pupper);
}

/* Setting plastiter as 32bit-wide integer may look weird, but it fits
 * the ABI... */
static inline void __for_static_init_64(int32_t schedtype, int32_t *plastiter, int64_t *plower,
                                        int64_t *pupper, int64_t *pstride, int64_t incr, int64_t chunk) {
    int64_t start = *plower;
    int64_t end = *pupper + 1;

    if (((incr > 0) && (end <= start)) || ((incr < 0) && (start <= end)))
        return;

    int64_t chunk_size = schedtype == kmp_sch_static ? 0 : chunk;
    __loop_policy_start_64(KOMP_FOREACH_SCHED_STATIC, start, end, incr, chunk_size, pstride, plower, pupper);
}

void __kmpc_for_static_init_4(ident_t *loc, kmp_int32 gtid, kmp_int32 schedtype, kmp_int32 *plastiter,
                              kmp_int32 *plower, kmp_int32 *pupper,
                              kmp_int32 *pstride, kmp_int32 incr, kmp_int32 chunk) {
    TRACE_ENTER();

    __for_static_init_32(schedtype, plastiter, plower, pupper, pstride, incr, chunk);

    TRACE_LEAVE();
}

void __kmpc_for_static_init_4u(ident_t *loc, kmp_int32 gtid, kmp_int32 schedtype, kmp_int32 *plastiter,
                               kmp_uint32 *plower, kmp_uint32 *pupper,
                               kmp_int32 *pstride, kmp_int32 incr, kmp_int32 chunk) {
    TRACE_ENTER();

    __for_static_init_32(schedtype, (int32_t *)plastiter, (int32_t *)plower,
                         (int32_t *)pupper, (int32_t *)pstride, incr, chunk);

    TRACE_LEAVE();
}

void __kmpc_for_static_init_8(ident_t *loc, kmp_int32 gtid, kmp_int32 schedtype, kmp_int32 *plastiter,
                              kmp_int64 *plower, kmp_int64 *pupper,
                              kmp_int64 *pstride, kmp_int64 incr, kmp_int64 chunk) {
    TRACE_ENTER();

    __for_static_init_64(schedtype, plastiter, plower, pupper, pstride, incr, chunk);

    TRACE_LEAVE();
}

void __kmpc_for_static_init_8u(ident_t *loc, kmp_int32 gtid, kmp_int32 schedtype, kmp_int32 *plastiter,
                               kmp_uint64 *plower, kmp_uint64 *pupper,
                               kmp_int64 *pstride, kmp_int64 incr, kmp_int64 chunk) {
    TRACE_ENTER();

    __for_static_init_64(schedtype, plastiter, (int64_t *)plower,
                         (int64_t *)pupper, (int64_t *)pstride, incr, chunk);

    TRACE_LEAVE();
}


void __kmpc_for_static_fini(ident_t *loc, kmp_int32 global_tid) {
    TRACE_ENTER();

    /* FIXME: Check whether this is called every time a thread
     * completes a chunk or only when the total loop is complete. */
    komp_loop_workfini( komp_self_thread() );

    TRACE_LEAVE();
}

static inline void __kmpc_dispatch_init(enum sched_type schedule, int64_t lower,
                                        int64_t upper, int64_t stride, int64_t chunk_size)
{
  komp_foreach_attr_policy_t policy;
  switch(schedule)
  {
        /* static loops */
    case kmp_sch_static:
    case kmp_sch_static_chunked:
    case kmp_ord_lower:
    case kmp_ord_upper:
    case kmp_ord_static:
    case kmp_ord_static_chunked:
        policy = KOMP_FOREACH_SCHED_STATIC;
        break;
        /* dynamic loops */
    case kmp_sch_dynamic_chunked:
    case kmp_ord_dynamic_chunked:
        policy = KOMP_FOREACH_SCHED_DYNAMIC;
        break;
        /* guided loops */
    case kmp_sch_guided_chunked:
    case kmp_ord_guided_chunked:
        policy = KOMP_FOREACH_SCHED_GUIDED;
        /* runtime loops */
    case kmp_sch_runtime:
    {
        komp_dataenv_icv_t* icvs = komp_get_dataenv_icv( kaapi_self_processor());
        chunk_size = icvs->run_sched_modifier ? icvs->run_sched_modifier : chunk_size;
        policy = (komp_foreach_attr_policy_t)icvs->run_sched;
    } break;
    default:
        abort();
  }
  kaapi_processor_t *kproc = kaapi_self_processor();
  kaapi_context_t *context = kproc->context;
  komp_foreach_attr_t attr;
  komp_foreach_attr_init( &attr );
  komp_foreach_attr_set_policy( &attr, policy );
  komp_foreach_attr_set_grains( &attr, chunk_size, 0 );
  komp_loop_workinit((komp_thread_t*)kaapi_context2thread(context), 1, &attr, lower, upper, stride, 0, 0);
}

void __kmpc_dispatch_init_4(ident_t *loc, kmp_int32 gtid, enum sched_type schedule,
                            kmp_int32 lower, kmp_int32 upper, kmp_int32 stride, kmp_int32 chunk) {
    TRACE_ENTER();
    __kmpc_dispatch_init(schedule, (int64_t)lower, (int64_t)upper+1, (int64_t)stride, (int64_t)chunk);
    TRACE_LEAVE();
}

void __kmpc_dispatch_init_4u(ident_t *loc, kmp_int32 gtid, enum sched_type schedule,
                             kmp_uint32 lower, kmp_uint32 upper, kmp_uint32 stride, kmp_uint32 chunk) {
    TRACE_ENTER();
    __kmpc_dispatch_init(schedule, (int64_t)lower, (int64_t)upper+1, (int64_t)stride, (int64_t)chunk);
    TRACE_LEAVE();
}

void __kmpc_dispatch_init_8(ident_t *loc, kmp_int32 gtid, enum sched_type schedule,
                            kmp_int64 lower, kmp_int64 upper, kmp_int64 stride, kmp_int64 chunk) {
    TRACE_ENTER();
    __kmpc_dispatch_init(schedule, (int64_t)lower, (int64_t)upper+1, (int64_t)stride, (int64_t)chunk);
    TRACE_LEAVE();
}

void __kmpc_dispatch_init_8u(ident_t *loc, kmp_int32 gtid, enum sched_type schedule,
                             kmp_uint64 lower, kmp_uint64 upper, kmp_uint64 stride, kmp_uint64 chunk) {
    TRACE_ENTER();
    __kmpc_dispatch_init(schedule, (int64_t)lower, (int64_t)upper+1, (int64_t)stride, (int64_t)chunk);
    TRACE_LEAVE();
}

int __kmpc_dispatch_next_4(ident_t *loc, kmp_int32 gtid, kmp_int32 *plastiter,
                           kmp_int32 *plower, kmp_int32 *pupper, kmp_int32 *pstride) {
    TRACE_ENTER();
    kaapi_processor_t *kproc = kaapi_self_processor();
    bool has_remaining_work = __loop_worknext_32(kproc->context, plower, pupper);
    if (!has_remaining_work) {
      komp_loop_workfini( komp_self_thread() );
      TRACE_LEAVE();
      return 0;
    }
    TRACE_LEAVE();
    return 1;
}

int __kmpc_dispatch_next_4u(ident_t *loc, kmp_int32 gtid, kmp_uint32 *plastiter,
                            kmp_uint32 *plower, kmp_uint32 *pupper, kmp_uint32 *pstride) {
    TRACE_ENTER();
    kaapi_processor_t *kproc = kaapi_self_processor();
    bool has_remaining_work = __loop_worknext_32(kproc->context, (int32_t *)plower, (int32_t *)pupper);
    if (!has_remaining_work) {
      komp_loop_workfini( komp_self_thread() );
      TRACE_LEAVE();
      return 0;
    }
    TRACE_LEAVE();
    return 1;
}

int __kmpc_dispatch_next_8(ident_t *loc, kmp_int32 gtid, kmp_int64 *plastiter,
                           kmp_int64 *plower, kmp_int64 *pupper, kmp_int64 *pstride) {
    TRACE_ENTER();
    kaapi_processor_t *kproc = kaapi_self_processor();
    bool has_remaining_work = __loop_worknext_64(kproc->context, plower, pupper);
    if (!has_remaining_work) {
      komp_loop_workfini( komp_self_thread() );
      TRACE_LEAVE();
      return 0;
    }
    TRACE_LEAVE();
    return 1;
}

int __kmpc_dispatch_next_8u(ident_t *loc, kmp_int32 gtid, kmp_uint64 *plastiter,
                            kmp_uint64 *plower, kmp_uint64 *pupper, kmp_uint64 *pstride) {
    TRACE_ENTER();
    kaapi_processor_t *kproc = kaapi_self_processor();
    bool has_remaining_work = __loop_worknext_64(kproc->context, (int64_t *)plower, (int64_t *)pupper);
    if (!has_remaining_work) {
      komp_loop_workfini( komp_self_thread() );
      TRACE_LEAVE();
      return 0;
    }
    TRACE_LEAVE();
    return 1;
}

void __kmpc_dispatch_fini_4(ident_t *loc, kmp_int32 gtid) {
    TRACE_ENTER();
    TRACE_LEAVE();
}

void __kmpc_dispatch_fini_4u(ident_t *loc, kmp_uint32 gtid) {
    TRACE_ENTER();
    TRACE_LEAVE();
}

void __kmpc_dispatch_fini_8(ident_t *loc, kmp_int64 gtid) {
    TRACE_ENTER();
    TRACE_LEAVE();
}
void __kmpc_dispatch_fini_8u(ident_t *loc, kmp_uint64 gtid) {
    TRACE_ENTER();
    TRACE_LEAVE();
}
