/* Copyright (C) 2005 Free Software Foundation, Inc.
   Contributed by Jakub Jelinek <jakub@redhat.com>.

   This file is part of the GNU OpenMP Library (libgomp).

   Libgomp is free software; you can redistribute it and/or modify it
   under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   Libgomp is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with libgomp; see the file COPYING.LIB.  If not, write to the
   Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.  */

/* As a special exception, if you link this library with other files, some
   of which are compiled with GCC, to produce an executable, this library
   does not by itself cause the resulting executable to be covered by the
   GNU General Public License.  This exception does not however invalidate
   any other reasons why the executable file might be covered by the GNU
   General Public License.  */

/* This file contains Fortran wrapper routines.  */

#include "libgomp.h"
#include "komp_lock-internal.h"
#include <stdlib.h>       

# define omp_lock_symver30(fn) \
  __asm (".symver " #fn "_30, " #fn "@@OMP_3.0");

extern void
omp_set_dynamic_ (const int32_t *set);

extern  void
omp_set_dynamic_8_ (const int64_t *set);

extern void
omp_set_nested_ (const int32_t *set);

extern void
omp_set_nested_8_ (const int64_t *set);

extern void
omp_set_num_threads_ (const int32_t *set);

extern void
omp_set_num_threads_8_ (const int64_t *set);

extern int32_t
omp_get_dynamic_ (void);

extern int32_t
omp_get_nested_ (void);

extern int32_t
omp_in_parallel_ (void);

extern int32_t
omp_in_final_ (void);

extern int32_t
omp_get_max_threads_ (void);

extern int32_t
omp_get_num_procs_ (void);

extern int32_t
omp_get_num_threads_ (void);

extern int32_t
omp_get_thread_num_ (void);

extern double
omp_get_wtick_ (void);

extern double
omp_get_wtime_ (void);

extern void
omp_set_schedule_ (const int32_t *kind, const int32_t *modifier);

extern void
omp_set_schedule_8_ (const int32_t *kind, const int64_t *modifier);

extern void
omp_get_schedule_ (int32_t *kind, int32_t *modifier);

extern void
omp_get_schedule_8_ (int32_t *kind, int64_t *modifier);

extern int32_t
omp_get_thread_limit_ (void);

extern void
omp_set_max_active_levels_ (const int32_t *levels);

extern void
omp_set_max_active_levels_8_ (const int64_t *levels);

extern int32_t
omp_get_max_active_levels_ (void);

extern int32_t
omp_get_level_ (void);

extern int32_t
omp_get_ancestor_thread_num_ (const int32_t *level);

extern int32_t
omp_get_ancestor_thread_num_8_ (const int64_t *level);

extern int32_t
omp_get_team_size_ (const int32_t *level);

extern int32_t
omp_get_team_size_8_ (const int64_t *level);

extern int32_t
omp_get_active_level_ (void);


#ifndef HAVE_VERSION_SYMBOL
# define komp_init_lock__30 omp_init_lock_
# define komp_destroy_lock__30 omp_destroy_lock_
# define komp_set_lock__30 omp_set_lock_
# define komp_unset_lock__30 omp_unset_lock_
# define komp_test_lock__30 omp_test_lock_
# define komp_init_nest_lock__30 omp_init_nest_lock_
# define komp_destroy_nest_lock__30 omp_destroy_nest_lock_
# define komp_set_nest_lock__30 omp_set_nest_lock_
# define komp_unset_nest_lock__30 omp_unset_nest_lock_
# define komp_test_nest_lock__30 omp_test_nest_lock_
#endif

#define KOMP_DIRECT_MAPPING 1

#ifndef KOMP_DIRECT_MAPPING
typedef komp_lock_t **omp_lock_arg_t;
# define omp_lock_arg(arg) (*(arg))
#else
typedef komp_lock_t *omp_lock_arg_t;
# define omp_lock_arg(arg) (arg)
#endif

#ifndef KOMP_DIRECT_MAPPING_NESTED
typedef komp_nest_lock_t **omp_nest_lock_arg_t;
# define omp_nest_lock_arg(arg) (*(arg))
#else
typedef komp_nest_lock_t *omp_nest_lock_arg_t;
# define omp_nest_lock_arg(arg) (arg)
#endif

extern void
komp_init_lock__30 (omp_lock_arg_t lock);

extern void
komp_init_nest_lock__30 (omp_nest_lock_arg_t lock);

extern void
komp_destroy_lock__30 (omp_lock_arg_t lock);

extern void
komp_destroy_nest_lock__30 (omp_nest_lock_arg_t lock);

extern void
komp_set_lock__30 (omp_lock_arg_t lock);

extern void
komp_set_nest_lock__30 (omp_nest_lock_arg_t lock);

extern void
komp_unset_lock__30 (omp_lock_arg_t lock);

extern void
komp_unset_nest_lock__30 (omp_nest_lock_arg_t lock);

extern int32_t
komp_test_lock__30 (omp_lock_arg_t lock);

extern int32_t
komp_test_nest_lock__30 (omp_nest_lock_arg_t lock);


#define TO_INT(x) (int)((x) > INT_MIN ? (x) < INT_MAX ? (x) : INT_MAX : INT_MIN)

void
omp_set_dynamic_ (const int32_t *set)
{
  omp_set_dynamic (*set);
}

void
omp_set_dynamic_8_ (const int64_t *set)
{
  omp_set_dynamic ((int)*set);
}

void
omp_set_nested_ (const int32_t *set)
{
  omp_set_nested (*set);
}

void
omp_set_nested_8_ (const int64_t *set)
{
  omp_set_nested ((int)*set);
}

void
omp_set_num_threads_ (const int32_t *set)
{
  omp_set_num_threads (*set);
}

void
omp_set_num_threads_8_ (const int64_t *set)
{
  omp_set_num_threads (TO_INT(*set));
}

int32_t
omp_get_dynamic_ (void)
{
  return omp_get_dynamic();
}

int32_t
omp_get_nested_ (void)
{
  return omp_get_nested();
}

int32_t
omp_in_parallel_ (void)
{
  return omp_in_parallel();
}

int32_t
omp_in_final_(void)
{
  return omp_in_final();
}

int32_t
omp_get_max_threads_ (void)
{
  return omp_get_max_threads();
}

int32_t
omp_get_num_procs_ (void)
{
  return omp_get_num_procs();
}

int32_t
omp_get_num_threads_ (void)
{
  return omp_get_num_threads();
}

int32_t
omp_get_thread_num_ (void)
{
  return omp_get_thread_num();
}

double
omp_get_wtick_ (void)
{
  return omp_get_wtick();
}

double
omp_get_wtime_ (void)
{
  return omp_get_wtime();
}


void
omp_set_schedule_ (const int32_t *kind, const int32_t *modifier)
{
  omp_set_schedule (*kind, *modifier);
}

void
omp_set_schedule_8_ (const int32_t *kind, const int64_t *modifier)
{
  omp_set_schedule (*kind, TO_INT(*modifier));
}

void
omp_get_schedule_ (int32_t *kind, int32_t *modifier)
{
  omp_sched_t k;
  int m;
  omp_get_schedule (&k, &m);
  *kind = k;
  *modifier = m;
}

void
omp_get_schedule_8_ (int32_t *kind, int64_t *modifier)
{
  omp_sched_t k;
  int m;
  omp_get_schedule (&k, &m);
  *kind = k;
  *modifier = m;
}

int32_t
omp_get_thread_limit_ (void)
{
  return omp_get_thread_limit();
}

void
omp_set_max_active_levels_ (const int32_t *levels)
{
  omp_set_max_active_levels (*levels);
}

void
omp_set_max_active_levels_8_ (const int64_t *levels)
{
  omp_set_max_active_levels (TO_INT(*levels));
}

int32_t
omp_get_max_active_levels_ (void)
{
  return omp_get_max_active_levels();
}

int32_t
omp_get_level_ (void)
{
  return omp_get_level();
}

int32_t
omp_get_ancestor_thread_num_ (const int32_t *level)
{
  return omp_get_ancestor_thread_num (*level);
}

int32_t
omp_get_ancestor_thread_num_8_ (const int64_t *level)
{
  return omp_get_ancestor_thread_num (TO_INT (*level));
}

int32_t
omp_get_team_size_ (const int32_t *level)
{
  return omp_get_team_size (*level);
}

int32_t
omp_get_team_size_8_ (const int64_t *level)
{
  return omp_get_team_size (TO_INT(*level));
}

int32_t
omp_get_active_level_ (void)
{
  return omp_get_active_level();
}
 

#ifndef HAVE_VERSION_SYMBOL
# define omp_init_lock__30 omp_init_lock_
# define omp_destroy_lock__30 omp_destroy_lock_
# define omp_set_lock__30 omp_set_lock_
# define omp_unset_lock__30 omp_unset_lock_
# define omp_test_lock__30 omp_test_lock_
# define omp_init_nest_lock__30 omp_init_nest_lock_
# define omp_destroy_nest_lock__30 omp_destroy_nest_lock_
# define omp_set_nest_lock__30 omp_set_nest_lock_
# define omp_unset_nest_lock__30 omp_unset_nest_lock_
# define omp_test_nest_lock__30 omp_test_nest_lock_
#endif

void
omp_init_lock__30 (omp_lock_arg_t lock)
{
#ifndef KOMP_DIRECT_MAPPING
  *lock = (omp_lock_t*) malloc( sizeof(omp_lock_t) );
#endif
  komp_init_lock(omp_lock_arg (lock));
}

void
omp_init_nest_lock__30 (omp_nest_lock_arg_t lock)
{
#ifndef KOMP_DIRECT_MAPPING_NESTED
  *lock = (komp_nest_lock_t*)malloc( sizeof(komp_nest_lock_t) );
#endif
  komp_init_nest_lock(omp_nest_lock_arg (lock));
}

void
omp_destroy_lock__30 (omp_lock_arg_t lock)
{
  komp_destroy_lock(omp_lock_arg (lock));
#ifndef KOMP_DIRECT_MAPPING
  free(omp_lock_arg (lock));
#endif
}

void
omp_destroy_nest_lock__30 (omp_nest_lock_arg_t lock)
{
  komp_destroy_nest_lock(omp_nest_lock_arg (lock));
  free(omp_nest_lock_arg (lock));
}

void
omp_set_lock__30 (omp_lock_arg_t lock)
{
  komp_set_lock(omp_lock_arg (lock));
}

void
omp_set_nest_lock__30 (omp_nest_lock_arg_t lock)
{
  komp_set_nest_lock(omp_nest_lock_arg (lock));
}

void
omp_unset_lock__30 (omp_lock_arg_t lock)
{
  komp_unset_lock(omp_lock_arg (lock));
}

void
omp_unset_nest_lock__30 (omp_nest_lock_arg_t lock)
{
  komp_unset_nest_lock(omp_nest_lock_arg (lock));
}

int32_t
omp_test_lock__30 (omp_lock_arg_t lock)
{
  return komp_test_lock(omp_lock_arg (lock));
}

int32_t
omp_test_nest_lock__30 (omp_nest_lock_arg_t lock)
{
  return komp_test_nest_lock(omp_nest_lock_arg (lock));
}

#ifdef HAVE_VERSION_SYMBOL
omp_lock_symver30(omp_init_lock_)
omp_lock_symver30(omp_destroy_lock_)
omp_lock_symver30(omp_set_lock_)
omp_lock_symver30(omp_unset_lock_)
omp_lock_symver30(omp_test_lock_)
omp_lock_symver30(omp_init_nest_lock_)
omp_lock_symver30(omp_destroy_nest_lock_)
omp_lock_symver30(omp_set_nest_lock_)
omp_lock_symver30(omp_unset_nest_lock_)
omp_lock_symver30(omp_test_nest_lock_)
#endif

