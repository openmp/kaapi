/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi++"

namespace ka {

// --------------------------------------------------------------------------
Format::Format( 
        const char*        name,
        size_t             size,
        void             (*cstor)( void* ),
        void             (*dstor)( void* ),
        void             (*cstorcopy)( void*, const void* ),
        void             (*copy)( void*, const void*),
        void             (*assign)( void*, const kaapi_memory_view_t*, const void*, const kaapi_memory_view_t*)
)
 : fmt(0) /* should be set to 0 ! */
{
  std::string fmt_name = std::string("__Z4TypeI")+name;
  if (fmt ==0) 
    fmt = kaapi_format_allocate();
  kaapi_format_structregister(
    fmt,
    fmt_name.c_str(),
    size,
    cstor,
    dstor,
    cstorcopy,
    copy,
    assign
  );
}


// --------------------------------------------------------------------------
Format::Format( kaapi_format_t* f ) 
 : fmt(f) 
{
}

// --------------------------------------------------------------------------
void Format::reinit( kaapi_format_t* f ) const
{
  fmt = f;
}

// --------------------------------------------------------------------------
struct kaapi_format_t* Format::get_c_format() 
{ 
  if (fmt ==0) fmt = kaapi_format_allocate();
  return fmt; 
}

// --------------------------------------------------------------------------
const struct kaapi_format_t* Format::get_c_format() const 
{ 
  if (fmt ==0) fmt = kaapi_format_allocate();
  return fmt; 
}

// --------------------------------------------------------------------------
FormatUpdateFnc::FormatUpdateFnc( 
  const char* name,
  int (*update_mb)(void* data, const struct kaapi_format_t* fmtdata,
                   const void* value, const struct kaapi_format_t* fmtvalue )
) : Format::Format(name, 0, 0, 0, 0, 0, 0)
{
  kaapi_format_set_update_mb(fmt,update_mb);
}


// --------------------------------------------------------------------------
FormatTask::FormatTask( 
  const char*                    name,
  kaapi_fmt_fnc_get_name         get_name,
  kaapi_fmt_fnc_get_size         get_size,
  kaapi_fmt_fnc_task_copy        task_copy,
  kaapi_fmt_fnc_get_count_params get_count_params,
  kaapi_fmt_fnc_get_mode_param   get_mode_param,
  kaapi_fmt_fnc_get_data_param   get_data_param,
  kaapi_fmt_fnc_get_access_param get_access_param,
  kaapi_fmt_fnc_set_access_param set_access_param,
  kaapi_fmt_fnc_get_fmt_param    get_fmt_param,
  kaapi_fmt_fnc_get_view_param   get_view_param,
  kaapi_fmt_fnc_set_view_param   set_view_param,
  kaapi_fmt_fnc_reducor          reducor,
  kaapi_fmt_fnc_redinit          redinit,
  kaapi_fmt_fnc_get_splitter	   get_splitter
) 
 : Format((kaapi_format_t*)0)
{
  std::string fmt_name = std::string("__Z4TypeI")+name;
  if (fmt ==0) {
    fmt = kaapi_format_allocate();
    kaapi_format_taskregister_func( 
          fmt,
          (void*)(uintptr_t)kaapi_hash_value(name),
          0, 
          name,
          get_name,
          get_size,
          task_copy,
          get_count_params,
          get_mode_param,
          get_data_param,
          get_access_param,
          set_access_param,
          get_fmt_param,
  /**/    get_view_param,
  /**/    set_view_param,
          reducor,
          redinit,
          get_splitter,
          0
    );
  }
}

} // namespace ka
