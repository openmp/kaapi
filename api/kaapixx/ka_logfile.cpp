/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi++"
#include "ka_error.h"
#include "ka_debug.h"
#include "ka_timer.h"
#include "kaapi_time.h"
#include <map>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>


#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#if defined(__APPLE__)
#include <sys/sysctl.h>
#endif


namespace ka {

// --------------------------------------------------------------------
std::ostream& logfile()
{
  int self = kaapi_self_kid();
  std::cout << System::local_gid << "::" << self << "::[" << std::setw(9) << std::setprecision(7) << std::showpoint;
  std::cout.fill( '0' );
  std::cout.setf( std::ios_base::left, std::ios_base::adjustfield);
  std::cout << (kaapi_get_elapsedns_since_start())*1e-6 << "]: ";
  return std::cout;
}


}
