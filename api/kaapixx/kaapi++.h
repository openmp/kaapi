/*
 ** xkaapi
 ** 
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#ifndef _KAAPI_CPP_H_
#define _KAAPI_CPP_H_

#include "kaapi.h"
#include "ka_error.h"
#include "ka_timer.h"
#include <stdio.h>
#include <string.h>
#include <vector>
#include <typeinfo>
#include <iterator>
#include <stdexcept>
#include <cstddef>
#include <sstream>
#include <new>

/** Version number for the API
 - v1: new API for DFG with pointer interface.
 */
#define KAAPIXX_API_VERSION 1
namespace ka {
  
  /** Log file for this process
   \ingroup atha
   If not set_logfile, then return the std::cout stream.
   */
  extern std::ostream& logfile();
  
  //@{
  /** Return an system wide identifier of the type of an expression
   */
#define kaapi_get_swid(EXPR) kaapi_hash_value(typeid(EXPR).name())
  //@}
  
  /* take a constant... should be adjusted */
  enum { STACK_ALLOC_THRESHOLD = __BIGGEST_ALIGNMENT__ };  
  
  // --------------------------------------------------------------------
  struct kaapi_bodies_t {
    kaapi_bodies_t( kaapi_task_body_t cpu_body, kaapi_task_body_t gpu_body );
    kaapi_task_body_t cpu_body;
    kaapi_task_body_t gpu_body;
    kaapi_task_body_t default_body;
  };
  
  /* Kaapi C++ thread <-> Kaapi C thread */
  class Thread;
  
  /* Kaapi C++ Request <-> Kaapi C kaapi_request_t */
  class Request;
  
  /* for next networking part */
  class IStream;
  class OStream;
  class ODotStream;
  class SyncGuard;
  
  /* used in utility functions */
  typedef char kaapi_no_type;
  typedef double kaapi_yes_type;
  
  
#if 0
  // --------------------------------------------------------------------
  class gpuStream {
  public:
    gpuStream( kaapi_gpustream_t gs ): stream(gs) {}
    gpuStream( ): stream(0) {}
    kaapi_gpustream_t stream; /* at least for CUDA */
  };
#endif  
  
  // --------------------------------------------------------------------
  /** link C++ format -> kaapi format */
  class Format {
  public:
    Format( 
           const char*        name = "empty",
           size_t             size = 0,
           void             (*cstor)( void* dest) = 0,
           void             (*dstor)( void* dest) = 0,
           void             (*cstorcopy)( void* dest, const void* src) = 0,
           void             (*copy)( void* dest, const void* src) = 0,
           void             (*assign)( void*, const kaapi_memory_view_t*, const void*, const kaapi_memory_view_t*) =0
           );
    struct kaapi_format_t* get_c_format();
    const struct kaapi_format_t* get_c_format() const;
    
  public:
    /* should be protected and friend with wrapperformat */
    Format( kaapi_format_t* f );
    void reinit( kaapi_format_t* f ) const;
    
  protected:
    mutable struct kaapi_format_t* fmt;
  };
  
  /** format for update function */
  class FormatUpdateFnc : public Format {
  public:
    FormatUpdateFnc( 
                    const char* name,
                    int (*update_mb)(void* data, const struct kaapi_format_t* fmtdata,
                                     const void* value, const struct kaapi_format_t* fmtvalue )
                    );
  };
  
  // --------------------------------------------------------------------  
  /** format for all kind of task */
  class FormatTask : public Format {
  public:
    
    /* task with dynamic format */
    FormatTask( 
              const char*                    name,
              kaapi_fmt_fnc_get_name         get_name,
              kaapi_fmt_fnc_get_size         get_size,
              kaapi_fmt_fnc_task_copy        task_copy,
              kaapi_fmt_fnc_get_count_params get_count_params,
              kaapi_fmt_fnc_get_mode_param   get_mode_param,
              kaapi_fmt_fnc_get_data_param   get_data_param,
              kaapi_fmt_fnc_get_access_param get_access_param,
              kaapi_fmt_fnc_set_access_param set_access_param,
              kaapi_fmt_fnc_get_fmt_param    get_fmt_param,
              kaapi_fmt_fnc_get_view_param   get_view_param,
              kaapi_fmt_fnc_set_view_param   set_view_param,
              kaapi_fmt_fnc_reducor          reducor,
              kaapi_fmt_fnc_redinit          redinit,
              kaapi_fmt_fnc_get_splitter	   get_splitter
    );
  };
  
  // --------------------------------------------------------------------  
  template <class T>
  class WrapperFormat : public Format {
    WrapperFormat(kaapi_format_t* f);
  public:
    WrapperFormat();
    static const WrapperFormat<T> format;
    static const Format* get_format();
    static const kaapi_format_t* get_c_format();
    static void cstor( void* dest) { new (dest) T; }
    static void dstor( void* dest) { T* d = (T*)dest; d->T::~T(); } 
    static void cstorcopy( void* dest, const void* src) { T* s = (T*)src; new (dest) T(*s); } 
    static void copy( void* dest, const void* src) { T* d = (T*)dest; T* s = (T*)src; *d = *s; } 
    static void assign( 
                       void* dest, const kaapi_memory_view_t* view_dest, 
                       const void* src, const kaapi_memory_view_t* view_src) 
    { 
      T* d = (T*)dest; T* s = (T*)src; 
#if !defined(KAAPI_NDEBUG)
      if (view_dest->type != view_src->type)
        kaapi_abort(__LINE__, __FILE__, "*** Format, bad assignment of views with different types ");
#endif
      if (kaapi_memory_view_iscontiguous(view_dest) && kaapi_memory_view_iscontiguous(view_src))
      {
        /* size in byte */
        size_t size = kaapi_memory_view_size(view_dest) / sizeof(T);
        for (size_t i=0; i < size; ++i)
          d[i] = s[i];
      }
      else if (view_src->type == KAAPI_MEMORY_VIEW_2D)
      {
        /* size per dimension is in sizeof(T) unit */
        size_t size_i = view_src->size[0];
        size_t size_j = view_src->size[1];
        for (size_t i=0; i < size_i; ++i)
        {
          for (size_t j=0; j < size_j; ++j)
            d[j] = s[j];
          d += view_dest->lda;
          s += view_src->lda;
        }
      }
      else {
        kaapi_abort(__LINE__, __FILE__, "Format, bad assignment of unimplemented views");
      }
    } 
    static void print( FILE* file, const void* src) 
    { 
      const T* data = (const T*)src;
      std::stringstream sstr;
      sstr << *data;
      fprintf(file, "%s", sstr.str().c_str());
    } 
  };
  
  template <class UpdateFnc>
  class WrapperFormatUpdateFnc : public FormatUpdateFnc {
  protected:
    template<class UF, class T, class Y>
    static bool Caller( bool (UF::*)( T&, const Y& ), void* d, const void* v )
    {
      static UpdateFnc ufc;
      T* data = static_cast<T*>(d);
      const Y* value = static_cast<const Y*>(v);
      return ufc( *data, *value );
    }
    
  public:
    static int update_kaapi( void* data, const kaapi_format_t* fmtdata, const void* value, const kaapi_format_t* fmtvalue )
    {
      return Caller( &UpdateFnc::operator(), data, value ) ? 1 : 0;
    }
    static const FormatUpdateFnc format;
    static const FormatUpdateFnc* get_format();
  };
  
  template <class T>
  WrapperFormat<T>::WrapperFormat()
  : Format(  typeid(T).name(),
           sizeof(T),
           WrapperFormat<T>::cstor, 
           WrapperFormat<T>::dstor, 
           WrapperFormat<T>::cstorcopy, 
           WrapperFormat<T>::copy, 
           WrapperFormat<T>::assign
           )
  {}
  
  template <class T>
  WrapperFormat<T>::WrapperFormat(kaapi_format_t* f)
  : Format( f )
  {}
  
  template <class T>
  const WrapperFormat<T> WrapperFormat<T>::format;
  template <class T>
  const Format* WrapperFormat<T>::get_format() 
  { 
    return &format; 
  }
  template <class T>
  const kaapi_format_t* WrapperFormat<T>::get_c_format() 
  { 
    return format.Format::get_c_format(); 
  }  
  
  
  template <> inline const kaapi_format_t* WrapperFormat<char>::get_c_format()
  { return kaapi_char_format; }
  template <> inline const kaapi_format_t* WrapperFormat<short>::get_c_format()
  { return kaapi_char_format; }
  template <> inline const kaapi_format_t* WrapperFormat<int>::get_c_format()
  { return kaapi_shrt_format; }
  template <> inline const kaapi_format_t* WrapperFormat<long>::get_c_format()
  { return kaapi_long_format; }
  template <> inline const kaapi_format_t* WrapperFormat<unsigned char>::get_c_format()
  { return kaapi_uchar_format; }
  template <> inline const kaapi_format_t* WrapperFormat<unsigned short>::get_c_format()
  { return kaapi_ushrt_format; }
  template <> inline const kaapi_format_t* WrapperFormat<unsigned int>::get_c_format()
  { return kaapi_uint_format; }
  template <> inline const kaapi_format_t* WrapperFormat<unsigned long>::get_c_format()
  { return kaapi_ulong_format; }
  template <> inline const kaapi_format_t* WrapperFormat<float>::get_c_format()
  { return kaapi_flt_format; }
  template <> inline const kaapi_format_t* WrapperFormat<double>::get_c_format()
  { return kaapi_dbl_format; }

  
  template <class UpdateFnc>
  const FormatUpdateFnc WrapperFormatUpdateFnc<UpdateFnc>::format (
                                                                   typeid(UpdateFnc).name(),
                                                                   &WrapperFormatUpdateFnc<UpdateFnc>::update_kaapi
                                                                   );
  
  template <class UpdateFnc>
  const FormatUpdateFnc* WrapperFormatUpdateFnc<UpdateFnc>::get_format()
  {
    return &format;
  }
  
  
  // --------------------------------------------------------------------
  class Community {
  protected:
    friend class System;
    Community( void* )
    { }
    
  public:
    Community( const Community& com );
    
    /* */
    void commit();
    
    /* */
    void leave();
    
    /* */
    bool is_leader() const;
  };
  
  // --------------------------------------------------------------------
  class System {
  public:
    static Community join_community()
    throw (std::runtime_error);
    
    static Community join_community( int& argc, char**& argv )
    throw (std::runtime_error);
    
    static Community initialize_community( int& argc, char**& argv )
    throw (std::runtime_error);
    
    static Community initialize( int& argc, char**& argv )
    throw (std::runtime_error)
    { return System::initialize_community(argc, argv); }
    
    static Thread* get_current_thread();
    
    static int getRank();
    
    static void terminate();
    
    
    /** The global id of this process
     */
    static uint32_t local_gid;
  public:
    static int saved_argc;
    static char** saved_argv;
  };
  
  // --------------------------------------------------------------------
  inline Thread* System::get_current_thread()
  {
    return (Thread*)kaapi_self_thread();
  }
  
  // --------------------------------------------------------------------
  /* same method exists in thread interface */
  template<class T>
  T* Alloca(size_t size)
  {
    void* data = kaapi_data_push( kaapi_self_thread(), sizeof(T)*size );
    return new (data) T[size];
  }
  
  template<class T>
  T* Alloca()
  {
    void* data = kaapi_data_push( kaapi_self_thread(), sizeof(T) );
    return new (data) T[1];
  }
  
  // --------------------------------------------------------------------
  struct SetStack {};
  extern SetStack SetInStack;
  
  // --------------------------------------------------------------------
  struct SetHeap {};
  extern SetHeap SetInHeap;
  
  // --------------------------------------------------------------------
  class SetStickyC{};
  extern SetStickyC SetSticky;
  
  
  // --------------------------------------------------------------------
  /* type for nothing */
  struct THIS_TYPE_IS_USED_ONLY_INTERNALLY  { };
  
  // --------------------------------------------------------------------
  /* typenames for access mode */
  struct VALUE_MODE  { enum {value = KAAPI_ACCESS_MODE_V}; };
  struct READ        { enum {value = KAAPI_ACCESS_MODE_R}; };
  struct WRITE       { enum {value = KAAPI_ACCESS_MODE_W}; };
  struct READWRITE   { enum {value = KAAPI_ACCESS_MODE_RW}; };
  struct C_WRITE     { enum {value = KAAPI_ACCESS_MODE_CW}; };
  struct IC_WRITE    { enum {value = KAAPI_ACCESS_MODE_ICW}; };
  struct READ_P      { enum {value = KAAPI_ACCESS_MODE_R  |KAAPI_ACCESS_MASK_MODE_P}; };
  struct WRITE_P     { enum {value = KAAPI_ACCESS_MODE_W  |KAAPI_ACCESS_MASK_MODE_P}; };
  struct READWRITE_P { enum {value = KAAPI_ACCESS_MODE_RW |KAAPI_ACCESS_MASK_MODE_P}; };
  struct C_WRITE_P   { enum {value = KAAPI_ACCESS_MODE_CW |KAAPI_ACCESS_MASK_MODE_P}; };
  struct IC_WRITE_P  { enum {value = KAAPI_ACCESS_MODE_ICW|KAAPI_ACCESS_MASK_MODE_P}; };
  struct TMP_MODE    { enum {value = KAAPI_ACCESS_MODE_SCRATCH}; };
  struct STACK_MODE  { enum {value = KAAPI_ACCESS_MODE_STACK}; };

  /* internal name */
  typedef VALUE_MODE  ACCESS_MODE_V;
  typedef READ        ACCESS_MODE_R;
  typedef WRITE       ACCESS_MODE_W;
  typedef READWRITE   ACCESS_MODE_RW;
  typedef C_WRITE     ACCESS_MODE_CW;
  typedef IC_WRITE    ACCESS_MODE_ICW;
  typedef READ_P      ACCESS_MODE_RP;
  typedef WRITE_P     ACCESS_MODE_WP;
  typedef C_WRITE_P   ACCESS_MODE_CWP;
  typedef READWRITE_P ACCESS_MODE_RPWP;
  typedef TMP_MODE    ACCESS_MODE_SCRATCH;
  typedef STACK_MODE  ACCESS_MODE_STACK;
  
  template<class Mode>
  struct TYPEMODE2VALUE{};
  
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_V>      { enum { value = ACCESS_MODE_V::value}; };
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_R>      { enum { value = ACCESS_MODE_R::value}; };
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_W>      { enum { value = ACCESS_MODE_W::value}; };
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_RW>     { enum { value = ACCESS_MODE_RW::value}; };
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_CW>     { enum { value = ACCESS_MODE_CW::value}; };
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_ICW>    { enum { value = ACCESS_MODE_ICW::value}; };
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_RP>     { enum { value = ACCESS_MODE_RP::value}; };
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_WP>     { enum { value = ACCESS_MODE_WP::value}; };
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_RPWP>   { enum { value = ACCESS_MODE_RPWP::value}; };
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_CWP>    { enum { value = ACCESS_MODE_CWP::value}; };
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_STACK>  { enum { value = ACCESS_MODE_STACK::value}; };
  template<> struct TYPEMODE2VALUE<ACCESS_MODE_SCRATCH>{ enum { value = ACCESS_MODE_SCRATCH::value}; };
  
  template<class Mode> 
  struct IsAccessMode { static const bool value = true; };
  template<> struct IsAccessMode<ACCESS_MODE_V> { static const bool value = false; };
  
  // --------------------------------------------------------------------
  /*  Attribut is responsible for pushing or not closure into the stack thread 
  */
  class DefaultTaskAttribut {
  public:
    void operator()(kaapi_thread_t* thread, kaapi_task_body_t body, void* arg) const
    { 
      kaapi_task_push(thread, body, arg);
    }
  };
  extern DefaultTaskAttribut SetDefaultTaskAttribut;

  /* This attribut is currently supported to make experiment.
     It would be deleted in the future when the right API, under design, will be fixed.
     Currently localityDomain correspond to NUMA locality domain only...
   */
  class AttributSchedLocalityDomainTask : protected DefaultTaskAttribut {
  public:
    int          _s;     // strict or not
    unsigned int _ld;    // index of locality domain
  public:
    AttributSchedLocalityDomainTask( int s, int k ) : _ld(k) {}
    void operator()(kaapi_thread_t* thread, kaapi_task_body_t body, void* arg) const
    { 
      kaapi_task_push_withld(thread, body, arg,
        (kaapi_task_flag_t)(_s ? KAAPI_TASK_FLAG_LD_BOUND : 0),
        1+_ld
      );
    }
  };

  size_t localityDomainsCount();

  inline AttributSchedLocalityDomainTask SetLocalityDomain( unsigned int ld, int s = 0 )
  { return AttributSchedLocalityDomainTask(ld,s); }

  inline AttributSchedLocalityDomainTask SetLD( unsigned int ld, int s = 0 )
  { return SetLocalityDomain(ld,s); }

  /*
  */
  class AttributSchedUnStealableTask : protected DefaultTaskAttribut {
  public:
    AttributSchedUnStealableTask( ) {}
    void operator()(kaapi_thread_t* thread, kaapi_task_body_t body, void* arg) const
    { 
      kaapi_task_push_withflags(thread, body, arg, KAAPI_TASK_FLAG_UNSTEALABLE );
    }
  };
  
  extern AttributSchedUnStealableTask SetUnStealable;

  // --------------------------------------------------------------------
  /* OwnerComputeRule attribut: specify, during a spwan that a task should
   have the same site location of one of its parameters
   Usage: 
   * Spawn<Task>( ka::OCR( <integer i> ) (... ): try to get site information from parameter i
   */
  struct OCRAttribut : protected DefaultTaskAttribut {
  public:
    uint8_t          _s;     // strict or not
  public:
    OCRAttribut( uint8_t ith ) : _s(ith) {}
    void operator()( kaapi_thread_t* thread, kaapi_task_body_t body, void* arg ) const
    { 
      kaapi_task_t* task = _kaapi_task_push(thread, body, arg);
      task->u.s.flag |= KAAPI_TASK_FLAG_AFF_OCR;
      task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;
      task->u.s.site = 1+_s;
      kaapi_task_commit( thread );
    }
  };
  
  inline OCRAttribut OCR( uint8_t ith )
  { return OCRAttribut(ith); }


#if 0
  template<class ATTR>
  class BinaryAttributSchedUnStealableTask : protected ATTR {
    const ATTR& _a;
  public:
    BinaryAttributSchedUnStealableTask( const ATTR& a) : _a(a) {}
    void operator()( kaapi_thread_t* thread, kaapi_task_t* task ) const
    { 
      kaapi_task_set_flag( task, KAAPI_TASK_FLAG_UNSTEALABLE );// first
      ATTR::operator()(thread,task);
    }
  };

  /* This attribut is currently supported to make experiment.
     It would be deleted in the future when the right API will be designed
   */
  struct _ArchHOST {};
  struct _ArchCUDA {};
  struct _ArchHOSTCUDA {};
  static const _ArchHOST ArchHost = _ArchHOST();
  static const _ArchCUDA ArchCUDA = _ArchCUDA();

  inline _ArchHOSTCUDA operator|( const _ArchHOST&, const _ArchCUDA& )
  { return _ArchHOSTCUDA(); }

  inline _ArchHOSTCUDA operator|( const _ArchCUDA&, const _ArchHOST& )
  { return _ArchHOSTCUDA(); }

  class AttributSchedSiteArch : protected DefaultTaskAttribut {
  public:
    uint8_t  _arch_mask;   // an architecture mask
  public:
    AttributSchedSiteArch( uint8_t k ) : _arch_mask(k) {}
    void operator()( kaapi_thread_t* thread, kaapi_task_t* task ) const
    { 
      kaapi_task_set_arch_maskask, _arch_mask);
      DefaultTaskAttribut::operator()(thread,task);
    }
  };
  
  inline AttributSchedSiteArch SetArch( _ArchHOST )
  { return AttributSchedSiteArch(1U << KAAPI_PROC_TYPE_HOST); }

  inline AttributSchedSiteArch SetArch( _ArchCUDA )
  { return AttributSchedSiteArch(1U << KAAPI_PROC_TYPE_CUDA); }

  inline AttributSchedSiteArch SetArch( _ArchHOSTCUDA )
  { 
    return AttributSchedSiteArch( 
        (1U << KAAPI_PROC_TYPE_HOST)
      | (1U << KAAPI_PROC_TYPE_CUDA)
    ); 
  }
#endif

#if 0
  /* The only attribut that can be passed to task creation:
   */
  class AttributPriorityTask : protected DefaultTaskAttribut {
  public:
    unsigned char   _priority;   // priority
  public:
    AttributPriorityTask( unsigned char p ) : _priority(p) {}
    void operator()( kaapi_thread_t* thread, kaapi_task_t* task ) const
    { 
      kaapi_task_set_priority(task, _priority);
      DefaultTaskAttribut::operator()(thread, task);
    }
  };
  
  inline AttributPriorityTask SetPriority( unsigned char p )
  { return AttributPriorityTask(p); }

#endif

  // --------------------------------------------------------------------
  // A task forked with SetStaticSched attribut may have first formal
  // parameter a StaticSchedInfo returned by the runtime
  class StaticSchedInfo : public kaapi_staticschedinfo_t {
  public:
    StaticSchedInfo() {}
    
    /* return the total number of core */
    size_t count() const 
    { size_t retval = 0;
      for (int i=0; i<KAAPI_PROC_TYPE_MAX-1; ++i)
        retval += nkproc[i];
      return retval;
    }
    
    /* return the number of cpu */
    size_t count_cpu() const { return nkproc[KAAPI_PROC_TYPE_CPU]; }
    
    /* return the number of gpu */
    size_t count_gpu() const { return nkproc[KAAPI_PROC_TYPE_GPU]; }
  };

  // --------------------------------------------------------------------
  /* Static Sched attribut pass to the runtime
   Allows the user to specified number of ressources (at least 
   number threads) which may be scheduled on typed ressources.
   - 4 kinds of constraints that can be specify by the user:
   1. total number of ressources: anonymous ressources that can be scheduled
   on CPU or GPU.
   The user asks for a given number of threads that will be scheduled
   on the available ressources (CPU or GPU) depending of its tasks and
   available ressources at runtime. 
   2. total number of CPUs and (not inclusive) total number of GPUs.
   In that case, the user asks for a fixed number of Ncpus threads and Ngpus threads
   that will be scheduled on the specified architecture.
   Because if CPU is always available, it is possible that no GPU is available.
   It is strongly recommanded that the user tests the number of ressources passed from
   the runtime to the user if it declares its tasks with SchedInfo attribute.
   3. AutoCPU | AutoGPU: detection by the runtime of the number of available ressources.
   Using this specification, the user will received in its schedinfo task's parameter
   the number of available ressources for its task's execution. The number of ressources
   given by the runtime will never exeed the physical number of ressources.
   4. No specification: is equivalent to AutoCPU | AutoGPU.
   
   In all the cases, if the physical ressources are less than the requested number, then the user's 
   threads will be scheduled non preemptively on the physical ressources.
   
   All requested ressources or the ressources allocated by the runtime are lineary numbered
   from 0 to N-1, where N is the total number of ressources. The first set of ressources
   from 0 to Ncpu-1 corresponds to the CPU ressources. The second set of ressources from
   Ncpu to Ngpu+Ncpu-1 corresponds to the GPU ressources. Both Ncpu and Ngpu are accessible
   through the sched info data structure.
   
   Implementation note.
   -1: means auto detect ressources
   -2: means all ressources of the given type
   */
  class SetStaticSchedAttribut {
    int16_t  _nress;  /* N ressources are requested by the user, -1: fixed by the runtime */
    int16_t  _ncpu;   /* N CPU requested by the user, -1 fixed by the runtime */
    int16_t  _ngpu;   /* N GPU requested by the user, -1 fixed by the runtime */
  public:
    SetStaticSchedAttribut( int nc, int ng )
    : _nress(-1), _ncpu(nc), _ngpu(ng)
    {}
    /* format for N ressources */
    SetStaticSchedAttribut( int n )
    : _nress(n), _ncpu(-1), _ngpu(-1)
    {}
    SetStaticSchedAttribut( )
    : _nress(-1), _ncpu(-1), _ngpu(-1)
    {}
    void operator()( kaapi_thread_t* thread, kaapi_task_body_t body, void* arg ) const
    { 
      /* push a task that will encapsulated the execution of the top task */
      kaapi_staticschedtask_arg_t* sdarg
      = (kaapi_staticschedtask_arg_t*)kaapi_data_push( thread, sizeof(kaapi_staticschedtask_arg_t) );
      sdarg->sub_arg  = arg;
      sdarg->sub_body = body;
      sdarg->sub_fmt  = 0;
      sdarg->schedinfo.nkproc[0]                   = (uint32_t)_nress;
      sdarg->schedinfo.nkproc[KAAPI_PROC_TYPE_CPU] = (uint32_t)_ncpu;
      sdarg->schedinfo.nkproc[KAAPI_PROC_TYPE_GPU] = (uint32_t)_ngpu;
      kaapi_task_push(thread, (kaapi_task_body_t)kaapi_staticschedtask_body, sdarg);
    }
  };

  /* */
  struct _KaapiCPU_Encode {
    _KaapiCPU_Encode() : ncpu(-1) {}
    _KaapiCPU_Encode( int16_t nc ) : ncpu(nc) {}
    int16_t ncpu;
  };
  
  /* */
  struct _KaapiGPU_Encode {
    _KaapiGPU_Encode() : ngpu(-1) {}
    _KaapiGPU_Encode( int16_t ng ) : ngpu(ng) {}
    int16_t ngpu;
  };
  
  /* */
  struct _KaapiCPUGPU_Encode : public _KaapiCPU_Encode, public _KaapiGPU_Encode {
    _KaapiCPUGPU_Encode() {}
    _KaapiCPUGPU_Encode( _KaapiCPU_Encode nc ) 
    : _KaapiCPU_Encode(nc) {}
    _KaapiCPUGPU_Encode( _KaapiGPU_Encode ng ) 
    : _KaapiGPU_Encode(ng) {}
    _KaapiCPUGPU_Encode( _KaapiCPU_Encode nc, _KaapiGPU_Encode ng ) 
    : _KaapiCPU_Encode(nc), _KaapiGPU_Encode(ng) {}
  };
  
  static const _KaapiCPU_Encode AutoCPU = _KaapiCPU_Encode();
  static const _KaapiGPU_Encode AutoGPU = _KaapiGPU_Encode();
  static const _KaapiCPU_Encode AllCPU  = _KaapiCPU_Encode(-2);
  static const _KaapiGPU_Encode AllGPU  = _KaapiGPU_Encode(-2);
  inline _KaapiCPU_Encode SetnCPU(int nc) { return _KaapiCPU_Encode(nc); }
  inline _KaapiGPU_Encode SetnGPU(int ng) { return _KaapiGPU_Encode(ng); }
  
  inline _KaapiCPUGPU_Encode operator|( const _KaapiCPU_Encode nc, const _KaapiGPU_Encode ng )
  { return _KaapiCPUGPU_Encode(nc,ng); }
  inline _KaapiCPUGPU_Encode operator|( const _KaapiGPU_Encode ng, const _KaapiCPU_Encode nc )
  { return _KaapiCPUGPU_Encode(nc,ng); }
  
  /* user level attribut definition */
  inline SetStaticSchedAttribut SetStaticSched(int n)
  { return SetStaticSchedAttribut(n); }
  inline SetStaticSchedAttribut SetStaticSched( _KaapiCPU_Encode xx )
  { return SetStaticSchedAttribut(xx.ncpu, 0); }
  inline SetStaticSchedAttribut SetStaticSched( _KaapiGPU_Encode xx )
  { return SetStaticSchedAttribut(0, xx.ngpu); }
  inline SetStaticSchedAttribut SetStaticSched( _KaapiCPUGPU_Encode xx )
  { return SetStaticSchedAttribut(xx.ncpu, xx.ngpu); }
  inline SetStaticSchedAttribut SetStaticSched()
  { return SetStaticSchedAttribut(); }

  /* */
  template<class T>
  struct DefaultAdd {
    static void init_neutral( T& value ) { new (&value)T; }
    void operator()( T& result, const T& value)
    { result += value; }
  };

#define KAXX_SPECIALIZED_SCALAR(TYPE) \
  template<>\
  struct DefaultAdd<TYPE> {\
    static void init_neutral( TYPE& value ) { value = 0; } \
    void operator()( TYPE& result, const TYPE& value) \
    { result += value; }\
  };

  KAXX_SPECIALIZED_SCALAR(char)
  KAXX_SPECIALIZED_SCALAR(unsigned char)
  KAXX_SPECIALIZED_SCALAR(short)
  KAXX_SPECIALIZED_SCALAR(unsigned short)
  KAXX_SPECIALIZED_SCALAR(int)
  KAXX_SPECIALIZED_SCALAR(unsigned int)
  KAXX_SPECIALIZED_SCALAR(long)
  KAXX_SPECIALIZED_SCALAR(unsigned long)
  KAXX_SPECIALIZED_SCALAR(long long)
  KAXX_SPECIALIZED_SCALAR(unsigned long long)
  KAXX_SPECIALIZED_SCALAR(float)
  KAXX_SPECIALIZED_SCALAR(double)

  /* fwd declarations */
  template<class T>
  class pointer;
  template<class T>
  class pointer_rpwp;
  template<class T>
  class pointer_rw;
  template<class T>
  class pointer_rp;
  template<class T>
  class pointer_r;
  template<class T>
  class pointer_wp;
  template<class T>
  class pointer_w;
  template<class T>
  class pointer_cwp;
  template<class T, typename OP=DefaultAdd<T> >
  class pointer_cw;
  template<class T, typename OP=DefaultAdd<T> >
  class pointer_icw;
  template<class T>
  class pointer_t;
  template<class T>
  class pointer_stack;

  // --------------------------------------------------------------------
  template<class T> 
  struct TraitIsStatic { static const bool value = true; };
  
} // end of namespace ka

#include "ka_api_array.h"

namespace ka {
  
  template<int dim, class T, Storage2DClass S> 
  struct TraitIsStatic<array<dim,T,S> > { static const bool value = false; };
  
  // --------------------------------------------------------------------
  template<class T>
  struct base_pointer {
    base_pointer() : _ptr(0)
    {}
    base_pointer( T* p ) : _ptr(p)
    {}
    T* ptr() const { return _ptr; }
    void ptr(T* p) { _ptr = p; }
  protected:
    mutable T* _ptr;
  };
  
  /* capture write */
  template<class T>
  class value_ref {
  public:
    value_ref(T* p) : _ptr(p){}
    operator T&() { return *_ptr; }
    void operator=( const T& value ) { *_ptr = value; }
  protected:
    T* _ptr;
  };
  
  /* capture cumulative write */
  template<class T,class OP>
  class cumul_value_ref {
  public:
    cumul_value_ref( pointer_cw<T,OP>& p ) : _ptr(p) {}
    cumul_value_ref( pointer_icw<T,OP>& p ) : _ptr((pointer_cw<T,OP>&)p) {}
    void operator+=( const T& value );
    template<typename OP2>
    void cumul( const T& value );
  protected:
    pointer_cw<T,OP>& _ptr; /* the order of field should correspond to order of pointer_cw */
  };
  
  
#define KAAPI_POINTER_ARITHMETIC_METHODS \
  Self_t& operator++() { ++base_pointer<T>::_ptr; return *this; }\
  Self_t operator++(int) { return base_pointer<T>::_ptr++; }\
  Self_t& operator--() { --base_pointer<T>::_ptr; return *this; }\
  Self_t operator--(int) { return base_pointer<T>::_ptr--; }\
  Self_t operator+(int i) const { return base_pointer<T>::_ptr+i; }\
  Self_t operator+(unsigned int i) const { return base_pointer<T>::_ptr+i; }\
  Self_t operator+(long i) const { return base_pointer<T>::_ptr+i; }\
  Self_t operator+(unsigned long i) const { return base_pointer<T>::_ptr+i; }\
  Self_t& operator+=(int i) { base_pointer<T>::_ptr+=i; return *this; }\
  Self_t& operator+=(unsigned int i) { base_pointer<T>::_ptr+=i; return *this; }\
  Self_t& operator+=(long i) { base_pointer<T>::_ptr+=i; return *this; }\
  Self_t& operator+=(unsigned long i) { base_pointer<T>::_ptr+=i; return *this; }\
  Self_t operator-(int i) const { return base_pointer<T>::_ptr-i; }\
  Self_t operator-(unsigned int i) const { return base_pointer<T>::_ptr-i; }\
  Self_t operator-(long i) const { return base_pointer<T>::_ptr-i; }\
  Self_t operator-(unsigned long i) const { return base_pointer<T>::_ptr-i; }\
  Self_t& operator-=(int i) { return base_pointer<T>::_ptr-=i; }\
  Self_t& operator-=(unsigned int i) { return base_pointer<T>::_ptr-=i; }\
  Self_t& operator-=(long i) { return base_pointer<T>::_ptr-=i; }\
  Self_t& operator-=(unsigned long i) { return base_pointer<T>::_ptr-=i; }\
  difference_type operator-(const Self_t& p) const { return base_pointer<T>::_ptr-p._ptr; }\
  bool operator==(const Self_t& p) const { return base_pointer<T>::_ptr == p._ptr; }\
  bool operator!=(const Self_t& p) const { return base_pointer<T>::_ptr != p._ptr; }
  
  
  // --------------------------------------------------------------------
  /* Information notes.
   - Access mode types (ka::W, ka::WP, ka::RW..) are defined to be used 
   in signature definition of tasks. They cannot be used to declare 
   variables or used as effective parameters during a spawn.
   - Effective parameters should be pointer in order to force verification
   of the parameter passing rules between effective parameters and formal parameters.
   They are closed to the Shared types of the previous Athapascan API but 
   may be used like normal pointer (arithmetic + deferencing of pointers).
   */
  // --------------------------------------------------------------------
  template<class T>
  class pointer : public base_pointer<T> {
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef pointer<T> Self_t;
    pointer() : base_pointer<T>() {}
    pointer( value_type* ptr ) : base_pointer<T>(ptr) {}
    //    operator value_type*() { return base_pointer<T>::ptr(); }
    
    KAAPI_POINTER_ARITHMETIC_METHODS
  };
  
  
  // --------------------------------------------------------------------
  template<class T>
  class pointer_rpwp : public base_pointer<T> {
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef pointer_rpwp<T> Self_t;
    
    pointer_rpwp() : base_pointer<T>() {}
    pointer_rpwp( value_type* ptr ) : base_pointer<T>(ptr) {}
    explicit pointer_rpwp( kaapi_access_t& ptr ) : base_pointer<T>(kaapi_data(value_type, &ptr)) {}
    //    operator value_type*() { return base_pointer<T>::ptr(); }
    
    KAAPI_POINTER_ARITHMETIC_METHODS
  };
  
  
  // --------------------------------------------------------------------
  template<class T>
  class pointer_rw: public base_pointer<T> {
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef pointer_rw<T> Self_t;
    
    pointer_rw() : base_pointer<T>() {}
    pointer_rw( value_type* ptr ) : base_pointer<T>(ptr) {}
    explicit pointer_rw( kaapi_access_t& ptr ) : base_pointer<T>(kaapi_data(value_type, &ptr)) {}
    pointer_rw( const pointer_rpwp<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_rw( const pointer<T>& ptr ) : base_pointer<T>(ptr) {}
    operator value_type*() { return base_pointer<T>::ptr(); }
    T* operator->() { return base_pointer<T>::ptr(); }
    value_type& operator*() { return *base_pointer<T>::ptr(); }
    value_type& operator[](int i) { return base_pointer<T>::ptr()[i]; }
    value_type& operator[](long i) { return base_pointer<T>::ptr()[i]; }
    value_type& operator[](difference_type i) { return base_pointer<T>::ptr()[i]; }
    
    KAAPI_POINTER_ARITHMETIC_METHODS
  };
  
  
  // --------------------------------------------------------------------
  template<class T>
  class pointer_rp : public base_pointer<T> {
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef pointer_rp<T> Self_t;
    
    pointer_rp() : base_pointer<T>() {}
    pointer_rp( value_type* ptr ) : base_pointer<T>(ptr) {}
    explicit pointer_rp( kaapi_access_t& ptr ) : base_pointer<T>(kaapi_data(value_type, &ptr)) {}
    pointer_rp( const pointer_rpwp<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_rp( const pointer<T>& ptr ) : base_pointer<T>(ptr) {}
    //    operator value_type*() { return base_pointer<T>::ptr(); }
    
    KAAPI_POINTER_ARITHMETIC_METHODS
  };
  
  
  // --------------------------------------------------------------------
  template<class T>
  class pointer_r : public base_pointer<T> {
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef pointer_r<T> Self_t;
    
    pointer_r() : base_pointer<T>() {}
//    pointer_r( value_type* ptr ) : base_pointer<T>(ptr) {}
    pointer_r( const value_type* ptr ) : base_pointer<T>((T*)ptr) {}
    explicit pointer_r( kaapi_access_t& ptr ) : base_pointer<T>(kaapi_data(value_type, &ptr)) {}
    pointer_r( const pointer_rpwp<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_r( const pointer<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_r( const pointer_rp<T>& ptr ) : base_pointer<T>(ptr) {}
    const T* operator->() { return base_pointer<T>::ptr(); }
    operator const value_type*() const { return base_pointer<T>::ptr(); }
    const T& operator*() const { return *base_pointer<T>::ptr(); }
    const T* operator->() const { return base_pointer<T>::ptr(); }
    const T& operator[](int i) const { return base_pointer<T>::ptr()[i]; }
    const T& operator[](long i) const { return base_pointer<T>::ptr()[i]; }
    const T& operator[](difference_type i) const { return base_pointer<T>::ptr()[i]; }
    
    KAAPI_POINTER_ARITHMETIC_METHODS
  };
  
  
  // --------------------------------------------------------------------
  template<class T>
  class pointer_wp : public base_pointer<T> {
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef pointer_wp<T> Self_t;
    
    pointer_wp() : base_pointer<T>() {}
    pointer_wp( value_type* ptr ) : base_pointer<T>(ptr) {}
    explicit pointer_wp( kaapi_access_t& ptr ) : base_pointer<T>(kaapi_data(value_type, &ptr)) {}
    pointer_wp( const pointer_rpwp<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_wp( const pointer<T>& ptr ) : base_pointer<T>(ptr) {}
    //    operator value_type*() { return base_pointer<T>::ptr(); }
    
    KAAPI_POINTER_ARITHMETIC_METHODS
  };
  
  
  // --------------------------------------------------------------------
  template<class T>
  class pointer_w : public base_pointer<T> {
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef pointer_w<T> Self_t;
    
    pointer_w() : base_pointer<T>() {}
    pointer_w( value_type* ptr ) : base_pointer<T>(ptr) {}
    explicit pointer_w( kaapi_access_t& ptr ) : base_pointer<T>(kaapi_data(value_type, &ptr)) {}
    pointer_w( const pointer_rpwp<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_w( const pointer<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_w( const pointer_wp<T>& ptr ) : base_pointer<T>(ptr) {}
    operator value_type*() { return base_pointer<T>::ptr(); }
    T* operator->() { return base_pointer<T>::ptr(); }
    value_ref<T> operator*() { return value_ref<T>(base_pointer<T>::ptr()); }
    value_ref<T> operator[](int i) { return value_ref<T>(base_pointer<T>::ptr()+i); }
    value_ref<T> operator[](long i) { return value_ref<T>(base_pointer<T>::ptr()+i); }
    value_ref<T> operator[](difference_type i) { return value_ref<T>(base_pointer<T>::ptr()+i); }
    
    KAAPI_POINTER_ARITHMETIC_METHODS
  };
  
  
  // --------------------------------------------------------------------
  template<class T>
  class pointer_cwp : public base_pointer<T> {
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef pointer_cwp<T> Self_t;
    
    pointer_cwp() : base_pointer<T>() {}
    pointer_cwp( value_type* ptr ) : base_pointer<T>(ptr) {}
    explicit pointer_cwp( kaapi_access_t& ptr ) : base_pointer<T>(kaapi_data(value_type, &ptr)) {}
    pointer_cwp( const pointer_rpwp<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_cwp( const pointer<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_cwp( const pointer_cwp<T>& ptr ) : base_pointer<T>(ptr) {}
    
    KAAPI_POINTER_ARITHMETIC_METHODS
  };
  
  
  // --------------------------------------------------------------------  
  template<class T, typename OP >
  class pointer_cw: public base_pointer<T> {
    friend class cumul_value_ref<T,OP>;
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef pointer_cw<T> Self_t;
    
    pointer_cw() : base_pointer<T>() {}
    pointer_cw( value_type* ptr ) : base_pointer<T>(ptr) {}
    /* to call user define task */
    explicit pointer_cw( kaapi_access_t& ptr ) : base_pointer<T>(kaapi_data(value_type, &ptr)) {}
    pointer_cw( const pointer_rpwp<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_cw( const pointer_cwp<T>& ptr ) : base_pointer<T>(ptr)  {}
    template<class OP2>
    pointer_cw( const pointer_cw<T, OP2>& ptr ) : base_pointer<T>(ptr) {}
    
    /* be carrefull here: pointer_cw and cumul_value_ref should be identic */
    cumul_value_ref<T,OP> operator*() 
    { return cumul_value_ref<T,OP>(*this); }
    cumul_value_ref<T,OP>* operator->() 
    { cumul_value_ref<T,OP>(*this); }
    
    KAAPI_POINTER_ARITHMETIC_METHODS
  };

  // --------------------------------------------------------------------  
  template<class T, typename OP >
  class pointer_icw: public base_pointer<T> {
    friend class cumul_value_ref<T,OP>;
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef pointer_icw<T> Self_t;

    pointer_icw() : base_pointer<T>() {}
    pointer_icw( value_type* ptr ) : base_pointer<T>(ptr) {}
    /* to call user define task */
    explicit pointer_icw( kaapi_access_t& ptr ) : base_pointer<T>(kaapi_data(value_type, &ptr)) {}
    pointer_icw( const pointer_rpwp<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_icw( const pointer_cwp<T>& ptr ) : base_pointer<T>(ptr)  {}
    template<class OP2>
    pointer_icw( const pointer_icw<T, OP2>& ptr ) : base_pointer<T>(ptr) {}

    /* be carrefull here: pointer_cw and cumul_value_ref should be identic */
    cumul_value_ref<T,OP> operator*() 
    { return cumul_value_ref<T,OP>(*this); }
    cumul_value_ref<T,OP>* operator->() 
    { cumul_value_ref<T,OP>(*this); }

    KAAPI_POINTER_ARITHMETIC_METHODS
  };

  template<class T, typename OP >
  void cumul_value_ref<T,OP>::operator+=( const T& value ) 
  { 
    *_ptr.ptr() += value; 
  }
  template<class T, typename OP >
  template<typename OP2>
  void cumul_value_ref<T,OP>::cumul( const T& value ) 
  {
    OP2()(*_ptr.ptr(),value);
  }
  
  
  
  // --------------------------------------------------------------------
  template<class T>
  class pointer_stack: public base_pointer<T> {
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef pointer_stack<T> Self_t;
    
    pointer_stack() : base_pointer<T>() {}
    pointer_stack( value_type* ptr ) : base_pointer<T>(ptr) {}
    explicit pointer_stack( kaapi_access_t& ptr ) : base_pointer<T>(kaapi_data(value_type, &ptr)) {}
    pointer_stack( const pointer_rpwp<T>& ptr ) : base_pointer<T>(ptr) {}
    pointer_stack( const pointer<T>& ptr ) : base_pointer<T>(ptr) {}
    operator value_type*() { return base_pointer<T>::ptr(); }
    T* operator->() { return base_pointer<T>::ptr(); }
    value_type& operator*() { return *base_pointer<T>::ptr(); }
    value_type& operator[](int i) { return base_pointer<T>::ptr()[i]; }
    value_type& operator[](long i) { return base_pointer<T>::ptr()[i]; }
    value_type& operator[](difference_type i) { return base_pointer<T>::ptr()[i]; }
    
    KAAPI_POINTER_ARITHMETIC_METHODS
  };


  // --------------------------------------------------------------------  
  /* here requires to distinguish pointer to object from pointer to function */
  template<class R> struct __kaapi_is_function { enum { value = false }; };
  template<class R> struct __kaapi_is_function<R (*)()> { enum { value = true }; };
  template<class R> struct __kaapi_is_function<R (*)(...)> { enum { value = true }; };
  template<class R, class T0> 
  struct __kaapi_is_function<R (*)(T0)> { enum { value = true }; };
  template<class R, class T0, class T1> 
  struct __kaapi_is_function<R (*)(T0, T1)> { enum { value = true }; };
  template<class R, class T0, class T1, class T2> 
  struct __kaapi_is_function<R (*)(T0, T1, T2)> { enum { value = true }; };
  template<class R, class T0, class T1, class T2, class T3> 
  struct __kaapi_is_function<R (*)(T0, T1, T2, T3)> { enum { value = true }; };
  template<class R, class T0, class T1, class T2, class T3, class T4>
  struct __kaapi_is_function<R (*)(T0, T1, T2, T3, T4)> { enum { value = true }; };
  template<class R, class T0, class T1, class T2, class T3, class T4, class T5>
  struct __kaapi_is_function<R (*)(T0, T1, T2, T3, T4, T5)> { enum { value = true }; };
  template<class R, class T0, class T1, class T2, class T3, class T4, class T5, class T6>
  struct __kaapi_is_function<R (*)(T0, T1, T2, T3,T4, T5, T6)> { enum { value = true }; };
  template<class R, class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7> 
  struct __kaapi_is_function<R (*)(T0, T1, T2, T3, T4, T5, T6, T7)> { enum { value = true }; };
  template<class R, class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8> 
  struct __kaapi_is_function<R (*)(T0, T1, T2, T3, T4, T5, T6, T7, T8)> { enum { value = true }; };
  template<class R, class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9> 
  struct __kaapi_is_function<R (*)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9)> { enum { value = true }; };
  template<class R, class T0, class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9, class T10> 
  struct __kaapi_is_function<R (*)(T0, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10)> { enum { value = true }; };
  
  
  // --------------------------------------------------------------------
  template<class T>
  struct TraitNoDestroyTask {
    enum { value = false };
  };
  
  /* user may specialize this trait to avoid spawn of delete for some object */
  template<> struct TraitNoDestroyTask<char> { enum { value = true}; };
  template<> struct TraitNoDestroyTask<short> { enum { value = true}; };
  template<> struct TraitNoDestroyTask<int> { enum { value = true}; };
  template<> struct TraitNoDestroyTask<long> { enum { value = true}; };
  template<> struct TraitNoDestroyTask<unsigned char> { enum { value = true}; };
  template<> struct TraitNoDestroyTask<unsigned short> { enum { value = true}; };
  template<> struct TraitNoDestroyTask<unsigned int> { enum { value = true}; };
  template<> struct TraitNoDestroyTask<unsigned long> { enum { value = true}; };
#if defined(__APPLE__) && defined(__ppc__) && defined(__GNUC__)
#else  
  template<> struct TraitNoDestroyTask<unsigned long long> { enum { value = true}; };
  template<> struct TraitNoDestroyTask<long long> { enum { value = true}; };
#endif
  template<> struct TraitNoDestroyTask<float> { enum { value = true}; };
  template<> struct TraitNoDestroyTask<double> { enum { value = true}; };
  
  /* case of pointer_XX to pointer to object */
  template<typename T> struct TraitNoDestroyTask<const T*> { enum { value = true}; };
  template<typename T> struct TraitNoDestroyTask<T*> { enum { value = true}; };
  
  /* autopointer: like a pointer, except that it spawn delete task
   at the end of the definition cope
   */
  template<class T>
  class auto_pointer : public pointer<T> {
  public:
    typedef T value_type;
    typedef size_t difference_type;
    typedef auto_pointer<T> Self_t;
    auto_pointer() : pointer<T>() {}
    ~auto_pointer();
    auto_pointer( value_type* ptr ) : pointer<T>(ptr) {}
    operator value_type*() { return pointer<T>::ptr(); }
    operator const value_type*() const { return pointer<T>::ptr(); }
    
    KAAPI_POINTER_ARITHMETIC_METHODS
  };
  
  template<class T>
  class auto_variable {
    T* _var;
  public:
    typedef T value_type;
    typedef auto_variable<T> Self_t;
    auto_variable() 
    {
      void* data = kaapi_data_push( kaapi_self_thread(), sizeof(T) );
      _var = new (data) T;
    }
    auto_variable(const T& value) 
    {
      void* data = kaapi_data_push( kaapi_self_thread(), sizeof(T) );
      _var = new (data) T(value);
    }
    auto_variable( Thread* thread ) 
    {
      void* data = kaapi_data_push( (kaapi_thread_t*)thread, sizeof(T) );
      _var = new (data) T;
    }
    auto_variable( Thread* thread, const T& value) 
    {
      void* data = kaapi_data_push( (kaapi_thread_t*)thread, sizeof(T) );
      _var = new (data) T(value);
    }
    ~auto_variable();
    operator const T&() const { return *_var; }
    operator T&() { return *_var; }
    T& operator=( const T& value) { *_var = value; return *_var; }
    T* operator&() { return _var; }
    
    /* clear variable: used for debug only */
    void clear() { _var = 0; }    
  };
  
  
  // --------------------------------------------------------------------
  /** Defined in order to used automatically generated recopy in Universal Access Mode Type constructor :
   - to convert TypeEff -> TypeInTask.
   - and to convert TypeInTask -> TypeFormal.
   */
  struct Access : public kaapi_access_t {
    Access( const Access& access ) : kaapi_access_t(access)
    { }
    template<typename T>
    explicit Access( const base_pointer<T>& p )
    { kaapi_access_init(this, p.ptr()); }
    
    template<typename pointer>
    explicit Access( pointer* p )
    { kaapi_access_init(this, p); }
    
    template<typename pointer>
    explicit Access( const pointer* p )
    { kaapi_access_init(this, (void*)p); }
    
    void operator=( const kaapi_access_t& a)
    { data = a.data; version = a.version; }
  };
  
  // --------------------------------------------------------------------
  /* Helpers to declare type in signature of task */
  template<typename UserType=void> struct Value {};
  template<typename UserType=void> struct RPWP {};
  template<typename UserType=void> struct RP {};
  template<typename UserType=void> struct R  {};
  template<typename UserType=void> struct WP {};
  template<typename UserType=void> struct W {};
  template<typename UserType=void> struct RW {};
  template<typename UserType=void, typename OpCumul = DefaultAdd<UserType> > struct CW {};
  template<typename UserType=void, typename OpCumul = DefaultAdd<UserType> > struct ICW {};
  template<typename UserType=void> struct CWP {};
  template<typename UserType=void> struct SCRATCH {};
  template<typename UserType=void> struct ST {};
  
  
  // --------------------------------------------------------------------
  /** Trait to encode operations / types required to spawn task
   *  TraitFormalParam<T>::type_t gives type of the underlaying C++ object. If not pointer, this is T
   *  TraitFormalParam<T>::formal_t gives type of the formal parameter
   *  TraitFormalParam<T>::mode_t gives type of the access mode of T
   *  TraitFormalParam<T>::type_inclosure_t gives type of C++ object store in the task argument
   *  TraitFormalParam<T>::is_static retuns true or false if the type does not contains a dynamic set 
   of pointer access.
   *  TraitFormalParam<T>::handl2data used when pointer to data for shared object are pointer to pointer
   to data. Should return a type formal_t after interpretation of the type in closure.
   
   *  TraitFormalParam<T>::get_data return the address in the task argument data structure of 
   the i-th data parameter
   *  TraitFormalParam<T>::get_version return the address in the task argument data structure of 
   the i-th version parameter
   *  TraitFormalParam<T>::get_nparam( typeinclosure* ): size_t
   *  TraitFormalParam<T>::is_access( typeinclosure* ): bool
   
   During the creation of task with a formal parameter fi of type Fi, then the task argument data structure
   stores an object of type TraitFormalParam<T>::type_inclosure_t called taskarg->fi.
   Then, to bind the effective parameter ei of type Ei to fi:
   1- the effective argument ei is bind to the type into the closure:
   new (&taskarg->fi) TraitFormalParam<T>::type_inclosure_t( ei )
   2- the object store into the task argument data structure is binded to the formal parameter during
   bootstrap code to execute a task body: taskbody( ..., taskarg->fi, ... )
   It means that the type to the closure should be convertible to the type of the formal parameter.        
   */
  template<typename T>
  struct TraitFormalParam { 
    typedef T                type_t; 
    typedef T                signature_t; 
    typedef T                formal_t; 
    typedef ACCESS_MODE_V    mode_t; 
    typedef T                type_inclosure_t; 
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const T* a ) { return 0; }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return (void*)a; }
    static void*             get_version( type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) {}
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t* view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };
  
  template<typename T>
  struct TraitFormalParam<Value<T> > { 
    typedef T                type_t; 
    typedef T                signature_t; 
    typedef T                formal_t; 
    typedef ACCESS_MODE_V    mode_t; 
    typedef T                type_inclosure_t; 
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const T* a ) { return 0; }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return (void*)a; }
    static void*             get_version( type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) {}
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t* view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };
  
  template<typename T>
  struct TraitFormalParam<const T&> { 
    typedef T                type_t; 
    typedef T                signature_t; 
    typedef const T&         formal_t; 
    typedef ACCESS_MODE_V    mode_t; 
    typedef T                type_inclosure_t; 
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const T* a ) { return 0; }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return (void*)a; }
    static void*             get_version( type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) {}
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { return kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };
  
  template<typename T>
  struct TraitFormalParam<const T> { 
    typedef T                type_t; 
    typedef const T          signature_t; 
    typedef const T          formal_t; 
    typedef ACCESS_MODE_V    mode_t; 
    typedef T                type_inclosure_t; 
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const T* a ) { return 0; }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return (void*)a; }
    static void*             get_version( type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) {}
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };
  
  template<typename T>
  struct TraitFormalParam<pointer<T> > { 
    typedef T                type_t; 
    typedef RPWP<T>          signature_t; 
    typedef ACCESS_MODE_RPWP mode_t; 
    typedef Access           type_inclosure_t; 
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const pointer<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
  };
  
  template<typename T>
  struct TraitFormalParam<auto_pointer<T> > { 
    typedef T                type_t; 
    typedef RPWP<T>          signature_t; 
    typedef ACCESS_MODE_RPWP mode_t; 
    typedef Access           type_inclosure_t; 
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const auto_pointer<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
  };
  
  template<typename T>
  struct TraitFormalParam<pointer_r<T> > { 
    typedef T                type_t; 
    typedef R<T>             signature_t; 
    typedef pointer_r<T>     formal_t; 
    typedef ACCESS_MODE_R    mode_t; 
    typedef Access           type_inclosure_t;  /* could be only one pointer without version */
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const pointer_r<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)a; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) { *a = *r; }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };
  
  template<typename T>
  struct TraitFormalParam<pointer_rp<T> > { 
    typedef T                type_t; 
    typedef RP<T>            signature_t; 
    typedef pointer_rp<T>    formal_t; 
    typedef ACCESS_MODE_RP   mode_t; 
    typedef Access           type_inclosure_t;  /* could be only one pointer without version */
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const pointer_rp<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)a; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) { *a = *r; }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };
  
  template<typename T>
  struct TraitFormalParam<pointer_w<T> > { 
    typedef T                type_t; 
    typedef W<T>             signature_t; 
    typedef pointer_w<T>     formal_t; 
    typedef ACCESS_MODE_W    mode_t; 
    typedef Access           type_inclosure_t;  /* could be only one pointer without version */
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const pointer_w<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)a; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) { *a = *r; }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };
  
  template<typename T>
  struct TraitFormalParam<pointer_wp<T> > { 
    typedef T                type_t; 
    typedef WP<T>            signature_t; 
    typedef pointer_wp<T>    formal_t; 
    typedef ACCESS_MODE_WP   mode_t; 
    typedef Access           type_inclosure_t;  /* could be only one pointer without version */
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const pointer_wp<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)a; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) { *a = *r; }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };
  
  template<typename T>
  struct TraitFormalParam<pointer_rw<T> > { 
    typedef T                type_t; 
    typedef RW<T>            signature_t; 
    typedef pointer_rw<T>    formal_t; 
    typedef ACCESS_MODE_RW   mode_t; 
    typedef Access           type_inclosure_t;  /* could be only one pointer without version */
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const pointer_rw<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)a; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) { *a = *r; }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };

  template<typename T>
  struct TraitFormalParam<pointer_rpwp<T> > {
    typedef T                type_t; 
    typedef RPWP<T>          signature_t; 
    typedef pointer_rpwp<T>  formal_t; 
    typedef ACCESS_MODE_RPWP mode_t; 
    typedef Access           type_inclosure_t; 
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const pointer_rpwp<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)a; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) { *a = *r; }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };  
  
  template<typename T, typename OP>
  struct TraitFormalParam<pointer_cw<T,OP> > { 
    typedef T                type_t; 
    typedef CW<T,OP>         signature_t; 
    typedef pointer_cw<T,OP> formal_t; 
    typedef ACCESS_MODE_CW   mode_t; 
    typedef Access           type_inclosure_t;  /* could be only one pointer without version */
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const pointer_cw<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return a->data; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i , kaapi_memory_view_t*  view)
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)a; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) { *a = *r; }
    static void              reducor_fnc(void* result, const void* value) 
    { T* r = static_cast<T*> (result); 
      const T* v = static_cast<const T*> (value);
      OP()(*r, *v);
    }
    static void              redinit_fnc(void* value) 
    { 
      OP::init_neutral(*(T*)value);
    }
  };

 template<typename T, typename OP>
  struct TraitFormalParam<pointer_icw<T,OP> > { 
    typedef T                 type_t; 
    typedef ICW<T,OP>         signature_t; 
    typedef pointer_icw<T,OP> formal_t; 
    typedef ACCESS_MODE_ICW   mode_t; 
    typedef Access            type_inclosure_t;  /* could be only one pointer without version */
    static const bool         is_static = TraitIsStatic<T>::value;
    static const void*        ptr( const pointer_icw<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)a; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) { *a = *r; }
    static void              reducor_fnc(void* result, const void* value) 
    { T* r = static_cast<T*> (result); 
      const T* v = static_cast<const T*> (value);
      OP()(*r, *v);
      kaapi_abort(__LINE__, __FILE__, "invalid case"); // ICW never requires reduction
    }
    static void              redinit_fnc(void* value) 
    { 
      OP::init_neutral(*(T*)value);
      kaapi_abort(__LINE__, __FILE__, "invalid case"); // ICW never requires reduction
    }
  };

  template<typename T>
  struct TraitFormalParam<pointer_cwp<T> > {
    typedef T                type_t; 
    typedef CWP<T>           signature_t; 
    typedef pointer_cwp<T>   formal_t; 
    typedef ACCESS_MODE_CWP  mode_t; 
    typedef Access           type_inclosure_t; 
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const pointer_cwp<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)a; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) { *a = *r; }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void* value) { }
  }; 

  template<typename T>
  struct TraitFormalParam<pointer_stack<T> > { 
    typedef T                type_t; 
    typedef ST<T>            signature_t; 
    typedef pointer_stack<T> formal_t; 
    typedef ACCESS_MODE_STACK mode_t; 
    typedef Access           type_inclosure_t;  /* could be only one pointer without version */
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const pointer_stack<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)a; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) { *a = *r; }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };


  template<typename T>
  struct TraitFormalParam<pointer_t<T> > { 
    typedef T                type_t; 
    typedef SCRATCH<T>       signature_t; 
    typedef pointer_t<T>     formal_t; 
    typedef ACCESS_MODE_SCRATCH mode_t; 
    typedef Access           type_inclosure_t;  /* could be only one pointer without version */
    static const bool        is_static = TraitIsStatic<T>::value;
    static const void*       ptr( const pointer_t<T>* a ) { return a->ptr(); }
    static void*             get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static const void*       get_version( const type_inclosure_t* a, unsigned int i ) { return &a->version; }
    static size_t            get_nparam ( const type_inclosure_t* a ) { return 1; }
    static void              get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t*  view )
    { kaapi_memory_view_make1d( view, 0, 1, sizeof(type_t) ); }
    static void              set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { }
    static kaapi_access_t*   get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)a; }
    static void              set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) { *a = *r; }
    static void              reducor_fnc(void*, const void*) {}
    static void              redinit_fnc(void*) {}
  };
    
  template<typename T>
  struct TraitFormalParam<W<T> > : public TraitFormalParam<pointer_w<T> > { };
  template<typename T>
  struct TraitFormalParam<R<T> > : public TraitFormalParam<pointer_r<T> > { };
  template<typename T>
  struct TraitFormalParam<WP<T> > : public TraitFormalParam<pointer_wp<T> > { };
  template<typename T>
  struct TraitFormalParam<RP<T> > : public TraitFormalParam<pointer_rp<T> > { };
  template<typename T>
  struct TraitFormalParam<RW<T> > : public TraitFormalParam<pointer_rw<T> > { };
  template<typename T>
  struct TraitFormalParam<RPWP<T> > : public TraitFormalParam<pointer_rpwp<T> > { };
  template<typename T>
  struct TraitFormalParam<CWP<T> > : public TraitFormalParam<pointer_cwp<T> > { };
  template<typename T, typename OP>
  struct TraitFormalParam<CW<T,OP> > : public TraitFormalParam<pointer_cw<T,OP> > { };
  template<typename T, typename OP>
  struct TraitFormalParam<ICW<T,OP> > : public TraitFormalParam<pointer_icw<T,OP> > { };
  template<typename T>
  struct TraitFormalParam<ST<T> > : public TraitFormalParam<pointer_stack<T> > { };
  template<typename T>
  struct TraitFormalParam<SCRATCH<T> > : public TraitFormalParam<pointer_t<T> > { };


  /* ------ rep of range of contiguous array of data: pointer_XX<array<dim, T> >
   */
  template<int d, typename T, Storage2DClass S>
  struct array_inclosure_t : public array<d,T,S> {
    array_inclosure_t() : array<d,T,S>()
    { kaapi_access_init( &access, 0 ); }
    array_inclosure_t(const array<d,T,S>& a) : array<d,T,S>(a)
    { kaapi_access_init( &access, (void*)array<d,T,S>::ptr() ); }
    template<typename P>
    array_inclosure_t( const P& p ) : array<d,T,S>(p)
    { kaapi_access_init( &access, (void*)array<d,T,S>::ptr() ); }
    kaapi_access_t access;
  };
  
  /* alias: ka::range1d<T> in place of array<1,T> */
  template<typename T, Storage2DClass S = RowMajor>
  struct range1d : public array<1,T,S> {
    typedef base_array::index_t index_t;    
    range1d( T* beg, T* end ) : array<1,T,S>(beg, end-beg) {}
    range1d( T* beg, size_t size ) : array<1,T,S>(beg, size ) {}
    range1d( const array<1,T,S>& a ) : array<1,T,S>(a) {}
    range1d operator() (const rangeindex& ri) 
    { return range1d( array<1,T,S>::operator()(ri) ); }
    range1d operator[] (const rangeindex& ri) 
    { return range1d( array<1,T,S>::operator[](ri) ); }
    range1d& operator=( const array<1,T,S>& a )
    { 
      array<1,T,S>::operator=( a );
      return *this;
    }
    T& operator() (const index_t& i)
    {
      return array<1,T,S>::operator()( i );
    }
    const T& operator() (const index_t& i) const
    {
      return array<1,T,S>::operator()( i );
    }
  };
  
  /* alias ka::range2d<T,S> in place of array<2,T,S> */
  template<typename T,Storage2DClass S=RowMajor>
  struct range2d : public array<2,T,S> {
    typedef base_array::index_t index_t;
    range2d( T* beg, size_t n, size_t m, size_t lda ) : array<2,T,S>(beg, n, m, lda ) {}
    range2d( const array<2,T,S>& a ) : array<2,T,S>(a) {}
    operator base_pointer<T>() { return *this; }
    operator array<2,T,S>&() { return *this; }
    operator const array<2,T,S>&() const { return *this; }
    array_rep<2,T,S>* operator->() { return this; }
    const array_rep<2,T,S>* operator->() const { return this; }
    range2d<T,S> operator() (const rangeindex& ri, const rangeindex& rj)
    { return range2d( array<2,T,S>::operator()(ri,rj) ); }
    range2d<T,S>& operator=( const array<2,T,S>& a )
    { 
      array<2,T,S>::operator=( a );
      return *this;
    }
    T& operator() (const index_t& i, const index_t& j)
    { 
      return array<2,T,S>::operator()( i,j );
    }
    const T& operator() (const index_t& i, const index_t& j) const
    { 
      return array<2,T,S>::operator()( i,j );
    }
  };
  template<typename T, Storage2DClass S = RowMajor>
  struct range2d_r;
  template<typename T, Storage2DClass S = RowMajor>
  struct range2d_rp;
  template<typename T, Storage2DClass S = RowMajor>
  struct range2d_w;
  template<typename T, Storage2DClass S = RowMajor>
  struct range2d_wp;
  template<typename T, Storage2DClass S = RowMajor>
  struct range2d_rw;
  template<typename T, Storage2DClass S = RowMajor>
  struct range2d_rpwp;
  template<typename T, Storage2DClass S = RowMajor>
  struct range2d_cw;
  template<typename T, Storage2DClass S = RowMajor>
  struct range2d_icw;

  /* ------ formal parameter of type _r, _w and _rw and rpwp over array */
  template<typename T, Storage2DClass S>
  class pointer_r<array<1,T,S> > : protected array<1,T,S> {
    friend struct array_inclosure_t<1,T,S>;
  public:
    typedef T                      value_type;
    typedef size_t                 difference_type;
    typedef pointer_r<array<1,T,S> > Self_t;
    
    pointer_r() : array<1,T,S>() {}
    pointer_r( const array<1,T,S>& a ) : array<1,T,S>(a) {}
    /* cstor call on closure creation */
    explicit pointer_r( array_inclosure_t<1,T,S>& a ) : array<1,T,S>(a) {}
    /* use in spawn effective -> in closure */
    operator array_inclosure_t<1,T,S>() const { return array_inclosure_t<1,T,S>(*this); }

    /* public interface */
    array<1,T,S>& operator*() { return *this; }
    array<1,T,S>* operator->() { return this; }
    const array<1,T,S>& operator*() const { return *this; }
    const array<1,T,S>* operator->() const { return this; }
    
    const T& operator[](int i) const { return array<1,T,S>::operator[](i); }
    const T& operator[](unsigned int i) const { return array<1,T,S>::operator[](i); }
    const T& operator[](long i) const { return array<1,T,S>::operator[](i); }
    const T& operator[](unsigned long i) const { return array<1,T,S>::operator[](i); }
    
    const T& operator()(int i) const { return array<1,T,S>::operator[](i); }
    const T& operator()(unsigned i) const { return array<1,T,S>::operator[](i); }
    const T& operator()(long i) const { return array<1,T,S>::operator[](i); }
    const T& operator()(unsigned long i) const { return array<1,T,S>::operator[](i); }
  };
  
  /* alias: ka::range1d_r<T> in place of pointer_r<array<1,T> > */
  template<typename T>
  struct range1d_r : public pointer_r<array<1,T> > {
    typedef pointer_r<array<1,T> >           Self_t;
    typedef typename Self_t::value_type      value_type;
    typedef typename Self_t::difference_type difference_type;
    typedef typename Self_t::index_t         index_t;

    range1d_r( range1d<T>& a ) : pointer_r<array<1,T> >(a) {}
    explicit range1d_r( const array<1,T>& a ) : pointer_r<array<1,T> >(a) {}

    const T& operator[](difference_type i) const { return array<1,T>::operator[](i); }
    const T& operator()(difference_type i) const { return array<1,T>::operator[](i); }

    range1d_r<T> operator[] (const rangeindex& r) const
    { return range1d_r<T>( array<1,T>::operator()(r) ); }
    range1d_r<T> operator() (const rangeindex& r) const
    { return range1d_r<T>( array<1,T>::operator()(r) ); }
  };

  /* alias: ka::range1d_rp<T> in place of pointer_rp<array<1,T> > */
  template<typename T>
  struct range1d_rp : public pointer_rp<array<1,T> > {
    typedef pointer_rp<array<1,T> >           Self_t;
    typedef typename Self_t::value_type      value_type;
    typedef typename Self_t::difference_type difference_type;
    typedef typename Self_t::index_t         index_t;
    
    range1d_rp( range1d<T>& a ) : pointer_rp<array<1,T> >(a) {}
    explicit range1d_rp( const array<1,T>& a ) : pointer_rp<array<1,T> >(a) {}
    
    array_rep<1,T,RowMajor>* operator->() { return this; }
    
    range1d_rp<T> operator() (const rangeindex& r) const
    { return range1d_rp<T>( array<1,T>::operator()(r) ); }
  };
  
  /* */
  template<typename T>
  class pointer_w<array<1,T> > : protected array<1,T> {
    friend struct array_inclosure_t<1,T,RowMajor>;
  public:
    typedef T                        value_type;
    typedef size_t                   difference_type;
    typedef pointer_w<array<1,T> >   Self_t;
    
    pointer_w() : array<1,T>() {}
    pointer_w( const array_inclosure_t<1,T,RowMajor>& a ) : array<1,T>(a) {}
    /* cstor call on closure creation */
    explicit pointer_w( array<1,T>& a ) : array<1,T>(a) {}
    /* use in spawn effective -> in closure */
    //operator array<1,T>() const { return *this; }
    
    /* public interface */
    array<1,T>& operator*() { return *this; }
    array<1,T>* operator->() { return this; }
    const array<1,T>& operator*() const { return *this; }
    const array<1,T>* operator->() const { return this; }
    
    size_t size() const { return array<1,T>::size(); }
    T* ptr() { return array<1,T>::ptr(); }
    T* begin() { return array<1,T>::ptr(); }
    T* end() { return array<1,T>::ptr()+array<1,T>::size(); }
    
    T& operator[](int i)  { return array<1,T>::operator[](i); }
    T& operator[](unsigned i)  { return array<1,T>::operator[](i); }
    T& operator[](long i) { return array<1,T>::operator[](i); }
    T& operator[](unsigned long i) { return array<1,T>::operator[](i); }
    
    T& operator()(int i)  { return array<1,T>::operator[](i); }
    T& operator()(unsigned i)  { return array<1,T>::operator[](i); }
    T& operator()(long i) { return array<1,T>::operator[](i); }
    T& operator()(unsigned long i) { return array<1,T>::operator[](i); }
    
    Self_t operator[] (const rangeindex& r) const 
    { return pointer_w( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r) const 
    { return pointer_w( array<1,T>::operator()(r) ); }
  };
  
  /* alias: ka::range1d_w<T> in place of pointer_w<array<1,T> > */
  template<typename T>
  struct range1d_w : public pointer_w<array<1,T> > {
    typedef range1d_w<T>             Self_t;
    typedef size_t                   difference_type;
    
    range1d_w( range1d<T>& a ) : pointer_w<array<1,T> >(a) {}
    explicit range1d_w( array<1,T>& a ) : pointer_w<array<1,T> >(a) {}
    
    T& operator[](difference_type i) { return array<1,T>::operator[](i); }
    T& operator()(difference_type i) { return array<1,T>::operator[](i); }
    
    Self_t operator[] (const rangeindex& r) 
    { return Self_t( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r) 
    { return Self_t( array<1,T>::operator()(r) ); }
  };

  template<typename T>
  struct range1d_wp : public pointer_wp<array<1,T> > {
    typedef range1d_wp<T>             Self_t;
    typedef size_t                   difference_type;
    
    range1d_wp( range1d<T>& a ) : pointer_wp<array<1,T> >(a) {}
    explicit range1d_wp( array<1,T>& a ) : pointer_wp<array<1,T> >(a) {}
    
    T& operator[](difference_type i) { return array<1,T>::operator[](i); }
    T& operator()(difference_type i) { return array<1,T>::operator[](i); }
    
    Self_t operator[] (const rangeindex& r)
    { return Self_t( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r)
    { return Self_t( array<1,T>::operator()(r) ); }
  };
  
  /**/
  template<typename T>
  class pointer_rw<array<1,T> > : protected array<1,T> {
    friend struct array_inclosure_t<1,T,RowMajor>;
  public:
    typedef T                        value_type;
    typedef size_t                   difference_type;
    typedef pointer_rw<array<1,T> > Self_t;
    
    pointer_rw() : array<1,T>() {}
    pointer_rw( const array<1,T>& a ) : array<1,T>(a) {}
    /* cstor call on closure creation */
    explicit pointer_rw( array_inclosure_t<1,T,RowMajor>& a ) : array<1,T>(a) {}
    /* use in spawn effective -> in closure */
    operator array_inclosure_t<1,T,RowMajor>() const { return array_inclosure_t<1,T,RowMajor>(*this); }
    
    /* public interface */
    array<1,T>& operator*() { return *this; }
    array<1,T>* operator->() { return this; }
    const array<1,T>& operator*() const { return *this; }
    const array<1,T>* operator->() const { return this; }
    
    size_t size() const { return array<1,T>::size(); }
    T* ptr() { return array<1,T>::ptr(); }
    T* begin() { return array<1,T>::ptr(); }
    T* end() { return array<1,T>::ptr()+array<1,T>::size(); }
    
    T& operator[](int i)  { return array<1,T>::operator[](i); }
    T& operator[](unsigned i)  { return array<1,T>::operator[](i); }
    T& operator[](long i) { return array<1,T>::operator[](i); }
    T& operator[](unsigned long i) { return array<1,T>::operator[](i); }
    
    T& operator()(int i)  { return array<1,T>::operator[](i); }
    T& operator()(unsigned i)  { return array<1,T>::operator[](i); }
    T& operator()(long i) { return array<1,T>::operator[](i); }
    T& operator()(unsigned long i) { return array<1,T>::operator[](i); }
    
    Self_t operator[] (const rangeindex& r) const 
    { return pointer_rw( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r) const 
    { return pointer_rw( array<1,T>::operator()(r) ); }
  };
  
  /* alias: ka::range1d_rw<T> in place of pointer_rw<array<1,T> > */
  template<typename T>
  struct range1d_rw : public pointer_rw<array<1,T> > {
    typedef range1d_rw<T>            Self_t;
    typedef size_t                   difference_type;
    
    range1d_rw( range1d<T>& a ) : pointer_rw<array<1,T> >(a) {}
    range1d_rw( const range1d<T>& a ) : pointer_rw<array<1,T> >(a) {}
    explicit range1d_rw( array<1,T>& a ) : pointer_rw<array<1,T> >(a) {}
    explicit range1d_rw( const array<1,T>& a ) : pointer_rw<array<1,T> >(a) {}
    
    T& operator[](difference_type i) { return array<1,T>::operator[](i); }
    T& operator()(difference_type i) { return array<1,T>::operator[](i); }
    
    Self_t operator[] (const rangeindex& r) 
    { return Self_t( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r) 
    { return Self_t( array<1,T>::operator()(r) ); }
  };
  
  template<typename T>
  class pointer_rp<array<1,T> > : protected array<1,T> {
    friend struct array_inclosure_t<1,T,RowMajor>;
  public:
    typedef T                         value_type;
    typedef size_t                    difference_type;
    typedef pointer_rp<array<1,T> > Self_t;
    
    pointer_rp() : array<1,T>() {}
    pointer_rp( const array<1,T>& a ) : array<1,T>(a) {}
    /* cstor call on closure creation */
    explicit pointer_rp( array_inclosure_t<1,T,RowMajor>& a ) : array<1,T>(a) {}
    /* use in spawn effective -> in closure */
    operator array_inclosure_t<1,T,RowMajor>() const { return array_inclosure_t<1,T,RowMajor>(*this); }
    
    /* public interface */
    array<1,T>& operator*() { return *this; }
    array<1,T>* operator->() { return this; }
    const array<1,T>& operator*() const { return *this; }
    const array<1,T>* operator->() const { return this; }
    
    size_t size() const { return array<1,T>::size(); }
    
    Self_t operator() (const rangeindex& r) const
    { return pointer_rp( array<1,T>::operator()(r) ); }
  };
  
  
  template<typename T>
  class pointer_rpwp<array<1,T> > : protected array<1,T> {
    friend struct array_inclosure_t<1,T,RowMajor>;
  public:
    typedef T                        value_type;
    typedef size_t                   difference_type;
    typedef pointer_rpwp<array<1,T> > Self_t;
    
    pointer_rpwp() : array<1,T>() {}
    pointer_rpwp( const array<1,T>& a ) : array<1,T>(a) {}
    /* cstor call on closure creation */
    explicit pointer_rpwp( array_inclosure_t<1,T,RowMajor>& a ) : array<1,T>(a) {}
    /* use in spawn effective -> in closure */
    operator array_inclosure_t<1,T,RowMajor>() const { return array_inclosure_t<1,T,RowMajor>(*this); }
    
    /* public interface */
    array<1,T>& operator*() { return *this; }
    array<1,T>* operator->() { return this; }
    const array<1,T>& operator*() const { return *this; }
    const array<1,T>* operator->() const { return this; }
    
    size_t size() const { return array<1,T>::size(); }
    
    /* pointed values are assumed not to be accessed */
    T* ptr() const { return array<1,T>::ptr(); }
    T* begin() const { return array<1,T>::ptr(); }
    T* end() const { return array<1,T>::ptr()+array<1,T>::size(); }
    
    Self_t operator[] (const rangeindex& r) const 
    { return pointer_rpwp( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r) const 
    { return pointer_rpwp( array<1,T>::operator()(r) ); }
  };
  
  /* alias: ka::range1d_rpwp<T> in place of pointer_rw<array<1,T> > */
  template<typename T>
  struct range1d_rpwp : public pointer_rpwp<array<1,T> > {
    typedef range1d_rpwp<T>             Self_t;
    
    range1d_rpwp( range1d<T>& a ) : pointer_rpwp<array<1,T> >(a) {}
    explicit range1d_rpwp( array<1,T>& a ) : pointer_rpwp<array<1,T> >(a) {}
    explicit range1d_rpwp( const array<1,T>& a ) : pointer_rpwp<array<1,T> >(a) {}
    
    Self_t operator[] (const rangeindex& r) const 
    { return Self_t( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r) const 
    { return Self_t( array<1,T>::operator()(r) ); }
  };
  
  
  template<typename T>
  class pointer_cw<array<1,T> > : protected array<1,T> {
    friend struct array_inclosure_t<1,T,RowMajor>;
  public:
    typedef T                        value_type;
    typedef size_t                   difference_type;
    typedef pointer_cw<array<1,T> > Self_t;
    
    pointer_cw() : array<1,T>() {}
    pointer_cw( const array<1,T>& a ) : array<1,T>(a) {}
    /* cstor call on closure creation */
    explicit pointer_cw( array_inclosure_t<1,T,RowMajor>& a ) : array<1,T>(a) {}
    /* use in spawn effective -> in closure */
    operator array_inclosure_t<1,T,RowMajor>() const { return array_inclosure_t<1,T,RowMajor>(*this); }
    
    /* public interface */
    array<1,T>& operator*() { return *this; }
    array<1,T>* operator->() { return this; }
    const array<1,T>& operator*() const { return *this; }
    const array<1,T>* operator->() const { return this; }
    
    size_t size() const { return array<1,T>::size(); }
    T* ptr() { return array<1,T>::ptr(); }
    T* begin() { return array<1,T>::ptr(); }
    T* end() { return array<1,T>::ptr()+array<1,T>::size(); }
    
    T& operator[](int i)  { return array<1,T>::operator[](i); }
    T& operator[](unsigned i)  { return array<1,T>::operator[](i); }
    T& operator[](long i) { return array<1,T>::operator[](i); }
    T& operator[](unsigned long i) { return array<1,T>::operator[](i); }
    
    T& operator()(int i)  { return array<1,T>::operator[](i); }
    T& operator()(unsigned i)  { return array<1,T>::operator[](i); }
    T& operator()(long i) { return array<1,T>::operator[](i); }
    T& operator()(unsigned long i) { return array<1,T>::operator[](i); }
    
    Self_t operator[] (const rangeindex& r) const 
    { return pointer_cw( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r) const 
    { return pointer_cw( array<1,T>::operator()(r) ); }
  };
  
  /* alias: ka::range1d_cw<T> in place of pointer_cw<array<1,T> > */
  template<typename T>
  struct range1d_cw : public pointer_cw<array<1,T> > {
    typedef range1d_cw<T>             Self_t;
    
    range1d_cw( range1d<T>& a ) : pointer_cw<array<1,T> >(a) {}
    explicit range1d_cw( array<1,T>& a ) : pointer_cw<array<1,T> >(a) {}
    
    Self_t operator[] (const rangeindex& r) const 
    { return Self_t( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r) const 
    { return Self_t( array<1,T>::operator()(r) ); }
  };

  /* alias: ka::range1d_cw<T> in place of pointer_cw<array<1,T> > */
  template<typename T>
  struct range1d_icw : public pointer_icw<array<1,T> > {
    typedef range1d_icw<T>             Self_t;

    range1d_icw( range1d<T>& a ) : pointer_icw<array<1,T> >(a) {}
    explicit range1d_icw( array<1,T>& a ) : pointer_icw<array<1,T> >(a) {}

    Self_t operator[] (const rangeindex& r) const 
    { return Self_t( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r) const 
    { return Self_t( array<1,T>::operator()(r) ); }
  };


  template<typename T>
  class pointer_stack<array<1,T> > : protected array<1,T> {
    friend struct array_inclosure_t<1,T,RowMajor>;
  public:
    typedef T                        value_type;
    typedef size_t                   difference_type;
    typedef pointer_stack<array<1,T> > Self_t;
    
    pointer_stack() : array<1,T>() {}
    pointer_stack( const array<1,T>& a ) : array<1,T>(a) {}
    /* cstor call on closure creation */
    explicit pointer_stack( array_inclosure_t<1,T,RowMajor>& a ) : array<1,T>(a) {}
    /* use in spawn effective -> in closure */
    operator array_inclosure_t<1,T,RowMajor>() const { return array_inclosure_t<1,T,RowMajor>(*this); }
    
    /* public interface */
    array<1,T>& operator*() { return *this; }
    array<1,T>* operator->() { return this; }
    const array<1,T>& operator*() const { return *this; }
    const array<1,T>* operator->() const { return this; }
    
    size_t size() const { return array<1,T>::size(); }
    const T* ptr() const { return array<1,T>::ptr(); }
    T* ptr() { return array<1,T>::ptr(); }
    T* begin() { return array<1,T>::ptr(); }
    T* end() { return array<1,T>::ptr()+array<1,T>::size(); }
    
    T& operator[](int i) { return array<1,T>::operator[](i); }
    T& operator[](unsigned i)  { return array<1,T>::operator[](i); }
    T& operator[](long i) { return array<1,T>::operator[](i); }
    T& operator[](unsigned long i) { return array<1,T>::operator[](i); }
    
    T& operator()(int i)  { return array<1,T>::operator[](i); }
    T& operator()(unsigned i)  { return array<1,T>::operator[](i); }
    T& operator()(long i) { return array<1,T>::operator[](i); }
    T& operator()(unsigned long i) { return array<1,T>::operator[](i); }
    
    Self_t operator[] (const rangeindex& r) const 
    { return pointer_stack( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r) const 
    { return pointer_stack( array<1,T>::operator()(r) ); }
  };
  
  /* alias: ka::range1d_cw<T> in place of pointer_cw<array<1,T> > */
  template<typename T>
  struct range1d_stack : public pointer_stack<array<1,T> > {
    typedef range1d_stack<T>             Self_t;
    
    range1d_stack( range1d<T>& a ) : pointer_stack<array<1,T> >(a) {}
    explicit range1d_stack( array<1,T>& a ) : pointer_stack<array<1,T> >(a) {}
    explicit range1d_stack( const array<1,T>& a ) : pointer_stack<array<1,T> >(a) {}

    /* cannot access to this operators except with full qualification ? */
    T& operator[](int i) { return pointer_stack<array<1,T> >::operator[](i); }
    T& operator[](unsigned i)  { return pointer_stack<array<1,T> >::operator[](i); }
    T& operator[](long i) { return pointer_stack<array<1,T> >::operator[](i); }
    T& operator[](unsigned long i) { return pointer_stack<array<1,T> >::operator[](i); }
    
    Self_t operator[] (const rangeindex& r) const 
    { return Self_t( array<1,T>::operator()(r) ); }
    Self_t operator() (const rangeindex& r) const 
    { return Self_t( array<1,T>::operator()(r) ); }
  };


  /* same for 2d range */
  template<typename T, Storage2DClass S>
  class pointer_rp<array<2,T,S> > : protected array<2,T,S> {
    friend struct array_inclosure_t<2,T,S>;
  public:
    typedef T                         value_type;
    typedef size_t                    difference_type;
    typedef pointer_rp<array<2,T,S> > Self_t;
    
    pointer_rp() : array<2,T,S>() {}
    pointer_rp( const array<2,T,S>& a ) : array<2,T,S>(a) {}
    /* cstor call on closure creation */
    explicit pointer_rp( array_inclosure_t<2,T,S>& a ) : array<2,T,S>(a) {}
    /* use in spawn effective -> in closure */
    operator array_inclosure_t<2,T,S>() const { return array_inclosure_t<2,T,S>(*this); }
    
    /* public interface */
    array<2,T,S>& operator*() { return *this; }
    array<2,T,S>* operator->() { return this; }
    const array<2,T,S>& operator*() const { return *this; }
    const array<2,T,S>* operator->() const { return this; }
    
    Self_t operator() (const rangeindex& ri, const rangeindex& rj) const 
    { return Self_t( array_rep<2,T,S>::operator()(ri,rj) ); }
  };
  
  /* alias: ka::range2d_rp<T> in place of pointer_rw<array<2,T,S> > */
  template<typename T, Storage2DClass S>
  struct range2d_rp : public pointer_rp<array<2,T,S> >
  {
    typedef pointer_rp<array<2,T,S> >        Self_t;
    typedef typename Self_t::value_type      value_type;
    typedef typename Self_t::difference_type difference_type;
    typedef typename Self_t::index_t         index_t;
    
    range2d_rp( range2d<T,S>& a ) : pointer_rp<array<2,T,S> >(a) {}
    explicit range2d_rp( const array<2,T,S>& a ) : pointer_rp<array<2,T,S> >(a) {}
    operator array<2,T,S>&() { return *this; }
    operator const array<2,T,S>&() const { return *this; }

    array_rep<2,T,S>* operator->() { return this; }
    
    range2d_rp<T,S> operator() (const rangeindex& ri, const rangeindex& rj) const
    { return range2d_rp<T,S>( array<2,T,S>::operator()(ri,rj) ); }
  };


  /* ------ formal parameter of type _r, _w and _rw and rpwp over array */
  template<typename T, Storage2DClass S>
  class pointer_r<array<2,T,S> > : protected array<2,T,S> {
    friend struct array_inclosure_t<2,T,S>;
  public:
    typedef T                              value_type;
    typedef size_t                         difference_type;
    typedef typename array<2,T,S>::index_t index_t;
    typedef pointer_r<array<2,T,S> >       Self_t;
    
    pointer_r() : array<2,T,S>() {}
    pointer_r( const array<2,T,S>& a ) : array<2,T,S>(a) {}
    /* cstor call on closure creation */
    explicit pointer_r( array_inclosure_t<2,T,S>& a ) : array<2,T,S>(a) {}
    
    /* use in spawn effective -> in closure */
    operator array_inclosure_t<2,T,S>() const { return array_inclosure_t<2,T,S>(*this); }
    
    /* public interface */
    array<2,T,S>& operator*() { return *this; }
    array<2,T,S>* operator->() { return this; }
    const array<2,T,S>& operator*() const { return *this; }
    const array<2,T,S>* operator->() const { return this; }
    Self_t operator() (const rangeindex& ri, const rangeindex& rj) const
    { return Self_t( array<2,T,S>::operator()(ri,rj) ); }
  };
  
  /* alias: ka::range2d_r<T,S> in place of pointer_r<array<2,T,S> > */
  template<typename T, Storage2DClass S>
  struct range2d_r : public pointer_r<array<2,T,S> > {
    typedef pointer_r<array<2,T,S> >         Self_t;
    typedef typename Self_t::value_type      value_type;
    typedef typename Self_t::difference_type difference_type;
    typedef typename Self_t::index_t         index_t;
    
    range2d_r( range2d<T,S>& a ) : pointer_r<array<2,T,S> >(a) {}
    explicit range2d_r(  const array<2,T,S>& a ) : pointer_r<array<2,T,S> >(a) {}
    explicit range2d_r(  const range2d_rp<T,S>& a ) : pointer_r<array<2,T,S> >(a) {}

    //operator array<2,T,S>&() { return *this; }
    //operator const array<2,T,S>&() const { return *this; }

    const T& operator()(int i, int j) const { return array_rep<2,T,S>::operator()(i,j); }
    const T& operator()(unsigned int i, unsigned int j) const { return array_rep<2,T,S>::operator()(i,j); }
    const T& operator()(long i, long j) const { return array_rep<2,T,S>::operator()(i,j); }
    const T& operator()(unsigned long i, unsigned long j) const { return array_rep<2,T,S>::operator()(i,j); }
    
    const array_rep<2,T,S>* operator->() const { return this; }

    range2d_r<T,S> operator()(const rangeindex& ri, const rangeindex& rj)  const 
    { return range2d_r<T,S>( range2d_r<T,S>(array<2,T,S>::operator()(ri,rj) ) ); }
  };
  
  
  template<typename T, Storage2DClass S>
  class pointer_w<array<2,T,S> > : protected array<2,T,S> {
    friend struct array_inclosure_t<2,T,S>;
  public:
    typedef T                            value_type;
    typedef size_t                       difference_type;
    typedef typename array<2,T,S>::index_t index_t;
    typedef pointer_w<array<2,T,S> >       Self_t;
    
    pointer_w() : array<2,T,S>() {}
    pointer_w( const array_inclosure_t<2,T,S>& a ) : array<2,T,S>(a) {}
    /* cstor call on closure creation */
    explicit pointer_w( array<2,T,S>& a ) : array<2,T,S>(a) {}
    /* use in spawn effective -> in closure */
    operator array<2,T,S>() const { return *this; }
    
    /* public interface */
    array<2,T,S>& operator*() { return *this; }
    array<2,T,S>* operator->() { return this; }
    const array<2,T,S>& operator*() const { return *this; }
    const array<2,T,S>* operator->() const { return this; }
    Self_t operator() (const rangeindex& ri, const rangeindex& rj) 
    { return Self_t( array_rep<2,T,S>::operator()(ri,rj) ); }
  };
  
  
  /* alias: ka::range2d_w<T,S> in place of pointer_w<array<2,T,S> > */
  template<typename T, Storage2DClass S>
  struct range2d_w : public pointer_w<array<2,T,S> > 
  {
    typedef pointer_w<array<2,T,S> >           Self_t;
    typedef typename Self_t::value_type      value_type;
    typedef typename Self_t::difference_type difference_type;
    typedef typename Self_t::index_t         index_t;
    
    range2d_w( range2d<T,S>& a ) : pointer_w<array<2,T,S> >(a) {}
    explicit range2d_w( const array<2,T,S>& a ) : pointer_w<array<2,T,S> >(a) {}
    
    T& operator()(int i, int j)  { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(unsigned int i, unsigned int j)  { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(long i, long j) { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(unsigned long i, unsigned long j) { return array_rep<2,T,S>::operator()(i,j); }
    
    array_rep<2,T,S>* operator->() { return this; }
    
    range2d_w<T,S> operator() (const rangeindex& ri, const rangeindex& rj)
    { return range2d_w<T,S>( array<2,T,S>::operator()(ri,rj) ); }
    
    void operator=(const T& value)  { array_rep<2,T,S>::operator=(value); }
  };
  

  template<typename T, Storage2DClass S>
  class pointer_rpwp<array<2,T,S> > : protected array<2,T,S> {
    friend struct array_inclosure_t<2,T,S>;
  public:
    typedef T                           value_type;
    typedef size_t                      difference_type;
    typedef pointer_rpwp<array<2,T,S> > Self_t;
    
    pointer_rpwp() : array<2,T,S>() {}
    pointer_rpwp( const array<2,T,S>& a ) : array<2,T,S>(a) {}
    /* cstor call on closure creation */
    explicit pointer_rpwp( array_inclosure_t<2,T,S>& a ) : array<2,T,S>(a) {}
    /* use in spawn effective -> in closure */
    operator array_inclosure_t<2,T,S>() const { return array_inclosure_t<2,T,S>(*this); }
    
    /* public interface */
    array<2,T,S>& operator*() { return *this; }
    array<2,T,S>* operator->() { return this; }
    const array<2,T,S>& operator*() const { return *this; }
    const array<2,T,S>* operator->() const { return this; }
    
    Self_t operator() (const rangeindex& ri, const rangeindex& rj) const 
    { return Self_t( array_rep<2,T,S>::operator()(ri,rj) ); }
  };
  
  /* alias: ka::range2d_rpwp<T> in place of pointer_rw<array<2,T,S> > */
  template<typename T, Storage2DClass S>
  struct range2d_rpwp : public pointer_rpwp<array<2,T,S> > 
  {
    typedef pointer_rpwp<array<2,T,S> >      Self_t;
    typedef typename Self_t::value_type      value_type;
    typedef typename Self_t::difference_type difference_type;
    typedef typename Self_t::index_t         index_t;
    
    range2d_rpwp( range2d<T,S>& a ) : pointer_rpwp<array<2,T,S> >(a) {}
    explicit range2d_rpwp( const array<2,T,S>& a ) : pointer_rpwp<array<2,T,S> >(a) {}
    
    array_rep<2,T,S>* operator->() { return this; }

    range2d_rpwp<T,S> operator() (const rangeindex& ri, const rangeindex& rj) const
    { return range2d_rpwp<T,S>( array<2,T,S>::operator()(ri,rj) ); }
  };
  

  template<typename T, Storage2DClass S>
  class pointer_rw<array<2,T,S> > : protected array<2,T,S> {
    friend struct array_inclosure_t<2,T,S>;
  public:
    typedef T                            value_type;
    typedef size_t                       difference_type;
    typedef typename array<2,T,S>::index_t index_t;
    typedef pointer_rw<array<2,T,S> >      Self_t;
    
    pointer_rw() : array<2,T,S>() {}
    pointer_rw( const array<2,T,S>& a ) : array<2,T,S>(a) {}
    /* cstor call on closure creation */
    explicit pointer_rw( array_inclosure_t<2,T,S>& a ) : array<2,T,S>(a) {}
    /* use in spawn effective -> in closure */
    operator array_inclosure_t<2,T,S>() const { return array_inclosure_t<2,T,S>(*this); }
    
    /* public interface */
    array<2,T,S>& operator*() { return *this; }
    array<2,T,S>* operator->() { return this; }
    const array<2,T,S>& operator*() const { return *this; }
    const array<2,T,S>* operator->() const { return this; }
    
    Self_t operator() (const rangeindex& ri, const rangeindex& rj) const 
    { return Self_t( array_rep<2,T,S>::operator()(ri,rj) ); }
  };
  
  /* alias: ka::range2d_rw<T,S> in place of pointer_rw<array<2,T,S> > */
  template<typename T, Storage2DClass S>
  struct range2d_rw : public pointer_rw<array<2,T,S> > 
  {
    typedef pointer_rw<array<2,T,S> >          Self_t;
    typedef typename Self_t::value_type      value_type;
    typedef typename Self_t::difference_type difference_type;
    typedef typename Self_t::index_t         index_t;
    
    range2d_rw( range2d<T,S>& a ) : pointer_rw<array<2,T,S> >(a) {}
    explicit range2d_rw( const array<2,T,S>& a ) : pointer_rw<array<2,T,S> >(a) {}

    T& operator()(int i, int j)  { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(unsigned int i, unsigned int j)  { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(long i, long j) { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(unsigned long i, unsigned long j) { return array_rep<2,T,S>::operator()(i,j); }
    
    array_rep<2,T,S>* operator->() { return this; }
    
    range2d_rw<T,S> operator() (const rangeindex& ri, const rangeindex& rj) const
    { return range2d_rw<T,S>( array<2,T,S>::operator()(ri,rj) ); }
    
    void operator=(const T& value) { array_rep<2,T,S>::operator=(value); }
  };
  


  template<typename T, Storage2DClass S>
  class pointer_cw<array<2,T,S> > : protected array<2,T,S> {
    friend struct array_inclosure_t<2,T,S>;
  public:
    typedef T                              value_type;
    typedef size_t                         difference_type;
    typedef typename array<2,T,S>::index_t index_t;
    typedef pointer_cw<array<2,T,S> >      Self_t;
    
    pointer_cw() : array<2,T,S>() {}
    pointer_cw( const array<2,T,S>& a ) : array<2,T,S>(a) {}
    /* cstor call on closure creation */
    explicit pointer_cw( array_inclosure_t<2,T,S>& a ) : array<2,T,S>(a) {}
    /* use in spawn effective -> in closure */
    operator array_inclosure_t<2,T,S>() const { return array_inclosure_t<2,T,S>(*this); }
    
    /* public interface */
    array<2,T,S>& operator*() { return *this; }
    array<2,T,S>* operator->() { return this; }
    const array<2,T,S>& operator*() const { return *this; }
    const array<2,T,S>* operator->() const { return this; }
    
    Self_t operator() (const rangeindex& ri, const rangeindex& rj) const 
    { return Self_t( array_rep<2,T,S>::operator()(ri,rj) ); }
  };
  
  /* alias: ka::range2d_cw<T,S> in place of pointer_cw<array<2,T,S> > */
  template<typename T, Storage2DClass S>
  struct range2d_cw : public pointer_cw<array<2,T,S> > 
  {
    typedef pointer_cw<array<2,T,S> >        Self_t;
    typedef typename Self_t::value_type      value_type;
    typedef typename Self_t::difference_type difference_type;
    typedef typename Self_t::index_t         index_t;
    
    range2d_cw( range2d<T,S>& a ) : pointer_cw<array<2,T,S> >(a) {}
    explicit range2d_cw( const array<2,T,S>& a ) : pointer_cw<array<2,T,S> >(a) {}
    
    T& operator()(int i, int j)  { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(unsigned int i, unsigned int j)  { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(long i, long j) { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(unsigned long i, unsigned long j) { return array_rep<2,T,S>::operator()(i,j); }
    
    array_rep<2,T,S>* operator->() { return this; }
    
    range2d_cw<T,S> operator() (const rangeindex& ri, const rangeindex& rj) const
    { return range2d_cw<T,S>( array<2,T,S>::operator()(ri,rj) ); }
    
    void operator=(const T& value) { array_rep<2,T,S>::operator=(value); }
  };

  /* alias: ka::range2d_cw<T,S> in place of pointer_icw<array<2,T,S> > */
  template<typename T, Storage2DClass S>
  struct range2d_icw : public pointer_icw<array<2,T,S> > 
  {
    typedef pointer_icw<array<2,T,S> >       Self_t;
    typedef typename Self_t::value_type      value_type;
    typedef typename Self_t::difference_type difference_type;
    typedef typename Self_t::index_t         index_t;

    range2d_icw( range2d<T,S>& a ) : pointer_icw<array<2,T,S> >(a) {}
    explicit range2d_icw( const array<2,T,S>& a ) : pointer_icw<array<2,T,S> >(a) {}

    T& operator()(int i, int j)  { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(unsigned int i, unsigned int j)  { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(long i, long j) { return array_rep<2,T,S>::operator()(i,j); }
    T& operator()(unsigned long i, unsigned long j) { return array_rep<2,T,S>::operator()(i,j); }

    array_rep<2,T,S>* operator->() { return this; }

    range2d_icw<T,S> operator() (const rangeindex& ri, const rangeindex& rj) const 
    { return range2d_icw<T,S>( array<2,T,S>::operator()(ri,rj) ); }

    void operator=(const T& value) { array_rep<2,T,S>::operator=(value); }
  };


  /* trait */    
  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam<pointer_r<array<dim,T,S> > > { 
    typedef array_inclosure_t<dim,T,S>  type_inclosure_t;
    typedef R<array<dim,T,S> >          signature_t; 
    typedef pointer_r<array<dim,T,S> >  formal_t; 
    typedef ACCESS_MODE_R             mode_t; 
    typedef T                         type_t;
    static const bool                 is_static = false;
    static const void*                ptr( const pointer_r<array<dim,T,S> >* a ) { return a->ptr(); }
    static void*                      get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*            get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)&a->access; }
    static void                       set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r )
    { a->setptr( (T*)(a->access.data = r->data) ); a->access.version = r->version; }
    static size_t                     get_nparam( const type_inclosure_t* a )
    { return 1; }
    static void                       get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t* view )
    { return a->get_view(view); }
    static void                       set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { a->set_view(view); }
    static void                       reducor_fnc(void*, const void*) {}
    static void                       redinit_fnc(void*) {}
  };

  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam<pointer_rp<array<dim,T,S> > > {
    typedef array_inclosure_t<dim,T,S>  type_inclosure_t;
    typedef RP<array<dim,T,S> >          signature_t;
    typedef pointer_rp<array<dim,T,S> >  formal_t;
    typedef ACCESS_MODE_R             mode_t; 
    typedef T                         type_t;
    static const bool                 is_static = false;
    static const void*                ptr( const pointer_r<array<dim,T,S> >* a ) { return a->ptr(); }
    static void*                      get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*            get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)&a->access; }
    static void                       set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r )
    { a->setptr( (T*)(a->access.data = r->data) ); a->access.version = r->version; }
    static size_t                     get_nparam( const type_inclosure_t* a )
    { return 1; }
    static void                       get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t* view )
    { return a->get_view(view); }
    static void                       set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view ) 
    { a->set_view(view); }
    static void                       reducor_fnc(void*, const void*) {}
    static void                       redinit_fnc(void*) {}
  };

  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam<pointer_w<array<dim,T,S> > > { 
    typedef array_inclosure_t<dim,T,S>  type_inclosure_t;
    typedef W<array<dim,T,S> >          signature_t; 
    typedef pointer_w<array<dim,T,S> >  formal_t; 
    typedef ACCESS_MODE_W             mode_t; 
    typedef T                         type_t;
    static const bool                 is_static = false;
    static const void*                ptr( const pointer_w<array<dim,T,S> >* a ) { return a->ptr(); }
    static void*                      get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*            get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)&a->access; }
    static void                       set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r )
    { a->setptr( (T*)(a->access.data = r->data) ); a->access.version = r->version; }
    static size_t                     get_nparam( const type_inclosure_t* a )
    { return 1; }
    static void                       get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t* view )
    { return a->get_view(view); }
    static void                       set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view )
    { a->set_view(view); }
    static void                       reducor_fnc(void*, const void*) {}
    static void                       redinit_fnc(void*) {}
  };

  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam<pointer_wp<array<dim,T,S> > > {
    typedef array_inclosure_t<dim,T,S>  type_inclosure_t;
    typedef WP<array<dim,T,S> >          signature_t;
    typedef pointer_wp<array<dim,T,S> >  formal_t;
    typedef ACCESS_MODE_WP               mode_t;
    typedef T                         type_t;
    static const bool                 is_static = false;
    static const void*                ptr( const pointer_w<array<dim,T,S> >* a ) { return a->ptr(); }
    static void*                      get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*            get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)&a->access; }
    static void                       set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r )
    { a->setptr( (T*)(a->access.data = r->data) ); a->access.version = r->version; }
    static size_t                     get_nparam( const type_inclosure_t* a )
    { return 1; }
    static void                       get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t* view )
    { return a->get_view(view); }
    static void                       set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view )
    { a->set_view(view); }
    static void                       reducor_fnc(void*, const void*) {}
    static void                       redinit_fnc(void*) {}
  };

  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam<pointer_rw<array<dim,T,S> > > { 
    typedef array_inclosure_t<dim,T,S>  type_inclosure_t;
    typedef RW<array<dim,T,S> >         signature_t; 
    typedef pointer_rw<array<dim,T,S> > formal_t; 
    typedef ACCESS_MODE_RW            mode_t; 
    typedef T                         type_t;
    static const bool                 is_static = false;
    static const void*                ptr( const pointer_rw<array<dim,T,S> >* a ) { return a->ptr(); }
    static void*                get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*            get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)&a->access; }
    static void                       set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r )
    { a->setptr( (T*)(a->access.data = r->data) ); a->access.version = r->version; }
    static size_t                     get_nparam( const type_inclosure_t* a )
    { return 1; }
    static void                       get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t* view )
    { return a->get_view(view); }
    static void                       set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view )
    { a->set_view(view); }
    static void                       reducor_fnc(void*, const void*) {}
    static void                       redinit_fnc(void*) {}
  };   
  
  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam<pointer_rpwp<array<dim,T,S> > > { 
    typedef array_inclosure_t<dim,T,S>  type_inclosure_t;
    typedef RPWP<array<dim,T,S> >       signature_t; 
    typedef pointer_rpwp<array<dim,T,S> > formal_t; 
    typedef ACCESS_MODE_RPWP          mode_t; 
    typedef T                         type_t;
    static const bool                 is_static = false;
    static const void*                ptr( const pointer_rpwp<array<dim,T,S> >* a ) { return a->ptr(); }
    static void*                      get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*            get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)&a->access; }
    static void                       set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r )
    { a->setptr( (T*)(a->access.data = r->data) ); a->access.version = r->version; }
    static size_t                     get_nparam( const type_inclosure_t* a )
    { return 1; }
    static void                       get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t* view )
    { return a->get_view(view); }
    static void                       set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view )
    { a->set_view(view); }
    static void                       reducor_fnc(void*, const void*) {}
    static void                       redinit_fnc(void*) {}
  };   
  
  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam<pointer_cw<array<dim,T,S> > > { 
    typedef array_inclosure_t<dim,T,S>  type_inclosure_t;
    typedef CW<array<dim,T,S> >         signature_t; 
    typedef pointer_cw<array<dim,T,S> > formal_t; 
    typedef ACCESS_MODE_CW              mode_t; 
    typedef T                           type_t;
    static const bool                   is_static = false;
    static const void*                  ptr( const pointer_cw<array<dim,T,S> >* a ) { return a->ptr(); }
    static void*                        get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*              get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)&a->access; }
    static void                         set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r )
    { a->setptr( (T*)(a->access.data = r->data) ); a->access.version = r->version; }
    static size_t                       get_nparam( const type_inclosure_t* a )
    { return 1; }
    static void                         get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t* view )
    { return a->get_view(view); }
    static void                         set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view )
    { a->set_view(view); }
    static void                         reducor_fnc(void*, const void*) {}
    static void                         redinit_fnc(void*) {}
  };   

  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam<pointer_icw<array<dim,T,S> > > { 
    typedef array_inclosure_t<dim,T,S>  type_inclosure_t;
    typedef ICW<array<dim,T,S> >        signature_t; 
    typedef pointer_icw<array<dim,T,S> >formal_t; 
    typedef ACCESS_MODE_ICW             mode_t; 
    typedef T                           type_t;
    static const bool                   is_static = false;
    static const void*                  ptr( const pointer_icw<array<dim,T,S> >* a ) { return a->ptr(); }
    static void*                        get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*              get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)&a->access; }
    static void                         set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r )
    { a->setptr( (T*)(a->access.data = r->data) ); a->access.version = r->version; }
    static size_t                       get_nparam( const type_inclosure_t* a )
    { return 1; }
    static void                         get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t* view )
    { return a->get_view(view); }
    static void                         set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view )
    { a->set_view(view); }
    static void                         reducor_fnc(void*, const void*) {}
    static void                         redinit_fnc(void*) {}
  };   
  
  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam<pointer_stack<array<dim,T,S> > > { 
    typedef array_inclosure_t<dim,T,S>  type_inclosure_t;
    typedef ST<array<dim,T,S> >         signature_t; 
    typedef pointer_stack<array<dim,T,S> > formal_t; 
    typedef ACCESS_MODE_STACK           mode_t; 
    typedef T                           type_t;
    static const bool                   is_static = false;
    static const void*                  ptr( const pointer_stack<array<dim,T,S> >* a ) { return a->ptr(); }
    static void*                        get_data   ( const type_inclosure_t* a, unsigned int i ) { return 0; }
    static kaapi_access_t*              get_access ( const type_inclosure_t* a, unsigned int i ) { return (kaapi_access_t*)&a->access; }
    static void                         set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r )
    { a->setptr( (T*)(a->access.data = r->data) ); a->access.version = r->version; }
    static size_t                       get_nparam( const type_inclosure_t* a ) 
    { return 1; }
    static void                         get_view_param( const type_inclosure_t* a, unsigned int i, kaapi_memory_view_t* view )
    { return a->get_view(view); }
    static void                         set_view_param( type_inclosure_t* a, unsigned int i, const kaapi_memory_view_t*  view )
    { a->set_view(view); }
    static void                         reducor_fnc(void*, const void*) {}
    static void                         redinit_fnc(void*) {}
  };   


  
  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam< R<array<dim,T,S> > > : public TraitFormalParam<pointer_r<array<dim,T,S> > > {};
  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam< W<array<dim,T,S> > > : public TraitFormalParam<pointer_w<array<dim,T,S> > > {};
  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam< RW<array<dim,T,S> > > : public TraitFormalParam<pointer_rw<array<dim,T,S> > > {};
  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam< RPWP<array<dim,T,S> > > : public TraitFormalParam<pointer_rpwp<array<dim,T,S> > > {};
  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam< CW<array<dim,T,S> > > : public TraitFormalParam<pointer_cw<array<dim,T,S> > > {};
  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam< ICW<array<dim,T,S> > > : public TraitFormalParam<pointer_icw<array<dim,T,S> > > {};
  template<int dim, typename T, Storage2DClass S>
  struct TraitFormalParam< ST<array<dim,T,S> > > : public TraitFormalParam<pointer_stack<array<dim,T,S> > > {};

  template<typename T>
  struct TraitFormalParam<range1d<T> > : public TraitFormalParam<array<1,T> > { 
    typedef ACCESS_MODE_RPWP   mode_t; 
  };
  
  template<typename T>
  struct TraitFormalParam<range1d_r<T> > : public TraitFormalParam<pointer_r<array<1,T> > > {
    typedef TraitFormalParam<pointer_r<array<1,T> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range1d_r<T>    formal_t;
    typedef R<range1d <T> > signature_t;
  };
  template<typename T>
  struct TraitFormalParam<range1d_rp<T> > : public TraitFormalParam<pointer_rp<array<1,T> > > {
    typedef TraitFormalParam<pointer_rp<array<1,T> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range1d_rp<T>    formal_t;
    typedef RP<range1d <T> > signature_t;
  };
  template<typename T>
  struct TraitFormalParam<range1d_w<T> > : public TraitFormalParam<pointer_w<array<1,T> > > {
    typedef TraitFormalParam<pointer_w<array<1,T> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range1d_w<T>    formal_t; 
    typedef W<range1d<T> >  signature_t; 
  };
  template<typename T>
  struct TraitFormalParam<range1d_wp<T> > : public TraitFormalParam<pointer_wp<array<1,T> > > {
    typedef TraitFormalParam<pointer_wp<array<1,T> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range1d_wp<T>    formal_t;
    typedef WP<range1d<T> >  signature_t;
  };
  template<typename T>
  struct TraitFormalParam<range1d_rw<T> > : public TraitFormalParam<pointer_rw<array<1,T> > > {
    typedef TraitFormalParam<pointer_rw<array<1,T> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range1d_rw<T>   formal_t; 
    typedef RW<range1d<T> > signature_t; 
  };
  template<typename T>
  struct TraitFormalParam<range1d_rpwp<T> > : public TraitFormalParam<pointer_rpwp<array<1,T> > > {
    typedef TraitFormalParam<pointer_rpwp<array<1,T> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range1d_rpwp<T>   formal_t; 
    typedef RPWP<range1d<T> > signature_t; 
  };
  template<typename T>
  struct TraitFormalParam<range1d_cw<T> > : public TraitFormalParam<pointer_cw<array<1,T> > > {
    typedef TraitFormalParam<pointer_cw<array<1,T> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range1d_cw<T>     formal_t; 
    typedef CW<range1d<T> >   signature_t; 
  };
  template<typename T>
  struct TraitFormalParam<range1d_icw<T> > : public TraitFormalParam<pointer_icw<array<1,T> > > {
    typedef TraitFormalParam<pointer_icw<array<1,T> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range1d_icw<T>    formal_t; 
    typedef ICW<range1d<T> >  signature_t; 
  };
  template<typename T>
  struct TraitFormalParam<range1d_stack<T> > : public TraitFormalParam<pointer_stack<array<1,T> > > {
    typedef TraitFormalParam<pointer_stack<array<1,T> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range1d_stack<T>  formal_t; 
    typedef ST<range1d<T> >   signature_t; 
  };

  template<typename T>
  struct TraitFormalParam< R<range1d<T> > > : public TraitFormalParam<range1d_r<T> > {};
  template<typename T>
  struct TraitFormalParam< RP<range1d<T> > > : public TraitFormalParam<range1d_rp<T> > {};
  template<typename T>
  struct TraitFormalParam< W<range1d<T> > > : public TraitFormalParam<range1d_w<T> > {};
  template<typename T>
  struct TraitFormalParam< WP<range1d<T> > > : public TraitFormalParam<range1d_wp<T> > {};
  template<typename T>
  struct TraitFormalParam< RW<range1d<T> > > : public TraitFormalParam<range1d_rw<T> > {};
  template<typename T>
  struct TraitFormalParam< RPWP<range1d<T> > > : public TraitFormalParam<range1d_rpwp<T> > {};
  template<typename T>
  struct TraitFormalParam< CW<range1d<T> > > : public TraitFormalParam<range1d_cw<T> > {};
  template<typename T>
  struct TraitFormalParam< ICW<range1d<T> > > : public TraitFormalParam<range1d_icw<T> > {};
  template<typename T>
  struct TraitFormalParam< ST<range1d<T> > > : public TraitFormalParam<range1d_stack<T> > {};


  template<typename T, Storage2DClass S>
  struct TraitFormalParam<range2d<T,S> > : public TraitFormalParam<array<2,T,S> > {
    typedef ACCESS_MODE_RPWP   mode_t; 
  };
  
  template<typename T, Storage2DClass S>
  struct TraitFormalParam<range2d_r<T,S> > : public TraitFormalParam<pointer_r<array<2,T,S> > > { 
    typedef TraitFormalParam<pointer_r<array<2,T,S> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range2d_r<T,S>               formal_t;
    typedef R<range2d<T,S> >             signature_t;
  };

  template<typename T, Storage2DClass S>
  struct TraitFormalParam<range2d_rp<T,S> > : public TraitFormalParam<pointer_rp<array<2,T,S> > > {
    typedef TraitFormalParam<pointer_rp<array<2,T,S> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range2d_rp<T,S>               formal_t;
    typedef RP<range2d<T,S> >             signature_t;
  };

  template<typename T, Storage2DClass S>
  struct TraitFormalParam<range2d_w<T,S> > : public TraitFormalParam<pointer_w<array<2,T,S> > > {
    typedef TraitFormalParam<pointer_w<array<2,T,S> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range2d_w<T,S>    formal_t; 
    typedef W<range2d<T,S> >  signature_t; 
  };
  
  template<typename T, Storage2DClass S>
  struct TraitFormalParam<range2d_rw<T,S> > : public TraitFormalParam<pointer_rw<array<2,T,S> > > {
    typedef TraitFormalParam<pointer_rw<array<2,T,S> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range2d_rw<T,S>   formal_t; 
    typedef RW<range2d<T,S> > signature_t; 
  };
  
  template<typename T, Storage2DClass S>
  struct TraitFormalParam<range2d_rpwp<T,S> > : public TraitFormalParam<pointer_rpwp<array<2,T,S> > > {
    typedef TraitFormalParam<pointer_rpwp<array<2,T,S> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range2d_rpwp<T,S>   formal_t; 
    typedef RPWP<range2d<T,S> > signature_t; 
  };
  
  template<typename T, Storage2DClass S>
  struct TraitFormalParam<range2d_cw<T,S> > : public TraitFormalParam<pointer_cw<array<2,T,S> > > {
    typedef TraitFormalParam<pointer_cw<array<2,T,S> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range2d_cw<T,S>     formal_t; 
    typedef CW<range2d<T,S> >   signature_t;
  };
  
  template<typename T, Storage2DClass S>
  struct TraitFormalParam<range2d_icw<T,S> > : public TraitFormalParam<pointer_icw<array<2,T,S> > > {
    typedef TraitFormalParam<pointer_icw<array<2,T,S> > > inherited_t;
    typedef typename inherited_t::type_inclosure_t type_inclosure_t;
    typedef range2d_icw<T,S>    formal_t;
    typedef ICW<range2d<T,S> >  signature_t;
  };

  template<typename T, Storage2DClass S>
  struct TraitFormalParam< R<range2d<T,S> > > : public TraitFormalParam<range2d_r<T,S> > {};
  template<typename T, Storage2DClass S>
  struct TraitFormalParam< RP<range2d<T,S> > > : public TraitFormalParam<range2d_rp<T,S> > {};
  template<typename T, Storage2DClass S>
  struct TraitFormalParam< W<range2d<T,S> > > : public TraitFormalParam<range2d_w<T,S> > {};
  template<typename T, Storage2DClass S>
  struct TraitFormalParam< WP<range2d<T,S> > > : public TraitFormalParam<range2d_wp<T,S> > {};
  template<typename T, Storage2DClass S>
  struct TraitFormalParam< RW<range2d<T,S> > > : public TraitFormalParam<range2d_rw<T,S> > {};
  template<typename T, Storage2DClass S>
  struct TraitFormalParam< RPWP<range2d<T,S> > > : public TraitFormalParam<range2d_rpwp<T,S> > {};
  template<typename T, Storage2DClass S>
  struct TraitFormalParam< CW<range2d<T,S> > > : public TraitFormalParam<range2d_cw<T,S> > {};
  template<typename T, Storage2DClass S>
  struct TraitFormalParam< ICW<range2d<T,S> > > : public TraitFormalParam<range2d_icw<T,S> > {};


  /* ------ rep of array into a closure      
   */
  template<int dim, typename T, Storage2DClass S=RowMajor>
  struct arraytype_inclosure_t;
  
  template<typename T, Storage2DClass S>
  struct arraytype_inclosure_t<1,T,S> {
    arraytype_inclosure_t<1,T,S>( const array<1,T,S>& a ) : _data(a), _version() {}
    
    int size() const { return _data.size(); }
    
    array_rep<1,T,S>              _data;
    array_rep<1,T*,S>             _version; /* only used if the task is stolen */
  private:
    arraytype_inclosure_t<1,T,S>() {}
  };
  
  template<typename T,Storage2DClass S>
  struct arraytype_inclosure_t<2,T,S> {
    arraytype_inclosure_t<2,T,S>( const array<2,T,S>& a ) : _data(a), _version() { }
    size_t size() const { return _data.dim(0)*_data.dim(1); }
    
    array_rep<2,T,S>              _data;
    array_rep<2,T*,S>             _version; /* only used if the task is stolen */
  private:
    arraytype_inclosure_t<2,T,S>() {}
  };
  
  /* ------ specialisation of representation of array */
  template<typename T>
  class array1d_rep_with_write {
  public:
    typedef base_array::index_t index_t;
    typedef T*                  pointer_t;
    
    class reference_t {
    public:
      reference_t( T& v ) : ref(v) {}
      reference_t operator=( const T& v )
      { ref = v; return *this; }
    private:
      T& ref;
    };
    
    class const_reference_t {
    public:
      const_reference_t( T& v ) : ref(v) {}
    private:
      T& ref;
    };
    
    /** */
    array1d_rep_with_write() : _data(0) {}
    /** */
    array1d_rep_with_write(T* ptr, size_t sz) : _data(ptr), _size(sz) {}
    
    /** */
    size_t size() const
    { return _size; }
    
    /** */
    reference_t operator[](index_t i)
    { return reference_t(_data[i]); }
    /** */
    pointer_t operator+(index_t i) const
    { return _data+ i; }
    /** */
    void set (index_t i, const T& value) const 
    { _data[i] = value; }
    /** */
    pointer_t shift_base(index_t shift) 
    { return _data+shift; }
  protected:
    T*     _data;
    size_t _size;
  };
  
  template<typename T>
  class array1d_rep_with_read {
  public:
    typedef base_array::index_t index_t;
    typedef T*                  pointer_t;
    typedef const T&            const_reference_t;
    typedef const T&            reference_t;
    
    /** */
    array1d_rep_with_read() : _data(0) {}
    /** */
    array1d_rep_with_read(T* ptr, size_t sz) : _data(ptr), _size(sz) {}
    
    /** */
    size_t size() const
    { return _size; }
    
    /** */
    const_reference_t operator[](index_t i)
    { return _data[i]; }
    /** */
    const_reference_t operator[](index_t i) const
    { return _data[i]; }
    /** */
    pointer_t operator+(index_t i) const
    { return _data+ i; }
    /** */
    const_reference_t get (index_t i) const 
    { return _data[i]; }
    /** */
    pointer_t shift_base(index_t shift) 
    { return _data+shift; }
  protected:
    T*     _data;
    size_t _size;
  };
  
  /* ------ specialisation of representation of array */
  template<typename T>
  class array1d_rep_with_readwrite {
  public:
    typedef base_array::index_t index_t;
    typedef T*                  pointer_t;
    
    class reference_t {
    public:
      reference_t( T& v ) : ref(v) {}
      reference_t operator=( const T& v )
      { ref = v; return *this; }
      operator T&() { return ref; }
      T* operator ->() { return &ref; }
    private:
      T& ref;
    };
    
    class const_reference_t {
    public:
      const_reference_t( T& v ) : ref(v) {}
      operator const T&() const { return ref; }
    private:
      T& ref;
    };
    
    /** */
    size_t size() const
    { return _size; }
    
    /** */
    array1d_rep_with_readwrite() : _data(0), _size(0) {}
    /** */
    array1d_rep_with_readwrite(T* ptr, size_t sz) : _data(ptr), _size(sz) {}
    /** */
    reference_t operator[](index_t i)
    { return reference_t(_data[i]); }
    /** */
    const_reference_t operator[](index_t i) const
    { return const_reference_t(_data[i]); }
    /** */
    pointer_t operator+(index_t i) const
    { return _data+ i; }
    /** */
    void set (index_t i, const T& value) const 
    { _data[i] = value; }
    /** */
    pointer_t shift_base(index_t shift) 
    { return _data+shift; }
  protected:
    T*     _data;
    size_t _size;
  };
  
  /* specialisation of array representation */
  template<typename T>
  class array_rep<1,pointer_r<T>,RowMajor> : public array1d_rep_with_read<T> {
  public:
    array_rep<1,pointer_r<T>,RowMajor>( arraytype_inclosure_t<1,T,RowMajor>& arg_clo )
    : array1d_rep_with_read<T>( arg_clo._data.ptr(), arg_clo._data.size() ) 
    {
    }
  };
  
  /* specialisation of array representation */
  template<typename T>
  class array_rep<1,pointer_w<T>,RowMajor > : public array1d_rep_with_write<T> {
  public:    
    array_rep<1,pointer_w<T>,RowMajor >( arraytype_inclosure_t<1,T>& arg_clo )
    : array1d_rep_with_write<T>( arg_clo._data.ptr(), arg_clo._data.size() ) 
    {
    }
  };
  
  /* specialisation of array representation */
  template<typename T>
  class array_rep<1,pointer_rw<T>,RowMajor > : public array1d_rep_with_readwrite<T> {
  public:    
    array_rep<1,pointer_rw<T>,RowMajor >( arraytype_inclosure_t<1,T>& arg_clo )
    : array1d_rep_with_readwrite<T>( arg_clo._data.ptr(), arg_clo._data.size() ) 
    {
    }
  };
  
  
  template<int dim, typename T>
  struct TraitFormalParam<array<dim, pointer<T> > > { 
    typedef arraytype_inclosure_t<dim,T> type_inclosure_t;
    typedef ACCESS_MODE_RPWP   mode_t; 
    typedef T                  type_t;
    static const bool          is_static = false;
    static const void*         ptr( const array<dim, pointer<T> >* a ) { return a->ptr(); }
  };
  
#if 1
  template<int dim, typename T>
  struct TraitFormalParam<array<dim, T> > { 
    typedef arraytype_inclosure_t<dim,T> type_inclosure_t;
    typedef ACCESS_MODE_RPWP   mode_t; 
    typedef T                  type_t;
    static const bool          is_static = false;
    static const void*         ptr( const array<dim, T>* a ) { return a->ptr(); }
    static size_t              get_nparam( const type_inclosure_t* a ) { return a->size(); }
  };
#endif

  template<int dim, typename T>
  struct TraitFormalParam<array<dim, pointer_r<T> > > { 
    typedef arraytype_inclosure_t<dim,T> type_inclosure_t;
    typedef array<dim, R<T> >         signature_t; 
    typedef array<dim, pointer_r<T> > formal_t; 
    typedef ACCESS_MODE_R             mode_t; 
    typedef T                         type_t;
    static const bool                 is_static = false;
    static void*                get_data   ( const type_inclosure_t* a, unsigned int i ) { return &a->_data[i]; }
    static const void*                get_version( const type_inclosure_t* a, unsigned int i ) { return &a->_version[i]; }
    static void                       get_access ( const type_inclosure_t* a, unsigned int i, kaapi_access_t* r ) 
    { r->data = (void*)&a->_data[i]; r->version = (void*)&a->_version[i]; }
    static void                       set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) 
    { if( &a->_data[i] != (type_t*)r->data)
      a->_data[i] = *(type_t*)r->data; 
      a->_version[i] = (type_t*)r->version; 
    }
    static size_t                     get_nparam( const type_inclosure_t* a ) { return a->size(); }
    static size_t                     get_size_param( const type_inclosure_t* a, unsigned int i ) 
    { return TraitFormalParam<type_t>::get_size_param( &a->_data[i], 0); }
    static void                       reducor_fnc(void*, const void*) {}
    static void                       redinit_fnc(void*) {}
  };
  
  template<int dim, typename T>
  struct TraitFormalParam<array<dim, pointer_w<T> > > { 
    typedef arraytype_inclosure_t<dim,T> type_inclosure_t;
    typedef array<dim, W<T> >         signature_t; 
    typedef array<dim, pointer_w<T> > formal_t; 
    typedef ACCESS_MODE_W             mode_t; 
    typedef T                         type_t;
    static const bool                 is_static = false;
    static void*                get_data   ( const type_inclosure_t* a, unsigned int i ) { return &a->_data[i]; }
    static const void*                get_version( const type_inclosure_t* a, unsigned int i ) { return &a->_version[i]; }
    static void                       get_access ( const type_inclosure_t* a, unsigned int i, kaapi_access_t* r ) 
    { r->data = (void*)&a->_data[i]; r->version = (void*)&a->_version[i]; }
    static void                       set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) 
    { if( &a->_data[i] != (type_t*)r->data)
      a->_data[i] = *(type_t*)r->data; 
      a->_version[i] = (type_t*)r->version; 
    }
    static size_t                     get_nparam( const type_inclosure_t* a ) { return a->size(); }
    static size_t                     get_size_param( const type_inclosure_t* a, unsigned int i ) 
    { return TraitFormalParam<type_t>::get_size_param( &a->_data[i], 0); }
    static void                       reducor_fnc(void*, const void*) {}
    static void                       redinit_fnc(void*) {}
  };
  
  template<int dim, typename T>
  struct TraitFormalParam<array<dim, pointer_rw<T> > > { 
    typedef arraytype_inclosure_t<dim,T> type_inclosure_t;
    typedef array<dim, RW<T> >        signature_t; 
    typedef array<dim, pointer_rw<T> > formal_t; 
    typedef ACCESS_MODE_RW            mode_t; 
    typedef T                         type_t;
    static const bool                 is_static = false;
    static void*                      get_data   ( const type_inclosure_t* a, unsigned int i ) { return &a->_data[i]; }
    static const void*                get_version( const type_inclosure_t* a, unsigned int i ) { return &a->_version[i]; }
    static void                       get_access ( const type_inclosure_t* a, unsigned int i, kaapi_access_t* r ) 
    { r->data = (void*)&a->_data[i]; r->version = (void*)&a->_version[i]; }
    static void                       set_access ( type_inclosure_t* a, unsigned int i, const kaapi_access_t* r ) 
    { if( &a->_data[i] != (type_t*)r->data)
      a->_data[i] = *(type_t*)r->data; 
      a->_version[i] = (type_t*)r->version; 
    }
    static size_t                     get_nparam ( const type_inclosure_t* a ) { return a->size(); }
    static size_t                     get_size_param( const type_inclosure_t* a, unsigned int i ) 
    { return TraitFormalParam<type_t>::get_size_param( &a->_data[i], 0); }
    static void                       reducor_fnc(void*, const void*) {}
    static void                       redinit_fnc(void*) {}
  };   
  
  
  template<int dim, typename T>
  struct TraitFormalParam<array<dim, R<T> > > : public TraitFormalParam<array<dim, pointer_r<T> > > {};
  template<int dim, typename T>
  struct TraitFormalParam<array<dim, W<T> > > : public TraitFormalParam<array<dim, pointer_w<T> > > {};
  template<int dim, typename T>
  struct TraitFormalParam<array<dim, RW<T> > > : public TraitFormalParam<array<dim, pointer_rw<T> > > {};

  /* ------ */
  //  template<typename UserType>
  //  template<typename T>
  //  struct TraitFormalParam<pointer_rw<T>& > : public TraitFormalParam<pointer_rw<T> > { 
  //  };
  
#if 0 // no more used: detection of correct passing rule using both effective and formal type.
  template<bool isfunc, class UserType> struct __kaapi_pointer_switcher {};
  
  template<class UserType> struct __kaapi_pointer_switcher<false, const UserType*> {
    typedef TraitFormalParam<pointer_rp<UserType> >   tfp_t;
  };
  template<class UserType> struct __kaapi_pointer_switcher<false, UserType*> {
    typedef TraitFormalParam<pointer_rpwp<UserType> > tfp_t;
  };
  template<class UserType> struct __kaapi_pointer_switcher<true, const UserType*> {
    typedef TraitFormalParam<Value<UserType*> >        tfp_t;
  };
  template<class UserType> struct __kaapi_pointer_switcher<true, UserType*> {
    typedef TraitFormalParam<Value<UserType*> >        tfp_t;
  };
  
  /* to be able to use pointer to data as arg of spawn. If it is a pointer to function, 
   consider it as a pass-by-value passing rule
   */
  template<typename UserType>
  struct TraitFormalParam<const UserType*> : public 
  __kaapi_pointer_switcher< __kaapi_is_function<const UserType*>::value, const UserType*>::tfp_t
  {
  };
  
  template<typename UserType>
  struct TraitFormalParam<UserType*> : public 
  __kaapi_pointer_switcher< __kaapi_is_function<UserType*>::value, UserType*>::tfp_t
  {
  };
#endif
  
  /* used to initialize representation into a closure from effective parameter */
  template<class E, class F, class InClosure>
  struct ConvertEffective2InClosure {
    static inline  /* __attribute__((__always_inline__)) */
    void doit(InClosure* inclo, const E& e) { new (inclo) InClosure(e); }
  };
  
  
  // --------------------------------------------------------------------
  template<class F>
  struct TraitIsOut {
    enum { value = 0 };
  };
  template<class F>
  struct TraitIsOut<F&> {
    enum { value = 1 };
  };
  
  
  // --------------------------------------------------------------------
  /* for better understand error message */
  template<int i>
  struct FOR_ARG {};
  
  /* for better understand error message */
  template<class TASK>
  struct FOR_TASKNAME {};
  
  /* ME: effectif -> MF: formal */
  template<class ME, class MF, class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE {
    //    static void IS_COMPATIBLE();
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_V, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };

  /* specialization if effective mode is C++ pointer + formal is kaapi pointer */
  template<class T, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_R, T*, F, PARAM, TASK> 
  : WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_R, T*, F, PARAM, TASK> 
  { };
  template<class T, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_RP, T*, F, PARAM, TASK> 
  : WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_RP, T*, F, PARAM, TASK> 
  { };
  template<class T, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_W, T*, F, PARAM, TASK> 
  : WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_W, T*, F, PARAM, TASK> 
  { };
  template<class T, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_WP, T*, F, PARAM, TASK> 
  : WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_WP, T*, F, PARAM, TASK> 
  { };
  template<class T, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_RW, T*, F, PARAM, TASK> 
  : WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_RW, T*, F, PARAM, TASK> 
  { };
  template<class T, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_RPWP, T*, F, PARAM, TASK> 
  : WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_RPWP, T*, F, PARAM, TASK> 
  { };
  template<class T, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_CW, T*, F, PARAM, TASK> 
  : WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_CW, T*, F, PARAM, TASK> 
  { };
  template<class T, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_CWP, T*, F, PARAM, TASK> 
  : WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_CWP, T*, F, PARAM, TASK> 
  { };
  template<class T, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_ICW, T*, F, PARAM, TASK> 
  : WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_ICW, T*, F, PARAM, TASK> 
  { };
  template<class T, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_STACK, T*, F, PARAM, TASK> 
  : WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_STACK, T*, F, PARAM, TASK> 
  { };

  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_R, ACCESS_MODE_R, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK> /* this rule is only valid for terminal fork... */
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_W, ACCESS_MODE_W, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_CW, ACCESS_MODE_CW, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_ICW, ACCESS_MODE_ICW, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_RPWP, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_RW, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_WP, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_W, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_CWP, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_CW, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_ICW, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_R, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_RP, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RP, ACCESS_MODE_RP, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RP, ACCESS_MODE_R, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_WP, ACCESS_MODE_WP, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_WP, ACCESS_MODE_W, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_CWP, ACCESS_MODE_CW, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_CWP, ACCESS_MODE_ICW, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_CWP, ACCESS_MODE_CWP, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };

  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_RPWP, ACCESS_MODE_STACK, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  template<class E, class F, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_STACK, ACCESS_MODE_STACK, E, F, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  
  
  /* specialization if effective mode is C++ pointer + formal parameter is a C++ pointer */
  template<class T, class PARAM, class TASK>
  struct WARNING_UNDEFINED_PASSING_RULE<ACCESS_MODE_V, ACCESS_MODE_V, T*, T*, PARAM, TASK> {
    static void IS_COMPATIBLE(){}
  };
  
  /* required for most of stl like parallel algorithm
   */
  template <typename type >
  class counting_iterator : public std::iterator< 
  std::random_access_iterator_tag,     /* category */
  const type                           /* element type */                                            
  >
  {
  public:
    typedef type value_type;
    typedef ptrdiff_t difference_type;
    typedef const type& reference;
    typedef const type* pointer;
    
    counting_iterator()
    : _rep(0)
    {}
    explicit counting_iterator(value_type x) 
    : _rep(x) 
    {}
    value_type const& base() const
    { return _rep; }
    
    counting_iterator& operator++() 
    { 
      ++_rep;
      return *this;
    }
    counting_iterator& operator+=(int i) 
    { 
      _rep+=i;
      return *this;
    }
    counting_iterator& operator+=(size_t i) 
    { 
      _rep+=i;
      return *this;
    }
    counting_iterator operator++(int) 
    { 
      counting_iterator retval = *this;
      ++_rep;
      return retval; 
    }
    counting_iterator& operator--() 
    { 
      --_rep;
      return *this;
    }
    counting_iterator& operator-=(int i) 
    { 
      _rep -=i;
      return *this;
    }
    counting_iterator& operator-=(size_t i) 
    { 
      _rep -=i;
      return *this; 
    }
    counting_iterator operator--(int) 
    { 
      counting_iterator retval = *this;
      --_rep;
      return retval; 
    }
    difference_type operator-(const counting_iterator& it) const
    { return _rep - it._rep; }
    counting_iterator operator+(value_type v) const
    { return counting_iterator(_rep +v); }
    counting_iterator operator-(value_type v) const
    { return counting_iterator(_rep -v); }
    counting_iterator operator[](int i) const
    { return counting_iterator(_rep +i); }
    bool operator==(const counting_iterator& rhs) 
    { return (_rep==rhs._rep); }
    
    bool operator!=(const counting_iterator& rhs) 
    { return (_rep!=rhs._rep); }
    
    reference operator*() 
    { return _rep; }
    
    pointer operator->() 
    { return &_rep; }
    
  private:
    value_type _rep;
  };
  
  /* ICI: signature avec kaapi_stack & kaapi_task as first parameter ?
   Quel interface C++ pour les tâches adaptatives ?
   */
  
  template<int i>
  struct Task {};
  
  template<int i>
  struct Splitter {};
  
  template<typename TYPE>
  kaapi_no_type kaapi_is_func_splitter( TYPE );
  
  template<class TASK>
  struct IsASplitter {
    static const bool is_splitter = false;
  };
  
} // end of namespace atha: following definition sould be in global namespace in 
// order to be specialized easily

// --------------------------------------------------------------------  
template<class TASK>
struct TaskBodyCPU : public TASK {};

// --------------------------------------------------------------------
template<class TASK>
struct TaskBodyGPU : public TASK {};

template<class TASK>
struct TaskBodyAlpha : public TASK {};

/* Task Interface for debug information using dot graph */
template<class TASK>
struct TaskDOT: public TASK {
  /* DOT name to display */
  static const char* name() { return 0; }
  
  /* DOT color name to display */
  static const char* color() { return 0; }
};

// --------------------------------------------------------------------  
template<class TASK>
struct TaskSplitter;

namespace ka {
  
  /**
   */
  struct FlagReplyHead {};
  extern FlagReplyHead ReplyHead;
  struct FlagReplyTail {};
  extern FlagReplyTail ReplyTail;
  
  class Request;
  
  /* Class to envelop iteration over requests
   */
  class ListRequest {
  public:
    /* forward iterator */
    struct iterator {
      Request* operator->()
      { 
        return (Request*)(kaapi_listrequest_iterator_get(_lri));
      }
      
      Request* operator*()
      { 
        return (Request*)(kaapi_listrequest_iterator_get(_lri));
      }
      
      /* */
      bool operator==(const iterator& i) const
      { 
        return
           (   ((_lri !=0) && (i._lri !=0)
                && (kaapi_listrequest_iterator_get( _lri) ==
                    kaapi_listrequest_iterator_get( i._lri) ))
            || ((_lri ==0) && (i._lri ==0))
           );
      }
      
      /* */
      bool operator!=(const iterator& i) const
      { return !( *this == i); }
      
      /* prefix op*/
      iterator& operator++()
      {
        if (0 == kaapi_listrequest_iterator_next(_lri))
          _lri = 0;
        return *this;
      }
      
    public: /* to be used by wrapper to C++ splitter */
      iterator( kaapi_listrequest_iterator_t* lri)
      : _lri(lri)
      {}
    private:
      kaapi_listrequest_iterator_t* _lri;
    };
  };
  
  
  // --------------------------------------------------------------------
  template<class TASK>
  struct KaapiTask0 {
    static TASK dummy;
    static void body( kaapi_task_t* task, kaapi_thread_t* stack )
    { 
      dummy();
    }
  };
  template<class TASK>
  TASK KaapiTask0<TASK>::dummy;
  
#include "ka_api_clo.h"
}

// --------------------------------------------------------------------  
template<class TASK>
struct TaskSplitter : public ka::Splitter<TASK::nargs> {};

namespace ka {
  
  template<class TASK>
  struct TraitSplitter {
    static const bool has_splitter = 
    (sizeof(kaapi_yes_type) == sizeof(kaapi_is_func_splitter( &TaskSplitter<TASK>::operator())));
  };
  
#if 0
  // --------------------------------------------------------------------
  /* Mapping of data into logical set of partitions
   */
  struct BlockCyclic1D {
    BlockCyclic1D()
    : np(0), bs(0), M(0)
    {}
    
    /* P: number of ressources, B: block size */
    BlockCyclic1D(int P, int B)
    : np(P), bs(B), M(B*P) 
    {}
    
    /* element maping */
    struct rep {
      int p;   /* ressource number */
      int b;   /* block number on the ressource */
      int i;   /* index in the block */
    };
    
    /* map global index to full representation */
    void map( rep& r, int index ) const
    {
      r.p = (index % M)/bs;
      r.b = index / M;
      r.i = index % bs;
    }
    
    /* specialized: map global index to ressource id */
    int map_ressource( int index ) const
    {
      return (index % M)/bs;
    }
    
    /* inverse mapping */
    void invmap( int& index, const rep& r) const
    {
      index = r.p * bs + r.b * M + r.i;
    }
    
    /* return the number of ressources */
    int size() const
    { return np; }
    
    int np;  /* ressource number */
    int bs;  /* = bloc size */
    int M;   /* = np * bloc size */
  };
  
  /* 2D version */
  struct BlockCyclic2D {
    BlockCyclic2D()
    : d1(), d2()
    {}
    
    /* P: number of ressources, B: block size */
    BlockCyclic2D(int P1, int B1, int P2, int B2)
    : d1(P1,B1), d2(P2,B2)
    {}
    
    /* element maping */
    struct rep {
      BlockCyclic1D::rep d1;
      BlockCyclic1D::rep d2;
    };
    
    /* map global index to full representation */
    void map( rep& r, int index1, int index2 ) const
    {
      d1.map(r.d1, index1);
      d2.map(r.d2, index2);
    }
    
    /* specialized: map global index to ressource k */
    int map_ressource( int index1, int index2 ) const
    {
      int r1, r2;
      r1 = d1.map_ressource(index1);
      r2 = d2.map_ressource(index2);
      return r1 * d2.size() + r2;
    }
    
    /* inverse mapping */
    void invmap( int& index1, int& index2, const rep& r) const
    {
      d1.invmap( index1, r.d1 );
      d2.invmap( index2, r.d2 );
    }
    
    /* return the number of ressources */
    int size() const
    { return d1.size()*d2.size(); }
    
    BlockCyclic1D d1;
    BlockCyclic1D d2;
  };
  
  /* specialization for Block distribution */
  struct Block1D : public BlockCyclic1D {
    Block1D() 
    : BlockCyclic1D()
    {}
    /* P: number of ressources, L: size of the sequence */
    Block1D(int P, int L)
    : BlockCyclic1D(P, L/P )
    {}
  };
  
  struct Block2D : public BlockCyclic2D {
    Block2D() 
    : BlockCyclic2D()
    {}
    /* P: number of ressources, L: size of the sequence */
    Block2D(int P1, int L1, int P2, int L2)
    : BlockCyclic2D(P1, L1/P1, P2, L2/P2 )
    {}
  };
  
  template<class DD>
  struct Distribution1D {
    static int map( const DD& d, int index )
    { 
      int retval;
      d.map_ressource( retval, index );
      return retval;
    }
  };
  
  /* 2D distribution.
   Given the set of ressources (cpu,gpu), this class returns the kid of 
   the ressource where global index (index1,index2) is distributed.
   The kid is computed using a linearization of the grid of ressources 
   of the distribution dd. The set of ressources may be either mapped to 
   ncpu (may be =0) and ngpu (may be =0).
   More precisely, the final set of ressources is between 0 and K-1.
   The first ncpu ones correspond to CPU, followed by ngpu kid for GPUs.
   
   If the size of ressources in the distribution DD does not correspond to the
   size of the set, then the kid is returned modulo the number of specified 
   ressources.
   
   Implementation note:
   - DD is assumed to have the following interface/fields:
   * int map_ressource( int index1, int index2 )
   that return the global index of the ressource in range [0:N).
   - the Encode set is assume to be consistent with the real used hardware,
   if the number of GPU is greather than the number of GPUs rely available,
   then it was not detected and will certainely generate and error.
   */
  template<class DD>
  struct Distribution2D {
    Distribution2D( _KaapiCPUGPU_Encode set, const DD& d )
    : _set(set), _dist(d)
    {}
    
    /* Return the kid where global index (index1,index2) is mapped
     */
    int map_ressource( int index1, int index2 ) const
    {
      int k = _dist.map_ressource( index1, index2 );
      if (k < _set.ncpu) 
        return k;
      else 
        return kaapi_getconcurrency_cpu() + (k - _set.ncpu);
    }
    int operator()(int index1, int index2) const
    { return map_ressource(index1, index2); }
    
    _KaapiCPUGPU_Encode _set;
    const DD& _dist;
  };
#endif

  // --------------------------------------------------------------------
  /* New API: thread.Spawn<TASK>([ATTR])( args )
   Spawn<TASK>([ATTR])(args) with be implemented on top of 
   System::get_current_thread()->Spawn<TASK>([ATTR])( args ).
   */
  class Thread {
  private:
    Thread() {}
  public:

    static inline Thread* self()
    { return (Thread*)kaapi_self_thread(); }
    
    template<class T>
    inline __attribute__((__always_inline__))
    T* Alloca(size_t size)
    {
      void* data = kaapi_data_push( &_thread, sizeof(T)*size );
      return new (data) T[size];
    }
    
    template<class T>
    inline __attribute__((__always_inline__))
    T* Alloca()
    {
      void* data = kaapi_data_push( &_thread, sizeof(T) );
      return new (data) T[1];
    }

    inline __attribute__((__always_inline__))
    void Sync()
    { kaapi_sched_sync(&_thread); }
    
    template<class TASK,class ATTR>
    class Spawner {
    public:
      inline __attribute__((__always_inline__))
      Spawner( kaapi_thread_t* t, const ATTR& a ) : _thread(t), _attr(a) {}
      
#include "ka_api_spawn.h"      
      
    protected:
      kaapi_thread_t* _thread;
      const ATTR&     _attr;
    };
    
    template<class TASK>
    inline __attribute__((__always_inline__))
    Spawner<TASK,DefaultTaskAttribut> Spawn()
    { return Spawner<TASK,DefaultTaskAttribut>(&_thread,SetDefaultTaskAttribut); }


    template<class TASK, class ATTR>
    inline __attribute__((__always_inline__))
    Spawner<TASK, ATTR > Spawn(const ATTR& a)
    { return Spawner<TASK, ATTR>(&_thread, a); }
    
  protected:
    kaapi_thread_t _thread;
    friend class SyncGuard;
  };
  
  
  // --------------------------------------------------------------------
  /** Top level Spawn */
  template<class TASK>
  inline __attribute__((__always_inline__))
  Thread::Spawner<TASK,DefaultTaskAttribut> Spawn()
  { return Thread::Spawner<TASK,DefaultTaskAttribut>(kaapi_self_thread(),SetDefaultTaskAttribut); }
  
  template<class TASK, class ATTR>
  inline __attribute__((__always_inline__))
  Thread::Spawner<TASK, ATTR> Spawn(const ATTR& a) 
  { return Thread::Spawner<TASK, ATTR>(kaapi_self_thread(), a); }

    
#if 0 //TODO
  /* New API: request->Spawn<TASK>(sc)( args ) for adaptive tasks
     We must be able to :
          - define a splitter like a entrypoint for the task
     The trait TraitSplitter could be used to push automatically a adaptive task
     in ka::Spawn<TASK>([attribut] )( args ) for normal creation or during the reply to 
     req->Spawn<TASK>(victim task)( args );
  */
  class Request {
  private:
    Request() {}
    
    template<class TASK>
    class Spawner {
    protected:
      Spawner( kaapi_request_t* r, kaapi_task_t* task ) 
      : _req(r), _adaptivetask(task)) 
      {}
      
    public:
      /**
       */      
      void operator()()
      { 
        kaapi_task_t* task = kaapi_request_alloctask(_req);
        kaapi_task_init(task,  KaapiTask0<TASK>::body );
        if (TraitSplitter<TASK>::has_splitter)
          kaapi_request_pushtask_adaptive( _req, _adaptivetask, task,
                                          KaapiWrapperSplitter0<TASK>::splitter 
                                          );
        else 
          kaapi_request_pushtask( _req, _adaptivetask, task );
      }
      
#include "ka_api_reqspawn.h"
      
    protected:
      kaapi_request_t*  _req;           /* the request to reply */
      kaapi_task_t*     _adaptivetask;  /* the victim task to pass on reply (terminaison/preemption)*/
      friend class Request;
    };
    
  public:
    template<class TASK,class VICTIM_TASK>
    Spawner<TASK> Spawn(Request* req, VICTIM_TASK*) 
    { return Spawner<TASK>(&_request, &sc->_adaptivetask); }
    template<class TASK>
    Spawner<TASK> Spawn(StealContext* sc, FlagReplyHead flag) 
    { return Spawner<TASK>(&_request, &sc->_adaptivetask); }
    template<class TASK>
    Spawner<TASK> Spawn(StealContext* sc, FlagReplyTail flag) 
    { return Spawner<TASK>(&_request, &sc->_adaptivetask); }
    
    /* reply to the request */
    void commit()
    { kaapi_request_committask(&_request); }
    
    /* return its id */
    int ident() const
    { return _request.ident; }
    
    /* return a buffer of size bytes */
    void* allocate(size_t size)
    { return kaapi_request_pushdata(&_request, size); }
    
  protected:
    kaapi_request_t _request;
  };
  
  
  template< typename TASK, 
            void (TASK::*pmethod)(TASK*, 
            int count, 
            ListRequest::iterator, 
            ListRequest::iterator)
  >
  int WrapperSplitter(
                      kaapi_task_t* victim, 
                      struct kaapi_listrequest_t* lr,
                      struct kaapi_listrequest_iterator_t* lri 
                      )
  { 
    TASK* o = (TASK*)victim; 
    (o->*pmethod)( (TASK*)victim,
                  kaapi_listrequest_iterator_count(lri),
                  ListRequest::iterator(lr,lri), ListRequest::iterator(lr,0) );
    return 0;
  }
  
#endif


  // --------------------------------------------------------------------
  /* Main task */
  template<class TASK>
  struct MainTaskBodyArgcv {
    static void body( int argc, char** argv )
    {
      TASK()( argc, argv );
    }
  };
  template<class TASK>
  struct MainTaskBodyNoArgcv {
    static void body( int argc, char** argv )
    {
      TASK()( );
    }
  };
  
  template<class TASK>
  struct SpawnerMain
  {
    SpawnerMain(kaapi_thread_t* t)  : _thread(t) {}
    
    void operator()( int argc, char** argv)
    {
      kaapi_taskmain_arg_t* arg = 
        (kaapi_taskmain_arg_t*)kaapi_data_push(_thread, sizeof(kaapi_taskmain_arg_t) );
      arg->argc = argc;
      arg->argv = argv;
      arg->mainentry = &MainTaskBodyArgcv<TASK>::body;
      kaapi_task_push( _thread, kaapi_taskmain_body, arg );
    }
    
    void operator()()
    {
      kaapi_taskmain_arg_t* arg = 
        (kaapi_taskmain_arg_t*)kaapi_data_push(_thread, sizeof(kaapi_taskmain_arg_t) );
      arg->argc = 0;
      arg->argv = 0;
      arg->mainentry = &MainTaskBodyArgcv<TASK>::body;
      kaapi_task_push( _thread, kaapi_taskmain_body, arg );
    }
    
  protected:
    kaapi_thread_t* _thread;
  };
  
  template<class TASK>
  SpawnerMain<TASK> SpawnMain()
  {
    return SpawnerMain<TASK>(kaapi_self_thread());
  }


  // --------------------------------------------------------------------
  /** Wait execution of all forked tasks of the running task */
  inline void Sync()
  {
    kaapi_sched_sync( kaapi_self_thread() );
  }

/* experimental function */
  inline void SyncNoDfg()
  {
    kaapi_sched_sync_nodfg( kaapi_self_thread() );
  }

/* experimental function */
  inline void SyncDfg()
  {
    kaapi_sched_sync_dfg( kaapi_self_thread() );
  }

  // --------------------------------------------------------------------
  template<typename Mapper>
  struct WrapperMapping {
    static kaapi_address_space_id_t mapping_function( void* arg, int nodecount, int tid )
    {
      Mapper* mapper = static_cast<Mapper*> (arg);
      return (kaapi_address_space_id_t)(*mapper)(nodecount, tid);
    }
  };
    

  /** Move into the local address space all global memory ops */
  extern void MemorySync();
  
  template<typename mode_t, typename T> 
  struct MemorySyncFuncClass { 
    static void doit(const T*a) 
    { /* kaapi_memory_synchronize(); */ } // HERE MUST CALL SPECIFIC FUNCTION WITH PTR 
  };
  
  /* specialization for non shared object */
  template<typename T> 
  struct MemorySyncFuncClass<ACCESS_MODE_V,T> { 
    static void doit(const T*a) 
    { }
  };
  
  template<typename T>
  extern void MemorySync(const T& a)
  { MemorySyncFuncClass<typename TraitFormalParam<T>::mode_t, T>::doit(&a); }

  namespace Memory {
    template<typename T>
    void Register( T& a ) {
      kaapi_memory_bind( 0, a.ptr(), a.get_view() );
    }
    
    template<typename T>
    void Unregister( T& a ) {
      kaapi_memory_unbind( a.ptr(), a.get_view() );
    }
  }
  

  // --------------------------------------------------------------------
  // Call delete on the pointer
  template<class T>
  struct TaskDelete : public Task<1>::Signature<RW<T> > { };

  // --------------------------------------------------------------------
  // Only call the destructor. Not necessary for POD type.
  template<class T>
  struct TaskDestroy : public Task<1>::Signature<RW<T> > { };

  /* specialization: always push Delete with unstealable flag */
  template<class ATTR, typename T, typename TraitFormalParam1>
  struct KaapiPushTask1<false, ATTR, TaskDelete<T>, TraitFormalParam1>
  {
    static inline __attribute__((__always_inline__))
    void push( const ATTR& attr, kaapi_thread_t* thread, kaapi_task_body_t body, void* arg ) 
    {
      kaapi_task_push_withflags( thread, body, arg, KAAPI_TASK_FLAG_UNSTEALABLE );
    }
  };

  /* specialize for DefaultTaskAttribut */
  template<typename T, typename TraitFormalParam1>
  struct KaapiPushTask1<false, DefaultTaskAttribut, TaskDelete<T> ,TraitFormalParam1> 
  {
    static inline __attribute__((__always_inline__))
    void push( const DefaultTaskAttribut& attr, kaapi_thread_t* thread, kaapi_task_body_t body, void* arg ) 
    {
      kaapi_task_push_withflags( thread, body, arg, KAAPI_TASK_FLAG_UNSTEALABLE );
    }
  };
  
} // namespace ka

// --------------------------------------------------------------------
template<typename TASK>
struct TaskFormat {
  typedef typename TASK::Signature View; /* here should be defined using default interpretation of args */
};

template<class T> struct TaskBodyCPU<ka::TaskDelete<T> > {
  void operator() ( ka::Thread* thread, ka::pointer_rw<T> res )
  { delete &*res; }
};

template<class T> struct TaskBodyCPU<ka::TaskDestroy<T> > {
  void operator() ( ka::Thread* thread, ka::pointer_rw<T> res )
  { res->T::~T(); }
};

namespace ka {
  
  template<bool noneedtaskdelete=false>
  struct SpawnDelete {
    template<class T> static void doit( pointer<T>& ap ) 
    { 
      Spawn<TaskDelete<T> >()(ap); 
      /* could not set to 0 the poiner, because may be used for spawned tasks */
    }
  };
  template<>
  struct SpawnDelete<true> {
    template<class T> static void doit( auto_pointer<T>& ap ) 
    { 
      /* could not set to 0 the poiner, because may be used for spawned tasks */
    }
  };
  
  template<bool noneedtaskdelete>
  struct SpawnDestroy {
    template<class T> static void doit( auto_variable<T>& av ) 
    { 
      Spawn<TaskDestroy<T> >()(&av); 
      /* could not clear the variable, because may be used for spawned tasks */
    }
  };
  template<>
  struct SpawnDestroy<true> {
    template<class T> static void doit( auto_variable<T>& av ) 
    { 
      /* could not clear the variable, because may be used for spawned tasks */
    }
  };
  
  template<class T>
  auto_pointer<T>::~auto_pointer()
  { SpawnDelete<false>::doit(*this); }
  
  template<class T>
  auto_variable<T>::~auto_variable()
  { SpawnDestroy<TraitNoDestroyTask<T>::value>::doit(*this); }
  
  
  // --------------------------------------------------------------------
  class SyncGuard {
    kaapi_thread_t*  _thread;
  public:
    SyncGuard();
    
    ~SyncGuard();
  };
  
  
  // --------------------------------------------------------------------
  // Should be defined for real distributed computation
  class OStream {
  public:
    enum Mode {
      IA = 1,
      DA = 2
    };
    size_t write( const Format*, int, const void*, size_t ) { return 0; }
  };
  
  class IStream {
  public:
    enum Mode {
      IA = 1,
      DA = 2
    };
    size_t read( const Format*, int, void*, size_t ) { return 0; }
  };
  
  // --------------------------------------------------------------------
  struct InitKaapiCXX {
    InitKaapiCXX();
  };
  static InitKaapiCXX kaapixx_dummy_InitKaapiCXX;
} // namespace ka


/* compatibility toolkit */
inline ka::OStream& operator<< (ka::OStream& s_out, char )
{ return s_out; }
inline ka::OStream& operator<< (ka::OStream& s_out, short)
{ return s_out; }
inline ka::OStream& operator<< (ka::OStream& s_out, int)
{ return s_out; }
inline ka::OStream& operator<< (ka::OStream& s_out, long)
{ return s_out; }
inline ka::OStream& operator<< (ka::OStream& s_out, unsigned char)
{ return s_out; }
inline ka::OStream& operator<< (ka::OStream& s_out, unsigned short)
{ return s_out; }
inline ka::OStream& operator<< (ka::OStream& s_out, unsigned int)
{ return s_out; }
inline ka::OStream& operator<< (ka::OStream& s_out, unsigned long)
{ return s_out; }
inline ka::OStream& operator<< (ka::OStream& s_out, float)
{ return s_out; }
inline ka::OStream& operator<< (ka::OStream& s_out, double)
{ return s_out; }

inline ka::IStream& operator>> (ka::IStream& s_in, char&)
{ return s_in; }
inline ka::IStream& operator>> (ka::IStream& s_in, short&)
{ return s_in; }
inline ka::IStream& operator>> (ka::IStream& s_in, int&)
{ return s_in; }
inline ka::IStream& operator>> (ka::IStream& s_in, long&)
{ return s_in; }
inline ka::IStream& operator>> (ka::IStream& s_in, unsigned char&)
{ return s_in; }
inline ka::IStream& operator>> (ka::IStream& s_in, unsigned short&)
{ return s_in; }
inline ka::IStream& operator>> (ka::IStream& s_in, unsigned int&)
{ return s_in; }
inline ka::IStream& operator>> (ka::IStream& s_in, unsigned long&)
{ return s_in; }
inline ka::IStream& operator>> (ka::IStream& s_in, float&)
{ return s_in; }
inline ka::IStream& operator>> (ka::IStream& s_in, double&)
{ return s_in; }


// --------------------------------------------------------------------
inline void* operator new( size_t size, ka::Request* arenea)
{ return kaapi_request_pushdata((kaapi_steal_request_t*)arenea, size); }

#if !defined(_KAAPIPLUSPLUS_NOT_IN_GLOBAL_NAMESPACE)
using namespace ka;
#endif

#endif

