// --------------------------------------------------------------------
// Kaapi closure representation
ifelse(KAAPI_NUMBER_PARAMS,0,`',`template<M4_PARAM(`typename TraitFormal$1', `', `, ')>')
struct KAAPI_TASKARG(KAAPI_NUMBER_PARAMS) { 
 M4_PARAM(`typedef typename TraitFormal$1::type_inclosure_t inclosure$1_t;
  ', ` ', `')
 static const bool is_static = true 
      M4_PARAM(`&& TraitFormal$1::is_static
      ', ` ', `')
      ;
 M4_PARAM(`inclosure$1_t f$1;
  ', ` ', `')
};


// --------------------------------------------------------------------
// KAAPI_NUMBER_PARAMS is the number of possible parameters
template<>
struct Task<KAAPI_NUMBER_PARAMS> {
  public:
  ifelse(KAAPI_NUMBER_PARAMS,0,`',`template<M4_PARAM(`class F$1', `', `, ')>')
  struct Signature { 
    M4_PARAM(`typedef typename TraitFormalParam<F$1>::type_inclosure_t inclosure$1_t;
    ', `', `')
    M4_PARAM(`typedef typename TraitFormalParam<F$1>::mode_t mode$1_t;
    ', `', `')
    M4_PARAM(`typedef F$1 signature$1_t;
    ', `', `')
    typedef KAAPI_TASKARG(KAAPI_NUMBER_PARAMS) ifelse(KAAPI_NUMBER_PARAMS,0,`',`<M4_PARAM(`TraitFormalParam<F$1>', `', `,') >') TaskArg_t;
    
    void operator() ( THIS_TYPE_IS_USED_ONLY_INTERNALLY M4_PARAM(`, signature$1_t', `', `') ) {}
    void dummy_method_to_have_formal_param_type ( Thread* M4_PARAM(`, signature$1_t', `', `') ){}
    static const int nargs = KAAPI_NUMBER_PARAMS;
  };
};

// --------------------------------------------------------------------
// KAAPI_NUMBER_PARAMS is the number of possible parameters. Same
template<>
struct Splitter<KAAPI_NUMBER_PARAMS> {
  void operator()( THIS_TYPE_IS_USED_ONLY_INTERNALLY ) {}
};

template<class TASK M4_PARAM(`,typename F$1', `', ` ')>
kaapi_yes_type kaapi_is_func_splitter( int (TASK::*)(void*, int, ListRequest::iterator, ListRequest::iterator M4_PARAM(`, F$1', `', ` ')) );

#if 0
// --------------------------------------------------------------------
// KAAPI_NUMBER_PARAMS is the number of possible parameters
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPIWRAPPER_SPLITTER(KAAPI_NUMBER_PARAMS) { 
  typedef KAAPI_TASKARG(KAAPI_NUMBER_PARAMS) ifelse(KAAPI_NUMBER_PARAMS,0,`',`<M4_PARAM(`TraitFormalParam$1', `', `,')>') TaskArg_t;
  M4_PARAM(`typedef typename TraitFormalParam$1::formal_t formal$1_t;
  ', ` ', `')
  static TaskSplitter<TASK> dummy;
  static int splitter( 
      struct kaapi_task_t* victim_task,
      void* task_arg,
      struct kaapi_listrequest_t* lr,
      struct kaapi_listrequest_iterator_t* lri
  )
  {
    int nreq = kaapi_listrequest_iterator_count(lri);
    ifelse(KAAPI_NUMBER_PARAMS,0,`',`TaskArg_t* args = (TaskArg_t*)task_arg;')
    dummy( 
        (StealContext*)victim_task,
        nreq,
        ListRequest::iterator(lr,lri), ListRequest::iterator(lr,0)
        M4_PARAM(`(formal$1_t)args->f$1', `,', `,')
    );
    return 0;
  };
};

template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
TaskSplitter<TASK> KAAPIWRAPPER_SPLITTER(KAAPI_NUMBER_PARAMS)<TASK M4_PARAM(`,TraitFormalParam$1', `', `')>::dummy;
#endif

// --------------------------------------------------------------------
// KAAPI_NUMBER_PARAMS is the number of possible parameters
template<bool flag, class ATTR, class TASK M4_PARAM(`,typename F$1', `', ` ')>
struct KAAPI_PUSHER(KAAPI_NUMBER_PARAMS) {};

template<class ATTR, class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPI_PUSHER(KAAPI_NUMBER_PARAMS)<false, ATTR, TASK M4_PARAM(`,TraitFormalParam$1', `', ` ')> 
{
  static inline __attribute__((__always_inline__))
  void push( const ATTR& attr, kaapi_thread_t* thread, kaapi_task_body_t body, void* arg ) 
  {
    attr( thread, body, arg );
  }
};

/* specialize for DefaultTaskAttribut */
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPI_PUSHER(KAAPI_NUMBER_PARAMS)<false, DefaultTaskAttribut, TASK M4_PARAM(`,TraitFormalParam$1', `', ` ')> 
{
  static inline __attribute__((__always_inline__))
  void push( const DefaultTaskAttribut& attr, kaapi_thread_t* thread, kaapi_task_body_t body, void* arg ) 
  {
    kaapi_task_push( thread, body, arg );
  }
};


// --------------------------------------------------------------------
// Body generators: 1 -> only user args, 2 -> Thread*, 3-> steal context, 4-> sched info
template<int type, class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPIWRAPPER_CPU_BODY(KAAPI_NUMBER_PARAMS) {};


// Kaapi binder to call task without stack args
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPIWRAPPER_CPU_BODY(KAAPI_NUMBER_PARAMS)<1, TASK M4_PARAM(`, TraitFormalParam$1', `', ` ')> {
  typedef KAAPI_TASKARG(KAAPI_NUMBER_PARAMS) ifelse(KAAPI_NUMBER_PARAMS,0,`',`<M4_PARAM(`TraitFormalParam$1', `', `,')>') TaskArg_t;
  M4_PARAM(`typedef typename TraitFormalParam$1::formal_t formal$1_t;
  ', ` ', `')
  static TaskBodyCPU<TASK> dummy;
  static void body(kaapi_task_t* task, kaapi_thread_t* thread)
  {
    ifelse(KAAPI_NUMBER_PARAMS,0,`',`TaskArg_t* args = (TaskArg_t*)kaapi_task_getargs(task);')
    dummy( M4_PARAM(`(formal$1_t)args->f$1', `', `,'));
  }
};
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
TaskBodyCPU<TASK> KAAPIWRAPPER_CPU_BODY(KAAPI_NUMBER_PARAMS)<1, TASK M4_PARAM(`, TraitFormalParam$1', `', ` ')>::dummy;

// Kaapi binder to call task with thread arg
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPIWRAPPER_CPU_BODY(KAAPI_NUMBER_PARAMS)<2, TASK M4_PARAM(`, TraitFormalParam$1', `', ` ')> {
  typedef KAAPI_TASKARG(KAAPI_NUMBER_PARAMS) ifelse(KAAPI_NUMBER_PARAMS,0,`',`<M4_PARAM(`TraitFormalParam$1', `', `,')>') TaskArg_t;
  M4_PARAM(`typedef typename TraitFormalParam$1::formal_t formal$1_t;
  ', ` ', `')
  static TaskBodyCPU<TASK> dummy;
  static void body(kaapi_task_t* task, kaapi_thread_t* thread)
  {
    ifelse(KAAPI_NUMBER_PARAMS,0,`',`TaskArg_t* args = (TaskArg_t*)kaapi_task_getargs(task);')
    dummy( (Thread*)thread M4_PARAM(`, (formal$1_t)args->f$1', `', `'));
  }
};
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
TaskBodyCPU<TASK> KAAPIWRAPPER_CPU_BODY(KAAPI_NUMBER_PARAMS)<2, TASK M4_PARAM(`, TraitFormalParam$1', `', ` ')>::dummy;

// Kaapi binder to call task with static sched info args
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPIWRAPPER_CPU_BODY(KAAPI_NUMBER_PARAMS)<4, TASK M4_PARAM(`, TraitFormalParam$1', `', ` ')> {
  typedef KAAPI_TASKARG(KAAPI_NUMBER_PARAMS) ifelse(KAAPI_NUMBER_PARAMS,0,`',`<M4_PARAM(`TraitFormalParam$1', `', `,')>') TaskArg_t;
  M4_PARAM(`typedef typename TraitFormalParam$1::formal_t formal$1_t;
  ', ` ', `')
  static TaskBodyCPU<TASK> dummy;
};
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
TaskBodyCPU<TASK> KAAPIWRAPPER_CPU_BODY(KAAPI_NUMBER_PARAMS)<4, TASK M4_PARAM(`, TraitFormalParam$1', `', ` ')>::dummy;


// Binder to GPU entry point
template<int type, class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPIWRAPPER_GPU_BODY(KAAPI_NUMBER_PARAMS) {};


// Kaapi binder to call task without stack args
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPIWRAPPER_GPU_BODY(KAAPI_NUMBER_PARAMS)<1, TASK M4_PARAM(`, TraitFormalParam$1', `', ` ')> {
  typedef KAAPI_TASKARG(KAAPI_NUMBER_PARAMS) ifelse(KAAPI_NUMBER_PARAMS,0,`',`<M4_PARAM(`TraitFormalParam$1', `', `,')>') TaskArg_t;
  M4_PARAM(`typedef typename TraitFormalParam$1::formal_t formal$1_t;
  ', ` ', `')
  static TaskBodyGPU<TASK> dummy;
  static void body(kaapi_task_t* task, kaapi_thread_t* thread)
  {
    ifelse(KAAPI_NUMBER_PARAMS,0,`',`TaskArg_t* args = (TaskArg_t*)kaapi_task_getargs(task);')
    dummy( M4_PARAM(`(formal$1_t)args->f$1', `', `,'));
  }
};
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
TaskBodyGPU<TASK>  KAAPIWRAPPER_GPU_BODY(KAAPI_NUMBER_PARAMS)<1, TASK M4_PARAM(`, TraitFormalParam$1', `', ` ')>::dummy;



// Kaapi binder to call task with stack args
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPIWRAPPER_GPU_BODY(KAAPI_NUMBER_PARAMS)<2, TASK M4_PARAM(`, TraitFormalParam$1', `', ` ')> {
  typedef KAAPI_TASKARG(KAAPI_NUMBER_PARAMS) ifelse(KAAPI_NUMBER_PARAMS,0,`',`<M4_PARAM(`TraitFormalParam$1', `', `,')>') TaskArg_t;
  M4_PARAM(`typedef typename TraitFormalParam$1::formal_t formal$1_t;
  ', ` ', `')
  static TaskBodyGPU<TASK> dummy;
  static void body(kaapi_task_t* task, kaapi_thread_t* thread)
  {
    ifelse(KAAPI_NUMBER_PARAMS,0,`',`TaskArg_t* args = (TaskArg_t*)kaapi_task_getargs(task);')
    dummy( (Thread*)thread M4_PARAM(`, (formal$1_t)args->f$1', `', `'));
  }
};
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
TaskBodyGPU<TASK>  KAAPIWRAPPER_GPU_BODY(KAAPI_NUMBER_PARAMS)<2, TASK M4_PARAM(`, TraitFormalParam$1', `', ` ')>::dummy;

template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` '), bool istatic>
struct KAAPI_FORMATCLOSURE_SD(KAAPI_NUMBER_PARAMS) {};



// specialisation for dynamic format 
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPI_FORMATCLOSURE_SD(KAAPI_NUMBER_PARAMS)<TASK  M4_PARAM(`,TraitFormalParam$1', `', ` '),false> 
{
  M4_PARAM(`typedef typename TraitFormalParam$1::type_inclosure_t inclosure$1_t;
  ', `', `')
  typedef KAAPI_TASKARG(KAAPI_NUMBER_PARAMS) ifelse(KAAPI_NUMBER_PARAMS,0,`',`<M4_PARAM(`TraitFormalParam$1', `', `,')>') TaskArg_t;
  

  static void get_name(const struct kaapi_format_t*, const void*, char* buffer, int size)
  {
    strncpy( buffer, typeid(TASK).name(), size);
  }

  static size_t get_size(const struct kaapi_format_t*, const void* _taskarg)
  {
    return sizeof(TaskArg_t);
  }

  static void task_copy(const struct kaapi_format_t*, void* _taskarg_dest, const void* _taskarg_src)
  {
    memcpy( _taskarg_dest, _taskarg_src, sizeof(TaskArg_t));
  }

  static unsigned int get_count_params(const struct kaapi_format_t*, const void* _taskarg)
  {
    const TaskArg_t* taskarg __attribute__((unused)) = static_cast<const TaskArg_t*>(_taskarg);
    unsigned int count __attribute__((unused)) = 0;
   M4_PARAM(`count += TraitFormalParam$1::get_nparam(&taskarg->f$1);
    ', ` ', `
    ')
    return count;
  }
  
  static kaapi_access_mode_t get_mode_param(const struct kaapi_format_t* fmt, unsigned int ith, const void* _taskarg)
  {
    const TaskArg_t* taskarg __attribute__((unused)) = static_cast<const TaskArg_t*>(_taskarg);
    size_t count __attribute__((unused)) = 0;
   M4_PARAM(`count += TraitFormalParam$1::get_nparam(&taskarg->f$1);
    if (ith < count) return (kaapi_access_mode_t)TYPEMODE2VALUE<typename TraitFormalParam$1::mode_t>::value;
    ', ` ', `
    ')
    return KAAPI_ACCESS_MODE_VOID;
  }
  
  static void* get_data_param(const struct kaapi_format_t*, unsigned int ith, const void* _taskarg)
  {
    void* retval = 0;
    const TaskArg_t* taskarg __attribute__((unused))= static_cast<const TaskArg_t*>(_taskarg);
    size_t countp __attribute__((unused)) = 0, count __attribute__((unused)) = 0;
   M4_PARAM(`count += TraitFormalParam$1::get_nparam(&taskarg->f$1);
    if (ith < count) 
    {
      retval = (void*)TraitFormalParam$1::get_data(&taskarg->f$1, ith-countp);
      return retval;
    }
    countp = count; 
    ', ` ', `
    ')
    return retval;
  }
  
  static kaapi_access_t* get_access_param(const struct kaapi_format_t*, unsigned int ith, const void* _taskarg)
  {
    kaapi_access_t* retval = 0;
    TaskArg_t* taskarg __attribute__((unused))= static_cast<TaskArg_t*>((void*)_taskarg);
    size_t countp __attribute__((unused)) = 0, count __attribute__((unused)) = 0;
   M4_PARAM(`count += TraitFormalParam$1::get_nparam(&taskarg->f$1);
    if (ith < count) 
    {
      retval = TraitFormalParam$1::get_access(&taskarg->f$1, ith-countp);
      return retval;
    }
    countp = count; 
    ', ` ', `
    ')
    return retval;
  }
  
  static void set_access_param(const struct kaapi_format_t*, unsigned int ith, void* _taskarg, const kaapi_access_t* a)
  {
    TaskArg_t* taskarg __attribute__((unused))= static_cast<TaskArg_t*>(_taskarg);
    size_t countp __attribute__((unused)) = 0, count __attribute__((unused)) = 0;
   M4_PARAM(`count += TraitFormalParam$1::get_nparam(&taskarg->f$1);
    if (ith < count) 
    {
      TraitFormalParam$1::set_access(&taskarg->f$1, ith-countp, a);
      return;
    }
    countp = count; 
    ', ` ', `
    ')
  }

  static const kaapi_format_t* get_fmt_param(const struct kaapi_format_t*, unsigned int ith, const void* _taskarg)
  {
    const TaskArg_t* taskarg __attribute__((unused))= static_cast<const TaskArg_t*>(_taskarg);
    size_t count __attribute__((unused)) = 0;
   M4_PARAM(`count += TraitFormalParam$1::get_nparam(&taskarg->f$1);
    if (ith < count) return WrapperFormat<typename TraitFormalParam$1::type_t>::get_c_format();
    ', ` ', `
    ')
    return 0;
  }
  
  static void get_view_param( const struct kaapi_format_t*, unsigned int ith, const void* _taskarg, kaapi_memory_view_t* view )
  {
    const TaskArg_t* taskarg __attribute__((unused))= static_cast<const TaskArg_t*>(_taskarg);
    size_t count __attribute__((unused)) = 0;
   M4_PARAM(`count += TraitFormalParam$1::get_nparam(&taskarg->f$1);
    if (ith < count) { TraitFormalParam$1::get_view_param(&taskarg->f$1, ith, view); return; }
    ', ` ', `
    ')
  }

  static void set_view_param( const struct kaapi_format_t*, unsigned int ith, void* _taskarg, const kaapi_memory_view_t* view )
  {
    TaskArg_t* taskarg __attribute__((unused))= static_cast<TaskArg_t*>(_taskarg);
    size_t count __attribute__((unused)) = 0;
   M4_PARAM(`count += TraitFormalParam$1::get_nparam(&taskarg->f$1);
    if (ith < count) return TraitFormalParam$1::set_view_param(&taskarg->f$1, ith, view);
    ', ` ', `
    ')
  }

  static void reducor(const struct kaapi_format_t*, unsigned int ith, void* _taskarg, const void* value)
  {
    TaskArg_t* taskarg __attribute__((unused))= static_cast<TaskArg_t*>(_taskarg);
    size_t countp __attribute__((unused)) = 0, count __attribute__((unused)) = 0;
   M4_PARAM(`count += TraitFormalParam$1::get_nparam(&taskarg->f$1);
    if (ith < count) { 
      TraitFormalParam$1::reducor_fnc(
        TraitFormalParam$1::get_data(&taskarg->f$1, ith-countp), value
      ); 
      return; 
    }
    countp = count;         
    ', ` ', `
    ')
  }

  static void redinit(const struct kaapi_format_t*, unsigned int ith, const void* _taskarg, void* value)
  {
    const TaskArg_t* taskarg __attribute__((unused))= static_cast<const TaskArg_t*>(_taskarg);
    size_t count __attribute__((unused)) = 0;
   M4_PARAM(`count += TraitFormalParam$1::get_nparam(&taskarg->f$1);
    if (ith < count) { 
      TraitFormalParam$1::redinit_fnc(value); 
    }
    ', ` ', `
    ')
  }

  static kaapi_format_t* registerformat()
  {
    // here we assume no concurrency during startup calls of the library that initialize format objects
    static FormatTask task_fmt(
          typeid(TASK).name(),
          &get_name,
          &get_size,
          &task_copy,
          &get_count_params,
          &get_mode_param,
          &get_data_param,
          &get_access_param,
          &set_access_param,
          &get_fmt_param,
          &get_view_param,
          &set_view_param,
          &reducor,
          &redinit,
          (kaapi_fmt_fnc_get_splitter)0
    );
    kaapi_format_set_dot_name( task_fmt.get_c_format(), TaskDOT<TASK>::name() );
    kaapi_format_set_dot_color( task_fmt.get_c_format(), TaskDOT<TASK>::color() );
    return task_fmt.get_c_format();
  }
};


// Top level class with main entry point
template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPI_FORMATCLOSURE(KAAPI_NUMBER_PARAMS) :
  public KAAPI_FORMATCLOSURE_SD(KAAPI_NUMBER_PARAMS)<
    TASK M4_PARAM(`,TraitFormalParam$1', `', ` '),
    false /* do not use static format for C++ interface */
/*
    KAAPI_TASKARG(KAAPI_NUMBER_PARAMS) ifelse(KAAPI_NUMBER_PARAMS,0,`',`<M4_PARAM(`TraitFormalParam$1', `', `,')>')::is_static
*/
  >
{
  static kaapi_bodies_t default_bodies;

  M4_PARAM(`typedef typename TraitFormalParam$1::type_inclosure_t inclosure$1_t;
  ', `', `')
  typedef KAAPI_TASKARG(KAAPI_NUMBER_PARAMS) ifelse(KAAPI_NUMBER_PARAMS,0,`',`<M4_PARAM(`TraitFormalParam$1', `', `,')>') TaskArg_t;
};


template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
struct KAAPI_INITFORMATCLOSURE(KAAPI_NUMBER_PARAMS) {

  M4_PARAM(`typedef typename TraitFormalParam$1::mode_t mode$1_t;
  ', `', `')
  M4_PARAM(`typedef typename TraitFormalParam$1::type_inclosure_t inclosure$1_t;
  ', `', `')
  M4_PARAM(`typedef typename TraitFormalParam$1::formal_t formal$1_t;
  ', `', `')
  M4_PARAM(`typedef typename TraitFormalParam$1::signature_t signature$1_t;
  ', `', `')

  /* GPU or CPU registration */

  /* trap function for undefined body */
  static kaapi_task_body_t registercpubody( kaapi_format_t* fmt, void (TaskBodyCPU<TASK>::*method)( THIS_TYPE_IS_USED_ONLY_INTERNALLY dummy M4_PARAM(`, signature$1_t', `', `') ) )
  {
    return 0;
  }

  ifelse(KAAPI_NUMBER_PARAMS,0,`',`template< M4_PARAM(`class F$1_t', `', `,')>')
  static kaapi_task_body_t registercpubody( kaapi_format_t* fmt, void (TaskBodyCPU<TASK>::*method)( M4_PARAM(`F$1_t', `', `,') ) )
  {
    typedef void (TASK::*type_default_t)(THIS_TYPE_IS_USED_ONLY_INTERNALLY M4_PARAM(`, signature$1_t', `', `'));
    type_default_t f_default = &TASK::operator();
    if ((type_default_t)method == f_default) return 0;
    kaapi_task_body_t body = (kaapi_task_body_t)KAAPIWRAPPER_CPU_BODY(KAAPI_NUMBER_PARAMS)<1, TASK M4_PARAM(`,TraitFormalParam$1', `', ` ')>::body;
    kaapi_format_taskregister_body(fmt, body, KAAPI_PROC_TYPE_CPU );
    return body;
  }

  ifelse(KAAPI_NUMBER_PARAMS,0,`',`template< M4_PARAM(`class F$1_t', `', `,')>')
  static kaapi_task_body_t registercpubody( kaapi_format_t* fmt, void (TaskBodyCPU<TASK>::*method)( Thread* M4_PARAM(`, F$1_t', `', `') ) )
  {
    typedef void (TASK::*type_default_t)(THIS_TYPE_IS_USED_ONLY_INTERNALLY M4_PARAM(`, signature$1_t', `', `'));
    type_default_t f_default = &TASK::operator();
    if ((type_default_t)method == f_default) return 0;

    kaapi_task_body_t body = (kaapi_task_body_t)KAAPIWRAPPER_CPU_BODY(KAAPI_NUMBER_PARAMS)<2, TASK M4_PARAM(`,TraitFormalParam$1', `', ` ')>::body;
    kaapi_format_taskregister_body(fmt, body, KAAPI_PROC_TYPE_CPU );
    return body;
  }


  /* GPU registration */

  /* trap function for undefined body */
  static kaapi_task_body_t registergpubody( kaapi_format_t* fmt, void (TaskBodyGPU<TASK>::*method)( THIS_TYPE_IS_USED_ONLY_INTERNALLY  M4_PARAM(`, signature$1_t', `', `') ) )
  {
    return 0;
  }

  static kaapi_task_body_t registergpubody( kaapi_format_t* fmt, void (TaskBodyGPU<TASK>::*method)( M4_PARAM(`formal$1_t', `', `,') ) )
  {
    typedef void (TASK::*type_default_t)(THIS_TYPE_IS_USED_ONLY_INTERNALLY M4_PARAM(`, signature$1_t', `', `'));
    type_default_t f_default = &TASK::operator();
    if ((type_default_t)method == f_default) return 0;
    kaapi_task_body_t body = (kaapi_task_body_t)KAAPIWRAPPER_GPU_BODY(KAAPI_NUMBER_PARAMS)<1, TASK M4_PARAM(`,TraitFormalParam$1', `', ` ')>::body;
    kaapi_format_taskregister_body(fmt, body, KAAPI_PROC_TYPE_GPU );
    return body;
  }

  ifelse(KAAPI_NUMBER_PARAMS,0,`',`template<M4_PARAM(`typename SIG$1_t', `', `,') >')
  static kaapi_bodies_t registerbodies( kaapi_format_t* fmt, void (TASK::*method)( Thread* thread M4_PARAM(`, SIG$1_t', `', `') ) )
  {
    kaapi_bodies_t retval = kaapi_bodies_t( registercpubody( fmt, &TaskBodyCPU<TASK>::operator() ), 
                                            registergpubody( fmt, &TaskBodyGPU<TASK>::operator() )
    );
    return retval;
  }
};



template<class TASK M4_PARAM(`,typename TraitFormalParam$1', `', ` ')>
kaapi_bodies_t KAAPI_FORMATCLOSURE(KAAPI_NUMBER_PARAMS)<TASK M4_PARAM(`,TraitFormalParam$1', `', ` ')>::default_bodies =
    kaapi_bodies_t( KAAPI_INITFORMATCLOSURE(KAAPI_NUMBER_PARAMS)<TASK M4_PARAM(`,TraitFormalParam$1', `', ` ')>::registercpubody( KAAPI_FORMATCLOSURE(KAAPI_NUMBER_PARAMS)<TASK M4_PARAM(`,TraitFormalParam$1', `', ` ')>::registerformat(), &TaskBodyCPU<TASK>::operator() ), 
                    KAAPI_INITFORMATCLOSURE(KAAPI_NUMBER_PARAMS)<TASK M4_PARAM(`,TraitFormalParam$1', `', ` ')>::registergpubody( KAAPI_FORMATCLOSURE(KAAPI_NUMBER_PARAMS)<TASK M4_PARAM(`,TraitFormalParam$1', `', ` ')>::registerformat(), &TaskBodyGPU<TASK>::operator() ) );

