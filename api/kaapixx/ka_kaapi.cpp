/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi++"

namespace ka {

// --------------------------------------------------------------------
kaapi_bodies_t::kaapi_bodies_t( kaapi_task_body_t cb, kaapi_task_body_t gb )
 : cpu_body(cb), gpu_body( gb ),
   default_body(cb) /* because we are on a CPU */
{
}

// --------------------------------------------------------------------
#if defined(KAAPI_DEBUG_MEM)
void* __kaapi_malloc( size_t s)
{ return malloc(s); }
void* __kaapi_calloc( size_t s1, size_t s2)
{ return calloc(s1,s2); }
void __kaapi_free( void* p )
{ return free(p); }
#endif

// --------------------------------------------------------------------
DefaultTaskAttribut SetDefaultTaskAttribut;
AttributSchedUnStealableTask SetUnStealable;

SetStickyC SetSticky;
SetStack SetInStack;
SetHeap SetInHeap;
FlagReplyHead ReplyHead;
FlagReplyTail ReplyTail;

// --------------------------------------------------------------------
uint32_t System::local_gid = uint32_t( 0U ); // <-> GlobalId::NO_SITE;


// --------------------------------------------------------------------
int System::saved_argc = 0;
char** System::saved_argv = 0;

// --------------------------------------------------------------------
Community::Community( const Community& com )
{ }


// --------------------------------------------------------------------
void Community::commit()
{
}


// --------------------------------------------------------------------
bool Community::is_leader() const
{ 
  return true;
}


// --------------------------------------------------------------------
void Community::leave() 
{ 
  Sync();
}

static kaapi_atomic_t count_init = {0};

// --------------------------------------------------------------------
Community System::initialize_community( int& argc, char**& argv )
  throw (std::runtime_error)
{
  if (KAAPI_ATOMIC_INCR(&count_init)!=1) return Community(0);

  System::saved_argc = argc;
  System::saved_argv = new char*[argc];
  for (int i=0; i<argc; ++i)
  {
    size_t lenargvi = strlen(argv[i]);
    System::saved_argv[i] = new char[lenargvi+1];
    memcpy(System::saved_argv[i], argv[i], lenargvi );
    System::saved_argv[i][lenargvi] = 0;
  }
  

  /** Init should not have been called by InitKaapiCXX
  */
  kaapi_assert(kaapi_init(1, &argc, &argv) == 0 );

  return Community(0);
}


// --------------------------------------------------------------------
Community System::join_community( int& argc, char**& argv )
  throw (std::runtime_error)
{
  Community thecom = System::initialize_community( argc, argv);
  thecom.commit();
  return thecom;
}


// --------------------------------------------------------------------
Community System::join_community( )
  throw (std::runtime_error)
{
  int argc = 1;
  const char* targv[] = {"kaapi"};
  char** argv = (char**)targv;
  Community thecom = System::initialize_community( argc, argv);
  thecom.commit();
  return thecom;
}


// --------------------------------------------------------------------
void System::terminate()
{
  Sync();
  if (KAAPI_ATOMIC_DECR(&count_init)!=0) 
    return;
  kaapi_finalize();
}


// --------------------------------------------------------------------
int System::getRank()
{
  return 1;
}


// --------------------------------------------------------------------
size_t localityDomainsCount()
{
  return kaapi_locality_domain_getcount();
}

// --------------------------------------------------------------------
void MemorySync()
{
  kaapi_sched_sync( kaapi_self_thread() );
  kaapi_memory_sync();
}


// --------------------------------------------------------------------
SyncGuard::SyncGuard() : _thread( kaapi_self_thread() )
{
}

// --------------------------------------------------------------------
SyncGuard::~SyncGuard()
{
  kaapi_sched_sync( _thread );
}


// --------------------------------------------------------------------
InitKaapiCXX::InitKaapiCXX()
{
  static int iscalled = 0;
  if (iscalled !=0) return;
  iscalled = 1;
    
  WrapperFormat<char>::format.reinit(kaapi_char_format);
  WrapperFormat<short>::format.reinit(kaapi_shrt_format);
  WrapperFormat<int>::format.reinit(kaapi_int_format);
  WrapperFormat<long>::format.reinit(kaapi_long_format);
  WrapperFormat<unsigned char>::format.reinit(kaapi_uchar_format);
  WrapperFormat<unsigned short>::format.reinit(kaapi_ushrt_format);
  WrapperFormat<unsigned int>::format.reinit(kaapi_uint_format);
  WrapperFormat<unsigned long>::format.reinit(kaapi_ulong_format);
  WrapperFormat<float>::format.reinit(kaapi_flt_format);
  WrapperFormat<double>::format.reinit(kaapi_dbl_format);
}


} // namespace ka
