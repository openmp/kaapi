/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_PERFLIB_H
#define _KAAPI_PERFLIB_H 1

#include "kaapi_event.h"
#include "kaapi_perf.h"
#include "kaapi_recorder.h"

#if defined(__cplusplus)
extern "C" {
#endif

/* return a reference to the idp-th performance counter of the k-processor 
   in the current set of counters 
*/
#define KAAPI_PERF_REG(perfproc, idp) ((perfproc)->curr_perf_regs[(idp)])
#define KAAPI_PERFCTR_INCR(perfproc,id, value) KAAPI_PERF_REG(perfproc,id) += value
#define KAAPI_PERFCTR_MAX(perfproc,id, value) if (KAAPI_PERF_REG(perfproc,id) < value) \
                                              KAAPI_PERF_REG(perfproc,id)=value

/* return a reference to the idp-th USER performance counter of the k-processor */
#define KAAPI_PERF_REG_USR(perf_regs, idp) ((perf_regs).state[KAAPI_PERF_USR_STATE][(idp)])

/* return a reference to the idp-th USER performance counter of the k-processor */
#define KAAPI_PERF_REG_SYS(perf_regs, idp) ((perf_regs).state[KAAPI_PERF_SYS_STATE][(idp)])

/* return the sum of the idp-th USER and SYS performance counters */
#define KAAPI_PERF_REG_READALL(perf_regs, idp) \
  (KAAPI_PERF_REG_SYS(perf_regs, idp)+KAAPI_PERF_REG_USR(perf_regs, idp))

#define KAAPI_GET_THREAD_STATE(perfproc)\
  ((perfproc)->curr_perf_regs == (perfproc)->perf_regs.state[KAAPI_PERF_USR_STATE] ? KAAPI_PERF_USR_STATE : KAAPI_PERF_SYS_STATE)

/* ========================================================================= */
/* Tracing facilities for Kaapi                                              */
/* Begin to be external library from Kaapi core library                      */
/* ========================================================================= */

/** Predefined value for KAAPI_DISPLAY_PERF
*/
typedef enum  kaapi_display_perf_value {
  KAAPI_NO_DISPLAY_PERF      = 0,  /* do not display performance counters */
  KAAPI_DISPLAY_PERF_FULL    = 1,  /* display all counters, for each threads */
  KAAPI_DISPLAY_PERF_RESUME  = 2,  /* display only cumulated counters per process */
  KAAPI_DISPLAY_PERF_GPLOT   = 3,  /* display only gplot output */
  KAAPI_DISPLAY_PERF_DISPLAY = 4,
  KAAPI_DISPLAY_PERF_FINAL   = 5   /* display cumulative at the end */
} kaapi_display_perf_value_t;

/* Global variable for the sublibrary tracelib
*/
typedef struct {
  int                  gid;
  int                  cpucount;
  int                  gpucount;
  int                  numaplacecount;
  uint64_t             eventmask;
  kaapi_perf_idset_t   perfctr_idset;
  kaapi_perf_idset_t   taskperfctr_idset;
  int                  proc_event_count;    /* !=0 number of events in perfctr_idset */
  int                  task_event_count;    /* !=0 number of events in taskperfctr_idset */
  int                  papi_event_count;    /* !=0 if PAPI is configured and papi counters defined */
  kaapi_descrformat_t**fmt_list;            /* array of fdescr */
  int                  fmt_listsize;
  kaapi_display_perf_value_t display_perfcounter;
} kaapi_tracelib_param_t;
extern kaapi_tracelib_param_t kaapi_tracelib_param;

/* Initialize perf sublibrary
   Initialize different masks from KAAPI_RECORD_MASK & KAAPI_RECORD_TRACE
   Initialize different masks from KAAPI_TASKPERF_EVENTS & KAAPI_PERF_EVENTS
*/
extern int kaapi_tracelib_init(
  int gid,
  int cpucount,
  int gpucount,
  int numaplacecount,
  kaapi_descrformat_t** fmt_descr,
  int fmt_descrcount
);

/* Finalization */
extern void kaapi_tracelib_fini(void);


typedef struct kaapi_team_perfstat {
  int                         nthreads;        /*  */
  int                         count;           /* when team stat are agglomerate together */
  struct kaapi_team_perfstat* parent;
  struct kaapi_team_perfstat* firstchild;
  struct kaapi_team_perfstat* lastchild;
  struct kaapi_team_perfstat* sibling;
  struct kaapi_team_perfstat* next;            /* creation order */
  char*                       name;
  kaapi_thread_perfctr_t      self_perfctr;    /* sum over all self threads */
  kaapi_thread_perfctr_t      all_perfctr;     /* sum over all threads including those of sub teams */
  kaapi_perfproc_t**          threads_perfctr  /* perfctr[i] for the i-th threads */
      __attribute__((aligned (KAAPI_CACHE_LINE)));
} kaapi_team_perfstat_t;

/*
*/
extern kaapi_team_perfstat_t* kaapi_tracelib_team_init(
  int nproc,
  void* (*routine)(),
  const char* func_name,
  const char* (*filter_func)(const char*),
  kaapi_team_perfstat_t* parent  
);

/*
*/
extern int kaapi_tracelib_team_fini(
  kaapi_team_perfstat_t* team_perfctr
);

/* Initialize trace & performance for the current thread
   A call to kaapi_tracelib_thread_init declares a performance counters set and
   an event stream. If the tracing sublibrary is initialized such that the performance
   counter set or the event set to capture are empty, then the corresponding
   performance counter set or the event stream is set to 0.
   Once turn, the thread is considered to in state state (running or waiting).

*/
extern kaapi_perfproc_t* kaapi_tracelib_thread_init (
    kaapi_evtproc_t**      stream,
    kaapi_team_perfstat_t* team,
    unsigned int           proctype,
    int                    kid,
    int                    numaid,
    int                    isuser
);


/* Finalization for the thread performance counter set or event stream.
   After the call to kaapi_tracelib_thread_fini data pointer by perf or
   eventstream are not usable.
*/
extern void kaapi_tracelib_thread_fini (
    kaapi_perfproc_t* perf,
    kaapi_evtproc_t*  evtstream
 );

/* Set the thread as running user part of the application */
extern void kaapi_tracelib_thread_start (
    kaapi_perfproc_t*     perfkproc,
    kaapi_evtproc_t*      evtkproc
);

/* Tell the thread is stopping running part of the user application */
extern void kaapi_tracelib_thread_stop (
    kaapi_perfproc_t*     perfkproc,
    kaapi_evtproc_t*      evtkproc
);

/* stop current state and switch to isuser state.
   Return the old state.
*/
extern int kaapi_tracelib_thread_switchstate(
    kaapi_perfproc_t*     perfkproc,
    kaapi_evtproc_t*      evtkproc,
    int isuser
);

/*
*/
typedef void* kaapi_task_id_t;
extern kaapi_task_id_t kaapi_tracelib_newtask_id(void);

/* Start executing the task 'task'
*/
extern void kaapi_tracelib_task_begin(
    kaapi_perfproc_t*         perfkproc,
    kaapi_evtproc_t*          evtkproc,
    kaapi_perf_counter_t*     parent_perfctr0,
    kaapi_perf_counter_t*     parent_perfctr,
    kaapi_perf_counter_t*     task_perfctr0,
    void*                     task,
    int                       fmtid,
    int                       isexplicit
);


/* End of execution of the task 'task'. Return computed Tinf
   accumulate in task_perfctr the values of the current counters minus
   the starting values of the counters.
   Ie. compute
     task_perfctr <- task_perfctr + (current - start_perfctr)
   and accumulated value in named_perfctr for statitics.
   task_perfctr may be 0 and was not returned.
*/
extern uint64_t kaapi_tracelib_task_end(
    kaapi_perfproc_t*         perfkproc,
    kaapi_evtproc_t*          evtkproc,
    int                       gtid,
    kaapi_named_perfctr_t*    named_perfctr,
    kaapi_perf_counter_t*     parent_perfctr0,
    kaapi_perf_counter_t*     start_perfctr,
    kaapi_perf_counter_t*     task_perfctr,
    void*                     task,
    uint64_t                  Tinf
);

/* Returns the name of the performance counter id */
extern const char* kaapi_tracelib_perfid_to_name(kaapi_perf_id_t id);

/* create new user counter with function used to collect counter value */
extern kaapi_perf_id_t kaapi_tracelib_create_perfid(
      const char* name,
      kaapi_perf_counter_t (*read)(void*),
      void* ctxt
);

/* Reserve named' performance counters
   Must be serialized
*/
extern kaapi_descrformat_t* kaapi_tracelib_reserve_perfcounter(
);

/* Register a new format descriptor if not already done and returns it.
   New created descriptor entry are serialized.
*/
kaapi_descrformat_t* kaapi_tracelib_register_fmtdescr(
      int implicit,
      void* (*routine)(),
      const char* prefix_name,
      const char* func_name,
      const char* (*filter_func)(const char*)
);

/* utility */
extern size_t kaapi_tracelib_count_perfctr(void);

/* read selected register in counters
   returns result += (read = current_counter) - start
*/
extern void kaapi_perflib_readsub_registers(
    kaapi_perfproc_t*           perfkproc,
    const kaapi_perf_idset_t*   idset,
    kaapi_perf_counter_t*       read,
    const kaapi_perf_counter_t* start,
    kaapi_perf_counter_t*       result
);


/* read and subtract selected register in counters */
extern void kaapi_perflib_read_registers(
    kaapi_perfproc_t*         perfkproc,
    const kaapi_perf_idset_t* idset,
    kaapi_perf_counter_t*     counter
);

/* report stats perf task types */
extern void kaapi_perflib_update_perf_stat(
    int                       kid,
    kaapi_named_perfctr_t*    perf,
    const kaapi_perf_idset_t* idset,
    kaapi_perf_counter_t*     counters
);

/** Push a new event into the eventbuffer of the kprocessor.
    Assume that the event buffer was allocated into the kprocessor.
    Current implementation only work if library is compiled 
    with KAAPI_USE_PERFCOUNTER flag.
*/
static inline kaapi_event_buffer_t* kaapi_event_push0(
    kaapi_evtproc_t*          evtkproc,
    uint64_t                  tclock,
    uint8_t                   eventno
)
{
  kaapi_event_buffer_t* evb = evtkproc->eventbuffer;
  kaapi_event_t* evt = &evb->buffer[evb->pos++];
  tclock -= kaapi_event_startuptime;
  evt->evtno   = eventno;
  evt->kid     = evb->ident;
  evt->date    = tclock;

  if (evb->pos == KAAPI_EVENT_BUFFER_SIZE)
    evb = evtkproc->eventbuffer = kaapi_event_flushbuffer(evb);
  return evb;
}

/** Push a new event into the eventbuffer of the kprocessor.
    Assume that the event buffer was allocated into the kprocessor.
    Current implementation only work if library is compiled 
    with KAAPI_USE_PERFCOUNTER flag.
*/
static inline kaapi_event_buffer_t*  kaapi_event_push1(
    kaapi_evtproc_t*        evtkproc,
    uint64_t                tclock,
    uint8_t                 eventno,
    uint64_t                p0
)
{
  kaapi_event_buffer_t* evb = evtkproc->eventbuffer;
  kaapi_event_t* evt = &evb->buffer[evb->pos++];
  tclock -= kaapi_event_startuptime;
  evt->evtno   = eventno;
  evt->kid     = evb->ident;
  evt->date    = tclock;
  KAAPI_EVENT_DATA(evt,0,u) = p0;

  if (evb->pos == KAAPI_EVENT_BUFFER_SIZE)
    evb = evtkproc->eventbuffer = kaapi_event_flushbuffer(evb);
  return evb;
}

/** Push a new event into the eventbuffer of the kprocessor.
    Assume that the event buffer was allocated into the kprocessor.
    Current implementation only work if library is compiled 
    with KAAPI_USE_PERFCOUNTER flag.
*/
static inline kaapi_event_buffer_t*  kaapi_event_push2(
    kaapi_evtproc_t*        evtkproc,
    uint64_t                tclock,
    uint8_t                 eventno,
    uint64_t                p0,
    uint64_t                p1
)
{
  kaapi_event_buffer_t* evb = evtkproc->eventbuffer;
  kaapi_event_t* evt = &evb->buffer[evb->pos++];
  tclock -= kaapi_event_startuptime;
  evt->evtno   = eventno;
  evt->kid     = evb->ident;
  evt->date    = tclock;
  KAAPI_EVENT_DATA(evt,0,u) = p0;
  KAAPI_EVENT_DATA(evt,1,u) = p1;

  if (evb->pos == KAAPI_EVENT_BUFFER_SIZE)
    evb = evtkproc->eventbuffer = kaapi_event_flushbuffer(evb);
  return evb;
}


/** Push a new event into the eventbuffer of the kprocessor.
    Assume that the event buffer was allocated into the kprocessor.
    Current implementation only work if library is compiled 
    with KAAPI_USE_PERFCOUNTER flag.
*/
static inline kaapi_event_buffer_t*  kaapi_event_push3(
    kaapi_evtproc_t*        evtkproc,
    uint64_t                tclock,
    uint8_t                 eventno,
    uint64_t                p0,
    uint64_t                p1,
    uint64_t                p2
)
{
  kaapi_event_buffer_t* evb = evtkproc->eventbuffer;
  kaapi_event_t* evt = &evb->buffer[evb->pos++];
  tclock -= kaapi_event_startuptime;
  evt->evtno   = eventno;
  evt->kid     = evb->ident;
  evt->date    = tclock;
  KAAPI_EVENT_DATA(evt,0,u) = p0;
  KAAPI_EVENT_DATA(evt,1,u) = p1;
  KAAPI_EVENT_DATA(evt,2,u) = p2;

  if (evb->pos == KAAPI_EVENT_BUFFER_SIZE)
    evb = evtkproc->eventbuffer = kaapi_event_flushbuffer(evb);
  return evb;
}


/** Push a new event into the eventbuffer of the kprocessor.
    Assume that the event buffer was allocated into the kprocessor.
    Current implementation only work if library is compiled
    with KAAPI_USE_PERFCOUNTER flag.
*/
static inline kaapi_event_buffer_t*  kaapi_event_push4(
    kaapi_evtproc_t*        evtkproc,
    uint64_t                tclock,
    uint8_t                 eventno,
    uint64_t                p0,
    uint64_t                p1,
    uint64_t                p2,
    uint64_t                p3
)
{
  kaapi_event_buffer_t* evb = evtkproc->eventbuffer;
  kaapi_event_t* evt = &evb->buffer[evb->pos++];
  tclock -= kaapi_event_startuptime;
  evt->evtno   = eventno;
  evt->kid     = evb->ident;
  evt->date    = tclock;
  KAAPI_EVENT_DATA(evt,0,u) = p0;
  KAAPI_EVENT_DATA(evt,1,u) = p1;
  KAAPI_EVENT_DATA(evt,2,u) = p2;
  KAAPI_EVENT_DATA(evt,3,u) = p3;

  if (evb->pos == KAAPI_EVENT_BUFFER_SIZE)
    evb = evtkproc->eventbuffer = kaapi_event_flushbuffer(evb);
  return evb;
}



/* Write event counter values in idset to the trace file
*/
extern kaapi_event_buffer_t* kaapi_event_push_perfctr(
    kaapi_evtproc_t*            evtkproc,
    uint64_t                    tclock,
    uint8_t                     eventno,
    uint64_t                    d0,
    const kaapi_perf_idset_t*   idset,
    const kaapi_perf_counter_t* perfctr
);


/*
*/

#define KAAPI_IFUSE_TRACE(trclib,inst) \
    if ((trclib)->eventbuffer) { inst; }
#define KAAPI_EVENT_PUSH0(trclib, kthread, eventno ) \
    ( ((trclib) && ((trclib)->eventbuffer) && (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push0((trclib), kaapi_event_date(), eventno ) : 0 )
#define KAAPI_EVENT_PUSH1(trclib, kthread, eventno, p1 ) \
    ( ((trclib) && ((trclib)->eventbuffer) && (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push1((trclib), kaapi_event_date(), eventno, (uint64_t)(p1) ) : 0 )
#define KAAPI_EVENT_PUSH2(trclib, kthread, eventno, p1, p2 ) \
    ( ((trclib) && ((trclib)->eventbuffer) && (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push2((trclib), kaapi_event_date(), eventno, (uint64_t)(p1), (uint64_t)(p2) ) : 0)
#define KAAPI_EVENT_PUSH3(trclib, kthread, eventno, p1, p2, p3 ) \
    ( ((trclib) && ((trclib)->eventbuffer) && (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push3((trclib), kaapi_event_date(), eventno, (uint64_t)(p1), (uint64_t)(p2), (uint64_t)(p3) ): 0)
#define KAAPI_EVENT_PUSH4(trclib, kthread, eventno, p1, p2, p3, p4 ) \
    ( ((trclib) && ((trclib)->eventbuffer) && (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push4((trclib), kaapi_event_date(), eventno, (uint64_t)(p1), (uint64_t)(p2), (uint64_t)(p3), (uint64_t)(p4) ): 0)
#define KAAPI_EVENT_PUSH_PERFCTR(trclib, kthread, pc, idset, perfctr ) \
    ( ((trclib) && ((trclib)->eventbuffer) && (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(KAAPI_EVT_TASK_PERFCOUNTER))) ? \
      kaapi_event_push_perfctr((trclib), kaapi_event_date(), KAAPI_EVT_TASK_PERFCOUNTER, (uintptr_t)pc, idset, perfctr ) : 0)


/* push new event with given date (value return by kaapi_event_date()) 
   the macros returns the date of the event else 0
*/
#define KAAPI_EVENT_PUSH0_AT(trclib, tclock, kthread, eventno ) \
    ( ((trclib) && ((trclib)->eventbuffer) && (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push0((trclib), tclock, eventno ) : 0UL )
#define KAAPI_EVENT_PUSH1_AT(trclib, tclock, kthread, eventno, p1 ) \
    ( ((trclib) && ((trclib)->eventbuffer) && (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push1((trclib), tclock, eventno, (uint64_t)(p1)) : 0UL )
#define KAAPI_EVENT_PUSH2_AT(trclib, tclock, kthread, eventno, p1, p2 ) \
    ( ((trclib) && ((trclib)->eventbuffer) && (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push2((trclib), tclock, eventno, (uint64_t)(p1), (uint64_t)(p2)) : 0UL)
#define KAAPI_EVENT_PUSH3_AT(trclib, tclock, kthread, eventno, p1, p2, p3 ) \
    ( ((trclib) && ((trclib)->eventbuffer) && (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(eventno))) ? \
      kaapi_event_push3((trclib), tclock, eventno, (uint64_t)(p1), (uint64_t)(p2), (uint64_t)(p3)) : 0)

#if defined(__cplusplus)
}
#endif

#endif
