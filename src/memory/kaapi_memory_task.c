/*
 ** xkaapi
 **
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 **
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 **
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 **
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 **
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 **
 */

#include <stdio.h>
#include <stdlib.h>

#include "kaapi_impl.h"

/*
*/
static kaapi_address_space_id_t _kaapi_memory_replica_choose_valid(
      kaapi_metadata_info_t* mdi
)
{
  kaapi_data_replica_t* replica;
  int lid;
  lid = __builtin_ffsll( KAAPI_ATOMIC_READ(&mdi->valid));
  if (lid != 0 )
  {
    --lid;
    kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
    replica = &mdi->replicas[lid];
    return kaapi_pointer2asid(replica->ptr);
  }

  kaapi_abort(__LINE__, __FILE__, "Invalid condition");
  return 0;
}


/*
*/
static inline kaapi_data_replica_t* _kaapi_memory_replica_get(
  kaapi_metadata_info_t* mdi,
  kaapi_address_space_id_t kasid
)
{
  uint8_t lid = kaapi_memory_asid_get_lid(kasid);
  kaapi_assert_debug(mdi != 0);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);

  return &mdi->replicas[lid];
}


/* Call when data have been sent
*/
static void callback_set_valid(
    kaapi_io_status_t status,
    kaapi_io_stream_t* ios,
    void* arg0, void* arg1, void* u
)
{
  kaapi_metadata_info_t* mdi  = (kaapi_metadata_info_t*)arg0;
  kaapi_task_t* task          = (kaapi_task_t*)arg1;
  kaapi_memory_replica_set_valid(mdi, ios->stream->device->asid);
  if (KAAPI_ATOMIC_DECR(&task->wc) ==0)
    /* launch task */
    kaapi_stream_insert_io_end_inst(
      ios->stream,
      KAAPI_IO_STREAM_OUTPUT,
      callback_launch_task,
      task, 0, 0
    );
}


/* TODO: choose the "BEST" asid 
*/
static void kaapi_memory_replica_validate(
  kaapi_metadata_info_t*   mdi,
  kaapi_address_space_id_t asid,
  kaapi_task_t*            task,
  int                      ith
)
{
  kaapi_address_space_id_t src_asid;
  kaapi_data_replica_t *src_replica, *dest_replica;

  kaapi_assert_debug(mdi != 0);
  kaapi_assert_debug(kaapi_memory_asid_get_lid(asid) < KAAPI_MEMORY_MAX_NODES);
  kaapi_assert_debug(!kaapi_memory_replica_is_valid(mdi, asid));

  /* Here: the best asid must be returned, following the topology of the architecture OR
     if the communication device is too overloaded.
     Performance model may also be used to estimate the delay.
  */
  src_asid = _kaapi_memory_replica_choose_valid(mdi);
  kaapi_assert_debug(kaapi_memory_asid_get_lid(src_asid) < KAAPI_MEMORY_MAX_NODES);
  kaapi_assert_debug(kaapi_memory_replica_is_valid(mdi, src_asid));

  src_replica  = _kaapi_memory_replica_get( mdi, src_asid );
  dest_replica = _kaapi_memory_replica_get( mdi, asid );

  kaapi_assert( asid != src_asid );
  kaapi_memory_copy(
      dest_replica->ptr, &dest_replica->view,
      src_replica->ptr, &src_replica->view,
      callback_set_valid, mdi, task, 0 );
}


/* Bind input data to kasid and allocate output data
*/
int kaapi_memory_task_param_bind(
    kaapi_address_space_id_t  asid,
    kaapi_task_t*             task,
    const kaapi_format_t*     fmt,
    const kaapi_access_mode_t mode,
    int                       ith
)
{
  int remote = 0;
  kaapi_memory_view_t view;
  kaapi_access_t* access;
  void* new_data;
  kaapi_metadata_info_t* mdi;
  kaapi_data_replica_t* replica = 0;

  access = kaapi_format_get_access_param(fmt, (unsigned int)ith, kaapi_task_getargs(task));
  kaapi_format_get_view_param(fmt, (unsigned int)ith, kaapi_task_getargs(task), &view);

  /* Returns a replica of the data in asid.
     If data is not already in the DSM, then add it.
  */
  mdi = kaapi_memory_bind(asid, kaapi_local_asid, access->data, &view, &replica);

  /* store in the data access the pointer translated by the original offset */
  new_data = kaapi_memory_view2Npointer(kaapi_pointer2void(replica->ptr), &replica->view);

  if (KAAPI_ACCESS_IS_READ(mode))
  {
    /* */
    if (!kaapi_memory_replica_is_valid(mdi, asid))
    {
      kaapi_memory_replica_validate(mdi, asid, task, ith);
      remote = 1;
    }

  }
  if (KAAPI_ACCESS_IS_WRITE(mode))
    kaapi_memory_replica_set_all_dirty_except(mdi, asid);

  kaapi_assert_debug(access->version == 0);
  access->version = access->data;
  access->data    = new_data;
  kaapi_format_set_access_param(fmt, (unsigned int)ith, kaapi_task_getargs(task), access );

  return remote;
}



/* TODO: cache management
*/
int kaapi_memory_task_param_unbind(
    kaapi_address_space_id_t  asid,
    kaapi_task_t*             task,
    const kaapi_format_t*     fmt,
    const kaapi_access_mode_t mode,
    int                       ith
)
{
  int remote = 0;
  kaapi_access_t* access;
#if 0
  kaapi_memory_view_t view;
  kaapi_metadata_info_t* mdi;
#endif

  if (KAAPI_ACCESS_IS_WRITE(mode))
  {
#if 0
    access = kaapi_format_get_access_param(fmt, (unsigned int)ith, kaapi_task_getargs(task));
    kaapi_format_get_view_param(fmt, (unsigned int)ith, kaapi_task_getargs(task), &view);

    /* here force local update */
    mdi = kaapi_memory_mdi_lookup( access->version, view );
    kaapi_assert_debug( mdi !=0);
    if (kaapi_memory_replica_is_valid(mdi, asid) == false)
    {
      kaapi_memory_replica_validate(stream, mdi, asid, task, ith);
      remote = 1;
    }
    /* WHY ???? */
    access->version = 0;
#endif
  }
  return remote;
}
