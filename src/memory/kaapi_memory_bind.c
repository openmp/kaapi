/*
 ** xkaapi
 ** 
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** fabien.lementec@imag.fr
 ** jvlima@inf.ufsm.br
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include <string.h>

#include "kaapi_impl.h"
#include "kaapi_offload_stream.h"
#include "kaapi_memory_cache.h"

#if defined(_MEMORY_DEBUG)
#include <stdio.h>
#endif


/* One global DSM object for the process
   - size is 2^KAAPI_SIZE_DSM_CTXT = 8192
*/
#define KAAPI_SIZE_DSM_CTXT KAAPI_SIZE_DFGCTXT

typedef struct kaapi_global_map_t {
  kaapi_hashmap_t          map;
  kaapi_hashentries_t*     mapentries[1<<KAAPI_SIZE_DSM_CTXT];
  kaapi_hashentries_bloc_t mapbloc;
  kaapi_lock_t             lock;
} kaapi_global_map_t;

static kaapi_global_map_t* kaapi_memory_map = NULL;

/* local memory asid (for the whole host) */
kaapi_address_space_id_t kaapi_local_asid;

/* map lid -> memory_node* */
static kaapi_memory_node_t* kaapi_memory_nodes[KAAPI_MEMORY_MAX_NODES];


/* lookup for ptr in dsm, if not found register on asid_ptr ptr as an allocated and
   valid pointer.
*/
kaapi_metadata_info_t* kaapi_memory_mdi_lookup(
  void*                      ptr,
  kaapi_address_space_id_t   asid_ptr,
  const kaapi_memory_view_t* view
)
{
  kaapi_metadata_info_t *mdi;
  void* key = kaapi_memory_view2pointer(ptr, view );
  kaapi_hashentries_t* entry;

  kaapi_atomic_lock(&kaapi_memory_map->lock);
  entry = kaapi_hashmap_findinsert( &kaapi_memory_map->map, key );
  kaapi_atomic_unlock(&kaapi_memory_map->lock);
  mdi = KAAPI_HASHENTRIES_GET(entry, kaapi_metadata_info_t*);

  if (mdi ==0)
  {
    mdi = (kaapi_metadata_info_t*)malloc(sizeof(kaapi_metadata_info_t));
    memset( &mdi->replicas[0], 0, sizeof(mdi->replicas));
    KAAPI_ATOMIC_WRITE(&mdi->valid, 0);
    KAAPI_ATOMIC_WRITE(&mdi->allocated, 0);
    /* set the original pointer to be allocated & valid */
    uint8_t lid_ptr = kaapi_memory_asid_get_lid(asid_ptr);
    if (!kaapi_memory_replica_is_allocated(mdi, asid_ptr))
    {
      mdi->replicas[lid_ptr].ptr  = kaapi_make_pointer(ptr, asid_ptr);
      mdi->replicas[lid_ptr].view = *view;
      kaapi_memory_replica_set_allocated( mdi, asid_ptr );
      kaapi_memory_replica_set_valid    ( mdi, asid_ptr );
    }
    KAAPI_HASHENTRIES_SET(entry, mdi, kaapi_metadata_info_t*);
  }
#if defined(KAAPI_DEBUG)
  else
  {
    uint8_t lid_ptr = kaapi_memory_asid_get_lid(asid_ptr);
    kaapi_assert_debug( mdi->replicas[lid_ptr].ptr.ptr == (uintptr_t) ptr);
  }
#endif
  return mdi;
}


/*
*/
static int _kaapi_memory_dsm_init(void)
{
  if (kaapi_memory_map != NULL)
  {
#if defined(KAAPI_DEBUG)
    kaapi_abort(__LINE__, __FILE__, "Invalid operation");
#endif
    return EPERM;
  }

  kaapi_memory_map = (kaapi_global_map_t*)malloc(sizeof(kaapi_global_map_t));
  if(kaapi_memory_map == NULL)
    kaapi_abort(__LINE__, __FILE__, "Out of memory");

  kaapi_atomic_initlock(&kaapi_memory_map->lock);
  kaapi_hashmap_init( &kaapi_memory_map->map,
                      kaapi_memory_map->mapentries,
                      KAAPI_SIZE_DSM_CTXT,
                      &kaapi_memory_map->mapbloc );

  return 0;
}


/*
*/
static void _kaapi_memory_dsm_finalize(void)
{
  if(kaapi_memory_map == NULL)
    kaapi_abort(__LINE__, __FILE__, "Invalid operation");

  kaapi_atomic_lock(&kaapi_memory_map->lock);
  kaapi_hashmap_destroy(&kaapi_memory_map->map);
  kaapi_atomic_unlock(&kaapi_memory_map->lock);
  kaapi_atomic_destroylock(&kaapi_memory_map->lock);
  free(kaapi_memory_map);
  kaapi_memory_map = NULL;
}


/*
*/
kaapi_memory_node_t* kaapi_memory_node_unregister(kaapi_address_space_id_t asid )
{
  uint8_t lid;
  lid = kaapi_memory_asid_get_lid(asid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  kaapi_memory_node_t* node = kaapi_memory_nodes[lid];
  kaapi_memory_nodes[lid] = 0;
  kaapi_memory_cache_destroy(node->cache);
  node->cache = 0;
  return node;
}

/*
*/
int kaapi_memory_node_register(kaapi_address_space_id_t asid, kaapi_memory_node_t* const node)
{
  uint8_t lid;
  kaapi_assert_debug(node != NULL);
  lid = kaapi_memory_asid_get_lid(asid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  if (kaapi_memory_nodes[lid] !=0)
  {
    kaapi_assert_debug(0);
    return EINVAL;
  }
  kaapi_memory_nodes[lid] = node;

#if defined(_MEMORY_DEBUG)
  fprintf(stdout, "%d:%s: (kasid=%x type=%i lid=%u) registered node %p\n",
          kaapi_self_kid(), __FUNCTION__,
          (unsigned int)asid,
          kaapi_memory_asid_get_arch(asid),
          kaapi_memory_asid_get_lid(asid),
          (void*)node);
  fflush(stdout);
#endif

  node->asid  = asid;
  node->cache = kaapi_memory_cache_init(node);

  return 0;
}


/*
*/
kaapi_memory_node_t* kaapi_memory_node_get(
    kaapi_address_space_id_t   asid
)
{
  uint8_t lid;
  lid = kaapi_memory_asid_get_lid(asid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  kaapi_assert_debug(kaapi_memory_nodes[lid] !=0);
  return kaapi_memory_nodes[lid];
}


/* Bind mdi ptr to asid
*/
kaapi_metadata_info_t* kaapi_memory_bind(
     kaapi_address_space_id_t   asid,
     kaapi_address_space_id_t   asid_ptr,  /* where come from the pointer */
     void*                      ptr,
     const kaapi_memory_view_t* view,
     kaapi_data_replica_t**     preplica
)
{
  kaapi_metadata_info_t* mdi = kaapi_memory_mdi_lookup(ptr, asid_ptr, view);
  bool miss = false;

  /* set allocated to the address space asid */
  uint8_t lid = kaapi_memory_asid_get_lid(asid);
  kaapi_assert_debug( lid <KAAPI_MEMORY_MAX_NODES);
  if (kaapi_memory_replica_is_allocated(mdi,asid))
  {
   if (kaapi_memory_asid_get_arch(asid) != KAAPI_PROC_TYPE_HOST)
      kaapi_memory_cache_hit(asid, mdi);
    if (kaapi_memory_replica_view_size(mdi,asid) != kaapi_memory_view_size(view))
      kaapi_memory_free( kaapi_memory_replica_get(mdi,asid)->ptr ) ;
  }
  else /* (!kaapi_memory_replica_is_allocated(mdi, asid)) */
  {
    miss = true;
    int ntries = 0;
    while(ntries < 5)
    {
      /* works with the memory cache to evict (5 times) */
      ntries++;
      mdi->replicas[lid].ptr = kaapi_memory_alloc( asid, kaapi_memory_view_size(view));
//      kaapi_assert(mdi->replicas[lid].ptr.ptr != 0); /* else cache should evict data if possible */
      if(mdi->replicas[lid].ptr.ptr == 0)
      {
        kaapi_assert( kaapi_memory_cache_evict( asid, kaapi_memory_view_size(view) ) != 0 );
      }
      else
        break;
    }
    kaapi_assert(mdi->replicas[lid].ptr.ptr != 0); /* evict did not work */
    mdi->replicas[lid].view = *view;
    kaapi_memory_view_reallocated(&mdi->replicas[lid].view);
    kaapi_memory_replica_set_allocated(mdi, asid);
  }
  if (preplica) *preplica = &mdi->replicas[lid];

  if (miss == true)
    kaapi_memory_cache_miss(asid, mdi);
  return mdi;
  kaapi_assert_debug( !kaapi_memory_replica_is_valid(mdi, asid));
}


/*
*/
void kaapi_memory_unbind( kaapi_address_space_id_t kasid, void* ptr, kaapi_memory_view_t view )
{
#warning "TODO"
}


/*
*/
kaapi_pointer_t kaapi_memory_alloc(kaapi_address_space_id_t asid, size_t size)
{
  uint8_t lid = kaapi_memory_asid_get_lid(asid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  return kaapi_make_pointer( kaapi_memory_nodes[lid]->f_alloc(kaapi_memory_nodes[lid], size), asid );
}

/*
*/
void kaapi_memory_free(kaapi_pointer_t ptr )
{
  uint8_t lid = kaapi_memory_asid_get_lid(ptr.asid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  kaapi_memory_nodes[lid]->f_free(kaapi_memory_nodes[lid], (void*)ptr.ptr);
}


/*
*/
int kaapi_memory_copy(
    kaapi_pointer_t dest, const kaapi_memory_view_t* view_dest,
    kaapi_pointer_t src, const kaapi_memory_view_t* view_src,
    kaapi_io_cbk_fnc_t cbk, void* arg0, void* arg1, void* arg2
)
{
  /* route copy to the first non local asid. Dest is visiting first. */
  uint8_t lid;
  if (dest.asid != kaapi_local_asid)
    lid = kaapi_memory_asid_get_lid(dest.asid);
  else
    lid = kaapi_memory_asid_get_lid(src.asid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);

  return kaapi_memory_nodes[lid]->f_copy(
        kaapi_memory_nodes[lid],
        dest,
        view_dest,
        src,
        view_src,
        cbk, arg0, arg1, arg2
  );
}




/*
*/
static kaapi_lock_t dsm_memsync_lock = KAAPI_LOCK_INITIALIZER;

static void callback_memory_set_validbit(
    kaapi_io_status_t status,
    kaapi_io_stream_t* ios,
    void* arg0, void* arg1, void* arg2
)
{
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_metadata_info_t* mdi     = (kaapi_metadata_info_t*)arg0;
  kaapi_address_space_id_t* asid = (kaapi_address_space_id_t*)arg1;

//  kaapi_assert_debug( ios->stream->device->asid == *asid );
  kaapi_memory_replica_set_valid(mdi, *asid);

  KAAPI_OFFLOAD_TRACE_OUT
}

/*
*/
int kaapi_memory_sync(void)
{
  kaapi_atomic_lock(&dsm_memsync_lock);

  kaapi_memory_node_t* implied_mem_node[KAAPI_MEMORY_MAX_NODES];
  memset(implied_mem_node, 0, sizeof(implied_mem_node));

  for (size_t i=0; i<KAAPI_MEMORY_MAX_NODES; ++i)
  {
    if (kaapi_memory_nodes[i] !=0)
      kaapi_memory_nodes[i]->f_memsync( kaapi_memory_nodes[i], 0 );
  }

  for (size_t i=0; i<kaapi_memory_map->map.size; ++i)
  {
    if (kaapi_memory_map->map.entries[i] ==0) continue;
    kaapi_hashentries_t* entry = kaapi_memory_map->map.entries[i];
    while (entry !=0)
    {
      kaapi_metadata_info_t* mdi = KAAPI_HASHENTRIES_GET(entry, kaapi_metadata_info_t*);
      if (!kaapi_memory_replica_is_valid( mdi, kaapi_local_asid))
      {
        kaapi_address_space_id_t asid;
        uint8_t lid;
        kaapi_data_replica_t *src_replica, *dest_replica;

        asid = kaapi_memory_choose_valid_asid( mdi );
        lid = kaapi_memory_asid_get_lid( asid );
        src_replica  = kaapi_memory_replica_get(mdi, asid);
        dest_replica = kaapi_memory_replica_get(mdi, kaapi_local_asid);

//printf("%s: @:%p not valid on local node, src: %p\n", __FUNCTION__, (void*)dest_replica->ptr.ptr, (void*)src_replica->ptr.ptr ); 

        int err = kaapi_memory_copy(
          dest_replica->ptr,
          &dest_replica->view,
          src_replica->ptr,
          &src_replica->view,
          callback_memory_set_validbit, mdi, &kaapi_local_asid, 0
        );
        kaapi_assert( (err ==0) || (err == EBUSY));
        if ((err ==EBUSY) && (implied_mem_node[lid] ==0))
          implied_mem_node[lid] = kaapi_memory_nodes[lid];
      }
      entry = entry->next;
    }
  }

  for (size_t i=0; i<KAAPI_MEMORY_MAX_NODES; ++i)
  {
    if (implied_mem_node[i] !=0)
      implied_mem_node[i]->f_memsync(implied_mem_node[i], 1);
  }
  kaapi_atomic_unlock(&dsm_memsync_lock);
  return 0;
}



static void* kaapi_host_node_alloc(kaapi_memory_node_t* node, size_t size)
{
  kaapi_assert_debug(node != 0);
  return malloc(size);
}

static void kaapi_host_node_free(kaapi_memory_node_t* node, void *ptr)
{
  kaapi_assert_debug(node != 0);
  free(ptr);
}

/*
*/
static int kaapi_host_node_copy(
    struct kaapi_memory_node_t* node,
    kaapi_pointer_t dest,
    const kaapi_memory_view_t* view_dest,
    kaapi_pointer_t src,
    const kaapi_memory_view_t* view_src,
    kaapi_io_cbk_fnc_t cbk, void* arg0, void* arg1, void* arg2
)
{
  /* if src & dest are local to synchronous blocking call */
  if ( (dest.asid == kaapi_local_asid) && (src.asid  == kaapi_local_asid) )
  {
    int err =0;
    switch (view_src->type)
    {
      case KAAPI_MEMORY_VIEW_1D:
      {
        kaapi_assert(view_dest->type == KAAPI_MEMORY_VIEW_1D);
        if (view_dest->size[0] != view_src->size[0])
        {
          err = EINVAL;
          break;
        }

        memcpy( kaapi_pointer2void(dest),
                kaapi_pointer2void(src),
                view_src->size[0]*view_src->wordsize
        );
        break;
      }
        
      case KAAPI_MEMORY_VIEW_2D:
      {
        const char* laddr;
        char* raddr;
        size_t size;
        
        size = view_src->size[0] * view_src->size[1];
        kaapi_assert(view_dest->type == KAAPI_MEMORY_VIEW_2D);

        if ((view_dest->size[0] != view_src->size[0]) || (view_src->size[1] != view_dest->size[1]))
        {
          err = EINVAL;
          break;
        }
        
        laddr = kaapi_pointer2void(src);
        raddr = (char*)kaapi_pointer2void(dest);
        
        if (kaapi_memory_view_iscontiguous(view_src) &&
            kaapi_memory_view_iscontiguous(view_dest))
          memcpy( raddr, laddr, size * view_src->wordsize);
        else
        {
          size_t i;
          size_t size_row = view_src->size[1]*view_src->wordsize;
          size_t llda = view_src->lda * view_src->wordsize;
          size_t rlda = view_dest->lda * view_src->wordsize;
          
          kaapi_assert_debug( view_dest->size[1] == view_src->size[1] );
          
          for (i=0; i<view_src->size[0]; ++i, laddr += llda, raddr += rlda)
            memcpy(raddr, laddr, size_row);
        }
        break;
      }
        
      default:
        err = EINVAL;
    }
    if (cbk)
    {
      kaapi_io_status_t status = { 0, err };
      cbk( status, 0, arg0, arg1, arg2 );
    }
    return 0;
  }

  /* Copy with non HOST architecture: send request to non local offload node */
  uint8_t lid;
  if (dest.asid != kaapi_local_asid)
    lid = kaapi_memory_asid_get_lid(dest.asid);
  else
    lid = kaapi_memory_asid_get_lid(src.asid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  return (*kaapi_memory_nodes[lid]->f_copy)(
        kaapi_memory_nodes[lid],
        dest,
        view_dest,
        src,
        view_src,
        cbk,
        arg0, arg1, arg2 );
}


__attribute__((unused))static int kaapi_host_node_poll(kaapi_memory_node_t* node)
{
  kaapi_assert_debug(node != 0);
  /* wait for any device event */
  return 0;
}


static void  kaapi_host_node_memsync(struct kaapi_memory_node_t* node, int begend)
{
}


static size_t  kaapi_host_node_get_total_mem(struct kaapi_memory_node_t* node)
{
  return 1024UL*1024UL*1024UL*2; /* 2GBytes */
}


/*
*/
int kaapi_memory_init(__attribute__((unused)) int flag)
{
#if 0
  kaapi_address_space_id_t kasid;
  kaapi_memory_asid_set_null(&kasid);
  kaapi_memory_asid_set_type(&kasid, KAAPI_PROC_TYPE_PHI);
  kaapi_memory_asid_set_gid(&kasid, 1024);
  kaapi_memory_asid_set_lid(&kasid, 128);
  kaapi_memory_asid_set_nuid(&kasid, 32);
  fprintf(stdout,  "%s: kasid=%lx type=%i gid=%u lid=%u nuid=%u\n", __FUNCTION__,
          kasid,
          kaapi_memory_asid_get_arch(kasid), kaapi_memory_asid_get_gid(kasid),
          kaapi_memory_asid_get_lid(kasid), kaapi_memory_asid_get_nuid(kasid) );
  fflush(stdout);
#endif

  /* */
  memset(&kaapi_memory_nodes, 0, sizeof(kaapi_memory_nodes));

  kaapi_local_asid = kaapi_memory_create_asid(
      kaapi_memory_get_current_globalid(),
      0,
      KAAPI_PROC_TYPE_HOST
  );

  static kaapi_memory_node_t host_node;

  host_node.f_alloc = kaapi_host_node_alloc;
  host_node.f_free  = kaapi_host_node_free;
  host_node.f_copy  = kaapi_host_node_copy;
  host_node.f_memsync = kaapi_host_node_memsync;
  host_node.f_get_total_mem = kaapi_host_node_get_total_mem;
  kaapi_memory_node_register( kaapi_local_asid, &host_node );

  return _kaapi_memory_dsm_init();
}

/*
*/
void kaapi_memory_finalize(void)
{
  _kaapi_memory_dsm_finalize();
}

int _kaapi_memory_sync_addr(kaapi_address_space_id_t asid, kaapi_metadata_info_t* mdi)
{
  kaapi_memory_node_t* node;
  kaapi_address_space_id_t valid_asid;
  kaapi_data_replica_t *src_replica, *dest_replica;
  
  valid_asid = kaapi_memory_choose_valid_asid( mdi );
  /* check which node we need to send sync commands. */
  if (asid != kaapi_local_asid)
    node = kaapi_memory_node_get(asid);
  else
    node = kaapi_memory_node_get(valid_asid);

  src_replica  = kaapi_memory_replica_get(mdi, valid_asid);
  dest_replica = kaapi_memory_replica_get(mdi, asid);
//printf("%s: @:%p not valid on local node, src: %p\n", __FUNCTION__, (void*)dest_replica->ptr.ptr, (void*)src_replica->ptr.ptr );
  int err = kaapi_memory_copy(
    dest_replica->ptr,
    &dest_replica->view,
    src_replica->ptr,
    &src_replica->view,
    callback_memory_set_validbit, mdi, &kaapi_local_asid, 0
  );
  kaapi_assert( (err ==0) || (err == EBUSY));
#if defined(KAAPI_USE_OFFLOAD)
  if (err == EBUSY){
#warning "FIX use of synchronous tranfers"
    kaapi_device_t* device = (kaapi_device_t*)node;
    kaapi_offload_stream_process_instruction( device->stream, KAAPI_IO_STREAM_INPUT );
    kaapi_offload_wait_stream( device->stream, KAAPI_IO_STREAM_INPUT);
  }
#else
  kaapi_assert(err != EBUSY);
#endif
  return 0;
}

int kaapi_memory_sync_addr( kaapi_address_space_id_t asid, void* ptr, kaapi_memory_view_t view )
{
  kaapi_metadata_info_t* mdi;
  
  kaapi_assert_debug(ptr != 0);
  mdi = kaapi_memory_mdi_lookup(ptr, asid, &view);
  kaapi_assert_debug(mdi != 0);
  _kaapi_memory_sync_addr(asid, mdi);
  return 0;
}
