/*
 ** xkaapi
 ** 
 ** Copyright 2-110 INRIA.
 **
 ** Contributors :
 **
 ** fabien.lementec@imag.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include "kaapi_impl.h"

#include <unistd.h>
#include <sys/mman.h>
#include <stdio.h>


#define KAAPI_MAX_PROCESSOR 256

/*
*/
kaapi_numaid_t kaapi_numa_getpage_id(const void* addr)
{
#if defined(KAAPI_USE_NUMA)
  int mode = -1;
  const int err = get_mempolicy(&mode, (void*)0, 0, (void* )addr, MPOL_F_NODE | MPOL_F_ADDR);

  if (err)
    return (kaapi_numaid_t)-1;

  /* convert to internal kaapi identifier */
  return mode;
#else
  return 0;
#endif
}


#if 0
/*
*/
kaapi_numaset_t kaapi_numa_getpage_ids(const void* addr, size_t size)
{
  kaapi_numaid_t retval = kaapi_numa_getpage_id(addr);
  if (retval !=(kaapi_numaid_t)-1)
    return 1UL << retval;
  return 0UL;
}
#endif


/*
*/
int kaapi_numa_bind(kaapi_numaid_t node, const void* addr, size_t size)
{
#if defined(KAAPI_USE_NUMA)
  const int mode = MPOL_BIND;
  const unsigned int flags = MPOL_MF_STRICT | MPOL_MF_MOVE;
  const unsigned long maxnode = KAAPI_MAX_PROCESSOR;
  
  if ((size & 0xfff) !=0) /* should be divisible by 4096 */
  {
    errno = EINVAL;
    return -1;
  }
  if (((uintptr_t)addr & 0xfff) !=0) /* should aligned on page boundary */
  {
    errno = EFAULT;
    return -1;
  }
  
  unsigned long nodemask
    [(KAAPI_MAX_PROCESSOR + (8 * sizeof(unsigned long) -1))/ (8 * sizeof(unsigned long))];

  memset(nodemask, 0, sizeof(nodemask));

  nodemask[node / (8 * sizeof(unsigned long))] |= 1UL << (node % (8 * sizeof(unsigned long)));

  if (mbind((void*)addr, size, mode, nodemask, maxnode, flags))
    return -1;
#endif
  return 0;
}


/*
*/
int kaapi_numa_bind_bloc1dcyclic(kaapi_numaset_t numaset, const void* addr, size_t size, size_t blocsize)
{
#if defined(KAAPI_USE_NUMA)
  const int mode = MPOL_BIND;
  const unsigned int flags = MPOL_MF_STRICT | MPOL_MF_MOVE;
  const char* base = (const char*)addr;

  size_t pagesize = getpagesize();
  const unsigned long maxnode = KAAPI_MAX_PROCESSOR;
  unsigned long nodemask
      [(KAAPI_MAX_PROCESSOR + (8 * sizeof(unsigned long) -1))/ (8 * sizeof(unsigned long))];

  if ((numaset ==0) || ((blocsize & (pagesize-1)) !=0) /* should be divisible by getpagesize() */)
  {
    errno = EINVAL;
    return -1;
  }
  if (((uintptr_t)addr & (pagesize-1)) !=0) /* should aligned on page boundary */
  {
    errno = EFAULT;
    return -1;
  }
  
  kaapi_numaset_t set = numaset;
  kaapi_numaid_t node = 0;
  while (size >0)
  {
    int numaid = __builtin_ffsll( set );
    if (numaid ==0)
    {
      set = numaset;
      continue;
    }
    node = numaid-1;
    set &= ~(1U << node);

    memset(nodemask, 0, sizeof(nodemask));
    nodemask[node / (8 * sizeof(unsigned long))] |= 1UL << (node % (8 * sizeof(unsigned long)));

    if (mbind((void*)base, blocsize, mode, nodemask, maxnode, flags))
      return -1;

    base += blocsize;
    if (size > blocsize)
      size -= blocsize;
    else
      size = 0;
  }
  /* mbind reminder */
  if (mbind((void*)base, pagesize, mode, nodemask, maxnode, flags))
  {
      printf("*** Error in binding past the last page\n");
      return -1;
  }
#endif

  return 0;
}


/*
*/
int kaapi_numa_bind_bloc2dcyclic(
  kaapi_numaset_t numaset, size_t nsetx, size_t nsety,              /* grid of numa node */
  const void* addr, size_t n, size_t m, size_t ld, size_t wordsize, /* matrix */
  size_t bsx,                                                       /* blocsize / x in word */
  size_t bsy                                                        /* blocsize / y in word */
)
{
  size_t pagesize = getpagesize();
  if ((numaset ==0) || (bsx == 0) || (bsy ==0) || (n*m*wordsize ==0))
  {
    errno = EINVAL;
    return -1;
  }
  if (((uintptr_t)addr & (pagesize-1)) !=0) /* should aligned on page boundary */
  {
    errno = EFAULT;
    return -1;
  }
  if ((m*wordsize < pagesize) || (((m*wordsize) & (pagesize-1)) !=0))
  {
    errno = EFAULT;
    return -1;
  }
  if (nsetx*nsety ==0)
  {
    errno = EINVAL;
    return -1;
  }

#if defined(KAAPI_USE_NUMA)
  kaapi_numaid_t set2numanode[nsetx*nsety];
  memset(set2numanode, 0, sizeof(set2numanode));
  kaapi_numaset_t set = numaset;
  for (size_t cnt = 0; cnt < nsetx*nsety; ++cnt)
  {
    if (set ==0) set = numaset; 
    int node = __builtin_ffsll( set )-1;
    set &= ~(1U << node);
    set2numanode[cnt] = node;
//printf("Setnumanode[%i]=%i\n", cnt, node);
  }

  const int mode = MPOL_BIND;
  const unsigned int flags = MPOL_MF_STRICT | MPOL_MF_MOVE;
  const unsigned long maxnode = KAAPI_MAX_PROCESSOR;
  unsigned long nodemask
      [(KAAPI_MAX_PROCESSOR + (8 * sizeof(unsigned long) -1))/ (8 * sizeof(unsigned long))];

  size_t blocsizex = bsx;
  size_t blocsizey = bsy*wordsize;
  blocsizey = (blocsizey + pagesize-1) & ~(pagesize-1);

  if (blocsizey <pagesize)
    blocsizey = pagesize;

  blocsizey = blocsizey / wordsize;
  size_t seti = 0;
  size_t setj = 0;

  for (size_t i=0; i<n; ++i)
  {
    seti = (i / blocsizex) % nsetx;
    //seti = (i % (n/blocsizex)) % nsetx;

    for (size_t j=0; j<m; j += blocsizey)
    {
      //setj = (j% (m/blocsizey)) % nsety;
      setj = (j/blocsizey) % nsety;
      void* base = (void*)(((uintptr_t)addr)+wordsize*(i*ld + j));
      kaapi_numaid_t node = set2numanode[seti*nsety+setj];
    //  kaapi_numaid_t node = set2numanode[seti*nsety+setj];

      memset(nodemask, 0, sizeof(nodemask));
      nodemask[node / (8 * sizeof(unsigned long))] |= 1UL << (node % (8 * sizeof(unsigned long)));

      if (mbind(base, blocsizey*wordsize, mode, nodemask, maxnode, flags))
        return -1;
//printf("Bind, node:%i, @:%p\n", node, base);
    }
  }
#endif

  return 0;
}



/*
*/
void* kaapi_numa_alloc(size_t size)
{
  void* retval;
  size_t pagesize, size_mmap;
  
  pagesize = getpagesize();
  size_mmap = (( size + pagesize -1 ) / pagesize) * pagesize + pagesize;
  retval = mmap( 0, size_mmap, PROT_READ|PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, (off_t)0 );
  if (retval == (void*)-1)
    return 0;
  return retval;
}


/*
*/
int kaapi_numa_free(void* ptr, size_t size)
{
  size_t pagesize, size_mmap;
  pagesize = getpagesize();

  if (((uintptr_t)ptr & (pagesize-1)) !=0) /* should aligned on page boundary */
  {
    errno = EFAULT;
    return -1;
  }
  size_mmap = (( size + pagesize -1 ) / pagesize) * pagesize + pagesize;
  munmap( ptr, size_mmap );
  return 0;
}


/*
*/
void* kaapi_numa_allocate_bloc1dcyclic(kaapi_numaset_t ldset, size_t size, size_t blocsize)
{
  int err;
  void* retval;

  retval = kaapi_numa_alloc(size);
  if (retval == 0)
    return 0;

  err = kaapi_numa_bind_bloc1dcyclic( ldset, retval, size, blocsize );
  if (err !=0)
  {
    kaapi_numa_free( retval, size );
    return 0;
  }
  return retval;
}


/*
*/
void* kaapi_numa_allocate_bloc2dcyclic(
  kaapi_numaset_t numaset, size_t nsetx, size_t nsety, /* grid of numa node */
  size_t n, size_t m, size_t wordsize,                 /* matrix */
  size_t bsx,                                          /* blocsize / x */
  size_t bsy                                           /* blocsize / y */
)
{
  int err;
  void* retval;

  retval = kaapi_numa_alloc(n*m*wordsize);
  if (retval == 0)
    return 0;

  err = kaapi_numa_bind_bloc2dcyclic( numaset, nsetx, nsety, retval, n, m, m, wordsize, bsx, bsy );
  if (err !=0)
  {
    kaapi_numa_free( retval, n*m );
    return 0;
  }
  return retval;
}



/* Initialize
*/
int kaapi_numa_initialize(void)
{
#if defined(KAAPI_USE_NUMA)
  //numa_set_bind_policy(1);
  //numa_set_strict(1);
#endif
  
  return 0;
}


/*
*/
int kaapi_numa_finalize(void)
{
  return 0;
}
