/*
 ** xkaapi
 **
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** jvlima@inf.ufsm.br
 **
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 **
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 **
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 **
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 **
 */

#include "kaapi_impl.h"
#include "kaapi_memory_cache.h"
#include "uthash.h"

#include <stdbool.h>

struct kaapi_device_t;
//#define _CACHE_DEBUG  1

typedef struct kaapi_mem_list_t {
  kaapi_metadata_info_t* mdi;
  struct kaapi_mem_list_t* prev;
  struct kaapi_mem_list_t* next;
  UT_hash_handle hh;
} kaapi_mem_list_t;

//typedef struct {
//  kaapi_mem_list_t* item;
//  void* addr;
//} kaapi_cache_entry_t;

typedef struct kaapi_memory_cache_t {
  size_t total;
  size_t used;
  struct kaapi_device_t* const device;
  struct {
    kaapi_mem_list_t* head;
    kaapi_mem_list_t* tail;
  } queue;
  kaapi_mem_list_t* hash;
} kaapi_memory_cache_t;

static inline void kaapi_cache_list_init( kaapi_memory_cache_t* cache )
{
  cache->queue.tail = cache->queue.head = 0;
}

static inline bool kaapi_cache_list_empty( kaapi_memory_cache_t* cache )
{
  return (cache->queue.head == 0);
}

static inline void kaapi_cache_list_push_head( kaapi_memory_cache_t* cache, kaapi_mem_list_t* item)
{
  kaapi_assert_debug( item->next == 0 );
  kaapi_assert_debug( item->prev == 0 );
  item->next = cache->queue.head;
  item->prev = 0;
  if (cache->queue.head == 0)
    cache->queue.tail = item;
  else
    cache->queue.head->prev = item;
  cache->queue.head = item;
}

static inline void kaapi_cache_list_push_tail( kaapi_memory_cache_t* cache, kaapi_mem_list_t* item)
{
  kaapi_assert_debug( item->next == 0 );
  kaapi_assert_debug( item->prev == 0 );
  item->next = 0;
  item->prev = cache->queue.tail;
  if (cache->queue.tail == 0)
    cache->queue.head = item;
  else
    cache->queue.tail->next = item;
  cache->queue.tail = item;
}

/* pop from head */
static inline kaapi_mem_list_t* kaapi_cache_list_pop_head( kaapi_memory_cache_t* cache )
{
  kaapi_mem_list_t* item = cache->queue.head;
  if (item ==0)
    return 0;
  cache->queue.head = item->next;
  if (item->next ==0)
    cache->queue.tail = 0;
  else
    cache->queue.head->prev = 0;
  item->next = 0;
  kaapi_assert_debug( item->prev == 0 );
  return item;
}

/* pop from tail */
static inline kaapi_mem_list_t* kaapi_cache_list_pop_tail( kaapi_memory_cache_t* cache )
{
  kaapi_mem_list_t* item = cache->queue.tail;
  if (item ==0)
    return 0;
  cache->queue.tail = item->prev;
  if (item->prev ==0)
    cache->queue.head = 0;
  else
    cache->queue.tail->next = 0;
  item->prev = 0;
  kaapi_assert_debug( item->next == 0 );
  return item;
}

/*
 */
static inline void kaapi_cache_list_remove( kaapi_memory_cache_t* cache, kaapi_mem_list_t* item)
{
  if (item->prev !=0)
    item->prev->next = item->next;
  if (item->next!=0)
    item->next->prev = item->prev;
  if (cache->queue.tail == item) cache->queue.tail = item->prev;
  if (cache->queue.head == item) cache->queue.head = item->next;
  item->next = 0;
  item->prev = 0;
}

/*
*/
static inline void kaapi_memory_cache_destroy_hash(kaapi_memory_cache_t* cache)
{
  kaapi_mem_list_t *item, *tmp;
  HASH_ITER(hh, cache->hash, item, tmp){
    HASH_DEL(cache->hash, item);
    free(item); /* frees also list item */
  }
  cache->hash = 0;
}

#if 0
/* NOT USED */
static inline void kaapi_memory_cache_destroy_queue(kaapi_memory_cache_t* cache)
{
  kaapi_mem_list_t* item;
  while(kaapi_cache_list_empty(cache) == false)
  {
    item= kaapi_cache_list_pop_head(cache);
    free(item);
  }
  kaapi_cache_list_init(cache);
}
#endif


/*
*/
static inline kaapi_mem_list_t* kaapi_memory_cache_find(
    kaapi_memory_cache_t* cache,
    kaapi_metadata_info_t* mdi
)
{
  kaapi_mem_list_t* item;
  HASH_FIND_PTR(cache->hash, &mdi, item);
  return item;
}

static inline void kaapi_memory_cache_remove(
    kaapi_memory_cache_t* cache,
    kaapi_mem_list_t* item
)
{
  kaapi_cache_list_remove(cache, item);
  HASH_DEL(cache->hash, item);
}

/*
*/
static inline void kaapi_memory_cache_usage_add(
    kaapi_memory_node_t* const node,
    kaapi_memory_cache_t* cache,
    kaapi_metadata_info_t* mdi
)
{
  uint8_t lid = kaapi_memory_asid_get_lid(node->asid);
  kaapi_data_replica_t* rep = &mdi->replicas[lid];
  cache->used += kaapi_memory_view_size(&rep->view);
}

static inline kaapi_mem_list_t* kaapi_memory_cache_find_valid_on_host(
    kaapi_memory_cache_t* cache,
    kaapi_address_space_id_t asid
)
{
  kaapi_mem_list_t* item = cache->queue.tail;
  kaapi_assert_debug(item != 0);
  kaapi_assert_debug(item->mdi != 0);
  /* first tries to remove valid copies on host to avoid transfer */
  if(kaapi_memory_replica_is_valid(item->mdi, asid) == false)
  {
    kaapi_memory_cache_remove(cache, item);
    return item;
  }
  /* if no valid copy is on host, transfer to host one */
  if(kaapi_memory_replica_is_valid(item->mdi, kaapi_local_asid) == false)
    _kaapi_memory_sync_addr( kaapi_local_asid, item->mdi );
  kaapi_assert_debug((kaapi_memory_replica_is_valid( item->mdi, kaapi_local_asid) == true));
  kaapi_memory_cache_remove(cache, item);
  return item;
}



/*
*/
kaapi_memory_cache_t* kaapi_memory_cache_init(kaapi_memory_node_t* const node)
{
  kaapi_memory_cache_t* cache;

  cache = (kaapi_memory_cache_t*)malloc(sizeof(kaapi_memory_cache_t));
  kaapi_assert(cache != 0);

  cache->hash = NULL;
  kaapi_cache_list_init(cache);

  cache->total = node->f_get_total_mem(node);
  cache->used = 0;

#if defined(_CACHE_DEBUG)
  fprintf(stdout, "%s: cache %p device %d (total %zu)\n", __FUNCTION__,
          (void*)cache, kaapi_memory_asid_get_lid(node->asid), cache->total);
  fflush(stdout);
#endif
  return  (struct kaapi_memory_cache_t*)cache;
}


/*
*/
void kaapi_memory_cache_destroy(struct kaapi_memory_cache_t* const cache)
{
  kaapi_assert_debug(cache != 0);
#if defined(_CACHE_DEBUG)
  fprintf(stdout, "%s: cache %p\n", __FUNCTION__, (void*)cache);
  fflush(stdout);
#endif
  kaapi_memory_cache_destroy_hash(cache);
  free(cache);
}


/*
*/
void kaapi_memory_cache_hit(
    kaapi_address_space_id_t asid,
    kaapi_metadata_info_t*   mdi
)
{
  kaapi_memory_cache_t* cache;
  kaapi_mem_list_t* item;

  kaapi_memory_node_t* node = kaapi_memory_node_get( asid );
  kaapi_assert_debug(node != 0);
  kaapi_assert_debug(node->cache != 0);
  kaapi_assert_debug(mdi != 0);
#if defined(_CACHE_DEBUG)
  fprintf(stdout, "%s: cache hit %p device %d\n", __FUNCTION__, (void*)mdi, kaapi_memory_asid_get_lid(node->asid));
  fflush(stdout);
#endif
  cache = (kaapi_memory_cache_t*)node->cache;
  item = kaapi_memory_cache_find(cache, mdi);
  kaapi_assert_debug(item != 0);

  /* optimize hit when item is the queue head, avoiding remove/insert */
  if(item != cache->queue.head)
  {
    /* LRU policy: replica goes to queue's head */
    kaapi_cache_list_remove(cache, item);
    kaapi_cache_list_push_head(cache, item);
  }
}


/*
*/
void kaapi_memory_cache_miss(
    kaapi_address_space_id_t asid,
    kaapi_metadata_info_t* mdi
)
{
  kaapi_memory_cache_t* cache;
  kaapi_mem_list_t* item;
  kaapi_memory_node_t* node = kaapi_memory_node_get( asid );
  kaapi_assert_debug(node != 0);
  kaapi_assert_debug(node->cache != 0);
  kaapi_assert_debug(mdi != 0);

  cache = (kaapi_memory_cache_t*)node->cache;
#if defined(_CACHE_DEBUG)
  fprintf(stdout, "%s: cache miss %p data: %p->%p device %d %zu/%zu (%.2f%%)\n", __FUNCTION__,
          (void*)mdi, (void*)mdi->replicas[kaapi_memory_asid_get_lid(kaapi_local_asid)].ptr.ptr,
          (void*)mdi->replicas[kaapi_memory_asid_get_lid(node->asid)].ptr.ptr,
          kaapi_memory_asid_get_lid(node->asid),
          cache->used, cache->total,
          ((cache->used*100.0f)/cache->total)
          );
  fflush(stdout);
#endif
  kaapi_assert_debug(kaapi_memory_cache_find(cache, mdi) == 0);
  item = (kaapi_mem_list_t*)malloc(sizeof(kaapi_mem_list_t));
  kaapi_assert_debug(item != 0);
  item->prev= item->next= 0;
  item->mdi = mdi;
  /* LRU policy */
  kaapi_cache_list_push_head(cache, item);
  HASH_ADD_PTR(cache->hash, mdi, item);
  /* add  memory usage */
  kaapi_memory_cache_usage_add(node, cache, mdi);
}

size_t kaapi_memory_cache_evict(
    kaapi_address_space_id_t asid,
    size_t size
)
{
  size_t evicted = 0;
  kaapi_memory_cache_t* cache;
  kaapi_mem_list_t* item;
  kaapi_data_replica_t *replica = 0;
  kaapi_memory_node_t* node = kaapi_memory_node_get( asid );
  kaapi_assert_debug(node != 0);
  kaapi_assert_debug(node->cache != 0);
  
  cache = (kaapi_memory_cache_t*)node->cache;
  while( evicted < size )
  {
    if(kaapi_cache_list_empty(cache))
      return 0;
    
    /* LRU: remove from the tail */
    item = kaapi_memory_cache_find_valid_on_host(cache, asid);
    kaapi_assert_debug(item != 0);
    replica = kaapi_memory_replica_get(item->mdi, asid);
    /* update cache state */
    evicted += kaapi_memory_view_size(&replica->view);
    cache->used -= kaapi_memory_view_size(&replica->view);
#if defined(_CACHE_DEBUG)
    fprintf(stdout, "%s: evict asid %d mdi %p data %p  (%zu/%zu) cache %.2f%%\n",
            __FUNCTION__,
            kaapi_memory_asid_get_lid(asid), (void*)item->mdi, (void*)replica->ptr.ptr,
            evicted, size, ((cache->used*100.0f)/cache->total)
    );
#endif
    kaapi_memory_replica_unset_valid(item->mdi, asid);
    kaapi_memory_replica_unset_allocated(item->mdi, asid);
    kaapi_memory_free( replica->ptr ) ;
    free(item);
  }
  
  return evicted;
}

