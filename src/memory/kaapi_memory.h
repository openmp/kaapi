/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** vincent.danjean@imag.fr
** joao.lima@inf.ufsm.br
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_MEMORY_H
#define _KAAPI_MEMORY_H 1

#include <stdbool.h>
    
//#define _MEMORY_DEBUG   1

/* Representation of address space id:
   - 256 numa node id
   - 256 local identifiers
   - (0x00FFFFFFFFFF0000ULL>>16) global identifiers
   - 16 bit for coding architecture
   numa node id is considered as sub domain of the address space id.
   For instance on one multicore machine there is one address space id
   with several numa node (up to 256).
*/
#define KAAPI_ASID_MASK_NUID  0x00000000000000FFULL /* shift = 0 */
#define KAAPI_ASID_MASK_LID   0x000000000000FF00ULL /* shift = 8 */
#define KAAPI_ASID_MASK_GID   0x00FFFFFFFFFF0000ULL /* shift = 16 */
#define KAAPI_ASID_MASK_ARCH  0xFF00000000000000ULL /* shift = 56 */

/* ============================ Memory data ================================= */
    
#define KAAPI_MEMORY_MAX_NODES      12
    
typedef struct kaapi_data_replica_t {
    kaapi_pointer_t         ptr;
    kaapi_memory_view_t     view;
} kaapi_data_replica_t;
    
/* Metadata information for each address */
typedef struct kaapi_metadata_info_t {
    kaapi_data_replica_t    replicas[KAAPI_MEMORY_MAX_NODES];
    kaapi_atomic64_t        allocated;
    kaapi_atomic64_t        valid;
} kaapi_metadata_info_t;
    
extern int kaapi_memory_init(int flag);

extern void kaapi_memory_finalize(void);


/* ============================ Address space ================================= */
extern kaapi_address_space_id_t kaapi_local_asid;

extern kaapi_address_space_id_t kaapi_memory_create_asid(
    kaapi_globalid_t gid,
    uint8_t nuid,
    uint8_t arch
);

extern kaapi_address_space_id_t kaapi_memory_asid_init(
    kaapi_globalid_t gid,
    uint8_t nuid,
    uint8_t lid,
    uint8_t arch
);
    
static inline kaapi_globalid_t kaapi_memory_get_current_globalid(void)
{ return 0;}


/* ============================ Memory type ================================= */
/** assume that now the view points to a new allocate view
*/
static inline void kaapi_memory_view_reallocated( kaapi_memory_view_t* kmv )
{
  switch (kmv->type) 
  {
    case KAAPI_MEMORY_VIEW_1D: return;
    case KAAPI_MEMORY_VIEW_2D: kmv->lda = kmv->size[1]; return;
    default:
      kaapi_assert(0);
      break;
  }
}


/** Return the gid of the address space
    \param kasid [IN] an address space identifier
    \return the gid encoded into the address space identifier
*/
static inline kaapi_globalid_t kaapi_memory_asid_get_gid( kaapi_address_space_id_t kasid )
{ return (kaapi_globalid_t)((kasid & KAAPI_ASID_MASK_GID)>> 16ULL); }

/** Return the type of the address space location.
    \param kasid [IN] an address space identifier
    \return the type encoded into the address space identifier
*/
static inline uint8_t kaapi_memory_asid_get_arch( kaapi_address_space_id_t kasid )
{ return (uint8_t)((kasid & KAAPI_ASID_MASK_ARCH) >> 56UL); }

static inline uint8_t kaapi_memory_asid_get_lid( kaapi_address_space_id_t kasid )
{ return (uint8_t)( (kasid & KAAPI_ASID_MASK_LID) >> 8UL); }

static inline uint8_t kaapi_memory_asid_get_nuid( kaapi_address_space_id_t kasid )
{ return (uint8_t)(kasid & KAAPI_ASID_MASK_NUID); }

        
/* return the address space identifier
*/
static inline kaapi_address_space_id_t kaapi_pointer2asid(kaapi_pointer_t p)
{ return p.asid; }

/* return the location (gid) where the pointer points.
*/
static inline kaapi_globalid_t kaapi_pointer2gid(kaapi_pointer_t p)
{ return kaapi_memory_asid_get_gid(p.asid); }
    
    
static inline void kaapi_memory_asid_set_null(kaapi_address_space_id_t* kasid)
{
    kaapi_assert_debug(kasid != 0);
    *kasid = 0UL;
}

extern kaapi_metadata_info_t* kaapi_memory_mdi_lookup(
   void*                      ptr,
   kaapi_address_space_id_t   asid_ptr,
   const kaapi_memory_view_t* view
);

/* ====================== Memory task param bind / unbind ========================= */
struct kaapi_offload_stream;
extern int kaapi_memory_task_param_bind(
    kaapi_address_space_id_t     asid,
    kaapi_task_t*                task,
    const struct kaapi_format_t* fmt,
    const kaapi_access_mode_t    mode,
    int                         ith
);

extern int kaapi_memory_task_param_unbind(
    kaapi_address_space_id_t     asid,
    kaapi_task_t*                task,
    const struct kaapi_format_t* fmt,
    const kaapi_access_mode_t    mmode,
    int                         ith
);


/* ============================ Memory Node ================================= */
typedef struct kaapi_memory_node_t {
    kaapi_address_space_id_t asid;

    void *(*f_alloc)(struct kaapi_memory_node_t*,  size_t);
    void  (*f_free)(struct kaapi_memory_node_t*, void *);
    int   (*f_copy)(struct kaapi_memory_node_t*,
                    kaapi_pointer_t /* dest*/,
                    const kaapi_memory_view_t* /*view_dest*/,
                    kaapi_pointer_t /*src*/,
                    const kaapi_memory_view_t* /*view_src*/,
                    kaapi_io_cbk_fnc_t cbk,
                    void* arg0, void* arg1, void* arg2
    );
    void  (*f_memsync)(struct kaapi_memory_node_t*, int begend);

    /* to manage cache */
    size_t (*f_get_total_mem)(struct kaapi_memory_node_t* node);

    /* memory software cache */
    struct kaapi_memory_cache_t* cache;
} kaapi_memory_node_t;

/* Unregister a memory node.
*/
extern kaapi_memory_node_t* kaapi_memory_node_unregister(kaapi_address_space_id_t asid );

/* Register a memory node. Pointer validity must be enough.
*/
extern int kaapi_memory_node_register(
    kaapi_address_space_id_t kasid,
    kaapi_memory_node_t* const node
);


/*
*/
extern kaapi_memory_node_t* kaapi_memory_node_get( kaapi_address_space_id_t asid );

/*
*/
extern kaapi_pointer_t kaapi_memory_alloc(kaapi_address_space_id_t kasid, size_t size);

/*
*/
extern void kaapi_memory_free(kaapi_pointer_t ptr );

/* High level function: do memory copy
   Return 0 if the operation successfully completes before 
   returning to the caller.
   It returns EBUSY if the operation does not complete.
   Else it returns error code.
*/
extern int kaapi_memory_copy(
    kaapi_pointer_t dest, const kaapi_memory_view_t* view_dest,
    kaapi_pointer_t src, const kaapi_memory_view_t* view_src,
    kaapi_io_cbk_fnc_t cbk, void* arg0, void* arg1, void* arg2
);

/*
 */
extern int _kaapi_memory_sync_addr(kaapi_address_space_id_t asid, kaapi_metadata_info_t* mdi);

/* ============================ Replicas ================================= */
    
extern kaapi_data_replica_t* kaapi_memory_replica_bind(
    kaapi_metadata_info_t* mdi,
    kaapi_address_space_id_t kasid,
    void* ptr, kaapi_memory_view_t view
);

extern void kaapi_memory_replica_unbind( kaapi_metadata_info_t* mdi, kaapi_address_space_id_t kasid );

extern kaapi_address_space_id_t kaapi_memory_choose_valid_asid(
      kaapi_metadata_info_t* mdi
);

static inline kaapi_data_replica_t* kaapi_memory_replica_get(
    kaapi_metadata_info_t*   mdi,
    kaapi_address_space_id_t kasid
)
{
  kaapi_assert_debug(mdi != 0);
  uint8_t lid = kaapi_memory_asid_get_lid(kasid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  return &mdi->replicas[lid];
}

static inline void kaapi_memory_replica_set_valid(
    kaapi_metadata_info_t*   mdi,
    kaapi_address_space_id_t kasid
)
{
  kaapi_assert_debug(mdi != 0);
  uint8_t lid = kaapi_memory_asid_get_lid(kasid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  KAAPI_ATOMIC_OR64(&mdi->valid, (1UL<<lid));
}

static inline void kaapi_memory_replica_unset_valid(
    kaapi_metadata_info_t*   mdi,
    kaapi_address_space_id_t kasid
)
{
  kaapi_assert_debug(mdi != 0);
  uint8_t lid = kaapi_memory_asid_get_lid(kasid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  KAAPI_ATOMIC_AND64(&mdi->valid, ~(uint64_t)(1UL<<lid));
}

static inline bool kaapi_memory_replica_is_valid(
    kaapi_metadata_info_t*   mdi,
    kaapi_address_space_id_t kasid
)
{
  kaapi_assert_debug(mdi != 0);
  uint8_t lid = kaapi_memory_asid_get_lid(kasid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  return  ((KAAPI_ATOMIC_READ(&mdi->valid) & (1UL<<lid)) !=0)
       && ((KAAPI_ATOMIC_READ(&mdi->allocated) & (1UL<<lid)) !=0);
}

static inline void kaapi_memory_replica_set_all_dirty_except(
    kaapi_metadata_info_t*   mdi,
    kaapi_address_space_id_t kasid
)
{
  kaapi_assert_debug(mdi != 0);
  uint8_t lid = kaapi_memory_asid_get_lid(kasid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  KAAPI_ATOMIC_OR64(&mdi->valid, (1UL<<lid));
  KAAPI_ATOMIC_AND64(&mdi->valid, (1UL<<lid));
}

static inline void kaapi_memory_replica_set_allocated(
    kaapi_metadata_info_t*   mdi,
    kaapi_address_space_id_t kasid
)
{
  kaapi_assert_debug(mdi != 0);
  uint8_t lid = kaapi_memory_asid_get_lid(kasid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  KAAPI_ATOMIC_OR64(&mdi->allocated, (1UL<<lid));
}

static inline void kaapi_memory_replica_unset_allocated(
    kaapi_metadata_info_t*   mdi,
    kaapi_address_space_id_t kasid
)
{
  kaapi_assert_debug(mdi != 0);
  uint8_t lid = kaapi_memory_asid_get_lid(kasid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  KAAPI_ATOMIC_AND64(&mdi->allocated, ~(uint64_t)(1UL<<lid));
}

static inline bool kaapi_memory_replica_is_allocated(
    kaapi_metadata_info_t*   mdi,
    kaapi_address_space_id_t kasid
)
{
  kaapi_assert_debug(mdi != 0);
  uint8_t lid = kaapi_memory_asid_get_lid(kasid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  return  ((KAAPI_ATOMIC_READ(&mdi->allocated) & (1UL<<lid)) !=0);
}

static inline size_t kaapi_memory_replica_view_size(
    kaapi_metadata_info_t*   mdi,
    kaapi_address_space_id_t kasid
)
{
  kaapi_assert_debug(mdi != 0);
  uint8_t lid = kaapi_memory_asid_get_lid(kasid);
  kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
  return  kaapi_memory_view_size(&mdi->replicas[lid].view);
}



/* ============================ Replicas ================================= */
struct kaapi_memory_cache_t;

/** \ingroup Offload
 * Allocates an empty software cache for the device. All blocks
 * are in a FIFO double-ended queue, LRU policy.
 */
extern struct kaapi_memory_cache_t* kaapi_memory_cache_init(struct kaapi_memory_node_t* const node);

/** \ingroup Offload
 */
extern void kaapi_memory_cache_destroy(struct kaapi_memory_cache_t* const cache);

/** \ingroup Offload
 * A hit means that this replica (asid) has been allocated, and now
 * had a new access from its cache. The replica goes to the queue head,
 * following a LRU policy.
 */
extern void kaapi_memory_cache_hit(
  kaapi_address_space_id_t   asid,
  struct kaapi_metadata_info_t* mdi
);

/** \ingroup Offload
 * A miss means that this replica (asid) was not found on the SW cache,
 * and it was allocated just now.
 */
extern void kaapi_memory_cache_miss(
  kaapi_address_space_id_t asid,
  struct kaapi_metadata_info_t* mdi
);

/** \ingroup Offload
 * Evict some memory to make room for more allocations.
 * Return the total space freed.
 */
extern size_t kaapi_memory_cache_evict(
  kaapi_address_space_id_t asid,
  size_t size
);


#endif
