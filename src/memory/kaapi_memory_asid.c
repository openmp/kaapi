/*
 ** xkaapi
 ** 
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** fabien.lementec@imag.fr
 ** jvlima@inf.ufsm.br
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */

#include <stdio.h>

#include "kaapi_impl.h"

static kaapi_atomic_t count_localid = {0};

static inline void kaapi_memory_asid_set_gid( kaapi_address_space_id_t* kasid, kaapi_globalid_t gid )
{
    kaapi_assert_debug(kasid != 0);
    *kasid = (kaapi_address_space_id_t)( (gid << 16ULL) | (*kasid & ~KAAPI_ASID_MASK_GID) );
}

static inline void kaapi_memory_asid_set_type( kaapi_address_space_id_t* kasid, uint8_t arch )
{
    kaapi_assert_debug(kasid != 0);
    *kasid = (kaapi_address_space_id_t)( (((uint64_t)arch) << 56ULL) | (*kasid & ~KAAPI_ASID_MASK_ARCH) );
}
static inline void kaapi_memory_asid_set_lid( kaapi_address_space_id_t* kasid, uint8_t lid )
{
    kaapi_assert_debug(kasid != 0);
    *kasid = (kaapi_address_space_id_t)( (lid << 8UL) | (*kasid & ~KAAPI_ASID_MASK_LID) );
}
static inline void kaapi_memory_asid_set_nuid( kaapi_address_space_id_t* kasid, uint8_t nuid )
{
    kaapi_assert_debug(kasid != 0);
    *kasid = (kaapi_address_space_id_t)( (nuid) | (*kasid & ~KAAPI_ASID_MASK_NUID) );
}


/*
*/
kaapi_address_space_id_t kaapi_memory_choose_valid_asid(
      kaapi_metadata_info_t* mdi
)
{
  kaapi_data_replica_t* replica;
  int lid;
  lid = __builtin_ffsll( KAAPI_ATOMIC_READ(&mdi->valid));
  if (lid != 0 )
  {
    --lid;
    kaapi_assert_debug(lid < KAAPI_MEMORY_MAX_NODES);
    replica = &mdi->replicas[lid];
    return kaapi_pointer2asid(replica->ptr);
  }

  kaapi_abort(__LINE__, __FILE__, "Invalid condition");
  return 0;
}

/*
*/
kaapi_address_space_id_t kaapi_memory_asid_init(
  kaapi_globalid_t gid,
  uint8_t nuid,
  uint8_t lid,
  uint8_t arch
)
{
  kaapi_address_space_id_t kasid;

  kaapi_memory_asid_set_null(&kasid);
  kaapi_memory_asid_set_gid(&kasid, gid);
  kaapi_memory_asid_set_nuid(&kasid, nuid);
  kaapi_memory_asid_set_lid(&kasid, lid);
  kaapi_memory_asid_set_type(&kasid, arch);

  kaapi_assert_debug(kaapi_memory_asid_get_gid(kasid) == gid);
  kaapi_assert_debug(kaapi_memory_asid_get_nuid(kasid) == nuid);
  kaapi_assert_debug(kaapi_memory_asid_get_lid(kasid) == lid);
  kaapi_assert_debug(kaapi_memory_asid_get_arch(kasid) == arch);

#if 0
  fprintf(stdout, "%s: kasid %llx (gid=%d,type=%d,nuid=%d,lid=%d)\n", __FUNCTION__, kasid,
          kaapi_memory_asid_get_gid(kasid),
          kaapi_memory_asid_get_arch(kasid),
          kaapi_memory_asid_get_nuid(kasid),
          kaapi_memory_asid_get_lid(kasid));
#endif

    return kasid;
}

/*
*/
kaapi_address_space_id_t kaapi_memory_create_asid(kaapi_globalid_t gid, uint8_t nuid, uint8_t arch)
{
  if (arch >= KAAPI_PROC_TYPE_MAX) return 0;
  int l = KAAPI_ATOMIC_INCR( &count_localid );
  kaapi_assert( l<256 );
  uint8_t lid = (uint8_t)l;
  return kaapi_memory_asid_init(gid, nuid, lid, arch);
}
