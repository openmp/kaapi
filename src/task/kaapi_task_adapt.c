/*
 ** xkaapi
 ** 
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** fabien.lementec@imag.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include "kaapi_impl.h"

/* - adaptive task body: this task drives the execution of the adaptive task,
   especially for the terminaison part.
*/
void kaapi_taskadapt_body(kaapi_task_t* task, kaapi_thread_t* thread )
{
  kaapi_taskadaptive_arg_t* arg = (kaapi_taskadaptive_arg_t*)kaapi_task_getargs(task);
  arg->user_task.body( &arg->user_task, thread );
}


/*
*/
void kaapi_tasksignaladapt_body(kaapi_task_t* task, kaapi_thread_t* thread)
{
  kaapi_taskadaptive_arg_t* arg = (kaapi_taskadaptive_arg_t*)kaapi_task_getargs(task);

  /* normal adapt body */
  kaapi_taskadapt_body( task, thread );

  /* wait sub tasks and signal victim context */
  kaapi_sched_sync( thread );
  if (arg->msc !=0)
  {
    KAAPI_ATOMIC_DECR( &arg->msc->count );
  }
}


/*
*/
kaapi_task_t* kaapi_task_create_adaptive_withflags(
  kaapi_task_t*                 task,
  kaapi_thread_t*               thread,
  kaapi_task_body_t             body,
  kaapi_adaptivetask_splitter_t user_splitter,
  void*                         arg,
  kaapi_task_flag_t             flag  
)
{
  kaapi_taskadaptive_arg_t* adaptarg;

  /* this is the new adaptative task */
  adaptarg = (kaapi_taskadaptive_arg_t*)
          kaapi_thread_pushdata_align(thread, sizeof(kaapi_taskadaptive_arg_t),sizeof(void*));
  adaptarg->user_task.body        = body;
  adaptarg->user_task.state.steal = body;
  adaptarg->user_task.arg         = arg;
  adaptarg->user_task.u.dummy     = 0;
  adaptarg->user_splitter         = user_splitter;
  KAAPI_ATOMIC_WRITE(&adaptarg->count, 0);
  adaptarg->msc                   = 0;

  return kaapi_task_create_withflags( task, kaapi_taskadapt_body, adaptarg, flag);
}

/*
*/
int kaapi_task_push_adaptive_withflags(
  kaapi_thread_t*               thread,
  kaapi_task_body_t             body,
  kaapi_adaptivetask_splitter_t splitter,
  void*                         arg,
  kaapi_task_flag_t             flag  
)
{
  kaapi_task_t* task = thread->sp;
  if (!_kaapi_has_enough_taskspace(thread))
    task = kaapi_thread_slow_pushtask(thread);
  kaapi_task_create_adaptive_withflags( task, thread, body, splitter, arg, flag);
  return kaapi_task_commit(thread);
}


/*
*/
int kaapi_task_push_adaptive(
  kaapi_thread_t*               thread,
  kaapi_task_body_t             body,
  kaapi_adaptivetask_splitter_t splitter,
  void*                         arg
)
{
  return kaapi_task_push_adaptive_withflags(
                thread,
                body,
                splitter,
                arg,
                KAAPI_TASK_FLAG_DEFAULT
  );
}


/*
*/
int kaapi_thread_push_stolentask(
    kaapi_thread_t*        thread,
    kaapi_task_t*          victim_task,
    kaapi_task_body_t      body,
    void*                  arg
)
{
  kaapi_taskadaptive_arg_t* adaptarg;
  
  if (victim_task ==0)
  {
    _kaapi_task_push( thread, body, arg );
    _kaapi_task_commit( thread );
    return 0;
  }

  /* this is the new task from adaptive task */
  adaptarg = (kaapi_taskadaptive_arg_t*)
      kaapi_thread_pushdata(thread, sizeof(kaapi_taskadaptive_arg_t));
  adaptarg->user_task.body = body;
  adaptarg->user_task.arg  = arg;
  adaptarg->user_splitter  = 0;
  KAAPI_ATOMIC_WRITE(&adaptarg->count, 0);
  adaptarg->msc            = 0;

  /* memory barrier done by kaapi_task_push */
  _kaapi_task_push( thread, kaapi_tasksignaladapt_body, adaptarg );
  _kaapi_task_commit( thread );
  return 0;
}

/*
*/
int kaapi_request_pushtask(
    kaapi_steal_request_t* request,
    kaapi_task_t*          victim_task,
    kaapi_task_body_t      body,
    void*                  arg
)
{
  return kaapi_thread_push_stolentask( &request->thread, victim_task, body, arg );
}


/* Here:
 */
int kaapi_request_pushtask_adaptive(
  kaapi_steal_request_t*        request,
  kaapi_task_t*                 victim_task, 
  kaapi_task_body_t             body,
  kaapi_adaptivetask_splitter_t splitter,
  void*                         arg
)
{
  kaapi_taskadaptive_arg_t* victim_adapt_arg;
  kaapi_taskadaptive_arg_t* adapt_arg;
  
  kaapi_assert_debug( kaapi_task_is_splittable(victim_task) );
  kaapi_assert_debug( victim_task->body == kaapi_taskadapt_body );

  /* same as push an adaptive task but change the body to kaapi_tasksignaladapt_body */  
  kaapi_task_push_adaptive(&request->thread, body, splitter, arg);
  request->thread.sp[-1].body = kaapi_tasksignaladapt_body;

  victim_adapt_arg = kaapi_task_getargst(victim_task, kaapi_taskadaptive_arg_t);
  adapt_arg = kaapi_task_getargst(request->thread.sp-1,  kaapi_taskadaptive_arg_t);

  /* link together tasks */
  adapt_arg->msc = (victim_adapt_arg->msc == 0 ? victim_adapt_arg : victim_adapt_arg->msc);
  KAAPI_ATOMIC_INCR( &victim_adapt_arg->count );

  return 0;
}


/*
*/
int kaapi_request_committask(kaapi_steal_request_t* request)
{
  return kaapi_request_reply((kaapi_request_t*)request, KAAPI_REQUEST_S_OK);
}

