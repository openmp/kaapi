/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"
#include <errno.h>

#if defined(KAAPI_DEBUG)
#include <unistd.h>
#include <sys/mman.h>
#endif

#if defined(KAAPI_DEBUG)
/* to be called from debuger */
__attribute__((unused)) static kaapi_stack_bloc_t* __kaapi_end_blocaddr( void* addr )
{
  return _kaapi_end_blocaddr(addr, kaapi_stack_bloc_t*);
}
__attribute__((unused))static kaapi_stack_bloc_t* __kaapi_start_blocaddr( void* addr ) 
{
  return _kaapi_start_blocaddr(addr, kaapi_stack_bloc_t*);
}
__attribute__((unused))static kaapi_task_t* __kaapi_firstin_blocaddr( kaapi_stack_bloc_t* addr )
{
  return kaapi_firstin_stack_bloc(addr, kaapi_task_t);
}
__attribute__((unused))static kaapi_task_t* __kaapi_lastin_stack_bloc( kaapi_stack_bloc_t* addr )
{
  return kaapi_lastin_stack_bloc(addr, kaapi_task_t);
}
#endif

#define KAAPI_ALIGNDOWN(addr,a) (((uintptr_t)(addr)) & ~((a)-1))

kaapi_stack_bloc_t* kaapi_processor_alloc_stackbloc(kaapi_processor_t* kproc)
{
  size_t sizealloc = kaapi_default_param.stackblocsize;
  kaapi_assert_debug( sizealloc >=  KAAPI_STACKBLOCSIZE);
  kaapi_assert_debug( sizealloc % sizeof(kaapi_task_t) == 0);
  kaapi_stack_bloc_t* bloc;
  kaapi_place_t* place = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[kproc->rsrc.numaid];

redo_pop:
  bloc = place->freebloc;
  if (bloc !=0)
  {
    if (!KAAPI_ATOMIC_CASPTR(&place->freebloc, bloc, bloc->next))
      goto redo_pop;

    bloc->next = 0;
    bloc->save_ptr = 0;
    return bloc;
  }

  int err = posix_memalign((void**)&bloc, KAAPI_STACKBLOCSIZE, sizealloc );
  kaapi_assert_debug_m(err ==0, "stack alloc");
  if (err !=0)
    return 0;
  kaapi_assert_debug( (_kaapi_end_blocaddr(bloc,char*) - kaapi_firstin_stack_bloc(bloc,char)) % sizeof(kaapi_task_t) == 0 );
  bloc->size = kaapi_default_param.stackblocsize;
  bloc->save_ptr = 0;
  bloc->next = 0;
#if defined(KAAPI_USE_PERFCOUNTER)
  KAAPI_PERFCTR_INCR(kproc->rsrc.perfkproc, KAAPI_PERF_ID_STACKSIZEBLOC, sizealloc);
#endif
  return bloc;
}


/* no lock */
void kaapi_processor_dealloc_stackbloc(
    kaapi_processor_t* kproc, 
    kaapi_stack_bloc_t* bloc
)
{
  if (bloc == 0) return;
  kaapi_place_t* place = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[kproc->rsrc.numaid];
  kaapi_stack_bloc_t* head;

redo_push:
  head = place->freebloc;
  bloc->next = head;
  if (!KAAPI_ATOMIC_CASPTR(&place->freebloc, head, bloc))
    goto redo_push;
}


/*
*/
int kaapi_stack_init( kaapi_processor_t* kproc, kaapi_stack_t* stack )
{
  char* first_data;
  kaapi_frame_t* frame;

  stack->bloc_task = kaapi_processor_alloc_stackbloc(kproc);
  if (stack->bloc_task ==0)
    KAAPI_RETURN_ERROR(ENOMEM,0);
  stack->bloc_task0 = stack->bloc_task;
  stack->bloc_data = kaapi_processor_alloc_stackbloc(kproc);
  if (stack->bloc_data ==0)
    KAAPI_RETURN_ERROR(ENOMEM,0);
  stack->bloc_data0 = stack->bloc_data;

  first_data = kaapi_firstin_stack_bloc(stack->bloc_data, char);

  /* this frame should never be deleted */
  frame = (kaapi_frame_t*)first_data;
  first_data += sizeof(kaapi_frame_t);
  stack->top_frame = frame;
  _kaapi_frame_init( frame );

  stack->thread.sp_data      = first_data;
  stack->thread.sp           = kaapi_firstin_stack_bloc(stack->bloc_task, kaapi_task_t);
  stack->pc                  = stack->thread.sp;
  stack->unlink              = frame;

  stack->popfp               = 1;
  stack->thieffp             = 0;
  stack->cnt_task            = 0;

  return kaapi_atomic_initlock( &stack->lock);
}


/*
*/
int kaapi_stack_reset(kaapi_stack_t* stack )
{
  char* first_data;
  kaapi_frame_t* frame;
  kaapi_processor_t* kproc = kaapi_stack2context(stack)->proc;

  /* */
  kaapi_assert_debug( stack->top_frame->next == 0 );

  /* pop all frames until stack->top_frame */
  kaapi_assert_debug( stack->bloc_data->next == 0  );
  kaapi_assert_debug( stack->bloc_task->next == 0  );

  /* the first pushed remains in the stack */
  kaapi_assert_debug( stack->pc == stack->thread.sp );
  kaapi_assert_debug( stack->popfp == 1 );

  /* free bloc between bloc_task0 and bloc_task */
  if ((stack->bloc_task != stack->bloc_task0) || (stack->bloc_data != stack->bloc_data0))
  {
    kaapi_place_t* place = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[kproc->rsrc.numaid];
    kaapi_stack_bloc_t* head;
    if (stack->bloc_task != stack->bloc_task0)
    {
      kaapi_stack_bloc_t* blocn = stack->bloc_task0->next;
      stack->bloc_task0->next = 0;

  redo_push1:
      head = place->freebloc;
      stack->bloc_task->next = head;
      if (!KAAPI_ATOMIC_CASPTR(&place->freebloc, head, blocn))
        goto redo_push1;
    }

    if (stack->bloc_data != stack->bloc_data0)
    {
      kaapi_stack_bloc_t* blocn = stack->bloc_data0->next;
      stack->bloc_data0->next = 0;

  redo_push2:
      head = place->freebloc;
      stack->bloc_data->next = head;
      if (!KAAPI_ATOMIC_CASPTR(&place->freebloc, head, blocn))
        goto redo_push2;
    }
    stack->bloc_task = stack->bloc_task0;
    stack->bloc_data = stack->bloc_data0;
  }

  first_data = kaapi_firstin_stack_bloc(stack->bloc_data, char);
  frame = (kaapi_frame_t*)first_data;
  first_data += sizeof(kaapi_frame_t);
  kaapi_assert_debug( stack->top_frame == frame );
  _kaapi_frame_init(frame);
/*
  if (frame->rdlist && (frame->flag & KAAPI_FRAME_FLAG_DFG_OK))
    kaapi_hashmap_clear( &frame->rdlist->ctxt.access_to_gd );
*/

  /* ordered like this to allow concurrent update */
  stack->pc                = 0;
  kaapi_writemem_barrier();

  stack->thread.sp_data    = first_data;
  stack->thread.sp         = kaapi_firstin_stack_bloc(stack->bloc_task, kaapi_task_t);
  stack->pc                = stack->thread.sp;
  stack->unlink            = frame;

  kaapi_writemem_barrier();

  stack->pc                = stack->thread.sp;
  stack->cnt_task          = 0;

  return 0;
}


/*
*/
int kaapi_stack_destroy(kaapi_processor_t* kproc, kaapi_stack_t* stack )
{
  if (stack->thread.sp != stack->pc)
    return EBUSY;

  /* free bloc between bloc_task0 and bloc_task */
  if ((0 != stack->bloc_task0) || (0 != stack->bloc_data0))
  {
    kaapi_place_t* place = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[kproc->rsrc.numaid];
    kaapi_stack_bloc_t* head;
    kaapi_stack_bloc_t* blocn;

    blocn = stack->bloc_task0;
    kaapi_assert_debug(blocn->next == 0);

redo_push1:
    head = place->freebloc;
    stack->bloc_task->next = head;
    if (!KAAPI_ATOMIC_CASPTR(&place->freebloc, head, blocn))
      goto redo_push1;


    blocn = stack->bloc_data0;
    kaapi_assert_debug(blocn->next == 0);

redo_push2:
    head = place->freebloc;
    stack->bloc_data->next = head;
    if (!KAAPI_ATOMIC_CASPTR(&place->freebloc, head, blocn))
      goto redo_push2;

    stack->bloc_task = stack->bloc_task0 = 0;
    stack->bloc_data = stack->bloc_data0 = 0;
  }

  stack->top_frame = 0;
  
  return kaapi_atomic_destroylock( &stack->lock);
}



/*
*/
int kaapi_stack_iterator_init(
  kaapi_stack_iterator_t* iter,
  kaapi_stack_t* stack,
  kaapi_frame_t* frame
)
{
  if (frame->next ==0)
  { /* current runnng frame */
    iter->first = stack->pc;
    iter->last  = stack->thread.sp;
  }
  else
  { /* pushed frame: abort here ? */
    iter->first = frame->save_pc;
    iter->last  = frame->save_thread.sp;
  }

  if (iter->last == iter->first) 
    return 0;

  kaapi_stack_bloc_t* bloc;
  if (iter->first == _kaapi_start_blocaddr(iter->first, kaapi_task_t*))
  {
    bloc = _kaapi_start_blocaddr(iter->first-1, kaapi_stack_bloc_t*);
    bloc = bloc->next;
    iter->first = kaapi_firstin_stack_bloc( bloc, kaapi_task_t);
  }
  else
    bloc = _kaapi_start_blocaddr(iter->first, kaapi_stack_bloc_t*);

  kaapi_task_t* sp_bloc = bloc->next == 0 ?  iter->last : _kaapi_end_blocaddr(iter->first,kaapi_task_t*);

  if (iter->first == sp_bloc)
  {
    kaapi_stack_bloc_t* nextbloc = bloc->next;
    if (nextbloc ==0)
    {
      iter->first = iter->last;
      return 0;
    }
    iter->first = kaapi_firstin_stack_bloc(nextbloc, kaapi_task_t);
  }
#if defined(KAAPI_DEBUG)
  bloc = _kaapi_start_blocaddr(iter->first, kaapi_stack_bloc_t*);
  kaapi_stack_bloc_t* endbloc __attribute__((unused))
      = _kaapi_end_blocaddr(iter->first, kaapi_stack_bloc_t*);
  kaapi_stack_bloc_t* blocsp __attribute__((unused))
      = _kaapi_start_blocaddr(iter->last, kaapi_stack_bloc_t*);
  kaapi_assert_debug(iter->first >= kaapi_firstin_stack_bloc(bloc,kaapi_task_t) );
  kaapi_assert_debug(iter->first < kaapi_lastin_stack_bloc(bloc,kaapi_task_t) );
#endif
  return 0;
}


/* Pass to the next task. 
*/
void kaapi_stack_iterator_next( kaapi_stack_iterator_t* iter )
{
  kaapi_stack_bloc_t* bloc;
  if (iter->first == _kaapi_start_blocaddr(iter->first, kaapi_task_t*))
  {
    bloc = _kaapi_start_blocaddr(iter->first-1, kaapi_stack_bloc_t*);
    bloc = bloc->next;
    iter->first = kaapi_firstin_stack_bloc( bloc, kaapi_task_t);
  }
  else
    bloc = _kaapi_start_blocaddr(iter->first, kaapi_stack_bloc_t*);

  kaapi_task_t* sp_bloc = bloc->next == 0 ?  iter->last : _kaapi_end_blocaddr(iter->first,kaapi_task_t*);

  ++iter->first;
  if (iter->first == iter->last) 
    return;
    
  if (iter->first == sp_bloc)
  {
    kaapi_stack_bloc_t* nextbloc = bloc->next;
    if (nextbloc ==0)
    {
      iter->first = iter->last;
      return;
    }
    iter->first = kaapi_firstin_stack_bloc(nextbloc, kaapi_task_t);
  }
}


/* iterates over iter->first downto iter->last
*/
int kaapi_stack_reverse_iterator_init(
  kaapi_stack_iterator_t* iter,
  kaapi_stack_t*          stack,
  kaapi_frame_t*          frame
)
{
  if (frame->next ==0)
  { /* current runnng frame */
    iter->first  = stack->thread.sp;
    iter->last   = stack->pc;
  }
  else
  { /* pushed frame: abort here ? */
    iter->first  = frame->save_thread.sp;
    iter->last   = frame->save_pc;
  }

  if (iter->last == iter->first) 
    return 0;

  /* get the bloc that contains iter->first and may be correct iter->first */
  kaapi_stack_bloc_t* bloc;
redo:
  bloc = _kaapi_start_blocaddr(iter->first, kaapi_stack_bloc_t*);
  if (iter->first == kaapi_firstin_stack_bloc( bloc, kaapi_task_t))
  {
    iter->first = (kaapi_task_t*)bloc->save_ptr;
    goto redo;
  }
  else
    --iter->first;

  /* correct last to be last-1 */
redo2:
  bloc = _kaapi_start_blocaddr(iter->last, kaapi_stack_bloc_t*);
  if (iter->last == kaapi_firstin_stack_bloc( bloc, kaapi_task_t))
  {
    iter->last = (kaapi_task_t*)bloc->save_ptr;
    goto redo2;
  }
  else
    --iter->last;

  return 0;
}


/* Pass to the prev task.
*/
void kaapi_stack_reverse_iterator_next( kaapi_stack_iterator_t* iter )
{
  kaapi_stack_bloc_t* bloc;
redo:
  bloc = _kaapi_start_blocaddr(iter->first, kaapi_stack_bloc_t*);
  if (iter->first == kaapi_firstin_stack_bloc( bloc, kaapi_task_t))
  {
    iter->first = (kaapi_task_t*)bloc->save_ptr;
    goto redo;
  }
  else
    --iter->first;
}
