/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_TASK_H_
#define _KAAPI_TASK_H_ 1

#include "config.h"
#include "kaapi_error.h"
#include "kaapi_atomic.h"
#include "kaapi_hashmap.h"
#include <string.h>
//#include <stdio.h>

/* use linked list */
#define KAAPI_LINKED_LIST 1

struct kaapi_task_t;
struct kaapi_frame_t;
struct kaapi_stack_t;
struct kaapi_stack_bloc_t;
struct kaapi_processor_t;
struct kaapi_ressource_t;


/* ============================= Task's states and flags ============================ */
/** Scheduling information for the task:
    - KAAPI_TASK_IS_UNSTEALABLE: defined iff the task is not stealable
    - KAAPI_TASK_IS_SPLITTABLE : defined iff the task is splittable
*/
#define KAAPI_TASK_UNSTEALABLE_MASK    0x01
#define KAAPI_TASK_STRICTAFFINITY_MASK 0x10

/** States of a task.
*/
#define KAAPI_TASK_STATE_INIT       0x0
#define KAAPI_TASK_STATE_EXEC       0x1
#define KAAPI_TASK_STATE_STEAL      0x2
#define KAAPI_TASK_STATE_TERM       0x4
#define KAAPI_TASK_STATE_MERGE      0x8


/* Help in decoding access mode
*/
extern char kaapi_getmodename( kaapi_access_mode_t m );

/* ============================= List, queue of tasks ============================ */
/** A list of task. Double linked.
*/
#if defined(KAAPI_LINKED_LIST)
typedef struct kaapi_list_t {
  struct kaapi_task_t*           head;
  struct kaapi_task_t*           tail;    /* for insertion at the end (seq order) */
} kaapi_list_t;

typedef struct kaapi_list_iterator_t {
  struct kaapi_task_t* curr;
} kaapi_list_iterator_t;

#else
typedef struct kaapi_list_t {
  struct kaapi_task_t**          buffer;
  uint32_t                       capacity;
  uint32_t                       mask;
  uint32_t                       head;      /* head of the list */
  uint32_t                       tail;      /* 1+ last item in the list */
} kaapi_list_t;

typedef struct kaapi_list_iterator_t {
  struct kaapi_task_t**          buffer;
  uint32_t                       first;
  uint32_t                       last;
  uint32_t                       mask;
} kaapi_list_iterator_t;
#endif


/** Queue of tasks with low and high priority queue
*/
typedef struct kaapi_queue_t {
#if defined(OLD_QUEUE)
  kaapi_list_t  hq;   /* high priority */
  kaapi_list_t  q;    /* low priority */
  uint32_t      size; /* size of the queue */
#else
  uint64_t      bitmap[4];  /* bit i  !=0 -> (*q)[i] is not empty */
  kaapi_list_t  (*q)[256];
  uint32_t      size; /* size of the queue */
#endif
} kaapi_queue_t;


/* ============================= The stack of task data structure ============================ */
/* Iterator on a stack of task (used by thief)
   - it iterates form oldest to newest task in the creation order
*/
typedef struct kaapi_stack_iterator_t {
  kaapi_task_t*  first;
  kaapi_task_t*  last;
} kaapi_stack_iterator_t;


typedef enum {
  KAAPI_STACK_ITERATE_DEFAULT
} kaapi_stack_iterator_flag_t;

/*
*/
#define KAAPI_SIZE_DFGCTXT 15
typedef struct {
  kaapi_hashmap_t                access_to_gd;
  kaapi_hashentries_t*           mapentries[1<<KAAPI_SIZE_DFGCTXT];
  kaapi_hashentries_bloc_t       mapbloc;
} kaapi_frame_dfgctxt_t;

/* extension to frame with dfg representation as list of task with successor 
*/
typedef struct kaapi_frame_wrdlist_t {
  kaapi_list_t                   rd;           /* ready list of tasks when computed */
  kaapi_task_t*                  starttask;
  struct kaapi_frame_wrdlist_t*  master;
  uint64_t                       t_infinity;   /* t_infinity */
  uint64_t                       cnt_task;     /* number of tasks after DFG computation */
  kaapi_atomic64_t               cnt_exec;     /* number of tasks exec */
  kaapi_atomic64_t               cnt_worker;   /* number of active worker */
  kaapi_frame_dfgctxt_t*         ctxt;         /* pointer to the context */
} kaapi_frame_wrdlist_t;


/* Internal representation of a frame in a stack:
   - the frame store the state of the stack (pc,sp,sp_data,last_sp_data,bloc) as well as
   link pointer: prev to pop a frame and next to allow thieves to following frame structure
   during task stealing.
*/
typedef struct kaapi_frame_t {
  kaapi_thread_t                 save_thread;
  kaapi_task_t*                  save_pc;
  struct kaapi_frame_t*          next;       /* next frame to steal (from top) */

  uintptr_t                      flag;       /* set if ready list should be used else next fields are invalid*/
  kaapi_frame_wrdlist_t*         rdlist;     /* valid iff flag & DFG_OK */
  uint64_t                       cnt_task;
} kaapi_frame_t __attribute__((aligned(16)));

#define KAAPI_FRAME_FLAG_DFG_VOID     0
#define KAAPI_FRAME_FLAG_DFG_INIT     0x1
#define KAAPI_FRAME_FLAG_DFG_OK       0x2
#define KAAPI_FRAME_FLAG_MASK_STATE   0x3
#define KAAPI_FRAME_FLAG_NOTSTEAL     0x4
#define KAAPI_FRAME_FLAG_STACKALLOC   0x8


typedef struct kaapi_stack_bloc_t {
  struct kaapi_stack_bloc_t* next;
  uintptr_t                  size;
  uintptr_t                  dummy; /* of the current bloc */
  void*                      save_ptr;
  char*                      data __attribute__((aligned(sizeof(kaapi_task_t))));
} kaapi_stack_bloc_t;

#define kaapi_firstin_stack_bloc(bloc,type) \
  ((type*)&(bloc)->data)
#define kaapi_lastin_stack_bloc(bloc,type) \
  (((type*)&(bloc)->data)+((bloc)->size-sizeof(type))/sizeof(type))


/* A stack
   A stack a is LIFO queue of frames. A frame is a FIFO queue of tasks.
   New frame is pushed on bottom and poped on bottom.
   Thief request occurs on the top of the queue through top_frame field.

   WARNING: the public interface does not expose the kaapi_stack_t data structure. 
   The first field (stack.thread) is cast to a kaapi_thread_t on request : it has the same
   first field.
   A public pointer to a kaapi_thread_t can always be casted to a kaapi_stack_t*
   in order to get access to internal field.
*/
typedef struct kaapi_stack_t {
  kaapi_thread_t             thread;    /* used to store task by API, recopy back into frame */
  kaapi_task_t*              pc;        /* next task to execute or the running task */
  kaapi_frame_t*             unlink;    /* prev of the current frame */
  struct kaapi_stack_bloc_t* bloc_task;
  struct kaapi_stack_bloc_t* bloc_data;
  struct kaapi_stack_bloc_t* bloc_task0; /* used to keep first allocated bloc and free other */
  struct kaapi_stack_bloc_t* bloc_data0; /* same for data */
  long volatile              popfp   __attribute__((aligned (KAAPI_CACHE_LINE)));
  long volatile              thieffp __attribute__((aligned (KAAPI_CACHE_LINE)));
  kaapi_frame_t*             top_frame;   /* top frame for the stealer */
  uint64_t                   cnt_task; 
  kaapi_lock_t               lock;
} kaapi_stack_t __attribute__((aligned (KAAPI_CACHE_LINE)));

/* conversion: assume that stack->thread is the first field returned to the public API */
#define kaapi_stack2thread(st) (&(st)->thread)
#define kaapi_stack2context(st) ((kaapi_context_t*)st)
#define kaapi_stack2rsrc(st)    (&((kaapi_context_t*)st)->proc->rsrc)
#define kaapi_thread2stack(th) ((kaapi_stack_t*)(th))
#define kaapi_thread2context(th) ((kaapi_context_t*)(th))


#if 0
/* CW context for parallel reduction */
typedef struct kaapi_cw_localcontext_t {
  void*        reference;
} kaapi_cw_localcontext_t;

typedef struct kaapi_cw_context_t {
  kaapi_lock_t          lock;
  kaapi_cpuset_t        used;  /* used[i] !=0 iff in used */
  kaapi_cpuset_t        alloc; /* alloc[i] !=0 iff alloc */
  int                   size;
  void**                data;  /* data[i] = on numa node i */
} kaapi_cw_context_t;
#endif

/* */

/* Return the number of tasks in the frame.
   Value may be bigger or less than real number of tasks. See sched_sync update of cnt
   task when the code pass from one bloc to an other one.
   Not complete version. Return >= 256 if not trivial computation
*/
static inline
uint64_t kaapi_frame_count_task( const kaapi_stack_t* stack, const kaapi_frame_t* frame )
{
  return frame->cnt_task;
}


/* Mark frame as not stealable, then switch flag to stealable when frame is pushed.
   Seems to be the OpenMP semantic, except if the running task do a scheduling point
   when pushing a new task.
*/
static inline void _kaapi_frame_init( kaapi_frame_t* frame )
{
  frame->save_thread.sp      = 0;
  frame->save_thread.sp_data = 0;
  frame->save_pc             = 0;
  frame->next                = 0;
  frame->flag                = KAAPI_FRAME_FLAG_DFG_VOID|KAAPI_FRAME_FLAG_NOTSTEAL;
  frame->rdlist              = 0;
  frame->cnt_task            = 0;
}

/*
*/
extern int kaapi_stack_iterator_init(
  kaapi_stack_iterator_t* iter,
  kaapi_stack_t* stack,
  kaapi_frame_t* frame
);

/*
*/
extern int kaapi_stack_reverse_iterator_init(
  kaapi_stack_iterator_t* iter,
  kaapi_stack_t* stack,
  kaapi_frame_t* frame
);

/*
*/
static inline int kaapi_stack_iterator_destroy( kaapi_stack_iterator_t* iter )
{
  return 0;
}

/*
*/
static inline int kaapi_stack_reverse_iterator_destroy( kaapi_stack_iterator_t* iter )
{
  return 0;
}

/* Pass to the next task.
*/
extern void kaapi_stack_iterator_next( kaapi_stack_iterator_t* iter );

/* Pass to the next task.
*/
extern void kaapi_stack_reverse_iterator_next( kaapi_stack_iterator_t* iter );

/* Return true if the iteration space is empty
*/
static inline int kaapi_stack_iterator_empty( kaapi_stack_iterator_t* iter )
{
  return (iter->first == iter->last);
}

/* Return the current task
*/
static inline kaapi_task_t* kaapi_stack_iterator_get( kaapi_stack_iterator_t* iter )
{
  return iter->first;
}

/** \ingroup TASK
    The function kaapi_stack_topframe() will return the top frame (the oldest pushed frame).
    The function returns a pointer to the bottom frame or 0. 
    \param stack IN a pointer to the kaapi_stack_t data structure.
    \retval a pointer to the top frame
*/
static inline kaapi_frame_t* kaapi_stack_topframe(const kaapi_stack_t* stack)
{
  return stack->top_frame;
}


/** \ingroup TASK
    The initialize a new stack.
    Return 0 in case of success.
*/
extern int kaapi_stack_init(
    struct kaapi_processor_t* kproc,
    kaapi_stack_t* stack
);


/** \ingroup TASK
    The reset the stack as 
    Return 0 in case of success.
*/
extern int kaapi_stack_reset(kaapi_stack_t* stack );


/** \ingroup TASK
    Destroy an init stack
    Return 0 in case of success.
*/
extern int kaapi_stack_destroy(struct kaapi_processor_t* kproc, kaapi_stack_t* stack );


/* Push a frame.
*/
extern kaapi_frame_t* kaapi_stack_push_frame( kaapi_stack_t* st );

/* Pop a frame. 
*/
extern void kaapi_stack_pop_frame( kaapi_stack_t* st, kaapi_frame_t* frame );


/** Assert
*/
static inline int kaapi_stack_isempty( const struct kaapi_stack_t* stack )
{
  if (stack ==0) return 0;
  if (stack->top_frame == stack->unlink) 
  {
    kaapi_task_t* pc = stack->pc; 
    kaapi_task_t* sp = stack->thread.sp; 
    return (sp == pc);
  }
  kaapi_frame_t* frame = stack->top_frame;
  while ((frame !=0) && (frame != stack->unlink))
  {
    kaapi_frame_t* next = frame->next;
    if (next ==0) return 1;
    if (next == stack->unlink)
    {
      if (frame->save_thread.sp != stack->pc) return 0;
    } else {
      if (frame->save_thread.sp != next->save_pc) return 0;
    }
    frame = frame->next;
  }
  return 1;
}


/* ===================== Default internal task body ==================================== */

/** Merge result after a steal
    This body is set by a thief at the end of the steal operation in case of 
    results to merge. Else the thief set the task' steal body to kaapi_term_body
    \ingroup TASK
*/
extern void kaapi_aftersteal_body( kaapi_task_t*, kaapi_thread_t*);

/** Body of task steal created on thief stack to execute a theft task.
    \ingroup TASK
*/
extern void kaapi_tasksteal_body( kaapi_task_t*, kaapi_thread_t* );

/** Body of task sync to mark synchronisation after multiple concurrent task
*/
extern void kaapi_tasksync_body( kaapi_task_t*, kaapi_thread_t* );

/** Set by steal on pc->steal field
*/
extern void kaapi_slowexec_body( kaapi_task_t*, kaapi_thread_t* );

/** Write result after a steal and signal the theft task.
    This is used during a task stealing operation.
    This task is pushed into the thief thread by kaapi_tasksteal_body.
    \ingroup TASK
*/
extern void kaapi_taskwrite_body( kaapi_task_t*, kaapi_thread_t* );

/** Body of the task to wrapper existing task to be adaptive
    The argument of such a task is of type kaapi_taskadaptive_arg_t
    \ingroup TASK
*/
extern void kaapi_taskadapt_body( kaapi_task_t*, kaapi_thread_t* );

/** Body of the task of an adaptive task pushed when reply to request.
    Same as kaapi_taskadapt_body with additional stuff to signal the splitted task
    as terminated.
    The argument of such a task is of type kaapi_taskadaptive_arg_t
    \ingroup TASK
*/
extern void kaapi_tasksignaladapt_body( kaapi_task_t*, kaapi_thread_t* );

/** \ingroup TASK
*/
/**@{ */
static inline int kaapi_task_get_priority(const struct kaapi_task_t* task)
{ 
  return (task->u.s.flag & KAAPI_TASK_FLAG_PRIORITY ? task->u.s.priority : 0);
}

#if 0
/* return 1 if kid is in the site mask */
static inline int kaapi_task_has_arch(const struct kaapi_task_t* task, int arch)
{ 
  return (task->u.s.arch ==0) || ((task->u.s.arch & (1<<arch)) !=0);
}

static inline int kaapi_task_is_unstealable(struct kaapi_task_t* task)
{ 
  return (task->u.s.flag & KAAPI_TASK_UNSTEALABLE_MASK) !=0; 
}
#endif

static inline int kaapi_task_is_splittable(struct kaapi_task_t* task)
{ 
  return (task->body == kaapi_taskadapt_body) || (task->body == kaapi_tasksignaladapt_body);
}

/* Return the !=0 iff the task was marked first to be theft.
   Should ensure excluive mark with respect to markexec.
*/
static inline int kaapi_task_marksteal( struct kaapi_task_t* task )
{
  kaapi_task_body_t body = kaapi_slowexec_body;
  KAAPI_ATOMIC_EXCHANGE(body, task->state.steal );
  kaapi_assert_debug( (body == 0) || (body == task->body) || (body == kaapi_slowexec_body) );
  return (body == task->body);
}

/* Task was stolen does not need atomic operation
*/
static inline void kaapi_task_markterm( struct kaapi_task_t* task )
{
  task->state.steal = kaapi_nop_body;
}

/* Task was stolen does not need atomic operation
*/static inline void kaapi_task_markaftersteal( struct kaapi_task_t* task )
{
  task->state.steal = kaapi_aftersteal_body;
}



/** Initialize specific format for some task 
*/
extern int kaapi_taskformat_init(void);
extern int kaapi_taskformat_finalize(void);

/** Args for tasksteal
*/
typedef struct kaapi_tasksteal_arg_t {
  struct kaapi_task_t*           origin_task;       /* the stolen task into origin_stack */
  struct kaapi_frame_wrdlist_t*  origin_frame_rd;   /* */
  unsigned int                   war_param;         /* bit i=1 iff a w mode with war dependency */
  unsigned int                   cw_param;          /* bit i=1 iff it is a cw mode */
  void*                          copy_task_args;    /* set by tasksteal a copy of the task args */
} kaapi_tasksteal_arg_t;


/** Args for kaapi_taskwrite_body
*/
typedef struct kaapi_taskwrite_arg_t {
  kaapi_tasksteal_arg_t*         stealarg;          /* arg of the stealbody */
} kaapi_taskwrite_arg_t;


/** Argument for kaapi_taskadapt_body.
    This argument is used to encapsulated any kind of adaptative task in order 
    to add an extra access to the steal context.
    The format of this task is the same as the format of the encapsulated task.
*/
typedef struct kaapi_taskadaptive_arg_t {
  kaapi_task_t                     user_task;         /* in the stack */
  kaapi_adaptivetask_splitter_t    user_splitter;
  kaapi_atomic_t                   count;             /* number of thieves */
  struct kaapi_taskadaptive_arg_t* msc;
} kaapi_taskadaptive_arg_t;


/* Argument for kaapi_tasksync_body used to mark synchronisation between multiple concurrent
   task.
*/
typedef struct {
  kaapi_access_t access;
  int            is_cw;
} kaapi_tasksync_arg_t;

/* Format for this task */
extern const struct kaapi_format_t* kaapi_tasksync_fmt;


/** \ingroup TASK
    The function kaapi_task_body_isstealable() will return non-zero value iff the task body may be stolen.
    All user tasks are stealable.
    \param body IN a task body
*/
static inline int kaapi_task_isstealable(const struct kaapi_task_t* task)
{
  kaapi_task_body_t body = task->state.steal;
  return (body == task->body) && ((task->u.s.flag & KAAPI_TASK_UNSTEALABLE_MASK) ==0);
}


/** \ingroup TASK
    The function kaapi_task_isready() will return non-zero value iff the task may be executed.
    The method assumes that dependency analysis has already verified that it does not exist unstatisfied
    data flow constraint.
    \param task IN a pointer to a task
*/
static inline int kaapi_task_isready(const struct kaapi_task_t* task)
{ 
  kaapi_task_body_t body = task->state.steal;
  return (body == task->body) || (body == kaapi_nop_body) || (body == kaapi_aftersteal_body);
}


/*
*/
static inline int kaapi_stack_isready( kaapi_stack_t* stack )
{
  return kaapi_task_isready(stack->pc);
}


/* */
extern void kaapi_list_init( kaapi_list_t* rd );

/* */
#if defined(KAAPI_LINKED_LIST)
static inline void kaapi_list_destroy( kaapi_list_t* rd )
{ }
#else
extern void kaapi_list_destroy( kaapi_list_t* rd );
#endif

/* */
static inline int kaapi_list_empty( const kaapi_list_t* rd )
{
#if defined(KAAPI_LINKED_LIST)
  return rd->head == 0;
#else
  return rd->head == rd->tail;
#endif
}

/* */
static inline kaapi_task_t* kaapi_list_head( kaapi_list_t* rd)
{
#if defined(KAAPI_LINKED_LIST)
  return rd->head;
#else
  return rd->buffer[rd->head];
#endif
}

#if defined(KAAPI_LINKED_LIST)
extern void kaapi_list_remove( kaapi_list_t* l, kaapi_task_t* t);
#endif

#if !defined(KAAPI_LINKED_LIST)
/*
*/
extern void _kaapi_list_resize( kaapi_list_t* rd);
#endif

/* */
extern void kaapi_list_push_head( kaapi_list_t* rd, kaapi_task_t* task);

/* */
extern void kaapi_list_push_tail( kaapi_list_t* rd, kaapi_task_t* task);


/* pop from head */
extern kaapi_task_t* kaapi_list_pop_head( kaapi_list_t* rd );

/* pop from tail */
extern kaapi_task_t* kaapi_list_pop_tail( kaapi_list_t* rd );


/*
*/
static inline int kaapi_list_iterator_empty( kaapi_list_iterator_t* iter )
{
#if defined(KAAPI_LINKED_LIST)
  return iter->curr ==0;
#else
  return iter->first == iter->last;
#endif
}

/*
*/
static inline int kaapi_list_iterator_init( kaapi_list_t* rd, kaapi_list_iterator_t* iter )
{
#if defined(KAAPI_LINKED_LIST)
  iter->curr = rd->head;
#else
  iter->buffer = rd->buffer;
  iter->mask   = rd->mask;
  iter->first  = rd->head;
  iter->last   = rd->tail;
#endif
  return kaapi_list_iterator_empty(iter);
}

/*
*/
static inline void kaapi_list_iterator_next( kaapi_list_iterator_t* iter )
{
#if defined(KAAPI_LINKED_LIST)
  iter->curr = iter->curr->next;
#else
  iter->first = (iter->first+1) & iter->mask;
#endif
}

/*
*/
static inline kaapi_task_t* kaapi_list_iterator_get( kaapi_list_iterator_t* iter )
{
#if defined(KAAPI_LINKED_LIST)
  return iter->curr;
#else
  return iter->buffer[iter->first];;
#endif
}

/*
*/
static inline int kaapi_list_iterator_destroy( kaapi_list_iterator_t* iter )
{
  return 0;
}


/* */
static inline int kaapi_queue_empty( const kaapi_queue_t* q )
{ 
  return (q->size == 0);
}

/* */
static inline int kaapi_queue_size( const kaapi_queue_t* q )
{ 
  return q->size;
}

/* */
extern int kaapi_queue_init( kaapi_queue_t* q );

/* */
extern int kaapi_queue_destroy( kaapi_queue_t* q );


/* Push a pointer to the task on the queue.
*/
extern int kaapi_queue_push_head
(
    kaapi_queue_t*         q,
    kaapi_task_t*          task
);

/* Push a pointer to the task on the queue.
*/
extern int kaapi_queue_push_tail
(
    kaapi_queue_t*         q,
    kaapi_task_t*          task
);

/* Return a task marked as stolen or 0 
   Pop from head
*/
extern kaapi_task_t* kaapi_queue_pop_head( kaapi_queue_t* q );

/* return a task marked as stolen or 0 
   Steal from tail
*/
extern void kaapi_queue_steal( kaapi_queue_t* q, struct kaapi_listrequest_iterator_t* lri );

extern void kaapi_queue_stealin( kaapi_queue_t* q, struct kaapi_listrequest_iterator_t* lri );

/* inline def */
static inline void kaapi_frame_rdlist_init( kaapi_frame_wrdlist_t* f, kaapi_frame_dfgctxt_t* ctxt )
{
  kaapi_hashmap_init( &ctxt->access_to_gd, ctxt->mapentries, KAAPI_SIZE_DFGCTXT, &ctxt->mapbloc );
  f->ctxt = ctxt;
  kaapi_list_init(&f->rd);
  f->starttask  = 0;
  f->t_infinity = 0;
  f->master     = 0;
  f->cnt_task   = 0;
  KAAPI_ATOMIC_WRITE(&f->cnt_exec, 0);
  KAAPI_ATOMIC_WRITE(&f->cnt_worker, 0);
}

#endif
