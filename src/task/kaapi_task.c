/*
** kaapi_task_standard.c
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"

#if !defined(KAAPI_LINKED_LIST)
/*
*/
typedef struct kaapi_list_bloc_t {
  struct kaapi_list_bloc_t* next;
} kaapi_list_bloc_t;


/* If no place
*/
static kaapi_list_bloc_t* head = 0;
#endif


#if defined(KAAPI_LINKED_LIST)
#warning "Use linked list implementation for queue"
#else
#warning "Use array based list implementation for queue"
#endif
#if defined(OLD_QUEUE)
#warning "Use old implementation queue, without priority"
#else
#warning "Use new implementation of priority queue"
#endif


/*
*/
char kaapi_getmodename( kaapi_access_mode_t m )
{
  switch (m) {
    case KAAPI_ACCESS_MODE_V:  return 'v';
    case KAAPI_ACCESS_MODE_R:  return 'r';
    case KAAPI_ACCESS_MODE_W:  return 'w';
    case KAAPI_ACCESS_MODE_CW: return 'c';
    case KAAPI_ACCESS_MODE_RW: return 'x';
    case KAAPI_ACCESS_MODE_SCRATCH: return 't';
    default: return '!';
  }
}


/*
*/
void kaapi_list_init( kaapi_list_t* rd )
{
  rd->tail = rd->head = 0;
#if !defined(KAAPI_LINKED_LIST)
#define LIST_CAPACITY 16
  kaapi_list_bloc_t* bloc;
  kaapi_list_bloc_t** ref_head;
  kaapi_ressource_t* rsrc = kaapi_self_rsrc();
  if (rsrc !=0)
  {
    kaapi_place_t* place = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid];
    ref_head = &place->l_freebloc;
  }
  else
    ref_head = &head;

redo_pop:
  bloc = *ref_head;
  if (bloc !=0)
  {
    if (!KAAPI_ATOMIC_CASPTR(ref_head, bloc, bloc->next))
      goto redo_pop;

    bloc->next = 0;
  }

  if (bloc == 0)
    bloc = (kaapi_list_bloc_t*)malloc( (1<<LIST_CAPACITY) * sizeof(kaapi_task_t*) );

  rd->capacity = LIST_CAPACITY;
  rd->mask     = (1<<rd->capacity)-1;
  rd->buffer   = (kaapi_task_t**)bloc;
#endif
}


/*
*/
#if !defined(KAAPI_LINKED_LIST)
void kaapi_list_destroy( kaapi_list_t* rd )
{
  kaapi_list_bloc_t* bloc = (kaapi_list_bloc_t*)rd->buffer;
  if (bloc == 0) return;

  kaapi_list_bloc_t** ref_head;
  kaapi_ressource_t* rsrc = kaapi_self_rsrc();
  if (rsrc !=0)
  {
    kaapi_place_t* place = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid];
    ref_head = &place->l_freebloc;
  }
  else 
    ref_head = &head;

  kaapi_list_bloc_t* head;

redo_push:
  head = *ref_head;
  bloc->next = head;
  if (!KAAPI_ATOMIC_CASPTR(ref_head, head, bloc))
    goto redo_push;
}
#endif


#if !defined(KAAPI_LINKED_LIST)
/*
*/
void _kaapi_list_resize( kaapi_list_t* rd)
{
  kaapi_assert( 0 );
  uint32_t newcapacity = rd->capacity+1;
  rd->buffer = realloc( rd->buffer, (1<<newcapacity)*sizeof(kaapi_task_t*));
  rd->capacity = newcapacity;
  rd->mask     = (1<<newcapacity) - 1;
}
#endif


/*
*/
static inline int kaapi_task_match_request(const kaapi_task_t* task, const kaapi_header_request_t* req)
{
  const kaapi_format_t* fmt = _kaapi_task_getformat( task );
  if (fmt == 0) return 0;
  return (fmt->mask_arch & req->mask_arch) != 0;
}


/* pop from head */
kaapi_task_t* kaapi_list_pop_head( kaapi_list_t* rd )
{
#if defined(KAAPI_LINKED_LIST)
  kaapi_task_t* task = rd->head;
  if (task ==0)
    return 0;
  rd->head = task->next;
  if (task->next ==0)
    rd->tail = 0;
  else
    rd->head->prev = 0;
  task->next = 0;
  kaapi_assert_debug( task->prev == 0 );
  return task;
#else
  if (rd->head == rd->tail) return 0;
  uint32_t head = rd->head;
  kaapi_task_t* task = rd->buffer[head];
  rd->buffer[head] = 0;
  rd->head = (head+1) & rd->mask;
  return task;
#endif
}


/* pop from tail */
kaapi_task_t* kaapi_list_pop_tail( kaapi_list_t* rd )
{
#if defined(KAAPI_LINKED_LIST)
  kaapi_task_t* task = rd->tail;
  if (task ==0)
    return 0;
  rd->tail = task->prev;
  if (task->prev ==0)
    rd->head = 0;
  else
    rd->tail->next = 0;
  task->prev = 0;
  kaapi_assert_debug( task->next == 0 );
  return task;
#else
  if (rd->head == rd->tail) return 0;
  uint32_t tail = (rd->tail-1) & rd->mask;
  kaapi_task_t* task = rd->buffer[tail];
  rd->buffer[tail] = 0;
  rd->tail = tail;
  return task;
#endif
}


/* */
void kaapi_list_push_head( kaapi_list_t* rd, kaapi_task_t* task)
{
#if defined(KAAPI_LINKED_LIST)
  kaapi_assert_debug( task->next == 0 );
  kaapi_assert_debug( task->prev == 0 );
  task->next = rd->head;
  task->prev = 0;
  if (rd->head == 0)
    rd->tail = task;
  else
    rd->head->prev = task;
  rd->head = task;
#else
  uint32_t head = (rd->head -1) & rd->mask;
  if (head == rd->tail)
  {
    _kaapi_list_resize(rd);
    head = (rd->head -1) & rd->mask;
  }
  rd->buffer[head] = task;
  rd->head         = head;
#endif
}


/* */
void kaapi_list_push_tail( kaapi_list_t* rd, kaapi_task_t* task)
{
#if defined(KAAPI_LINKED_LIST)
  kaapi_assert_debug( task->next == 0 );
  kaapi_assert_debug( task->prev == 0 );
  task->next = 0;
  task->prev = rd->tail;
  if (rd->tail == 0)
    rd->head = task;
  else
    rd->tail->next = task;
  rd->tail = task;
#else
  uint32_t otail = rd->tail;
  uint32_t tail = (otail +1) & rd->mask;
  if (tail == rd->head)
  {
    _kaapi_list_resize(rd);
    tail = (otail +1) & rd->mask;
  }
  rd->buffer[otail] = task;
  rd->tail          = tail;
#endif
}


int kaapi_queue_init( kaapi_queue_t* q )
{ 
#if defined(OLD_QUEUE)
  kaapi_list_init(&q->hq);
  kaapi_list_init(&q->q);
#else
  int i;
  q->bitmap[0] = q->bitmap[1] = q->bitmap[2] = q->bitmap[3] = 0;
  kaapi_assert(posix_memalign ((void**)&q->q, 64, sizeof(kaapi_list_t)*256 ) == 0);
  for (i=0; i<256; ++i)
    kaapi_list_init( &(*q->q)[i] );
#endif
  q->size = 0;
  return 1;
}

/* */
int kaapi_queue_destroy( kaapi_queue_t* q )
{ 
  kaapi_assert_debug( kaapi_queue_empty(q) );
#if defined(OLD_QUEUE)
  kaapi_list_destroy(&q->hq);
  kaapi_list_destroy(&q->q);
#else
  int i;
  kaapi_assert_debug(q->bitmap[0] == 0);
  kaapi_assert_debug(q->bitmap[1] == 0);
  kaapi_assert_debug(q->bitmap[2] == 0);
  kaapi_assert_debug(q->bitmap[3] == 0);
  for (i=0; i<256; ++i)
    kaapi_list_destroy( &(*q->q)[i] );
  free( q-> q);
  q->q = 0;
#endif
  return 1;
}


#if defined(KAAPI_LINKED_LIST)
/*
*/
void kaapi_list_remove( kaapi_list_t* l, kaapi_task_t* t)
{
  if (t->prev !=0)
    t->prev->next = t->next;
  if (t->next!=0)
    t->next->prev = t->prev;
  if (l->tail == t) l->tail = t->prev;
  if (l->head == t) l->head = t->next;
  t->next = 0;
  t->prev = 0;
}
#endif

#if defined(KAAPI_LINKED_LIST) && defined(OLD_QUEUE)
/* return a task marked as stolen or 0
   Steal from tail
*/
static kaapi_task_t* kaapi_list_steal( kaapi_list_t* l, kaapi_header_request_t* req )
{
  if (kaapi_default_param.use_priority) 
  {
    /*Look for maximum according to priority specified in task*/
    kaapi_task_t* task = l->tail;
    kaapi_task_t *max_task = 0;
    while (task !=0)
    {
      /*Check if it matches current arch and if it has higher priority than the last one*/
      if (kaapi_task_match_request(task, req) && (!max_task || (task->T > max_task->T)))
        max_task = task;
      task = task->prev;
    }
    if (max_task)
      kaapi_list_remove(l, max_task);
    return max_task;
  } else {
    /*Look for the first matching task*/
    kaapi_task_t* task = l->tail;
    while (task !=0)
    {
      /*Check if it matches current arch*/
      if (kaapi_task_match_request(task, req))
      {
        kaapi_list_remove(l, task);
        return task;
      }
      task = task->prev;
    }
    return 0;
  }
}
#else
/* return a task marked as stolen or 0 
   Steal from tail
*/
static kaapi_task_t* kaapi_list_steal( kaapi_list_t* l, kaapi_header_request_t* req )
{
  return kaapi_list_pop_tail( l );
}
#endif


#if !defined(OLD_QUEUE)
static inline int _kaapi_bitmap_empty( uint64_t* bits)
{
  if (bits[0]) return 0;
  if (bits[1]) return 0;
  if (bits[2]) return 0;
  if (bits[3]) return 0;
  return 1;
}

static inline int _kaapi_bitmap_ffl( uint64_t* bits)
{
  if (bits[0]) return __builtin_ffsll( bits[0] );
  if (bits[1]) return 64+__builtin_ffsll( bits[1] );
  if (bits[2]) return 128+__builtin_ffsll( bits[2] );
  if (bits[3]) return 192+__builtin_ffsll( bits[3] );
  return -1;
}

static inline void _kaapi_bitmap_set(int i, uint64_t* bits)
{
  bits[i/64] |= (1UL<< (i%64));
}

static inline void _kaapi_bitmap_clear(int i, uint64_t* bits)
{
  bits[i/64] &= ~(1UL<< (i%64));
}
#endif

/* return a task marked as stolen or 0
   Steal from tail
*/
static void _kaapi_queue_steal(
  kaapi_queue_t* q,
  struct kaapi_listrequest_iterator_t* lri,
  int flag
)
{
  kaapi_task_t* task;

#if !defined(OLD_QUEUE)
  int idx;
  uint64_t bitmap[4];
  bitmap[0] = q->bitmap[0];
  bitmap[1] = q->bitmap[1];
  bitmap[2] = q->bitmap[2];
  bitmap[3] = q->bitmap[3];
#endif

  while (!kaapi_listrequest_iterator_empty(lri))
  {
    kaapi_request_t* request = (kaapi_request_t*)kaapi_listrequest_iterator_get(lri);
    kaapi_assert_debug( ((flag == 0) && (request->header.op == KAAPI_REQUEST_OP_STEAL)) 
                     || ((flag == 1) && (request->header.op == KAAPI_REQUEST_OP_STEAL_INPLACE)) 
    );
    kaapi_assert_debug( !flag || (request->steal_a.thread.sp != 0) );
    kaapi_assert_debug( !flag || (request->steal_a.thread.sp_data != 0) );

    task = 0;

#if defined(OLD_QUEUE)
    if (!kaapi_list_empty(&q->hq))
      task = kaapi_list_steal( &q->hq, (kaapi_header_request_t*)request);
    if ((task ==0) && !kaapi_list_empty(&q->q))
      task = kaapi_list_steal(&q->q, (kaapi_header_request_t*)request);
    if (task ==0) break;
#else

redo:
    if (_kaapi_bitmap_empty(bitmap)) break;
    idx = _kaapi_bitmap_ffl( bitmap ) -1;
    task = kaapi_list_pop_tail( &(*q->q)[idx] ); // kaapi_list_steal( &(*q->q)[idx], (kaapi_header_request_t*)request );
    if (task ==0)
    {
       _kaapi_bitmap_clear( idx, bitmap );
       goto redo;
    }
    if (kaapi_list_empty( &(*q->q)[idx] ))
      _kaapi_bitmap_clear(idx, q->bitmap);
    //printf("%p:: steal task %p, prio: %i\n", q, task, idx );
#endif

    if (flag)
    {
      int err = kaapi_sched_copy_steal_task(
        task,
        0,
        0,
        task->state.frame,
        (kaapi_steal_request_t*)request
      );
#if defined (KAAPI_USE_PERFCOUNTER)
      KAAPI_PERFCTR_INCR(kaapi_self_rsrc()->perfkproc, KAAPI_PERF_ID_TASKSPAWN, 1);
#endif
      kaapi_assert( err == 0 );
    }
    else
    {
      request->pop_a.task = task;
      kaapi_request_reply( request, KAAPI_REQUEST_S_OK );
    }
    --q->size;
    kaapi_assert_debug(q->size >=0);

#if defined (KAAPI_USE_PERFCOUNTER)
    KAAPI_PERFCTR_INCR(kaapi_self_rsrc()->perfkproc, KAAPI_PERF_ID_TASKSPAWN, 1);
#endif
    kaapi_listrequest_iterator_next(lri);
  }

#if !defined(OLD_QUEUE) && defined(KAAPI_DEBUG)
  if (q->size == 0)
  { kaapi_assert( _kaapi_bitmap_empty(q->bitmap ) );  } 
  else
  { kaapi_assert( !_kaapi_bitmap_empty(q->bitmap ) );  }
#endif

  return;
}

/*
*/
void kaapi_queue_steal(
  kaapi_queue_t* q,
  struct kaapi_listrequest_iterator_t* lri
)
{
  if (kaapi_queue_empty(q)) return;
  _kaapi_queue_steal( q, lri, 0 );
}

/*
*/
void kaapi_queue_stealin(
  kaapi_queue_t* q,
  struct kaapi_listrequest_iterator_t* lri
)
{
  if (kaapi_queue_empty(q)) return;
  _kaapi_queue_steal( q, lri, 1 );
}


/* Push a pointer to the task on the queue.
*/
int kaapi_queue_push_head
(
    kaapi_queue_t*         q,
    kaapi_task_t*          task
)
{
#if defined(OLD_QUEUE)
  if (task->u.s.flag & KAAPI_TASK_FLAG_PRIORITY)
    kaapi_list_push_head(&q->hq, task);
  else
    kaapi_list_push_head(&q->q, task);
#else
  int prio = 0;
  if ((task->u.s.flag & KAAPI_TASK_FLAG_PRIORITY) || kaapi_default_param.use_priority)
  {
kaapi_assert( task->T < 256);
    prio = task->T % 256;
  }
  _kaapi_bitmap_set( 255-prio, q->bitmap );
  kaapi_list_push_head(&(*q->q)[255-prio], task);
  //printf("%p:: push task %p, prio: %i\n", q,  task, 255-prio );
#endif
  ++q->size;
  return 0;
}


/* Push a pointer to the task on the queue.
*/
int kaapi_queue_push_tail
(
    kaapi_queue_t*         q,
    kaapi_task_t*          task
)
{
#if defined(OLD_QUEUE)
  if (task->u.s.flag & KAAPI_TASK_FLAG_PRIORITY)
    kaapi_list_push_tail(&q->hq, task);
  else
    kaapi_list_push_tail(&q->q, task);
#else
  int prio = 0;
  if ((task->u.s.flag & KAAPI_TASK_FLAG_PRIORITY) || kaapi_default_param.use_priority)
  {
kaapi_assert( task->T < 256);
    prio = task->T % 256;
  }
  _kaapi_bitmap_set( 255-prio, q->bitmap );
  kaapi_list_push_tail(&(*q->q)[255-prio], task);
  //printf("%p:: push task %p, prio: %i\n", q,  task, 255-prio );
#endif
  ++q->size;
  return 0;
}


#if defined(OLD_QUEUE)
/* pop from head */
static kaapi_task_t* kaapi_list_pop_head_prio( kaapi_list_t* rd )
{
#if defined(KAAPI_LINKED_LIST)
  if (kaapi_default_param.use_priority)
  {
    /*Look for maximum according to priority specified in task*/
    kaapi_task_t* task = rd->head;
    kaapi_task_t *max_task = 0;
    while (task !=0)
    {
      /*Check if it matches current arch and if it has higher priority than the last one*/
      if (!max_task || (task->T > max_task->T))
        max_task = task;
      task = task->next;
    }
    if (max_task)
      kaapi_list_remove(rd, max_task);
    return max_task;
  }
#endif
  return kaapi_list_pop_head( rd );
}
#endif


/* Return a task marked as stolen or 0
   Pop from head
*/
kaapi_task_t* kaapi_queue_pop_head( kaapi_queue_t* q )
{
  kaapi_task_t* task;
#if defined(OLD_QUEUE)
  task = kaapi_list_pop_head_prio(&q->hq);
  if (task ==0) task = kaapi_list_pop_head_prio(&q->q);
  if (task ==0) return 0;
#else
  uint64_t bitmap[4];
  bitmap[0] = q->bitmap[0];
  bitmap[1] = q->bitmap[1];
  bitmap[2] = q->bitmap[2];
  bitmap[3] = q->bitmap[3];

redo:
  if (!_kaapi_bitmap_empty(bitmap))
  {
    int idx = _kaapi_bitmap_ffl( bitmap ) -1;
    task = kaapi_list_pop_head( &(*q->q)[idx] );
    if (task ==0)
    {
      _kaapi_bitmap_clear(idx, bitmap);
      _kaapi_bitmap_clear(idx, q->bitmap);
      goto redo;
    }
    if (kaapi_list_empty( &(*q->q)[idx] ))
      _kaapi_bitmap_clear(idx, q->bitmap);
    //printf("%p:: pop task %p, prio: %i\n", q, task, idx );
  }
  else
  {
#if defined(KAAPI_DEBUG)
    kaapi_assert( _kaapi_bitmap_empty(q->bitmap ) );
    kaapi_assert( q->size == 0);
#endif
    return 0;
  }
#endif

  --q->size;
  kaapi_assert_debug(q->size >=0);
#if !defined(OLD_QUEUE) && defined(KAAPI_DEBUG)
  if (q->size == 0)
  { kaapi_assert( _kaapi_bitmap_empty(q->bitmap ) );  } 
  else
  { kaapi_assert( !_kaapi_bitmap_empty(q->bitmap ) );  }
#endif
  return task;
}


/*
*/
int kaapi_task_commit(kaapi_thread_t* thread)
{
#if defined(KAAPI_DEBUG)
  kaapi_stack_bloc_t* bloc __attribute__((unused))
    = _kaapi_start_blocaddr(thread->sp, kaapi_stack_bloc_t*);
  kaapi_stack_bloc_t* endbloc __attribute__((unused))
    = _kaapi_end_blocaddr(thread->sp, kaapi_stack_bloc_t*);
#endif
  kaapi_stack_t* stack = kaapi_thread2stack(thread);
  kaapi_frame_t* frame = stack->unlink;
  ++frame->cnt_task;
  _kaapi_task_commit( thread );
#if defined (KAAPI_USE_PERFCOUNTER)
  KAAPI_PERFCTR_INCR(kaapi_thread2perfproc(thread), KAAPI_PERF_ID_TASKSPAWN, 1);
#endif
  return 0;
}

/*
*/
int kaapi_task_push_withstate(kaapi_thread_t* thread, kaapi_task_body_t body, void* arg, uint8_t state)
{
  kaapi_task_t* task = thread->sp;
  if (!_kaapi_has_enough_taskspace(thread))
    task = kaapi_thread_slow_pushtask(thread);
  task->body      = body;
  if (state == KAAPI_TASK_STATE_EXEC)
    task->state.steal     = 0;
  else if (state == KAAPI_TASK_STATE_STEAL)
    task->state.steal     = kaapi_slowexec_body;
  task->arg       = arg;
  task->u.dummy   = 0;
#if !defined(NDEBUG) && defined(KAAPI_DFG_ON_STEAL)
  task->next      = 0;
  KAAPI_ATOMIC_WRITE( &task->wc, 0);
#endif
  kaapi_task_commit(thread);
  return 0;
}


/*
*/
int kaapi_task_push_withld(
    kaapi_thread_t* thread,
    kaapi_task_body_t body,
    void* arg,
    kaapi_task_flag_t flag,
    int   ld
)
{
  kaapi_task_t* task = thread->sp;
#if defined(KAAPI_DEBUG)
  kaapi_task_t* first = _kaapi_start_blocaddr( task, kaapi_task_t*);
  kaapi_task_t* last  = _kaapi_end_blocaddr( task, kaapi_task_t*);
  kaapi_assert( first <= task );
  kaapi_assert( task < last );
#endif
  if (!_kaapi_has_enough_taskspace(thread))
  {
    task = kaapi_thread_slow_pushtask(thread);
#if defined(KAAPI_DEBUG)
    first = _kaapi_start_blocaddr( task, kaapi_task_t*);
    last  = _kaapi_end_blocaddr( task, kaapi_task_t*);
    kaapi_assert( first <= task );
    kaapi_assert( task < last );
#endif
  }

  task->body      = body;
  task->state.steal = body;
  task->arg       = arg;
  task->u.dummy   = 0;
  task->u.s.site  = ld;
  task->u.s.flag  = flag;
#if !defined(NDEBUG) && defined(KAAPI_LINKED_LIST)
  task->next      = 0;
  task->prev      = 0;
  KAAPI_ATOMIC_WRITE( &task->wc, 0);
#endif
  return kaapi_task_commit(thread);
}


/*
*/
void kaapi_nop_body(
  kaapi_task_t*   task,
  kaapi_thread_t* thread
)
{
}


/*
*/
void kaapi_taskmain_body( 
  kaapi_task_t* task,
  kaapi_thread_t* thread __attribute__((unused))
)

{
  kaapi_taskmain_arg_t* arg = (kaapi_taskmain_arg_t*)kaapi_task_getargs(task);
  arg->mainentry( arg->argc, arg->argv );
}


/**
*/
void kaapi_aftersteal_body( kaapi_task_t* task, kaapi_thread_t* thread )
{
  unsigned int          i;
  size_t                count_params;
  const kaapi_format_t* fmt;   /* format of the task */
  const kaapi_format_t* fmt_param;
  kaapi_access_t*       access_param;
  void*                 taskarg = kaapi_task_getargs(task);
  
  /* the task has been stolen: the body contains the original task body */
  fmt = _kaapi_task_getformat( task );
  kaapi_assert_debug( fmt !=0 );

  count_params = kaapi_format_get_count_params(fmt, task );

  /* for each access parameter, we have:
     - data that points to the original effective parameter
     - version that points to the data that should have been used during the execution of the stolen task
     If the access is a W (or CW) access then, version contains the newly produced data.
     The purpose here is to merge the version into the original data
        - if W mode -> copy + free of the data
        - if CW mode -> accumulation (data is the left side of the accumulation, version the right side)
  */
  for (i=0; i<count_params; ++i)
  {
    kaapi_access_mode_t mode = kaapi_format_get_mode_param(fmt, i, taskarg);
    kaapi_access_mode_t m = KAAPI_ACCESS_GET_MODE( mode ); /* forget extra flags */
    if (m & KAAPI_ACCESS_MODE_V)
      continue;

    if (KAAPI_ACCESS_IS_ONLYWRITE(m) || KAAPI_ACCESS_IS_CUMULWRITE(m) )
    {
      fmt_param    = kaapi_format_get_fmt_param(fmt, i, taskarg);
      access_param = kaapi_format_get_access_param(fmt, i, taskarg);

      /* if m == W || CW and data == version it means that data used by W was not copied due 
         to non WAR dependency or no CW access 
      */
      if (access_param->data != access_param->version )
      {
        /* add an assign + dstor function will avoid 2 calls to function, especially for basic types which do not
           required to be dstor.
        */
        if (!KAAPI_ACCESS_IS_CUMULWRITE(m))
        {
          kaapi_memory_view_t view_dest;
          kaapi_format_get_view_param(fmt, i, taskarg, &view_dest);
          kaapi_memory_view_t view_src;
          
          /* in case of WAR resolution, view_src is == to reallocation of view_src */
          view_src = view_dest;
          kaapi_memory_view_reallocated( &view_src );

          /* here we assume that if no assignment, then this is a pod type, so we use memcpy version for cpu2cpu
             If version is produced remotely... version should store the location...
          */
          if (fmt_param->assign ==0)
          {
            kaapi_pointer_t     ptr_dest, ptr_src;
            kaapi_void2pointer(&ptr_dest, access_param->data);
            kaapi_void2pointer(&ptr_src, access_param->version);
            kaapi_memory_copy(ptr_dest, &view_dest, ptr_src, &view_src, 0, 0, 0, 0);
          }
          else
            (*fmt_param->assign)( access_param->data, &view_dest, access_param->version, &view_src );
        }
        else {
          /* this is a CW mode : do reduction except if it is INPLACE */
          if ((mode & KAAPI_ACCESS_MODE_IP) ==0)
            kaapi_format_reduce_param( fmt, i, taskarg, access_param->version );
        }
        if ((mode & KAAPI_ACCESS_MODE_IP) ==0)
        {
          if (fmt_param->dstor !=0) (*fmt_param->dstor) ( access_param->version );
          free(access_param->version);
        }
      }
      access_param->version = 0;
    }
  }
}





/**
*/
void kaapi_taskwrite_body( 
  kaapi_task_t*   task,
  kaapi_thread_t* thread
)
{
  unsigned int i;
  size_t count_params;

  const kaapi_format_t* fmt;
  void*                 orig_task_args;
  kaapi_access_t*       access_param;

  kaapi_access_mode_t   mode;
  kaapi_access_mode_t   mode_param;
  const kaapi_format_t* fmt_param;
  unsigned int          war_param;     /* */
  unsigned int          cw_param;     /* */

  void*                 copy_task_args;
  void*                 copy_data_param;
  kaapi_access_t*       copy_access_param;

  kaapi_tasksteal_arg_t* arg = ((kaapi_taskwrite_arg_t*)kaapi_task_getargs(task))->stealarg;
  orig_task_args             = kaapi_task_getargs(arg->origin_task);
  copy_task_args             = arg->copy_task_args;
  war_param                  = arg->war_param;
  cw_param                   = arg->cw_param;

  if (arg->origin_task->u.s.flag & KAAPI_TASK_FLAG_DFGOK)
    kaapi_abort(__LINE__,__FILE__, "Invalid case");

  if (copy_task_args !=0)
  {
    /* for each parameter of the copy of the theft' task on mode:
       - V: we destroy the data
       - R,RW: do nothing
       - W,CW: set in field ->version of the original task args the field ->data of the copy args.
    */
    fmt         = arg->origin_task->fmt;
    kaapi_assert_debug(fmt !=0);

    count_params = kaapi_format_get_count_params( fmt, orig_task_args );
    for (i=0; i<count_params; ++i)
    {
      mode            = kaapi_format_get_mode_param(fmt, i, copy_task_args); 
      mode_param      = KAAPI_ACCESS_GET_MODE( mode ); 
      if (mode_param & KAAPI_ACCESS_MODE_V)
      {
        fmt_param       = kaapi_format_get_fmt_param(fmt, i, orig_task_args);
        copy_data_param = (void*)kaapi_format_get_data_param(fmt, i, copy_task_args);
        (*fmt_param->dstor)(copy_data_param);
        continue;
      }

      if ( KAAPI_ACCESS_IS_ONLYWRITE(mode_param) 
       || (KAAPI_ACCESS_IS_CUMULWRITE(mode_param) && ((mode & KAAPI_ACCESS_MODE_IP) ==0)))
      {
        access_param      = kaapi_format_get_access_param(fmt, i, orig_task_args);
        copy_access_param = kaapi_format_get_access_param(fmt, i, copy_task_args);

        /* write the value as the version */
        kaapi_assert_debug( access_param->version == 0);
        access_param->version = copy_access_param->data;
        kaapi_format_set_access_param(fmt, i, orig_task_args, access_param );
      }
      else if (KAAPI_ACCESS_IS_STACK(mode_param))
      { /* never merge result here */
        copy_access_param = kaapi_format_get_access_param(fmt, i, copy_task_args);
//Temporary delete free(copy_access_param.data);
        copy_access_param->data = 0;

        /* suppress war_param bit to avoid pushing a merge task */
        war_param &= ~(1<<i);
      }
    }
  }

  /* order write to the memory before changing the origin task' state */
  kaapi_writemem_barrier();

  /* signal the original task : mark it as executed, the old returned body should have steal flag */
  if ((war_param ==0) && (cw_param ==0))
    kaapi_task_markterm( arg->origin_task );
  else 
    kaapi_task_markaftersteal( arg->origin_task );
}


/* copy task argument 
*/
static void* __kaapi_cpy_taskarg(
  kaapi_thread_t* thread,
  const kaapi_format_t*  fmt,
  kaapi_tasksteal_arg_t* arg,
  unsigned int war_param,
  unsigned int cw_param
)
{
  /* the original task arguments */
  void*                  data_param;
  kaapi_access_t*        access_param;

  kaapi_access_mode_t    mode;
  kaapi_access_mode_t    mode_param;
  const kaapi_format_t*  fmt_param;

  void*                  copy_task_args;
  void*                  copy_data_param;
  kaapi_access_t*        copy_access_param;
  void* orig_task_args  = kaapi_task_getargs(arg->origin_task);
  size_t count_params    = kaapi_format_get_count_params(fmt, orig_task_args);
  unsigned int i;

  size_t task_size     = kaapi_format_get_size( fmt, orig_task_args );
  copy_task_args       = kaapi_data_push( thread, (uint32_t)task_size);
  arg->copy_task_args  = copy_task_args;

  kaapi_format_task_copy( fmt, copy_task_args, orig_task_args );

  for (i=0; i<count_params; ++i)
  {
    mode            = kaapi_format_get_mode_param(fmt, i, orig_task_args); 
    mode_param      = KAAPI_ACCESS_GET_MODE( mode ); 
    fmt_param       = kaapi_format_get_fmt_param(fmt, i, orig_task_args);
    
    if (mode_param & KAAPI_ACCESS_MODE_V)
    {
      data_param      = kaapi_format_get_data_param(fmt, i, orig_task_args);
      copy_data_param = kaapi_format_get_data_param(fmt, i, copy_task_args);
      (*fmt_param->cstorcopy)(copy_data_param, data_param);
      continue;
    }

    /* initialize all parameters ... */
    access_param              = kaapi_format_get_access_param(fmt, i, orig_task_args);
    copy_access_param         = kaapi_format_get_access_param(fmt, i, copy_task_args);
    copy_access_param->data    = access_param->data;
    copy_access_param->version = 0; /*access_param->version; not required... */

    KAAPI_DEBUG_INST(access_param->version = 0;)
    
    if (KAAPI_ACCESS_IS_STACK(mode_param) && (access_param->data !=0))
    {
      copy_access_param->data = 0;
      kaapi_memory_view_t view;
      kaapi_format_get_view_param(fmt, i, orig_task_args, &view);
      if (kaapi_memory_view_size(&view) < 1024 )
      { /* try to do stack allocation */
        copy_access_param->data = kaapi_data_push(thread, (int)kaapi_memory_view_size(&view));
      }
      if (copy_access_param->data ==0)
      {
#if defined(KAAPI_DEBUG)
        copy_access_param->data   = calloc(1, kaapi_memory_view_size(&view));
#else
        copy_access_param->data   = malloc(kaapi_memory_view_size(&view));
#endif
      }
      kaapi_assert_debug( copy_access_param->data != 0 );
      
      if (fmt_param->cstor !=0) 
        (*fmt_param->cstor)(copy_access_param->data);

      kaapi_memory_view_t view_dest = view;
      kaapi_memory_view_reallocated( &view_dest );
      (*fmt_param->assign)(copy_access_param->data, &view_dest, access_param->data, &view );
      
      /* set new data to copy with new view */
      kaapi_format_set_access_param(fmt, i, copy_task_args, copy_access_param );
      kaapi_format_set_view_param( fmt, i, copy_task_args, &view_dest );
    }
    /* allocate new data for the war or cw param or stack data, else points to the original data
       if mode is CW and Inplace flag is set, then do not copy data.
    */
    else if ( KAAPI_ACCESS_IS_ONLYWRITE(mode_param) 
          || (KAAPI_ACCESS_IS_CUMULWRITE(mode_param) && ((mode & KAAPI_ACCESS_MODE_IP) ==0)) 
    )
    {
      if (((war_param & (1<<i)) !=0) || ((cw_param & (1<<i)) !=0))
      { 
        kaapi_memory_view_t view;
        kaapi_format_get_view_param(fmt, i, orig_task_args, &view);
#if defined(KAAPI_DEBUG)
        copy_access_param->data   = calloc(1, kaapi_memory_view_size(&view));
#else
        copy_access_param->data   = malloc(kaapi_memory_view_size(&view));
#endif
        if (fmt_param->cstor !=0) 
          (*fmt_param->cstor)(copy_access_param->data);
        if (KAAPI_ACCESS_IS_STACK(mode_param))
        {
          kaapi_memory_view_t view_dest = view;
          kaapi_memory_view_reallocated( &view_dest );
          (*fmt_param->assign)(copy_access_param->data, &view_dest, access_param->data, &view );
        }
        else
          /* if cw: init with neutral element with respect to the reduction */
          if ((cw_param & (1<<i)) !=0)
            kaapi_format_redinit_neutral(fmt, i, copy_task_args, copy_access_param->data );
        
        /* set new data to copy with new view */
        kaapi_format_set_access_param(fmt, i, copy_task_args, copy_access_param );
        kaapi_memory_view_reallocated( &view );
        kaapi_format_set_view_param( fmt, i, copy_task_args, &view );
      }
    }
  }
  return copy_task_args;
}


/**
*/
void kaapi_tasksteal_body( kaapi_task_t* task, kaapi_thread_t* thread  )
{
  kaapi_taskwrite_arg_t* taskwrite;
  kaapi_tasksteal_arg_t* arg;
  kaapi_task_body_t      body;          /* format of the stolen task */
  const kaapi_format_t*  fmt;
  unsigned int           war_param;     /* */
  unsigned int           cw_param;      /* */

  void*                  orig_task_args;
  void*                  copy_task_args;

  /* get information of the task to execute */
  arg = (kaapi_tasksteal_arg_t*)kaapi_task_getargs(task);

  /* format of the original stolen task, resolved it if not yet set */  
  body            = arg->origin_task->body;
  fmt             = arg->origin_task->fmt;
  /* the original task arguments */
  orig_task_args  = kaapi_task_getargs(arg->origin_task);

  /* DFG & successors = activate tasks locally
     Startup if stolen task is DFG ok, it to initialize the readlist for the current frame.
  */
  if (arg->origin_task->u.s.flag & KAAPI_TASK_FLAG_DFGOK)
  {
    kaapi_stack_t* stack = (kaapi_stack_t*)thread;
    kaapi_frame_t* frame = stack->unlink;
    kaapi_frame_wrdlist_t* rdlist;

    if (fmt ==0)
      fmt = _kaapi_task_getformat(arg->origin_task);
    kaapi_assert_debug( fmt != 0);
    //Not true from KOMP task forkcall_wrapper  kaapi_assert_debug( body == fmt->entrypoint[KAAPI_PROC_TYPE_HOST] );

    kaapi_assert_debug( (frame->flag & KAAPI_FRAME_FLAG_MASK_STATE) == KAAPI_FRAME_FLAG_DFG_VOID);
    kaapi_assert_debug( frame->rdlist == 0);

#if 1
    frame->rdlist = rdlist = (kaapi_frame_wrdlist_t*)kaapi_alloca( thread, sizeof(kaapi_frame_wrdlist_t) + sizeof(kaapi_frame_dfgctxt_t));
    frame->flag |= KAAPI_FRAME_FLAG_STACKALLOC;
#else
    frame->rdlist = rdlist = (kaapi_frame_wrdlist_t*)malloc(sizeof(kaapi_frame_wrdlist_t) + sizeof(kaapi_frame_dfgctxt_t));
#endif
    kaapi_frame_rdlist_init(rdlist, (kaapi_frame_dfgctxt_t*)(rdlist+1));
    rdlist->master = arg->origin_frame_rd;

    /* task pushed not really used & execute : execution is through ready list, but thread could
       not be empty.
    */
    if (arg->origin_task->u.s.flag & KAAPI_TASK_FLAG_COPY_ON_STEAL)
    {
      copy_task_args = __kaapi_cpy_taskarg(
        thread,
        fmt,
        arg,
        0U,
        0U
      );
    }
    else
      copy_task_args = orig_task_args;

    /* push the task with not stolen flag */
    kaapi_task_push_withflags(
        thread,
        body,
        copy_task_args,
        arg->origin_task->u.s.flag | KAAPI_TASK_FLAG_UNSTEALABLE | KAAPI_TASK_FLAG_DFGOK
    );
    stack->pc->fmt         = fmt;
    stack->pc->state.frame = arg->origin_frame_rd;
    rdlist->starttask      = stack->pc;
    frame->flag           |= KAAPI_FRAME_FLAG_DFG_OK;
    ++frame->cnt_task;
#if defined (KAAPI_USE_PERFCOUNTER)
    /* TODO: similar translation if no dfg is built, 2 cases below */
    KAAPI_EVENT_PUSH2( kaapi_thread2rsrc(thread)->evtkproc, 0, KAAPI_EVT_TASK_STEAL, stack->pc, arg->origin_task );
#endif
    return;
  }

  /**/
  war_param = arg->war_param;
  cw_param  = arg->cw_param;

  /* not a bound task */
  arg->copy_task_args = 0;

  if (!war_param && !cw_param)
  {
    /* Execute the orinal body function with the original args.
    */
    body( arg->origin_task, thread );
  }
  else /* it exists at least one w parameter with war dependency or a cw_param: recopies the arguments */
  {
    copy_task_args = __kaapi_cpy_taskarg(
      thread,
      fmt,
      arg,
      war_param,
      cw_param
    );

    /* call directly the stolen body function */
    kaapi_task_t newtask;
    newtask.body  = body;
    newtask.state.steal = 0;
    newtask.arg   = copy_task_args;

    body( &newtask, thread);
  }

  /* push task that will be executed after all created tasks spawned
     by the user task 'body'
     - this task is unstealable because it does not has format
  */
  taskwrite = (kaapi_taskwrite_arg_t*)kaapi_data_push(thread, sizeof(kaapi_taskwrite_arg_t));
  taskwrite->stealarg = arg;
  kaapi_task_push( thread, kaapi_taskwrite_body, taskwrite);
}




/* format definition for any tasksync_arg_t */
static size_t
kaapi_tasksync_format_get_size(const struct kaapi_format_t* fmt, const void* sp)
{
  return sizeof(kaapi_tasksync_arg_t);
}

static void
kaapi_tasksync_format_task_copy(const struct kaapi_format_t* fmt, void* sp_dest, const void* sp_src)
{
  memcpy( sp_dest, sp_src, sizeof(kaapi_tasksync_arg_t) );
}

static
unsigned int kaapi_tasksync_format_get_count_params(const struct kaapi_format_t* fmt, const void* sp)
{
  kaapi_tasksync_arg_t* arg __attribute__((unused)) = (kaapi_tasksync_arg_t*)sp;
  return 1;
}

static
kaapi_access_mode_t kaapi_tasksync_format_get_mode_param(
  const struct kaapi_format_t* fmt, unsigned int i, const void* sp
)
{
//  kaapi_tasksync_arg_t* arg = (kaapi_tasksync_arg_t*)sp;
  return KAAPI_ACCESS_MODE_RW;
}

static
void* kaapi_tasksync_format_get_data_param(
  const struct kaapi_format_t* fmt, unsigned int i, const void* sp
)
{
  kaapi_tasksync_arg_t* arg = (kaapi_tasksync_arg_t*)sp;
  return arg->access.data;
}

static
kaapi_access_t* kaapi_tasksync_format_get_access_param(
  const struct kaapi_format_t* fmt, unsigned int i, const void* sp
)
{
  kaapi_tasksync_arg_t* arg = (kaapi_tasksync_arg_t*)sp;
  return &arg->access;
}

static
void kaapi_tasksync_format_set_access_param(
  const struct kaapi_format_t* fmt, unsigned int i, void* sp, const kaapi_access_t* a
)
{
  kaapi_tasksync_arg_t* arg = (kaapi_tasksync_arg_t*)sp;
  arg->access = *a;
}

static
const struct kaapi_format_t* kaapi_tasksync_format_get_fmt_param(
  const struct kaapi_format_t* fmt, unsigned int i, const void* sp
)
{
  return (kaapi_format_t*)kaapi_voidp_format;
}

static
void kaapi_tasksync_format_get_view_param(
  const struct kaapi_format_t* fmt, unsigned int i, const void* sp, kaapi_memory_view_t* view
)
{
  //kaapi_tasksync_arg_t* arg = (kaapi_tasksync_arg_t*)sp;
  kaapi_memory_view_make1d( view, 0, 1, sizeof(char) );
}

static
void kaapi_tasksync_format_set_view_param(
  const struct kaapi_format_t* fmt, unsigned int i, void* sp, const kaapi_memory_view_t* view
)
{
  //kaapi_tasksync_arg_t* arg = (kaapi_tasksync_arg_t*)sp;
}

static
void kaapi_tasksync_format_reducor(
  const struct kaapi_format_t* fmt, unsigned int i, void* sp, const void* v
)
{
  abort();
}

static
void kaapi_tasksync_format_redinit(
  const struct kaapi_format_t* fmt, unsigned int i, const void* sp, void* v
)
{
  abort();
}

static kaapi_adaptivetask_splitter_t	kaapi_tasksync_fnc_get_splitter(
  const struct kaapi_format_t* fmt, const void* sp
)
{
  return 0;
}


/*
*/
int kaapi_taskformat_init(void)
{
  kaapi_format_t* tmp = kaapi_format_allocate();
  kaapi_format_taskregister_func(
                                 tmp,
                                 (void*)kaapi_tasksync_body,
                                 kaapi_tasksync_body,
                                 "tasksync",
                                 0,
                                 kaapi_tasksync_format_get_size,
                                 kaapi_tasksync_format_task_copy,
                                 kaapi_tasksync_format_get_count_params,
                                 kaapi_tasksync_format_get_mode_param,
                                 kaapi_tasksync_format_get_data_param,
                                 kaapi_tasksync_format_get_access_param,
                                 kaapi_tasksync_format_set_access_param,
                                 kaapi_tasksync_format_get_fmt_param,
                                 kaapi_tasksync_format_get_view_param,
                                 kaapi_tasksync_format_set_view_param,
                                 kaapi_tasksync_format_reducor,
                                 kaapi_tasksync_format_redinit,
                                 kaapi_tasksync_fnc_get_splitter,
                                 0
                                 );
  kaapi_tasksync_fmt = tmp;
  return 0;
}

/*
*/
int kaapi_taskformat_finalize(void)
{
  kaapi_tasksync_fmt = 0;
  return 0;
}
