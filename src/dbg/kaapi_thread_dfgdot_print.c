/*
** kaapi_thread_print.c
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** fabien.lementec@imag.fr
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"
#include "kaapi_dbg.h"
#include <stdio.h>
#include <inttypes.h>


/* TODO: the noprint_data flag does not work.
   - the idea is to link together tasks without intermediate data node.
   - nothing was done.... if set, a set of independant tasks are outputed, which
   is not so convinient.
*/
static int noprint_versionlink = 0;
static int noprint_label = 0;
static int noprint_activationlink = 0;
static int noprint_label_info = 0;
static int noprint_data = 0;

/**/
static inline void _kaapi_print_data( FILE* file, const void* ptr, unsigned long version)
{
  if (noprint_label)
    fprintf(file,"%lu00%lu [label=\"\", shape=box, style=filled, color=steelblue];\n", 
        version, (uintptr_t)ptr
    );
  else
    fprintf(file,"%lu00%lu [label=\"%p v%lu\", shape=box, style=filled, color=steelblue];\n", 
        version, (uintptr_t)ptr, ptr,version-1 
    );
}

/**/
static inline void _kaapi_print_write_edge( 
    FILE* file, 
    const kaapi_task_t* task, 
    const void* ptr, 
    unsigned long version, 
    kaapi_access_mode_t m
)
{
  if (KAAPI_ACCESS_IS_READWRITE(m))
    fprintf(file,"%lu -> %lu00%lu[dir=both,arrowtail=diamond,arrowhead=vee];\n", 
        (uintptr_t)task, version, (uintptr_t)ptr );
  else if (KAAPI_ACCESS_IS_CUMULWRITE(m))
  { /* the final version will have tag version+1, not yet generated */
    fprintf(file,"%lu -> %lu00%lu[dir=both,arrowtail=inv,arrowhead=tee];\n", 
        (uintptr_t)task, version, (uintptr_t)ptr );
    return;
  } 
  else 
    fprintf(file,"%lu -> %lu00%lu;\n", (uintptr_t)task, version, (uintptr_t)ptr );

  if (!noprint_versionlink)
  {
    /* add version edge */
    if (version >1)
      fprintf(file,"%lu00%lu -> %lu00%lu [style=dotted];\n", 
          version-1, (uintptr_t)ptr, version, (uintptr_t)ptr );
  }

}

/**/
static inline void _kaapi_print_read_edge( 
    FILE* file, 
    const kaapi_task_t* task, 
    const void* ptr, 
    unsigned long version, 
    kaapi_access_mode_t m
)
{
  if (KAAPI_ACCESS_IS_READWRITE(m))
    fprintf(file,"%lu00%lu -> %lu[dir=both,arrowtail=diamond,arrowhead=vee];\n", version, (uintptr_t)ptr, (uintptr_t)task );
  else 
    fprintf(file,"%lu00%lu -> %lu;\n", version, (uintptr_t)ptr, (uintptr_t)task );
}


/** pair of pointer,int 
    Used to display tasklist
*/
typedef struct kaapi_pair_print_t {
  void*               ptr;
  uintptr_t           tag;
  kaapi_access_mode_t last_mode;
} kaapi_pair_print_t;


/*
*/
static void kaapi_frame_dot_activate_succ(
    kaapi_task_t*    task,
    kaapi_hashmap_t* task_khm,
    kaapi_hashmap_t* readytask_khm
)
{
  kaapi_access_mode_t   mode, mode_param;
  kaapi_access_t*       access;
  const kaapi_format_t* fmt;

  unsigned int          i;
  size_t                count_params;

  kaapi_assert_debug( task->u.s.flag & KAAPI_TASK_FLAG_DFGOK);

  fmt         = task->fmt;
  kaapi_assert_debug( fmt != 0);
  count_params = kaapi_format_get_count_params( fmt, task->arg );

  for (i=0; i<count_params; ++i)
  {
    mode            = kaapi_format_get_mode_param(fmt, i, task->arg);
    mode_param      = KAAPI_ACCESS_GET_MODE( mode ); 
    if (mode_param & KAAPI_ACCESS_MODE_V)
      continue;

    access          = kaapi_format_get_access_param(fmt, i, task->arg);    access = access->next_out;
    while (access !=0)
    {
      int wc;
      kaapi_task_t* next_task = access->task;
      kaapi_hashentries_t* entry = kaapi_hashmap_find(task_khm, next_task);
      if (entry ==0)
      {
        entry = kaapi_hashmap_findinsert(task_khm, next_task);
        kaapi_pair_print_t* pp = &KAAPI_HASHENTRIES_GET(entry, kaapi_pair_print_t);
        pp->tag = KAAPI_ATOMIC_READ(&next_task->wc);
      }
      kaapi_pair_print_t* pp = &KAAPI_HASHENTRIES_GET(entry, kaapi_pair_print_t);

      if (pp->tag >0)
      {
#if 0
        fprintf( stdout, "\t[activate???] task:%p name: \"%s\", wc=%i\n",
          (void*)next_task, (next_task->fmt ? next_task->fmt->name : "<undef>"), (int)pp->tag
        );
#endif

        wc = (int)--pp->tag;
        kaapi_assert_debug( wc >= 0 );
        if (wc ==0)
        {
          entry = kaapi_hashmap_insert(readytask_khm, next_task);
#if 0
          fprintf( stdout, "\t=>[activated] task:%p name: \"%s\", wc=%i\n",
            (void*)next_task, (next_task->fmt ? next_task->fmt->name : "<undef>"), (int)pp->tag
          );
#endif
        }
      }

      access = access->next;
    }
  }
}

/**/
static void _kaapi_frame_print_visit_task(
  FILE* file,
  kaapi_task_t* task,
  kaapi_hashmap_t* data_khm,
  kaapi_hashmap_t* task_khm,
  kaapi_hashmap_t* readytask_khm
)
{
  const char* fname = "<empty format>";
  const char* color = "orange";
  const char* shape = "ellipse";
  const kaapi_format_t* fmt = 0;
  kaapi_task_body_t body;

  void* sp = task->arg;
  body = task->body;
  fmt = _kaapi_task_getformat( task );

  if (fmt !=0)
  {
    if (fmt->name_dot != NULL )
      fname = fmt->name_dot;
    else
      fname = fmt->name;
    if (fmt->color_dot != NULL )
      color = fmt->color_dot;
    else
      if (task->u.s.flag & KAAPI_TASK_FLAG_PRIORITY)
        color = "red";
  }

  if (body == kaapi_taskmain_body)
  {
      fname = "maintask";
      shape = "doubleoctagon";
  }
  else if (body == kaapi_aftersteal_body)
  {
      fname = "aftersteal";
      shape = "doubleoctagon";
  }
  else if (body == kaapi_taskwrite_body)
  {
      fname = "taskwrite";
      shape = "doubleoctagon";
  }
  else if (body == kaapi_tasksteal_body)
  {
      fname = "tasksteal";
      shape = "doubleoctagon";
  }
  else if (body == kaapi_taskadapt_body)
  {
      fname = "taskadapt";
      shape = "doubleoctagon";
  }
  else if (body == kaapi_tasksignaladapt_body)
  {
      fname = "tasksignaladapt";
      shape = "doubleoctagon";
  }

  if (fmt ==0)
  {
    if (noprint_label)
      fprintf( file, "%lu [label=\"\", shape=%s, style=filled, color=%s];\n", 
        (uintptr_t)task, shape, color
      );
    else
      fprintf( file, "%lu [label=\"%s\\ntask=%p\\nsp=%p\", shape=%s, style=filled, color=%s];\n", 
        (uintptr_t)task, fname, (void*)task, task->arg, shape, color
      );
    return;
  }

  /* print the task */
  if (noprint_label)
    fprintf( file, "%lu [label=\"\", shape=%s, style=filled, color=%s];\n", 
      (uintptr_t)task, shape, color );
  else if( noprint_label_info )
    fprintf( file, "%lu [label=\"%s\", shape=%s, style=filled, color=%s];\n", 
      (uintptr_t)task, fname, shape, color
    );
  else
  {
#if 0
    fprintf( stdout, "[visit] task:%p name: \"%s\", wc=%i\n",
      (void*)task, fname, (int)KAAPI_ATOMIC_READ(&task->wc)
    );
#endif
    if (task->u.s.flag & KAAPI_TASK_FLAG_DFGOK)
      fprintf( file, "%lu [label=\"%s\\ntask=%p\\nsp=%p\\n"
               "wc=%i, T:%"PRIu32"\\nprio:%u, size=%i\", shape=%s, style=filled, color=%s];\n",
        (uintptr_t)task, fname, (void*)task, task->arg,
        (int)KAAPI_ATOMIC_READ(&task->wc),
        task->T,
        task->u.s.priority,
        (int)_kaapi_task_getsize(task, task->fmt),
        shape, color
      );
    else
      fprintf( file, "%lu [label=\"%s\\ntask=%p\\nsp=%p\","
               "shape=%s, style=filled, color=%s];\n",
        (uintptr_t)task, fname, (void*)task, task->arg,
        shape, color
      );
  }

  size_t count_params = kaapi_format_get_count_params(fmt, sp );
  kaapi_memory_view_t view;

  for (unsigned int i=0; i < count_params; i++) 
  {
    kaapi_access_mode_t m = kaapi_format_get_mode_param(fmt, i, sp);
    m = KAAPI_ACCESS_GET_MODE( m );
    if (m & KAAPI_ACCESS_MODE_V)
      continue;
    
    /* its an access */
    kaapi_access_t* access = kaapi_format_get_access_param(fmt, i, sp);

    /* next task if dfg */
    if ((task_khm !=0) && (access->next !=0))
    {
      kaapi_access_t* an = access->next;
      kaapi_hashentries_t* entry = kaapi_hashmap_find(task_khm, an->task);
      if (entry ==0)
      {
        entry = kaapi_hashmap_findinsert(task_khm, an->task);
        kaapi_pair_print_t* pp = &KAAPI_HASHENTRIES_GET(entry, kaapi_pair_print_t);
        pp->tag = KAAPI_ATOMIC_READ(&an->task->wc);
      }
    }

    kaapi_format_get_view_param(fmt, i, kaapi_task_getargs(task), &view);
    void* ptr = kaapi_memory_view2pointer(access->data, &view );

    /* find the version info of the data using the hash map */
    kaapi_hashentries_t* entry = kaapi_hashmap_findinsert(data_khm, ptr);
    kaapi_pair_print_t* pp = &KAAPI_HASHENTRIES_GET(entry, kaapi_pair_print_t);
    if (pp->tag ==0)
    {
      /* display the node */
      pp->tag = 1;
      if (!noprint_data)
        _kaapi_print_data( file, entry->key, (int)pp->tag );
    }
    
    /* display arrow */
    if (KAAPI_ACCESS_IS_READ(m) && !KAAPI_ACCESS_IS_WRITE(m))
    {
      pp->last_mode = KAAPI_ACCESS_MODE_R;
      if (!noprint_data)
        _kaapi_print_read_edge(
            file, 
            task, 
            entry->key, 
            pp->tag,
            m  
        );
    }
    if (KAAPI_ACCESS_IS_WRITE(m))
    {
      pp->tag++;
      pp->last_mode = KAAPI_ACCESS_MODE_W;
      /* display new version */
      if (!noprint_data)
      {
        _kaapi_print_data( file, entry->key, (int)pp->tag );
        _kaapi_print_write_edge( 
            file, 
            task, 
            entry->key, 
            pp->tag,
            m 
        );
      }
    }
    if (KAAPI_ACCESS_IS_CUMULWRITE(m))
    {
      if (!KAAPI_ACCESS_IS_CUMULWRITE(pp->last_mode))
      {
        pp->tag++;
        if (!noprint_data)
        {
          _kaapi_print_data( file, entry->key, (int)pp->tag );
          if (!noprint_versionlink)
          {
            /* add version edge */
            if (pp->tag >1)
              fprintf(file,"%lu00%lu -> %lu00%lu [style=dotted];\n", 
                  pp->tag-1, (uintptr_t)entry->key, pp->tag, (uintptr_t)entry->key );
          }
        }
      }
      pp->last_mode = KAAPI_ACCESS_MODE_CW;

      if (!noprint_data)
        _kaapi_print_write_edge(
            file, 
            task, 
            entry->key, 
            pp->tag, 
            m 
        );
    }
  }

  if (task_khm !=0)
    kaapi_frame_dot_activate_succ(task, task_khm, readytask_khm);
}

/**
*/
int kaapi_frame_print_dot  (
  FILE* file,
  const kaapi_stack_t* stack,
  const kaapi_frame_t* frame
)
{
  kaapi_hashmap_t          data_khm; /* to store task & data visited */
  kaapi_hashentries_t*     mapentries[1<<KAAPI_SIZE_DFGCTXT];
  kaapi_hashentries_bloc_t mapbloc;
  kaapi_task_t*            pc;
  kaapi_stack_iterator_t   iter;

  noprint_activationlink = (0 != getenv("KAAPI_DOT_NOACTIVATION_LINK"));
  noprint_versionlink    = (0 != getenv("KAAPI_DOT_NOVERSION_LINK"));
  noprint_data           = (0 != getenv("KAAPI_DOT_NODATA_LINK"));
  noprint_label          = (0 != getenv("KAAPI_DOT_NOLABEL"));
  noprint_label_info     = (0 != getenv("KAAPI_DOT_NOLABEL_INFO"));

  /* be carrefull, the map should be clear before used */
  kaapi_hashmap_init(&data_khm, mapentries, KAAPI_SIZE_DFGCTXT, &mapbloc);

#if 0
  if (clusterflags !=0)
  {
    char sec_name[128];
    sprintf(sec_name, "subgraph cluster_%lu {\n",(uintptr_t)tasklist);
    the_hash_map.sec_name = sec_name;
    fprintf(file, "%s\n", sec_name);
  }
  else
#endif
    fprintf(file, "digraph G {\n");

  if (frame->rdlist == 0)
  {
    kaapi_stack_iterator_init(&iter, (kaapi_stack_t*)stack, (kaapi_frame_t*)frame);

    while (!kaapi_stack_iterator_empty(&iter))
    {
      pc = kaapi_stack_iterator_get( &iter );
      _kaapi_frame_print_visit_task( file, pc, &data_khm, 0, 0);
      kaapi_stack_iterator_next( &iter );
    }
  }
  else
  {
    kaapi_hashmap_t          task_khm; /* to store task & data visited */
    kaapi_hashentries_t*     task_mapentries[1<<KAAPI_SIZE_DFGCTXT];
    kaapi_hashentries_bloc_t task_mapbloc;

    kaapi_hashmap_t          readytask_khm; /* to store task & data visited */
    kaapi_hashentries_t*     readytask_mapentries[1<<KAAPI_SIZE_DFGCTXT];
    kaapi_hashentries_bloc_t readytask_mapbloc;

    /* be carrefull, the map should be clear before used */
    kaapi_hashmap_init(&task_khm, task_mapentries, KAAPI_SIZE_DFGCTXT, &task_mapbloc);
    kaapi_hashmap_init(&readytask_khm, readytask_mapentries, KAAPI_SIZE_DFGCTXT, &readytask_mapbloc);

    kaapi_list_iterator_t iter;
    kaapi_list_iterator_init( &frame->rdlist->rd, &iter );
    while (!kaapi_list_iterator_empty(&iter))
    {
      kaapi_task_t* task = kaapi_list_iterator_get( &iter );
      _kaapi_frame_print_visit_task( file, task, &data_khm, &task_khm, &readytask_khm);
      kaapi_list_iterator_next( &iter );
    }

    kaapi_hashentries_t* entry;
    int newttask;
  redo_print:
    newttask = 0;
    for (int i=0; i<readytask_khm.size; ++i)
    {
      while ((entry = _pop_hashmap_entry(&readytask_khm, i)) != 0)
      {
        newttask = 1;
        _kaapi_frame_print_visit_task( file, (kaapi_task_t*)entry->key, &data_khm, &task_khm, &readytask_khm);
      }
    }
    if (newttask) goto redo_print;

    kaapi_hashmap_destroy(&task_khm);
    kaapi_hashmap_destroy(&readytask_khm);
  }
  fprintf(file, "\n\n}\n");
  fflush(file);

  kaapi_hashmap_destroy(&data_khm);
  return 0;
}


/*
*/
int kaapi_stack_print_dot( FILE* file, const kaapi_stack_t* stack )
{
  kaapi_frame_t* frame;

  if (stack ==0) return 0;

  fprintf(file,"Stack @:%p\n", (void*)stack );

  frame    = kaapi_stack_topframe(stack);
  if (frame ==0) return 0;

  do /* loop over frame */
  {
    kaapi_frame_print_dot( file, stack, frame );
    frame = frame->next;
  } while (frame != 0);

  fflush(file);
  return 0;
}


/** 
*/
int kaapi_context_print_dot  ( FILE* file, const kaapi_context_t* context )
{
  if (context ==0) return 0;

  fprintf(file,"Thread @:%p\n", (void*)context );
  fprintf(file,"  proc  : @%p, kid: %i\n", 
          (void*)context->proc,
          (context->proc != 0 ? (int)context->proc->rsrc.kid : -1));
  fprintf(file,"\n" );
  kaapi_stack_print_dot( file, &context->stack );
  return 0;
}


int kaapi_context_print_dot_current(FILE* file )
{
  return kaapi_context_print_dot(file, kaapi_self_processor()->context);
}



