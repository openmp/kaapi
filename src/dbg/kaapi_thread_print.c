/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"
#include "kaapi_dbg.h"
#include <stdlib.h>
#include <inttypes.h>

/** Bits are SEAT
*/
static const char* tab_bit[] __attribute__((unused)) = {
  "0000",
  "0001",
  "0010",
  "0011",
  "0100",
  "0101",
  "0110",
  "0111",
  "1000",
  "1001",
  "1010",
  "111",
  "1100",
  "1101",
  "1110",
  "1111"
};

/*
*/
static int kaapi_task_getstate(const struct kaapi_task_t* task)
{
  kaapi_task_body_t body = task->state.steal;
  if (body == task->body) return 0;
  if ((body == kaapi_nop_body) || (body == 0))
    return KAAPI_TASK_STATE_TERM;
  if (body == kaapi_aftersteal_body)
    return KAAPI_TASK_STATE_MERGE;
  if (body == kaapi_slowexec_body)
    return KAAPI_TASK_STATE_STEAL;
  if (body == 0)
    return KAAPI_TASK_STATE_EXEC;
  return 0;
}

/*
 * E -> execution
 * S -> steal
 * _ -> nop 
 * A -> after steal op
 * T -> term
 * X -> term after steal
 * t -> scratch zone
 */
typedef char state_type_t[4];
static void kaapi_getstatename( const kaapi_task_t* task, state_type_t char_state )
{
  int state = kaapi_task_getstate(task);
  char_state[0] = (state & KAAPI_TASK_STATE_TERM ? 'T' : '_');
  char_state[1] = (state & KAAPI_TASK_STATE_MERGE ? 'A' : '_');
  char_state[2] = (state & KAAPI_TASK_STATE_EXEC ? 'E' : '_');
  char_state[3] = (state & KAAPI_TASK_STATE_STEAL ? 'S' : '_');
}


/**
*/
int kaapi_task_print( 
  FILE* file,
  const kaapi_task_t* task
)
{
  const kaapi_format_t* fmt;
  unsigned int i;
  size_t count_params;
  state_type_t state;

  fmt = _kaapi_task_getformat(task);
  
  void* sp = kaapi_task_getargs(task);
  if (fmt ==0) return 0;

  count_params = kaapi_format_get_count_params(fmt, sp );
  kaapi_getstatename(task, state);

  if (task->u.s.flag & KAAPI_TASK_FLAG_DFGOK)
  {
    fprintf( file, "@%p |%c%c%c%c|, name:%-40.40s, #p:%u, arg:%p, wc: %i\n",
          (void*)task, 
          state[3], state[2], state[1], state[0],
          fmt->name, 
          (unsigned int)count_params,
          sp,
          KAAPI_ATOMIC_READ(&task->wc)
    );
  }
  else
  {
    fprintf( file, "@%p |%c%c%c%c|, name:%-40.40s, #p:%u, arg:%p\n",
          (void*)task, 
          state[3], state[2], state[1], state[0],
          fmt->name, 
          (unsigned int)count_params,
          sp
    );
  }

  /* access mode */
  if (count_params >0)
  {
    for (i=0; i<count_params; ++i)
    {
      const kaapi_format_t* fmt_param = kaapi_format_get_fmt_param(fmt, i, sp );
      kaapi_access_mode_t m = KAAPI_ACCESS_GET_MODE( kaapi_format_get_mode_param(fmt, i, sp));
      fprintf( file, "\t\t\t [%u]", i );
      fputc(kaapi_getmodename(m), file );
      fputs("", file );
      if (m & KAAPI_ACCESS_MODE_V)
      {
        void* data = kaapi_format_get_data_param(fmt, i, sp );
        fprintf(file, "<%s>, @:%p=", fmt_param->name, data );
//        (*fmt_param->print)(file, data );
      }
      else if (m == KAAPI_ACCESS_MODE_S)
      {
      }
      else if (m == KAAPI_ACCESS_MODE_T)
      {
      }
      else 
      {
        kaapi_access_t* access = kaapi_format_get_access_param(fmt, i, sp );
        if (task->u.s.flag & KAAPI_TASK_FLAG_DFGOK)
          fprintf(file, "<%s>, access:%p, next:%p, next_out:%p, data@:%p, value=", fmt_param->name, (void*)access, (void*)access->next, (void*)access->next_out, (void*)access->data);
        else
          fprintf(file, "<%s>, access:%p data@:%p, value=", fmt_param->name, (void*)access, (void*)access->data);

//        (*fmt_param->print)(file, access->data );
        if (access->version !=0)
        {
          fprintf(file, ", ver:%p value=", access->version );
//          (*fmt_param->print)(file, access.version );
        }
      }
      if (i < count_params-1)
      {
        fputs("\n", file );
      }
    }
  }

  fputc('\n', file );

  fflush(file);
  return 0;
}



/*
*/
int kaapi_stack_print( FILE* file, const kaapi_stack_t* stack )
{
  kaapi_frame_t* frame;
  kaapi_task_t*  task;
  kaapi_task_t*  pc;
  kaapi_task_t*  sp;
  void*          sp_data;
  kaapi_task_body_t body;
  const kaapi_format_t* fmt;
  int count, iframe;
  char prompt;

  if (stack ==0) return 0;

  fprintf(file,"Stack @:%p\n", (void*)stack );

  count = 0;

  frame    = kaapi_stack_topframe(stack);
  if (frame ==0) return 0;
  iframe   = 0;

  do /* loop over frame */
  {
    if (frame->flag & KAAPI_FRAME_FLAG_DFG_OK)
      fprintf(file, "%i: --------frame: @:%p  :: pc:%p, sp:%p, spd:%p, type: '%s', t_infinity:%"PRIi64"\n",
          iframe, (void*)frame, (void*)pc, (void*)sp, (void*)sp_data,
          "DFG",
          frame->rdlist->t_infinity
      );
    else
      fprintf(file, "%i: --------frame: @:%p  :: pc:%p, sp:%p, spd:%p type: '%s'\n",
          iframe, (void*)frame, (void*)pc, (void*)sp, (void*)sp_data,
          "DFG"
      );

    kaapi_stack_iterator_t   iter;
    kaapi_stack_iterator_init(&iter, (kaapi_stack_t*)stack, (kaapi_frame_t*)frame);

    while (!kaapi_stack_iterator_empty(&iter))
    {
      task = kaapi_stack_iterator_get( &iter );
      body = task->body;
      fmt = _kaapi_task_getformat( task );
      
      if (fmt ==0) 
      {
        const char* fname = "<empty format>";
        if ( body == kaapi_taskmain_body) 
          fname = "maintask";
        else if (body == kaapi_tasksteal_body) 
          fname = "steal";
        else if (body == (kaapi_task_body_t)kaapi_aftersteal_body) 
          fname = "aftersteal";

        state_type_t state;
        kaapi_getstatename(task, state);
        if (task == pc) prompt ='>';
        else prompt = ' ';
        fprintf( file, "%c [%04i]: @%p |%c%c%c%c|, name:%-20.20s", 
              prompt,
              count, 
              (void*)task,
              state[3], state[2], state[1], state[0],
              fname
        );
        
        if (body == kaapi_tasksteal_body)
        {
          kaapi_tasksteal_arg_t* arg = kaapi_task_getargst( task, kaapi_tasksteal_arg_t );
          fprintf(file, ", thief task:" );
          kaapi_task_print(file, arg->origin_task );
        }
        else if (body == (kaapi_task_body_t)kaapi_aftersteal_body)
        {
          fprintf(file, ", steal/term task:" );
          kaapi_task_print(file, task );
        }
        fputc('\n', file);
      }
      else 
      {        
        /* print the task */
        if (task == pc) prompt ='>';
        else prompt = ' ';
        fprintf( file, "%c [%04i]: ", prompt, count );
        kaapi_task_print(file, task );
      }
      ++count;
      kaapi_stack_iterator_next( &iter );
    }

    frame = frame->next;
    ++iframe;
  } while (frame != 0);

  fflush(file);
  return 0;
}


int kaapi_stack_printreverse( FILE* file, const kaapi_stack_t* stack )
{
  kaapi_frame_t* frame;
  kaapi_task_t*  task;
  kaapi_task_t*  pc;
  kaapi_task_t*  sp;
  void*          sp_data;
  kaapi_task_body_t body;
  const kaapi_format_t* fmt;
  int count, iframe;
  char prompt;

  if (stack ==0) return 0;

  fprintf(file,"Stack @:%p\n", (void*)stack );

  count = 0;

  frame    = kaapi_stack_topframe(stack);
  if (frame ==0) return 0;
  iframe   = 0;

  do /* loop over frame */
  {
    if (frame->flag & KAAPI_FRAME_FLAG_DFG_OK)
      fprintf(file, "%i: --------frame: @:%p  :: pc:%p, sp:%p, spd:%p, type: '%s', t_infinity:%"PRIi64"\n",
          iframe, (void*)frame, (void*)pc, (void*)sp, (void*)sp_data,
          "DFG",
          frame->rdlist->t_infinity
      );
    else
      fprintf(file, "%i: --------frame: @:%p  :: pc:%p, sp:%p, spd:%p type: '%s'\n",
          iframe, (void*)frame, (void*)pc, (void*)sp, (void*)sp_data,
          "DFG"
      );

    kaapi_stack_iterator_t   iter;
    kaapi_stack_reverse_iterator_init(&iter, (kaapi_stack_t*)stack, (kaapi_frame_t*)frame);

    while (!kaapi_stack_iterator_empty(&iter))
    {
      task = kaapi_stack_iterator_get( &iter );
      body = task->body;
      fmt = _kaapi_task_getformat( task );
      
      if (fmt ==0) 
      {
        const char* fname = "<empty format>";
        if ( body == kaapi_taskmain_body) 
          fname = "maintask";
        else if (body == kaapi_tasksteal_body) 
          fname = "steal";
        else if (body == (kaapi_task_body_t)kaapi_aftersteal_body) 
          fname = "aftersteal";

        state_type_t state;
        kaapi_getstatename(task, state);
        if (task == pc) prompt ='>';
        else prompt = ' ';
        fprintf( file, "%c [%04i]: @%p |%c%c%c%c|, name:%-20.20s", 
              prompt,
              count, 
              (void*)task,
              state[3], state[2], state[1], state[0],
              fname
        );
        
        if (body == kaapi_tasksteal_body)
        {
          kaapi_tasksteal_arg_t* arg = kaapi_task_getargst( task, kaapi_tasksteal_arg_t );
          fprintf(file, ", thief task:" );
          kaapi_task_print(file, arg->origin_task );
        }
        else if (body == (kaapi_task_body_t)kaapi_aftersteal_body)
        {
          fprintf(file, ", steal/term task:" );
          kaapi_task_print(file, task );
        }
        fputc('\n', file);
      }
      else 
      {        
        /* print the task */
        if (task == pc) prompt ='>';
        else prompt = ' ';
        fprintf( file, "%c [%04i]: ", prompt, count );
        kaapi_task_print(file, task );
      }
      ++count;
      kaapi_stack_reverse_iterator_next( &iter );
    }

    frame = frame->next;
    ++iframe;
  } while (frame != 0);

  fflush(file);
  return 0;
}


/** 
*/
int kaapi_context_print ( FILE* file, const kaapi_context_t* thread )
{
  if (thread ==0) return 0;

  fprintf(file,"Thread @:%p\n", (void*)thread );
  fprintf(file,"  proc  : @%p, kid: %i\n", 
          (void*)thread->proc, 
          (thread->proc != 0 ? (int)thread->proc->rsrc.kid : -1));
  fprintf(file,"  asid  : " );
  fprintf(file,"\n" );
  kaapi_stack_print( file, &thread->stack );
  return 0;
}


int kaapi_context_print_current(FILE* file )
{
  return kaapi_context_print(file, kaapi_self_processor()->context);
}

/** 
*/
int kaapi_context_printreverse ( FILE* file, const kaapi_context_t* thread )
{
  if (thread ==0) return 0;

  fprintf(file,"Thread @:%p\n", (void*)thread );
  fprintf(file,"  proc  : @%p, kid: %i\n", 
          (void*)thread->proc, 
          (thread->proc != 0 ? (int)thread->proc->rsrc.kid : -1));
  fprintf(file,"  asid  : " );
  fprintf(file,"\n" );
  kaapi_stack_printreverse( file, &thread->stack );
  return 0;
}


int kaapi_context_printreverse_current(FILE* file )
{
  return kaapi_context_printreverse(file, kaapi_self_processor()->context);
}


static kaapi_lock_t lock_print = KAAPI_LOCK_INITIALIZER;

void kaapi_dbg_print_lock(void)
{
  kaapi_atomic_lock( &lock_print);
  fflush(stderr);
}

void kaapi_dbg_print_unlock(void)
{
  fflush(stdout);
  kaapi_atomic_unlock( &lock_print);
}
