/*
 ** xkaapi
 ** 
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threadctxts.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include "kaapi_impl.h"
#include <inttypes.h>
#include "kaapi_dbg.h"

static void kaapi_print_pad(FILE* file, int pad)
{
  for (int i=0; i<pad; ++i)
    fputc(' ', file);
}

/**
*/
static int kaapi_task_descriptor_print( FILE* file, int pad, kaapi_task_t* task )
{
  const char* name = 0;
  const kaapi_format_t* fmt;
  kaapi_task_body_t body;

  body = task->body;
  fmt = _kaapi_task_getformat( task );
  if (fmt !=0) 
    name = fmt->name;
  else {
    if (body == kaapi_nop_body)
      name = "nop";
    else if (body == kaapi_aftersteal_body)
      name = "aftersteal";
    else if (body == kaapi_slowexec_body)
      name = "slowbody";
    else if (body == kaapi_tasksteal_body)
      name = "stealbody";
    else if (body == kaapi_taskwrite_body)
      name = "writebody";
    else if (body == kaapi_taskadapt_body)
      name = "adaptive";
    else if (body == kaapi_tasksignaladapt_body)
      name = "tasksignaladapt";
    else
      name = "<undef>";
  }
    

  kaapi_print_pad(file, pad);
  fprintf(file, "task:%p  wc:%i, priority:%i, name:%s\n",
    (void*)task, KAAPI_ATOMIC_READ(&task->wc), task->u.s.priority, name
  );

  if (fmt ==0)
    return EACCES;

  return 0;
}



static void _kaapi_visit_task(
  FILE*            file,
  kaapi_task_t*    task,
  kaapi_hashmap_t* visited_task,
  kaapi_hashmap_t* notvisited_task
)
{
  /* first time I visit it: print and insert activated task descr into the hashmap */
  if (0 == kaapi_task_descriptor_print(file, 0, task))
  {
    unsigned int count_param = kaapi_format_get_count_params(task->fmt, task->arg);
    for (unsigned int i=0; i<count_param; ++i)
    {
      kaapi_access_mode_t mode = KAAPI_ACCESS_GET_MODE(kaapi_format_get_mode_param(task->fmt, i, task->arg));
      if (mode & KAAPI_ACCESS_MODE_V)
        continue;

      kaapi_access_t* a = kaapi_format_get_access_param(task->fmt, i, task->arg);

      if (a->next !=0)
      {
        kaapi_access_t* an = a->next;
        fprintf(file, "\tactivate/access [%c, @:%p, #%u]: ", kaapi_getmodename(mode), a->data, i);

#if 0
        while ((an !=0) && (kaapi_hashmap_find(visited_task, an) ==0))
        {
          kaapi_access_mode_t nmode = an->mode;
          fprintf(file, "(task: %p, @:%p, wc: %i) ", (void*)an->task, an->data, KAAPI_ATOMIC_READ(&an->task->wc));

          kaapi_hashentries_t* entry = kaapi_hashmap_find(visited_task, an->task);
          if (entry ==0)
          {
            entry = kaapi_hashmap_insert(visited_task, an->task);
            entry->u.data.tag = KAAPI_ATOMIC_READ( &an->task->wc ); /* task inserted */
            entry->u.data.ptr = an->task;
          }
          if (--entry->u.data.tag <=0)
          {
            entry = kaapi_hashmap_insert(notvisited_task, an->task);
            entry->u.data.tag = KAAPI_ATOMIC_READ( &an->task->wc ); /* task inserted */
            kaapi_assert(entry->u.data.tag>0);
            entry->u.data.ptr = an->task;
//printf("insert: task: %p\n", an->task);
          }

          if (!KAAPI_ACCESS_IS_CONCURRENT(mode, nmode))
            break;

          a = an;
          an = a->next;
        }
#endif
        fprintf(file, "\n");
      }
    }
  }
}

/** pair of pointer,int 
    Used to display tasklist
*/
typedef struct kaapi_pair_print_t {
  void*               ptr;
  uintptr_t           tag;
  kaapi_access_mode_t last_mode;
} kaapi_pair_print_t;


/**
*/
int kaapi_frame_tasklist_print( FILE* file, const kaapi_frame_wrdlist_t* rdlist )
{
  kaapi_hashmap_t notvisited_task;
  kaapi_hashentries_t*           mapentries[1<<KAAPI_SIZE_DFGCTXT];
  kaapi_hashentries_bloc_t       mapbloc;

  kaapi_hashmap_t visited_task;
  kaapi_hashentries_t*           mapentries2[1<<KAAPI_SIZE_DFGCTXT];
  kaapi_hashentries_bloc_t       mapbloc2;

  /* be carrefull, the map should be clear before used */
  kaapi_hashmap_init( &notvisited_task, mapentries, KAAPI_SIZE_DFGCTXT, &mapbloc);
  kaapi_hashmap_init( &visited_task, mapentries2, KAAPI_SIZE_DFGCTXT, &mapbloc2);

  fprintf(file, "\n*** ready task:\n");
  fprintf(file, "\tcnt_task:%u\n", (unsigned int)rdlist->cnt_task);


  kaapi_list_iterator_t iter;
  kaapi_list_iterator_init( (kaapi_list_t*)&rdlist->rd, &iter );
  while (!kaapi_list_iterator_empty(&iter))
  {
    kaapi_task_t* task = kaapi_list_iterator_get( &iter );
    _kaapi_visit_task( file, task, &visited_task, &notvisited_task);
    kaapi_list_iterator_next( &iter );
  }

  /* now print all non printed task descriptor */
  fprintf(file, "*** non ready task:\n");
  int newtask;
redo_print:
  newtask = 0;
  for (int i=0; i<notvisited_task.size; ++i)
  {
    kaapi_hashentries_t* entry;
    while ((entry = _pop_hashmap_entry(&notvisited_task, i)) != 0)
    {
      if (1)//entry->u.data.tag ==0)
      {
        newtask = 1;
        kaapi_pair_print_t* pp = KAAPI_HASHENTRIES_GET(entry, kaapi_pair_print_t*);

        _kaapi_visit_task(file, (kaapi_task_t*)pp->ptr, &visited_task, &notvisited_task);
      }
    }
  }
  if (newtask) 
    goto redo_print;

  return 0;
}


/**
*/
int kaapi_frame_tasklist_print_current(FILE* file )
{
  return kaapi_frame_tasklist_print(file, kaapi_self_processor()->context->stack.unlink->rdlist);
}

