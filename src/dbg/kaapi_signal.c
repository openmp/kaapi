/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/

#include "kaapi_impl.h"
#include "kaapi_dbg.h"
#if defined(KAAPI_DEBUG)

#if defined(__APPLE__)
#  if (__ENVIRONMENT_MAC_OS_X_VERSION_MIN_REQUIRED__ >= 1040)
#    include <execinfo.h>
#  else
#    define KAAPI_NOT_USE_EXECINFO 1
#  endif
#else 
#  include <execinfo.h>
#endif
#include <signal.h>
#include <unistd.h>


// --------------------------------------------------------------------
void _kaapi_signal_dump_backtrace(int sig, siginfo_t *si, void *unused)
{
#if defined(__APPLE__) || defined(__linux__)
  enum {KAAPI_STACK_MAX_DEPTH = 100};
  void *trace[KAAPI_STACK_MAX_DEPTH];
  int trace_size;
  char **trace_strings;
  
  if (sig == SIGSEGV)
  {
    printf("Catch SIGSEGV, address: %p\n", si->si_addr );
  }

  trace_size = backtrace(trace, KAAPI_STACK_MAX_DEPTH);
  trace_strings = backtrace_symbols(trace, trace_size);
  for (int i=0; i<trace_size; ++i)
  { 
    printf("[%i]: %s\n", i,  trace_strings[i] );
  }
  free(trace_strings); // malloc()ed by backtrace_symbols
#endif
#if defined(KAAPI_USE_PERFCOUNTER)
  _kaapi_signal_dump_counters(sig);  
#endif
  _exit(sig);
}


/* Kaapi signal handler to dump the state of the all the kprocessor
*/
void _kaapi_signal_dump_state(int sig)
{
  int i;
  /* block alarm, if period was to short... */
  sigset_t block_alarm;
  sigemptyset (&block_alarm);
  sigaddset (&block_alarm, SIGALRM);
  pthread_sigmask( SIG_BLOCK, &block_alarm, 0 );
 
  
  printf("\n\n>>>>>>>>>>>>>>>> DUMP PROCESSORS STATE\n");
  printf("                  Date: %f (s)\n", (double)kaapi_get_elapsedns_since_start()*1e-6 );
  for (i=0; i< kaapi_global_team->count; ++i)
  {
    kaapi_processor_t* kproc = kaapi_global_team->all_kprocessors[i];
    
    printf("\n\n*****Kprocessor kid: %i\n", i);
    printf("Proc type       : %s\n", (kproc->rsrc.proc_type == KAAPI_PROC_TYPE_CPU ? "CPU" : "GPU") );
    printf("Current thread  : %p\n", (void*)kproc->context);
    printf("\n");
    fflush(stdout);

    fflush(stdout);

    printf("SuspendList     : %s\n", (kaapi_list_empty(&kproc->lsuspend) ? "no" : "yes") );
    fflush(stdout);

    printf("\n**** Thread(s):\n");
    /* dump each thread */
    if (kproc->context !=0)
    {
      kaapi_context_print(stdout, kproc->context);
      fflush(stdout);
    }
        
    if (!kaapi_list_empty( &kproc->lsuspend ))
    {
      kaapi_list_iterator_t iter;
      kaapi_list_iterator_init( &kproc->lsuspend, &iter);
      while (!kaapi_list_iterator_empty(&iter))
      {
        kaapi_task_t* cell = kaapi_list_iterator_get(&iter);
        kaapi_context_t* thread = (kaapi_context_t*)cell->arg;
        if (thread !=0)
        {
          printf("Suspended thread:%p -> @task condition:%p\n",
            (void*)thread,
            (void*)thread->stack.pc
          );
          kaapi_context_print(stdout, thread);
        }
        kaapi_list_iterator_next(&iter);
      }
      fflush(stdout);
    }
  }
  printf("<<<<<<<<<<<<<<<<< END DUMP\n\n\n");
  fflush(stdout);

  pthread_sigmask( SIG_UNBLOCK, &block_alarm, 0 );
  alarm( kaapi_default_param.alarmperiod );
}

#endif
