/*
* kaapi.h
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_H
#define _KAAPI_H 1

#define KAAPI_LINKED_LIST 1

/*!
    @header kaapi
    @abstract   This is the public header for XKaapi
    @discussion XKaapi
*/
#define KAAPI_H _KAAPI_H

/* .0: initial release
   .1: with merge of static part and new libkomp for supporting libgomp and libiomp5
   .2: next release
*/
#define __KAAPI__ 3
#define __KAAPI_MINOR__ 1

#if !defined(__SIZEOF_POINTER__)
#  if defined(__LP64__) || defined(__x86_64__)
#    define __SIZEOF_POINTER__ 8
#  elif defined(__i386__) || defined(__ppc__)
#    define __SIZEOF_POINTER__ 4
#  else
#    error KAAPI needs __SIZEOF_* macros. Use a recent version of gcc
#  endif
#endif

#if ((__SIZEOF_POINTER__ != 4) && (__SIZEOF_POINTER__ != 8)) 
#  error KAAPI cannot be compiled on this architecture due to strange size for __SIZEOF_POINTER__
#endif

#ifndef __BIGGEST_ALIGNMENT__
#  define __BIGGEST_ALIGNMENT__ 16
#endif



#include <stdint.h>
#include <stddef.h>
#include "kaapi_error.h"

#if defined(__linux__)
#  ifndef _GNU_SOURCE
#    define _GNU_SOURCE
#    include <sched.h>
#    undef _GNU_SOURCE
#  else
#    include <sched.h>
#  endif
#endif

#include <stdio.h>

#if defined(__cplusplus)
extern "C" {
#endif

#if defined(__linux__)
#  define KAAPI_HAVE_COMPILER_TLS_SUPPORT 1
#elif defined(__APPLE__)
#endif

#if !defined(KAAPI_CACHE_LINE)
#define KAAPI_CACHE_LINE 64
#endif

/** Processor type for task implementation
*/
#define KAAPI_PROC_TYPE_VOID    0x0  /* not used */
#define KAAPI_PROC_TYPE_HOST    0x1
#define KAAPI_PROC_TYPE_CUDA    0x2
#define KAAPI_PROC_TYPE_PHI     0x3
#define KAAPI_PROC_TYPE_OPENGL  0x4
#define KAAPI_PROC_TYPE_MAX     0x5
#define KAAPI_PROC_TYPE_CPU     KAAPI_PROC_TYPE_HOST
#define KAAPI_PROC_TYPE_GPU     KAAPI_PROC_TYPE_CUDA
#define KAAPI_PROC_TYPE_MIC     KAAPI_PROC_TYPE_PHI
#define KAAPI_PROC_TYPE_DEFAULT KAAPI_PROC_TYPE_HOST

/* Maximal number of different tasks (format) in a program
*/
#define KAAPI_FORMAT_MAX 128

/* Kaapi types.
 */
typedef uint32_t  kaapi_globalid_t;
typedef uint32_t  kaapi_processor_id_t;
typedef uint32_t  kaapi_format_id_t;

/** Reducor to reduce value with cw access mode */
typedef void (*kaapi_reducor_t)(void* /*result*/, const void* /*value*/);

/** Redinit: build a neutral element for the reduction law */
typedef void (*kaapi_redinit_t)(void* /*result*/);

/* Fwd decl
*/
struct kaapi_task_t;
struct kaapi_context_t;
struct kaapi_stack_t;
struct kaapi_frame_t;
struct kaapi_format_t;
struct kaapi_processor_t;
struct kaapi_ressource_t;
struct kaapi_steal_request_t;
struct kaapi_place_t;
struct kaapi_listrequest_iterator_t;
struct kaapi_listrequest_t;
struct kaapi_team_t;

/** Identifier to an address space id
*/
typedef uint64_t  kaapi_address_space_id_t;
struct kaapi_data_replica_t;
struct kaapi_metadata_info_t;

/** \ingroup thread
    Identifier to a thread in a parallel region
*/
typedef uint32_t  kaapi_thread_id_t;

/** \ingroup HWS
    Identifier to a locality domain 
*/
typedef uint64_t  kaapi_ldid_t;

/** \ingroup HWS
    Identifier to a locality domain set
*/
typedef uint64_t  kaapi_ldset_t;


/** \ingroup HWS
    hierarchy level identifiers
    Duplicated in likomp.h
 */
typedef enum kaapi_hws_levelid
{
  KAAPI_HWS_LEVELID_THREAD  = 0,
  KAAPI_HWS_LEVELID_CORE    = 1,
  KAAPI_HWS_LEVELID_NUMA    = 2,
  KAAPI_HWS_LEVELID_SOCKET  = 3,
  KAAPI_HWS_LEVELID_BOARD   = 4,
  KAAPI_HWS_LEVELID_MACHINE = 5,
  KAAPI_HWS_LEVELID_OFFLOAD_HOST = 6,
  KAAPI_HWS_LEVELID_OFFLOAD_CUDA = 7,
  KAAPI_HWS_LEVELID_OFFLOAD_MIC  = 8,
  KAAPI_HWS_LEVELID_MAX
} kaapi_hws_levelid_t;


/* =========================== atomic support ====================================== */
#include "kaapi_atomic.h"


/* ========================================================================= */
/* Initialization & environement functions                                   */
/* ========================================================================= */
/* Main function: initialization of the library; terminaison and abort        
   In case of normal terminaison, all internal objects are (I hope so !) deleted.
   The abort function is used in order to try to flush most of the internal buffer.
   kaapi_init should be called before any other kaapi function.
   \param flag [IN] if !=0 then start execution in parallel, else only the main thread is started
   \retval 0 in case of sucsess
   \retval EALREADY if already called
*/
extern int kaapi_init(int flag, int* argc, char*** argv);

#define KAAPI_START_ONLY_MAIN   0
#define KAAPI_START_ALL_THREADS 1

/* Kaapi finalization. 
   After call to this functions all other kaapi function calls may not success.
   \retval 0 in case of sucsess
   \retval EALREADY if already called
*/
extern int kaapi_finalize(void);


/* Get the current processor kid. 
   The kprocessor id is an unique identifier that represent a kernel thread.
   Two concurrent runing tasks have access to two different identifiers kid1 and kid2.
   \retval the current processor kid
*/
extern unsigned int kaapi_self_kid(void);

/* Get the current processor kid. 
   The kprocessor id is an unique identifier that represent a kernel thread.
   Two concurrent runing tasks have access to two different identifiers kid1 and kid2.
   \retval the current processor kid
*/
extern struct kaapi_team_t* kaapi_self_team(void);

/** Return pointer to the self stack
    Only work if sfp is the first field of kaapi_context_t
*/
#if defined(KAAPI_HAVE_COMPILER_TLS_SUPPORT)
  extern __thread struct kaapi_thread_t*  kaapi_current_thread_key;
#  define kaapi_self_thread() \
     (kaapi_current_thread_key)
#else
  extern struct kaapi_thread_t* kaapi_self_thread (void);
#endif

/** Get the workstealing concurrency number, i.e. the number of kernel
    activities to execute the user level thread. 
    This function is machine dependent.
    \retval the number of active threads to steal tasks
 */
extern int kaapi_get_concurrency (void);


/** Return the number of locality domain
*/
extern int kaapi_locality_domain_getcount(void);

/* ========================================================================= */
/* Controling parallel region activities                                     */
/* ========================================================================= */
/* Standard definition for proc_bind
*/
typedef enum kaapi_procbind_t {
  KAAPI_PROCBIND_FALSE   = 0,
  KAAPI_PROCBIND_TRUE    = 1,
  KAAPI_PROCBIND_MASTER  = 2,
  KAAPI_PROCBIND_CLOSE   = 3,
  KAAPI_PROCBIND_SPREAD  = 4,
  KAAPI_PROCBIND_DEFAULT = 5
} kaapi_procbind_t;
#define KAAPI_PROCBIND_DEFINED 1

/** Declare the beginning of a parallel region.
    On return to the call the team is initialized and the threads can be started by calling
    kaapi_start_parallel.
    The current stack is saved and it will be resumed in kaapi_end_parallel.
    \param num_threads the number of thread to create for the parallel region
    \param body the function executed by all threads. The function has
    two parameters, the first one is the thread identifier in the parallel region,
    the second is the argument of the function.
    \param arg the argument of the body
    \retval a pointer to a kaapi_team_t that should be used to the call to kaapi_end_parallel.
*/
extern struct kaapi_team_t* kaapi_begin_parallel(
  unsigned int num_threads,
  kaapi_procbind_t procbind,
  void (*body)(void*),
  void (*wrapper)(void*),
  void* arg
);

/** Start the threads of a team
    \retval 0 in case of success
*/
extern int kaapi_start_parallel(
  struct kaapi_team_t* team,
  int startmaster
);

/** Declare the end of a parallel region
    To be called by the thread that begin parallel region
*/
extern void kaapi_end_parallel(struct kaapi_team_t* team);

/* Lock of a team of threads. 
   Support for OMP
*/
extern int kaapi_team_lock (struct kaapi_team_t *team);

/* Unlock of a team
   Support for OMP
*/
extern int kaapi_team_unlock (struct kaapi_team_t *team);

enum kaapi_team_barrier_wait_flag_t {
  KAAPI_BARRIER_FLAG_DEFAULT    = 0,
  KAAPI_BARRIER_FLAG_RESET_WORK = 0x1,   /* reset state for work / foreach */
  KAAPI_BARRIER_FLAG_WAITEXIT   = 0x2,   /* wait if not the master thread of the team */
  KAAPI_BARRIER_FLAG_NOSCHEDULE = 0x4    /* no schedule task */
};

/* Barrier for each thread of a team
   flag allows to change internal behavior.
   IF flag & KAAPI_BARRIER_FLAG_WAITEXIT then all the threads, except the master,
   are waiting to be wakeuped when the master calls kaapi_team_barrier_wait_signal.
*/
extern void kaapi_team_barrier_wait (
    struct kaapi_team_t *team,
    struct kaapi_processor_t* kproc,
    int flag
);

/*
*/
extern void kaapi_team_barrier_wait_signal (
    struct kaapi_team_t* team,
    struct kaapi_processor_t* kproc
);

/*
*/
extern void kaapi_team_barrier_wait_signal_term(
    struct kaapi_team_t* team,
    struct kaapi_processor_t* kproc
);

/*
*/
extern void kaapi_team_barrier_wait_term (
    struct kaapi_team_t* team,
    struct kaapi_processor_t* kproc
);

/* Single
   Kproc is the current running processor or 0.
   Return true iff the thread enter in the single section.
*/
extern int kaapi_team_single (struct kaapi_team_t* team, struct kaapi_processor_t* kproc);

/* Master
   Kproc is the current running processor or 0.
   Return true iff the thread enter in the section.
*/
extern int kaapi_team_master ( struct kaapi_team_t* team, struct kaapi_processor_t* kproc );

/*
*/
extern int kaapi_get_running_threads(void);

/* ========================================================================= */
/* Shared object and access mode                                             */
/* ========================================================================= */
/** Kaapi access mode mask
    \ingroup DFG
*/
/*@{*/
typedef enum kaapi_access_mode_t {
  KAAPI_ACCESS_MODE_VOID= 0,        /* 0000 0000 : */
  KAAPI_ACCESS_MODE_V   = 1,        /* 0000 0001 : */
  KAAPI_ACCESS_MODE_R   = 2,        /* 0000 0010 : */
  KAAPI_ACCESS_MODE_W   = 4,        /* 0000 0100 : */
  KAAPI_ACCESS_MODE_CW  = 8,        /* 0000 1000 : */
  KAAPI_ACCESS_MODE_S   = 16,       /* 0001 0000 : stack data */
  KAAPI_ACCESS_MODE_T   = 32,       /* 0010 0000 : for Quark support: scratch mode or temporary */
  KAAPI_ACCESS_MODE_P   = 64,       /* 0100 0000 : */
  KAAPI_ACCESS_MODE_IP  = 128,      /* 1000 0000 : in place, for CW only */

  KAAPI_ACCESS_MODE_RW  = KAAPI_ACCESS_MODE_R|KAAPI_ACCESS_MODE_W,
  KAAPI_ACCESS_MODE_STACK = KAAPI_ACCESS_MODE_S|KAAPI_ACCESS_MODE_RW,
  KAAPI_ACCESS_MODE_SCRATCH = KAAPI_ACCESS_MODE_T|KAAPI_ACCESS_MODE_V,
  KAAPI_ACCESS_MODE_CWP = KAAPI_ACCESS_MODE_P|KAAPI_ACCESS_MODE_CW,
  KAAPI_ACCESS_MODE_ICW = KAAPI_ACCESS_MODE_IP|KAAPI_ACCESS_MODE_CW
} kaapi_access_mode_t;

#define KAAPI_ACCESS_MASK_RIGHT_MODE   0x7F   /* 5 bits, ie bit 0, 1, 2, 3, 4, including P mode */
#define KAAPI_ACCESS_MASK_MODE         0x3F   /* without P, IP mode */
#define KAAPI_ACCESS_MASK_MODE_P       0x80   /* only P mode */
/*@}*/


/** Kaapi macro on access mode
    \ingroup DFG
*/
/*@{*/
#define KAAPI_ACCESS_GET_MODE( m ) \
  ((m) & KAAPI_ACCESS_MASK_MODE )

#define KAAPI_ACCESS_IS_READ( m ) \
  ((m) & KAAPI_ACCESS_MODE_R)

#define KAAPI_ACCESS_IS_WRITE( m ) \
  ((m) & KAAPI_ACCESS_MODE_W)

#define KAAPI_ACCESS_IS_CUMULWRITE( m ) \
  ((m) & KAAPI_ACCESS_MODE_CW)

#define KAAPI_ACCESS_IS_STACK( m ) \
  ((m) & KAAPI_ACCESS_MODE_S)

#define KAAPI_ACCESS_IS_POSTPONED( m ) \
  ((m) & KAAPI_ACCESS_MASK_MODE_P)

/* W and CW */
#define KAAPI_ACCESS_IS_ONLYWRITE( m ) \
  (KAAPI_ACCESS_IS_WRITE(m) && !KAAPI_ACCESS_IS_READ(m))

#define KAAPI_ACCESS_IS_READWRITE( m ) \
  ( ((m) & KAAPI_ACCESS_MASK_MODE) == (KAAPI_ACCESS_MODE_W|KAAPI_ACCESS_MODE_R))

/** Return true if two modes are concurrents
    a == b and a or b is R or CW
    or a or b is postponed.
*/
#define KAAPI_ACCESS_IS_CONCURRENT(a,b) ((((a)==(b)) && (((b) == KAAPI_ACCESS_MODE_R)||((b)==KAAPI_ACCESS_MODE_CW))) || ((a|b) & KAAPI_ACCESS_MODE_P))
/*@}*/



/* ========================================================================= */
/* Format of a task                                                          */
/* ========================================================================= */
/** predefined format 
*/
/*@{*/
extern struct kaapi_format_t* kaapi_schar_format;
extern struct kaapi_format_t* kaapi_char_format;
extern struct kaapi_format_t* kaapi_shrt_format;
extern struct kaapi_format_t* kaapi_int_format;
extern struct kaapi_format_t* kaapi_long_format;
extern struct kaapi_format_t* kaapi_llong_format;
extern struct kaapi_format_t* kaapi_int8_format;
extern struct kaapi_format_t* kaapi_int16_format;
extern struct kaapi_format_t* kaapi_int32_format;
extern struct kaapi_format_t* kaapi_int64_format;
extern struct kaapi_format_t* kaapi_uchar_format;
extern struct kaapi_format_t* kaapi_ushrt_format;
extern struct kaapi_format_t* kaapi_uint_format;
extern struct kaapi_format_t* kaapi_ulong_format;
extern struct kaapi_format_t* kaapi_ullong_format;
extern struct kaapi_format_t* kaapi_uint8_format;
extern struct kaapi_format_t* kaapi_uint16_format;
extern struct kaapi_format_t* kaapi_uint32_format;
extern struct kaapi_format_t* kaapi_uint64_format;
extern struct kaapi_format_t* kaapi_flt_format;
extern struct kaapi_format_t* kaapi_dbl_format;
extern struct kaapi_format_t* kaapi_ldbl_format;
extern struct kaapi_format_t* kaapi_voidp_format;
/*@}*/


/* ========================================================================= */
/* Task and stack interface                                                  */
/* ========================================================================= */
/** Stack organized by blocs
*/
//#define KAAPI_STACKBLOCSIZE 1024
#define KAAPI_STACKBLOCSIZE 1048576ULL
//#define KAAPI_STACKBLOCSIZE 4294967296UL

/* Frame: where tasks and some data are stored.
   sp (and sp_data points) to address aligned to KAAPI_STACKBLOCSIZE boundary.
   If sp or sp_data is greather than upper bound, then the stack is full.
*/
typedef struct kaapi_thread_t {
  struct kaapi_task_t*   sp;
  char*                  sp_data;
} kaapi_thread_t;


#define _kaapi_start_blocaddr( addr, type ) \
  ((type)((uintptr_t)(char*)(addr) & ~(KAAPI_STACKBLOCSIZE-1)))

#define _kaapi_end_blocaddr( addr, type ) \
  ((type)((((uintptr_t)(char*)(addr)) & ~(KAAPI_STACKBLOCSIZE-1)) + KAAPI_STACKBLOCSIZE))

#define _kaapi_addr_inbloc( bloc, addr ) \
  ((_kaapi_start_blocaddr(addr,char*) >= (char*)bloc) && \
  (_kaapi_end_blocaddr(addr,char*) < ((char*)bloc+KAAPI_STACKBLOCSIZE)))

#define _kaapi_has_enough_dataspace( thread, size )  \
  ((_kaapi_end_blocaddr( (thread)->sp_data, char*) - (thread)->sp_data) > (intptr_t)size)

#define _kaapi_has_enough_taskspace( thread )  \
  (((thread)->sp) != (_kaapi_start_blocaddr((thread)->sp, kaapi_task_t*)))


typedef struct kaapi_thread_register_t {
  struct kaapi_task_t*   pc;
  struct kaapi_task_t*   sp;
  char*                  sp_data;
  void*                  reserved1;
  void*                  reserved2;
  void*                  reserved3;
  void*                  reserved4;
} kaapi_thread_register_t __attribute__((aligned(8)));


/** Task body
    \ingroup TASK
    See internal doc in order to have better documentation of invariant between the task and the thread.
*/
typedef void (*kaapi_task_body_t)( struct kaapi_task_t* /* task */,
                                   kaapi_thread_t* /* thread */);

typedef int (*kaapi_isready_fnc_t)( void* );


/** Flag for task.
    \ingroup TASK
    Note that this is only for user level construction.
    Some values are exclusives (e.g. COOPERATIVE and CONCURRENT),
    and are only represented by one bit.
*/
typedef enum kaapi_task_flag_t {
  KAAPI_TASK_FLAG_DEFAULT       = 0x00000,  /* default value for initializing task flag */
  KAAPI_TASK_FLAG_UNSTEALABLE   = 0x00001, /* default task stealable */
  KAAPI_TASK_FLAG_SPLITTABLE    = 0x00002, /* default task not splittable */
  KAAPI_TASK_FLAG_DFGOK         = 0x00004, /* ==1 iff task dependency already computed */
  KAAPI_TASK_FLAG_INDPENDENT    = 0x00008, /* if independent task */
  KAAPI_TASK_FLAG_COPY_ON_STEAL = 0x00010, /* if need to copy task argument if stolen */
  KAAPI_TASK_FLAG_PRIORITY      = 0x00020, /* consider priority field */
  KAAPI_TASK_FLAG_LD_BOUND      = 0x00080,  /* Bound to the locality domain */
  KAAPI_TASK_FLAG_AFF_ADDR      = 0x00100,
  KAAPI_TASK_FLAG_AFF_NODE      = 0x00200,
  KAAPI_TASK_FLAG_AFF_CORE      = 0x00300,
  KAAPI_TASK_FLAG_AFF_OCR       = 0x00400, /* OCR flag: site if index of access parameter */
  KAAPI_TASK_FLAG_IN_FINAL      = 0x00800, /* in_final clause - libKOMP */
  KAAPI_TASK_FLAG_VOID5         = 0x01000, /* ununsed */
  KAAPI_TASK_FLAG_VOID4         = 0x02000, /* ununsed */
  KAAPI_TASK_FLAG_VOID3         = 0x04000, /* ununsed */
  KAAPI_TASK_FLAG_VOID2         = 0x08000, /* ununsed */
  KAAPI_TASK_FLAG_VOID1         = 0x10000, /* ununsed */
} kaapi_task_flag_t;

#define KAAPI_TASK_FLAG_MASK_AFFINITY \
    (KAAPI_TASK_FLAG_AFF_ADDR|KAAPI_TASK_FLAG_AFF_NODE|KAAPI_TASK_FLAG_AFF_CORE) /* 0x700 */

typedef enum kaapi_affinity_kind_t {
  kaapi_affinity_none        = 0,
  kaapi_affinity_depend      = 1,
  kaapi_affinity_numa        = 2,
  kaapi_affinity_core        = 3
} kaapi_affinity_kind_t;


#define KAAPI_TASK_MAX_PRIORITY     1
#define KAAPI_TASK_MIN_PRIORITY     0 /* must be 0 because the default initialisation == 0 */

#define KAAPI_TASK_HIGH_PRIORITY    KAAPI_TASK_MAX_PRIORITY
#define KAAPI_TASK_DEFAULT_PRIORITY KAAPI_TASK_MIN_PRIORITY

#define KAAPI_TASK_SPLITTABLE_MASK     0x02  /* means that a splitter can be called */

/** Kaapi task definition
    \ingroup TASK
    A Kaapi task is the basic unit of computation. 
    It has a constant size including some task's specific values.
    Variable size task has to store pointer to the memory where found extra data.
    The body field is the pointer to the function to execute. 
*/
typedef struct kaapi_task_t {
  void*                           arg;       /** arg of the task */
  kaapi_task_body_t               body;      /** task body  */
  union {
    kaapi_task_body_t             steal;     /** !=0 iff steal */
    struct kaapi_frame_wrdlist_t* frame;     /** if FLAG_DFG_OK is set */
    void*                         arg;       /** to view frame and steal as void* */
  }                               state;
  kaapi_atomic_t                  wc;
//  uint32_t                        pad;
  union { /* should be of size of uintptr */
    struct {
      uint16_t                    flag;      /** some flag as splittable, local... */
      uint8_t                     priority;  /** of the task */
      uint8_t                     site;      /** 1+preferred location */
//      uint32_t                    date;
      /* ... */                              /** some bits are available on 64bits LP machine */
    } s;
    uint32_t                      dummy;     /* to clear previous fields in one write */
  } u;
  uint32_t                        T;         /* computed only if KAAPI_USE_PERFCOUNTER is set or used for CP  */
  uint32_t                        Tinf;      /* computed only if KAAPI_USE_PERFCOUNTER is set or used for CP  */
  const struct kaapi_format_t*    fmt;       /* cache if DFG built / frame_rd pointer */
#if defined(KAAPI_LINKED_LIST)
  struct kaapi_task_t*            next;      /* when task pushed into list */
  struct kaapi_task_t*            prev;      /* */
#else
  void*                           pad[2];
#endif
} kaapi_task_t  __attribute__((aligned(KAAPI_CACHE_LINE)));
  
  

/** \ingroup DFG
    Kaapi access, public
*/
typedef struct kaapi_access_t {
  void*                            data;    /* global data */
  struct kaapi_access_t*           next;    /* next access */
  kaapi_task_t*                    task;    /* the owner task */
  void*                            version; /* used from breaking dependencies */

  // following fields are only valid if flag & DFG_OK is valid on the task
  struct kaapi_access_t*           next_out; /* next access to activate */
  //kaapi_perf_counter_t*            perf;     /* unused except if KAAPI_USE_PERFCOUNTER is set */
} kaapi_access_t;

#define kaapi_data(type, a)\
  ((type*)((kaapi_access_t*)(a))->data)

/** \ingroup TASK
    The function kaapi_access_init() initialize an access from a user defined pointer
    \param access INOUT a pointer to the kaapi_access_t data structure to initialize
    \param value INOUT a pointer to the user data
    \retval a pointer to the next task to push or 0.
*/
static inline void kaapi_access_init(kaapi_access_t* a, void* value )
{
  a->data = value;
  a->task = 0;
#if !defined(KAAPI_NDEBUG)
  a->version = 0;
#endif
  return;
}

/** \ingroup TASK
    Return a pointer to parameter of the task (void*) pointer
*/
#define kaapi_task_getargs( task) ((void*)(task)->arg)

/** \ingroup TASK
    Return a reference to parameter of the task (type*) pointer
*/
#define kaapi_task_getargst(task,type) ((type*)(task)->arg)


/* Anormal push
*/
extern void* kaapi_thread_slow_push_data( kaapi_thread_t* thread, unsigned long size );


/** \ingroup TASK
    The function kaapi_data_push() will return the pointer to the next top data.
    The top data is not yet into the stack.
    If successful, the kaapi_thread_pushdata() function will return a pointer to the next data to push.
    Otherwise, an 0 is returned to indicate the error.
    \param thread INOUT a pointer to the kaapi_thread_t data structure where to push data
    \retval a pointer to the next task to push or 0.
*/
static inline __attribute__((__always_inline__))
void* kaapi_data_push( kaapi_thread_t* th, size_t size )
{
  void* retval;
  if (_kaapi_has_enough_dataspace(th, size))
  {
    retval = th->sp_data;
    th->sp_data += size;
  }
  else
    retval = kaapi_thread_slow_push_data(th,size);
#if defined(KAAPI_DEBUG)
  void* first = _kaapi_start_blocaddr( retval, void*);
  void* last  = _kaapi_end_blocaddr( retval, void*);
  kaapi_assert( first <= retval );
  kaapi_assert( retval < last );
#endif
  return retval;
}

/** \ingroup TASK
    same as kaapi_thread_pushdata, but with alignment constraints.
    note the alignment must be a power of 2 and not 0
    \param align the alignment size, in BYTES
*/
static inline __attribute__((__always_inline__))
void* kaapi_data_push_align(kaapi_thread_t* thread, uint32_t count, uintptr_t align)
{  
  kaapi_assert_debug( (align !=0) && /*(__BIGGEST_ALIGNMENT__ >= align) && */ ((align & (align - 1)) == 0));
  const uintptr_t mask = align - (uintptr_t)1;
  thread->sp_data = (char*)((uintptr_t)(thread->sp_data + mask) & ~mask);
  return kaapi_data_push(thread, count);
}


/**  \ingroup TASK
    Shot cut to pushdata
*/
static inline __attribute__((__always_inline__))
void*  kaapi_alloca( kaapi_thread_t* thread, uint32_t count)
{ return kaapi_data_push(thread, count); }


/** \ingroup TASK
    The function kaapi_gettemporary_data returns a pointer to a temporary data of size at least size.
    The scope of this pointer is the task execution. At the end of the task execution, the temporary data may
    be reused for an other task, thus it cannot be passed as parameter.
    \param id an identifier between 0 and 15
    \param size the size of the request temporary data
*/
extern void* kaapi_gettemporary_data(unsigned int id, size_t size);


/*
*/
extern kaapi_task_t* kaapi_thread_slow_pushtask(kaapi_thread_t* thread);


/** \ingroup TASK
    The function kaapi_task_push() pushes the top task into the stack.
    If successful, the kaapi_task_push() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    \param thread INOUT a pointer to the kaapi_stack_t data structure.
    \retval EINVAL invalid argument: bad stack pointer.
*/
static inline __attribute__((__always_inline__))
kaapi_task_t* kaapi_task_create(kaapi_task_t* task, kaapi_task_body_t body, void* arg)
{
  task->body      = body;
  task->state.steal = body;
  task->arg       = arg;
  task->u.dummy   = 0;
  task->Tinf      = 0;
  task->T         = 0;
  task->fmt       = 0;
#if !defined(NDEBUG) && defined(KAAPI_LINKED_LIST)
  task->next      = 0;
  task->prev      = 0;
  KAAPI_ATOMIC_WRITE( &task->wc, 0);
#endif
  return task;
}

/* push the task */
static inline __attribute__((__always_inline__))
void _kaapi_task_commit(kaapi_thread_t* thread)
{
  kaapi_writemem_barrier();
  ++thread->sp;
}
extern int kaapi_task_commit(kaapi_thread_t* thread);

static inline __attribute__((__always_inline__))
kaapi_task_t* _kaapi_task_push(kaapi_thread_t* thread, kaapi_task_body_t body, void* arg)
{
  kaapi_task_t* task = thread->sp;
#if defined(KAAPI_DEBUG)
  kaapi_task_t* first = _kaapi_start_blocaddr( task, kaapi_task_t*);
  kaapi_task_t* last  = _kaapi_end_blocaddr( task, kaapi_task_t*);
  kaapi_assert( first <= task );
  kaapi_assert( task < last );
#endif
  if (!_kaapi_has_enough_taskspace(thread))
  {
    task = kaapi_thread_slow_pushtask(thread);
#if defined(KAAPI_DEBUG)
    first = _kaapi_start_blocaddr( task, kaapi_task_t*);
    last  = _kaapi_end_blocaddr( task, kaapi_task_t*);
    kaapi_assert( first <= task );
    kaapi_assert( task < last );
#endif
  }
  kaapi_task_create( task, body, arg);
  return task;
}

static inline //__attribute__((__always_inline__))
int kaapi_task_push(kaapi_thread_t* thread, kaapi_task_body_t body, void* arg)
{
  _kaapi_task_push( thread, body, arg);
  return kaapi_task_commit(thread);
}


/** \ingroup TASK
    The function kaapi_task_push() pushes the top task into the stack.
    If successful, the kaapi_task_push() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    \param thread INOUT a pointer to the kaapi_stack_t data structure.
    \retval EINVAL invalid argument: bad stack pointer.
*/
extern int kaapi_task_push_withstate(
    kaapi_thread_t* thread,
    kaapi_task_body_t body,
    void* arg,
    uint8_t state
);


/** \ingroup TASK
    The function kaapi_task_push() pushes the top task into the stack.
    If successful, the kaapi_task_push() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    \param thread INOUT a pointer to the kaapi_stack_t data structure.
    \retval EINVAL invalid argument: bad stack pointer.
*/
static inline //__attribute__((__always_inline__))
kaapi_task_t* kaapi_task_create_withflags(
    kaapi_task_t*     task,
    kaapi_task_body_t body,
    void*             arg,
    kaapi_task_flag_t flag
)
{
  task->body      = body;
  task->state.steal = body;
  task->arg       = arg;
  task->u.dummy   = 0;
  task->u.s.flag  = flag;
  task->Tinf      = 0;
  task->T         = 0;
  task->fmt       = 0;
#if !defined(NDEBUG) && defined(KAAPI_LINKED_LIST)
  task->next      = 0;
  task->prev      = 0;
  KAAPI_ATOMIC_WRITE( &task->wc, 0);
#endif
  return task;
}

static inline //__attribute__((__always_inline__))
int kaapi_task_push_withflags(
    kaapi_thread_t* thread,
    kaapi_task_body_t body,
    void* arg,
    kaapi_task_flag_t flag
)
{
  kaapi_task_t* task = thread->sp;
#if defined(KAAPI_DEBUG)
  kaapi_task_t* first = _kaapi_start_blocaddr( task, kaapi_task_t*);
  kaapi_task_t* last  = _kaapi_end_blocaddr( task, kaapi_task_t*);
  kaapi_assert( first <= task );
  kaapi_assert( task < last );
#endif
  if (!_kaapi_has_enough_taskspace(thread))
  {
    task = kaapi_thread_slow_pushtask(thread);
#if defined(KAAPI_DEBUG)
    first = _kaapi_start_blocaddr( task, kaapi_task_t*);
    last  = _kaapi_end_blocaddr( task, kaapi_task_t*);
    kaapi_assert( first <= task );
    kaapi_assert( task < last );
#endif
  }
  kaapi_task_create_withflags(task, body, arg, flag);
  return kaapi_task_commit(thread);
}

/** \ingroup TASK
    The function kaapi_task_push_withld() set the task attribut to 
    indicate to the runtime that the task has interest to be pushed to the localitydomain ld.
    The attribut is only take into account if the task is scheduled using readylist engine.
    If successful, the kaapi_task_push_withprio() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    \param thread INOUT a pointer to the kaapi_stack_t data structure.
    \retval EINVAL invalid argument: bad stack pointer.
*/
extern int kaapi_task_push_withld(
    kaapi_thread_t* thread,
    kaapi_task_body_t body,
    void* arg,
    kaapi_task_flag_t flag,
    int   ld
);

/** \ingroup TASK
    The function kaapi_task_push_withprio() pushes the top task into the stack.
    If successful, the kaapi_task_push_withprio() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    \param thread INOUT a pointer to the kaapi_stack_t data structure.
    \retval EINVAL invalid argument: bad stack pointer.
*/
static inline //__attribute__((__always_inline__))
int kaapi_task_push_withprio(
    kaapi_thread_t* thread,
    kaapi_task_body_t body,
    void* arg,
    int prio
)
{
  kaapi_task_t* task = thread->sp;
  if (!_kaapi_has_enough_taskspace(thread))
    task = kaapi_thread_slow_pushtask(thread);
  task->body      = body;
  task->state.steal = body;
  task->arg       = arg;
  task->u.dummy   = 0;
  task->u.s.priority = prio;
#if !defined(NDEBUG) && defined(KAAPI_LINKED_LIST)
  task->next      = 0;
  task->prev      = 0;
  KAAPI_ATOMIC_WRITE( &task->wc, 0);
#endif
  return kaapi_task_commit(thread);
}


/* Compatibility with previous version
@{
*/
static inline void* kaapi_thread_pushdata( kaapi_thread_t* thread, uint32_t count)
{ return kaapi_data_push(thread, count); }

static inline void* kaapi_thread_pushdata_align(kaapi_thread_t* thread, uint32_t count, uintptr_t align)
{ return kaapi_data_push_align(thread, count, align); }


static inline kaapi_task_t* kaapi_thread_toptask( kaapi_thread_t* thread)
{
  return thread->sp;
}


static inline void kaapi_task_init
  ( kaapi_task_t* task, kaapi_task_body_t body, void* arg )
{
  task->body      = body;
  task->state.steal = body;
  task->arg       = arg;
  task->u.dummy   = 0;
}

static inline void kaapi_task_set_priority(kaapi_task_t* task, int prio)
{
  task->u.s.priority = prio;
}
/*
@}
*/



/** \ingroup TASK
   Type for splitter:
   - called in order to split a running or init task
   - return value is 0 in case of success call to the splitter.
   - return value is ECHILD if no work can be split again.
   This value mean also that all futures calls will failed to split
   work because the work set is empty (forever).
*/
typedef int (*kaapi_adaptivetask_splitter_t)(
  struct kaapi_task_t*                 /* task to be used to get arguments */,
  struct kaapi_listrequest_iterator_t* /* iterator over the list*/
);


/** \ingroup TASK
    The function kaapi_task_push_adaptive() pushes the new_task into the stack
    and set its flag to be splittable.
    If successful, the kaapi_task_push_adaptive() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    \param thread INOUT a pointer to the kaapi_thread_t data structure.
    \param task INOUT a pointer to the kaapi_task_t data structure.
    \retval EINVAL invalid argument: bad stack pointer.
*/
extern int kaapi_task_push_adaptive(
  kaapi_thread_t*               thread,
  kaapi_task_body_t             body,
  kaapi_adaptivetask_splitter_t user_splitter,
  void*                         arg
);

/** \ingroup TASK
    Call to push into stack a stolen task.
*/
extern int kaapi_thread_push_stolentask(
    kaapi_thread_t*        thread,
    kaapi_task_t*          victim_task,
    kaapi_task_body_t      body,
    void*                  arg
);

/** \ingroup TASK
    The function kaapi_task_push_adaptive() pushes the new_task into the stack
    and set its flag to be splittable.
    If successful, the kaapi_task_push_adaptive() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    \param thread INOUT a pointer to the kaapi_thread_t data structure.
    \param task INOUT a pointer to the kaapi_task_t data structure.
    \retval EINVAL invalid argument: bad stack pointer.
*/
extern kaapi_task_t* kaapi_task_create_adaptive_withflags(
  kaapi_task_t*                 task,
  kaapi_thread_t*               thread,
  kaapi_task_body_t             body,
  kaapi_adaptivetask_splitter_t user_splitter,
  void*                         arg,
  kaapi_task_flag_t             flag  
);


extern int kaapi_task_push_adaptive_withflags(
  kaapi_thread_t*               thread,
  kaapi_task_body_t             body,
  kaapi_adaptivetask_splitter_t user_splitter,
  void*                         arg,
  kaapi_task_flag_t             flag  
);

/** \ingroup TASK
    The function kaapi_thread_save() saves the registers of the current thread.
    If successful, the kaapi_thread_save() return 0.
    Otherwise, return the error code.
*/
extern int kaapi_thread_save(kaapi_thread_t* thread, kaapi_thread_register_t* regs);

/** \ingroup TASK
    The function kaapi_thread_restore() restore the registers saved by a call to kaapi_thread_save.
    Both kaapi_thread_restore and kaapi_thread_save muste be called within the same task region.
    If successful, the kaapi_thread_restore() return 0
    Otherwise, return the error code.
*/
extern int kaapi_thread_restore(kaapi_thread_t* thread, const kaapi_thread_register_t* regs);

/** \ingroup TASK
    The function kaapi_sched_sync() execute all childs tasks of the current running task.
    If successful, the kaapi_sched_sync() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    \param thread INOUT a pointer to the thread data structure 
    \retval EINTR the control flow has received a KAAPI interrupt.
*/
extern int kaapi_sched_sync( kaapi_thread_t* thread );

/** Sync without computing dfg 
*/
extern int kaapi_sched_sync_nodfg( kaapi_thread_t* thread );

/** Sync without computing dfg 
*/
extern int kaapi_sched_sync_dfg( kaapi_thread_t* thread );

/** \ingroup SCHED
    Compute readlist of the tasks in the current frame.
*/
extern int kaapi_frame_computereadylist_current(void);



/* ========================================================================= */
/* API for adaptive algorithm                                                */
/* ========================================================================= */
/** \ingroup WS
    Allows the current thread of execution to create tasks on demand.
    A thread that begin an adaptive section, allows thiefs to call a splitter
    function with an arguments in order to push tasks for thiefs.
    If not thief is idle, then no tasks will be created and there is no cost
    due to creations of tasks.

    This is the way Kaapi implements adaptive algorithms.
    
    The current thread of execution (and the current executing task) should
    mark the section of code where tasks are created on demand by using 
    the instructions kaapi_task_begin_adaptive and kaapi_task_end_adaptive. 
    Between this two instructions, a splitter may be invoked with its arguments:
    - in concurrence with the local execution thread iff flag = KAAPI_SC_CONCURRENT
    - with cooperation with the local execution thread iff flag =KAAPI_SC_COOPERATIVE.
    In the case of the cooperative execution, the code should test presence of request
    using the instruction kaapi_stealpoint.
*/


extern struct kaapi_steal_request_t* kaapi_listrequest_iterator_get(
  struct kaapi_listrequest_iterator_t* lrrange
);
extern struct kaapi_steal_request_t* kaapi_listrequest_iterator_next(
  struct kaapi_listrequest_iterator_t* lrrange 
);
extern int kaapi_listrequest_iterator_empty( 
  struct kaapi_listrequest_iterator_t* lrrange
);
/* may reclaims O(k) ops */
extern int kaapi_listrequest_iterator_count(
  struct kaapi_listrequest_iterator_t* lrrange
);



/** \ingroup WS
    Opcode for request
*/
typedef enum {
  KAAPI_REQUEST_OP_PUSH,
  KAAPI_REQUEST_OP_PUSH_REMOTE,
  KAAPI_REQUEST_OP_PUSHLIST,
  KAAPI_REQUEST_OP_POP,
  KAAPI_REQUEST_OP_STEAL,
  KAAPI_REQUEST_OP_STEAL_INPLACE,
  KAAPI_REQUEST_OP_EXTRA
} kaapi_request_op_t;

/** \ingroup WS
    Common header to all requests.
    WARNING these fields must appears AT THE BEGINING of the sub
    request data structure (missing C++ class heritage !)
*/
typedef struct kaapi_header_request_t {
  kaapi_request_op_t            op;             /* the op to apply (on a place) */
  struct kaapi_place_t*         ld;             /* victim place */
  int                           ident;          /* system wide id who is emetting the request */
  int                           mask_arch;      /* accepted arch */
  int                           status;         /* request status */
} kaapi_header_request_t;

/** \ingroup WS
    A steal request.
    First fields are those ofkaapi_header_request_t
*/
typedef struct kaapi_steal_request_t {
  kaapi_request_op_t            op;
  struct kaapi_place_t*         ld;             /* victim place */
  int                           ident;          /* system wide id who is emetting the request */
  int                           mask_arch;      /* accepted arch */
  int                           status;         /* request status */
  kaapi_thread_t                thread;         /* where to store theft tasks/data */
  uintptr_t                     victim;         /* victim: used only to record trace */
  uintptr_t                     serial;         /* serial number: used only to record trace */
} kaapi_steal_request_t;


/** \ingroup ADAPTIVE
    The function kaapi_request_pushdata() allocates data in the request.
    If successful, the kaapi_request_pushdata() function will return a valid pointer.
    Otherwise, an 0 is returned to indicate the error.
    \param request INOUT a pointer to the kaapi_steal_request_t data structure
    \retval a valid pointer to a memory region with at least size bytes or 0.
*/
static inline void* kaapi_request_pushdata( kaapi_steal_request_t* request, size_t size)
{ return kaapi_data_push(&request->thread, size); }

/** \ingroup ADAPTIVE
    The function kaapi_request_pushtask() pushes the new_task into the request's frame.
    If successful, the kaapi_request_pushtask() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    \param request INOUT a pointer to the kaapi_steal_request_t data structure.
    \param victim_task INOUT a pointer to the kaapi_task_t that is stolen or 0.
    \param victim_task INOUT a pointer to the kaapi_task_t for the thief
    \retval EINVAL invalid argument: bad stack pointer.
*/
extern int kaapi_request_pushtask(
    kaapi_steal_request_t* request,
    kaapi_task_t*          victim_task,
    kaapi_task_body_t      body,
    void*                  arg
);

/** \ingroup ADAPTIVE
    The function kaapi_request_pushtask_adaptive() pushes the task with its spliiter
    into the thief stack. The pushed task is an adaptive task.
    If successful, the kaapi_request_pushtask_adaptive() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    \param request INOUT a pointer to the kaapi_steal_request_t data structure.
    \param victim_task INOUT a pointer to the victim stack under splitting operation.
    \param task INOUT a pointer to the thief task under splitting operation.
    \param splitter the splitter that will called with the task as parameter.
    \retval EINVAL invalid argument: bad request pointer.
*/
extern int kaapi_request_pushtask_adaptive(
  kaapi_steal_request_t*        request,
  kaapi_task_t*                 victim_task, 
  kaapi_task_body_t             body,
  kaapi_adaptivetask_splitter_t splitter,
  void*                         arg
);

/** \ingroup ADAPTIVE
    The function kaapi_request_committask() mades visible pushed tasks to the thief.
    If successful, the kaapi_request_committask() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    \param request INOUT a pointer to the kaapi_steal_request_t data structure.
    \param victim_task IN a pointer to the victim stack under splitting operation.
    \param headtail_flag IN either KAAPI_REQUEST_REPLY_HEAD or KAAPI_REQUEST_REPLY_TAIL
    \retval EINVAL invalid argument: bad request pointer.
*/
extern int kaapi_request_committask( kaapi_steal_request_t* request );


/* ========================================================================= */
/* Managing data & sub data                                                  */
/* ========================================================================= */
/** Type of allowed memory view for the memory interface:
    - 1D array (base, size)
      simple contiguous 1D array
    - 2D array (base, size[2], lda)
      assume a row major storage of the memory : the 2D array has
      size[0] rows of size[1] rowwidth. lda is used to pass from
      one row to the next one.
    The base (kaapi_pointer_t) is not part of the view description
*/
#define KAAPI_MEMORY_VIEW_1D 1
#define KAAPI_MEMORY_VIEW_2D 2  /* assume row major */
#define KAAPI_MEMORY_VIEW_3D 3
typedef struct kaapi_memory_view_t {
  int           type;
  long          offset;
  unsigned long size[2];
  unsigned long lda;
  unsigned long wordsize;
} kaapi_memory_view_t;

/** Type of pointer for all address spaces.
    The pointer encode both the pointer (field ptr) and the location of the address space
    in asid.
    Pointer arithmetic is allowed on this type on the ptr field.
*/
typedef struct kaapi_pointer_t { 
  kaapi_address_space_id_t asid;
  uintptr_t                ptr;
} kaapi_pointer_t;


/*
*/
static inline kaapi_pointer_t kaapi_make_pointer( void* ptr, kaapi_address_space_id_t asid)
{ kaapi_pointer_t p; p.asid = asid; p.ptr = (uintptr_t)ptr; return p; }

/** Make a null pointer object
*/
static inline kaapi_pointer_t kaapi_make_nullpointer(void)
{ kaapi_pointer_t p = {0, (uintptr_t)0}; return p; }

/** Make a null pointer object
*/
static inline void kaapi_pointer_setnull(kaapi_pointer_t* ptr )
{ ptr->ptr = 0; ptr->asid = 0; }

/** Return non null value if the pointer is null.
    A null pointer is independent from its location (asid).
*/
static inline int kaapi_pointer_isnull( kaapi_pointer_t p)
{ return p.ptr ==0; }

/* cast to void* 
*/
static inline void* kaapi_pointer2void(kaapi_pointer_t p)
{ return (void*)p.ptr; }

/* cast to uintptr 
*/
static inline uintptr_t kaapi_pointer2uintptr(kaapi_pointer_t p)
{ return p.ptr; }

/* cast from void* 
*/
static inline void kaapi_void2pointer(kaapi_pointer_t* p, void* ptr)
{ p->ptr = (uintptr_t)ptr; }


/** return the size of the view
*/
static inline size_t kaapi_memory_view_size( const kaapi_memory_view_t* const kmv )
{
  switch (kmv->type) 
  {
    case KAAPI_MEMORY_VIEW_1D: return kmv->size[0]*kmv->wordsize;
    case KAAPI_MEMORY_VIEW_2D: return kmv->size[0]*kmv->size[1]*kmv->wordsize;
    default:
      kaapi_assert(0);
      break;
  }
  return 0;
}

/** Return non null value iff the view is contiguous
*/
static inline int kaapi_memory_view_iscontiguous( const kaapi_memory_view_t* const kmv )
{
  switch (kmv->type) {
    case KAAPI_MEMORY_VIEW_1D: return 1;
    case KAAPI_MEMORY_VIEW_2D: return  kmv->lda == kmv->size[1]; /* row major storage */
    default:
      break;
  } 
  return 0;
}

/*
*/
static inline void* kaapi_memory_view2pointer(void* ptr, const kaapi_memory_view_t* view)
{
  return (void*)((uintptr_t)ptr + (uintptr_t)view->offset*view->wordsize);
}

/*
*/
static inline void* kaapi_memory_view2Npointer(void* ptr, const kaapi_memory_view_t* view)
{
  return (void*)((uintptr_t)ptr - (uintptr_t)view->offset*view->wordsize);
}

/*
*/
static inline void kaapi_memory_view_make1d(kaapi_memory_view_t* view, ptrdiff_t offset, size_t size, size_t wordsize)
{
  view->type     = KAAPI_MEMORY_VIEW_1D;
  view->offset   = offset;
  view->size[0]  = size;
  view->wordsize = wordsize;
  view->size[1]  = 0;
  view->lda      = 0;
}

/*
*/
static inline void kaapi_memory_view_make2d(
  kaapi_memory_view_t* view,
  ptrdiff_t offset, size_t n, size_t m, size_t lda, size_t wordsize
)
{
  view->type     = KAAPI_MEMORY_VIEW_2D;
  view->offset   = offset;
  view->size[0]  = n;
  view->size[1]  = m;
  view->lda      = lda;
  view->wordsize = wordsize;
}

/** \ingroup DSM
 Synchronize all shared memory in the local address space to the up-to-date value.
*/
extern int kaapi_memory_sync(void);

/** \ingroup DSM
  Synchronize a specific address space located at host.
 */
extern int kaapi_memory_sync_addr( kaapi_address_space_id_t asid, void* ptr, kaapi_memory_view_t view );

/** \ingroup DSM
  Bind a specific address space to the DSM.
 */
extern struct kaapi_metadata_info_t* kaapi_memory_bind(
    kaapi_address_space_id_t   kasid,
    kaapi_address_space_id_t   kasid_ptr,
    void*                      ptr,
    const kaapi_memory_view_t* view,
    struct kaapi_data_replica_t** r
);

/** \ingroup DSM
 Unbind a memory address.
 */
extern void kaapi_memory_unbind( kaapi_address_space_id_t kasid, void* ptr, kaapi_memory_view_t view );

/* ========================================================================= */
/* Format of data                                                            */
/* ========================================================================= */

/** \ingroup TASK
    Allocate a new format data
*/
extern struct kaapi_format_t* kaapi_format_allocate(void);

/* typdefs
*/
typedef int (*kaapi_fmt_update_mb)(void* data, const struct kaapi_format_t* fmtdata,
                      const void* value, const struct kaapi_format_t* fmtvalue );

typedef void (*kaapi_fmt_fnc_get_name)(
  const struct kaapi_format_t*, const void*, char* buffer, int size
);

typedef size_t (*kaapi_fmt_fnc_get_size)(const struct kaapi_format_t*, const void*);

typedef void (*kaapi_fmt_fnc_task_copy)(const struct kaapi_format_t*, void*, const void*);

typedef unsigned int (*kaapi_fmt_fnc_get_count_params)(const struct kaapi_format_t*, const void*);

typedef kaapi_access_mode_t (*kaapi_fmt_fnc_get_mode_param)(
    const struct kaapi_format_t*, unsigned int, const void*
);

typedef void* (*kaapi_fmt_fnc_get_data_param)(
    const struct kaapi_format_t*, unsigned int, const void*
);

typedef kaapi_access_t* (*kaapi_fmt_fnc_get_access_param)(
    const struct kaapi_format_t*, unsigned int, const void*
);

typedef void (*kaapi_fmt_fnc_set_access_param)(
    const struct kaapi_format_t*, unsigned int, void*, const kaapi_access_t*
);

typedef const struct kaapi_format_t*(*kaapi_fmt_fnc_get_fmt_param)(
    const struct kaapi_format_t*, unsigned int, const void*
);

typedef int (*kaapi_fmt_fnc_get_affinity)(
    const struct kaapi_format_t*, const void*, kaapi_task_t*, uint16_t
);

typedef void (*kaapi_fmt_fnc_get_view_param)(
    const struct kaapi_format_t*, unsigned int, const void*, kaapi_memory_view_t* 
);

typedef void (*kaapi_fmt_fnc_set_view_param)(
    const struct kaapi_format_t*, unsigned int, void*, const kaapi_memory_view_t*);

typedef void (*kaapi_fmt_fnc_reducor)(
    const struct kaapi_format_t*, unsigned int, void*, const void*);

typedef void (*kaapi_fmt_fnc_redinit)(
    const struct kaapi_format_t*, unsigned int, const void* sp, void*);

typedef kaapi_adaptivetask_splitter_t	(*kaapi_fmt_fnc_get_splitter)(
    const struct kaapi_format_t*, const void*
);


/** \ingroup TASK
    Register a task format with dynamic definition
*/
extern kaapi_format_id_t kaapi_format_taskregister_func(
  struct kaapi_format_t*         fmt, 
  void*                          key,
  kaapi_task_body_t              body,
  const char*                    name,
  kaapi_fmt_fnc_get_name         get_name,
  kaapi_fmt_fnc_get_size         get_size,
  kaapi_fmt_fnc_task_copy        task_copy,
  kaapi_fmt_fnc_get_count_params get_count_params,
  kaapi_fmt_fnc_get_mode_param   get_mode_param,
  kaapi_fmt_fnc_get_data_param   get_data_param,
  kaapi_fmt_fnc_get_access_param get_access_param,
  kaapi_fmt_fnc_set_access_param set_access_param,
  kaapi_fmt_fnc_get_fmt_param    get_fmt_param,
  kaapi_fmt_fnc_get_view_param   get_view_param,
  kaapi_fmt_fnc_set_view_param   set_view_param,
  kaapi_fmt_fnc_reducor          reducor,
  kaapi_fmt_fnc_redinit          redinit,
  kaapi_fmt_fnc_get_splitter	   get_splitter,
  kaapi_fmt_fnc_get_affinity	   get_affinity
);

/** \ingroup TASK
    format accessor
*/
extern void kaapi_format_set_dot_name( struct kaapi_format_t* , const char* );

/** \ingroup TASK
    format accessor
*/
extern void kaapi_format_set_dot_color( struct kaapi_format_t* , const char* );

extern void kaapi_format_set_update_mb(struct kaapi_format_t* fmt, kaapi_fmt_update_mb);

/** \ingroup TASK
    Register a task format 
*/
extern void kaapi_format_set_task_body
(struct kaapi_format_t*, unsigned int, kaapi_task_body_t);

/** \ingroup TASK
    Register a task body into its format.
    A task may have multiple implementation this function specifies in 'archi'
    the target archicture for the body.    
*/
extern kaapi_task_body_t kaapi_format_taskregister_body( 
        struct kaapi_format_t*      fmt,
        kaapi_task_body_t           body,
        unsigned int                archi
);

/** \ingroup TASK
    Register a data structure format
*/
extern kaapi_format_id_t kaapi_format_structregister( 
        struct kaapi_format_t*      fmt,
        const char*                 name,
        size_t                      size,
        void                       (*cstor)( void* ),
        void                       (*dstor)( void* ),
        void                       (*cstorcopy)( void*, const void*),
        void                       (*copy)( void*, const void*),
        void                       (*assign)( void*, const kaapi_memory_view_t*, const void*, const kaapi_memory_view_t*)
);

/** \ingroup TASK
    Resolve a format data structure from the key of its format
*/
extern struct kaapi_format_t* kaapi_format_resolve_bykey(void* key);

/** \ingroup TASK
    Resolve a format data structure from the body of a task
*/
extern struct kaapi_format_t* kaapi_format_resolve_bybody( kaapi_task_body_t body );

/** \ingroup TASK
    Resolve a format data structure from the format identifier
*/
extern struct kaapi_format_t* kaapi_format_resolvebyfmit(kaapi_format_id_t key);


#define KAAPI_REGISTER_STRUCTFORMAT( formatobject, name, size, cstor, dstor, cstorcopy, copy, assign ) \
  static inline struct kaapi_format_t* fnc_formatobject(void) \
  {\
    static kaapi_format_t formatobject##_object;\
    return &formatobject##_object;\
  }\
  static inline void __attribute__ ((constructor)) __kaapi_register_format_##formatobject (void)\
  { \
    static int isinit = 0;\
    if (isinit) return;\
    isinit = 1;\
    kaapi_format_structregister( &formatobject, name, size, cstor, dstor, cstorcopy, copy, assign );\
  }
  

/* Exported function to hash string
*/
extern uint32_t kaapi_hash_value(const char * data);



/* ========================================================================= */
/* Main task: created on demand                                              */
/* ========================================================================= */
/** The main task arguments
    \ingroup TASK
*/
typedef struct kaapi_taskmain_arg_t {
  int          argc;
  char**       argv;
  void (*mainentry)(int, char**);
} kaapi_taskmain_arg_t;

/** The main task
    \ingroup TASK
*/
extern void kaapi_taskmain_body( kaapi_task_t*, kaapi_thread_t* );


/* ========================================================================= */
/* NOP task: to do nothing                                                   */
/* ========================================================================= */
extern void kaapi_nop_body( kaapi_task_t*, kaapi_thread_t* );


/* ========================================================================= */
/* Task to compute DFG                                                       */
/* ========================================================================= */
/** Scheduler information pass by the runtime to task forked
    with set static attribut
    - array nkproc have the same dimension of number of static
    proc types. nkproc[i] == number of proc type i (i+1== KAAPI_PROC_TYPE_HOST|GPU|MPSOC)
    used for static scheduling.
    - bitmap may be also pass here.
*/
typedef struct kaapi_staticschedinfo_t {
  int16_t  nkproc[KAAPI_PROC_TYPE_MAX];
} kaapi_staticschedinfo_t;

/** Body of the task in charge of finalize of adaptive task
    \ingroup TASK
*/
typedef struct kaapi_staticschedtask_arg_t {
  void*                        sub_arg;   /* encapsulated task */
  kaapi_task_body_t            sub_body;  /* encapsulated body */
  const struct kaapi_format_t* sub_fmt;
  intptr_t                     key;
  kaapi_staticschedinfo_t      schedinfo; /* number of partition */
} kaapi_staticschedtask_arg_t;

extern void kaapi_staticschedtask_body( kaapi_task_t*, kaapi_thread_t* );


/* ========================================================================= */
/* For each interface                                                        */
/* ========================================================================= */
/* Scheduling strategy for foreach loop. Any change should be reported 
   in komp_foreach_attr_policy_t
*/
typedef enum {
  KAAPI_FOREACH_SCHED_DEFAULT    = 0x00,  /* default: implementation defined */
  KAAPI_FOREACH_SCHED_STATIC     = 0x01,  /* static distribution as OpenMP */
  KAAPI_FOREACH_SCHED_DYNAMIC    = 0x02,  /* dynamic distribution as OpenMP */
  KAAPI_FOREACH_SCHED_GUIDED     = 0x03,  /* guided distribution as OpenMP */
  KAAPI_FOREACH_SCHED_AUTO       = 0x04,  /* auto strategy as OpenMP */
  KAAPI_FOREACH_SCHED_STEAL      = 0x05,  /* steal half strategy */
  KAAPI_FOREACH_SCHED_ADAPTIVE   = 0x06,  /* add to the steal strategy a distribution step */
  KAAPI_FOREACH_SCHED_SPLIT      = 0x07,  /* internal only. Used to init range from steal */
  KAAPI_FOREACH_SCHED_RUNTIME    = 0x10   /* runtime strategy as OpenMP */
} kaapi_foreach_attr_policy_t;
#define KAAPI_SCHEDPOLICY_DEFINED 1

/* Foreach attribut. SHOULD BE THE SAME AS komp_foreach_attr_t
   Default values are :
   * sgrain, pgrain = 1
   * policy         = KAAPI_FOREACH_SCHED_DEFAULT
   * nthreads       = depending of the runtime number of threads, no limit
   * threadset      = no constraints
   flag is a bitfield:
    - bit0 = strict dist
    - bit1 = strict affinity 
*/
typedef struct kaapi_foreach_attr_t {
  unsigned int                init;      /* flags for each attributes == 1 iff initialize */
  unsigned long long          s_grain;   /* default = 1 */
  unsigned long long          p_grain;   /* default = 1 */
  kaapi_foreach_attr_policy_t policy;    /* choose the policy for splitting */
  kaapi_hws_levelid_t         dist;      /* default == CORE */
  kaapi_hws_levelid_t         affinity;  /* default == MACHINE */
  int                         flag;      /* default == 0 */
} kaapi_foreach_attr_t;


/*
*/
static inline int kaapi_foreach_attr_init(kaapi_foreach_attr_t* attr)
{
  attr->init  = 0;
  return 0;
}

/*
*/
extern int kaapi_foreach_attr_set_policy(
  kaapi_foreach_attr_t* attr, 
  kaapi_foreach_attr_policy_t policy
);

/*
*/
extern int kaapi_foreach_attr_get_policy(
  const kaapi_foreach_attr_t* attr,
  kaapi_foreach_attr_policy_t* policy
);

/*
*/
extern int kaapi_foreach_attr_set_grains(
  kaapi_foreach_attr_t* attr, 
  unsigned long long s_grain,
  unsigned long long p_grain
);

/*
*/
extern int kaapi_foreach_attr_get_grains(
  const kaapi_foreach_attr_t* attr,
  unsigned long long* s_grain,
  unsigned long long* p_grain
);

/* Core of the given locality domain 'level' of the running thread steal iterations in priority.
   If strictness !=0 then only those cores can steal iterations.
*/
extern int kaapi_foreach_attr_set_localitydomain(
  kaapi_foreach_attr_t* attr,
  kaapi_hws_levelid_t   level,
  int                   strict
);

/* 
*/
extern int kaapi_foreach_attr_get_localitydomain(
  const kaapi_foreach_attr_t* attr,
  kaapi_hws_levelid_t*        level,
  int*                        strict
);

/* Distribute the iteration over the localitydomain of the given 'level' :
   * pre-distribute following the level : machine / numa or core.
   * if strict !=0 then only threads (PU) of given level can steal the distribution
*/
extern int kaapi_foreach_attr_set_distribution(
  kaapi_foreach_attr_t* attr,
  kaapi_hws_levelid_t   level,
  int                   strict
);

/* Distribute the iteration over the localitydomain of the given 'level' :
   * distribute
*/
extern int kaapi_foreach_attr_get_distribution(
  const kaapi_foreach_attr_t* attr,
  kaapi_hws_levelid_t*        level,
  int*                        strict
);


/* Set flag for nowait loop: only the master finish the loop
   if nowait ==0 then the all threads wait for the end of the loop.
*/
extern int kaapi_foreach_attr_set_nowait(
  kaapi_foreach_attr_t* attr,
  int                   nowait
);
extern int kaapi_foreach_attr_get_nowait(
  const kaapi_foreach_attr_t* attr,
  int*                        nowait
);

/*
*/
static inline int kaapi_foreach_attr_destroy(kaapi_foreach_attr_t* attr __attribute__((unused)))
{ return 0; }

/* Signature of foreach body 
   Called with (first, last, arg) in order to do
   computation over the range [first,last[.
*/
typedef void (*kaapi_foreach_body_t)(
    unsigned long long,  /* first */
    unsigned long long,  /* last */
    void* );

/* High level for each function.
   Execute body on each iteration i from lb up to ub (not included).
   In case of concurrency, then execution is performed in parallel.
   See documentation
   \retval 0: in case of success
   \retval EINVAL: invalid arguments, bounds may represent empty range, null function
*/
extern int kaapi_foreach(
  unsigned long long          size,
  const kaapi_foreach_attr_t* attr,
  kaapi_foreach_body_t body,
  void* arg
);


/* Low level API
*/
struct kaapi_foreach_work_t;

/* Init work of a foreach construct.
   Function called by all threads in a team.
   The internal runtime only support iteration from [0,Last[ with unit increment.
   External code must transform iteration space to [0,Last[ before calling init
   and each times body_f is called.
   A common error is to pass negative value to last. Please make sure that caller
   test the values before calling the function.
   \param body_f and body_args may be 0 : in that case work can only be steal from an other thread
   in the team.
   \retval returns non zero if there is work to do, else returns 0
*/
extern struct kaapi_foreach_work_t*  kaapi_foreach_workinit(
  struct kaapi_context_t*     self_thread,
  int                         sync,
  const kaapi_foreach_attr_t* attr,
  unsigned long long          size,
  kaapi_foreach_body_t        body_f,
  void*                       body_args
);

/* Lower level function used by libgomp implementation */

extern int kaapi_foreach_workfini(
  struct kaapi_foreach_work_t* work
);

extern int kaapi_foreach_worknext(
  struct kaapi_foreach_work_t* work,
  unsigned long long*          first,
  unsigned long long*          last
);


#ifdef __cplusplus
}
#endif


#endif /* _KAAPI_H */
