/*
 ** xkaapi
 **
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** Joao.Lima@inf.ufsm.br
 **
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 **
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 **
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 **
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 **
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <stdarg.h>
#include <pthread.h>
#include <cuda.h>
#include <cublas_v2.h>

#define _PLUGIN_NAME "cuda"

#include "kaapi_plugin.h"
#include "kaapi_impl.h"
#include "kaapi_format.h"

#define CONFIG_USE_EVENT 1
//#define SYNCHRONOUS 1

//#define CONFIG_USE_P2P  1
#define CONFIG_MAX_DEV  10

typedef struct {
  int id;
  CUdevice cu_device;
  CUcontext ctx;
  cublasHandle_t handle;
  /* device properties (from NVIDIA website) */
  struct {
    bool overlap;      /* if the device can concurrently copy memory between host and device while executing a kernel */
    bool integrated;   /*  if the device is integrated with the memory subsystem */
    bool map;          /* if the device can map host memory into the CUDA address space */
    bool concurrent;   /* if the device supports executing multiple kernels within the same context simultaneously */
    int async_engines; /* Number of asynchronous engines */
    size_t mem_total;  /* total GPU memory size in bytes */
#if defined(_PLUGIN_DEBUG)
    char name[256];   /* GPU name */
#endif
  } prop;
#if defined(CONFIG_USE_P2P)
  bool has_peer[CONFIG_MAX_DEV]; /* devices that have peer transfer */
  bool enabled_peer[CONFIG_MAX_DEV]; /* devices enabled since we need the peer context */
#endif
} kaapi_device_cuda_t;

static bool plugin_initialized = false;
static int kaapi_device_count = 0;
static kaapi_device_cuda_t** kaapi_cuda_ndevices = 0;
static pthread_mutex_t kaapi_cuda_lock = PTHREAD_MUTEX_INITIALIZER;

/*
*/
static inline void kaapi_cuda_error_fatal(const char* msg, ...)
{
  va_list args;
  va_start(args, msg);
  vfprintf(stderr, msg, args);
  va_end(args);
  assert(0);
}

static inline void kaapi_cuda_plugin_lock(void)
{
  pthread_mutex_lock(&kaapi_cuda_lock);
}

static inline void kaapi_cuda_plugin_unlock(void)
{
  pthread_mutex_unlock(&kaapi_cuda_lock);
}

static inline bool kaapi_cuda_device_is_initialized(int n)
{
  return (kaapi_cuda_ndevices[n] != NULL);
}

#if defined(_PLUGIN_DEBUG)
static void kaapi_cuda_check_ctx(void)
{
  CUcontext ctx;
  CUresult res;
  kaapi_device_t* self_device = kaapi_offload_self_device();
  kaapi_device_cuda_t* cuda_dev= kaapi_cuda_ndevices[self_device->device_id];
  unsigned int version;
  /* unsigned int compiled_version = __CUDA_API_VERSION; */
  
  res= cuCtxGetCurrent(&ctx);
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuCtxGetCurrent: %d\n", res);
  
  if(cuda_dev->ctx != ctx)
    kaapi_cuda_error_fatal("ERROR: current ctx differs from current device: %d\n", self_device->device_id);

  res = cuCtxGetApiVersion(ctx, &version);
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuCtxGetApiVersion: %d\n", res);
}
#endif

static inline bool kaapi_cuda_check_address(uintptr_t ptr)
{
  CUresult res;
  CUdeviceptr pbase;
  size_t size;

  res = cuMemGetAddressRange( &pbase, &size, (CUdeviceptr)ptr );
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuMemGetAddressRange: %d\n", res);

  if(pbase != (CUdeviceptr)ptr)
    return false;

  return true;
}

static void kaapi_cuda_check_params( kaapi_task_t* task, kaapi_device_t* self_device)
{
  KAAPI_PLUGIN_TRACE_IN
  const kaapi_format_t* fmt = task->fmt;
  unsigned int count_params;
  unsigned int i;
  kaapi_access_t* access;
  kaapi_memory_view_t view;

  count_params = kaapi_format_get_count_params(fmt, kaapi_task_getargs(task));
  for(i= 0; i < count_params; i++)
  {
    kaapi_access_mode_t m = kaapi_format_get_mode_param(fmt, i, kaapi_task_getargs(task));
    kaapi_access_mode_t mp = KAAPI_ACCESS_GET_MODE(m);
    if (mp & KAAPI_ACCESS_MODE_V)
      continue;
    access = kaapi_format_get_access_param(fmt, i, kaapi_task_getargs(task));
    kaapi_format_get_view_param(fmt, i, kaapi_task_getargs(task), &view);
    void* right_ptr = kaapi_memory_view2pointer(access->data, &view);

    /* because reallocate translate allocated pointer with -offset, check in the righ device ptr!*/
    if (!kaapi_cuda_check_address((uintptr_t)right_ptr))
      kaapi_cuda_error_fatal("ERROR: bad address: %d\n", access->data);
    KAAPI_PLUGIN_TRACE_MSG("%s: task:%p param[%i]:%p->%p ok\n", __FUNCTION__,
        task, i, access->data, right_ptr
    );
  }

  KAAPI_PLUGIN_TRACE_OUT
}


static inline int kaapi_cuda_device_init(int n)
{
  kaapi_device_cuda_t* cuda_dev;
  CUresult res;
  CUdevice cu_device;
  CUcontext ctx;
  int pi;

  KAAPI_PLUGIN_TRACE_IN
  KAAPI_PLUGIN_TRACE_MSG("device: %d\n", n);

  res = cuDeviceGet(&cu_device, n);
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuDeviceGet: %d\n", res);

  cuda_dev = (kaapi_device_cuda_t*)malloc(sizeof(kaapi_device_cuda_t));
  assert(cuda_dev != NULL);
  cuda_dev->id = n;
  cuda_dev->cu_device = cu_device;
  
  /* thread has already a context, pop it */
  res = cuCtxGetCurrent(&ctx);
  if(res != CUDA_ERROR_INVALID_CONTEXT)
    cuCtxPopCurrent(&ctx);

  res = cuCtxCreate(&cuda_dev->ctx, CU_CTX_SCHED_AUTO, cu_device);
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuCtxCreate: %d\n", res);
  
  res = cuDeviceGetAttribute (&pi, CU_DEVICE_ATTRIBUTE_GPU_OVERLAP, cu_device);
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuDeviceGetAttribute: %d\n", res);
  cuda_dev->prop.overlap = pi;
  
  res = cuDeviceGetAttribute (&pi, CU_DEVICE_ATTRIBUTE_INTEGRATED, cu_device);
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuDeviceGetAttribute: %d\n", res);
  cuda_dev->prop.integrated = pi;

  res = cuDeviceGetAttribute (&pi, CU_DEVICE_ATTRIBUTE_CAN_MAP_HOST_MEMORY, cu_device);
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuDeviceGetAttribute: %d\n", res);
  cuda_dev->prop.map = pi;
  
  res = cuDeviceGetAttribute (&pi, CU_DEVICE_ATTRIBUTE_CONCURRENT_KERNELS, cu_device);
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuDeviceGetAttribute: %d\n", res);
  cuda_dev->prop.concurrent = pi;

  res = cuDeviceGetAttribute (&pi, CU_DEVICE_ATTRIBUTE_ASYNC_ENGINE_COUNT, cu_device);
  if(res != CUDA_SUCCESS)
    pi = 1;
  cuda_dev->prop.async_engines = pi;
  
  res = cuDeviceTotalMem(&cuda_dev->prop.mem_total, cu_device);
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuDeviceTotalMem: %d\n", res);
  
#if defined(_PLUGIN_DEBUG)
  memset(cuda_dev->prop.name, 0, 256*sizeof(char));
  cuDeviceGetName(cuda_dev->prop.name, 256, cu_device);
  fprintf(stdout, "cuda:%s: cuda %d device name '%s'\n", __FUNCTION__, n, cuda_dev->prop.name);
#endif
  
  cublasStatus_t cres = cublasCreate(&cuda_dev->handle);
  if(cres != CUBLAS_STATUS_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cublasCreate: %d\n", res);

#if defined(CONFIG_USE_P2P)
  {
    int i;
    for(i= 0; i < CONFIG_MAX_DEV; i++)
    {
      cuda_dev->has_peer[i]= false;
      cuda_dev->enabled_peer[i] = false;
    }
    /* configure each peer device */
    for(i= 0; i < kaapi_device_count; i++)
    {
      int canAccessPeer = 0;
      CUdevice cuPeerDev;
      if(i == cuda_dev->id)
        continue;
      
      res = cuDeviceGet(&cuPeerDev, i);
      if(res != CUDA_SUCCESS)
        kaapi_cuda_error_fatal("ERROR: cuDeviceGet: %d\n", res);
      
      res = cuDeviceCanAccessPeer(&canAccessPeer, cuda_dev->cu_device, cuPeerDev);
      kaapi_assert_debug(res == CUDA_SUCCESS);
      if(canAccessPeer){
//        fprintf(stdout, "%s: peer from %d -> %d\n", __FUNCTION__, cuda_dev->id, i);fflush(stdout);
        cuda_dev->has_peer[i] = true;
      }
    }
  }
#endif
  
  cuCtxSynchronize();
  
  kaapi_cuda_ndevices[n] = cuda_dev;

  KAAPI_PLUGIN_TRACE_OUT

  return 0;
}

static inline void kaapi_cuda_device_finalize(int n)
{
  kaapi_device_cuda_t* cuda_dev;
  assert( (n >= 0) && (n < kaapi_device_count) );
  assert( kaapi_cuda_ndevices[n] != NULL );

  cuda_dev = kaapi_cuda_ndevices[n];
//  cuCtxDestroy(cuda_dev->ctx);
  cuCtxPopCurrent(&cuda_dev->ctx);  
  free(cuda_dev);
  kaapi_cuda_ndevices[n] = NULL;
}

static inline void kaapi_cuda_device_attach(int n)
{
  kaapi_device_cuda_t* cuda_dev;
  CUresult res;
  assert(plugin_initialized == true);
  assert(kaapi_cuda_ndevices != NULL);
  assert( (n >= 0) && (n < kaapi_device_count) );

  cuda_dev = kaapi_cuda_ndevices[n];

  KAAPI_PLUGIN_TRACE_IN
  KAAPI_PLUGIN_TRACE_MSG("device: %d, ctx:%p\n", n, (void*)cuda_dev->ctx);

  res = cuCtxPushCurrent(cuda_dev->ctx);
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuCtxPushCurrent: %d\n", res);
  KAAPI_PLUGIN_TRACE_OUT
}

static inline void kaapi_cuda_device_detach(int n)
{
  kaapi_device_cuda_t* __attribute__((unused)) cuda_dev;
  CUcontext ctx;
  CUresult res;
  assert(plugin_initialized == true);
  assert(kaapi_cuda_ndevices != NULL);
  assert( (n >= 0) && (n < kaapi_device_count) );

  cuda_dev = kaapi_cuda_ndevices[n];
  KAAPI_PLUGIN_TRACE_IN
  res = cuCtxPopCurrent(&ctx);
  KAAPI_PLUGIN_TRACE_MSG("device: %d, ctx:%p, poped ctx:%p\n", n, (void*)cuda_dev->ctx, (void*)ctx);

  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuCtxPopCurrent: %d\n", res);
  KAAPI_PLUGIN_TRACE_OUT
}

static inline uintptr_t kaapi_cuda_device_alloc(size_t size)
{
  CUdeviceptr ptr;
  CUresult res;

//#warning "Include mechanism of memory ocupancy to avoid alloc errors"
#if defined(_PLUGIN_DEBUG)
  kaapi_cuda_check_ctx();
#endif
  res = cuMemAlloc( &ptr, size );
  if( res == CUDA_ERROR_OUT_OF_MEMORY )
    return 0;

  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuMemAlloc: %d\n", res);

#if defined(_PLUGIN_DEBUG)
  fprintf(stdout, "cuda:%s: alloc ptr=%p size=%ld\n", __FUNCTION__, (void*)ptr, size);
#endif

  return (uintptr_t)ptr;
}

static inline void kaapi_cuda_device_free(uintptr_t ptr)
{
  CUresult res;

  if( kaapi_cuda_check_address(ptr) == false )
    kaapi_cuda_error_fatal("ERROR: invalid address %x.\n", ptr);

#if defined(_PLUGIN_DEBUG)
  kaapi_cuda_check_ctx();
  fprintf(stdout, "cuda:%s: free ptr=%p\n", __FUNCTION__, (void*)ptr);
  fflush(stdout);
#endif
  res = cuMemFree((CUdeviceptr)ptr);
  if(res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuMemFree: %d (%x)\n", res, ptr);
}

/*
 */
char * KAAPI_PLUGIN_ENTRYPOINT(get_name)(void)
{
  return "cuda";
}

/*
 */
unsigned int KAAPI_PLUGIN_ENTRYPOINT(get_flags)(void)
{
  return 0;
}

/*
 */
unsigned int KAAPI_PLUGIN_ENTRYPOINT(get_type)(void)
{
  return 0x2; /* KAAPI_PROC_TYPE_CUDA */
}

unsigned int
KAAPI_PLUGIN_ENTRYPOINT(get_level)(void)
{
  return KAAPI_HWS_LEVELID_OFFLOAD_CUDA;
}

/*
 */
unsigned int KAAPI_PLUGIN_ENTRYPOINT(get_number)(void)
{
  assert(plugin_initialized == true);
  return kaapi_device_count;
}

/*
 */
int KAAPI_PLUGIN_ENTRYPOINT(init)(void)
{
  CUresult res;

  kaapi_cuda_plugin_lock();
  if (plugin_initialized == true)
  {
    kaapi_cuda_plugin_unlock();
    return 0;
  }

  res = cuInit(0);
  if (res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuInit error: %s\n", res);

  res = cuDeviceGetCount(&kaapi_device_count);
  if (res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuInit error: %s\n", res);

  /* Only use 1 device:*/
//#warning "FOR DEBUG"
//  kaapi_device_count = 1;

  kaapi_cuda_ndevices = (kaapi_device_cuda_t**)calloc(kaapi_device_count, sizeof(kaapi_device_cuda_t*));
  assert(kaapi_cuda_ndevices != NULL);
  plugin_initialized = true;
  kaapi_cuda_plugin_unlock();
#if defined(_PLUGIN_DEBUG)
  fprintf(stdout, "cuda:%s: cuda init with %d devices\n", __FUNCTION__, kaapi_device_count);
#endif

  return 0;
}

/*
 */
void
KAAPI_PLUGIN_ENTRYPOINT(finalize)(void)
{
  kaapi_cuda_plugin_lock();
  if (plugin_initialized == true)
  {
    free(kaapi_cuda_ndevices);
    plugin_initialized = false;
#if defined(_PLUGIN_DEBUG)
    fprintf(stdout, "cuda:%s:\n", __FUNCTION__);
#endif
  }
  kaapi_cuda_plugin_unlock();
}

/*
 */
int KAAPI_PLUGIN_ENTRYPOINT(device_init)(int dev)
{
  int ret;
  assert(plugin_initialized == true);
  assert(kaapi_cuda_ndevices != NULL);

  kaapi_cuda_plugin_lock();
  if (kaapi_cuda_device_is_initialized(dev) == true)
  {
    kaapi_cuda_plugin_unlock();
    return 0;
  }

  ret = kaapi_cuda_device_init(dev);
  kaapi_cuda_plugin_unlock();
#if defined(_PLUGIN_DEBUG)
  fprintf(stdout, "cuda:%s: cuda %d init\n", __FUNCTION__, dev);
#endif
  return ret;
}

/*
 */
void KAAPI_PLUGIN_ENTRYPOINT(device_finalize)(int dev)
{
  assert(plugin_initialized == true);
  assert(kaapi_cuda_ndevices != NULL);

  kaapi_cuda_plugin_lock();
  kaapi_cuda_device_finalize(dev);
  kaapi_cuda_plugin_unlock();

#if defined(_PLUGIN_DEBUG)
  fprintf(stdout, "cuda:%s: cuda %d finalize\n", __FUNCTION__, dev);
#endif
}

/*
 */
int KAAPI_PLUGIN_ENTRYPOINT(device_attach)(int dev)
{
  KAAPI_PLUGIN_TRACE_IN
  KAAPI_PLUGIN_TRACE_MSG("device: %d\n", dev);
  kaapi_cuda_device_attach(dev);
  KAAPI_PLUGIN_TRACE_OUT
  return 1;
}

/*
 */
void KAAPI_PLUGIN_ENTRYPOINT(device_detach)(int dev)
{
  KAAPI_PLUGIN_TRACE_IN
  KAAPI_PLUGIN_TRACE_MSG("device: %d\n", dev);
  kaapi_cuda_device_detach(dev);
  KAAPI_PLUGIN_TRACE_OUT
}

/*
 */
uintptr_t KAAPI_PLUGIN_ENTRYPOINT(alloc)(size_t size)
{
  return kaapi_cuda_device_alloc(size);
}

/*
 */
void KAAPI_PLUGIN_ENTRYPOINT(free)(void* p)
{
  kaapi_cuda_device_free((uintptr_t)p);
}


/*
 */
size_t KAAPI_PLUGIN_ENTRYPOINT(get_total_mem)(int dev)
{
  assert(plugin_initialized == true);
  assert(kaapi_cuda_ndevices != NULL);
  assert( (dev >= 0) && (dev < kaapi_device_count) );
  
  return kaapi_cuda_ndevices[dev]->prop.mem_total;
}


/* IO stream with specific field for CUDA
   If event is configured on, then one event is insert just after each asynchronous 
   operations (memcpy, kernel launch). Then the runtime can wait or test specific event
 */
typedef struct kaapi_cuda_io_stream_t {
  kaapi_io_stream_t inherited;
  CUstream          stream;
#if CONFIG_USE_EVENT
  CUevent*          end_events;
#if defined(KAAPI_USE_PERFCOUNTER)
  CUevent*          start_events;
#endif
#endif
} kaapi_cuda_io_stream_t;


/*
 */
kaapi_io_stream_t* KAAPI_PLUGIN_ENTRYPOINT(stream_alloc)( int device_id, int type, unsigned int capacity )
{
  CUresult res;
  kaapi_assert_debug(plugin_initialized == true);
  kaapi_assert_debug((device_id >= 0) && (device_id < kaapi_device_count) );

  kaapi_cuda_io_stream_t* cios = (kaapi_cuda_io_stream_t*)malloc( sizeof(kaapi_cuda_io_stream_t));
  if (cios ==0)
    return 0;

  res = cuStreamCreate (&cios->stream, CU_STREAM_NON_BLOCKING);
  if (res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuStreamCreate error: %s\n", res);

#if CONFIG_USE_EVENT
  unsigned int i;
  cios->end_events = (CUevent*)malloc( capacity * sizeof(CUevent) );
  if (cios->end_events ==0)
  {
    free(cios);
    return 0;
  }
#if defined(KAAPI_USE_PERFCOUNTER)
  cios->start_events = (CUevent*)malloc( capacity * sizeof(CUevent) );
  if (cios->start_events ==0)
  {
    free(cios->end_events);
    free(cios);
    return 0;
  }
#endif
  for (i=0; i<capacity; ++i)
  {
    /* TG NOTE: here if perfcounter is enable, then 2 events per communication may be inserted:
     the first that inserted just before the communication, the second just after.
     Using such insertions, it is able to compute (online) the bandwidth of communication.
     */
#if !defined(KAAPI_USE_PERFCOUNTER)
    res = cuEventCreate(&cios->end_events[i], CU_EVENT_DISABLE_TIMING);
    if (res != CUDA_SUCCESS)
      kaapi_cuda_error_fatal("ERROR: cuEventCreate error: %s\n", res);
#else
    res = cuEventCreate(&cios->end_events[i], CU_EVENT_DISABLE_TIMING);
    if (res != CUDA_SUCCESS)
      kaapi_cuda_error_fatal("ERROR: cuEventCreate error: %s\n", res);
/* not in use
    res = cuEventCreate(&cios->start_events[i], CU_EVENT_DEFAULT);
    if (res != CUDA_SUCCESS)
      kaapi_cuda_error_fatal("ERROR: cuEventCreate error: %s\n", res);
*/
#endif
  }
#endif

  return &cios->inherited;
}

/*
 */
void KAAPI_PLUGIN_ENTRYPOINT(stream_free)( kaapi_io_stream_t* ios )
{
  kaapi_cuda_io_stream_t* cios = (kaapi_cuda_io_stream_t*)ios;
#if CONFIG_USE_EVENT
  free(cios->end_events);
#if defined(KAAPI_USE_PERFCOUNTER)
  free(cios->start_events);
#endif
#endif
  cuStreamDestroy(cios->stream);
  free(cios);
}


/*
 */
int KAAPI_PLUGIN_ENTRYPOINT(stream_decode_ioinstruction)(
    kaapi_io_stream_t* ios,
    kaapi_io_instruction_t* instr
)
{
#if defined(_PLUGIN_DEBUG)
static char* name_io[] = {
  "IO_BEGIN",
  "IO_END",
  "IO_COPY_H2H",
  "IO_COPY_H2D",
  "IO_COPY_D2H",
  "IO_COPY_D2D",
  "IO_BARRIER",
  "IO_KERN"
};
#endif

  KAAPI_PLUGIN_TRACE_IN

#if defined(_PLUGIN_DEBUG)
  kaapi_cuda_check_ctx();
#endif

  CUresult res = CUDA_SUCCESS;
  kaapi_cuda_io_stream_t* cios = (kaapi_cuda_io_stream_t*)ios;
  kaapi_device_t* self_device = ios->stream->device;
  kaapi_assert_debug(kaapi_offload_self_device() == ios->stream->device);

  KAAPI_PLUGIN_TRACE_MSG("%s: instr '%s'\n", __FUNCTION__, name_io[instr->type]);

#if CONFIG_USE_EVENT && defined(KAAPI_USE_PERFCOUNTER)
#if 0
  switch (instr->type)
  {
    case KAAPI_IO_COPY_H2H:
    case KAAPI_IO_COPY_H2D:
    case KAAPI_IO_COPY_D2H:
    case KAAPI_IO_COPY_D2D:
    case KAAPI_IO_KERN:
    {
      res = cuEventRecord(cios->start_events[ ios->instr - instr], cios->stream);
      if (res != CUDA_SUCCESS)
        kaapi_cuda_error_fatal("ERROR: cuEventRecord error %d\n", res);
    } break;
    default:
      break;
  }
#endif
#endif

  switch (instr->type)
  {
    case KAAPI_IO_BEGIN:
    case KAAPI_IO_END:
      break;

    case KAAPI_IO_COPY_H2H:
    case KAAPI_IO_COPY_H2D:
    case KAAPI_IO_COPY_D2H:
    case KAAPI_IO_COPY_D2D:
    {
      struct kaapi_io_copy* op = &instr->inst.c_io;
      CUDA_MEMCPY2D pcopy;

      /* switch among view_src type (1D, 2D or 3D). May be some redistribution may be
       implemented here ?
       */
      size_t size = kaapi_memory_view_size(op->view_src);
      kaapi_assert( size == kaapi_memory_view_size(op->view_dest));
      switch (op->view_src->type)
      {
        case KAAPI_MEMORY_VIEW_1D:
        {
          KAAPI_PLUGIN_TRACE_MSG("%s: instr '%s' 1D data\n", __FUNCTION__, name_io[instr->type]);
          switch (instr->type)
          {
            case KAAPI_IO_COPY_H2H:
              KAAPI_EVENT_PUSH2(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_HTOH_BEG, op->src, size );
              memcpy( op->dest, op->src, size );
              res = 0;
            break;
            case KAAPI_IO_COPY_H2D:
              KAAPI_EVENT_PUSH2(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_HTOD_BEG, op->src, size );
#if defined(SYNCHRONOUS)
              res = cuMemcpyHtoD( (CUdeviceptr)op->dest,
                                       op->src,
                                       size);
#else
              res = cuMemcpyHtoDAsync( (CUdeviceptr)op->dest,
                                       op->src,
                                       size,
                                       cios->stream);
#endif
            break;
            case KAAPI_IO_COPY_D2H:
              KAAPI_EVENT_PUSH2(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_DTOH_BEG, op->src, size );
#if defined(SYNCHRONOUS)
              res = cuMemcpyDtoH( op->dest,
                                       (CUdeviceptr)op->src,
                                       size);
#else
              res = cuMemcpyDtoHAsync( op->dest,
                                       (CUdeviceptr)op->src,
                                       size,
                                       cios->stream);
#endif
            break;
            case KAAPI_IO_COPY_D2D:
              KAAPI_EVENT_PUSH2(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_DTOD_BEG, op->src, size );
#if defined(SYNCHRONOUS)
              res = cuMemcpyDtoD( (CUdeviceptr)op->dest,
                                       (CUdeviceptr)op->src,
                                       size);
#else
              res = cuMemcpyDtoDAsync( (CUdeviceptr)op->dest,
                                       (CUdeviceptr)op->src,
                                       size,
                                       cios->stream);
#endif
            break;
            default:
              kaapi_assert_debug(0);
          };
          if (res != CUDA_SUCCESS)
           kaapi_cuda_error_fatal("ERROR: cudaMemcpyXXAsync error",res);
        } break;

        case KAAPI_MEMORY_VIEW_2D:
        {
          KAAPI_PLUGIN_TRACE_MSG("%s: instr '%s' 2D data, src:%p, dest:%p, size:%i\n",
              __FUNCTION__,
              name_io[instr->type],
              op->src,
              op->dest, (int)size
          );

          pcopy.dstXInBytes = 0;
          pcopy.dstY        = 0;
          pcopy.dstPitch    = op->view_dest->lda * op->view_dest->wordsize;
          pcopy.srcXInBytes = 0;
          pcopy.srcY        = 0;
          pcopy.srcPitch    = op->view_src->lda * op->view_src->wordsize;
          switch (instr->type)
          {
            case KAAPI_IO_COPY_H2H:
              pcopy.srcMemoryType = CU_MEMORYTYPE_HOST;
              pcopy.srcHost       = op->src;
              pcopy.dstMemoryType = CU_MEMORYTYPE_HOST;
              pcopy.dstHost       = op->dest;
              KAAPI_EVENT_PUSH2(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_HTOH_BEG, op->src, size );
            break;
            case KAAPI_IO_COPY_H2D:
              pcopy.srcMemoryType = CU_MEMORYTYPE_HOST;
              pcopy.srcHost       = op->src;
              pcopy.dstMemoryType = CU_MEMORYTYPE_DEVICE;
              pcopy.dstDevice     = (CUdeviceptr)op->dest;
              KAAPI_EVENT_PUSH2(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_HTOD_BEG, op->src, size );
            break;
            case KAAPI_IO_COPY_D2H:
              pcopy.srcMemoryType = CU_MEMORYTYPE_DEVICE;
              pcopy.srcDevice     = (CUdeviceptr)op->src;
              pcopy.dstMemoryType = CU_MEMORYTYPE_HOST;
              pcopy.dstHost       = op->dest;
              KAAPI_EVENT_PUSH2(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_DTOH_BEG, op->src, size );
            break;
            case KAAPI_IO_COPY_D2D:
              pcopy.srcMemoryType = CU_MEMORYTYPE_DEVICE;
              pcopy.srcDevice     = (CUdeviceptr)op->src;
              pcopy.dstMemoryType = CU_MEMORYTYPE_DEVICE;
              pcopy.dstDevice     = (CUdeviceptr)op->dest;
              KAAPI_EVENT_PUSH2(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_DTOD_BEG, op->src, size );
            break;
            default:
              kaapi_assert_debug(0);
          };
          pcopy.WidthInBytes = op->view_dest->size[1] * op->view_dest->wordsize;
          pcopy.Height       = op->view_dest->size[0];

#if defined(_PLUGIN_DEBUG)
          kaapi_cuda_check_ctx();
#endif
#if defined(SYNCHRONOUS)
          res = cuMemcpy2D( &pcopy );
#else
          res = cuMemcpy2DAsync( &pcopy, cios->stream );
#endif
          if (res != CUDA_SUCCESS)
            kaapi_cuda_error_fatal("ERROR: cudaMemcpyXXAsync error: %d", res);
        } break;

        case KAAPI_MEMORY_VIEW_3D:
        default:
          kaapi_cuda_error_fatal("ERROR: kaapi_cuda_decode_ioinstruction error",CUDA_ERROR_INVALID_VALUE);
          break;
      };

#if defined(SYNCHRONOUS)
      switch (instr->type)
      {
        case KAAPI_IO_COPY_H2H:
          KAAPI_EVENT_PUSH1(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_HTOH_END, op->src );
        break;
        case KAAPI_IO_COPY_H2D:
          KAAPI_EVENT_PUSH1(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_HTOD_END, op->src );
        break;
        case KAAPI_IO_COPY_D2H:
          KAAPI_EVENT_PUSH1(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_DTOH_END, op->src );
        break;
        case KAAPI_IO_COPY_D2D:
          KAAPI_EVENT_PUSH1(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_DTOD_END, op->src );
        break;
        default:
          kaapi_assert_debug(0);
      };
#endif
      KAAPI_PLUGIN_TRACE_MSG/*printf*/("%s: stream %p instr '%s' src:%p, dest:%p size:%zu\n", __FUNCTION__,
          (void*)cios->stream,
          name_io[instr->type],
          (void*)op->src,
          (void*)op->dest,
          size
      );
    } break;

    case KAAPI_IO_BARRIER:
      break;

    case KAAPI_IO_KERN:
    {
      struct kaapi_io_kernel* op = &instr->inst.k_io;
      kaapi_task_body_t body = kaapi_format_get_task_body_by_arch(op->task->fmt, self_device->rsrc.proc_type);
      cublasStatus_t cres = cublasSetStream(
                  kaapi_cuda_ndevices[self_device->device_id]->handle, cios->stream
      );
      if (cres != CUBLAS_STATUS_SUCCESS)
        kaapi_cuda_error_fatal("ERROR: cublasSetStream error",res);
      self_device->cublas_handle = kaapi_cuda_ndevices[self_device->device_id]->handle;
      kaapi_cuda_check_params( op->task, self_device);
      KAAPI_PLUGIN_TRACE_MSG("%s: instr '%s' exec task:%p, stream: %p\n", __FUNCTION__,
          name_io[instr->type],
          op->task,
          (void*)cios->stream
      );
      KAAPI_EVENT_PUSH1(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_KERNEL_BEG, op->task );
      body( op->task, kaapi_self_thread() );
#if defined(SYNCHRONOUS)
      res = cuStreamSynchronize( cios->stream );
      KAAPI_EVENT_PUSH1(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_KERNEL_END, op->task );
#endif
    }
  }

#if CONFIG_USE_EVENT && !defined(SYNCHRONOUS)
  /* if events are use, record the event at position ios->pos_wp where the pending instruction
     will be copied
  */
  switch (instr->type)
  {
    case KAAPI_IO_COPY_H2H:
    case KAAPI_IO_COPY_H2D:
    case KAAPI_IO_COPY_D2H:
    case KAAPI_IO_COPY_D2D:
    case KAAPI_IO_KERN:
    {
      res = cuEventRecord(cios->end_events[ ios->pos_wp ], cios->stream);
      if (res != CUDA_SUCCESS)
        kaapi_cuda_error_fatal("ERROR: cuEventRecord error %d\n", res);
    } break;
    default:
      break;
  }
#endif
  KAAPI_PLUGIN_TRACE_OUT
  return 0;
}


/*
 */
int KAAPI_PLUGIN_ENTRYPOINT(stream_process_pending)( kaapi_io_stream_t* ios, int blocking)
{
  KAAPI_PLUGIN_TRACE_IN

  CUresult res;
  kaapi_io_status_t status = {0,0};
  kaapi_cuda_io_stream_t* cios = (kaapi_cuda_io_stream_t*)ios;
  if (ios->pos_rp == ios->pos_wp)
    return 0;

#if defined(_PLUGIN_DEBUG)
  kaapi_cuda_check_ctx();
#endif
  
#if CONFIG_USE_EVENT
#else
  if (blocking)
  {
    res = cuStreamSynchronize( cios->stream );
    KAAPI_PLUGIN_TRACE_MSG("%s: stream %p synchronize, res:%i\n", __FUNCTION__, (void*)cios->stream, (int)res);
  }
  else {
    res = cuStreamQuery( cios->stream );
    if (res == CUDA_ERROR_NOT_READY)
    {
      KAAPI_PLUGIN_TRACE_OUT
      return EINPROGRESS;
    }
    KAAPI_PLUGIN_TRACE_MSG("%s: stream %p query, res:%i\n", __FUNCTION__, (void*)cios->stream, (int)res);
  }
  if (res != CUDA_SUCCESS)
    kaapi_cuda_error_fatal("ERROR: cuStreamSynchronize error %d\n", res);
#endif

  /* call callback functions */
  while (ios->pos_rp != ios->pos_wp)
  {
    int p = ios->pos_rp;
    kaapi_io_instruction_t op = ios->pending[p];
    switch (op.type)
    {
      case KAAPI_IO_KERN:
        /* kaapi_sched_sync( kaapi_self_thread()); */
      case KAAPI_IO_COPY_H2H:
      case KAAPI_IO_COPY_H2D:
      case KAAPI_IO_COPY_D2H:
      case KAAPI_IO_COPY_D2D:
#if CONFIG_USE_EVENT
        if (blocking)
          res = cuEventSynchronize(cios->end_events[p]);
        else
          res = cuEventQuery( cios->end_events[p] );
        if (res == CUDA_ERROR_NOT_READY)
        {
          KAAPI_PLUGIN_TRACE_OUT
          return EINPROGRESS;
        }
        else if (res != CUDA_SUCCESS)
          kaapi_cuda_error_fatal("ERROR: cuEventQuery/cuEventSynchronize error %d\n", res);
#endif
      case KAAPI_IO_END:
      case KAAPI_IO_BARRIER:
        /* commit index for the next event */
        ios->pos_rp = (1+p) % ios->count;
        if (op.inst.cbk.fnc)
          op.inst.cbk.fnc(status, ios, op.inst.cbk.arg[0], op.inst.cbk.arg[1], op.inst.cbk.arg[2]);
        break;

      default:
        kaapi_assert(0);
        break;
    }
#if defined(KAAPI_USE_PERFCOUNTER) && !defined(SYNCHRONOUS)
    kaapi_device_t* self_device = ios->stream->device;
    kaapi_assert_debug(kaapi_offload_self_device() == ios->stream->device);
    switch (op.type)
    {
      case KAAPI_IO_KERN:
          KAAPI_EVENT_PUSH1(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_KERNEL_END, op.inst.k_io.task );
        break;
        case KAAPI_IO_COPY_H2H:
          KAAPI_EVENT_PUSH1(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_HTOH_END, op.inst.c_io.src );
        break;
        case KAAPI_IO_COPY_H2D:
          KAAPI_EVENT_PUSH1(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_HTOD_END, op.inst.c_io.src );
        break;
        case KAAPI_IO_COPY_D2H:
          KAAPI_EVENT_PUSH1(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_DTOH_END, op.inst.c_io.src );
        break;
        case KAAPI_IO_COPY_D2D:
          KAAPI_EVENT_PUSH1(&self_device->rsrc.trace, 0, KAAPI_EVT_OFFLOAD_DTOD_END, op.inst.c_io.src );
        break;
        default:
        break;
    }
#endif
  }
  KAAPI_PLUGIN_TRACE_OUT
  return 0;
}


/*
*/
void* KAAPI_PLUGIN_ENTRYPOINT(get_cublas_handle)(int dev)
{
  kaapi_assert_debug(plugin_initialized == true);
  kaapi_assert_debug((dev >= 0) && (dev < kaapi_device_count) );
#if defined(_PLUGIN_DEBUG)
  kaapi_cuda_check_ctx();
#endif
  return kaapi_cuda_ndevices[dev]->handle;
}
