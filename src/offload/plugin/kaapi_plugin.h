/*
 ** xkaapi
 **
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** Joao.Lima@inf.ufsm.br
 **
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 **
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 **
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 **
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 **
 */
#ifndef __KAAPI_PLUGIN_H_
#define __KAAPI_PLUGIN_H_

#include <stdint.h>

#define KAAPI_PLUGIN_PREFIX_NAME "KAAPI_PLUGIN_"
#define KAAPI_PLUGIN_ENTRYPOINT_NAME( func_name ) KAAPI_PLUGIN_PREFIX_NAME #func_name
#define KAAPI_PLUGIN_ENTRYPOINT( func_name ) KAAPI_PLUGIN_ ## func_name

struct kaapi_io_stream;
struct kaapi_io_instruction;

//#define _PLUGIN_DEBUG   1

#if defined(_PLUGIN_DEBUG)
#  define KAAPI_PLUGIN_TRACE_IN \
    fprintf(stdout, "IN %s/%s\n", _PLUGIN_NAME, __FUNCTION__);\
    fflush(stdout);
#  define KAAPI_PLUGIN_TRACE_OUT \
    fprintf(stdout, "OUT %s/%s\n", _PLUGIN_NAME, __FUNCTION__);\
    fflush(stdout);
#  define KAAPI_PLUGIN_TRACE_MSG(...) \
    fprintf(stdout, __VA_ARGS__);\
    fflush(stdout);
#else
#  define KAAPI_PLUGIN_TRACE_IN
#  define KAAPI_PLUGIN_TRACE_OUT
#  define KAAPI_PLUGIN_TRACE_MSG(...)
#endif

/*
 */
extern char *
KAAPI_PLUGIN_ENTRYPOINT(get_name)(void);

/*
 */
extern unsigned int
KAAPI_PLUGIN_ENTRYPOINT(get_flags)(void);

/*
 */
extern unsigned int
KAAPI_PLUGIN_ENTRYPOINT(get_type)(void);

/*
 */
extern unsigned int
KAAPI_PLUGIN_ENTRYPOINT(get_level)(void);

/*
 */
extern unsigned int
KAAPI_PLUGIN_ENTRYPOINT(get_number)(void);

/*
 */
extern int
KAAPI_PLUGIN_ENTRYPOINT(init)(void);

/*
 */
extern void
KAAPI_PLUGIN_ENTRYPOINT(finalize)(void);

/*
 */
extern int KAAPI_PLUGIN_ENTRYPOINT(device_init)(int dev);

/*
 */
extern void KAAPI_PLUGIN_ENTRYPOINT(device_finalize)(int dev);

/*
 */
extern int KAAPI_PLUGIN_ENTRYPOINT(device_attach)(int dev);

/*
 */
extern void KAAPI_PLUGIN_ENTRYPOINT(device_detach)(int dev);


/*
 */
extern uintptr_t KAAPI_PLUGIN_ENTRYPOINT(alloc)(size_t size);

/*
 */
extern void KAAPI_PLUGIN_ENTRYPOINT(free)(void* p);

/*
 */
extern size_t
KAAPI_PLUGIN_ENTRYPOINT(get_total_mem)(int dev);


/*
 */
extern struct kaapi_io_stream* KAAPI_PLUGIN_ENTRYPOINT(stream_alloc)(
    int device_id,
    int type,
    unsigned int capacity
);

/*
 */
extern void KAAPI_PLUGIN_ENTRYPOINT(stream_free)( struct kaapi_io_stream* ios );

/* May return EINPROGRESS if pending operations already are in the stream & blocking = 0
   Returns 0 if case of success
   Else returns error code
 */
extern int KAAPI_PLUGIN_ENTRYPOINT(stream_process_pending)(
    struct kaapi_io_stream* ios,
    int blocking
);

/*
*/
extern int KAAPI_PLUGIN_ENTRYPOINT(stream_decode_ioinstruction)(
    struct kaapi_io_stream* ios,
    struct kaapi_io_instruction* instr
);

/*
 */
extern void* KAAPI_PLUGIN_ENTRYPOINT(get_cublas_handle)(int dev);

#endif
