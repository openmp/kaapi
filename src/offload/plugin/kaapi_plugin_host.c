/*
 ** xkaapi
 **
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** Joao.Lima@inf.ufsm.br
 **
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 **
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 **
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 **
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 **
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define _PLUGIN_NAME "host"

#include "kaapi_plugin.h"
#include "kaapi_impl.h"

static bool plugin_initialized = false;

char *
KAAPI_PLUGIN_ENTRYPOINT(get_name)(void)
{
  return "host";
}

unsigned int
KAAPI_PLUGIN_ENTRYPOINT(get_flags)(void)
{
  return 0;
}

unsigned int
KAAPI_PLUGIN_ENTRYPOINT(get_type)(void)
{
  return 0x1; /* KAAPI_PROC_TYPE_HOST */
}

unsigned int
KAAPI_PLUGIN_ENTRYPOINT(get_level)(void)
{
  return KAAPI_HWS_LEVELID_OFFLOAD_HOST;
}

unsigned int
KAAPI_PLUGIN_ENTRYPOINT(get_number)(void)
{
  return 1;
}

int
KAAPI_PLUGIN_ENTRYPOINT(init)(void)
{
  KAAPI_PLUGIN_TRACE_IN
  plugin_initialized = true;
  KAAPI_PLUGIN_TRACE_OUT
  return 0;
}

void
KAAPI_PLUGIN_ENTRYPOINT(finalize)(void)
{
  KAAPI_PLUGIN_TRACE_IN
  KAAPI_PLUGIN_TRACE_OUT
}

int KAAPI_PLUGIN_ENTRYPOINT(device_init)(int dev)
{
  KAAPI_PLUGIN_TRACE_IN
  KAAPI_PLUGIN_TRACE_MSG("device %d init\n",dev)
  KAAPI_PLUGIN_TRACE_OUT
  return 0;
}

void KAAPI_PLUGIN_ENTRYPOINT(device_finalize)(int dev)
{
  KAAPI_PLUGIN_TRACE_IN
  KAAPI_PLUGIN_TRACE_MSG("device %d finalize\n",dev)
  KAAPI_PLUGIN_TRACE_OUT
}

int KAAPI_PLUGIN_ENTRYPOINT(device_attach)(int dev)
{
  KAAPI_PLUGIN_TRACE_IN
  KAAPI_PLUGIN_TRACE_MSG("device %d\n",dev)
  KAAPI_PLUGIN_TRACE_OUT
  return 1;
}

void KAAPI_PLUGIN_ENTRYPOINT(device_detach)(int dev)
{
  KAAPI_PLUGIN_TRACE_IN
  KAAPI_PLUGIN_TRACE_MSG("device %d\n",dev)
  KAAPI_PLUGIN_TRACE_OUT
}


uintptr_t KAAPI_PLUGIN_ENTRYPOINT(alloc)(size_t size)
{
  KAAPI_PLUGIN_TRACE_IN
  void* p = malloc(size);
  KAAPI_PLUGIN_TRACE_MSG("alloc ptr=%p size=%ld\n",p, size)
  KAAPI_PLUGIN_TRACE_OUT
  return (uintptr_t)p;
}

void KAAPI_PLUGIN_ENTRYPOINT(free)(void* p)
{
  KAAPI_PLUGIN_TRACE_IN
  KAAPI_PLUGIN_TRACE_MSG("free ptr=%p\n",p)
  free((void*)p);
  KAAPI_PLUGIN_TRACE_OUT
}

size_t
KAAPI_PLUGIN_ENTRYPOINT(get_total_mem)(int dev)
{
  return 0;
}

/* IO stream with specific field for CPU/NUMA
*/
typedef struct kaapi_numa_io_stream_t {
  kaapi_io_stream_t inherited;
  int numa_nodeid;
} kaapi_numa_io_stream_t;


/*
 */
kaapi_io_stream_t* KAAPI_PLUGIN_ENTRYPOINT(stream_alloc)( int device_id, int type, unsigned int capacity )
{
  kaapi_assert_debug(plugin_initialized == true);
  kaapi_assert_debug((device_id >= 0) && (device_id < 1) );
  kaapi_numa_io_stream_t* ios = (kaapi_numa_io_stream_t*)malloc( sizeof(kaapi_numa_io_stream_t));
  if (ios ==0)
    return 0;
  return &ios->inherited;
}

/*
 */
void KAAPI_PLUGIN_ENTRYPOINT(stream_free)( kaapi_io_stream_t* ios )
{
  kaapi_numa_io_stream_t* cios = (kaapi_numa_io_stream_t*)ios;
  free(cios);
}


/*
*/
int KAAPI_PLUGIN_ENTRYPOINT(stream_decode_ioinstruction)(
    kaapi_io_stream_t* ios,
    kaapi_io_instruction_t* instr
)
{
#if defined(_PLUGIN_DEBUG)
static char* name_io[] = {
  "IO_BEGIN",
  "IO_END",
  "IO_COPY_H2H",
  "IO_COPY_H2D",
  "IO_COPY_D2H",
  "IO_COPY_D2D",
  "IO_BARRIER",
  "IO_KERN"
};
#endif

  KAAPI_PLUGIN_TRACE_IN
  KAAPI_PLUGIN_TRACE_MSG("%s: instr '%s'\n", __FUNCTION__, name_io[instr->type]);

  kaapi_device_t* self_device = ios->stream->device;
  kaapi_assert_debug(kaapi_offload_self_device() == ios->stream->device);

  switch (instr->type)
  {
    case KAAPI_IO_BEGIN:
    case KAAPI_IO_END:
      break;

    case KAAPI_IO_COPY_H2H:
    {
      /* other request should have been inserted in other device */
      kaapi_assert(instr->type == KAAPI_IO_COPY_H2H);

      struct kaapi_io_copy* op = &instr->inst.c_io;

      /* switch among view_src type (1D, 2D or 3D). May be some redistribution may be
         implemented here ?
      */
      const char* laddr = op->src;
      char* raddr = op->dest;
      size_t size = kaapi_memory_view_size(op->view_src);
      kaapi_assert( size == kaapi_memory_view_size(op->view_dest));
      switch (op->view_src->type)
      {
        KAAPI_PLUGIN_TRACE_MSG("%s: instr '%s' 1D data\n", __FUNCTION__, name_io[instr->type]);
        case KAAPI_MEMORY_VIEW_1D:
          memcpy( raddr, laddr, size );
          break;
        case KAAPI_MEMORY_VIEW_2D:
        {
          kaapi_assert_debug(op->view_dest->type == KAAPI_MEMORY_VIEW_2D);
          kaapi_assert(op->view_dest->size[0] == op->view_src->size[0]);
          kaapi_assert(op->view_src->size[1] == op->view_dest->size[1]);

          if (kaapi_memory_view_iscontiguous(op->view_src)
           && kaapi_memory_view_iscontiguous(op->view_dest))
            memcpy( raddr, laddr, size );
          else
          {
            KAAPI_PLUGIN_TRACE_MSG("%s: instr '%s' 2D data\n", __FUNCTION__, name_io[instr->type]);
            size_t i;
            size_t size_row = op->view_src->size[1]*op->view_src->wordsize;
            size_t llda = op->view_src->lda * op->view_src->wordsize;
            size_t rlda = op->view_dest->lda * op->view_src->wordsize;

            for (i=0; i<op->view_src->size[0]; ++i, laddr += llda, raddr += rlda)
              memcpy( raddr, laddr, size_row );
          }
          KAAPI_PERFCTR_INCR(self_device->rsrc.perf, KAAPI_PERF_ID_COMM_H2H, size);
          break;
        }
        case KAAPI_MEMORY_VIEW_3D:
        default:
          kaapi_assert(0);
          break;
        };
    } break;

    case KAAPI_IO_BARRIER:
      break;
    case KAAPI_IO_KERN:
    {
      struct kaapi_io_kernel* op = &instr->inst.k_io;
      kaapi_task_body_t body = (op->task->fmt == 0 ? op->task->body :
          kaapi_format_get_task_body_by_arch(op->task->fmt, KAAPI_PROC_TYPE_HOST));
#if defined(KAAPI_USE_PERFCOUNTER)
    kaapi_ressource_t* rsrc = self_device->rsrc;
    kaapi_named_perfctr_t* namedperf =0;
    int fmtid = 0;
    const kaapi_format_t* fmt = pc->fmt;
    if (fmt == 0)
      fmt = kaapi_format_resolve_bybody( pc->body );
    if (fmt ==0)
    {
      namedperf = 0;
      fmtid = 0;
    }
    else {
      namedperf = fmt->perf;
      fmtid = fmt->fmtid;
    }
    kaapi_tracelib_task_begin( rsrc->trclib,
        0,
        0,
        perfctr,
        pc,
        fmtid,
        0
    );
#endif
      body( op->task, kaapi_self_thread() );
#if defined(KAAPI_USE_PERFCOUNTER)
    if (kaapi_perf_idset_test_mask(&kaapi_tracelib_param.perfctr_idset, MASK_PERF_READWRITE_ACCESS))
      _kaapi_count_access(rsrc->trclib, rsrc->numaid, pc);
    op->task->Tinf = (uint32_t) kaapi_tracelib_task_end(
        rsrc->trclib,
        rsrc->kid,
        namedperf,
        0,
        0,
        perfctr,
        op->task, op->task->Tinf
    );
#endif
    } break;

    case KAAPI_IO_COPY_H2D: /* this plugin cannot handle device to host comm */
    case KAAPI_IO_COPY_D2H:
    case KAAPI_IO_COPY_D2D:
    default:
      kaapi_assert(0);
      break;
  }
  KAAPI_PLUGIN_TRACE_OUT
  return 0;
}


/*
*/
int KAAPI_PLUGIN_ENTRYPOINT(stream_process_pending)( kaapi_io_stream_t* ios, int blocking)
{
  kaapi_io_status_t status = {0,0};

  if (ios->pos_r == ios->pos_rp) /* no pending */
    return 0;

  KAAPI_PLUGIN_TRACE_IN

  /* call callback functions */
  while (ios->pos_rp != ios->pos_r)
  {
    int p = ios->pos_rp;
    kaapi_io_instruction_t* op = &ios->instr[p];
    switch (op->type)
    {
      case KAAPI_IO_END:
      case KAAPI_IO_BARRIER:
      case KAAPI_IO_KERN:
        if (op->inst.cbk.fnc)
          op->inst.cbk.fnc( status, ios,
                op->inst.cbk.arg[0],
                op->inst.cbk.arg[1],
                op->inst.cbk.arg[2]);
        break;

      default:
        break;
    }
    ios->pos_rp = (1+p) % ios->count;
  }
  KAAPI_PLUGIN_TRACE_OUT
  return 0;
}



