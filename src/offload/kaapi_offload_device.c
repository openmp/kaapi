/*
 ** xkaapi
 **
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** Joao.Lima@inf.ufsm.br
 **
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 **
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 **
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 **
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 **
 */

#include <stdio.h>
#include "kaapi_impl.h"

/* WARNING:
   - method called indirectly by a combiner thread.
*/
static void callback_epilogue(
    kaapi_io_status_t status,
    kaapi_io_stream_t* ios,
    void* arg0, void* arg1, void* arg2
)
{
#if defined(_OFFLOAD_DEBUG)
  fprintf(stdout, "%s\n", __FUNCTION__);
  fflush(stdout);
#endif
  kaapi_device_t* device = ios->stream->device;
  kaapi_offload_place_t* place =(kaapi_offload_place_t*)device->rsrc.ownplace;
  kaapi_task_t* task = (kaapi_task_t*)arg0;
  kaapi_assert_debug(KAAPI_ATOMIC_READ(&task->wc) ==0);

  /* TODO: move these two lines to callback_validate_data
     [TG]: I think no, because here the task is completed (data may be anywhere).
     The running thread may activate successors of the task taking care of data mapping.
  */
  kaapi_writemem_barrier();
  KAAPI_ATOMIC_INCR(&task->state.frame->cnt_exec);

  /* activate successors and insert them following the push strategy */
  kaapi_list_t readytasks;
  kaapi_list_init(&readytasks);
  kaapi_sched_activate_succ( &device->rsrc, task, 0, &readytasks);
  kaapi_atomic_lock( &place->inherit.inherit.lock );
  while (!kaapi_list_empty(&readytasks))
  {
    kaapi_task_t* top = kaapi_list_pop_tail(&readytasks);
    kaapi_list_push_head(&place->readytasks, top);
  }
  kaapi_atomic_unlock( &place->inherit.inherit.lock );
#if defined(KAAPI_OFFLOAD_PROGRESS_THREAD)
  kaapi_offload_signal();
#endif
}


/* Call when data have been sent
   When all data are valid for this task, insert the task into the stream
*/
static void callback_set_valid(
    kaapi_io_status_t status,
    kaapi_io_stream_t* ios,
    void* arg0, void* arg1, void* arg2
)
{
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_metadata_info_t* mdi     = (kaapi_metadata_info_t*)arg0;
  kaapi_task_t* task             = (kaapi_task_t*)arg1;
  kaapi_offload_stream_t* stream = (kaapi_offload_stream_t*)arg2;
  kaapi_memory_replica_set_valid(mdi, ios->stream->device->memnode.asid);

  if ((task !=0) && (KAAPI_ATOMIC_DECR(&task->wc) ==0))
  {
    /* launch task */
    kaapi_stream_insert_io_task_inst(
      stream,
      KAAPI_IO_STREAM_KERN,
      task,
      callback_epilogue,
      task, 0, 0
    );
#if defined (KAAPI_USE_PERFCOUNTER)
    KAAPI_PERFCTR_INCR(stream->device->rsrc.perfkproc, KAAPI_PERF_ID_OFFLOADTASK, 1);
#endif
  }
  KAAPI_OFFLOAD_TRACE_OUT
}


/* Bind parameters, execute task and unbind parameters */
int kaapi_offload_device_run(
     kaapi_device_t* const device,
     kaapi_task_body_t body,
     kaapi_task_t*     task,
     kaapi_thread_t*   thread
 )
{
  KAAPI_OFFLOAD_TRACE_IN

  const kaapi_format_t* fmt = task->fmt;
  unsigned int count_params;
  unsigned int ith;
  kaapi_memory_view_t view;
  kaapi_access_t* access;
  kaapi_metadata_info_t* mdi;
  kaapi_data_replica_t* replica = 0;
  kaapi_address_space_id_t src_asid;
  kaapi_data_replica_t *src_replica, *dest_replica;

  kaapi_assert_debug(KAAPI_ATOMIC_READ(&task->wc) ==0);
  /* incr: keep lock on task before send to the device by the progress thread */
  KAAPI_ATOMIC_INCR(&task->wc);

  if ((fmt !=0) && ((task->u.s.flag & KAAPI_TASK_FLAG_INDPENDENT) ==0))
  {
    count_params = kaapi_format_get_count_params(fmt, kaapi_task_getargs(task));
    for(ith= 0; ith < count_params; ++ith)
    {
      kaapi_access_mode_t m = kaapi_format_get_mode_param(fmt, ith, kaapi_task_getargs(task));
      kaapi_access_mode_t mp = KAAPI_ACCESS_GET_MODE(m);
      if (mp & KAAPI_ACCESS_MODE_V)
        continue;

      void* new_data;

      /* do bind ptr to the device->asid */
      access = kaapi_format_get_access_param(fmt, (unsigned int)ith, kaapi_task_getargs(task));
      kaapi_format_get_view_param(fmt, (unsigned int)ith, kaapi_task_getargs(task), &view);

      /* Returns a replica of the data in asid.
         If data is not already in the DSM, then add it.
         access->data is always the host pointer, except from above in the function
      */
      mdi = kaapi_memory_bind(device->memnode.asid, kaapi_local_asid, access->data, &view, &replica);

      /* store in the data access the pointer translated by the original offset */
      new_data = kaapi_memory_view2Npointer(kaapi_pointer2void(replica->ptr), &replica->view);

      if (KAAPI_ACCESS_IS_READ(mp))
      {
        /* */
        if (!kaapi_memory_replica_is_valid(mdi, device->memnode.asid))
        {
          /* old: kaapi_memory_replica_validate(mdi, device->asid, task, ith); */
          src_asid = kaapi_memory_choose_valid_asid(mdi);

          kaapi_assert_debug(kaapi_memory_replica_is_valid(mdi, src_asid));

          src_replica  = kaapi_memory_replica_get(mdi, src_asid);
          dest_replica = kaapi_memory_replica_get(mdi, device->memnode.asid);
          if (kaapi_memory_asid_get_arch(src_asid) == KAAPI_PROC_TYPE_HOST)
            src_replica->view = view;
          KAAPI_OFFLOAD_TRACE_MSG("%s: COPY TASK:%p src:@%p, %li,%li,%li -> dest:@%p, %li,%li, %li\n", __FUNCTION__,
              (void*)task,
              (void*)src_replica->ptr.ptr,
              src_replica->view.size[0], src_replica->view.size[1], src_replica->view.lda,
              (void*)dest_replica->ptr.ptr,
              dest_replica->view.size[0], dest_replica->view.size[1], dest_replica->view.lda
          )
          kaapi_assert( device->memnode.asid != src_asid );
          KAAPI_ATOMIC_INCR(&task->wc);
          kaapi_memory_copy(
              dest_replica->ptr, &dest_replica->view,
              src_replica->ptr, &src_replica->view,
              callback_set_valid,
              (void*)mdi,
              (void*)task,
              (void*)device->stream
          );
        }
#if defined(KAAPI_DEBUG)
else {
  src_replica = kaapi_memory_replica_get(mdi, device->memnode.asid);
  KAAPI_OFFLOAD_TRACE_MSG("%s: ALREADY VALID TASK:%p src:@%p\n", __FUNCTION__,
      (void*)task,
      (void*)src_replica->ptr.ptr
  )
}
#endif
      }
      if (KAAPI_ACCESS_IS_WRITE(mp))
        kaapi_memory_replica_set_all_dirty_except(mdi, device->memnode.asid);

      kaapi_assert_debug(access->version == 0);
      /* update pointer in the task arguments */
      access->version = access->data;
      access->data    = new_data;
      kaapi_format_set_access_param(fmt, ith, kaapi_task_getargs(task), access );
      kaapi_format_set_view_param(fmt, ith, kaapi_task_getargs(task), &replica->view );
    }
  }

  /* if nothing remote, then insert task for execution in the device stream */
  if (KAAPI_ATOMIC_DECR(&task->wc)==0)
  {
    kaapi_stream_insert_io_task_inst(
      device->stream,
      KAAPI_IO_STREAM_KERN,
      task,
      callback_epilogue,
      task, 0, 0
    );
#if defined (KAAPI_USE_PERFCOUNTER)
    KAAPI_PERFCTR_INCR(device->rsrc.perfkproc, KAAPI_PERF_ID_OFFLOADTASK, 1);
#endif
  }

  KAAPI_OFFLOAD_TRACE_OUT
  return 0;
}


/*
*/
int kaapi_offload_device_init(kaapi_device_t* const device)
{
  int err = 0;
  if (device->is_initialized) 
    goto return_value;

  err = device->f_device_init(device->device_id);
  if (err != 0)
    goto return_value;

  device->stream = kaapi_offload_stream_init(device, 256);
  device->save_rsrc = 0;
  device->rsrc.numaid = 0;
  device->rsrc.kid = device->device_id;

#if defined(KAAPI_USE_PERFCOUNTER)
  device->rsrc.perfkproc = kaapi_tracelib_thread_init(
    &device->rsrc.evtkproc,
    offload_team,
    KAAPI_PROC_TYPE_GPU,
    device->rsrc.kid,
    device->rsrc.numaid,
    KAAPI_PERF_SYS_STATE
  );
  kaapi_tracelib_thread_start(device->rsrc.perfkproc, device->rsrc.evtkproc);
#endif

  device->is_initialized = true;

return_value:
  return err;
}

/*
*/
void kaapi_offload_device_finalize(kaapi_device_t* const device)
{
  if (device->is_initialized)
  {
#if defined(KAAPI_USE_PERFCOUNTER)
    kaapi_tracelib_thread_stop(device->rsrc.perfkproc, device->rsrc.evtkproc);
    kaapi_tracelib_thread_fini(device->rsrc.perfkproc, device->rsrc.evtkproc);
#endif
    kaapi_offload_stream_destroy(device->stream);
    device->f_device_finalize(device->device_id);
    device->is_initialized = false;
  }
}


/*
*/
kaapi_device_t* kaapi_offload_device_push(kaapi_device_t* const device)
{
  kaapi_device_t* save_device;
  save_device = kaapi_offload_get_current_device();

  KAAPI_OFFLOAD_TRACE_MSG("IN %s: current_device:%p,%i -> device to attach:%p, %i\n", __FUNCTION__,
                (void*)save_device, (save_device==0 ? -1 : save_device->gid),
                (void*)device, (device==0 ? -1 : device->gid)
  );

  if (KAAPI_ATOMIC_INCR(&device->cnt_push) >1)
  {
    kaapi_assert_debug( device == save_device );
    kaapi_assert_debug( device->save_rsrc == kaapi_self_rsrc() );
    goto out;
  }

  kaapi_offload_set_current_device(device);
  /* plugin specific */
  kaapi_assert(device->f_device_attach(device->device_id) !=0);
  device->save_rsrc = kaapi_self_rsrc();
  kaapi_set_self_ressource( &device->rsrc );

out:
  KAAPI_OFFLOAD_TRACE_OUT
  return save_device;
}


/* device is the device returned by push
*/
void kaapi_offload_device_pop(kaapi_device_t* const device)
{
  kaapi_device_t* curr_device = kaapi_offload_get_current_device();

  KAAPI_OFFLOAD_TRACE_MSG("IN %s: current_device:%p,%i -> device to attach:%p, %i\n", __FUNCTION__,
                (void*)curr_device, (curr_device==0 ? -1 : curr_device->gid),
                (void*)device, (device==0 ? -1 : device->gid)
  );

  if (KAAPI_ATOMIC_DECR(&curr_device->cnt_push) !=0)
  {
    kaapi_assert_debug( device == curr_device );
    kaapi_assert_debug( device->save_rsrc == kaapi_self_rsrc() );
    goto out;
  }

  curr_device->f_device_detach(curr_device->device_id);
  kaapi_set_self_ressource( curr_device->save_rsrc );
  kaapi_offload_set_current_device( device );

out:
  KAAPI_OFFLOAD_TRACE_OUT;
}


/*
*/
extern void* kaapi_get_cublas_handle(void);
void* kaapi_get_cublas_handle(void)
{
  kaapi_device_t* device = kaapi_offload_get_current_device();
  return device->cublas_handle;
}
