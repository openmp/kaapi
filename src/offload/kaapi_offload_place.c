/*
 ** xkaapi
 **
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** Joao.Lima@inf.ufsm.br
 **
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 **
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 **
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 **
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 **
 */

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <dirent.h>

#include <string.h>
#include <limits.h>
#include "kaapi_impl.h"


#define KERN_WINDOWS 8


/* This function is  make all communications progress through all the stream.
   A a thread or a set of threads management communication progress for the device, this
   function becomes not necessary.
   This function must be called by the agregation protocol through call
   to kaapi_place_internalop for instance.
*/
static int _kaapi_offload_poll_device( kaapi_place_t* ld, void* arg)
{
  int err =0;
  kaapi_device_t* device = (kaapi_device_t*)arg;
  kaapi_assert_debug( device->is_initialized );
  kaapi_assert_debug( ld == device->rsrc.ownplace );
  kaapi_assert_debug( device->rsrc.ownplace !=0 );

  if (kaapi_offload_stream_size(device->stream, KAAPI_IO_STREAM_KERN) < KERN_WINDOWS)
  {
    /* do not use extern interface kaapi_place_pop that will make recursive call to 
       the agregation protocol
    */
    kaapi_offload_place_t* place =(kaapi_offload_place_t*)ld;
    kaapi_task_t* task = kaapi_queue_pop_head(&place->inherit.queue);
    if (task != 0)
    {
      kaapi_task_body_t body
          = kaapi_format_get_task_body_by_arch( task->fmt, device->rsrc.proc_type );

      err = kaapi_offload_device_run(
        device,
        body,
        task,
        kaapi_self_thread()
      );
      kaapi_assert(err ==0);
    }
  }

  /* two steps decoding. to rewrite in order to implement kind of order */
  if (!kaapi_offload_stream_isempty(device->stream,KAAPI_IO_STREAM_ALL))
  {
    err = kaapi_offload_stream_process_instruction( device->stream, KAAPI_IO_STREAM_ALL );
    kaapi_assert_debug(err == 0);
    err = kaapi_offload_test_stream(device->stream, KAAPI_IO_STREAM_ALL);
    kaapi_assert_debug( (err == 0) || (err == EINPROGRESS));
  }
  return err;
}


/* Call agregation protocol to ensure serialisation
*/
int kaapi_offload_poll_device(kaapi_device_t* device)
{
  kaapi_ressource_t* save_rsrc = kaapi_self_rsrc();
  kaapi_set_self_ressource( &device->rsrc );

  /* process operations that may have recursive call to the agregation protoocol */
  kaapi_offload_place_t* place = (kaapi_offload_place_t*)device->rsrc.ownplace;

  kaapi_atomic_lock( &place->inherit.inherit.lock );
  kaapi_list_t keepcopyreadytasks = place->readytasks;
  kaapi_list_t readytasks = keepcopyreadytasks;
  kaapi_list_init( &place->readytasks );;
  kaapi_atomic_unlock( &place->inherit.inherit.lock );

  kaapi_assert( 0 == kaapi_default_param.wspush( &device->rsrc,
                                                 device->rsrc.ownplace,
                                                 &readytasks)
  );

  /* process internal concurrent operations */
  int err = kaapi_place_internalop(
      &device->rsrc,
      device->rsrc.ownplace,
      &_kaapi_offload_poll_device, device );

  kaapi_set_self_ressource( save_rsrc );
  return err;
}


/*
*/
static void _kaapi_offload_place_steal_task(
    kaapi_place_t* ld,
    struct kaapi_listrequest_iterator_t* lri
)
{
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_offload_place_t* place =(kaapi_offload_place_t*)ld;
  if (place->device == kaapi_offload_get_host_device())
    return;

//    _kaapi_offload_poll_device( ld, place->device );
  KAAPI_OFFLOAD_TRACE_OUT
  return;
}


/*
*/
static void _kaapi_offload_place_nsteal_task(
  struct kaapi_place_t* ld,
  struct kaapi_listrequest_iterator_t* lri
)
{
  KAAPI_OFFLOAD_TRACE_IN
//  kaapi_offload_place_t* place = (kaapi_offload_place_t*)ld;

//  _kaapi_offload_poll_device( ld, place->device );
  KAAPI_OFFLOAD_TRACE_OUT
}


/*
*/
static kaapi_task_t* _kaapi_offload_place_pop_task( kaapi_place_t* ld)
{
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_offload_place_t* place =(kaapi_offload_place_t*)ld;
  kaapi_task_t* task = kaapi_queue_pop_head(&place->inherit.queue);
//    _kaapi_offload_poll_device( ld, place->device );
  KAAPI_OFFLOAD_TRACE_OUT
  return task;
}


/*
*/
static int _kaapi_offload_place_push_task( kaapi_place_t* ld, kaapi_task_t* task)
{
  KAAPI_OFFLOAD_TRACE_IN;
  int err = 0;
  kaapi_offload_place_t* place =(kaapi_offload_place_t*)ld;
#if defined(KAAPI_OFFLOAD_PROGRESS_THREAD)
  if (kaapi_offload_stream_size(place->device->stream, KAAPI_IO_STREAM_KERN) < KERN_WINDOWS)
  {
    kaapi_task_body_t body
        = kaapi_format_get_task_body_by_arch(task->fmt, place->device->rsrc.proc_type);
    err = kaapi_offload_device_run(
      place->device,
      body,
      task,
      kaapi_self_thread()
    );
    if (err) goto return_value;
  }
  else
#endif
    kaapi_queue_push_tail( &place->inherit.queue, task );

#if defined(KAAPI_OFFLOAD_PROGRESS_THREAD)
  kaapi_offload_signal();
#endif

return_value:
  KAAPI_OFFLOAD_TRACE_OUT;
  return err;
}


/*
*/
static int _kaapi_offload_place_push_list( kaapi_place_t* ld, kaapi_list_t* list)
{
  KAAPI_OFFLOAD_TRACE_IN;
  int err = 0;
  while (!kaapi_list_empty(list))
  {
    kaapi_task_t* task = kaapi_list_pop_head(list);
    err |= _kaapi_offload_place_push_task(ld, task);
  }
  KAAPI_OFFLOAD_TRACE_OUT;
  return err;
}


/*
*/
static int _kaapi_offload_place_isempty( const kaapi_place_t* ld)
{
  const kaapi_offload_place_t* place =(kaapi_offload_place_t*)ld;
  return kaapi_queue_empty(&place->inherit.queue)
      && kaapi_queue_empty(&place->inherit.private_queue)
      && kaapi_offload_stream_isempty( place->device->stream, KAAPI_IO_STREAM_ALL);
}


/*
*/
static void _kaapi_offload_place_destroy( kaapi_place_t* ld )
{
  kaapi_offload_place_t* place =(kaapi_offload_place_t*)ld;
  KAAPI_OFFLOAD_TRACE_IN
  place->device = 0;
  KAAPI_OFFLOAD_TRACE_OUT
}


/*
*/
static struct kaapi_vtable_place_t default_vtable_offload ={
  .fc_steal       = 0,
  .fc_stealin     = 0,
  .fc_pop         = 0,
  .fc_push        = 0,
  .fc_push_remote = 0,
  .fc_pushlist    = 0,
  .f_isempty      = _kaapi_offload_place_isempty,
  .f_destroy      = _kaapi_offload_place_destroy,
  .fs_steal       = _kaapi_offload_place_steal_task,
  .fs_stealin     = _kaapi_offload_place_nsteal_task,
  .fs_pop         = _kaapi_offload_place_pop_task,
  .fs_push        = _kaapi_offload_place_push_task,
  .fs_push_remote = _kaapi_offload_place_push_task,
  .fs_pushlist    = _kaapi_offload_place_push_list
};


static void kaapi_init_offload_place(
    kaapi_offload_place_t* place,
    kaapi_device_t* device
)
{
  kaapi_init_plainplace( &place->inherit );
  default_vtable_offload.fc_steal       = place->inherit.inherit.vtable->fc_steal;
  default_vtable_offload.fc_stealin     = place->inherit.inherit.vtable->fc_stealin;
  default_vtable_offload.fc_pop         = place->inherit.inherit.vtable->fc_pop;
  default_vtable_offload.fc_push        = place->inherit.inherit.vtable->fc_push;
  default_vtable_offload.fc_push_remote = place->inherit.inherit.vtable->fc_push_remote;
  default_vtable_offload.fc_pushlist    = place->inherit.inherit.vtable->fc_pushlist;
  place->inherit.inherit.vtable = &default_vtable_offload;
  kaapi_list_init( &place->readytasks );
  place->device         = device;
}


static void* kaapi_offload_node_alloc(kaapi_memory_node_t* node, size_t size)
{
  kaapi_assert_debug(node != 0);
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_device_t* device = (kaapi_device_t*)node;
  kaapi_device_t* saved_device = kaapi_offload_device_push( device );
  void* ptr = (void*)device->f_alloc(size);
  kaapi_offload_device_pop( saved_device );
  KAAPI_OFFLOAD_TRACE_OUT
  return ptr;
}

static void kaapi_offload_node_free(kaapi_memory_node_t* node, void *ptr)
{
  kaapi_assert_debug(node != 0);
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_device_t* device = (kaapi_device_t*)node;
  kaapi_device_t* saved_device = kaapi_offload_device_push( device );
  device->f_free((uintptr_t)ptr);
  kaapi_offload_device_pop( saved_device );
  KAAPI_OFFLOAD_TRACE_OUT
}


static int kaapi_offload_node_copy(
    kaapi_memory_node_t*       node,
    kaapi_pointer_t            dest,
    const kaapi_memory_view_t* view_dest,
    kaapi_pointer_t            src,
    const kaapi_memory_view_t* view_src,
    kaapi_io_cbk_fnc_t         cbk,
    void*                      arg0,
    void*                      arg1,
    void*                      arg2
)
{
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_device_t* device = (kaapi_device_t*)node;
  kaapi_assert_debug(&device->memnode == node);

  kaapi_assert( (dest.asid !=KAAPI_PROC_TYPE_HOST) && (src.asid != KAAPI_PROC_TYPE_HOST) );


  if (kaapi_memory_asid_get_arch(src.asid) == KAAPI_PROC_TYPE_HOST)
    /* verify iff all inputs are in local node */
    kaapi_stream_insert_io_copy_inst(
        device->stream,
        KAAPI_IO_STREAM_OUTPUT,
        KAAPI_IO_COPY_H2D,
        kaapi_pointer2void(src), view_src,
        kaapi_pointer2void(dest), view_dest,
        cbk, arg0, arg1, arg2
    );
  else if (kaapi_memory_asid_get_arch(dest.asid) == KAAPI_PROC_TYPE_HOST)
    /* verify iff all inputs are in local node */
    kaapi_stream_insert_io_copy_inst(
        device->stream,
        KAAPI_IO_STREAM_INPUT,
        KAAPI_IO_COPY_D2H,
        kaapi_pointer2void(src), view_src,
        kaapi_pointer2void(dest), view_dest,
        cbk, arg0, arg1, arg2
    );
  else
    kaapi_stream_insert_io_copy_inst(
       device->stream,
       KAAPI_IO_STREAM_INPUT,
       KAAPI_IO_COPY_D2D,
       kaapi_pointer2void(src), view_src,
       kaapi_pointer2void(dest), view_dest,
       cbk, arg0, arg1, arg2
   );


  KAAPI_OFFLOAD_TRACE_OUT
  return EBUSY;
}



static int _kaapi_offload_memsync( kaapi_place_t* ld, void* arg)
{
  int err =0;
  kaapi_device_t* device = (kaapi_device_t*)arg;
  kaapi_assert_debug( device->is_initialized );
  kaapi_assert_debug( ld == device->rsrc.ownplace );
  kaapi_assert_debug( device->rsrc.ownplace !=0 );

  err = kaapi_offload_stream_process_instruction( device->stream, KAAPI_IO_STREAM_OUTPUT );
  if (err) return err;
  err = kaapi_offload_wait_stream( device->stream, KAAPI_IO_STREAM_OUTPUT);
  if (err) return err;
  err = kaapi_offload_stream_process_instruction( device->stream, KAAPI_IO_STREAM_KERN );
  if (err) return err;
  err = kaapi_offload_wait_stream( device->stream, KAAPI_IO_STREAM_KERN);
  if (err) return err;
  err = kaapi_offload_stream_process_instruction( device->stream, KAAPI_IO_STREAM_INPUT );
  if (err) return err;
  err = kaapi_offload_wait_stream( device->stream, KAAPI_IO_STREAM_INPUT);
  return err;
}

void  kaapi_offload_node_memsync(struct kaapi_memory_node_t* node, int begend)
{
  kaapi_assert_debug(node != 0);
  kaapi_assert_debug((begend ==0) || (begend ==1)) ;
  KAAPI_OFFLOAD_TRACE_IN

  kaapi_device_t* device = (kaapi_device_t*)node;

  kaapi_ressource_t* save_rsrc = kaapi_self_rsrc();
  kaapi_set_self_ressource( &device->rsrc );
  int err __attribute__((unused)) = kaapi_place_internalop(
      &device->rsrc,
      device->rsrc.ownplace,
      &_kaapi_offload_memsync, device );
  kaapi_set_self_ressource( save_rsrc );

  KAAPI_OFFLOAD_TRACE_OUT
}

size_t  kaapi_offload_node_get_total_mem(struct kaapi_memory_node_t* node)
{
  kaapi_assert_debug(node != 0);
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_device_t* device = (kaapi_device_t*)node;
  size_t retval = (size_t)-1UL;
  if (device->is_initialized)
  {
    kaapi_device_t* save_device = kaapi_offload_device_push( device );
    retval = device->f_get_total_mem( device->device_id );
    kaapi_offload_device_pop( save_device );
  }
  KAAPI_OFFLOAD_TRACE_OUT
  return retval;
}

void kaapi_offload_register_node(kaapi_device_t* device)
{
  KAAPI_OFFLOAD_TRACE_IN
  device->memnode.f_alloc = kaapi_offload_node_alloc;
  device->memnode.f_free  = kaapi_offload_node_free;
  device->memnode.f_copy  = kaapi_offload_node_copy;
  device->memnode.f_memsync = kaapi_offload_node_memsync;
  device->memnode.f_get_total_mem = kaapi_offload_node_get_total_mem;

  /* do not create extra asid for the host place */
  if (kaapi_offload_get_host_device() == device)
  {
    kaapi_memory_node_unregister( kaapi_local_asid );
    kaapi_memory_node_register( kaapi_local_asid, &device->memnode );
  }
  else
  {
    kaapi_address_space_id_t asid = kaapi_memory_create_asid(
        kaapi_memory_get_current_globalid(),
        0, /* device->device_id, */
        device->rsrc.proc_type
    );
    kaapi_memory_node_register( asid, &device->memnode );
  }

  KAAPI_OFFLOAD_TRACE_OUT
}


/* Add one places in the KAAPI_HWS_LEVELID_OFFLOAD_XX level set of places
*/
int kaapi_offload_init_places(void)
{
  int i;
  kaapi_device_t* device;
  kaapi_placeset_t* placeset;

  int count_per_type[KAAPI_HWS_LEVELID_MAX-KAAPI_HWS_LEVELID_OFFLOAD_HOST];
  memset( &count_per_type, 0, sizeof(count_per_type));
  for(i = 0; i < kaapi_offload_get_num_devices(); ++i)
  {
    device = kaapi_offload_device( i );
    ++count_per_type[device->hwslevel-KAAPI_HWS_LEVELID_OFFLOAD_HOST];
  }

  for (i=KAAPI_HWS_LEVELID_OFFLOAD_HOST; i<KAAPI_HWS_LEVELID_MAX; ++i)
  {
    placeset = &kaapi_default_param.places_set[i];
    size_t count = count_per_type[i-KAAPI_HWS_LEVELID_OFFLOAD_HOST];
    placeset->places = calloc(count, sizeof(kaapi_place_t*));
  }

  for(i = 0; i < kaapi_offload_get_num_devices(); i++)
  {
    device = kaapi_offload_device( i );
    placeset = &kaapi_default_param.places_set[device->hwslevel];
    kaapi_offload_register_node(device);
    kaapi_offload_place_t* theplace;
    theplace = (kaapi_offload_place_t*)malloc( sizeof(kaapi_offload_place_t));
    kaapi_init_offload_place( theplace, device );
    device->rsrc.ownplace = (kaapi_place_t*)theplace;

//    if (device != kaapi_offload_get_host_device())
    placeset->places[placeset->count++] = device->rsrc.ownplace;
#if defined(_OFFLOAD_DEBUG)
  fprintf(stdout, "%s: register memory\n", __FUNCTION__);
  fflush(stdout);
#endif
  }
  return 0;
}
