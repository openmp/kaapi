/*
 ** xkaapi
 **
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** Joao.Lima@inf.ufsm.br
 **
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 **
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 **
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 **
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 **
 */

/*
 * XKaapi interface for Offload plugins. Each plugin can load several devices.
 *
 * Based on the GCC libgomp plugins (2014) and clang offloading for Intel.
 * Extended to allow highly asynchronous communication between devices & host
 */

#ifndef _KAAPI_OFFLOAD_H_
#define _KAAPI_OFFLOAD_H_

#include "kaapi_offload_dbg.h"

#define KAAPI_PLUGIN_PREFIX_NAME "KAAPI_PLUGIN_"
#define KAAPI_PLUGIN_ENTRYPOINT_NAME( func_name ) KAAPI_PLUGIN_PREFIX_NAME #func_name
#define KAAPI_PLUGIN_ENTRYPOINT( func_name ) KAAPI_PLUGIN_ ## func_name



/* Use to have a thread making communication progression
*/
//#define KAAPI_OFFLOAD_PROGRESS_THREAD 1

#include "kaapi_offload_stream.h"


#if !defined(KAAPI_USE_OFFLOAD)

static inline unsigned int kaapi_offload_get_num_devices(void)
{ return 0; }

#else

/*
*/
struct kaapi_io_stream;
struct kaapi_io_instruction;
struct kaapi_device;


#if defined(KAAPI_USE_PERFCOUNTER)
/* Team for stat about all devices */
extern kaapi_team_perfstat_t* offload_team;
#endif

/* Place for offloading device.
   Each device has a kaapi_offload_place that store a communication queue
   between other devices and a pointer to the associated device.
   The global agregation protocol ensure that the thread entering in the kaapi_offload_place
   is unique.
*/
typedef struct kaapi_offload_place {
  kaapi_plainplace_t   inherit;
  kaapi_list_t         readytasks;     /* ready tasks activated because of execution */
                                       /* will be pushed to remote places using default wspush*/
  struct kaapi_device* device;
} kaapi_offload_place_t;


/* Kaapi device.
   Each remote computing ressource is currently view as a device.
*/
typedef struct kaapi_device {
    kaapi_memory_node_t      memnode;    /* must be first because ptr casted to kaapi_device* */
    kaapi_offload_stream_t*  stream;     /* communication streams between local node and device */
    kaapi_ressource_t        rsrc;       /* the ressource with its place */
    kaapi_ressource_t*       save_rsrc;  /* when device is pushed */
    kaapi_atomic_t           cnt_push;   /* number of times the ressource is pushed */

    const char*              name;	     /* Device name */
    unsigned int             flags;	     /* Device flags */
    kaapi_hws_levelid_t      hwslevel;   /* hws level */
    unsigned int             device_id;	/* Internal id for a specific device type (ordering) */
    unsigned int             gid;	/* Global id of the device */
    int                      is_initialized;	/* True if device is initialized */
    
    void*                    handle;        /* plugin handle */
    void*                    cublas_handle; /* cudablas handle */

//    pthread_mutex_t          lock;

    /* Function handlers */
    const char   *(*f_get_name)(void);
    unsigned int (*f_get_flags)(void);
    unsigned int (*f_get_type)(void);
    unsigned int (*f_get_level)(void);
    unsigned int (*f_get_number)(void);
    int (*f_init)(void);
    void (*f_finalize)(void);
    int (*f_device_init)(int);
    void (*f_device_finalize)(int);
    int (*f_device_attach)(int);
    void (*f_device_detach)(int);
    uintptr_t (*f_alloc)(size_t);
    void (*f_free)(uintptr_t);
    size_t (*f_get_total_mem)(int);

    /* stream functions */
    struct kaapi_io_stream* (*f_stream_alloc)( int, int, unsigned int);
    void (*f_stream_free)( struct kaapi_io_stream* );
    int (*f_stream_process_pending )( struct kaapi_io_stream*, int );
    int (*f_stream_decode_ioinstruction)( struct kaapi_io_stream*, struct kaapi_io_instruction*);
  
    /* cublas support */
    void* (*f_get_cublas_handle)(int);
} kaapi_device_t;


/* API methods */
extern kaapi_device_t* kaapi_offload_get_host_device(void);

/* Submit task to offloading ressources "host"
*/
extern int kaapi_offload_host_pushtask(kaapi_task_t* task);


/** \ingroup Offload
 Search for xkaapi plugins in KAAPI_PLUGIN_PATH and initilize then. */
extern int kaapi_offload_init(int flag);

/** \ingroup Offload
  Initialize places corresponding to created device in kaapi_offload_init.
*/
extern int kaapi_offload_init_places(void);

/** \ingroup Offload
  Start offload module. Must be called after kaapi_offload_init_places.
*/
extern int kaapi_offload_start(void);

/** \ingroup Offload
 Finalize all devices from plugins */
extern void kaapi_offload_finalize(void);

/** \ingroup Offload
  Return a descriptor of the current offload device.
 \retval 0 if the current thread is not a device.
 \retval a pointer to the current offload device.
 */
extern kaapi_device_t* kaapi_offload_self_device(void);


/*
*/
static inline kaapi_address_space_id_t kaapi_offload_self_device_get_asid(void)
{
    kaapi_assert_debug(kaapi_offload_self_device() != NULL);
    kaapi_assert_debug(kaapi_offload_self_device()->is_initialized == true);
    return kaapi_offload_self_device()->memnode.asid;
}

/** \ingroup Offload
  It inits a device and attaches to the current thread.
 */
extern int kaapi_offload_device_init(kaapi_device_t* const device);

/** \ingroup Offload
 */
extern void kaapi_offload_device_finalize(kaapi_device_t* const device);

/** \ingroup Offload
 */
extern int kaapi_offload_device_run( kaapi_device_t* const device,
                            kaapi_task_body_t body, kaapi_task_t* task,
                            kaapi_thread_t* thread);

/** \ingroup Offload
 */
extern kaapi_device_t* kaapi_offload_device_push(kaapi_device_t* const device);

/** \ingroup Offload
     device is the device returned by push
 */
extern void kaapi_offload_device_pop(kaapi_device_t* const device);


/** \ingroup Offload
 */
extern unsigned int kaapi_offload_get_num_devices(void);

/** \ingroup Offload
 */
extern kaapi_device_t* kaapi_offload_device(int devid);

/** \ingroup Offload
 */
extern void kaapi_offload_register_node(kaapi_device_t* device);

static inline bool kaapi_offload_is_active(void){
  if(kaapi_offload_get_num_devices() > 0)
    return true;
  else
    return false;
}


/** \ingroup Offload
 * Synchronize pending memory operations.
 */
extern void kaapi_offload_node_memsync(struct kaapi_memory_node_t* node, int begend);

/** \ingroup Offload
 * Synchronize pending memory operations.
 */
extern size_t  kaapi_offload_node_get_total_mem(struct kaapi_memory_node_t* node);


/** \ingroup Offload
 * Poll IO for the specific device.
 * Returns 0 in case of success
 */
extern int kaapi_offload_poll_device(kaapi_device_t*);


/** \ingroup Offload
 * Poll IO for the all devices.
 * Returns 0 in case of success
 */
extern int kaapi_offload_poll_devices(void);


/** \ingroup Offload
 * Set the device dev to be the current device
 */
extern void kaapi_offload_set_current_device( kaapi_device_t* device);

/** \ingroup Offload
 * Return the current device
 */
extern kaapi_device_t* kaapi_offload_get_current_device(void);


/** \ingroup Offload
 * temporary synchronize strategy.
 */
extern int kaapi_offload_synchronize(void);


#if defined(KAAPI_OFFLOAD_PROGRESS_THREAD)
extern void kaapi_offload_signal(void);
extern void kaapi_offload_wait_progression(void);
#endif

#endif // #if defined(KAAPI_USE_OFFLOAD)


#endif /* _KAAPI_OFFLOAD_H_ */
