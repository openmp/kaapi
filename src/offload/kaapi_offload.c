/*
 ** xkaapi
 **
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** Joao.Lima@inf.ufsm.br
 **
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 **
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 **
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 **
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 **
 */
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <dirent.h>
#include <string.h>
#include <limits.h>

#define _OFFLOAD_DEBUG 1
#include "kaapi_impl.h"

/* number of devices from plugins */
static unsigned int kaapi_offload_num_devices = 0;

/* kaapi devices indexed by Id */
static kaapi_device_t* kaapi_offload_devices = NULL;

/* current device Id (internal) */
static __thread kaapi_device_t* kaapi_offload_current_device = 0;

/* need a host device to execute task following constraints on the data flow graph */
static int _kaapi_host_device = -1;

#if defined(KAAPI_USE_PERFCOUNTER)
/* */
kaapi_team_perfstat_t* offload_team = 0;
#endif

/*
*/
kaapi_device_t* kaapi_offload_get_host_device(void)
{
  return (_kaapi_host_device == -1 ? 0 : &kaapi_offload_devices[_kaapi_host_device]);
}

/*
*/
int kaapi_offload_host_pushtask(kaapi_task_t* task)
{
  kaapi_assert( _kaapi_host_device != -1 );
  kaapi_place_push(
        &kaapi_offload_devices[_kaapi_host_device].rsrc,
        kaapi_offload_devices[_kaapi_host_device].rsrc.ownplace,
        task);
  return 0;
}

/*
*/
kaapi_device_t* kaapi_offload_device(int devid)
{
  if ((devid<0) && (devid >=kaapi_offload_num_devices))
     return 0;
  kaapi_device_t* device = &kaapi_offload_devices[devid];
  if (!device->is_initialized)
    kaapi_offload_device_init(device);
  return device;
}


/*
*/
void kaapi_offload_set_current_device(kaapi_device_t* device)
{
  if (device && !device->is_initialized)
    kaapi_offload_device_init(device);
  kaapi_offload_current_device = device;
}

/*
*/
kaapi_device_t* kaapi_offload_get_current_device(void)
{
  kaapi_device_t* device = kaapi_offload_current_device;
  if (device && !device->is_initialized)
    kaapi_offload_device_init(device);
  return device;
}

/*
*/
kaapi_device_t* kaapi_offload_self_device(void)
{
  kaapi_device_t* device = kaapi_offload_current_device;
  kaapi_assert_debug(device->is_initialized);
  return device;
}


/* based on GCC GOMP implementation (target.c) 
*/
static bool kaapi_offload_check_plugin_name(char* fname)
{
  const char *prefix = "libkaapi_plugin_";
#if defined(__linux__)
  const char *suffix = ".so.1";
#elif defined(__APPLE__)
  const char *suffix = ".1.dylib";
#endif
  if(!fname)
    return false;
  if(strncmp(fname, prefix, strlen(prefix)) != 0)
    return false;
  if(strncmp(fname + strlen(fname) - strlen(suffix), suffix,
             strlen(suffix)) != 0)
    return false;
  return true;
}

/* based on GCC GOMP implementation (target.c) 
*/
static bool kaapi_offload_load_plugin(
    kaapi_device_t* const device,
    const char* plugin_name
)
{
  char* err = NULL;

  dlerror();
  device->handle = dlopen(plugin_name, RTLD_LAZY);
  if(device->handle == NULL){
    err = dlerror();
    goto out;
  }
  /* */
#define DLSYM(func)                                                     \
  do                                                                    \
  {                                                                     \
    *(void**)(&device->f_##func) = dlsym (device->handle, KAAPI_PLUGIN_ENTRYPOINT_NAME(func));	\
    if (device->f_##func ==0) {                                         \
      err = dlerror ();                                                 \
      goto out;                                                         \
    }                                                                   \
  }                                                                     \
  while (0)

#define DLSYM_OPT(func)                                                 \
    *(void**)(&device->f_##func) = dlsym (device->handle, KAAPI_PLUGIN_ENTRYPOINT_NAME(func))

  /* */
  DLSYM (get_name);
  DLSYM (get_flags);
  DLSYM (get_type);
  DLSYM (get_level);
  DLSYM (get_number);
  DLSYM (init);
  DLSYM (finalize);
  DLSYM (device_init);
  DLSYM (device_finalize);
  DLSYM (device_attach);
  DLSYM (device_detach);
  DLSYM (alloc);
  DLSYM (free);
  DLSYM (get_total_mem);

  DLSYM (stream_alloc);
  DLSYM (stream_free);
  DLSYM (stream_process_pending);
  DLSYM (stream_decode_ioinstruction);
  
  DLSYM_OPT(get_cublas_handle);

  return true;

out:
  if(err != NULL){
    fprintf(stderr, "**** Error loading offload plugin '%s', "
            "file: '%s', line: %d, error='%s'\n", plugin_name,
            __FILE__, __LINE__, err);
    if(device->handle != NULL)
      dlclose(device->handle);
  }

  return err == NULL;
}
#undef DLSYM


/* Configure Kaapi based on the loaded device plugin.
   All devices from its plugin are not initialized here.
*/
static void
kaapi_offload_config_devices(kaapi_device_t* device)
{
  int n_devices;
  int i;

  if(device->f_init() != 0)
    return;

  n_devices = device->f_get_number();
  if( n_devices < 1 )
    return;

  /* ToDo: share data between library & plugin ? 
     It seems to be good that plugin extends the device in order to add internal, plugin specific
     fields. Currently only identifier allows to make correspondance between offload part and 
     plugin part (device_id is the identifier in the plugin part, specific for each type of plugin).
     Advantage: avoid to recopy data to initialize device.
  */
  kaapi_offload_devices =
      (kaapi_device_t*)realloc( kaapi_offload_devices,
                                (kaapi_offload_num_devices+n_devices)*sizeof(kaapi_device_t));
  if( kaapi_offload_devices == 0 )
    return;

  device->is_initialized = false;
  device->name = device->f_get_name();
  device->flags = device->f_get_flags();
#if defined(KAAPI_USE_PERFCOUNTER)
  device->rsrc.perfkproc = 0;
  device->rsrc.evtkproc  = 0;
#endif
  device->rsrc.ownplace  = 0;
  device->cublas_handle  = 0;
  KAAPI_ATOMIC_WRITE(&device->cnt_push, 0);
  device->rsrc.proc_type = device->f_get_type(); /* compatible with Kaapi types (kaapi.h) */
  device->hwslevel = device->f_get_level(); /* compatible with Kaapi types (kaapi.h) */
  device->stream = 0;

  for(i= 0; i < n_devices; i++)
  {
    device->device_id = i; /* internal id */
    device->gid = kaapi_offload_num_devices; /* global id */
    memset( &kaapi_offload_devices[kaapi_offload_num_devices], 0, sizeof(kaapi_device_t));
    kaapi_offload_devices[kaapi_offload_num_devices] = *device;
    kaapi_offload_num_devices++;
  }

  /* register the host device (should always be loaded) and initialize it */
  if (strcasecmp("host", device->name)==0)
    _kaapi_host_device = kaapi_offload_num_devices-1;
}

/*
*/
static bool
kaapi_offload_plugin_filter(kaapi_device_t* device, char *const plugin_filter)
{
  char* filter;
  char* _filter; /* to free */
  char* token;
  const char* sep = ",";
  const char* plugin_name = device->f_get_name();

  /* No filter = load all plugins */
  if(plugin_filter == NULL)
    return true;

  /* Plugin with no name = load it */
  if(plugin_name == NULL)
    return true;

  filter = _filter = strdup(plugin_filter);
  while( (token = strsep(&filter, sep)) != NULL ){
    if( strcmp(token, plugin_name) == 0 ){
      /* load this plugin */
      free(_filter);
      return true;
    }
  }
  free(_filter);

  /* not NULL, does not load plugins by default */
  return false;
}


/* based on GCC GOMP implementation (target.c).
   Find available plugins and load them.
*/
static void
kaapi_offload_find_plugins(void)
{
  char* plugin_path = getenv("KAAPI_PLUGIN_PATH"); // Path to compiled plugins
  char* env_plugin_filter = getenv("KAAPI_DEVICE_TYPE"); // device type: cuda, host, etc
  char plugin_filter[128];
  if (env_plugin_filter !=0)
    snprintf(plugin_filter, 128, "host,%s", env_plugin_filter);
  else
    snprintf(plugin_filter, 128, "");

  DIR* dir = NULL;
  struct dirent *ent;
  char plugin_name[PATH_MAX];

  if (plugin_path == 0)
    plugin_path = KAAPI_PREFIX "/lib";

  if(plugin_path == NULL)
    return;

  /* */
  dir = opendir(plugin_path);
  if(dir == NULL)
    return;

#if defined(_OFFLOAD_DEBUG)
  fprintf(stdout, "%s: reading directory %s\n", __FUNCTION__, plugin_path);
  fflush(stdout);
#endif

  while((ent = readdir(dir)) != NULL)
  {
    kaapi_device_t current;
    if(!kaapi_offload_check_plugin_name(ent->d_name))
      continue;
    if(strlen(plugin_path) + 1 + strlen(ent->d_name) >= PATH_MAX)
      continue;

    strcpy(plugin_name, plugin_path);
    strcat(plugin_name, "/");
    strcat(plugin_name, ent->d_name);

#if defined(_OFFLOAD_DEBUG)
    fprintf(stdout, "%s: found %s\n", __FUNCTION__, plugin_name);
    fflush(stdout);
#endif
    if(!kaapi_offload_load_plugin(&current, plugin_name))
      continue;

    /* Can I load this device ? */
    if(kaapi_offload_plugin_filter(&current, plugin_filter) == true)
    {
      kaapi_offload_config_devices(&current);
#if defined(_OFFLOAD_DEBUG)
      fprintf(stdout, "%s: plugin loaded %s\n", __FUNCTION__, plugin_name);
      fflush(stdout);
#endif
    }
  }

  closedir(dir);
}


/*
*/
unsigned int kaapi_offload_get_num_devices(void)
{
  return kaapi_offload_num_devices;
}


/*
*/
int kaapi_offload_poll_devices(void)
{
  int err, i;
  for (i=0; i<kaapi_offload_num_devices; ++i)
  {
    kaapi_device_t* device = kaapi_offload_device(i);
    err = kaapi_offload_poll_device( device );
    if (err) return err;
  }
  return 0;
}


/* Thread to poll stream and generate communication with device
 * Any update to the stream/queue/.. should be signaled to the thread.
 * The thread plays role to ensure completion of asynchronous operations.
 * Worker thread executes tasks (cpu) and initiate asynchronous commmunication with the device:
 * memory copies, kernel launches. The internal offload thread only test/wait for 
 * completion of asynchronous operation.
 */
#if defined(KAAPI_OFFLOAD_PROGRESS_THREAD)
static pthread_t    _ptid;
static volatile int _term = 0;
static volatile int _kaapi_offload_condition = 0;

/* use futex */
static void kaapi_offload_wait_cond(
    int* version
)
{
  int ver = *version;
  while (_kaapi_offload_condition < ver)
  {
    kaapi_slowdown_cpu();
  }
  ++*version;
}

/* use futex */
void kaapi_offload_signal(void)
{
  __sync_add_and_fetch( &_kaapi_offload_condition, 1 );
}

/*
*/
void kaapi_offload_wait_progression(void)
{
  int ver = _kaapi_offload_condition;
  kaapi_offload_signal();
  while (_kaapi_offload_condition == ver)
    kaapi_slowdown_cpu();
}


static void* _kaapi_offload_thread_poll( void* arg )
{
  extern int _kaapi_advance_stream( kaapi_offload_place_t* place );
  int version = 0;
  while (1)
  {
    /* wait that _kaapi_offload_condition has version number version */
    if (_term) break;
    kaapi_offload_wait_cond( &version );
    kaapi_offload_poll_devices();
  }
  return 0;
}
#endif


/*
*/
int kaapi_offload_init(int flag)
{
  KAAPI_OFFLOAD_TRACE_IN
  /* load device plugins and functions */
  kaapi_offload_find_plugins();

#if defined(KAAPI_OFFLOAD_PROGRESS_THREAD)
  for (int i =0; i<kaapi_offload_num_devices; ++i)
  {
    kaapi_device_t* device = kaapi_offload_device(i);
    kaapi_assert_debug(device->is_initialized);
  }
#endif

  KAAPI_OFFLOAD_TRACE_OUT
  return 0;
}

/*
*/
int kaapi_offload_start(void)
{
  /* initialize the host device */
  if (kaapi_offload_num_devices >0)
  {
    kaapi_offload_device_init(&kaapi_offload_devices[_kaapi_host_device]);
#if defined(KAAPI_OFFLOAD_PROGRESS_THREAD)
    pthread_create(&_ptid,0, _kaapi_offload_thread_poll, 0);
#endif
#if defined(KAAPI_USE_PERFCOUNTER)
    offload_team = kaapi_tracelib_team_init(
      kaapi_offload_num_devices,
      (void* (*)())kaapi_offload_init,
      "device",
      0,
      0
    );
#endif
  }
  return 0;
}

/*
*/
void kaapi_offload_finalize(void)
{
  KAAPI_OFFLOAD_TRACE_IN

  if (kaapi_offload_num_devices > 0)
  {
#if defined(KAAPI_OFFLOAD_PROGRESS_THREAD)
    void* res =0;
    _term = 1;
    kaapi_offload_signal();
    pthread_join(_ptid, &res);
#endif
    int i;
    for(i = 0; i < kaapi_offload_num_devices; i++)
    {
      kaapi_device_t* device = &kaapi_offload_devices[i];
      kaapi_offload_device_finalize(device);
    }
#if defined(KAAPI_USE_PERFCOUNTER)
    kaapi_tracelib_team_fini(offload_team);
#endif
    free(kaapi_offload_devices);
  }
  KAAPI_OFFLOAD_TRACE_OUT
}

