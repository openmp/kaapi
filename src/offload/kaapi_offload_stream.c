/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** Joao.Lima@inf.ufsm.br
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"
#include <stdlib.h>
#include <errno.h>

#ifdef _OFFLOAD_DEBUG
#  undef _OFFLOAD_DEBUG
#  undef KAAPI_OFFLOAD_TRACE_IN
#  undef KAAPI_OFFLOAD_TRACE_OUT
#  undef KAAPI_OFFLOAD_TRACE_MSG
#  define KAAPI_OFFLOAD_TRACE_IN
#  define KAAPI_OFFLOAD_TRACE_OUT
#  define KAAPI_OFFLOAD_TRACE_MSG(...)
#endif

/*
*/
static int _kaapi_offload_iostream_init(
    kaapi_io_stream_t* io,
    kaapi_io_stream_type_t type,
    unsigned int capacity
)
{
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_assert( capacity >= 2);
  ///if (type == KAAPI_IO_STREAM_KERN) capacity = 2;

  io->type    = type;
  io->pos_r   = 0;
  io->pos_rp  = 0;
  io->pos_w   = 0;
  io->pos_wp  = 0;
  io->instr   = (kaapi_io_instruction_t*) malloc( capacity * sizeof(kaapi_io_instruction_t) );
  if (io->instr ==0) return ENOMEM;
  io->pending = (kaapi_io_instruction_t*) malloc( capacity * sizeof(kaapi_io_instruction_t) );
  if (io->pending ==0) return ENOMEM;
  io->count  = capacity;
  KAAPI_OFFLOAD_TRACE_OUT
  return 0;
}

/*
*/
static int _kaapi_offload_iostream_destroy(
    kaapi_io_stream_t* io
)
{
  if (io ==0) return 0;
  int err = 0;
  KAAPI_OFFLOAD_TRACE_IN
  if (io->pos_r != io->pos_w)
    err = EBUSY;
  else
    free(io->instr);
  if (io->pos_rp != io->pos_wp)
    err = EBUSY;
  else
    free(io->pending);
  KAAPI_OFFLOAD_TRACE_OUT
  return 0;
}

/*
*/
kaapi_offload_stream_t* kaapi_offload_stream_init(
    kaapi_device_t* device,
    unsigned int capacity
)
{
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_offload_stream_t* s = (kaapi_offload_stream_t*) malloc( sizeof(kaapi_offload_stream_t));
  s->device = device;
  s->input_fifo = 0;
  s->output_fifo = 0;
#if CONFIG_USE_CONCURRENT_KERNELS
  int i;
  for (i=0; i<4; ++i) s->kernel_fifos[i] = 0;
#else
  s->kernel_fifo = 0;
#endif

  s->input_fifo = device->f_stream_alloc(device->device_id, KAAPI_IO_STREAM_INPUT, capacity);
  if (s->input_fifo ==0) goto rollback_error;
  s->input_fifo->stream = s;
  if (_kaapi_offload_iostream_init( s->input_fifo, KAAPI_IO_STREAM_INPUT, capacity ))
    goto rollback_error;

  s->output_fifo = device->f_stream_alloc(device->device_id, KAAPI_IO_STREAM_OUTPUT, capacity);
  if (s->output_fifo ==0) goto rollback_error;
  s->output_fifo->stream = s;
  if (_kaapi_offload_iostream_init( s->output_fifo, KAAPI_IO_STREAM_OUTPUT, capacity ))
    goto rollback_error;

#if CONFIG_USE_CONCURRENT_KERNELS
  for (i=0; i<4; ++i)
  {
    s->kernel_fifos[i] = device->f_stream_alloc(device->device_id, KAAPI_IO_STREAM_KERN, capacity);
    if (s->kernel_fifos[i] ==0) goto rollback_error;
    s->kernel_fifos[i]->stream = s;
    if (_kaapi_offload_iostream_init( s->kernel_fifos[i], KAAPI_IO_STREAM_KERN, capacity ))
      goto rollback_error;
  }
  s->kernel_fifo_pos   = 0;
  s->kernel_fifo_count = 4;
#else
  s->kernel_fifo = device->f_stream_alloc(device->device_id, KAAPI_IO_STREAM_KERN, capacity);
  if (s->kernel_fifo ==0) goto rollback_error;
  s->kernel_fifo->stream = s;
  if (_kaapi_offload_iostream_init( s->kernel_fifo, KAAPI_IO_STREAM_KERN, capacity ))
    goto rollback_error;
#endif
  KAAPI_OFFLOAD_TRACE_OUT
  return s;

rollback_error:
  if (s->input_fifo !=0) device->f_stream_free( s->input_fifo);
  if (s->output_fifo !=0) device->f_stream_free( s->output_fifo);
#if CONFIG_USE_CONCURRENT_KERNELS
  for (i=0; i<4; ++i) s->kernel_fifos[i] = 0;
    if (s->kernel_fifos[i] !=0) device->f_stream_free( s->kernel_fifos[i]);
#else
  if (s->kernel_fifo !=0) device->f_stream_free( s->kernel_fifo);
#endif
  KAAPI_OFFLOAD_TRACE_OUT
  return 0;
}

/*
*/
int kaapi_offload_stream_destroy(
    kaapi_offload_stream_t * stream
)
{
  if (stream ==0) return 0;
  KAAPI_OFFLOAD_TRACE_IN
  int err;
  err = _kaapi_offload_iostream_destroy( stream->input_fifo );
  if (err) goto return_witherror;
  err = _kaapi_offload_iostream_destroy( stream->output_fifo );
  if (err) goto return_witherror;
#if CONFIG_USE_CONCURRENT_KERNELS
  int i;
  for (i=0; i<4; ++i)
  {
    err = _kaapi_offload_iostream_destroy( stream->kernel_fifos[i] );
    if (err) goto return_witherror;
  }
#else
  err = _kaapi_offload_iostream_destroy( stream->kernel_fifo );
#endif

return_witherror:
  stream->device->f_stream_free( stream->input_fifo );
  stream->device->f_stream_free( stream->output_fifo );
#if CONFIG_USE_CONCURRENT_KERNELS
  for (i=0; i<4; ++i)
    stream->device->f_stream_free( stream->kernel_fifos[i] );
#else
  stream->device->f_stream_free( stream->kernel_fifo );
#endif
  KAAPI_OFFLOAD_TRACE_OUT
  return err;
}


/*
*/
kaapi_io_stream_t* kaapi_offload_select_io_stream(
    kaapi_offload_stream_t* const stream,
    kaapi_io_stream_type_t stype
)
{
  kaapi_io_stream_t* retval = 0;
  //TOOMANY KAAPI_OFFLOAD_TRACE_IN
  switch (stype)
  {
    case KAAPI_IO_STREAM_OUTPUT:
      retval = stream->output_fifo;
      break;
    case KAAPI_IO_STREAM_INPUT:
      retval = stream->input_fifo;
      break;
    case KAAPI_IO_STREAM_KERN:
#if CONFIG_USE_CONCURRENT_KERNELS
      retval = stream->kernel_fifos[stream->kernel_fifo_pos];
#else
      retval = stream->kernel_fifo;
#endif
      break;
      case KAAPI_IO_STREAM_ALL:
      break;
  }
  //TOOMANY KAAPI_OFFLOAD_TRACE_OUT
  return retval;
}


/*
*/
int kaapi_offload_stream_size(
    kaapi_offload_stream_t* const stream,
    kaapi_io_stream_type_t stype
)
{
  kaapi_io_stream_t* ios;
  int s;
  kaapi_assert((stype == KAAPI_IO_STREAM_INPUT)
            || (stype != KAAPI_IO_STREAM_OUTPUT)
            || (stype != KAAPI_IO_STREAM_KERN));

  ios = kaapi_offload_select_io_stream( stream, stype );
  s = kaapi_io_stream_sizeinstr(ios);
  if (s >0) return s;
  return kaapi_io_stream_sizepending(ios);
}


/**
*/
int kaapi_offload_stream_isempty(
    kaapi_offload_stream_t* const stream,
    kaapi_io_stream_type_t stype
)
{
  kaapi_io_stream_t* ios;
  if (stype != KAAPI_IO_STREAM_ALL)
  {
    ios = kaapi_offload_select_io_stream( stream, stype );
    return kaapi_io_stream_emptyinstr(ios) && kaapi_io_stream_emptypending(ios);
  }

  ios = kaapi_offload_select_io_stream(stream, KAAPI_IO_STREAM_INPUT);
  if (!(kaapi_io_stream_emptyinstr( ios )  && kaapi_io_stream_emptypending(ios)) )
    return 0;
  ios = kaapi_offload_select_io_stream(stream, KAAPI_IO_STREAM_OUTPUT);
  if (!(kaapi_io_stream_emptyinstr( ios )  && kaapi_io_stream_emptypending(ios)) )
    return 0;
#if CONFIG_USE_CONCURRENT_KERNELS
  int i;
  for (i=0; (i<4) && (err==0); ++i)
  {
    ios = &stream->kernel_fifos[ (i+stream->kernel_fifo_pos+1) % kernel_fifo_count];
    if (!(kaapi_io_stream_emptyinstr( ios )  && kaapi_io_stream_emptypending(ios)))
      return 0;
  }
#else
  ios = stream->kernel_fifo;
  if (!(kaapi_io_stream_emptyinstr( ios )  && kaapi_io_stream_emptypending(ios)))
    return 0;
#endif
  return 1;
}


int kaapi_offload_stream_isfull(
    kaapi_offload_stream_t* const stream,
    kaapi_io_stream_type_t stype
)
{
  kaapi_io_stream_t* ios;
  if (stype != KAAPI_IO_STREAM_ALL)
  {
    ios = kaapi_offload_select_io_stream( stream, stype );
    return kaapi_io_stream_fullinstr(ios) || kaapi_io_stream_fullpending(ios);
  }

  ios = kaapi_offload_select_io_stream(stream, KAAPI_IO_STREAM_INPUT);
  if (!kaapi_io_stream_fullinstr( ios ) )
    return 0;
  ios = kaapi_offload_select_io_stream(stream, KAAPI_IO_STREAM_OUTPUT);
  if (!kaapi_io_stream_fullinstr( ios ) )
    return 0;
#if CONFIG_USE_CONCURRENT_KERNELS
  int i;
  for (i=0; (i<4) && (err==0); ++i)
  {
    ios = &stream->kernel_fifos[ (i+stream->kernel_fifo_pos+1) % kernel_fifo_count];
    if (!kaapi_io_stream_fullinstr( ios ))
      return 0;
  }
#else
  ios = stream->kernel_fifo;
  if (!kaapi_io_stream_fullinstr( ios ))
    return 0;
#endif
  return 1;
}

/*
*/
kaapi_io_instruction_t* kaapi_offload_stream_push(
    kaapi_offload_stream_t* const stream,
    kaapi_io_stream_type_t stype
)
{
  if (stream ==0) return 0;
  if ((stype != KAAPI_IO_STREAM_INPUT) && (stype != KAAPI_IO_STREAM_OUTPUT) && (stype != KAAPI_IO_STREAM_KERN))
    return 0;
  kaapi_io_stream_t* ios = kaapi_offload_select_io_stream(stream, stype );
  if (ios ==0) return 0;

#if defined(KAAPI_OFFLOAD_PROGRESS_THREAD)
  if (kaapi_io_stream_fullinstr(ios))
  {
    //kaapi_device_t* device = stream->device;
    while (kaapi_io_stream_fullinstr(ios) || kaapi_io_stream_fullpending(ios))
    {
      //kaapi_offload_test_stream( stream, stype);
      kaapi_offload_stream_process_instruction( stream, stype );
      //kaapi_slowdown_cpu();
      //kaapi_offload_signal();
    }
  }
#else
  while (kaapi_io_stream_fullinstr(ios))
    kaapi_offload_test_stream( stream, stype );
#endif

  kaapi_io_instruction_t* inst = &ios->instr[ios->pos_w];
  return inst;
}


/*
*/
kaapi_io_instruction_t* kaapi_offload_stream_commit(
    kaapi_offload_stream_t* const stream,
    kaapi_io_stream_type_t stype,
    kaapi_io_instruction_t* inst
)
{
  if ((stream ==0) || (inst ==0)) return 0;
  if ((stype != KAAPI_IO_STREAM_INPUT) && (stype != KAAPI_IO_STREAM_OUTPUT) && (stype != KAAPI_IO_STREAM_KERN))
    return 0;
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_io_stream_t* ios = kaapi_offload_select_io_stream(stream, stype );
  switch (inst->type)
  {
    case KAAPI_IO_BEGIN:
    case KAAPI_IO_END:
      break;
    case KAAPI_IO_COPY_H2H:
    case KAAPI_IO_COPY_H2D:
    case KAAPI_IO_COPY_D2H:
    case KAAPI_IO_COPY_D2D: {
#if defined(KAAPI_USE_PERFCOUNTER)
      /* move here the recording of the event to ensure that offloaded task may register perf counter */
      KAAPI_PERFCTR_INCR(stream->device->rsrc.perfkproc,
          ((inst->type-KAAPI_IO_COPY_H2H)+KAAPI_PERF_ID_COMM_H2H),
          kaapi_memory_view_size(inst->inst.c_io.view_src)
      );
#endif
    } break;
    case KAAPI_IO_BARRIER:
      break;
    case KAAPI_IO_KERN:
#if CONFIG_USE_CONCURRENT_KERNELS
      if (stype == KAAPI_IO_STREAM_KERN)
        stream->kernel_fifo_pos = (stream->kernel_fifo_pos+1) % stream->kernel_fifo_count;
#endif
      break;
    default:
      KAAPI_OFFLOAD_TRACE_OUT
      return 0;
  }
  if (inst != &ios->instr[ios->pos_w])
  {
    KAAPI_OFFLOAD_TRACE_OUT
    return 0;
  }
  kaapi_writemem_barrier();
  ios->pos_w = (1+ios->pos_w) % ios->count;
#if defined(KAAPI_OFFLOAD_PROGRESS_THREAD)
  kaapi_offload_signal();
#endif
  KAAPI_OFFLOAD_TRACE_OUT

  return inst;
}

/*
 */
static int _kaapi_offload_onestream_process_instruction(
  kaapi_device_t* const device,
  kaapi_io_stream_t* ios
)
{
  if (ios->pos_r == ios->pos_w) return 0; /* empty */
  KAAPI_OFFLOAD_TRACE_IN
  int err = 0;
  kaapi_device_t* saved_device = kaapi_offload_device_push( device );
  while (!kaapi_io_stream_emptyinstr(ios) && !kaapi_io_stream_fullpending(ios) && (err ==0))
  {
    int p  = ios->pos_r;
    int wp = ios->pos_wp;
    err = device->f_stream_decode_ioinstruction(ios, &ios->instr[p]);
    kaapi_assert_debug(err ==0);
    kaapi_writemem_barrier();
    /* recopy op in pending op */
    wp = ios->pos_wp;
    ios->pending[wp] = ios->instr[p];
    ios->pos_wp = (1+wp) % ios->count;
    ios->pos_r  = (1+p)  % ios->count;
  }
  kaapi_offload_device_pop( saved_device );
  KAAPI_OFFLOAD_TRACE_OUT
  return err;
}

/*
*/
static int _kaapi_offload_stream_process_instruction(
  kaapi_offload_stream_t* const stream,
  kaapi_io_stream_type_t stype
)
{
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_device_t* device;
  kaapi_io_stream_t* ios;
  int err = 0;

  device = stream->device;

  switch (stype)
  {
    case KAAPI_IO_STREAM_INPUT:
    case KAAPI_IO_STREAM_OUTPUT:
      ios = kaapi_offload_select_io_stream(stream, stype );
      if (!kaapi_io_stream_emptyinstr(ios))
      {
        err = _kaapi_offload_onestream_process_instruction( device, ios);
        if (err) goto return_witherror;
      }
    break;

    case KAAPI_IO_STREAM_KERN:
    {
#if CONFIG_USE_CONCURRENT_KERNELS
      int i;
      for (i=0; (i<4) && (err==0); ++i)
      {
        ios = &stream->kernel_fifos[ (i+stream->kernel_fifo_pos+1) % kernel_fifo_count];
        if (!kaapi_io_stream_emptyinstr(ios))
        {
          err = _kaapi_offload_onestream_process_instruction( device, ios);
          if (err) goto return_witherror;
        }
      }
#else
      ios = stream->kernel_fifo;
      if (!kaapi_io_stream_emptyinstr(ios))
        err = _kaapi_offload_onestream_process_instruction( device, ios);
#endif
    } break;
    default: return EINVAL;
  };

return_witherror:
  KAAPI_OFFLOAD_TRACE_OUT
  return err;
}

/*
*/
int kaapi_offload_stream_process_instruction(
  kaapi_offload_stream_t* const stream,
  kaapi_io_stream_type_t stype
)
{
  if (stream ==0) return EINVAL;
  int err;

  if (stype == KAAPI_IO_STREAM_ALL)
  {
    err = _kaapi_offload_stream_process_instruction( stream, KAAPI_IO_STREAM_INPUT );
    if (err) goto return_witherror;
    err = _kaapi_offload_stream_process_instruction( stream, KAAPI_IO_STREAM_KERN );
    if (err) goto return_witherror;
    err = _kaapi_offload_stream_process_instruction( stream, KAAPI_IO_STREAM_OUTPUT );
    if (err) goto return_witherror;
  }
  else
    err = _kaapi_offload_stream_process_instruction( stream, stype );

return_witherror:
  return err;
}




/** blocking=1 -> wait; blocking=0 -> test
*/
static int _kaapi_offload_waittest_onestream(
    kaapi_offload_stream_t* const stream,
    kaapi_io_stream_type_t stype,
    int blocking
)
{
  KAAPI_OFFLOAD_TRACE_IN
  kaapi_device_t* device;
  kaapi_io_stream_t* ios;
  int err = 0;

  device = stream->device;
  kaapi_device_t* saved_device = kaapi_offload_device_push( device );

  switch (stype)
  {
    case KAAPI_IO_STREAM_INPUT:
    case KAAPI_IO_STREAM_OUTPUT:
      ios = kaapi_offload_select_io_stream(stream, stype );
      if (!kaapi_io_stream_emptypending(ios))
      {
        /* release lock : because call back may require to push device */
        err = device->f_stream_process_pending( ios, blocking);
        if ((err != EINPROGRESS) && (err !=0)) goto return_witherror;
      }
    break;

    case KAAPI_IO_STREAM_KERN:
    {
#if CONFIG_USE_CONCURRENT_KERNELS
      int i;
      for (i=0; (i<4) && (err==0); ++i)
      {
        ios = &stream->kernel_fifos[ (i+stream->kernel_fifo_pos+1) % kernel_fifo_count];
        if (!kaapi_io_stream_emptypending(ios))
        {
          /* release lock : because call back may require to push device */
          err = device->f_stream_process_pending( ios, blocking);
          if ((err != EINPROGRESS) && (err !=0)) goto return_witherror;
        }
      }
#else
      ios = stream->kernel_fifo;
      if (!kaapi_io_stream_emptypending(ios))
      {
        /* release lock : because call back may require to push device */
        err = device->f_stream_process_pending( ios, blocking);
        if ((err != EINPROGRESS) && (err !=0)) goto return_witherror;
      }
#endif
    } break;
    default: return EINVAL;
  };

return_witherror:
  kaapi_offload_device_pop( saved_device );
  KAAPI_OFFLOAD_TRACE_OUT
  if ((err == EINPROGRESS) || (err ==0)) return 0;
  return err;
}


/*
*/
static int _kaapi_offload_waittest_stream(
    kaapi_offload_stream_t* const stream,
    kaapi_io_stream_type_t stype,
    int blocking
)
{
  if (stream ==0) return EINVAL;
  int err;

  if (stype == KAAPI_IO_STREAM_ALL)
  {
    err = _kaapi_offload_waittest_onestream( stream, KAAPI_IO_STREAM_INPUT, blocking );
    if (err) goto return_witherror;
    err = _kaapi_offload_waittest_onestream( stream, KAAPI_IO_STREAM_KERN, blocking );
    if (err) goto return_witherror;
    err = _kaapi_offload_waittest_onestream( stream, KAAPI_IO_STREAM_OUTPUT, blocking );
    if (err) goto return_witherror;
  }
  else
    err = _kaapi_offload_waittest_onestream( stream, stype, blocking );

return_witherror:
  return err;
}


/*
*/
int kaapi_offload_wait_stream(
    kaapi_offload_stream_t* const stream,
    kaapi_io_stream_type_t stype
)
{
  if (stream ==0) return EINVAL;
  KAAPI_OFFLOAD_TRACE_IN
  int err = _kaapi_offload_waittest_stream( stream, stype, 1);
  KAAPI_OFFLOAD_TRACE_OUT
  return err;
}


/*
*/
int kaapi_offload_test_stream(
    kaapi_offload_stream_t* const stream,
    kaapi_io_stream_type_t stype
)
{
  if (stream ==0) return EINVAL;
  KAAPI_OFFLOAD_TRACE_IN
  int err = _kaapi_offload_waittest_stream( stream, stype, 0);
  KAAPI_OFFLOAD_TRACE_OUT
  return err;
}
