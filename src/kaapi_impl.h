/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** vincent.danjean@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_IMPL_H
#define _KAAPI_IMPL_H 1

//#define OLD_QUEUE 1

/* to use new aggregation protocol */
#define KAAPI_USE_FLAT_CC_SYNC 1

#if defined(KAAPI_DEBUG)
#define KAAPI_RETURN_ERROR(err,val) \
  {\
    if (err != val) { printf("%s %i: error\n", __FILE__, __LINE__); }\
    return err; \
  }
#else
#define KAAPI_RETURN_ERROR(err,val) \
  {\
    return err; \
  }
#endif


/* Mark that we compile source of the library.
   Only used to avoid to include public definitition of some types.
*/
#define KAAPI_COMPILE_SOURCE 1

#define KAAPI_CACHE_LINE 64

/* in Byte */
#define KAAPI_KPROCESSOR_SIZE 16384

/* to be adapted on line */
#define KAAPI_ONELINE_DFG_THRESHOLD 1024

/* fwddecla */
struct kaapi_hashmap_t;

#include <errno.h>  // error code
#include <stdlib.h> // free, malloc

#include "config.h"
#include "kaapi_defs.h"
#include "kaapi_atomic.h"
#include "kaapi_time.h"
#include "kaapi.h"
#include "kaapi_task.h"
#include "kaapi_workqueue.h"

#include "kaapi_wsprotocol.h"

#if defined(KAAPI_DEBUG)
#include <signal.h>
#include "kaapi_dbg.h"
#define KAAPI_ATOMIC_PRINT( inst ) \
{\
  kaapi_dbg_print_lock();\
  inst;\
  fflush(stdout); \
  kaapi_dbg_print_unlock();\
}
#else
#define KAAPI_ATOMIC_PRINT( inst ) 
#endif


/** Current implementation relies on isoaddress allocation to avoid
    communication during synchronization at the end of partitionning.
*/
//#define KAAPI_ADDRSPACE_ISOADDRESS 1


/** This is the new version on top of X-Kaapi
*/
extern const char* get_kaapi_version(void);
extern const char* get_kaapi_git_hash(void);
extern const char* get_kaapi_configure_log(void);


/* ================== Library initialization/terminaison ======================= */
/* Low level initialization based on value of kaapi_default_param to initialize
   submodules.
   kaapi_init is only a driver to get information from environment variables to
   setup kaapi_default_param fields.
*/
extern int kaapi_mt_init(int flag);
extern int kaapi_mt_hwdetect(void);
extern int kaapi_mt_registerproc(void);
extern int kaapi_mt_finalize(void);


/* Fwd declaration 
*/
struct kaapi_listrequest_t;

/* Fwd declaration
*/
struct kaapi_taskdescr_t;
struct kaapi_metadata_info_t;


/* \ingroup SCHED
 * Return a priority from ct path value
 */
typedef uint8_t (*kaapi_ct2prio_fnc_t)(uint64_t, struct kaapi_task_t*);


/* ========================================================================= */
/* Runtime environment, set through environement variable                    */
/* ========================================================================= */

#if defined(KAAPI_USE_PERFCOUNTER)
#include "kaapi_trace.h"
#include "kaapi_trace_impl.h"
#endif
#include "kaapi_machine.h"
#include "kaapi_util.h"
#include "kaapi_sched.h"
#include "kaapi_hashmap.h"
#include "kaapi_format.h"


/** Definition of parameters for the runtime system
*/
typedef struct kaapi_rtparam_t {
  size_t                        stackblocsize;       /* default stack bloc size */
  size_t                        threadstacksize;     /* default pthread stack size */
  unsigned int                  syscpucount;         /* number of physical cpus of the system */
  unsigned int                  cpucount;            /* number of physical cpu used for execution */
  const char*                   cpuset_str;          /* string from getenv */
  const char*                   cpucount_str;        /* string from getenv */
  unsigned int                  gpucount;            /* number of physical gpu used for execution */
  kaapi_selectvictim_fnc_t      wsselect;            /* default method to select a victim */
  kaapi_push_fnc_t              wspush;              /* call to select queue where to push task */
  kaapi_push_init_fnc_t         wspush_init;         /* call to select queue where to push ready tasks */
  kaapi_push_init_fnc_t         wspush_init_distrib; /* call to select queue where to push distrib(init) task */
  kaapi_emitsteal_fnc_t	        emitsteal;
  kaapi_emitsteal_init_t        emitsteal_initctxt;  /* call to initialize the emitsteal ctxt */
  kaapi_emitsteal_dstor_t       emitsteal_dstorctxt; /* call to dstor ctxt */
  kaapi_sched_postrequest_fnc_t    request_post_fnc;  /* to post request depending / protocol */
  kaapi_sched_commit_request_fnc_t request_commit_fnc;
  kaapi_sched_commit_request_fnc_async_t request_commit_fnc_async;
  kaapi_request_reply_fnc_t        request_reply_fnc;
  kaapi_listrequest_iterator_empty_fnc_t lr_empty_fnc;
  kaapi_listrequest_iterator_get_fnc_t   lr_get_fnc;
  kaapi_listrequest_iterator_next_fnc_t  lr_next_fnc;
  kaapi_listrequest_iterator_count_fnc_t lr_count_fnc;
  int (*pgo_init)( struct kaapi_place_group_operation_t* kpgo );
  int (*pgo_wait)( struct kaapi_place_group_operation_t* kpgo );
  int (*pgo_fini)( struct kaapi_place_group_operation_t* kpgo );

#if defined(KAAPI_USE_PERFMODEL)
  unsigned int                  perfmodel;           /* enable performance model for prediction */
  unsigned int                  perfmodel_calibrate; /* enable time counters for the performance model */
  unsigned int                  noremotesteal;       /* no steal from remote-pushed tasks */
#endif

  unsigned int		              use_affinity;        /* use cpu affinity if CPUSET defined */
  kaapi_affinityset_t           affinity_set[KAAPI_HWS_LEVELID_MAX];
  kaapi_placeset_t              places_set[KAAPI_HWS_LEVELID_MAX];
  kaapi_map2affinityset_t       *map2affset;         /* syscpu size array of reverse mapping */
  kaapi_procbind_t              procbind;            /* */
  kaapi_places_part_t           places;              /* initial places */

  kaapi_ct2prio_fnc_t		        ctpriority;  /* use critical path priorities, if 0 no */
  int                           use_priority;  /* use task priority, if 0 no */
  int                           strict_push;
  long                          block_cyclic;        /* blocking factor for some cyclic distribution */
  int                           enable_push_local;   /* Enable pushing to local kproc instead of numa node*/
  int                           alarmperiod;         /* period for alarm */

  int                           display_env;         /* value defined in komp.c */
  int                           dump_graph;
} kaapi_rtparam_t;

extern kaapi_rtparam_t kaapi_default_param;




/* ===================== Helper task functions  ==================================== */
static inline const struct kaapi_format_t* _kaapi_task_getformat( const kaapi_task_t* task)
{
  const struct kaapi_format_t* fmt = 0;
  if (task->fmt)
    fmt = task->fmt;
  else
    fmt = kaapi_format_resolve_bybody( task->body );
  return fmt;
}


static inline size_t _kaapi_task_getsize( const kaapi_task_t* task, const struct kaapi_format_t* fmt)
{
  size_t size = 0;
  unsigned int count = kaapi_format_get_count_params(fmt, task->arg);
  unsigned int i;
  for (i=0; i<count; ++i)
  {
    if (KAAPI_ACCESS_GET_MODE(kaapi_format_get_mode_param(fmt, i, task->arg)) == KAAPI_ACCESS_MODE_V)
      continue;
    kaapi_memory_view_t view;
    kaapi_format_get_view_param(fmt, i, task->arg, &view);
    size += kaapi_memory_view_size(&view);
  }
  return size;
}


/* ========== MACHINE DEPEND DATA STRUCTURE =========== */


/** The Kaapi global team: master team creates by the main thread
*/
extern kaapi_team_t* kaapi_global_team;


/** Post steal request 
    proc type is the processor type: KAAPI_PROC_TYPE_CPU, _GPU, ...
*/
extern kaapi_request_t* kaapi_sched_ccsync_post_request (
  kaapi_ressource_t* kpi,
  kaapi_place_t* ld
);
extern kaapi_request_t* kaapi_sched_qlock_post_request (
  kaapi_ressource_t* kpi,
  kaapi_place_t* ld
);



/*
*/
static inline void kaapi_place_group_operation_init( struct kaapi_place_group_operation_t* kpgo )
{
  if (kaapi_default_param.pgo_init)
    kaapi_default_param.pgo_init( kpgo );
}

/*
*/
static inline void kaapi_place_group_operation_wait( struct kaapi_place_group_operation_t* kpgo )
{
  if (kaapi_default_param.pgo_wait)
    kaapi_default_param.pgo_wait( kpgo );
}

/*
*/
static inline void kaapi_place_group_operation_fini( struct kaapi_place_group_operation_t* kpgo )
{
  if (kaapi_default_param.pgo_fini)
    kaapi_default_param.pgo_fini( kpgo );
}


/* ======================== Perf counter interface: machine dependent ========================*/
//#include "kaapi_trace.h"
//#include "kaapi_trace_recorder.h"


#if defined(KAAPI_DEBUG)
/* output bits string */
extern char* kaapi_cpuset2string( int nproc, const kaapi_cpuset_t* cupset );
/* output bits string, reentrant version */
extern char* kaapi_cpuset2string_r( char* buffer, int size, int nproc, const kaapi_cpuset_t* cupset );

/* ======================== MACHINE DEPENDENT FUNCTION THAT SHOULD BE DEFINED ========================*/
/* ........................................ PUBLIC INTERFACE ........................................*/
extern void* kaapi_alloc_protect( size_t size );
extern void kaapi_free_protect( void* p );
extern void* kaapi_realloc_protect(void *ptr, size_t size);


#if defined(KAAPI_DEBUG)
/* Signal handler to dump the state of the internal kprocessors
   This signal handler is attached to SIGALARM when KAAPI_DUMP_PERIOD env. var. is defined.
*/
extern void _kaapi_signal_dump_state(int);
#endif

/* Signal handler to print the backtrace
   This signal handler is attached to:
    - SIGABRT
    - SIGTERM
    - SIGSEGV
    - SIGFPE 
    - SIGILL
    If the library is configured with --with-perfcounter, then the function call _kaapi_signal_dump_counters.
*/
extern void _kaapi_signal_dump_backtrace(int, siginfo_t *si, void *unused);
#endif


#if defined(KAAPI_USE_PERFCOUNTER)
/* Signal handler attached to:
    - SIGINT
    - SIGQUIT
    - SIGABRT
    - SIGTERM
    - SIGSTOP
  when the library is configured with --with-perfcounter in order to flush some counters.
*/
extern void _kaapi_signal_dump_counters(int);

/* kpi is a pointer to kaapi processor info data structure
*/
#define KAAPI_PERFCTR_IF(perf, id, D, I) \
    D; \
    if ((perf !=0) && kaapi_perf_idset_test(&kaapi_tracelib_param.perfctr_idset, id)) \
    { I }
#else
#define KAAPI_PERFCTR_IF(perf, id, D, I) {}
#endif


#if defined(__cplusplus)
}
#endif

#endif
