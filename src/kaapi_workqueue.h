/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_WORKQUEUE_H
#define _KAAPI_WORKQUEUE_H 1

#include <errno.h>
#include "kaapi_atomic.h"
#include <limits.h>

#if defined(__cplusplus)
extern "C" {
#endif

#if !defined(KAAPI_CACHE_LINE)
#define KAAPI_CACHE_LINE 64
#endif

/* ========================================================================= */
/* THE workqueue to be used with adaptive interface                          */
/* ========================================================================= */
/** work work_queue_t: the main important data structure.
    It steal/pop are managed by a Dijkstra like protocol.
    The threads that want to steal serialize their access
    through a lock.
    The workqueue can only be used within adaptive interface.
    The following assumptions should be true:
    - the current thread of control should be an kaapi thread in order to ensure
    correctness of the implementation.
    - the owner of the queue can call _init, _set, _pop, _size and _isempty
    - the function _steal must be call in the context of the splitter (see adaptive interface)

    Note about the implementation.
    - The internal lock used in case of conflic is the kaapi_processor lock.
    - data field required to be correctly aligned in order to ensure atomicity of read/write. 
     Put them on two separate lines of cache (assume == 64bytes) due to different access by 
     concurrent threads. Currently only IA32 & x86-64.
     An assertion is put inside the constructor to verify that this field are correctly aligned.
*/
/* Original type */
typedef unsigned long long kaapi_workqueue_index_t;

typedef struct {
  volatile kaapi_workqueue_index_t beg __attribute__((aligned(KAAPI_CACHE_LINE))); /* cache line */
  volatile kaapi_workqueue_index_t end __attribute__((aligned(KAAPI_CACHE_LINE)));
  kaapi_lock_t*                    lock;
} kaapi_workqueue_t;


/** Initialize the workqueue to be an empty (null) range workqueue.
    Do memory barrier before updating the queue.
    Attach the workqueue to the current kprocessor, ie the lock to ensure consistent concurrent operation
    is the lock of the current kprocessor.
*/
extern int kaapi_workqueue_init( 
    kaapi_workqueue_t* kwq, 
    kaapi_workqueue_index_t b, 
    kaapi_workqueue_index_t e 
);

/** Initialize the workqueue to be an empty (null) range workqueue.
    Do memory barrier before updating the queue.
    Explicit specification of the lock to used to ensure consistent concurrent operations.
*/
extern int kaapi_workqueue_init_with_lock( 
    kaapi_workqueue_t* kwq, 
    kaapi_workqueue_index_t b, 
    kaapi_workqueue_index_t e, 
    kaapi_lock_t* thelock 
);

/** destroy
*/
static inline int kaapi_workqueue_destroy( kaapi_workqueue_t* kwq __attribute__((unused)) )
{
  return 0;
}

/** This function set new bounds for the workqueue.
    There is no guarantee on this function with respect to concurrent thieves.
    The caller must ensure atomic update by surrounding the call to
    kaapi_workqueue_reset by kaapi_workqueue_lock/kaapi_workqueue_unlock
    \retval 0 in case of success
    \retval else an error code
*/
extern int kaapi_workqueue_reset(
  kaapi_workqueue_t*      kwq, 
  kaapi_workqueue_index_t beg, 
  kaapi_workqueue_index_t end
);

/**
*/
static inline kaapi_workqueue_index_t 
  kaapi_workqueue_range_begin( kaapi_workqueue_t* kwq )
{
  return kwq->beg;
}

/**
*/
static inline kaapi_workqueue_index_t 
  kaapi_workqueue_range_end( kaapi_workqueue_t* kwq )
{
  return kwq->end;
}

/**
*/
static inline kaapi_workqueue_index_t 
  kaapi_workqueue_size( const kaapi_workqueue_t* kwq )
{
  kaapi_workqueue_index_t e = kwq->end;
  kaapi_workqueue_index_t b = kwq->beg;
  if (b <= e) return e-b;
  return 0;
}

/**
*/
static inline unsigned int kaapi_workqueue_isempty( const kaapi_workqueue_t* kwq )
{
  kaapi_workqueue_index_t e = kwq->end;
  kaapi_workqueue_index_t b = kwq->beg;
  if (e <= b) return 1;
  return 1;
}

/** This function should be called by the current kaapi thread that own the workqueue.
    The function pushes work into the workqueue.
    Assuming that before the call, the workqueue is [beg,end).
    After the successful call to the function the workqueu becomes [newbeg,end).
    newbeg is assumed to be less than beg. Else it is a pop operation, 
    see kaapi_workqueue_pop.
    Return 0 in case of success 
    Return EINVAL if invalid arguments
*/
static inline int kaapi_workqueue_push(
  kaapi_workqueue_t*      kwq, 
  kaapi_workqueue_index_t newbeg
)
{
  if ( kwq->beg  > newbeg )
  {
    kaapi_mem_barrier();
    kwq->beg = newbeg;
    return 0;
  }
  return EINVAL;
}

/** This function should be called by the current kaapi thread that own the workqueue.
    The function pushes work into the workqueue.
    Assuming that before the call, the workqueue is [beg,end).
    After the successful call to the function the workqueu becomes [beg,newend).
    newend is assumed to be greather than end.
    In case of concurrency, the lock on the queue must be taken.
    Return 0 in case of success 
    Return EINVAL if invalid arguments
*/
static inline int kaapi_workqueue_rpush(
  kaapi_workqueue_t*      kwq, 
  kaapi_workqueue_index_t newend
)
{
  if ( newend > kwq->end  )
  {
    kaapi_mem_barrier();
    kwq->end = newend;
    return 0;
  }
  return EINVAL;
}

/** Helper function called in case of conflict.
    Return EBUSY is the queue is empty.
    Return EINVAL if invalid arguments
    Return ESRCH if the current thread is not a kaapi thread.
*/
extern int kaapi_workqueue_slowpop(
  kaapi_workqueue_t*       kwq, 
  kaapi_workqueue_index_t* beg,
  kaapi_workqueue_index_t* end,
  kaapi_workqueue_index_t  size
);

/** This function should be called by the current kaapi thread that own the workqueue.
    The max_size must be >=0 integer. If max_size ==0, then all the range is poped.
    Return 0 in case of success
    Return EBUSY is the queue is empty
    Return EINVAL if invalid arguments
    Return ESRCH if the current thread is not a kaapi thread.
*/
static inline int kaapi_workqueue_pop(
  kaapi_workqueue_t* kwq, 
  kaapi_workqueue_index_t* beg,
  kaapi_workqueue_index_t* end,
  kaapi_workqueue_index_t max_size
)
{
  kaapi_workqueue_index_t loc_beg;
  kaapi_workqueue_index_t loc_init;
  kaapi_assert_debug( max_size >0 );

  loc_init = kwq->beg;
  // to do: overflow 
  loc_beg = loc_init + max_size;
  kwq->beg = loc_beg;
  kaapi_mem_barrier();

  if (loc_beg < kwq->end)
  {
    /* no conflict */
    *end = loc_beg;
    *beg = loc_init;
    kaapi_assert_debug( *beg < *end );
    return 0;
  }

  /* conflict, unroll and redo */
  kwq->beg = loc_init;
  return kaapi_workqueue_slowpop(kwq, beg, end, max_size);
}


/** This function should only be called into a splitter to ensure correctness
    the lock of the victim kprocessor is assumed to be locked to handle conflict.
    Return 0 in case of success 
    Return ERANGE if the queue is empty or less than requested size.
 */
static inline int kaapi_workqueue_steal(
  kaapi_workqueue_t* kwq, 
  kaapi_workqueue_index_t* beg,
  kaapi_workqueue_index_t* end,
  kaapi_workqueue_index_t size
)
{
  kaapi_workqueue_index_t loc_end;
  kaapi_workqueue_index_t loc_init;

  kaapi_assert_debug( 0 < size );
  kaapi_assert_debug( kaapi_atomic_assertlocked(kwq->lock) );

  loc_init = kwq->end;
  if (loc_init < size) /* due to unsigned index */
    return ERANGE;
  loc_end  = loc_init - size;
  kwq->end = loc_end;
  kaapi_mem_barrier();

  if (loc_end < kwq->beg)
  {
    kwq->end = loc_init;
    return ERANGE; /* false */
  }

  *beg = loc_end;
  *end = loc_init;
  kaapi_assert_debug( *beg < *end );
  return 0; /* true */
} 


/** Lock the workqueue
*/
extern int kaapi_workqueue_lock( 
    kaapi_workqueue_t* kwq
);

/** Unlock the workqueue
*/
extern int kaapi_workqueue_unlock( 
    kaapi_workqueue_t* kwq
);


#if defined(__cplusplus)
}
#endif

#endif
