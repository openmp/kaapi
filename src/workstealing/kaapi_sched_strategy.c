/*
 n* xkaapi
 ** 
 ** Created on Tue Mar 31 15:21:00 2009
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include "kaapi_impl.h"
#include <stdio.h> // debug
#if defined(KAAPI_USE_NUMA)
#include <numa.h>
#include <numaif.h>
#endif

/*
*/
static int kaapi_sched_select_queue_attr_task(
    kaapi_ressource_t*     rsrc,
    kaapi_place_t*         local,
    kaapi_task_t*          task
);



/* Initial push using wspush strategy
*/
void kaapi_sched_push_init_queue_wspush(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t*  frame_rd
)
{
  kaapi_place_t* self = rsrc->ownplace;
  kaapi_assert( 0 == kaapi_default_param.wspush( rsrc,
                                                 self,
                                                 &frame_rd->rd)
  );
}


/** Global strategy
*/
void kaapi_sched_push_init_queue_global(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t*  frame_rd
)
{
  if (!kaapi_list_empty(&frame_rd->rd))
    kaapi_place_pushlist( rsrc,
          kaapi_default_param.places_set[KAAPI_HWS_LEVELID_MACHINE].places[0],
          &frame_rd->rd
    );
}


/* Initial push using cyclic strategy over kprocessors
*/
void kaapi_sched_push_init_queue_cyclickproc(
    kaapi_ressource_t*      rsrc,
    kaapi_frame_wrdlist_t*  frame_rd
)
{
  int err;
  kaapi_task_t* task;
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_team_t* team = kproc->team;
  int count = team->count;
  int counter = rsrc->kid;
  int victim;
  kaapi_place_t* ld;
  kaapi_place_group_operation_t kpgo;
  kaapi_place_group_operation_init( &kpgo );

  while ((task = kaapi_list_pop_tail(&frame_rd->rd)) !=0)
  {
    /* */
    if (kaapi_default_param.strict_push)
      task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;
redo:
    victim = counter % count;
    ++counter;
    kaapi_processor_t* kp = team->all_kprocessors[victim];
    ld = kp->rsrc.ownplace;
    if (ld ==0) goto redo;
    if (kp == kproc)
      err = kaapi_place_push_async( &kpgo, rsrc, ld, task );
    else
      err = kaapi_place_push_remote_async( &kpgo, rsrc, ld, task );
    kaapi_assert( (err == 0) || (err == EINPROGRESS) );
  }
  kaapi_place_group_operation_wait(&kpgo);
  kaapi_place_group_operation_fini(&kpgo);
}


/* Initial push using random strategy over kprocessors
*/
void kaapi_sched_push_init_queue_randkproc(
    kaapi_ressource_t*      rsrc,
    kaapi_frame_wrdlist_t*  frame_rd
)
{
  kaapi_task_t* task;
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_team_t* team = kproc->team;
  int count = team->count;
  kaapi_place_group_operation_t kpgo;
  kaapi_place_group_operation_init( &kpgo );

  while ((task = kaapi_list_pop_tail(&frame_rd->rd)) !=0)
  {
    if (kaapi_default_param.strict_push)
      task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;
    /* */
    int victim = rand_r( &rsrc->seed ) % count;
    kaapi_processor_t* kp = team->all_kprocessors[victim];
    kaapi_place_t* ld = kp->rsrc.ownplace;
    if (kp == kproc)
    {
      kaapi_assert( 0 == kaapi_place_push( rsrc, ld, task ));
    }
    else
    {
      kaapi_assert( 0 == kaapi_place_push_remote( rsrc, ld, task ));
    }
  }
  kaapi_place_group_operation_wait(&kpgo);
  kaapi_place_group_operation_fini(&kpgo);
}


#if defined(KAAPI_USE_NUMA)

/* Initial push using cyclic distribution among numa node
*/
void kaapi_sched_push_init_queue_cyclicnuma(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t*  frame_rd
)
{
  kaapi_task_t* task;
  uint64_t      cnt_task = 0;
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_team_t* team = kproc->team;
  int count = team->numaplaces.count;
  //kaapi_place_group_operation_t kpgo;
  //kaapi_place_group_operation_init( &kpgo );

  while ((task = kaapi_list_pop_tail(&frame_rd->rd)) !=0)
  {

    /* */
    kaapi_place_t* ld = team->numaplaces.places[cnt_task/kaapi_default_param.block_cyclic % count];
    ++cnt_task;
    if (task->u.s.flag & KAAPI_TASK_FLAG_MASK_AFFINITY)
    {
      kaapi_sched_select_queue_attr_task( rsrc, ld, task );
    }
    else
    {
      if (kaapi_default_param.strict_push)
        task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;
      if (ld == kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid])
        //kaapi_place_push_async( &kpgo, rsrc, ld, task );
        kaapi_place_push( rsrc, ld, task );
      else
        //kaapi_place_push_remote_async( &kpgo, rsrc, ld, task );
        kaapi_place_push_remote( rsrc, ld, task );
    }
  }
  //kaapi_place_group_operation_wait(&kpgo);
  //kaapi_place_group_operation_fini(&kpgo);
}


/*
*/
void kaapi_sched_push_init_queue_cyclicnumastrict(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t*  frame_rd
)
{
  kaapi_task_t* task;
  uint64_t      cnt_task = 0;
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_team_t* team = kproc->team;
  int count = team->numaplaces.count;
  kaapi_place_group_operation_t kpgo;
  kaapi_place_group_operation_init( &kpgo );

  while ((task = kaapi_list_pop_head(&frame_rd->rd)) !=0)
  {
    /* */
    kaapi_place_t* ld = team->numaplaces.places[cnt_task/kaapi_default_param.block_cyclic % count];
    ++cnt_task;
    task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;
    if (task->u.s.flag & KAAPI_TASK_FLAG_MASK_AFFINITY)
      kaapi_sched_select_queue_attr_task( rsrc, ld, task );
    else
    {
      if (ld == kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid])
        kaapi_place_push_async( &kpgo, rsrc, ld, task );
      else
        kaapi_place_push_remote_async( &kpgo, rsrc, ld, task );
    }
  }
  kaapi_place_group_operation_wait(&kpgo);
  kaapi_place_group_operation_fini(&kpgo);
}


/* Initial push using random strategy over numa node
*/
void kaapi_sched_push_init_queue_randnuma(
    kaapi_ressource_t*      rsrc,
    kaapi_frame_wrdlist_t*  frame_rd
)
{
  kaapi_task_t* task;
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_team_t* team = kproc->team;
  int count = team->numaplaces.count;
  while ((task = kaapi_list_pop_tail(&frame_rd->rd)) !=0)
  {
    /* */
    int victim = rand_r( &rsrc->seed ) % count;
    kaapi_place_t* ld = team->numaplaces.places[victim];
// FORCE
task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;
    if (task->u.s.flag & KAAPI_TASK_FLAG_MASK_AFFINITY)
      kaapi_sched_select_queue_attr_task( rsrc, ld, task );
    else
    {
      if (kaapi_default_param.strict_push)
        task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;
      if (ld == kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid])
        kaapi_place_push( rsrc, ld, task );
      else
        kaapi_place_push_remote( rsrc, ld, task );
    }
  }
}
#endif


/** Used attribut of the task to push them on correct queue
*/
int kaapi_sched_select_queue_attr_task(
    kaapi_ressource_t*     rsrc,
    kaapi_place_t*         local,
    kaapi_task_t*          task
)
{
  kaapi_place_t* ld = local;
  bool rmt = false;

#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
  int ldid = -1;

  if (kaapi_default_param.strict_push)
    task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;

  if ((task->fmt !=0)
   && (task->u.s.flag & KAAPI_TASK_FLAG_MASK_AFFINITY)
   && (task->fmt->get_affinity !=0))
    ldid = kaapi_format_get_affinity(task->fmt, task->arg, task, task->u.s.flag);

  if (ldid == -1)
   return kaapi_place_push( rsrc, local, task );

  kaapi_team_t* team = kaapi_self_team();

  /* correct outofbound value modulo the container size */
  if ((task->u.s.flag & KAAPI_TASK_FLAG_MASK_AFFINITY) == KAAPI_TASK_FLAG_AFF_CORE)
  {
    if (ldid >= team->count)
    {
#if defined(KAAPI_DEBUG)
      fprintf(stderr, "*** warning 'task core affinity is out of bound: %i'\n", ldid);
#endif
      ldid %= team->count;
    }

    kaapi_processor_t* victim_proc = team->all_kprocessors[ldid];
    ld = victim_proc->rsrc.ownplace;
    if (&victim_proc->rsrc != rsrc)
      rmt = true;
    /*ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_THREAD].places[ldid];*/
  }
  else
  { /* NUMA or ADDR flag */
    if (ldid >= kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count) {
#if defined(KAAPI_DEBUG)
      fprintf(stderr, "*** warning 'task attribut ld is out of bound: %i'\n", ldid);
#endif
      ldid %= kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count;
    }

    /* Prevent pushing to empty node !*/
    if (! KAAPI_CPUSET_ISSET(ldid, &team->numa_bitmap)) {
#if defined(KAAPI_DEBUG)
      fprintf(stderr, "*** warning 'task attribut ld is not in team set: %i, / %u'\n", ldid, (unsigned)team->numa_bitmap.bits[0]);
#endif
      ldid = rsrc->numaid;
    }
    ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[ldid];
    if (ld != kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid])
      rmt = true;
  }

#endif // #if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
  if (!rmt)
    return kaapi_place_push( rsrc, ld, task );
  else
    return kaapi_place_push_remote( rsrc, ld, task );
}


/*
*/
int kaapi_sched_select_queue_attr(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* local,
    kaapi_list_t* list
)
{
  while (!kaapi_list_empty(list))
  {
    kaapi_task_t* task = kaapi_list_pop_tail(list);
    kaapi_sched_select_queue_attr_task( rsrc, local, task );
  }
  return 0;
}


/*
*/
kaapi_atomic_t counter_kproc = {0};
int kaapi_sched_select_queue_cyclic(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* local,
    kaapi_list_t* list
)
{
  while (!kaapi_list_empty(list))
  {
    kaapi_task_t* task = kaapi_list_pop_tail(list);
    if (kaapi_default_param.strict_push)
      task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;
    if (task->u.s.flag & KAAPI_TASK_FLAG_MASK_AFFINITY)
      kaapi_sched_select_queue_attr_task( rsrc, local, task );
    else
    {
      kaapi_team_t* team = kaapi_self_processor()->team;
      int count = team->count;
      int kid = KAAPI_ATOMIC_INCR(&counter_kproc) % count;
      kaapi_place_t* ld = team->all_kprocessors[kid]->rsrc.ownplace;
      if (ld == local)
        return kaapi_place_push( rsrc, ld, task );
      else
        return kaapi_place_push_remote( rsrc, ld, task );
    }
  }
  return 0;
}


/** Local first strategy
*/
int kaapi_sched_select_queue_local(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* local,
    kaapi_list_t* list
)
{
  kaapi_list_t localtasks;
  kaapi_list_init(&localtasks);
  while (!kaapi_list_empty(list))
  {
    kaapi_task_t* task = kaapi_list_pop_tail(list);
    if (task->u.s.flag & KAAPI_TASK_FLAG_MASK_AFFINITY)
      kaapi_sched_select_queue_attr_task(rsrc, local, task  );
    else
    {
      if (kaapi_default_param.strict_push)
        task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;
      kaapi_list_push_head(&localtasks, task);
    }
  }

  if (!kaapi_list_empty(&localtasks))
    kaapi_place_pushlist( rsrc, local, &localtasks );

  kaapi_list_destroy( &localtasks );

  return 0;
}

/** Centralised strategy
*/
static int kaapi_sched_select_queue_centralized(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* local,
    kaapi_list_t* list
)
{
  return 
    kaapi_place_pushlist( rsrc,
          kaapi_default_param.places_set[KAAPI_HWS_LEVELID_MACHINE].places[0],
          list
    );
}

/** Global strategy
*/
int kaapi_sched_select_queue_global(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* local,
    kaapi_list_t* list
)
{
  kaapi_list_t localtasks;
  kaapi_list_init(&localtasks);
  while (!kaapi_list_empty(list))
  {
    kaapi_task_t* task = kaapi_list_pop_head(list);
    if (task->u.s.flag & KAAPI_TASK_FLAG_PRIORITY)
    {
      kaapi_place_t* ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_MACHINE].places[0];
      kaapi_place_push(rsrc, ld, task);
    }
    else
      kaapi_list_push_tail(&localtasks, task);
  }
#if defined(KAAPI_USE_NUMA)
  if (!kaapi_list_empty(&localtasks))
    kaapi_sched_select_queue_Wnuma( rsrc, local, &localtasks );
#else
    kaapi_place_pushlist(rsrc, local, &localtasks);
#endif

  kaapi_list_destroy( &localtasks );

  return 0;
}


#if defined(KAAPI_USE_NUMA)
/**
*/
int kaapi_sched_select_queue_localnuma(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* local,
    kaapi_list_t* list
)
{
  kaapi_list_t localtasks;
  kaapi_list_init(&localtasks);
  while (!kaapi_list_empty(list))
  {
    kaapi_task_t* task = kaapi_list_pop_tail(list);
    if (task->u.s.flag & KAAPI_TASK_FLAG_MASK_AFFINITY)
      kaapi_sched_select_queue_attr_task(rsrc, local, task );
    else
    {
      if (kaapi_default_param.strict_push)
        task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;
      kaapi_list_push_head(&localtasks, task);
    }
  }

  if (!kaapi_list_empty(&localtasks))
  {
    kaapi_place_t* ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid];
    kaapi_place_pushlist( rsrc, ld, &localtasks );
  }

  kaapi_list_destroy( &localtasks );

  return 0;
}
#endif


#if defined(KAAPI_USE_NUMA)
/* initialisation for whws
*/
static void kaapi_sched_whws_init()
{
  kaapi_default_param.enable_push_local = 1;
}

/** Return the numa queue were a W param of the task is stored.
    One possible way to improve the code is to make use of asynchronous
    kaapi_place_push_XXX.
    When all tasks have been pushed, then make completion of asynchronous calls.
    Note: it means that place_empty should return 0 iff pending push request exists
    an over approximation is to has at least a pending request !
*/
int kaapi_sched_select_queue_Wnuma(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* local,
    kaapi_list_t* list
)
{
  kaapi_team_t* team = kaapi_self_processor()->team;
  kaapi_task_t* task;
  kaapi_place_t *ld;
  kaapi_access_t* access;
  kaapi_memory_view_t view;
  unsigned int i;
  unsigned int count_params;

  kaapi_place_group_operation_t kpgo;
  kaapi_place_group_operation_init( &kpgo );

  while (!kaapi_list_empty(list))
  {
    task = kaapi_list_pop_head(list);
    if (task->u.s.flag & KAAPI_TASK_FLAG_MASK_AFFINITY)
      kaapi_sched_select_queue_attr_task(rsrc, local, task );
    else
    {
      const kaapi_format_t* fmt = task->fmt;
      kaapi_assert((rsrc != 0) && (fmt !=0));
      /* else FIXEME: push task on local numa node ?
        kaapi_sched_select_queue_local(rsrc, task, local);
      */
      if (kaapi_default_param.strict_push)
        task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;

      count_params = kaapi_format_get_count_params( fmt, task->arg );
      for (i=0; i<count_params; ++i)
      {
        kaapi_access_mode_t mode            = kaapi_format_get_mode_param(fmt, i, task->arg);
        if (!KAAPI_ACCESS_IS_WRITE(mode)) continue;

        access = kaapi_format_get_access_param(fmt, i, task->arg);
        kaapi_format_get_view_param(fmt, i, kaapi_task_getargs(task), &view);
        void* ptr = kaapi_memory_view2pointer(access->data, &view );
        int numanode = kaapi_numa_getpage_id(ptr);

        if (numanode == -1) 
          numanode = rsrc->numaid;

        /* find the numa queue for node */
        if (numanode < team->numaplaces.count)
        {
          /* If this node is our numa node, check if we should push to local kproc (Whws strat) */
          if (kaapi_default_param.enable_push_local &&
              (!KAAPI_CPUSET_ISSET(numanode, &team->numa_bitmap) || (numanode == rsrc->numaid)))
            ld = rsrc->ownplace;
          else
            ld = team->numaplaces.places[numanode];
        }
        else
          ld = team->numaplaces.places[numanode % team->numaplaces.count];
        if (ld != 0)
        {
          if ((ld == kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid])
           || (ld == rsrc->ownplace))
            kaapi_place_push_async( &kpgo, rsrc, ld, task );
          else
            kaapi_place_push_remote_async( &kpgo, rsrc, ld, task );
          task = 0;
          break;
        }
      } // end for
      if (task) /* local numa node */
      {
        ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid];
        kaapi_place_push_async( &kpgo, rsrc, ld, task );
      }
    }
  }
  kaapi_place_group_operation_wait( &kpgo );
  kaapi_place_group_operation_fini( &kpgo );

  return 0;
}
#endif


#if defined(KAAPI_USE_PERFCOUNTER)

/* ETF initialization */
static void kaapi_sched_init_etf()
{
  kaapi_perf_idset_add( &kaapi_tracelib_param.taskperfctr_idset, KAAPI_PERF_ID_WORK );
}

static uint64_t* machine_state = 0;
static kaapi_lock_t lock_state = KAAPI_LOCK_INITIALIZER;
/*
*/
int kaapi_sched_select_queue_etf(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* local,
    kaapi_list_t* list
)
{
  while (!kaapi_list_empty(list))
  {
    kaapi_task_t* task = kaapi_list_pop_tail(list);
    if (task->u.s.flag & KAAPI_TASK_FLAG_MASK_AFFINITY)
      kaapi_sched_select_queue_attr_task(rsrc, local, task );
    else
    {
      const kaapi_format_t* fmt = task->fmt;
      int idx = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_WORK);
      if ((rsrc == 0) || (fmt ==0)||
          ((kaapi_tracelib_param.taskperfctr_idset & idx) ==0))
      {
        /* FIXEME: push task on local numa node ? */
        kaapi_sched_select_queue_attr_task(rsrc, local, task);
      }
      else
      {
        /* compute time for this task */
        kaapi_team_t* team = kaapi_self_team();
        kaapi_named_perfctr_t* perf = fmt->perf;
        uint64_t time_task = 0;
        uint64_t count = 0;
        int kid;
        for (kid=0; kid<team->count; ++kid)
        {
          kaapi_perfstat_t* stats = perf->kproc_stats[kid];
          if (stats == 0) continue;
          time_task += stats->sum_counters[KAAPI_PERF_ID_WORK];
          count += stats->count;
        }
        if (count >0)
          time_task /= count;
        if (time_task==0)
          time_task = 1;

        /* map the task on the ressource that minize the completion time 
           do not take into account communication
        */
        kaapi_atomic_lock(&lock_state);
        if (machine_state == 0)
        {
          machine_state = (uint64_t*)malloc( sizeof(uint64_t)* team->count );
          uint64_t date = kaapi_get_elapsedns();
          for (kid = 0; kid < team->count; ++kid)
            machine_state[kid] = date;
        }

        uint64_t date = kaapi_get_elapsedns();
        uint64_t date_min = 0;
        int kid_min = 0;
        for (kid = 0; kid < team->count; ++kid)
        {
      #if 0
          if (machine_state[kid] < date)
            machine_state[kid] = date;
      #endif
          uint64_t date_kid = machine_state[kid]+time_task;
      //printf("[ETF] if----> map task %s on kid: %i, date:%lu, timetask: %lu\n", perf->name, kid, date_kid, time_task);

          if (date_min ==0) { date_min = date_kid; kid_min = kid; }
          else if (date_kid < date_min)
          {
            date_min = date_kid;
            kid_min = kid;
          }
        }
        machine_state[kid_min] = date_min;
        kaapi_atomic_unlock(&lock_state);

        kaapi_place_t* ld = team->all_kprocessors[kid_min]->rsrc.ownplace;
      //printf("[ETF] map task %s to kid: %i, date:%lu,  timetask: %lu\n", perf->name, kid_min, date_min, time_task);
        kaapi_place_push( rsrc, ld, task );
      }
    }
  }
  return 0;
}
#endif


#if defined(KAAPI_USE_OFFLOAD)
int kaapi_sched_select_queue_offload_rand(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* local,
    kaapi_list_t* list
)
{
  if (kaapi_default_param.places_set[KAAPI_HWS_LEVELID_OFFLOAD_CUDA].count ==0)
#if defined(KAAPI_USE_NUMA)
    return kaapi_sched_select_queue_localnuma( rsrc, local, list );
#else
    return kaapi_sched_select_queue_local( rsrc, local, list );
#endif

  while (!kaapi_list_empty(list))
  {
    kaapi_task_t* task = kaapi_list_pop_tail(list);

    const kaapi_format_t* fmt = task->fmt;
    /* to do: offload place could be associated to arch: select the arch... */
    if ((fmt !=0) && (kaapi_format_get_task_body_by_arch( fmt, KAAPI_PROC_TYPE_CUDA) !=0))
    {
       /* push task using random place */
       int id;
       if (rsrc ==0)
         id = rand();
       else
         id = rand_r(&rsrc->seed);

       id = id % kaapi_default_param.places_set[KAAPI_HWS_LEVELID_OFFLOAD_CUDA].count;
       kaapi_place_t* ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_OFFLOAD_CUDA].places[id];
#if defined(_OFFLOAD_DEBUG)
      fprintf(stdout, "%s push to offload LD:%i @:%p\n", __FUNCTION__, id, (void*)ld);
      fflush(stdout);
#endif
      kaapi_place_push( rsrc, ld, task );
    }
    else
      kaapi_sched_select_queue_attr_task(rsrc, local, task);
  }
  return 0;
}

static inline size_t _kaapi_sched_data_aware_get_valid_footprint(
  kaapi_address_space_id_t asid,
  kaapi_task_t* task
)
{
  unsigned int count_params;
  unsigned int ith;
  const kaapi_format_t* fmt = task->fmt;
  count_params = kaapi_format_get_count_params(fmt, kaapi_task_getargs(task));
  size_t footprint = 0;
  for(ith= 0; ith < count_params; ++ith)
  {
    kaapi_access_mode_t m = kaapi_format_get_mode_param(fmt, ith, kaapi_task_getargs(task));
    kaapi_access_mode_t mp = KAAPI_ACCESS_GET_MODE(m);
    kaapi_access_t* access;
    kaapi_memory_view_t view;
    kaapi_metadata_info_t* mdi;
    if (mp & KAAPI_ACCESS_MODE_V)
      continue;
    access = kaapi_format_get_access_param(fmt, (unsigned int)ith, kaapi_task_getargs(task));
    kaapi_format_get_view_param(fmt, (unsigned int)ith, kaapi_task_getargs(task), &view);
    mdi = kaapi_memory_mdi_lookup(access->data, kaapi_local_asid, &view);
    if (kaapi_memory_replica_is_valid(mdi, asid) == true)
    {
      footprint += kaapi_memory_replica_view_size(mdi, asid);
    }
  } // end for
  return footprint;
}

/* data-aware strategy */
int kaapi_sched_select_queue_offload_data_aware(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* local,
    kaapi_list_t* list
)
{
  if (kaapi_default_param.places_set[KAAPI_HWS_LEVELID_OFFLOAD_CUDA].count ==0)
#if defined(KAAPI_USE_NUMA)
    return kaapi_sched_select_queue_localnuma( rsrc, local, list );
#else
    return kaapi_sched_select_queue_local( rsrc, local, list );
#endif

  while (!kaapi_list_empty(list))
  {
    kaapi_task_t* task = kaapi_list_pop_tail(list);

    const kaapi_format_t* fmt = task->fmt;
    /* to do: offload place could be associated to arch: select the arch... */
    if ((fmt !=0) && (kaapi_format_get_task_body_by_arch( fmt, KAAPI_PROC_TYPE_CUDA) !=0))
    {
      int id;
      size_t footprint= 0;
      size_t max_footprint= 0;
#define DATA_MAX_CONFLICT 32
      int id_equal[DATA_MAX_CONFLICT]; /* devices with the same footprint */
      int nrepeat = 0; /* number of devices with the same footprint */
      int target_id = local->level; /* target device with maximum valid footprint */
      
      for(id = 0; id < kaapi_default_param.places_set[KAAPI_HWS_LEVELID_OFFLOAD_CUDA].count; id++)
      {
        kaapi_offload_place_t* place =(kaapi_offload_place_t*)kaapi_default_param.places_set[KAAPI_HWS_LEVELID_OFFLOAD_CUDA].places[id];
        kaapi_address_space_id_t asid = place->device->memnode.asid;
        footprint = _kaapi_sched_data_aware_get_valid_footprint(asid, task);
        if(footprint == max_footprint)
        {
          id_equal[nrepeat] = id;
          if(nrepeat < DATA_MAX_CONFLICT)
            nrepeat++;
        }
        if(footprint > max_footprint)
        {
          target_id = id;
          max_footprint = footprint;
          nrepeat = 0;
        }
      } // end for
      
      /* no one has valid copies: push at random place */
      if(max_footprint == 0)
      {
        if (rsrc == 0)
          id = rand();
        else
          id = rand_r(&rsrc->seed);
        target_id = id % kaapi_default_param.places_set[KAAPI_HWS_LEVELID_OFFLOAD_CUDA].count;
      } else {
          /* some devices had the same footprint -> choose at random among them */      
        if(nrepeat > 0)
        {
          if (rsrc == 0)
            id = rand();
          else
            id = rand_r(&rsrc->seed);
          target_id = id_equal[(id % nrepeat) % DATA_MAX_CONFLICT];
        }
      }
      
      kaapi_place_t* ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_OFFLOAD_CUDA].places[target_id];
#if defined(_OFFLOAD_DEBUG)
      fprintf(stdout, "%s push to offload LD:%i @:%p (footprint=%zu repeated=%d)\n", __FUNCTION__, target_id, (void*)ld,
              max_footprint, nrepeat);
      fflush(stdout);
#endif
      kaapi_place_push( rsrc, ld, task );
    } // end if
    else
      kaapi_sched_select_queue_attr_task(rsrc, local, task);
  }
  return 0;
}
#endif


/*
*/
kaapi_strategy_selectvictim_t kaapi_default_selectvictim[] = {
  { "rand",    &kaapi_sched_select_victim_rand},
  { "global",  &kaapi_sched_select_victim_global},
#if defined(KAAPI_USE_NUMA)
  { "local",   &kaapi_sched_select_victim_strict_push},
  { "numa",    &kaapi_sched_select_victim_rand_numa},
#endif
#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
  { "hws_P",   &kaapi_sched_select_victim_hierarchy_kproc},
  { "hws_N",   &kaapi_sched_select_victim_hierarchy_numa},
  { "hws_P_N", &kaapi_sched_select_victim_hierarchy_kproc_numa},
  { "hws_N_P", &kaapi_sched_select_victim_hierarchy_numa_kproc},
  { "wnuma",   &kaapi_sched_select_victim_numa},
  { "dnuma",   &kaapi_sched_select_victim_hierarchy_distance_numa},
#endif
  { 0, 0 }
};

/*
*/
kaapi_strategy_pushinit_t kaapi_default_pushinit[] = {
  { "wspush",           0, &kaapi_sched_push_init_queue_wspush },
  { "global",           0, &kaapi_sched_push_init_queue_global},
  { "cyclic",           0, &kaapi_sched_push_init_queue_cyclickproc},
  { "rand",             0, &kaapi_sched_push_init_queue_randkproc},
#if defined(KAAPI_USE_NUMA)
  { "cyclicnuma",       0, &kaapi_sched_push_init_queue_cyclicnuma},
  { "randnuma",         0, &kaapi_sched_push_init_queue_randnuma},
  { "cyclicnumastrict", 0, &kaapi_sched_push_init_queue_cyclicnumastrict},
#endif
  { 0, 0, 0 }
};

/*
*/
kaapi_strategy_push_t kaapi_default_push[] = {
  { "local",  0, &kaapi_sched_select_queue_local},
  { "global", 0, &kaapi_sched_select_queue_global},
  { "centralized", 0, &kaapi_sched_select_queue_centralized},
#if defined(KAAPI_USE_NUMA)
  { "numa",   0, &kaapi_sched_select_queue_localnuma},
  { "Wnuma",  0, &kaapi_sched_select_queue_Wnuma},
  { "Whws",   &kaapi_sched_whws_init, &kaapi_sched_select_queue_Wnuma},
#endif
#if defined(KAAPI_USE_PERFCOUNTER)
  { "etf",    &kaapi_sched_init_etf, &kaapi_sched_select_queue_etf },
#endif
  { "attr",   0, &kaapi_sched_select_queue_attr},
  { "cyclic", 0, &kaapi_sched_select_queue_cyclic},
#if defined(KAAPI_USE_OFFLOAD)
  { "offload_rand", 0, &kaapi_sched_select_queue_offload_rand},
  { "offload_data", 0, &kaapi_sched_select_queue_offload_data_aware},
#endif
  { 0, 0, 0 }
};

