/*
** kaapi_sched_stealprocessor.c
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"

/* to store past visited access of task in the stack */
typedef struct kaapi_gd_t {
  kaapi_access_mode_t         last_mode;    /* last access mode to the data */
  void*                       last_version; /* last verion of the data, 0 if not ready */
  kaapi_access_t*             last_writer;  /* */
  kaapi_access_t*             last_access;  /* */
#if defined(KAAPI_DEBUG)
  struct kaapi_task_t*        last_task;    /* last task with last mode */
#endif
} kaapi_gd_t;


/** \ingroup WS
    Compute if the task is ready using the history of accessed stored in map.
    On return the bitset of paramaters with war dependencies or cw accesses are
    returned.
    \retval the number of non ready parameters
 Compute if the task with arguments pointed by sp and with format task_fmt is ready
 Return the number of non ready data
 */
int kaapi_task_computeready(
  kaapi_task_t*          task,
  const kaapi_format_t*  task_fmt,
  unsigned int*          war_param,
  unsigned int*          cw_param,
  kaapi_hashmap_t*       map
)
{
  int wc = 0;
  if (task->u.s.flag & KAAPI_TASK_FLAG_DFGOK)
    return KAAPI_ATOMIC_READ( &task->wc );

  kaapi_assert_debug( (task_fmt !=0) || (task->u.s.flag & KAAPI_TASK_FLAG_INDPENDENT) );

  if ((task->u.s.flag & KAAPI_TASK_FLAG_INDPENDENT) ==0)
  {
    size_t count_params;
    unsigned int i;
    
    kaapi_assert_debug( task->body == task_fmt->entrypoint[KAAPI_PROC_TYPE_HOST] );
    task->fmt = task_fmt;

    count_params = kaapi_format_get_count_params(task_fmt, kaapi_task_getargs(task));
    wc = (int)count_params;
    
    for (i=0; i<count_params; ++i)
    {
      kaapi_access_mode_t m = kaapi_format_get_mode_param(task_fmt, i, kaapi_task_getargs(task));
      kaapi_access_mode_t mp = KAAPI_ACCESS_GET_MODE(m);
      if (mp & KAAPI_ACCESS_MODE_V)
      {
        --wc;
        continue;
      }
      kaapi_access_t* access = kaapi_format_get_access_param(task_fmt, i, kaapi_task_getargs(task));
      kaapi_memory_view_t view;
      kaapi_format_get_view_param(task_fmt, i, kaapi_task_getargs(task), &view);
      void* ptr = kaapi_memory_view2pointer(access->data, &view );

      /* */
      kaapi_hashentries_t* entry = kaapi_hashmap_findinsert( map, ptr );
      kaapi_assert_debug( sizeof(entry->u) >= sizeof(kaapi_gd_t) );
      kaapi_gd_t* gd = &KAAPI_HASHENTRIES_GET(entry, kaapi_gd_t);

#if 0 // bug if first access of multiple accesses by the same task is not a write
      /* some tasks may have multiple access to same data. we hope that write mode at the first
         position.
      */
      if ((gd->last_access != KAAPI_ACCESS_MODE_VOID) && (gd->last_access->task == task))
      {
#if 0
        kaapi_assert( !KAAPI_ACCESS_IS_READ(mp) || !KAAPI_ACCESS_IS_ONLYWRITE(gd->last_access->mode) );
        --wc;
        continue;
#endif
    int j;
kaapi_dbg_print_lock();
    printf("task: @:%p, '%s' :: ", (void*)task, task_fmt->name );
    for (j=0; j<count_params; ++j)
    {
      kaapi_access_mode_t m = kaapi_format_get_mode_param(task_fmt, j, kaapi_task_getargs(task));
      kaapi_access_mode_t mp = KAAPI_ACCESS_GET_MODE(m);
      if (mp & KAAPI_ACCESS_MODE_V)
      {
        printf("V ");
        continue;
      }

      kaapi_access_t* access = kaapi_format_get_access_param(task_fmt, j, kaapi_task_getargs(task));
      kaapi_memory_view_t view;
      kaapi_format_get_view_param(task_fmt, j, kaapi_task_getargs(task), &view);
      void* ptr = kaapi_memory_view2pointer(access->data, &view );

      if (KAAPI_ACCESS_IS_ONLYWRITE(mp))
        printf("W,@:%p ", ptr);
      else if (KAAPI_ACCESS_IS_READWRITE(mp))
        printf("X,@:%p ", ptr);
      else if (KAAPI_ACCESS_IS_READ(mp))
        printf("R,@:%p ", ptr);
      else if (KAAPI_ACCESS_IS_CUMULWRITE(mp))
        printf("C,@:%p ", ptr);
    }
printf("\n");
kaapi_dbg_print_unlock();
     
      }
#endif

      /* compute readyness of access
         - note that stack access, even if it as R|W flag is always concurrent
      */
      if (   KAAPI_ACCESS_IS_ONLYWRITE(mp)
          || (gd->last_mode == KAAPI_ACCESS_MODE_VOID)
          || (KAAPI_ACCESS_IS_STACK(mp))
          || (KAAPI_ACCESS_IS_CONCURRENT(mp, gd->last_mode))
          /* if two access on the same task */
          || ((gd->last_writer !=0)
           && (gd->last_writer->task == task))
         )
      {
        --wc;
        /* set bit i in war_param, if copy on steal is required */
        if (  
              (KAAPI_ACCESS_IS_ONLYWRITE(mp)  && (gd->last_mode != KAAPI_ACCESS_MODE_VOID))
           || (KAAPI_ACCESS_IS_CUMULWRITE(mp) && KAAPI_ACCESS_IS_CONCURRENT(mp,gd->last_mode)
                                              && ((m & KAAPI_ACCESS_MODE_IP)==0))
           || (KAAPI_ACCESS_IS_STACK(mp))
        )
        {
          /* stack data are reused through recursive task execution and copied on steal but never merged */
          *war_param |= 1<<i;
        }
        if (KAAPI_ACCESS_IS_CUMULWRITE(mp) && ((m & KAAPI_ACCESS_MODE_IP) ==0))
          *cw_param |= 1<<i;
      }

      /* update map information for next access if no set */
      if ((gd->last_mode == KAAPI_ACCESS_MODE_VOID)
        || KAAPI_ACCESS_IS_WRITE(mp)
        || KAAPI_ACCESS_IS_CUMULWRITE(mp)
      )
      {
        gd->last_mode   = mp;
        if (KAAPI_ACCESS_IS_WRITE(mp) || KAAPI_ACCESS_IS_CUMULWRITE(mp))
          gd->last_writer = access; /* may be not a writer if last_mode was equal to void */
#if defined(KAAPI_DEBUG)
        gd->last_task = task;
#endif
      }
    }
  }

  return wc;
}



/** \ingroup TASK
    Steal a task. Dependencies must have been computed before.

    \retval 0 if case of successfull steal of the task
    \retval ENOENT the task does not have format and cannot be stolen
    \retval EACCES the task is not ready due to data flow dependency
    \retval EINVAL the task cannot be execute on the any of the requests
    \retval EPERM the task is not stealable: either state is not INIT or flag unstealable is set
    \retval EBUSY the task has not been stolen because some body else has steal it or execute it.
*/
static int kaapi_sched_steal_task
(
  kaapi_task_t*                        task
)
{
  if (task->body ==kaapi_tasksteal_body) 
    return EPERM;

  /* body != task->body it means:
     - 0: owner takes it
     - wasexec: owner executes it
     - nop,aftersteal : stealer write it terminaison
     - slowexec: task already steal
  */
  if (task->state.steal != task->body)
    return EALREADY;

  if (!kaapi_task_isstealable(task))
    return EPERM;

  int retval = kaapi_task_marksteal( task );
  if (unlikely( !retval ) ) 
    return EBUSY;

  kaapi_assert_debug( (task->state.steal == kaapi_slowexec_body) || (task->state.steal == 0));
  return 0;
}


/*
*/
int kaapi_sched_copy_steal_task
(
  kaapi_task_t*           task,
  unsigned int            war_param,
  unsigned int            cw_param,
  kaapi_frame_wrdlist_t*  masterframe,
  kaapi_steal_request_t*  request
)
{
  /* - create the task steal that will execute the stolen task
     The task stealtask stores:
     - the original task pointer
     - the pointer to shared data with R / RW access data
     - and at the end it reserve enough space to store original task arguments
  */
  kaapi_tasksteal_arg_t* argsteal 
    = (kaapi_tasksteal_arg_t*)kaapi_request_pushdata(request, sizeof(kaapi_tasksteal_arg_t));
  argsteal->origin_task     = task;
  argsteal->origin_frame_rd = masterframe;
  argsteal->war_param       = war_param;
  argsteal->cw_param        = cw_param;

  kaapi_request_pushtask( request, 0, kaapi_tasksteal_body, argsteal );
  kaapi_request_reply( (kaapi_request_t*)request, KAAPI_REQUEST_S_OK);
  return 0;
}

/*
*/
int kaapi_sched_copy_stolentask_onstack
(
  kaapi_thread_t*         thread,
  kaapi_task_t*           task,
  unsigned int            war_param,
  unsigned int            cw_param,
  kaapi_frame_wrdlist_t*  masterframe
)
{
  /* - create the task steal that will execute the stolen task
     The task stealtask stores:
     - the original task pointer
     - the pointer to shared data with R / RW access data
     - and at the end it reserve enough space to store original task arguments
  */
  kaapi_tasksteal_arg_t* argsteal 
    = (kaapi_tasksteal_arg_t*)kaapi_thread_pushdata(thread, sizeof(kaapi_tasksteal_arg_t));
  argsteal->origin_task     = task;
  argsteal->origin_frame_rd = masterframe;
  argsteal->war_param       = war_param;
  argsteal->cw_param        = cw_param;

  _kaapi_task_push( thread, kaapi_tasksteal_body, argsteal );
  kaapi_task_commit( thread );
  return 0;
}


/** \ingroup TASK
    Try to steal a DFG task using the history of access stored in map.
    If map 0 then the dependencies are not computed, this is used to
    steal tasks from queues in hws because these tasks are assumed to
    be always ready.
    
    \retval 0 if case of successfull steal of the task
    \retval ENOENT the task does not have format and cannot be stolen
    \retval EACCES the task is not ready due to data flow dependency
    \retval EINVAL the task cannot be execute on the any of the requests
    \retval EPERM the task is not stealable: either state is not INIT or flag unstealable is set
    \retval EBUSY the task has not been stolen because some body else has steal it or execute it.
*/
static int kaapi_sched_trysteal_task
(
  kaapi_context_t*                     context,
  kaapi_frame_t*                       frame,
  kaapi_hashmap_t*                     map,
  kaapi_task_t*                        task,
  unsigned int*                        war_param,
  unsigned int*                        cw_param,
  struct kaapi_listrequest_iterator_t* lrrange
)
{
  int err;
  const kaapi_format_t* task_fmt;

  if (task->body ==kaapi_tasksteal_body) 
    return EPERM;

  task_fmt = kaapi_format_resolve_bybody( task->body );
  if (task_fmt == 0)
    return ENOENT;

  *war_param = 0;
  *cw_param = 0;
  if ((map !=0) && !(task->u.s.flag & KAAPI_TASK_FLAG_INDPENDENT))
  {
    int wc = 0;
    /* if map == history of visited access, then compute readiness
       - even if the task is not stealable, then it is necessary to
       propagate data flow constraints.
    */
    wc = kaapi_task_computeready(
      task,
      task_fmt,
      war_param,
      cw_param,
      map
    );

    if (wc !=0)
      return EACCES;
  }

  err = kaapi_sched_steal_task(task);
  return err;
}


/** \ingroup WS
    This method tries to steal work from the tasks of a stack passed in argument.
    The method iterates through all the tasks in the stack until it found a ready task
    or until the request count reaches 0.
    The current implementation is cooperative or concurrent depending of configuration flag.
    only exported for kaapi_stealpoint.
    \param stack the victim stack
    \param task the current running task (cooperative) or 0 (concurrent)
    \retval the number of positive replies to the thieves
*/
static int kaapi_sched_steal_stack_from_frame
( 
    kaapi_processor_t*                   kproc,
    kaapi_context_t*                     context,
    kaapi_frame_t*                       frame,
    int                                  input_thieffp,
    struct kaapi_listrequest_iterator_t* lrrange
)
{
  kaapi_frame_dfgctxt_t* dfgctxt = 0;
  kaapi_frame_dfgctxt_t  buffer;
  int                    buffer_init = 0;
  kaapi_stack_iterator_t iter;
  kaapi_task_t*          task;
  kaapi_task_body_t      task_body;
  int                    err;
  int                    thieffp;
  unsigned int           war_param;
  unsigned int           cw_param;

  kaapi_assert_debug ((context != 0) && (context->unstealable == 0) && (frame !=0));

  thieffp = input_thieffp;
  do
  {
    /* owner try to pop : */
    ++thieffp;
    context->stack.thieffp = thieffp;
    kaapi_assert_debug(__kaapi_isaligned( &context->stack.thieffp, sizeof(context->stack.thieffp)));
    kaapi_mem_barrier();
    if (thieffp >= context->stack.popfp)
      break;

    if (frame->flag & KAAPI_FRAME_FLAG_NOTSTEAL) 
      goto return_val;

    /* Allocate & intialize dfg context for stealing task with dependencies */
    if (frame->rdlist !=0)
      dfgctxt = frame->rdlist->ctxt;
    if (dfgctxt ==0)
    {
      {
        dfgctxt = &buffer;
        if (!buffer_init)
        {
          kaapi_hashmap_init(&buffer.access_to_gd, buffer.mapentries, KAAPI_SIZE_DFGCTXT, &buffer.mapbloc);
          buffer_init = 1;
        }
        else {
          /* new frame: clear hashmap */
          kaapi_hashmap_clear( &buffer.access_to_gd );
        }
      }
    }

    /* try to steal task into the frame. iteration follows creation order */
    kaapi_assert_debug(dfgctxt !=0);

    kaapi_stack_iterator_init(&iter, &context->stack, frame);
    task = kaapi_stack_iterator_get(&iter);
    while (!kaapi_stack_iterator_empty(&iter) && !kaapi_listrequest_iterator_empty(lrrange))
    {
      task = kaapi_stack_iterator_get(&iter);
      err = kaapi_sched_trysteal_task( context, frame, &dfgctxt->access_to_gd, task, &war_param, &cw_param, lrrange );

      /* then try to split the task */
      if (((err == EPERM) || (err == EBUSY)) && kaapi_task_is_splittable(task))
      {
        task_body = task->body;
        if ((task->state.steal == task_body) || (task->state.steal ==0)) /* means init / exec */
        {
          kaapi_adaptivetask_splitter_t splitter;
          splitter = kaapi_task_getargst( task, kaapi_taskadaptive_arg_t)->user_splitter;
          if (splitter !=0) 
            splitter( &(kaapi_task_getargst( task, kaapi_taskadaptive_arg_t)->user_task), lrrange );
        }
      }
      else if (err == 0)
      {
        //printf("Steal task %p '%s' in frame !\n", task, task->fmt->name);
        kaapi_steal_request_t* request = (kaapi_steal_request_t*)kaapi_listrequest_iterator_get(lrrange);
        err = kaapi_sched_copy_steal_task(task, war_param, cw_param, 0, request);
#if defined (KAAPI_USE_PERFCOUNTER)
        KAAPI_PERFCTR_INCR(kaapi_thread2perfproc(kaapi_kproc2thread(kaapi_self_processor())), KAAPI_PERF_ID_TASKSPAWN, 1);
#endif
        kaapi_listrequest_iterator_next(lrrange);
      }
      
      kaapi_stack_iterator_next(&iter);
    }
    kaapi_stack_iterator_destroy(&iter);
    if (kaapi_listrequest_iterator_empty(lrrange))
      break;

    dfgctxt = 0;
    kaapi_assert_debug( __kaapi_isaligned(&frame->next, sizeof(kaapi_frame_t*)));
    frame = frame->next;

  } while (frame !=0);

return_val:
  /* reset to 0 : stealer should break stealing in the stack */
  context->stack.thieffp = 0;
  if (buffer_init)
    kaapi_hashmap_destroy( &buffer.access_to_gd );

  return 0;
}


/* 8 MBytes */
#define LLC_SIZE (8*1024*1024UL)

/* Steal into stack, first
*/
static int kaapi_sched_steal_stack
( 
    kaapi_processor_t*                   kproc,
    kaapi_context_t*                     context,
    struct kaapi_listrequest_iterator_t* lrrange
)
{
  kaapi_assert_debug((context !=0) && (context->unstealable == 0));

  /* toplevel frame == empty */
  kaapi_frame_t* frame = kaapi_stack_topframe(&context->stack);
  kaapi_assert_debug( __kaapi_isaligned(frame, sizeof(kaapi_frame_t*)));
  if (frame ==0)
    return 0;

  int thieffp = 0;

#if 0
  /* Here: test to select LLC marked executable task in order to steal form the next frame 
     i.e. the frame of tasks generated by the first LLC marked task.
  */
  kaapi_task_t* task = frame->save_pc;
  const kaapi_format_t* fmt = 0;
  size_t size = 0;
  while (frame != 0)
  {
    if ( frame->flag & KAAPI_FRAME_FLAG_NOTSTEAL)
      return EINVAL;
    ++thieffp;
    context->stack.thieffp = thieffp;
    kaapi_assert_debug(__kaapi_isaligned( &context->stack.thieffp, sizeof(context->stack.thieffp)));
    kaapi_mem_barrier();
    if (thieffp >= context->stack.popfp)
    {
      thieffp = 0;
      break;
    }

    if (task ==0)
    {
      frame = frame->next;
      task = frame->save_pc;
      continue;
    }

    fmt = _kaapi_task_getformat(task);
    if ((fmt !=0) && ((size =_kaapi_task_getsize( task, fmt )) <= LLC_SIZE))
    {
      break;
    }

    frame = frame->next;
    if (frame == 0) break;
    task = frame->save_pc;
  }
  if (thieffp ==0)
  {
    context->stack.thieffp = thieffp;
    return 0;
  }

  if (frame == 0)
  { /* try to steal from the begining of the stack */
//    frame = kaapi_stack_topframe(&context->stack);
//    thieffp = 0;
    return 0;
  }
  else
  {
   // printf("Find task: %p with size %u <= LLC_SIZE (=%u), #frame:%i\n", (void*)task, (unsigned int)size, (unsigned int) LLC_SIZE, thieffp);
    return kaapi_sched_steal_stack_from_frame(kproc, context, frame, thieffp, lrrange);
  }
#else
  return kaapi_sched_steal_stack_from_frame(kproc, context, frame, thieffp, lrrange);
#endif
}


/* Steal on the stack. Other kind of steal operations on places are handled by selecting new steal order.
*/
int kaapi_sched_stealprocessor(
  kaapi_processor_t*                   kproc,
  struct kaapi_listrequest_iterator_t* lrrange
)
{
  kaapi_task_t* stask;
  kaapi_context_t* thread;

  /* */
  kaapi_assert_debug( !kaapi_listrequest_iterator_empty(lrrange) );

  /* 1/ steal current thread; make local copy because it can disappear
     note: isidle test degrades performances: may it is incorrectly 
     set in exec/sched_idle or sched_suspend ? 
  */
  if (((thread = kproc->context)!=0) && !thread->unstealable)
  {
    kaapi_sched_steal_stack( kproc, thread, lrrange );
    if (kaapi_listrequest_iterator_empty(lrrange))
      return 0;
  }

  /* Steal in the suspended threads list */
  if (!kaapi_listrequest_iterator_empty(lrrange) && !kaapi_list_empty(&kproc->lsuspend))
  {
    kaapi_atomic_lock(&kproc->lock_lsuspend);
    kaapi_list_iterator_t iter;
    kaapi_list_iterator_init( &kproc->lsuspend, &iter );
    while (!kaapi_listrequest_iterator_empty(lrrange) && !kaapi_list_iterator_empty(&iter))
    {
      stask = kaapi_list_iterator_get(&iter);
      thread = (kaapi_context_t*)stask->arg;
      if ((thread != 0) && !thread->unstealable)
        kaapi_sched_steal_stack( kproc, thread, lrrange );

      kaapi_list_iterator_next(&iter);
    }
    kaapi_atomic_unlock(&kproc->lock_lsuspend);
  }

  return 0;
}


/* Have request independent from OPeration with different operand
*/
kaapi_request_status_t kaapi_sched_emitsteal (
  kaapi_processor_t* kproc
)
{
  kaapi_request_status_t       retval;
  kaapi_victim_t               victim;
  kaapi_task_t*                task;
  int                          err;

redo_select:
  /* select the victim processor */
  err = kaapi_default_param.wsselect( kproc, &victim, KAAPI_SELECT_VICTIM );
  if (unlikely(err !=0)) 
    goto redo_select;

  retval = KAAPI_REQUEST_S_NOK;
  if (victim.ld ==0)
    goto return_value;

  /* quick test to detect if thread has no work */
  if (kaapi_place_empty(victim.ld))
  {
    kaapi_default_param.wsselect( kproc, &victim, KAAPI_STEAL_FAILED );
    goto return_value;
  }

#if defined (KAAPI_USE_PERFCOUNTER)
  KAAPI_EVENT_PUSH1(kproc->rsrc.evtkproc, 0, KAAPI_EVT_STEAL_OP, (uintptr_t)victim.ld );
#endif

  /* (1) Fill & Post the request to the victim processor 
  */
  if ((victim.ld == kproc->rsrc.ownplace) ||
      (victim.ld == kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[kproc->rsrc.numaid]))
  {
    task = kaapi_place_pop( &kproc->rsrc, victim.ld );
    if (task !=0)
    { /* must copy it into the stack of the thief processor */
      int err = kaapi_sched_copy_stolentask_onstack(kaapi_kproc2thread(kproc), task, 0, 0, task->state.frame );
#if defined (KAAPI_USE_PERFCOUNTER)
      KAAPI_PERFCTR_INCR(kaapi_thread2perfproc(kaapi_kproc2thread(kproc)), KAAPI_PERF_ID_TASKSPAWN, 1);
#endif
      kaapi_assert(err == 0);
      retval = KAAPI_REQUEST_S_OK;
    }
  }
  else
    retval = (kaapi_place_stealinplace(kproc, victim.ld) == 0 ? KAAPI_REQUEST_S_OK : KAAPI_REQUEST_S_NOK);

  if (retval == KAAPI_REQUEST_S_OK)
  {
    kaapi_default_param.wsselect( kproc, &victim, KAAPI_STEAL_SUCCESS );
#if defined (KAAPI_USE_PERFCOUNTER)
    KAAPI_PERFCTR_INCR(kproc->rsrc.perfkproc, KAAPI_PERF_ID_TASKSTEAL, 1);
#endif
  }
  else
    kaapi_default_param.wsselect( kproc, &victim, KAAPI_STEAL_FAILED );

return_value:
  KAAPI_PERFCTR_IF( kproc->rsrc.perfkproc, KAAPI_PERF_ID_STEALREQ,
    , /* no declaration */
    KAAPI_PERFCTR_INCR(kproc->rsrc.perfkproc, KAAPI_PERF_ID_STEALREQ, 1);
  );

  KAAPI_PERFCTR_IF( kproc->rsrc.perfkproc, KAAPI_PERF_ID_STEALREQOK,
    , /* no declaration */
    if (retval == KAAPI_REQUEST_S_OK)
      KAAPI_PERFCTR_INCR(kproc->rsrc.perfkproc, KAAPI_PERF_ID_STEALREQOK, 1);
  );

  return retval;
}
