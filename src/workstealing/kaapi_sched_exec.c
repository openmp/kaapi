/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"
#include <errno.h>
#include <stddef.h> // offsetof
#include <stdio.h>

#if defined(KAAPI_USE_PERFCOUNTER)
#include "kaapi_dbg.h"
#endif

// WARNING WARNING
#define OLD_IMPL 0

//#define LOG

static int __kaapi_sched_sync_rd_list(
  kaapi_stack_t*  stack,
  kaapi_frame_t*  unlink
);

static kaapi_task_t* _kaapi_sched_activate_pop_task(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t* frame_rd,
    kaapi_task_t* task
);

struct retrydata_t;
static kaapi_task_t* _kaapi_sched_steal_activated_task(
    kaapi_processor_t* kproc,
    kaapi_frame_wrdlist_t* frame_rd,
    struct retrydata_t* r
);

/* debug */
__attribute__((unused)) static char get_access_mode( kaapi_access_mode_t   mode )
{
  if (KAAPI_ACCESS_IS_READWRITE(mode)) 
    return 'x';
  if (KAAPI_ACCESS_IS_READ(mode)) 
    return 'r';
  if (KAAPI_ACCESS_IS_WRITE(mode)) 
    return 'w';
  if (KAAPI_ACCESS_IS_CUMULWRITE(mode)) 
    return 'c';
  return '?';
}

#if defined(KAAPI_USE_PERFCOUNTER)
/* utility */
static kaapi_named_perfctr_t* _kaapi_get_named_perfctr( kaapi_task_t* task, int* fmtid )
{
  kaapi_named_perfctr_t* namedperf =0;
  const kaapi_format_t* fmt = task->fmt;
  if (fmt == 0)
    fmt = kaapi_format_resolve_bybody( task->body );
  if (fmt ==0)
  {
    namedperf = 0;
    *fmtid = 0;
  }
  else {
    namedperf = fmt->perf;
    *fmtid = fmt->fmtid;
  }
  return namedperf;
}
#endif

/*
*/
#if defined(KAAPI_USE_PERFCOUNTER)
static const uint64_t MASK_PERF_READWRITE_ACCESS = 
   KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_READ)  | KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_WRITE) |
   KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_READ) | KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_WRITE);

static void _kaapi_count_access(
  kaapi_perfproc_t*         perfkproc,
  kaapi_evtproc_t*          evtkproc,
  kaapi_numaid_t            local_numaid,
  const kaapi_task_t*       task
)
{
  /* compute is task is local or remote with its parameters */
  if (task->fmt ==0) return;
  if ( !kaapi_perf_idset_test_mask(&kaapi_tracelib_param.perfctr_idset, MASK_PERF_READWRITE_ACCESS)
    && !(kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(KAAPI_EVT_TASK_ACCESS))
     )
    return;

  int count = kaapi_format_get_count_params(task->fmt, kaapi_task_getargs(task));
  kaapi_memory_view_t view;

  for (int i=0; i<count; ++i)
  {
    kaapi_access_mode_t m = kaapi_format_get_mode_param(task->fmt, i, kaapi_task_getargs(task));
    kaapi_access_mode_t mp = KAAPI_ACCESS_GET_MODE(m);
    if (mp & KAAPI_ACCESS_MODE_V)
      continue;

    kaapi_access_t* access = kaapi_format_get_access_param(task->fmt, i, kaapi_task_getargs(task));
    kaapi_format_get_view_param(task->fmt, i, kaapi_task_getargs(task), &view);
    void* ptr = kaapi_memory_view2pointer(access->data, &view );


    kaapi_numaid_t numaid = kaapi_numa_getpage_id( ptr );
    /* how to count remote access if numa information not available ? */
    if (numaid == (kaapi_numaid_t)-1) continue;

#if defined (KAAPI_USE_PERFCOUNTER)
    KAAPI_EVENT_PUSH4(evtkproc, 0, KAAPI_EVT_TASK_ACCESS, task, mp, ptr, numaid );
#endif

    if (kaapi_perf_idset_test_mask(&kaapi_tracelib_param.perfctr_idset, MASK_PERF_READWRITE_ACCESS))
    {
      if ((numaid == local_numaid) || (numaid == (kaapi_numaid_t)-1))
      {
        if (KAAPI_ACCESS_IS_READ(m))
          KAAPI_PERFCTR_INCR(perfkproc, KAAPI_PERF_ID_LOCAL_READ, 1);
        if (KAAPI_ACCESS_IS_WRITE(m))
          KAAPI_PERFCTR_INCR(perfkproc, KAAPI_PERF_ID_LOCAL_WRITE, 1);
      }
      else
      {
        if (KAAPI_ACCESS_IS_READ(m))
          KAAPI_PERFCTR_INCR(perfkproc, KAAPI_PERF_ID_REMOTE_READ, 1);
        if (KAAPI_ACCESS_IS_WRITE(m))
          KAAPI_PERFCTR_INCR(perfkproc, KAAPI_PERF_ID_REMOTE_WRITE, 1);
      }
    }
  }
}
#endif


/*
*/
static inline //__attribute__((always_inline))
kaapi_frame_t* _kaapi_stack_save_and_push_frame(
    kaapi_stack_t* stack,
    kaapi_frame_t* frame,
    void*          sp_data
)
{
  kaapi_assert_debug( kaapi_self_thread() == kaapi_stack2thread(stack) );
  kaapi_frame_t* curr_frame = stack->unlink;

  /* save the stack.thread into its current_frame */
  curr_frame->save_pc             = stack->pc;
  curr_frame->save_thread.sp      = stack->thread.sp;
  curr_frame->save_thread.sp_data = sp_data;

#if LOOG
printf("%i::[PUSH F:%p] pc: %p, sp: %p\n",kaapi_self_kid(), curr_frame, stack->pc, stack->thread.sp);
#endif

#if 0//defined(KAAPI_DEBUG)
  char* startbloc = _kaapi_start_blocaddr(stack->thread.sp,char*);
  char* endbloc = _kaapi_end_blocaddr(stack->thread.sp,char*);
  char* endcurrbloc = _kaapi_end_blocaddr(stack->bloc_task,char*);
  kaapi_assert_debug(
    startbloc == (char*)stack->bloc_task );
  kaapi_task_t* last = _kaapi_end_blocaddr(&stack->bloc_task->data,kaapi_task_t*);
  kaapi_task_t* first= _kaapi_start_blocaddr(&stack->bloc_task->data,kaapi_task_t*);
  kaapi_assert_debug(
    stack->thread.sp < last );
  kaapi_assert_debug(
    stack->thread.sp >  first);
  kaapi_assert_debug(
    _kaapi_start_blocaddr(stack->thread.sp_data,char*) == (char*)stack->bloc_data );
#endif

  /* init the new current frame: next== 0 means save_XX undefined values */
  _kaapi_frame_init( frame );

  /* update next pc to the new empty current frame */
  stack->pc                       = stack->thread.sp;

  /* update link */
  curr_frame->next                = frame;
  stack->unlink                   = frame;
  kaapi_writemem_barrier();
  ++stack->popfp;
  return curr_frame;
}


/*
*/
kaapi_frame_t* kaapi_stack_push_frame( kaapi_stack_t* stack )
{
  kaapi_assert_debug( kaapi_self_thread() == kaapi_stack2thread(stack) );
  kaapi_frame_t* frame;

  /* save stack pointer to restor on pop */
  char* sp_data            = stack->thread.sp_data;
  if (_kaapi_has_enough_dataspace(&stack->thread, sizeof(kaapi_frame_t)) )
  {
    frame = (kaapi_frame_t*)stack->thread.sp_data;
    stack->thread.sp_data += sizeof(kaapi_frame_t);
  }
  else
    frame = (kaapi_frame_t*)kaapi_thread_slow_push_data(&stack->thread, sizeof(kaapi_frame_t));

  kaapi_frame_t* retval = _kaapi_stack_save_and_push_frame( stack, frame, sp_data);
  return retval;
}


/* 
*/
static 
void _kaapi_stack_pop_and_restore_frame(
    kaapi_stack_t* stack,
    kaapi_frame_t* unlink
)
{
#if defined(KAAPI_DEBUG)
  kaapi_thread_t* th = kaapi_self_thread();
  kaapi_assert_debug( th == kaapi_stack2thread(stack) );
#endif
  kaapi_assert_debug( unlink !=0 ); /* else more pop than push */

  /* restore fields */
  stack->thread.sp_data = unlink->save_thread.sp_data;
  stack->thread.sp      = unlink->save_thread.sp;
  stack->pc             = unlink->save_pc;

#if LOOG
printf("%i::[POP F:%p] pc: %p, sp: %p\n",kaapi_self_kid(), unlink, stack->pc, stack->thread.sp);
#endif

  /* unlink */
  unlink->next = 0;

  /* synchronize with thieves: no more thief could access to ????
   */
  unlink->flag |= KAAPI_FRAME_FLAG_NOTSTEAL;
  unlink->flag &= ~KAAPI_FRAME_FLAG_DFG_OK;
  long popfp = --stack->popfp;
  //kaapi_writemem_barrier();
  kaapi_mem_barrier();
#if defined(KAAPI_USE_PERFCOUNTER)
  if (stack->thieffp >= popfp)
#endif
  {
    while (stack->thieffp >= popfp)
      kaapi_slowdown_cpu();
#if defined (KAAPI_USE_PERFCOUNTER)
    KAAPI_PERFCTR_INCR( kaapi_stack2perfproc(stack), KAAPI_PERF_ID_CONFLICTPOP, 1);
#endif
  }

  /* suppress all blocs from next bloc that contains stack->thread.sp */
  kaapi_processor_t* kproc = kaapi_stack2kproc(stack);
  kaapi_stack_bloc_t* bloc = _kaapi_start_blocaddr(stack->thread.sp-1, kaapi_stack_bloc_t*);
  if (bloc != stack->bloc_task)
  {
    kaapi_stack_bloc_t* blocn = bloc->next;
    bloc->next = 0;
    stack->bloc_task = bloc;
    while (blocn !=0)
    {
      kaapi_stack_bloc_t* next = blocn->next;
      kaapi_processor_dealloc_stackbloc( kproc, blocn );
      blocn = next;
    }
  }

  /* suppress all blocs from next bloc that contains stack->thread.sp_data */
  bloc = _kaapi_start_blocaddr(stack->thread.sp_data, kaapi_stack_bloc_t*);
  if (bloc != stack->bloc_data)
  {
    kaapi_stack_bloc_t* blocn = bloc->next;
    bloc->next = 0;
    stack->bloc_data = bloc;
    while (blocn !=0)
    {
      kaapi_stack_bloc_t* next = blocn->next;
      kaapi_processor_dealloc_stackbloc( kproc, blocn );
      blocn = next;
    }
  }

#if 0 //defined(KAAPI_DEBUG)
  char* startbloc = _kaapi_start_blocaddr(stack->thread.sp,char*);
  kaapi_assert_debug(
    startbloc == (char*)stack->bloc_task );
  kaapi_task_t* last = _kaapi_end_blocaddr(&stack->bloc_task->data,kaapi_task_t*);
  kaapi_task_t* first= _kaapi_start_blocaddr(&stack->bloc_task->data,kaapi_task_t*);
  kaapi_assert_debug(
    stack->thread.sp < last );
  kaapi_assert_debug(
    stack->thread.sp >  first);
  kaapi_assert_debug(
    _kaapi_start_blocaddr(stack->thread.sp_data,char*) == (char*)stack->bloc_data );
#endif

  stack->unlink    = unlink;
  if (unlink->rdlist !=0)
  {
    if ((unlink->flag & KAAPI_FRAME_FLAG_STACKALLOC) ==0)
      free(unlink->rdlist);
    unlink->rdlist = 0;
  }
}


/*
*/
void kaapi_stack_pop_frame( kaapi_stack_t* stack, kaapi_frame_t* frame )
{
  _kaapi_stack_pop_and_restore_frame(stack, frame);
}



/* To wakup thread when executing a stolen task (slowexec body)
*/
static int _kaapi_condition_task_isready(void* thearg)
{
  kaapi_task_t* task = (kaapi_task_t*)thearg;
  kaapi_task_body_t body = task->state.steal;
  if ((body == kaapi_nop_body) || (body == kaapi_aftersteal_body))
    return 1;
  return 0;
}


/* Execute when the owner execute a stolen task
*/
void kaapi_slowexec_body( 
  kaapi_task_t*   task,
  kaapi_thread_t* th
)
{
  kaapi_processor_t* kproc = kaapi_thread2kproc(th);
  kaapi_sched_suspend(kproc, _kaapi_condition_task_isready, task);
  kaapi_assert_debug((task->state.steal == kaapi_nop_body) || (task->state.steal == kaapi_aftersteal_body));
  if (task->state.steal == kaapi_aftersteal_body)
    kaapi_aftersteal_body(task, th);
}



/* Precond: thread->pc != thread->sp (not empty)
   Post cond: all tasks between [pc,sp) are completed.
*/
static int __kaapi_sched_sync(
  kaapi_stack_t*  stack,
  kaapi_frame_t*  unlink
)
{
  kaapi_frame_t     frame;
  kaapi_task_body_t body;
  uint32_t          cnt_task = 0;

  unlink->flag &= ~KAAPI_FRAME_FLAG_NOTSTEAL;
  kaapi_task_t*  pc = stack->pc;
  kaapi_task_t*  sp = stack->thread.sp;

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_perf_counter_t perfctr0[kaapi_tracelib_param.task_event_count];
  kaapi_perf_counter_t perfctr[kaapi_tracelib_param.task_event_count];
#endif

#if LOOG
kaapi_task_t* save_pc = pc;
#endif

  kaapi_stack_bloc_t* bloc;
  kaapi_task_t* sp_bloc;
  if (pc == _kaapi_start_blocaddr(pc, kaapi_task_t*))
  {
    bloc = _kaapi_start_blocaddr(pc-1, kaapi_stack_bloc_t*);
    bloc = bloc->next;
    pc = kaapi_firstin_stack_bloc( bloc, kaapi_task_t); 
  }
  else 
    bloc = _kaapi_start_blocaddr(pc, kaapi_stack_bloc_t*);

  sp_bloc = (stack->bloc_task == bloc ? sp : _kaapi_end_blocaddr(pc,kaapi_task_t*));

#if LOOG
kaapi_task_t* save_pc2 = pc;
kaapi_task_t* save_sp_bloc = sp_bloc;
printf("%i::[spbl] @spbloc: %p, @pc: %p\n",kaapi_self_kid(), sp_bloc, pc);
#endif

  kaapi_assert_debug( stack->bloc_task == _kaapi_start_blocaddr(stack->thread.sp-1,kaapi_stack_bloc_t*));
  kaapi_assert_debug( stack->bloc_data == _kaapi_start_blocaddr(stack->thread.sp_data,kaapi_stack_bloc_t*));

  /* -1 because the last entry is lost in the bloc in order to detect bloc change when pushing */
  kaapi_assert_debug(sp_bloc > kaapi_firstin_stack_bloc(bloc,kaapi_task_t) );

  kaapi_assert_debug( stack->unlink == unlink );
  kaapi_assert_debug( stack->unlink->next == 0 );

#if defined (KAAPI_USE_PERFCOUNTER)
  KAAPI_PERFCTR_INCR(kaapi_stack2perfproc(stack), KAAPI_PERF_ID_SYNCINST, 1);
#endif

  /* save thread state and push frame, frame automatic variable */
  _kaapi_stack_save_and_push_frame( stack, &frame, stack->thread.sp_data );
  
redo:
  kaapi_assert_debug( sizeof(kaapi_task_t)*(sp_bloc - pc) <= KAAPI_STACKBLOCSIZE);
  __builtin_prefetch( pc );
  while (pc != sp_bloc)
  {
    kaapi_assert_debug(pc >= kaapi_firstin_stack_bloc(bloc,kaapi_task_t) );
    kaapi_assert_debug(pc < _kaapi_end_blocaddr(pc,kaapi_task_t*) );
    __builtin_prefetch( pc+1 );
    body = 0;
    KAAPI_ATOMIC_EXCHANGE(body, pc->state.steal );

    kaapi_assert_debug( body != 0 );
    __builtin_prefetch( pc->arg );

#if defined(KAAPI_USE_PERFCOUNTER)
    kaapi_ressource_t* rsrc = kaapi_stack2rsrc(stack);
    int fmtid = 0;
    kaapi_named_perfctr_t* namedperf = _kaapi_get_named_perfctr(pc, &fmtid );
    kaapi_tracelib_thread_switchstate(
        rsrc->perfkproc, rsrc->evtkproc, KAAPI_PERF_USR_STATE
    );
    kaapi_tracelib_task_begin( rsrc->perfkproc, rsrc->evtkproc,
        0,
        0,
        perfctr0,
        pc,
        fmtid,
        0
    );
#endif

    body(pc, kaapi_stack2thread(stack));

#if defined(KAAPI_USE_PERFCOUNTER)
    pc->Tinf = (uint32_t) kaapi_tracelib_task_end(
        rsrc->perfkproc,
        rsrc->evtkproc,
        rsrc->kid,
        namedperf,
        0,
        perfctr0,
        perfctr,
        pc, pc->Tinf
    );
    _kaapi_count_access(rsrc->perfkproc, rsrc->evtkproc,rsrc->numaid, pc);
    kaapi_tracelib_thread_switchstate(
        rsrc->perfkproc, rsrc->evtkproc, KAAPI_PERF_SYS_STATE
    );
#endif

    if (__builtin_expect(stack->pc != stack->thread.sp, 0))
    {
      if (frame.rdlist)
        __kaapi_sched_sync_rd_list( stack, &frame );
      else
        __kaapi_sched_sync( stack, &frame );
      kaapi_assert_debug(stack->pc == stack->thread.sp);
    }

    ++pc;
    ++cnt_task;
    /* next line may be important to let thief go directly on the next running task
       this is particulary important for frame with a lot of task
    */
    unlink->save_pc = pc;
  }

  unlink->cnt_task -= cnt_task;
  /* here, may be some tasks stored on the next bloc */
  if (pc != sp)
  {
    kaapi_stack_bloc_t* nextbloc = bloc->next;
    pc = kaapi_firstin_stack_bloc(nextbloc,kaapi_task_t);
    bloc = nextbloc;
    sp_bloc = _kaapi_start_blocaddr(sp-1, kaapi_stack_bloc_t*) == bloc ?
                    sp :
                    _kaapi_end_blocaddr(pc,kaapi_task_t*);
#if LOOG
save_pc = pc;
printf("%i::[spbl] @spbloc: %p, @pc: %p\n",kaapi_self_kid(), sp_bloc);
#endif

    kaapi_assert_debug( pc != sp );
    kaapi_assert_debug(sp_bloc <= _kaapi_end_blocaddr(bloc,kaapi_task_t*) );
    kaapi_assert_debug(sp_bloc > kaapi_firstin_stack_bloc(bloc,kaapi_task_t) );
    goto redo;
  }

  /* pop the frame and return */
  _kaapi_stack_pop_and_restore_frame( stack, unlink );
  kaapi_assert_debug(stack->pc == stack->thread.sp);
  kaapi_assert_debug( stack->bloc_task == _kaapi_start_blocaddr(stack->thread.sp-1, kaapi_stack_bloc_t*));
  kaapi_assert_debug( stack->bloc_data == _kaapi_start_blocaddr(stack->thread.sp_data, kaapi_stack_bloc_t*));

  return 0;
}


/* */
struct retrydata_t {
  int count_retry;
  int count;
  int count_numa;
  int delay;
};


/* Execute task using a ready list of tasks.
   Assumption : all the tasks of the frame have processed to compute successors,
   i.e. the list of tasks accessible through readylist == the list of tasks in the stack.
 */
static int __kaapi_sched_sync_rd_list(
  kaapi_stack_t*  stack,
  kaapi_frame_t*  unlink
)
{
  kaapi_task_t*  pc;
  kaapi_task_body_t body;
  kaapi_frame_t  frame;
  kaapi_frame_t* curr_frame = unlink;
  kaapi_frame_wrdlist_t* frame_rd = curr_frame->rdlist;
  kaapi_frame_wrdlist_t* frame_master = frame_rd->master == 0 ? frame_rd : frame_rd->master;
  kaapi_processor_t* kproc = kaapi_stack2kproc(stack);

  uint64_t local_frame_task = 0;
  int register_tomaster_flag = 0;
  struct retrydata_t retrydata;

  kaapi_ressource_t* rsrc = &kproc->rsrc;
  KAAPI_PERFCTR_IF(rsrc->perfkproc, KAAPI_PERF_ID_RDLISTEXEC,
    kaapi_perf_counter_t    t0 =0,
    t0 = kaapi_get_elapsedns();
  );

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_perf_counter_t perfctr0[kaapi_tracelib_param.task_event_count];
  kaapi_perf_counter_t perfctr[kaapi_tracelib_param.task_event_count];
#endif

#if 1
#define KAAPI_STEAL_FAILED_INST( arg )\
   { ++(arg)->count;  \
     kaapi_slowdown_cpu(); \
     kaapi_slowdown_cpu(); \
   }
#else
#define KAAPI_STEAL_FAILED_INST( arg )\
   { ++(arg)->count; \
     if ((arg)->count > 16) \
     { \
       (arg)->delay *= 2; \
       struct timespec ts = { 0, (arg)->delay }; \
       nanosleep( &ts, 0 ); \
       if ((arg)->delay > 128) \
       { (arg)->delay = 128; (arg->count=0; }\
     } \
     else kaapi_slowdown_cpu(); \
   }
#endif

//    {(arg)->count_retry = 8*kaapi_get_concurrency(); 
#define KAAPI_STEAL_SUCCESS_INST( arg )\
   {(arg)->count_retry = 16;\
    (arg)->count = 0;\
    (arg)->count_numa = 0;\
    (arg)->delay = 1; }\

  kaapi_assert_debug( unlink->flag & KAAPI_FRAME_FLAG_NOTSTEAL);

#if defined(KAAPI_USE_PERFCOUNTER)
  /* here dfg is initialized, dump graph if required */
  if ((kaapi_default_param.dump_graph) && (kproc->rsrc.kid ==0))
  {
    static uint32_t counter = 0;
    char filename[128]; 
    if (getenv("USER") !=0)
      sprintf(filename,"/tmp/graph_sync_rdlist.%s.%i.dot", getenv("USER"), counter);
    else
      sprintf(filename,"/tmp/graph_sync_rdlist.%i.dot", counter);
    FILE* filedot = fopen(filename, "w");
    kaapi_frame_print_dot( filedot, stack, unlink );
    fclose(filedot);

    if (getenv("USER") !=0)
      sprintf(filename,"/tmp/graph_sync_rdlist.%s.%i.txt", getenv("USER"), counter );
    else
      sprintf(filename,"/tmp/graph_sync_rdlist.%i.txt", counter);
    filedot = fopen(filename, "w");
    kaapi_stack_print( filedot, stack );
    fclose(filedot);

    ++counter;
  }
#endif


  /* distribute initial ready tasks */
  if (!kaapi_list_empty(&frame_rd->rd))
  {
    KAAPI_PERFCTR_IF(rsrc->perfkproc, KAAPI_PERF_ID_RDLISTINIT,
      kaapi_perf_counter_t    t0 =0,
      t0 = kaapi_get_elapsedns();
    );

    kaapi_default_param.wspush_init(rsrc, frame_rd);

    KAAPI_PERFCTR_IF(rsrc->perfkproc, KAAPI_PERF_ID_RDLISTINIT,
      , /* no declaration */
      kaapi_perf_counter_t t1 = kaapi_get_elapsedns();
      KAAPI_PERFCTR_INCR(rsrc->perfkproc,KAAPI_PERF_ID_RDLISTINIT, t1-t0);
    );
  }
  unlink->flag &= ~KAAPI_FRAME_FLAG_NOTSTEAL;

#if defined (KAAPI_USE_PERFCOUNTER)
  KAAPI_PERFCTR_INCR(rsrc->perfkproc, KAAPI_PERF_ID_SYNCINST, 1);
#endif

  KAAPI_STEAL_SUCCESS_INST(&retrydata);
  
  kaapi_assert_debug( stack->unlink == unlink );
  kaapi_assert_debug( stack->unlink->next == 0 );

  /* save thread state and push newframe, frame automatic variable */
  _kaapi_stack_save_and_push_frame( stack, &frame, stack->thread.sp_data );

redo:
  pc = _kaapi_sched_steal_activated_task(kproc, frame_rd, &retrydata );

  while ( pc != 0 )
  {
    int completed = 0;
    kaapi_assert_debug( pc->u.s.flag & KAAPI_TASK_FLAG_DFGOK);

    /* task is pop from queue, do not need atomic exchange to mark it */
    body = pc->body;
    /* not possible here : except if task was poped several times */
    kaapi_assert_debug(body != kaapi_slowexec_body);

    __builtin_prefetch( pc->arg );
#if defined(KAAPI_USE_OFFLOAD)
    if (kaapi_offload_get_num_devices() >0)
    {
      completed = kaapi_offload_host_pushtask(pc);
#if 0 // Why ??? except if thread becomes GPU worker
{
      kaapi_offload_device_run(
            kaapi_offload_get_host_device(),
            body,
            pc,
            (kaapi_thread_t*)stack
      );
}
      completed = 0;
#endif
    }
    else
    {
#endif
      /* execute task and read performance counters */
#if defined(KAAPI_USE_PERFCOUNTER)
    kaapi_ressource_t* rsrc = kaapi_stack2rsrc(stack);
    int fmtid = 0;
    kaapi_named_perfctr_t* namedperf = _kaapi_get_named_perfctr(pc, &fmtid );
    kaapi_tracelib_thread_switchstate(
        rsrc->perfkproc, rsrc->evtkproc, KAAPI_PERF_USR_STATE
    );
    kaapi_tracelib_task_begin(
        rsrc->perfkproc,
        rsrc->evtkproc,
        0,
        0,
        perfctr0,
        pc,
        fmtid,
        1);
#endif
      body(pc, (kaapi_thread_t*)stack);
      completed = 1;

#if defined(KAAPI_USE_PERFCOUNTER)
      pc->Tinf = (uint32_t)kaapi_tracelib_task_end(
          rsrc->perfkproc, 
          rsrc->evtkproc, 
          rsrc->kid,
          namedperf,
          0,
          perfctr0,
          perfctr,
          pc,
          pc->Tinf);
      _kaapi_count_access(rsrc->perfkproc, rsrc->evtkproc, rsrc->numaid, pc);
      kaapi_tracelib_thread_switchstate(
          rsrc->perfkproc, rsrc->evtkproc, KAAPI_PERF_SYS_STATE
      );
#endif
#if defined(KAAPI_USE_OFFLOAD)
    }
#endif

    /* If task creates new task : always execute them first before activating sucessor of pc
    */
    if (__builtin_expect(stack->pc != stack->thread.sp, 0))
    {
      if (frame.rdlist)
        __kaapi_sched_sync_rd_list( stack, &frame );
      else
        __kaapi_sched_sync( stack, &frame );
      kaapi_assert_debug(stack->pc == stack->thread.sp);
    }
    /* If steal task from frame != frame_master, then signal original master frame
       else cache data count locally.
       If offload is enable, then the kaapi_offload_device_run will increase the cnt_exec counter
       of the frame and activate successor tasks.
    */
    if (completed)
    {
      if (pc->state.frame != frame_master)
      {
        kaapi_writemem_barrier();
        KAAPI_ATOMIC_INCR(&pc->state.frame->cnt_exec);
      }
      else
        ++local_frame_task;
      pc = _kaapi_sched_activate_pop_task( rsrc, frame_rd, pc );
    }
    else
    {
      /* for offload  code, activated already done */
      if (!kaapi_place_empty(rsrc->ownplace))
        pc = kaapi_place_pop(rsrc, rsrc->ownplace);
      else
        pc = 0;
    }
  }

  /* end of the principal loop: test completion of all tasks */
  kaapi_writemem_barrier();
  if (frame_rd->master ==0)
  {
    if (local_frame_task)
    {
      kaapi_writemem_barrier();
      KAAPI_ATOMIC_ADD(&frame_rd->cnt_exec, local_frame_task);
      local_frame_task = 0;
      KAAPI_STEAL_SUCCESS_INST(&retrydata);
    }
    else 
    {
      //SEEMS to not be good 
      KAAPI_STEAL_FAILED_INST(&retrydata);
#if defined(KAAPI_USE_OFFLOAD)
      if (retrydata.count % 64 ==0)
        kaapi_offload_poll_devices();
#endif
    }

    if (KAAPI_ATOMIC_READ(&frame_rd->cnt_exec) != frame_rd->cnt_task)
      goto redo;

    unlink->cnt_task -= frame_rd->cnt_task;

    /* here: cnt_exec == cnt_task, wait worker to finish */
    while (KAAPI_ATOMIC_READ(&frame_rd->cnt_worker) !=0)
    {
      kaapi_slowdown_cpu();
    }
  }
  else
  {
    /* no need to synchronize because worker steal at least one task, thus master always has
       its frame valid.
    */
    if (!register_tomaster_flag)
    {
       register_tomaster_flag = 1;
       KAAPI_ATOMIC_INCR(&frame_master->cnt_worker);
    }

    if (local_frame_task ==0)
    {
      if (--retrydata.count_retry !=0)
      {
        KAAPI_STEAL_FAILED_INST(&retrydata);
        goto redo;
      }
      if (KAAPI_ATOMIC_READ(&frame_master->cnt_exec) != frame_master->cnt_task)
      {
        KAAPI_STEAL_SUCCESS_INST(&retrydata);
        goto redo;
      }
    }
    else 
    { /* signal master frame and returns if completion */
      kaapi_writemem_barrier();
      KAAPI_ATOMIC_ADD(&frame_master->cnt_exec, local_frame_task );
      local_frame_task = 0;
      KAAPI_STEAL_SUCCESS_INST(&retrydata);
      goto redo;
    }
    /* do not continue to steal directly from frame, update master worker counter */
    KAAPI_ATOMIC_DECR(&frame_master->cnt_worker);
  }

  curr_frame->save_pc = curr_frame->save_thread.sp;


  /* pop the frame and return */
  _kaapi_stack_pop_and_restore_frame( stack, unlink );

  KAAPI_PERFCTR_IF(rsrc->perfkproc, KAAPI_PERF_ID_RDLISTEXEC,
    , /* no declaration */
    kaapi_perf_counter_t t1 = kaapi_get_elapsedns();
    KAAPI_PERFCTR_INCR(rsrc->perfkproc, KAAPI_PERF_ID_RDLISTEXEC, t1-t0);
    //printf("[kaapi]: syncrdlist %e\n", (t1-t0)*1e-9);
  );

  return 0;
}


/* Threshold to switch from stack of tasks to ready list management
   Should be computed by stealer to avoid the necessity to use threshold.
*/
#define KAAPI_TASK_THRESHOLD 8
//#define KAAPI_TASK_THRESHOLD 16384

/* Public entry point
*/
int kaapi_sched_sync( kaapi_thread_t* thread )
{
  /* return if empty task to perform */
  kaapi_stack_t* stack = kaapi_thread2stack(thread);
  if (stack->pc == thread->sp)
    return 0;

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_ressource_t* rsrc = kaapi_thread2rsrc(thread);
  kaapi_assert_debug( rsrc != 0);
  int state = KAAPI_GET_THREAD_STATE(rsrc->perfkproc);
  kaapi_tracelib_thread_switchstate(rsrc->perfkproc, rsrc->evtkproc, KAAPI_PERF_SYS_STATE );
#endif

  kaapi_frame_t* frame = stack->unlink;
  if ((frame->flag & KAAPI_FRAME_FLAG_DFG_OK) ==0)
  {
    int tcount = (int)kaapi_frame_count_task(stack, frame);
    if (tcount <= KAAPI_TASK_THRESHOLD)
    {
      __kaapi_sched_sync(stack, stack->unlink);
      kaapi_assert_debug( stack->pc == thread->sp );
    }
    else
    {
      kaapi_assert_debug( frame->rdlist == 0 );
      kaapi_frame_computereadylist(stack,frame);
      if (frame->rdlist)
        __kaapi_sched_sync_rd_list(stack, frame);
      else
        __kaapi_sched_sync(stack, stack->unlink);

    }
    kaapi_assert_debug( frame->cnt_task == 0 );
  }
  else if ((frame->rdlist->starttask !=0) || !kaapi_list_empty(&frame->rdlist->rd))
    __kaapi_sched_sync_rd_list(stack, frame);

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_assert_debug( rsrc != 0);
  kaapi_assert_debug( KAAPI_GET_THREAD_STATE(rsrc->perfkproc) == KAAPI_PERF_SYS_STATE );
  // TODO: make it conditional depending of the event mask
  kaapi_tracelib_thread_switchstate(rsrc->perfkproc, rsrc->evtkproc, state );
#endif
  return 0;
}

/* Not Yet Public entry point: sync with forced computation of dfg
*/
int kaapi_sched_sync_dfg( kaapi_thread_t* thread )
{
  /* return if empty task to perform */
  kaapi_stack_t* stack = kaapi_thread2stack(thread);
  if (stack->pc == thread->sp)
    return 0;

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_ressource_t* rsrc = kaapi_thread2rsrc(thread);
  kaapi_assert_debug( rsrc != 0);
  int state = KAAPI_GET_THREAD_STATE(rsrc->perfkproc);
  kaapi_tracelib_thread_switchstate(rsrc->perfkproc, rsrc->evtkproc, KAAPI_PERF_SYS_STATE );
#endif

  kaapi_frame_t* frame = stack->unlink;
  if (frame->flag & KAAPI_FRAME_FLAG_DFG_OK)
  {
    if ((frame->rdlist->starttask !=0) || !kaapi_list_empty(&frame->rdlist->rd))
      __kaapi_sched_sync_rd_list(stack, frame);
  }
  else
  {
    kaapi_assert_debug( frame->rdlist == 0 );
#if defined(KAAPI_USE_PERFCOUNTER)
    //printf("[sync] #task pushed: %i\n", frame->cnt_task );
#endif
    kaapi_frame_computereadylist(stack,frame);

    if (frame->rdlist)
      __kaapi_sched_sync_rd_list(stack, frame);
    else
      __kaapi_sched_sync(stack, stack->unlink);

    kaapi_assert_debug( frame->cnt_task == 0 );
  }

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_assert_debug( rsrc != 0);
  kaapi_tracelib_thread_switchstate(rsrc->perfkproc, rsrc->evtkproc, state );
#endif
  return 0;
}


/* Not Yet Public entry point: sync not forced computation of dfg
*/
int kaapi_sched_sync_nodfg( kaapi_thread_t* thread )
{
  /* return if empty task to perform */
  kaapi_stack_t* stack = kaapi_thread2stack(thread);
  if (stack->pc == thread->sp)
    return 0;

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_ressource_t* rsrc = kaapi_thread2rsrc(thread);
  kaapi_assert_debug( rsrc != 0);
  int state = KAAPI_GET_THREAD_STATE(rsrc->perfkproc);
  kaapi_tracelib_thread_switchstate(rsrc->perfkproc, rsrc->evtkproc, KAAPI_PERF_SYS_STATE );
#endif

  kaapi_frame_t* frame = stack->unlink;
  if (frame->flag & KAAPI_FRAME_FLAG_DFG_OK)
  {
    if ((frame->rdlist->starttask !=0) || !kaapi_list_empty(&frame->rdlist->rd))
      __kaapi_sched_sync_rd_list(stack, frame);
  }
  else
  {
    kaapi_assert_debug( frame->rdlist == 0 );
    __kaapi_sched_sync(stack, stack->unlink);
    kaapi_assert_debug( stack->pc == thread->sp );

    kaapi_assert_debug( frame->cnt_task == 0 );
  }

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_assert_debug( rsrc != 0);
  kaapi_tracelib_thread_switchstate(rsrc->perfkproc, rsrc->evtkproc, state );
#endif
  return 0;
}


/*
*/
static kaapi_task_t* _kaapi_sched_steal_activated_task(
    kaapi_processor_t* kproc,
    kaapi_frame_wrdlist_t* frame_rd,
    struct retrydata_t* r
)
{
  kaapi_task_t* task;
  kaapi_ressource_t* rsrc = &kproc->rsrc;

  /* initial task - kind of private queue */
  task = frame_rd->starttask;
  if (task)
  {
    frame_rd->starttask = 0;
    goto label_ok;
  }

#if OLD_IMPL /* responsability to wsselect function */
  kaapi_place_t*ld;

#warning "Using old implementation"
  /* my local queue */
  ld = rsrc->ownplace;
  task = kaapi_place_pop( rsrc, ld  );
  if (task !=0) goto label_ok;
#endif

  /* Except to look in my local place, other selection of place is to the responsability of the
     scheduling slection function (KAAPI_WSSELECT)
  */
#if OLD_IMPL 
#if 1
  /* my local NUMA queue */
  ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid];
  task = kaapi_place_pop( rsrc, ld );
  if (task !=0) goto label_ok;
#endif

  int victim;

#if 1
  /* random node */
  victim = rand_r(&rsrc->seed) % kproc->team->count;
  ld = kproc->team->all_kprocessors[victim]->rsrc.ownplace;
  if (ld == rsrc->ownplace)
    task = kaapi_place_pop( rsrc, ld );
  else
    task = kaapi_place_steal( rsrc, ld );
  if (task !=0) goto label_ok;
#endif

#if 1
  kaapi_team_t* team = kproc->team;
  int count = team->numaplaces.count;
  /* random numa */
  victim = rand_r(&rsrc->seed) % count;
  ld = team->numaplaces.places[victim];
  if (ld == kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid])
    task = kaapi_place_pop( rsrc, ld  );
  else
    task = kaapi_place_steal( rsrc, ld );
  if (task !=0) goto label_ok;
#else
  int shift = rand_r(&rsrc->seed) % kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].count;
  for (victim = 0; victim < kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].count; ++victim)
  {
    ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[ (victim+shift)% kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].count ];
    if (ld == kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid])
      task = kaapi_place_pop( rsrc, ld  );
    else
      task = kaapi_place_steal( rsrc, ld );
    if (task !=0) goto label_ok;
  }
#endif

#else // IF 00

  /* New implementation: select the victim processor using selection function */
  kaapi_victim_t victim;
  int err;

for (int j=0; j<5; ++j)
{
  err = kaapi_default_param.wsselect( kproc, &victim, KAAPI_SELECT_VICTIM );
  if (likely(err ==0) && (victim.ld !=0) && !kaapi_place_empty(victim.ld))
  {
    /* if local place (ownplace or local numa, do pop else do steal */
    if ((victim.ld == rsrc->ownplace) ||
        (victim.ld == kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[rsrc->numaid])
    )
      task = kaapi_place_pop( rsrc, victim.ld  );
    else
      task = kaapi_place_steal( rsrc, victim.ld );
    if (task !=0)
    {
      kaapi_default_param.wsselect( kproc, &victim, KAAPI_STEAL_SUCCESS );
      goto label_ok;
    }
  }
  else 
    kaapi_default_param.wsselect( kproc, &victim, KAAPI_STEAL_FAILED );
}

#endif // IF 00

#if defined(KAAPI_USE_OFFLOAD)
  /* random device */
  kaapi_hws_levelid_t i;
  for (i=KAAPI_HWS_LEVELID_OFFLOAD_HOST; i<KAAPI_HWS_LEVELID_MAX; ++i)
  {
    if (kaapi_default_param.places_set[i].count ==0) continue;
    int victimid = rand_r(&rsrc->seed) % kaapi_default_param.places_set[i].count;
      kaapi_place_t* ld = kaapi_default_param.places_set[i].places[victimid];
    if (ld == rsrc->ownplace)
      task = kaapi_place_pop( rsrc, ld  );
    else
      task = kaapi_place_steal( rsrc, ld );
    if (task !=0) goto label_ok;
  }
#endif
  return 0;

label_ok:
#if defined (KAAPI_USE_PERFCOUNTER)
  KAAPI_PERFCTR_INCR(rsrc->perfkproc, KAAPI_PERF_ID_TASKSTEAL, 1);
#endif
  return task;
}





/* Activate successors of the task.
   If popflag ==1, then return one of the activated tasks by bypassing the queue selection 
   strategy for stealing.
   WARNING: if strict push is set then do not bypass strategy because else it can
   induce wrong local selection (taking locally a task that should be executed on remote node).
*/
static inline kaapi_task_t*  _kaapi_sched_activate_succ(
    kaapi_ressource_t*      rsrc,
    kaapi_task_t*           task,
    int                     popflag,
    kaapi_list_t*           readytasks
)
{
  unsigned int i, count_params;
  const kaapi_format_t* fmt;
  kaapi_access_mode_t   mode;
  kaapi_access_t*       first_access;
  kaapi_access_t*       access;
  int wc;
  kaapi_task_t*         retval = 0;
  uint64_t              max_prio = 0;
  fmt         = task->fmt;
  kaapi_assert_debug( fmt != 0);

  if (kaapi_default_param.strict_push) popflag = 0;
  count_params = kaapi_format_get_count_params( fmt, task->arg );

  for (i=0; i<count_params; ++i)
  {
    mode         = kaapi_format_get_mode_param(fmt, i, task->arg);
    if (mode & KAAPI_ACCESS_MODE_V)
      continue;

    first_access = kaapi_format_get_access_param(fmt, i, task->arg);

    access = first_access->next_out;
    while (access !=0)
    {
      kaapi_task_t* next_task = access->task;
#if defined(KAAPI_USE_PERFCOUNTER)
      KAAPI_EVENT_PUSH2(rsrc->evtkproc, 0, KAAPI_EVT_TASK_SUCC, task, next_task );
      if (next_task->Tinf < task->Tinf)
        next_task->Tinf = task->Tinf;
#endif
      kaapi_assert_debug( KAAPI_ATOMIC_READ(&next_task->wc) > 0);
      wc = KAAPI_ATOMIC_DECR(&next_task->wc);
      kaapi_assert_debug( wc >= 0 );
      if (wc ==0)
      {
        if (popflag)
        {
          if ((next_task->u.s.flag & KAAPI_TASK_FLAG_PRIORITY) || kaapi_default_param.use_priority)
            if (!retval || (next_task->T > max_prio))
              retval = next_task;
        }
        kaapi_list_push_tail(readytasks, next_task);
      }
      access = access->next;
    }
  }

#if 0
  if ( (kaapi_default_param.wspush == &kaapi_sched_select_queue_local)
#if defined(KAAPI_USE_NUMA)
    || (kaapi_default_param.wspush == &kaapi_sched_select_queue_localnuma)
#endif
  )
#endif
#if 1
  if (popflag)
  {
    if (retval !=0)
      kaapi_list_remove(readytasks, retval);
    else if (!kaapi_list_empty(readytasks))
      retval = kaapi_list_pop_head(readytasks);
  }
#endif

  return retval;
}


/*
*/
kaapi_task_t*  kaapi_sched_activate_succ(
    kaapi_ressource_t*      rsrc,
    kaapi_task_t*           task,
    int                     popflag,
    kaapi_list_t*           readytasks
)
{
  return _kaapi_sched_activate_succ(rsrc, task, popflag, readytasks );
}


/*
*/
static kaapi_task_t* _kaapi_sched_activate_pop_task(
    kaapi_ressource_t*     rsrc,
    kaapi_frame_wrdlist_t* frame_rd,
    kaapi_task_t*          task
)
{
  /* activate all the next concurrent tasks with the task
  */
  kaapi_list_t readytasks;
  kaapi_list_init(&readytasks);
  kaapi_task_t* succ = _kaapi_sched_activate_succ(
      rsrc,
      task,
      1,
      &readytasks
  );
  if (!kaapi_list_empty(&readytasks))
    kaapi_default_param.wspush( rsrc, rsrc->ownplace, &readytasks );

#if 1// TG: REALLY NEEDED ?
  if ((succ ==0) && !kaapi_place_empty(rsrc->ownplace))
    succ = kaapi_place_pop(rsrc, rsrc->ownplace);
#endif
  kaapi_list_destroy( &readytasks );
  return succ;
}


/* Base kaapi_thread_save on top ofkaapi_stack_push_frame.
   Stored data are currently not used except reserved4 and are set only for debugging.
*/
int kaapi_thread_save(kaapi_thread_t* thread, kaapi_thread_register_t* regs)
{
  kaapi_stack_t* stack = kaapi_thread2stack(thread);
  regs->pc = stack->pc;
  regs->sp = thread->sp;
  regs->sp_data = thread->sp_data;
  regs->reserved1 = stack->unlink;
  regs->reserved2 = stack->bloc_task;
  regs->reserved3 = stack->bloc_data;
  regs->reserved4 = kaapi_stack_push_frame(stack);
  return 0;
}


/* 
*/
int kaapi_thread_restore(kaapi_thread_t* thread, const kaapi_thread_register_t* regs)
{
  kaapi_stack_t* stack = kaapi_thread2stack(thread);
  kaapi_stack_pop_frame( stack, (kaapi_frame_t*)regs->reserved4 );
  kaapi_assert_debug( thread->sp == regs->sp );
  kaapi_assert_debug( stack->pc == regs->pc );
  kaapi_assert_debug( thread->sp_data == regs->sp_data );
  kaapi_assert_debug( stack->unlink == regs->reserved1 );
  kaapi_assert_debug( stack->bloc_task == regs->reserved2 );
  kaapi_assert_debug( stack->bloc_data == regs->reserved3 );
  return 0;
}


static void _kaapi_resize_stack( kaapi_stack_t* stack, size_t data_size, size_t task_size )
{
  kaapi_stack_bloc_t** bloc= 0;
  if( data_size ==0)
    bloc = &stack->bloc_task;
  else if (task_size ==0)
    bloc = &stack->bloc_data;
  else
    kaapi_abort(__LINE__, __FILE__, "Invalid case");

  if ((data_size+task_size) >= kaapi_default_param.stackblocsize)
  {
    fprintf(stderr,"*** stack bloc overflow: data too big for one allocation,"
         " please extend your KAAPI_STACKBLOCSIZE and recompile the library."
         " KAAPI_STACKBLOCSIZE is actually set to %li bytes / %.2f MBytes. Data required is %li (bytes)",
      kaapi_default_param.stackblocsize, kaapi_default_param.stackblocsize/1024.0/1024.0, data_size+task_size
    );
    kaapi_abort(__LINE__,__FILE__, "Invalid value");
  }

#if 0
  if (data_size !=0)
    printf("[dbg] resize data stack\n");
  else 
    printf("[dbg] resize task stack\n");
#endif

  kaapi_processor_t* kproc = kaapi_stack2context(stack)->proc;
  kaapi_stack_bloc_t* newbloc = kaapi_processor_alloc_stackbloc(kproc);
  kaapi_assert_debug(newbloc != 0);
  kaapi_assert_debug(newbloc->next == 0);

  if (newbloc ==0)
    kaapi_abort(__LINE__,__FILE__, "No enough memory");

  /* link new bloc */
  newbloc->save_ptr = (data_size == 0 ? (void*)stack->thread.sp : (void*)stack->thread.sp_data);

  if (data_size ==0)
    stack->thread.sp = kaapi_firstin_stack_bloc( newbloc, kaapi_task_t);
  else
    stack->thread.sp_data = kaapi_firstin_stack_bloc( newbloc, char);

  (*bloc)->next = newbloc;
  (*bloc) = newbloc;
}


/*
*/
void* kaapi_thread_slow_push_data( kaapi_thread_t* thread, unsigned long requested_size )
{
  kaapi_stack_t* stack = kaapi_thread2stack(thread);
  _kaapi_resize_stack( stack, requested_size, 0 );
  char* retval = stack->thread.sp_data;
  stack->thread.sp_data += requested_size;
  return retval;
}

/*
*/
kaapi_task_t* kaapi_thread_slow_pushtask(kaapi_thread_t* thread)
{
  kaapi_stack_t* stack = kaapi_thread2stack(thread);
  _kaapi_resize_stack( stack, 0, 1 );
  kaapi_task_t* retval = stack->thread.sp;
  return retval;
}

