/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"

/** \ingroup WS
    \retval 0 if no context could be wakeup
    \retval else a context to wakeup
    \TODO faire specs ici
*/
static kaapi_context_t* kaapi_sched_wakeup (
  kaapi_processor_t* kproc
);



/*
*/
int kaapi_sched_advance ( kaapi_processor_t* kproc )
{
#if defined(KAAPI_USE_NETWORK)
    kaapi_network_poll();
#endif
  //pthread_yield();
  return 0;
}



/* Suspend the current running kproc until the predicat fcondition(arg_fcondition) is true (!=0).
   The implementation relies on pushing in LIFO link list kaapi_queue_t a task that represent
   the suspension point. This task has fields:
    .body  = fcondition
    .steal = arg_fcondition
    .arg   = the current running thread
*/
int kaapi_sched_suspend ( 
    kaapi_processor_t* kproc, 
    int (*fcondition)(void* ), 
    void* arg_fcondition
)
{
  int err = 0;
  kaapi_request_status_t ws_status;
  kaapi_context_t* context;
  kaapi_context_t* context_input = 0;
  kaapi_context_t* tmp;
  kaapi_task_t task_suspended;
  kaapi_team_t* team;

  kaapi_assert_debug( kproc !=0 );
  kaapi_assert_debug( kproc->context !=0 );
  kaapi_assert_debug( kproc == kaapi_self_processor() );

  team = kproc->team;


  if ((fcondition !=0) && fcondition(arg_fcondition))
    return 0;

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_assert_debug( KAAPI_GET_THREAD_STATE(kproc->rsrc.perfkproc) == KAAPI_PERF_USR_STATE );
  kaapi_tracelib_thread_switchstate(
      kproc->rsrc.perfkproc,
      kproc->rsrc.evtkproc,
      KAAPI_PERF_SYS_STATE );
  KAAPI_EVENT_PUSH0( kproc->rsrc.evtkproc, 0, KAAPI_EVT_SCHED_IDLE_BEG );
#endif

  /* here is the reason of suspension */
  context_input = kproc->context;
  if (context_input !=0)
  {
    kaapi_assert_debug( kproc == context_input->proc);
    /* put context in the list of suspended contexts: no critical section with respect of thieves */
    kaapi_setcontext(kproc, 0);
    task_suspended.body  = (kaapi_task_body_t)fcondition;
    task_suspended.state.arg = (void*)arg_fcondition;
    task_suspended.arg   = context_input;
#if defined(KAAPI_LINKED_LIST)
    task_suspended.next  = 0;
    task_suspended.prev  = 0;
#endif
    kaapi_atomic_lock(&kproc->lock_lsuspend);
    kaapi_list_push_head(&kproc->lsuspend, &task_suspended);
    kaapi_atomic_unlock(&kproc->lock_lsuspend);

    /* always allocate a thread before stealing */
    context = kaapi_context_alloc(kproc);
    kaapi_assert_debug( context != 0 );
    kaapi_setcontext(kproc, context);
  }

  do
  {
    kaapi_sched_advance(kproc);

    /* wakeup a context: either a ready thread (first) or a suspended thread.
       Precise the suspended thread 'context_input' in order to wakeup it first and task_condition.
    */
    if (!kaapi_list_empty(&kproc->lsuspend))
    {
      context = kaapi_sched_wakeup(kproc);
      if (context !=0)
      {
        tmp = kproc->context;
        kaapi_setcontext( kproc, context );
        kaapi_context_destroy( kproc, tmp );

        if ((fcondition !=0) && (context == context_input))
        {
          err = 0;
          break;
        }
        
        goto redo_execute;
      }
    }

    /* steal request */
    if (kaapi_default_param.emitsteal !=0)
    {
      ws_status = kaapi_default_param.emitsteal(kproc);
      if (ws_status != KAAPI_REQUEST_S_OK)
      {
        if ((ws_status == KAAPI_REQUEST_S_STOP) || team->isterm)
        {
          err = EINTR;
          break;
        }
        continue;
      }
    }
    else
      continue;

redo_execute:
#if defined (KAAPI_USE_PERFCOUNTER)
    KAAPI_EVENT_PUSH0( kproc->rsrc.evtkproc, 0, KAAPI_EVT_SCHED_IDLE_END );
#endif
    context = kproc->context;
    err = kaapi_sched_sync( kaapi_context2thread(context) );
    kaapi_assert( err ==0);
#if defined (KAAPI_USE_PERFCOUNTER)
    KAAPI_EVENT_PUSH0( kproc->rsrc.evtkproc, 0, KAAPI_EVT_SCHED_IDLE_BEG );
#endif

    /* synchronize on kproc in order to avoid stealer viewing the stack that will be reset */
    kproc->context = 0;
    kaapi_stack_reset(&context->stack);
    kaapi_writemem_barrier();
    kproc->context = context;
  } while (1);

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_tracelib_thread_switchstate(
      kproc->rsrc.perfkproc,
      kproc->rsrc.evtkproc,
      KAAPI_PERF_USR_STATE );
  KAAPI_EVENT_PUSH0(kproc->rsrc.evtkproc, 0, KAAPI_EVT_SCHED_IDLE_END );
  kaapi_assert_debug( KAAPI_GET_THREAD_STATE(kproc->rsrc.perfkproc) == KAAPI_PERF_USR_STATE );
#endif
  return err;
}


/*
*/
int kaapi_sched_idle (
    kaapi_processor_t* kproc,
    int (*fcondition)(void* ),
    void* arg_fcondition
)
{
  int err = 0;
#if defined(KAAPI_USE_PERFCOUNTER)
  int save_state;
  save_state = KAAPI_GET_THREAD_STATE(kproc->rsrc.perfkproc);
  kaapi_tracelib_thread_switchstate(
      kproc->rsrc.perfkproc,
      kproc->rsrc.evtkproc,
      KAAPI_PERF_SYS_STATE );
  KAAPI_EVENT_PUSH0(kproc->rsrc.evtkproc, 0, KAAPI_EVT_SCHED_IDLE_BEG );
  kaapi_assert_debug(
      (kproc->rsrc.perfkproc ==0) ||
      (KAAPI_GET_THREAD_STATE(kproc->rsrc.perfkproc) == KAAPI_PERF_SYS_STATE) );
#endif

  kaapi_request_status_t  ws_status;
  kaapi_context_t* context;
  kaapi_context_t* tmp;
  kaapi_team_t* team;

  kaapi_assert_debug( kproc !=0 );
  kaapi_assert_debug( kproc->context !=0 );
  kaapi_assert_debug( kproc == kaapi_self_processor() );

  team = kproc->team;

  do {
    kaapi_sched_advance(kproc);

    /* return if condition is true */
    if ((fcondition !=0) && (fcondition(arg_fcondition) !=0))
      break;

    /* wakeup a context: either a ready thread (first) or a suspended thread.
       Precise the suspended thread 'context_input' in order to wakeup it first and task_condition.
    */
    if (!kaapi_list_empty(&kproc->lsuspend))
    {
      context = kaapi_sched_wakeup(kproc);
      if (context !=0)
      {
        tmp = kproc->context;
        kaapi_setcontext( kproc, context );
        kaapi_context_destroy( kproc, tmp );
        goto redo_execute;
      }
    }
    
    /* steal request */
    if (kaapi_default_param.emitsteal !=0)
    {
      ws_status = kaapi_default_param.emitsteal(kproc);
      if (ws_status != KAAPI_REQUEST_S_OK)
      {
        if ((ws_status == KAAPI_REQUEST_S_STOP) || team->isterm)
        {
          err = EINTR;
          break;
        }
        continue;
      }
    }
    else 
      continue;

redo_execute:
#if defined (KAAPI_USE_PERFCOUNTER)
    KAAPI_EVENT_PUSH0( kproc->rsrc.evtkproc, 0, KAAPI_EVT_SCHED_IDLE_END );
#endif
    context = kproc->context;
    err = kaapi_sched_sync( kaapi_context2thread(context) );
    kaapi_assert( err ==0);

#if defined (KAAPI_USE_PERFCOUNTER)
    KAAPI_EVENT_PUSH0( kproc->rsrc.evtkproc, 0, KAAPI_EVT_SCHED_IDLE_BEG );
#endif

    /* synchronize on kproc in order to avoid stealer viewing the stack that will be reset */
    kproc->context = 0;

    kaapi_stack_reset(&context->stack);
    kaapi_writemem_barrier();
    kproc->context = context;
  } while (1);

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_assert_debug(
      (kproc->rsrc.perfkproc ==0) ||
      (KAAPI_GET_THREAD_STATE(kproc->rsrc.perfkproc) == KAAPI_PERF_SYS_STATE) );
  KAAPI_EVENT_PUSH0( kproc->rsrc.evtkproc, 0, KAAPI_EVT_SCHED_IDLE_END );
  kaapi_tracelib_thread_switchstate(
    kproc->rsrc.perfkproc,
    kproc->rsrc.evtkproc,
    save_state
  );
  kaapi_assert_debug(
      (kproc->rsrc.perfkproc ==0) ||
      (KAAPI_GET_THREAD_STATE(kproc->rsrc.perfkproc) == save_state) );
#endif
  return err;
}



/* See comment before definition of kaapi_sched_suspend
*/
static kaapi_context_t* kaapi_sched_wakeup (
    kaapi_processor_t*      kproc
)
{
  kaapi_task_t* stask;
    
  /* Note on this line: in the current implementation, only the owner of the suspend queue
     is able to call kaapi_sched_wakeup.
  */
  kaapi_assert_debug( kproc->rsrc.kid == kaapi_self_kid() );

  /* else...
     - here only do garbage, because in the current version the 
     thread that becomes ready is pushed into an other queue.
     Thus, all cell which are marked KAAPI_WSQUEUECELL_OUTLIST or
     KAAPI_WSQUEUECELL_STEALLIST are garbaged.
  */
  stask = kaapi_list_head(&kproc->lsuspend);
  if (stask ==0)
    return 0;
  kaapi_isready_fnc_t isready = (kaapi_isready_fnc_t)stask->body;
  void* arg_isready = stask->state.arg;

  if (isready(arg_isready))
  {
    kaapi_context_t* context = (kaapi_context_t*)stask->arg;
    kaapi_atomic_lock(&kproc->lock_lsuspend);
    kaapi_list_pop_head(&kproc->lsuspend);
    kaapi_atomic_unlock(&kproc->lock_lsuspend);
    return context;
  }

  return 0; 
}
