/*
** xkaapi
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/

#include "kaapi_impl.h"


/*
*/
static void _kaapi_destroy_ld( kaapi_place_t* ld )
{
  kaapi_assert(0 == kaapi_default_param.emitsteal_dstorctxt(ld->steal_ctxt) );
  kaapi_atomic_destroylock( &ld->lock );
}


/*
*/
static void _kaapi_plainplace_destroy( kaapi_place_t* arg )
{
  kaapi_plainplace_t* place =(kaapi_plainplace_t*)arg;
  kaapi_queue_destroy( &place->queue );
  kaapi_queue_destroy( &place->private_queue );
  _kaapi_destroy_ld(arg);
}


/* Server side
*/
static void _kaapi_plainplace_steal_task(
    kaapi_place_t* ld,
    struct kaapi_listrequest_iterator_t* lri
)
{
  kaapi_plainplace_t* place =(kaapi_plainplace_t*)ld;
  kaapi_queue_steal( &place->queue, lri );
}


/* Server side
*/
static void _kaapi_plainplace_stealin_task(
  struct kaapi_place_t* ld,
  struct kaapi_listrequest_iterator_t* lri
)
{
  kaapi_plainplace_t* place =(kaapi_plainplace_t*)ld;
  kaapi_queue_stealin( &place->queue, lri );

#if 0
  kaapi_plainplace_t* place = (kaapi_plainplace_t*)ld;
  kaapi_task_t* task;
  while (!kaapi_listrequest_iterator_empty(lri))
  {
    kaapi_request_t* request = (kaapi_request_t*)kaapi_listrequest_iterator_get(lri);
    kaapi_assert_debug( request->header.op == KAAPI_REQUEST_OP_STEAL_INPLACE);
    kaapi_assert_debug( request->steal_a.thread.sp != 0);
    kaapi_assert_debug( request->steal_a.thread.sp_data != 0);

    task = kaapi_queue_steal(&place->queue, (kaapi_header_request_t*)request);
    if (task ==0) break;
    int err = kaapi_sched_copy_steal_task(
        task,
        0,
        0,
        task->state.frame,
        (kaapi_steal_request_t*)request
    );
    KAAPI_PERFCTR_INCR(kaapi_self_rsrc()->perf, KAAPI_PERF_ID_TASKSPAWN, 1);
    kaapi_assert( err == 0 );
    kaapi_listrequest_iterator_next(lri);
  }
#endif
}


/* Server side
*/
static kaapi_task_t* _kaapi_plainplace_pop_task( kaapi_place_t* arg)
{
  kaapi_plainplace_t* place =(kaapi_plainplace_t*)arg;
  kaapi_task_t* task;
  task = kaapi_queue_pop_head(&place->private_queue);
  if (task !=0) return task;
  task = kaapi_queue_pop_head(&place->queue);
  return task;
}


/* Server side
*/
static int _kaapi_plainplace_push_task( kaapi_place_t* arg, kaapi_task_t* task)
{
  kaapi_plainplace_t* place =(kaapi_plainplace_t*)arg;
  int err;
  if (task->u.s.flag & KAAPI_TASK_FLAG_LD_BOUND)
    err = kaapi_queue_push_head( &place->private_queue, task );
  else
    err = kaapi_queue_push_head( &place->queue, task );
  return err;
}


/* Server side
*/
static int _kaapi_plainplace_pushremote_task( kaapi_place_t* arg, kaapi_task_t* task)
{
  kaapi_plainplace_t* place =(kaapi_plainplace_t*)arg;
  int err;
  if (task->u.s.flag & KAAPI_TASK_FLAG_LD_BOUND)
    err = kaapi_queue_push_tail( &place->private_queue, task );
  else
    err = kaapi_queue_push_tail( &place->queue, task );
  return err;
}


/* Server side
*/
static int _kaapi_plainplace_push_list( kaapi_place_t* arg, kaapi_list_t* list)
{
  kaapi_plainplace_t* place =(kaapi_plainplace_t*)arg;
  int err = 0;
  while (!kaapi_list_empty(list))
  {
    kaapi_task_t* task = kaapi_list_pop_head(list);
    if (task->u.s.flag & KAAPI_TASK_FLAG_LD_BOUND)
      err |= kaapi_queue_push_tail( &place->private_queue, task );
    else
      err |= kaapi_queue_push_tail( &place->queue, task );
  }
  return err;
}


/*
*/
static int _kaapi_plainplace_isempty( const kaapi_place_t* arg)
{
  const kaapi_plainplace_t* place =(const kaapi_plainplace_t*)arg;
  return kaapi_queue_empty(&place->queue) && kaapi_queue_empty(&place->private_queue);
}



/*
*/
static void _kaapi_kprocplace_destroy( kaapi_place_t* arg )
{
  kaapi_kprocplace_t* place =(kaapi_kprocplace_t*)arg;
  _kaapi_plainplace_destroy(arg);
  place->kproc = 0;
}


/*
*/
static void _kaapi_kprocplace_stealin_task(
  struct kaapi_place_t* ld,
  struct kaapi_listrequest_iterator_t* lri
)
{
  kaapi_kprocplace_t* place =(kaapi_kprocplace_t*)ld;
  kaapi_assert_debug( (void*)place->kproc->rsrc.ownplace == (void*)ld );
  if (!kaapi_place_empty(ld))
    _kaapi_plainplace_stealin_task(ld,lri);

  /* else steal into the stack */
  if (!kaapi_listrequest_iterator_empty(lri))
    kaapi_sched_stealprocessor( place->kproc, lri);

  return;
}


/*
*/
static int _kaapi_kprocplace_isempty( const kaapi_place_t* arg)
{
  const kaapi_kprocplace_t* place =(const kaapi_kprocplace_t*)arg;
  return _kaapi_plainplace_isempty( arg )
      && kaapi_stack_isempty( &place->kproc->context->stack )
  ;
}



int kaapi_listrequest_iterator_empty(
  struct kaapi_listrequest_iterator_t* lri
)
{ return kaapi_default_param.lr_empty_fnc(lri); }

struct kaapi_steal_request_t* kaapi_listrequest_iterator_get(
  struct kaapi_listrequest_iterator_t* lrrange
)
{ return kaapi_default_param.lr_get_fnc(lrrange); }

struct kaapi_steal_request_t* kaapi_listrequest_iterator_next(
  struct kaapi_listrequest_iterator_t* lrrange 
)
{ return kaapi_default_param.lr_next_fnc(lrrange); }

int kaapi_listrequest_iterator_count(
  struct kaapi_listrequest_iterator_t* lrrange
)
{ return kaapi_default_param.lr_count_fnc(lrrange); }


int kaapi_request_reply
( 
  kaapi_request_t* request,
  int              value
)
{ return kaapi_default_param.request_reply_fnc(request, value); }



/*  Client
*/
static kaapi_task_t* _kaapi_place_pop(
    kaapi_ressource_t* rsrc, 
    kaapi_place_t* ld
)
{
  kaapi_request_t* request = kaapi_default_param.request_post_fnc(rsrc, ld);

  request->header.ident = rsrc->kid;
  request->header.op    = KAAPI_REQUEST_OP_POP;

  kaapi_default_param.request_commit_fnc(ld, request);

  return (kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ?
              request->pop_a.task
            : 0);
}



/* Client 
*/
static kaapi_task_t* _kaapi_place_steal( kaapi_ressource_t* rsrc, kaapi_place_t* ld)
{
  kaapi_request_t* request = kaapi_default_param.request_post_fnc(rsrc, ld);

  request->header.ident   = rsrc->kid;
  request->header.op      = KAAPI_REQUEST_OP_STEAL;
  KAAPI_DEBUG_INST( request->steal_a.thread.sp = 0 );
  KAAPI_DEBUG_INST( request->steal_a.thread.sp_data = 0 );

  kaapi_default_param.request_commit_fnc(ld, request);

  return (kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ?
              request->pop_a.task
            : 0);
}


/*  Client 
*/
static int _kaapi_place_stealinplace(
    kaapi_processor_t* kproc,
    kaapi_place_t* ld
)
{
  kaapi_request_t* request = kaapi_default_param.request_post_fnc(&kproc->rsrc, ld);

  request->header.ident   = kproc->rsrc.kid;
  request->header.op      = KAAPI_REQUEST_OP_STEAL_INPLACE;
  request->steal_a.thread = *kaapi_kproc2thread(kproc);

  kaapi_default_param.request_commit_fnc(ld, request);
  if ( kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK)
  {
    kproc->context->stack.thread = request->steal_a.thread;
    kproc->context->stack.unlink->cnt_task += request->steal_a.thread.sp - kproc->context->stack.pc;
    return 0;
  }
  return EINVAL;
}

/* Client
*/
static int _kaapi_place_push(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* ld,
    kaapi_task_t* task
)
{
  kaapi_request_t* request = kaapi_default_param.request_post_fnc(rsrc, ld);

  request->header.ident = rsrc->kid;
  request->header.op    = KAAPI_REQUEST_OP_PUSH;
  request->push_a.task  = task;

  kaapi_default_param.request_commit_fnc(ld, request);

  return kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ? 0 : EINVAL;
}


/* push to remote queue
*/
static int _kaapi_place_push_remote(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* ld,
    kaapi_task_t* task
)
{
  kaapi_request_t* request = kaapi_default_param.request_post_fnc(rsrc, ld);

  request->header.ident = rsrc->kid;
  request->header.op    = KAAPI_REQUEST_OP_PUSH_REMOTE;
  request->push_a.task  = task;

  kaapi_default_param.request_commit_fnc(ld, request);

  return kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ? 0 : EINVAL;
}

/* Client
*/
static int _kaapi_place_push_async(
    struct kaapi_place_group_operation_t* kpgo,
    kaapi_ressource_t* rsrc,
    kaapi_place_t* ld,
    kaapi_task_t* task
)
{
  kaapi_request_t* request = kaapi_default_param.request_post_fnc(rsrc, ld);

  request->header.ident = rsrc->kid;
  request->header.op    = KAAPI_REQUEST_OP_PUSH;
  request->push_a.task  = task;

  if (kaapi_default_param.request_commit_fnc_async)
    kaapi_default_param.request_commit_fnc_async(kpgo, ld, request);
  else
    kaapi_default_param.request_commit_fnc(ld, request);

  return kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ? 0
       : kaapi_request_get_status(request) == KAAPI_REQUEST_S_POSTED ? EINPROGRESS : EINVAL;
}


/* push to remote queue
*/
static int _kaapi_place_push_remote_async(
    struct kaapi_place_group_operation_t* kpgo,
    kaapi_ressource_t* rsrc,
    kaapi_place_t* ld,
    kaapi_task_t* task
)
{
  kaapi_request_t* request = kaapi_default_param.request_post_fnc(rsrc, ld);

  request->header.ident = rsrc->kid;
  request->header.op    = KAAPI_REQUEST_OP_PUSH_REMOTE;
  request->push_a.task  = task;

  if (kaapi_default_param.request_commit_fnc_async)
    kaapi_default_param.request_commit_fnc_async(kpgo, ld, request);
  else
    kaapi_default_param.request_commit_fnc(ld, request);

  return kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ? 0
       : kaapi_request_get_status(request) == KAAPI_REQUEST_S_POSTED ? EINPROGRESS : EINVAL;
}


/*
*/
static int _kaapi_place_pushlist(
    kaapi_ressource_t* rsrc,
    kaapi_place_t*     ld,
    kaapi_list_t*      list
)
{
  kaapi_request_t* request = kaapi_default_param.request_post_fnc(rsrc, ld);

  request->header.ident = rsrc->kid;
  request->header.op    = KAAPI_REQUEST_OP_PUSHLIST;
  request->push_l.list  = list;

  kaapi_default_param.request_commit_fnc(ld, request);

  return kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ? 0 : EINVAL;
}


/* 
   Return negative value in case of error.
   Return the code of the function call.
*/
int kaapi_place_internalop(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* ld,
    int (*func)(kaapi_place_t*, void* ), void* arg
)
{
  if (kaapi_place_empty( ld )) return 0;
  kaapi_request_t* request = kaapi_default_param.request_post_fnc(rsrc, ld);

  request->header.ident   = rsrc->kid;
  request->header.op      = KAAPI_REQUEST_OP_EXTRA;
  request->extra_a.arg    = arg;
  request->extra_a.func   = func;
  request->extra_a.retval = 0;

  kaapi_default_param.request_commit_fnc(ld, request);

  return (kaapi_request_get_status(request) == KAAPI_REQUEST_S_OK ?
              request->extra_a.retval
            : -EINVAL);
}


/*
*/
static struct kaapi_vtable_place_t default_vtable = {
  .fc_steal       = 0,
  .fc_stealin     = 0,
  .fc_pop         = 0,
  .fc_push        = 0,
  .fc_push_remote = 0,
  .fc_push_remote_async = 0,
  .fc_push_async  = 0,
  .fc_pushlist    = 0,
  .f_isempty      = 0,
  .f_destroy      = 0,
  .fs_steal       = 0,
  .fs_stealin     = 0,
  .fs_pop         = 0,
  .fs_push        = 0,
  .fs_push_remote = 0,
  .fs_pushlist    = 0,
};


/*
*/
static struct kaapi_vtable_place_t default_vtable_plain ={
  .fc_steal       = _kaapi_place_steal,
  .fc_stealin     = _kaapi_place_stealinplace,
  .fc_pop         = _kaapi_place_pop,
  .fc_push        = _kaapi_place_push,
  .fc_push_remote = _kaapi_place_push_remote,
  .fc_push_async  = _kaapi_place_push_async,
  .fc_push_remote_async = _kaapi_place_push_remote_async,
  .fc_pushlist    = _kaapi_place_pushlist,
  .f_isempty      = _kaapi_plainplace_isempty,
  .f_destroy      = _kaapi_plainplace_destroy,
  .fs_steal       = _kaapi_plainplace_steal_task,
  .fs_stealin     = _kaapi_plainplace_stealin_task,
  .fs_pop         = _kaapi_plainplace_pop_task,
  .fs_push        = _kaapi_plainplace_push_task,
  .fs_push_remote = _kaapi_plainplace_pushremote_task,
  .fs_pushlist    = _kaapi_plainplace_push_list
};


/*
*/
static struct kaapi_vtable_place_t default_vtable_kproc ={
  .fc_steal       = _kaapi_place_steal,
  .fc_stealin     = _kaapi_place_stealinplace,
  .fc_pop         = _kaapi_place_pop,
  .fc_push        = _kaapi_place_push,
  .fc_push_remote = _kaapi_place_push_remote,
  .fc_push_async  = _kaapi_place_push_async,
  .fc_push_remote_async = _kaapi_place_push_remote_async,
  .fc_pushlist    = _kaapi_place_pushlist,
  .f_isempty      = _kaapi_kprocplace_isempty,
  .f_destroy      = _kaapi_kprocplace_destroy,
  .fs_steal       = _kaapi_plainplace_steal_task,
  .fs_stealin     = _kaapi_kprocplace_stealin_task,
  .fs_pop         = _kaapi_plainplace_pop_task,
  .fs_push        = _kaapi_plainplace_push_task,
  .fs_push_remote = _kaapi_plainplace_pushremote_task,
  .fs_pushlist    = _kaapi_plainplace_push_list
};


/*
*/
void kaapi_init_place( kaapi_place_t* ld )
{
  ld->count      = 0;
  ld->level      = 0;
  ld->steal_ctxt = 0;
  ld->freebloc   = 0;
  ld->l_freebloc = 0;
  ld->vtable     = &default_vtable;
  kaapi_atomic_initlock( &ld->lock );
  kaapi_assert( 0 == kaapi_default_param.emitsteal_initctxt(&ld->steal_ctxt) );
}


/*
*/
void kaapi_init_plainplace( kaapi_plainplace_t* ld)
{
  kaapi_init_place(&ld->inherit);
  ld->inherit.vtable     = &default_vtable_plain;
  kaapi_queue_init( &ld->queue );
  kaapi_queue_init( &ld->private_queue );
}


/*
*/
void kaapi_init_kprocld(
    kaapi_processor_t* kproc,
    kaapi_kprocplace_t* ld
)
{
  kaapi_init_plainplace(&ld->inherit);
  ld->kproc                  = kproc;
  ld->inherit.inherit.vtable = &default_vtable_kproc;
}
