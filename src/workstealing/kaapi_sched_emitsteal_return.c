/*
** xkaapi
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com 
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/

#include "kaapi_impl.h"


/*
*/
kaapi_request_status_t kaapi_sched_emitsteal_return (
    kaapi_processor_t* kproc,
    kaapi_victim_t*    victim,
    kaapi_request_t*   self_request
)
{
  kaapi_request_status_t status;

  status = kaapi_request_get_status(self_request);
  /* mark current processor as no stealing anymore */
  kaapi_assert_debug( status != KAAPI_REQUEST_S_POSTED );

  /* test if my request is ok */
  kaapi_request_syncdata( self_request );

  switch (status)
  {
    case KAAPI_REQUEST_S_OK:
       /* Currently only verify that the returned pointer are into the area given by stackframe[0]
          - Else it means that the result of the steal are tasks that lies outside the stackframe,
          then the start up in that case should be different:
            - first, execute the task between [req->frame.pc and req->frame.sp] in the context
            of the current stack of frame (i.e. pushed task must be pushed locally).
          This requires 1/ change the interface of execframe in order to add pc, sp in signature.
          2/ change the callee to execframe 3/ suppress this comment.
       */
      /* also assert that pc does not have changed (only used at runtime to execute task) */
      kaapi_assert_debug( self_request->steal_a.thread.sp - kproc->context->stack.pc < 10);
      kproc->context->stack.thread      = self_request->steal_a.thread;
      kproc->context->stack.unlink->cnt_task += self_request->steal_a.thread.sp - kproc->context->stack.pc;

#if defined(KAAPI_USE_PERFCOUNTER)
      if (kproc->rsrc.perfkproc !=0)
        ++KAAPI_PERF_REG(kproc->rsrc.perfkproc, KAAPI_PERF_ID_STEALREQOK);
#endif
      kaapi_default_param.wsselect( kproc, victim, KAAPI_STEAL_SUCCESS );
    break;

    case KAAPI_REQUEST_S_NOK:
      kaapi_default_param.wsselect( kproc, victim, KAAPI_STEAL_FAILED );
    break;

    case KAAPI_REQUEST_S_ERROR:
      kaapi_default_param.wsselect( kproc, victim, KAAPI_STEAL_ERROR );
    break;

    default:
      kaapi_assert_debug_m(0, "Bad request status" );
  }

  return status;
}
