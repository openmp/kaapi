/*
 ** kaapi_task_checkdenpendencies.c
 ** xkaapi
 ** 
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 ** thierry.gautier@imag.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include "kaapi_impl.h"
#include <stdio.h>
#include <inttypes.h>

//#define LOG
#undef LOG

static int kaapi_tasklist_critical_path(
    kaapi_stack_t* stack,
    kaapi_frame_t* frame,
    kaapi_frame_wrdlist_t* rdlist
);



/*
*/
int kaapi_frame_computereadylist_current(void)
{
  kaapi_context_t* ctxt = kaapi_self_context();
  kaapi_stack_t* stack  = &ctxt->stack;
  kaapi_frame_t* frame  = stack->unlink;
  return kaapi_frame_computereadylist(stack, frame);
}


/** \ingroup DFG
     Allow to iteration through sequence of accesses in order to group producer or consummer
     together. For instance in {r}* -> w | x  => mode is r, first is the reader and last is 
     the last reader and prev_mode = VOID. All readers are linked using next_field of access_t 
     structure.
     In case of {x|w} -> {r*} -> w | x, when reader are traversed, prev_mode = x | w and other
     fields are as in the first example. 
     Should be used BUT NOT YET IMPLEMENTED for CW.
*/
typedef struct kaapi_prodcons_t {
  kaapi_access_mode_t         mode;         /* last access mode to the data */
  kaapi_access_mode_t         prev_mode;    /* access mode to the previously viewed prodcons_t */
  kaapi_access_t*             first;        /* */
  kaapi_access_t*             last;         /* */
  int                         concurrency;  /* number of concurrent access */
  kaapi_task_t*               task;         /* to detect doublon in task parameters */
} kaapi_prodcons_t;



#define RDTSC_PUSH 0
#if RDTSC_PUSH
/*
*/
static inline uint64_t rdtsc(void)
{
  uint32_t lo,hi;
  __asm__ volatile ( "rdtsc" : "=a" ( lo ) , "=d" ( hi ) );
  return (uint64_t)hi << 32UL | lo;
}

extern uint64_t gomp_task_rdtsc;
#endif

/*
*/
int kaapi_frame_computereadylist(
  kaapi_stack_t* stack,
  kaapi_frame_t* frame
)
{
#if RDTSC_PUSH
uint64_t rd0 = rdtsc(); 
#endif
  int err = 0;
  kaapi_task_t* task;
  kaapi_context_t* context = kaapi_stack2context(stack);
  kaapi_frame_wrdlist_t* rdlist = 0;
  kaapi_frame_dfgctxt_t* dfgctxt = 0;
  kaapi_stack_iterator_t iter;

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_ressource_t* rsrc = &kaapi_stack2kproc(stack)->rsrc;
  uint64_t delay = kaapi_get_elapsedns();
  KAAPI_EVENT_PUSH0( rsrc->evtkproc, 0, KAAPI_EVT_COMP_DAG_BEG );
#endif

  unsigned int i;
  unsigned int count_params;
  int wc;

  rdlist = (kaapi_frame_wrdlist_t*)malloc( sizeof(kaapi_frame_wrdlist_t) + sizeof(kaapi_frame_dfgctxt_t) );
  kaapi_frame_rdlist_init( rdlist, (kaapi_frame_dfgctxt_t*)(rdlist+1));
  dfgctxt = rdlist->ctxt;

  err = kaapi_stack_iterator_init(&iter, stack, frame);
  if (err) goto return_retval;

  KAAPI_PERFCTR_IF(kaapi_stack2perfproc(stack), KAAPI_PERF_ID_DFGBUILD,
    kaapi_perf_counter_t    t0 =0,
    t0 = kaapi_get_elapsedns();
  );

  kaapi_assert_debug((frame->flag & KAAPI_FRAME_FLAG_DFG_OK) ==0);
  frame->flag |= KAAPI_FRAME_FLAG_NOTSTEAL;
  kaapi_writemem_barrier();

#if defined(LOG)
printf("\n\n----------------------------\n");
#endif
  while (!kaapi_stack_iterator_empty(&iter))
  {
    /* pass all task fully executed */
    task = kaapi_stack_iterator_get( &iter );

    if (task->state.steal != task->body)
    {
      kaapi_abort(__LINE__, __FILE__, "here");
      kaapi_stack_iterator_next( &iter );
      continue; 
    }

    /* cannot assume task->fmt !=0 : task->fmt not initialized if task->flag & DFG ==0 */
    const kaapi_format_t* task_fmt = _kaapi_task_getformat(task);
    if (task_fmt ==0)
      task->u.s.flag |= KAAPI_TASK_FLAG_INDPENDENT;
    else if (task->fmt == 0)
      task->fmt = task_fmt;

    task->Tinf = 0;
    task->T    = 0;
    KAAPI_ATOMIC_WRITE( &task->wc, 0);
    count_params = kaapi_format_get_count_params(task_fmt, kaapi_task_getargs(task));

    wc = (int)count_params;
    for (i=0; i<count_params; ++i)
    {
      kaapi_access_mode_t m = kaapi_format_get_mode_param(task_fmt, i, kaapi_task_getargs(task));
      if (m & KAAPI_ACCESS_MODE_V)
      {
        --wc;
        continue;
      }
      kaapi_access_t* access = kaapi_format_get_access_param(task_fmt, i, kaapi_task_getargs(task));

      /* init access field when DFG is built */
      access->task = task;
      access->next = 0;
      access->next_out = 0;
      access->version = 0;

      kaapi_memory_view_t view;
      kaapi_format_get_view_param(task_fmt, i, kaapi_task_getargs(task), &view);
      void* ptr = kaapi_memory_view2pointer(access->data, &view );

      /* state of the DFG with respect to the access */
      kaapi_hashentries_t* entry = kaapi_hashmap_findinsert( &dfgctxt->access_to_gd, ptr );
      kaapi_assert_debug( sizeof(entry->u) >= sizeof(kaapi_prodcons_t) );
      kaapi_prodcons_t* prcs = &KAAPI_HASHENTRIES_GET(entry, kaapi_prodcons_t);

#if defined(KAAPI_DEBUG) && defined(LOG)
printf("\t data: %p, access: %p, mode: %c\n", (void*)ptr, (void*)access, kaapi_getmodename(m));
#endif

      /* first access: already ready */
      if (prcs->mode == KAAPI_ACCESS_MODE_VOID)
      {
        prcs->mode      = m;
        prcs->prev_mode = KAAPI_ACCESS_MODE_VOID;
        prcs->first     = access;
        prcs->last      = access;
        prcs->concurrency= 1;
        prcs->task      = task;
        --wc;
        continue;
      }

      /* doublon: upgrade access mode */
      if (prcs->task == task)
      {
        kaapi_access_mode_t mix = prcs->mode | m;
        if (KAAPI_ACCESS_IS_READWRITE(mix))
          prcs->mode = KAAPI_ACCESS_MODE_RW;
        if ((KAAPI_ACCESS_IS_CUMULWRITE(mix) && KAAPI_ACCESS_IS_READ(mix))
        ||  (KAAPI_ACCESS_IS_CUMULWRITE(mix) && KAAPI_ACCESS_IS_WRITE(mix)))
          kaapi_abort(__LINE__, __FILE__, "**** warning, task aggregate both R and CW accesses to the same data. Ignore R");
        --wc;
        continue;
      }
      else prcs->task      = task;

      /* concurrent access with src_mode: also implies concurrent with previous access */
      if (KAAPI_ACCESS_IS_CONCURRENT(m, prcs->mode))
      {
        if (prcs->prev_mode == KAAPI_ACCESS_MODE_VOID) --wc;

        /* for next synchro */
        ++prcs->concurrency;
        /* link dep_in */
        kaapi_assert( access != prcs->last );
        prcs->last->next = access;
        prcs->last       = access;
      }
      else
      {
        /* wc for current task increased by concurrency - 1; */
        wc += (int)prcs->concurrency - 1;

        kaapi_access_t* curr = prcs->first;
        while (curr !=0)
        {
          curr->next_out = access;
          curr = curr->next;
        };
        prcs->prev_mode = prcs->mode;
        prcs->mode  = m;
        prcs->first = access;
        prcs->last  = access;
        prcs->concurrency = 1;
      }
    }

    kaapi_assert_debug( wc >= 0);
    KAAPI_ATOMIC_WRITE( &task->wc,(int32_t)wc );
#if defined(KAAPI_DEBUG) && defined(LOG)
printf("[DFG] task: %p, name: %s, wc: %d\n", (void*)task, task->fmt->name, (int)KAAPI_ATOMIC_READ(&task->wc) );
#endif

    task->u.s.flag |= KAAPI_TASK_FLAG_DFGOK;
    task->state.frame = rdlist;
#if defined(KAAPI_LINKED_LIST)
    task->next = 0;
    task->prev = 0;
#endif
    if (wc ==0)
    {
      kaapi_assert_debug(task->state.steal !=0);
      kaapi_list_push_tail( &rdlist->rd, task );
    }

    kaapi_stack_iterator_next( &iter );
    ++rdlist->cnt_task;
//printf("%i\n", rdlist->cnt_task );
  }
#if defined(KAAPI_USE_PERFCOUNTER)
  //printf("[DFG] #tasks       = %"PRIu64"\n", rdlist->cnt_task);
#endif

#undef KAAPI_DEBUG_INST
#define KAAPI_DEBUG_INST(x) x

  /* Here compute the apriori minimal date of execution */
  if (kaapi_default_param.ctpriority)
  {
    KAAPI_DEBUG_INST(double t0 =) kaapi_get_elapsedtime();
    kaapi_tasklist_critical_path( &context->stack, frame, rdlist );
    KAAPI_DEBUG_INST(double t1 =) kaapi_get_elapsedtime();
    KAAPI_DEBUG_INST(printf("#tasks       = %"PRIu64"\n", rdlist->cnt_task);)
    KAAPI_DEBUG_INST(printf("Criticalpath = %"PRIu64"\n computed in :%f s\n", rdlist->t_infinity, t1-t0);)
#if 0
    {
      static uint32_t counter = 0;
      char filename[128]; 
      if (getenv("USER") !=0)
        sprintf(filename,"/tmp/graph_ct.%s.%i.dot", getenv("USER"), counter++ );
      else
        sprintf(filename,"/tmp/graph_ct.%i.dot", counter++);
      FILE* filedot = fopen(filename, "w");
      kaapi_frame_print_dot( filedot, stack, frame);

      if (getenv("USER") !=0)
        sprintf(filename,"/tmp/graph_ct2.%s.%i.dot", getenv("USER"), counter++ );
      else
        sprintf(filename,"/tmp/graph_ct2.%i.dot", counter++);
      filedot = fopen(filename, "w");
      kaapi_frame_tasklist_print( filedot, rdlist);
      fclose(filedot);
    }
#endif
  }

  frame->rdlist = rdlist;
  kaapi_writemem_barrier();
  //OLD: frame->flag &= ~KAAPI_FRAME_FLAG_NOTSTEAL;
  frame->flag |= KAAPI_FRAME_FLAG_DFG_OK;

  err= kaapi_hashmap_destroy( &dfgctxt->access_to_gd );

  KAAPI_PERFCTR_IF(kaapi_stack2perfproc(stack), KAAPI_PERF_ID_DFGBUILD,
    , /* no declaration */
    kaapi_perf_counter_t t1 = kaapi_get_elapsedns();
    KAAPI_PERFCTR_INCR(kaapi_stack2perfproc(stack), KAAPI_PERF_ID_DFGBUILD, t1-t0);
    //printf("[kaapi]: DFG %e\n", (t1-t0)*1e-9);
  );

#if defined(KAAPI_USE_PERFCOUNTER)
  delay = kaapi_get_elapsedns() - delay;
  KAAPI_EVENT_PUSH1(rsrc->evtkproc, 0, KAAPI_EVT_COMP_DAG_END, delay );
#endif

#if defined(LOG)
  printf("\n\n----------------------------\n");
#endif
return_retval:
{
#if RDTSC_PUSH
uint64_t rd1 = rdtsc(); 
gomp_task_rdtsc += rd1-rd0;
#endif
}
  return err;
}


/* Returns 1+ max(successors(task))
*/
static uint64_t kaapi_compute_cp( kaapi_task_t* task )
{
  uint64_t      maxdatesucc = 1;

  kaapi_assert_debug( task->u.s.flag & KAAPI_TASK_FLAG_DFGOK );
  const kaapi_format_t* fmt = _kaapi_task_getformat( task );
  kaapi_assert_debug( fmt != 0 );
  unsigned int i, count_params = kaapi_format_get_count_params( fmt, task->arg );

  for (i=0; i<count_params; ++i)
  {
    kaapi_access_mode_t mode       = kaapi_format_get_mode_param(fmt, i, task->arg);
    kaapi_access_mode_t mode_param = KAAPI_ACCESS_GET_MODE( mode );
    if (mode_param & KAAPI_ACCESS_MODE_V)
      continue;

    /* look next successors */
    kaapi_access_t* access = kaapi_format_get_access_param(fmt, i, task->arg);
    access = access->next_out;
    while (access !=0)
    {
      kaapi_task_t* next_task = access->task;
      uint64_t  datesucc = next_task->T;
      if (datesucc > maxdatesucc)
        maxdatesucc = datesucc;
      access = access->next;
    }
  }
  return maxdatesucc + (uint64_t)1;
}


/* compute priority
*/
static void kaapi_compute_priority(
  kaapi_task_t* task,
  int version
)
{
#if 0
  uint64_t      maxdatesucc = 0;
  kaapi_task_t* task_maxdatesucc = 0;
  kaapi_access_mode_t next_mode_param;
  kaapi_access_mode_t first_next_mode_param;

  /* if already visited, return */
  if (task->u.s.mark == version)
    return;
  kaapi_assert_debug( task->u.s.mark <= version );
  kaapi_assert_debug( task->u.s.flag & KAAPI_TASK_FLAG_DFGOK );

  const kaapi_format_t* fmt = _kaapi_task_getformat(task);
  kaapi_assert_debug( fmt !=0 );
  unsigned int i, count_params = kaapi_format_get_count_params( fmt, task->arg );

  /* compute the max */
  for (i=0; i<count_params; ++i)
  {
    kaapi_access_mode_t mode       = kaapi_format_get_mode_param(fmt, i, task->arg);
    kaapi_access_mode_t mode_param = KAAPI_ACCESS_GET_MODE( mode );
    if (mode_param & KAAPI_ACCESS_MODE_V)
      continue;

    kaapi_access_t* access = kaapi_format_get_access_param(fmt, i, task->arg);

    /* iterate over all successors */
    access = access->next_out;
    while (access !=0)
    {
      uint64_t  datesucc = 0;
      kaapi_task_t* next_task = 0;
      next_task = access->task;

      datesucc = (next_task == 0 ? 0 : next_task->u.s.date);
      if (datesucc > maxdatesucc)
      {
        maxdatesucc = datesucc;
        task_maxdatesucc = next_task;
      }
      access = access->next;
    }
  }

  task->u.s.mark = version;
  task->u.s.flag |= KAAPI_TASK_FLAG_PRIORITY;
  task->u.s.priority = 1;
#endif
}



/* compute the longest path of each task
*/
static int kaapi_tasklist_critical_path( kaapi_stack_t* stack, kaapi_frame_t* frame, kaapi_frame_wrdlist_t* rdlist )
{
  if (kaapi_list_empty(&rdlist->rd))
    return EINVAL;

  uint64_t date, maxdate = 0;
  kaapi_task_t* task_maxdate = 0;

  kaapi_stack_iterator_t iter;
  kaapi_stack_reverse_iterator_init(&iter, stack, frame);

  while ( !kaapi_stack_iterator_empty(&iter))
  {
    kaapi_task_t* task = kaapi_stack_iterator_get(&iter);
    task->T = (uint32_t)(date = kaapi_compute_cp( task ));
    if (date > maxdate)
    {
      maxdate = date;
      task_maxdate = task;
    }
    kaapi_stack_reverse_iterator_next(&iter);
  }

  rdlist->t_infinity = maxdate;

  kaapi_stack_reverse_iterator_destroy(&iter);
  return 0;
}


/*
*/
void kaapi_tasksync_body( kaapi_task_t* task, kaapi_thread_t* thread )
{
}

const kaapi_format_t* kaapi_tasksync_fmt = 0;
