/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"
#include <inttypes.h>

/*
*/
void kaapi_staticschedtask_body(
    kaapi_task_t* pc,
    kaapi_thread_t* thread
)
{
  int save_state;
#if 0
  int16_t ngpu = 0;
  int16_t ncpu = 0;
#endif

  kaapi_staticschedtask_arg_t* arg = (kaapi_staticschedtask_arg_t*)pc->arg;
  kaapi_context_t* context = kaapi_thread2context(thread);

  /* here... begin execute frame tasklist*/
//  KAAPI_EVENT_PUSH0(context->proc->perf, thread, KAAPI_EVT_STATIC_BEG );

  /* Push a new frame */
  kaapi_frame_t* frame = kaapi_stack_push_frame(&context->stack);
  
  /* unset steal capability and wait no more thief 
     lock the kproc: it ensure that no more thief has reference on it 
  */
  save_state = context->unstealable;
  context->stack.unlink->flag |= KAAPI_FRAME_FLAG_NOTSTEAL;

#if 0
  /* Here, either:
     - nkproc[0] is !=-1 and represents annonymous ressources
       then nkproc[CPU] and nkproc[GPU] == -1.
     - nkproc[CPU] == nkproc[GPU] == nkproc[0] = -1, means auto detect
     - nkproc[CPU] or nkproc[GPU] == -2, means all ressources of ginve type
     - nkproc[CPU] or nkproc[GPU] is set to a user requested number
  */

  if (arg->schedinfo.nkproc[0] != -1)
  {
    kaapi_assert_debug(arg->schedinfo.nkproc[0] >0);

    /* first take all GPUs ressources, then complet with CPU ressources */
    if (ngpu < arg->schedinfo.nkproc[0])
    {
      ncpu = arg->schedinfo.nkproc[0] - ngpu;
      if (ncpu > (int16_t)kaapi_get_concurrency())
        ncpu = kaapi_get_concurrency();
    }
    arg->schedinfo.nkproc[KAAPI_PROC_TYPE_CPU] = ncpu;
    arg->schedinfo.nkproc[0] = ngpu + ncpu;
  }
  else 
  {
    if (arg->schedinfo.nkproc[KAAPI_PROC_TYPE_CPU] <0)
    { /* do not separate the case -1 and -2 (autodetec and all ressources) because
         the runtime is unable to return the available idle ressources
      */
      arg->schedinfo.nkproc[KAAPI_PROC_TYPE_CPU] = kaapi_get_concurrency();
    }
      
    arg->schedinfo.nkproc[KAAPI_PROC_TYPE_GPU] = 0;
    arg->schedinfo.nkproc[0] = arg->schedinfo.nkproc[KAAPI_PROC_TYPE_CPU] 
        + arg->schedinfo.nkproc[KAAPI_PROC_TYPE_GPU];
  }
#endif

  /* the embedded task cannot be steal because it was not created !*/
  kaapi_task_t task;
  task.body      = arg->sub_body;
  task.state.steal     = 0;
  task.arg       = arg->sub_arg;
  task.u.dummy   = 0;
#if defined(KAAPI_LINKED_LIST)
  task.next      = 0;
#endif

  arg->sub_body( &task, kaapi_stack2thread(&context->stack) );

  /* do nothing if no task pushed */
  if (context->stack.pc == context->stack.thread.sp)
    return;

  /* currently: that all, do not compute other things */
  kaapi_frame_computereadylist(&context->stack, context->stack.unlink);

//  KAAPI_EVENT_PUSH0(context->proc->perf, thread, KAAPI_EVT_STATIC_END );

  /* restore state : if NOTSTEAL is set, leave it*/
  //context->stack.unlink->flag &= ~KAAPI_FRAME_FLAG_NOTSTEAL;
  context->stack.unlink->flag = save_state;
  KAAPI_DEBUG_INST( if (save_state & KAAPI_FRAME_FLAG_NOTSTEAL) printf("***warning restore a stack with notsteal flag.\n"));

  /* exec the spawned subtasks */
  kaapi_sched_sync(kaapi_stack2thread(&context->stack));

  /* Pop & restore the frame */
  kaapi_stack_pop_frame(&context->stack, frame);

#if 0
  fprintf(stdout, "[%s] kid=%i tasklist tasks: %llu total: %llu\n", 
    __FUNCTION__,
    kaapi_get_self_kid(),
    KAAPI_ATOMIC_READ(&tasklist->cnt_exec),
    tasklist->total_tasks
  );
  fflush(stdout);
#endif

#if 0//defined(KAAPI_USE_PERFCOUNTER)
  printf("[tasklist] T1                      : %" PRIu64 "\n", tasklist->cnt_tasks);
  printf("[tasklist] Tinf                    : %" PRIu64 "\n", tasklist->t_infinity);
  printf("[tasklist] dependency analysis time: %e (s)\n",t1-t0);
  printf("[tasklist] exec time               : %e (s)\n",t1_exec-t0_exec);
#endif

}
