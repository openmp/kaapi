/*
 ** xkaapi
 ** 
 ** Created on Tue Mar 31 15:21:00 2009
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include "kaapi_impl.h"
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <time.h> // nanosleep

#if defined(KAAPI_DEBUG)
#include <stdio.h>
#endif

/* arg = argstruct for the steal state method */
#if 1 /* for High perf in dpotrf */
#define KAAPI_STEAL_FAILED_INST( arg )\
  { \
     ++arg->count; \
     kaapi_slowdown_cpu(); \
  }
#else
#define KAAPI_STEAL_FAILED_INST( arg )\
   ++arg->count; \
   if (arg->count > 8) \
   { \
     if (arg->delay ==0) arg->delay = 1;\
     struct timespec ts = { 0, arg->delay }; \
     nanosleep( &ts, 0 ); \
     arg->delay = 2*arg->delay; \
     if (arg->delay > 512) arg->delay = 1; \
   }
#endif

#define KAAPI_STEAL_SUCCESS_INST( arg )\
    arg->count = 0;\
    arg->delay = 1;\

/* linear backoff */
//   usleep(5 * (arg->count % 10) )

#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
static int next_elem(kaapi_cpuset_t *nodes, int first_index)
{
  kaapi_cpuset_t temp = *nodes;
  KAAPI_CPUSET_RSHIFT(&temp, first_index);
  int retVal = KAAPI_CPUSET_FIRSTONE(&temp) - 1;
  if (retVal < 0)
    return (KAAPI_CPUSET_FIRSTONE(nodes) - 1);
  else
    return retVal + first_index;
}
#endif

/* fnc_selecarg[Ø] == lastsuccess_kid +1 */
typedef struct kaapi_victim_rand_arg_t {
  int init;
  int lastvictimid;
  int level;
  int count;
  int delay;
} kaapi_victim_rand_arg_t;



/* Do rand selection
*/
int kaapi_sched_select_victim_rand
(
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
)
{
  kaapi_team_t* team = kproc->team;
  kaapi_victim_rand_arg_t* arg = (kaapi_victim_rand_arg_t*)kproc->arg_selecfn;
   
  switch (flag)
  {
    case KAAPI_SELECT_VICTIM:
    {
      int nbproc, victimid;
      kaapi_processor_t* victim_proc = 0;
      {
        nbproc = team->count;
#if defined(KAAPI_USE_OFFLOAD) /* add host device */
        if (kaapi_offload_get_num_devices() >0) ++nbproc;
#endif

        if (nbproc <=1)
          victimid = 0;
        else
        {
          victimid = rand_r( (unsigned int*)&kproc->rsrc.seed /*fnc_selecarg*/ ) % (2*nbproc);
          if (victimid <= nbproc*1) victimid = 0;
          else victimid -= 1*nbproc;
        }
      }
      
      /* Get the k-processor */    
#if defined(KAAPI_USE_OFFLOAD) /* add host device */
      if (victimid < team->count)
#endif
      {
        victim_proc = team->all_kprocessors[ victimid ];
        if (victim_proc ==0) return EINVAL;
        victim->ident = victimid;
        victim->ld    = victim_proc->rsrc.ownplace;
      }
#if defined(KAAPI_USE_OFFLOAD) /* add host device */
      else
      {
        victim->ident = -1;
        victim->ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_OFFLOAD_HOST].places[0];
      }
#endif      
      break;
    }
      
    case KAAPI_STEAL_SUCCESS:
    {
      KAAPI_STEAL_SUCCESS_INST(arg);
      break;
    }
    case KAAPI_STEAL_FAILED:
    {
      KAAPI_STEAL_FAILED_INST(arg);
      break;
    }

    default:
      break;
  }
  
  return 0;
}




#if defined(KAAPI_USE_NUMA)
/* Do rand selection over numa node: 1/ kproc 2/ numa node
*/
int kaapi_sched_select_victim_rand_numa
(
    kaapi_processor_t*       kproc, 
    kaapi_victim_t*          victim,
    kaapi_selecvictim_flag_t flag 
)
{
  kaapi_team_t* team = kproc->team;
  kaapi_victim_rand_arg_t* arg = (kaapi_victim_rand_arg_t*)kproc->arg_selecfn;
   
  switch (flag)
  {
    case KAAPI_SELECT_VICTIM:
    {
      int count, victimid;
      if (arg->level == 0)
        count = team->count;
      else
        count = team->numaplaces.count;

      if (count < 1)
        return EINVAL;

      /* victim */
      victimid = rand_r( (unsigned int*)&kproc->rsrc.seed ) % count;

      /* Get the k-processor */    
      victim->ident = arg->lastvictimid % count;
      arg->lastvictimid = victimid;
      switch (arg->level)
      {
        case 0:
        {
          /*Select our own kproc*/
          victim->ld = kproc->rsrc.ownplace;
        } break;

        case 1:
          victim->ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[kproc->rsrc.numaid];
        break;
        default:
          victim->ld = team->numaplaces.places[victim->ident];
        break;
      }
//printf("%i:: Select %p, level:%i\n", kproc->kid, victim->ld, arg->level);
    }
      
    case KAAPI_STEAL_SUCCESS:
    {
      KAAPI_STEAL_SUCCESS_INST(arg);
//printf("%i:: Steal OK, level:%i\n", kproc->kid, arg->level);
//      arg->level = 0;
//      arg->level = 0;
      break;
    }
    case KAAPI_STEAL_FAILED:
    {
      KAAPI_STEAL_FAILED_INST(arg);
      ++arg->level;
      if (arg->level > 6)
        arg->level = 0; 
//printf("%i:: Steal Fail, level:%i\n", kproc->kid, arg->level);
      break;
    }

    default:
      break;
  }
  
  return 0;
}


typedef struct kaapi_sched_select_victim_strict_numa_arg_t {
  int level;
} kaapi_sched_select_victim_strict_numa_arg_t;


/* Do selection strictly of our numa node (used for initialisation)
*/
int kaapi_sched_select_victim_strict_push
(
    kaapi_processor_t*       kproc, 
    kaapi_victim_t*          victim,
    kaapi_selecvictim_flag_t flag 
)
{
  kaapi_team_t* team = kproc->team;
  kaapi_sched_select_victim_strict_numa_arg_t* arg = (kaapi_sched_select_victim_strict_numa_arg_t*)kproc->arg_selecfn;
   
  switch (flag)
  {
    case KAAPI_SELECT_VICTIM:
    {
      int count;
      arg->level= 1 - arg->level;
      if (arg->level == 0)
        count = team->count;
      else
        count = team->numaplaces.count;

      if (count <1)
        return EINVAL;

      switch (arg->level)
      {
       case 0:
       {
          victim->ld = kproc->rsrc.ownplace;
          victim->ident = kproc->rsrc.kid;
//printf("%i::  select kid:%i\n", kproc->rsrc.kid );
        } break;

       default:
          victim->ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[kproc->rsrc.numaid];
          victim->ident = kproc->rsrc.numaid;
//printf("%i::  select numaid:%i\n", kproc->rsrc.kid, kproc->rsrc.numaid );
        break;
      }
    }
    case KAAPI_STEAL_SUCCESS:
    {
      break;
    }
    case KAAPI_STEAL_FAILED:
    {
      break;
    }

    default:
      break;
  }
  return 0;
}
#endif



/* Do rand selection
*/
typedef struct kaapi_victim_global_arg_t {
  int level;
  int count0;
  int count1;
  int delay;
  int failed;
} kaapi_victim_global_arg_t;

int kaapi_sched_select_victim_global
(
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
)
{
  kaapi_victim_global_arg_t* arg = (kaapi_victim_global_arg_t*)kproc->arg_selecfn;
  kaapi_team_t* team = kproc->team;
  int count =  team->numaplaces.count;
 
  switch (flag)
  {
    case KAAPI_SELECT_VICTIM:
    {
redo:
      switch (arg->level) {
        case 0:
          victim->ident = -1;
          victim->ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_MACHINE].places[0];
          if (!kaapi_place_empty(victim->ld))
            return 0;
        case 1:
          victim->ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[kproc->rsrc.numaid];
          if (!kaapi_place_empty(victim->ld))
            return 0;
        case 2:
          victim->ld = kproc->rsrc.ownplace;
          if (!kaapi_place_empty(victim->ld))
            return 0;
          if (++arg->count0 <8)
            goto redo; 
        case 3:
        {
#if 0
          /* neighbor numa place (on SGI) */
          kaapi_numaid_t local = kproc->rsrc.numaid;
          kaapi_numaid_t local0 = local & ~0x1;
          kaapi_numaid_t local1 = local + 1;
          local = (local == local0 ? local1 : local0);
          victim->ld = team->numaplaces.places[local % count];
          if (!kaapi_place_empty(victim->ld))
            return 0;
          if (++arg->count1 <2)
            goto redo; 
          arg->count1 = 0;
#endif
          arg->count0 = 0;
        }
        case 4:
          /* random victim */
          victim->ident = rand_r( (unsigned int*)&kproc->rsrc.seed ) % count;
          victim->ld = team->numaplaces.places[victim->ident];
        return 0;
     };


      /* */
#if 0
#if defined(KAAPI_USE_NUMA)
      return 0;
#else
      return kaapi_sched_select_victim_rand( kproc, victim, flag);
#endif
      return EINVAL;
#endif
      break;
    }
      
    case KAAPI_STEAL_SUCCESS:
    {
      arg->failed = 0;
      break;
    }
    case KAAPI_STEAL_FAILED:
    {
#if 0
      if (++arg->failed > 32)
      {
        if (arg->delay ==0) arg->delay = 2;
        struct timespec ts = { 0, arg->delay }; 
        nanosleep( &ts, 0 ); 
        arg->delay = arg->delay * 3 /2;
        if (arg->delay > 128)
        {
          arg->delay = 2;
          arg->failed = 0;
        }
      } else if (arg->failed > 16)
        pthread_yield();
      else if (arg->failed > 8)
        kaapi_slowdown_cpu(); 
      break;
#endif
    }

    default:
      break;
  }
  
  return 0;
}
#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
/** Do hierarchical selection on all numa levels
*/
typedef struct kaapi_victim_hws_P_N_arg_t {
  int checked;
  int rand_index;
  kaapi_cpuset_t *procs;
  kaapi_cpuset_t *nodes;
} kaapi_victim_hws_P_N_arg_t;

int kaapi_sched_select_victim_hierarchy_kproc_numa(
    kaapi_processor_t*       kproc,
    kaapi_victim_t*          victim,
    kaapi_selecvictim_flag_t flag
)
{
  kaapi_victim_hws_P_N_arg_t* arg = (kaapi_victim_hws_P_N_arg_t*)kproc->arg_selecfn;

  kaapi_team_t* team = kproc->team;
  int numaid = kproc->rsrc.numaid;
  if (arg->procs == 0)
  {
    //allocate on kproc stack the cpuset of the node
    arg->procs = kaapi_processor_alloc_data(kproc, sizeof(kaapi_cpuset_t));
    kaapi_assert_debug(arg->procs);
    *arg->procs = team->node_bitmap[numaid];
  }
  if (arg->nodes == 0) {
    //allocate on stack the cpuset of the numa nodes
    arg->nodes = kaapi_processor_alloc_data(kproc, sizeof(kaapi_cpuset_t));
    kaapi_assert_debug(arg->nodes);
    *arg->nodes = team->numa_bitmap;
    //We want to visit our node too
    /*KAAPI_CPUSET_CLR(numaid, arg->nodes);*/
  }
  switch (flag)
  {
    case KAAPI_SELECT_VICTIM:
    {
        if (team->count <1)
          return EINVAL;
        int nextnuma = numaid;
        /*Determine the numa node we want to work next*/
        if (!KAAPI_CPUSET_ISSET(numaid, arg->nodes)) {
          /*Get next node starting from a random index*/
          nextnuma = next_elem(arg->nodes, arg->rand_index);
        } else {
          /*We always start by our numaid, init the random index*/
          arg->rand_index = rand_r(&(kproc->rsrc.seed)) % (team->numaplaces.count);
        }

        /*Handle end of everything*/
        if (nextnuma < 0) {
          arg->checked = 0;
          *arg->procs = team->node_bitmap[numaid];
          *arg->nodes = team->numa_bitmap;
          nextnuma = numaid;
        }

        /*Proceed to numa if no core remaining*/
        if (KAAPI_CPUSET_COUNT(arg->procs) == 0) 
        {
          if (!arg->checked) {
            arg->checked = 1;
            victim->ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[nextnuma];
            kaapi_assert_debug(victim->ld);
            /*Clear the node we just visited*/
            if (victim->ld == 0) return EINVAL;
            victim->ident = numaid;
            KAAPI_CPUSET_CLR(nextnuma, arg->nodes);
            return 0;
          }
          /*If we are here, and no cpu remains, numa place has been checked and we proceed to next node*/
          else /* (KAAPI_CPUSET_COUNT(arg->procs) == 0) */ {
            arg->checked = 0;
            *arg->procs = team->node_bitmap[nextnuma];
          }
        }

        /*Check remaining procs*/
        int victimid = KAAPI_CPUSET_FIRSTONE(arg->procs) - 1;
        kaapi_assert_debug(victimid >= 0);
        kaapi_processor_t *victim_proc = team->all_kprocessors[victimid];
        if (victim_proc == 0) return EINVAL;
        victim->ld = victim_proc->rsrc.ownplace;
        victim->ident = victimid;
        KAAPI_CPUSET_CLR(victimid, arg->procs);

        return 0;

      break;
    }
    case KAAPI_STEAL_SUCCESS:
    {
      arg->checked = 0;
      //*arg->procs = team->node_bitmap[numaid];
      //*arg->nodes = team->numa_bitmap;
      break;
    }
    case KAAPI_STEAL_FAILED:
    {
      //pthread_yield();
      break;
    }

    default:
      break;
  }

  return 0;
}
#endif


#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
/** Do hierarchical selection on all numa levels
*/
typedef struct kaapi_victim_hws_N_P_arg_t {
  int checked;
  int rand_index;
  kaapi_cpuset_t *procs;
  kaapi_cpuset_t *nodes;
} kaapi_victim_hws_N_P_arg_t;

int kaapi_sched_select_victim_hierarchy_numa_kproc(
    kaapi_processor_t*       kproc,
    kaapi_victim_t*          victim,
    kaapi_selecvictim_flag_t flag
)
{
  kaapi_victim_hws_N_P_arg_t* arg = (kaapi_victim_hws_N_P_arg_t*)kproc->arg_selecfn;

  kaapi_team_t* team = kproc->team;
  int numaid = kproc->rsrc.numaid;
  if (arg->procs == 0) {
    //allocate on stack the cpuset of the node
    arg->procs = kaapi_processor_alloc_data(kproc, sizeof(kaapi_cpuset_t));
    kaapi_assert_debug(arg->procs);
    *arg->procs = team->node_bitmap[numaid];
  }
  if (arg->nodes == 0) {
    //allocate on stack the cpuset of the numa nodes
    arg->nodes = kaapi_processor_alloc_data(kproc, sizeof(kaapi_cpuset_t));
    kaapi_assert_debug(arg->nodes);
    *arg->nodes = team->numa_bitmap;
    //We want to visit our node too
    /*KAAPI_CPUSET_CLR(numaid, arg->nodes);*/
  }
  switch (flag)
  {
    case KAAPI_SELECT_VICTIM:
    {
      if (team->count <=1)
        return EINVAL;
      int nextnuma = numaid;
      /*Determine the numa node we want to work next*/
      if (!KAAPI_CPUSET_ISSET(numaid, arg->nodes)) {
        /*Get next node starting from a random index*/
        nextnuma = next_elem(arg->nodes, arg->rand_index);
      } else {
        /*We always start by our numaid, init the random index*/
        arg->rand_index = rand_r(&(kproc->rsrc.seed)) % (team->numaplaces.count);
      }

      /*Handle end of everything*/
      if (nextnuma < 0) {
        arg->checked = 0;
        *arg->procs = team->node_bitmap[numaid];
        *arg->nodes = team->numa_bitmap;
        nextnuma = numaid;
      }

      /*Proceed to next node*/
      if (KAAPI_CPUSET_COUNT(arg->procs) == 0) {
        arg->checked = 0;
        *arg->procs = team->node_bitmap[nextnuma];
      }

      /* Check numa place first*/
      if (!arg->checked) {
        arg->checked = 1;
        victim->ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[nextnuma];
        kaapi_assert_debug(victim->ld);
        /*Clear the node we just visited*/
        if (victim->ld == 0) return EINVAL;
        victim->ident = numaid;
        return 0;
      }
      /*Check remaining procs*/
      int victimid = KAAPI_CPUSET_FIRSTONE(arg->procs) - 1;
      kaapi_assert_debug(victimid >= 0);
      kaapi_processor_t *victim_proc = team->all_kprocessors[victimid];
      if (victim_proc == 0) return EINVAL;
      victim->ld = victim_proc->rsrc.ownplace;
      victim->ident = victimid;
      KAAPI_CPUSET_CLR(victimid, arg->procs);

      if (KAAPI_CPUSET_COUNT(arg->procs) == 0)
        KAAPI_CPUSET_CLR(nextnuma, arg->nodes);

      return 0;

      break;
    }
    case KAAPI_STEAL_SUCCESS:
    {
      arg->checked = 0;
      *arg->procs = team->node_bitmap[numaid];
      *arg->nodes = team->numa_bitmap;
      break;
    }
    case KAAPI_STEAL_FAILED:
    {
      break;
    }

    default:
      break;
  }

  return 0;
}
#endif


#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
/** Do hierarchical selection up to a threshold level
*/
typedef struct kaapi_victim_hws_P_arg_t {
  int numachecked;
  int rand_index;
  kaapi_cpuset_t *procs;
  kaapi_cpuset_t *nodes;
} kaapi_victim_hws_P_arg_t;

int kaapi_sched_select_victim_hierarchy_kproc(
    kaapi_processor_t*       kproc,
    kaapi_victim_t*          victim,
    kaapi_selecvictim_flag_t flag
)
{
  kaapi_victim_hws_P_arg_t* arg = (kaapi_victim_hws_P_arg_t*)kproc->arg_selecfn;

  kaapi_team_t* team = kproc->team;
  int numaid = kproc->rsrc.numaid;
  if (arg->procs == 0) {
    //allocate on stack the cpuset of the node
    arg->procs = kaapi_processor_alloc_data(kproc, sizeof(kaapi_cpuset_t));
    kaapi_assert_debug(arg->procs);
    *arg->procs = team->node_bitmap[numaid];
  }
  if (arg->nodes == 0) {
    //allocate on stack the cpuset of the numa nodes
    arg->nodes = kaapi_processor_alloc_data(kproc, sizeof(kaapi_cpuset_t));
    kaapi_assert_debug(arg->nodes);
    *arg->nodes = team->numa_bitmap;
    //Our node is preloaded on initialization
    KAAPI_CPUSET_CLR(numaid, arg->nodes);
  }
  switch (flag)
  {
    case KAAPI_SELECT_VICTIM:
    {
        if (team->count <=1)
          return EINVAL;
        /*Check if we ran out of proc to steal on this node*/
        if (KAAPI_CPUSET_COUNT(arg->procs) == 0) {
          if (!arg->numachecked) {
            /*Select from NUMA and return, don't reinit anything*/
            arg->numachecked = 1;
            arg->rand_index = rand_r(&(kproc->rsrc.seed)) % (team->numaplaces.count);
            victim->ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[numaid];
            kaapi_assert_debug(victim->ld);
            if (victim->ld == 0) return EINVAL;
            victim->ident = numaid;
            return 0;
          }
          /*Else reset to next node or to full machine*/
          int nextnode = next_elem(arg->nodes, arg->rand_index);
          if (nextnode >= 0) {
            KAAPI_CPUSET_CLR(nextnode, arg->nodes);
            *arg->procs = team->node_bitmap[nextnode];
          } else {
            arg->numachecked = 0;
            *arg->procs = team->node_bitmap[numaid];
            *arg->nodes = team->numa_bitmap;
            KAAPI_CPUSET_CLR(numaid, arg->nodes);
          }
          kaapi_assert_debug(KAAPI_CPUSET_COUNT(arg->procs) > 0);
        }
        /*TODO start by looking our queue if possible ?*/

        int victimid = KAAPI_CPUSET_FIRSTONE(arg->procs) - 1;
        kaapi_assert_debug(victimid >= 0);
        kaapi_processor_t *victim_proc = team->all_kprocessors[victimid];
        if (victim_proc == 0) return EINVAL;
        victim->ld = victim_proc->rsrc.ownplace;
        victim->ident = victimid;
        KAAPI_CPUSET_CLR(victimid, arg->procs);
      break;
    }
    case KAAPI_STEAL_SUCCESS:
    {
      arg->numachecked = 0;
      *arg->procs = team->node_bitmap[numaid];
      *arg->nodes = team->numa_bitmap;
      break;
    }
    case KAAPI_STEAL_FAILED:
    {
      break;
    }

    default:
      break;
  }

  return 0;
}

typedef struct kaapi_victim_hws_N_arg_t {
  int rand_index;
  kaapi_cpuset_t *procs;
  kaapi_cpuset_t *nodes;
} kaapi_victim_hws_N_arg_t;

int kaapi_sched_select_victim_hierarchy_numa(
    kaapi_processor_t*       kproc,
    kaapi_victim_t*          victim,
    kaapi_selecvictim_flag_t flag
)
{
  kaapi_victim_hws_N_arg_t* arg = (kaapi_victim_hws_N_arg_t*)kproc->arg_selecfn;

  kaapi_team_t* team = kproc->team;
  int numaid = kproc->rsrc.numaid;
  if (arg->procs == 0) {
    //allocate on stack the cpuset of the node
    arg->procs = kaapi_processor_alloc_data(kproc, sizeof(kaapi_cpuset_t));
    kaapi_assert_debug(arg->procs);
    *arg->procs = team->node_bitmap[numaid];
  }
  if (arg->nodes == 0) {
    //allocate on stack the cpuset of the numa nodes
    arg->nodes = kaapi_processor_alloc_data(kproc, sizeof(kaapi_cpuset_t));
    kaapi_assert_debug(arg->nodes);
    *arg->nodes = team->numa_bitmap;
  }
  switch (flag)
  {
    case KAAPI_SELECT_VICTIM:
    {
        if (team->count <=1)
          return EINVAL;
        /*Determine next node*/
        int nextnode = numaid;
        if (!KAAPI_CPUSET_ISSET(numaid, arg->nodes)) {
          nextnode = next_elem(arg->nodes, arg->rand_index);
        } else {
          /*Initialize random index for next step*/
          arg->rand_index = rand_r(&(kproc->rsrc.seed)) % (team->numaplaces.count);
        }

        /*Handle end of everything*/
        if (nextnode < 0) {
          *arg->procs = team->node_bitmap[numaid];
          *arg->nodes = team->numa_bitmap;
          nextnode = numaid;
        }

        if (KAAPI_CPUSET_COUNT(arg->procs) != 0) {
          /*Check from kprocs*/
          int victimid = KAAPI_CPUSET_FIRSTONE(arg->procs) - 1;
          kaapi_assert_debug(victimid >= 0);
          kaapi_processor_t *victim_proc = team->all_kprocessors[victimid];
          if (victim_proc == 0) return EINVAL;
          victim->ld = victim_proc->rsrc.ownplace;
          victim->ident = victimid;
          KAAPI_CPUSET_CLR(victimid, arg->procs);
          return 0;
        }

        /*Else check next node*/
        victim->ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[nextnode];
        if (victim->ld == 0) return EINVAL;
        victim->ident = numaid;
        /*Clear the node we just visited*/
        KAAPI_CPUSET_CLR(nextnode, arg->nodes);
        return 0;
      break;
    }
    case KAAPI_STEAL_SUCCESS:
    {
      *arg->procs = team->node_bitmap[numaid];
      *arg->nodes = team->numa_bitmap;
      break;
    }
    case KAAPI_STEAL_FAILED:
    {
      break;
    }

    default:
      break;
  }

  return 0;
}
#endif


/** Do local (intra numa node) steal, then other numa node
*/
typedef struct kaapi_victim_numa_arg_t {
  int level;
  int count;
  int delay;
} kaapi_victim_numa_arg_t;


//#define UNIQUE

int kaapi_sched_select_victim_numa(
    kaapi_processor_t*       kproc, 
    kaapi_victim_t*          victim,
    kaapi_selecvictim_flag_t flag 
)
{
  kaapi_victim_numa_arg_t* arg = (kaapi_victim_numa_arg_t*)kproc->arg_selecfn;
  kaapi_place_t* ld;

  switch (flag)
  {
    case KAAPI_SELECT_VICTIM:
    {
      if (arg->level ==0) 
        arg->level = KAAPI_HWS_LEVELID_NUMA;

      /* only one thread at once could emits steal request to inter-numa node */
      ld = kaapi_default_param.map2affset[kproc->rsrc.kid].affset[arg->level];
#ifdef UNIQUE
      if ((arg->level == KAAPI_HWS_LEVELID_MACHINE) && !kaapi_atomic_trylock(&ld->lock_up))
        arg->level = KAAPI_HWS_LEVELID_NUMA;
#endif
      ld = kaapi_default_param.map2affset[kproc->rsrc.kid].affset[arg->level];
      int count = kaapi_default_param.affinity_set[arg->level].count;
      int victimid = rand_r( (unsigned int*)&kproc->rsrc.seed ) % count;
      victim->ld = kaapi_default_param.places_set[arg->level].places[victimid];
      if (victim->ld ==0) return EINVAL;
      victim->ident = victimid;
      return 0;
    } break;

    case KAAPI_STEAL_SUCCESS:
    {
#ifdef UNIQUE
      if (arg->level == KAAPI_HWS_LEVELID_MACHINE)
      {
        ld = kproc->lds[arg->level];
        kaapi_atomic_unlock(&ld->lock_up);
        arg->level = KAAPI_HWS_LEVELID_NUMA;
      }
#endif
      arg->level = KAAPI_HWS_LEVELID_NUMA;
      KAAPI_STEAL_SUCCESS_INST(arg);
    } break;

    case KAAPI_STEAL_FAILED:
    {
      ld = kaapi_default_param.map2affset[kproc->rsrc.kid].affset[KAAPI_HWS_LEVELID_NUMA];
#ifdef UNIQUE
      if (arg->level == KAAPI_HWS_LEVELID_MACHINE)
        kaapi_atomic_unlock(&ld->lock_up);
#endif
#if 0
      KAAPI_STEAL_FAILED_INST(arg);
      if (arg->count >=  16*ld->count) 
      {
        if (arg->level == KAAPI_HWS_LEVELID_NUMA)
          arg->level = KAAPI_HWS_LEVELID_MACHINE;
      }
#endif
    } break;

    default:
      break;
  }

  return 0;
}


#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
/** Do hierarchical selection on all numa levels using distance information
*/
typedef struct numa_node_t {
  int distance;
  int peer;
} numa_node_t;

static int lessthan_node( const void* n1, const void* n2)
{
  const numa_node_t* node1 = (const numa_node_t*) n1;
  const numa_node_t* node2 = (const numa_node_t*) n2;
  if (node1->distance < node2->distance)
    return -1;
  else if (node1->distance == node2->distance)
    return 0;
  return 1;
}

typedef struct kaapi_victim_hws_distance_t {
  int which;
  int node_index;
  int rand_index;
  int count;
  int peer;
  unsigned short xsubi[3];
  numa_node_t* numa_distance;
  kaapi_cpuset_t* procs;
} kaapi_victim_hws_distance_t;

int kaapi_sched_select_victim_hierarchy_distance_numa(
    kaapi_processor_t*       kproc,
    kaapi_victim_t*          victim,
    kaapi_selecvictim_flag_t flag
)
{
  int i;
  kaapi_victim_hws_distance_t* arg = (kaapi_victim_hws_distance_t*)kproc->arg_selecfn[0];

  kaapi_team_t* team = kproc->team;
  int numaid = kproc->rsrc.numaid;
  int numa_count = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].count;
  if (arg == 0)
  {
    //allocate on stack the cpuset of the node
    arg = kaapi_processor_alloc_data(kproc, sizeof(kaapi_victim_hws_distance_t));
    kaapi_assert_debug(arg);
    arg->which = 0;
    arg->node_index = 0;
    arg->rand_index = rand_r(&(kproc->rsrc.seed)) % numa_count;
    arg->count = 0;
    arg->xsubi[0] = (short) rand_r( &kproc->rsrc.seed );
    arg->xsubi[1] = (short) rand_r( &kproc->rsrc.seed );
    arg->xsubi[2] = (short) rand_r( &kproc->rsrc.seed );
    arg->numa_distance = kaapi_processor_alloc_data(kproc, sizeof(numa_node_t)*numa_count);
    kaapi_assert_debug(arg->numa_distance);
    arg->procs = kaapi_processor_alloc_data(kproc, sizeof(kaapi_cpuset_t));
    kaapi_assert_debug(arg->procs);
    *arg->procs = team->node_bitmap[numaid];
    for (i=0; i<numa_count; ++i)
    {
      arg->numa_distance[i].peer = i;
      arg->numa_distance[i].distance = numa_distance( numaid, i);
    }
    qsort( arg->numa_distance, numa_count, sizeof(numa_node_t), &lessthan_node );
#if 0
kaapi_dbg_print_lock();
printf(">>>%i:: ", numaid );
 for (i=0; i<numa_count; ++i)
  printf("(d%i, n:%i)", arg->numa_distance[i].distance, arg->numa_distance[i].peer );
printf("\n");
kaapi_dbg_print_unlock();
#endif
    kproc->arg_selecfn[0] = (uintptr_t)arg;
  }
redo:
  switch (flag)
  {
    case KAAPI_SELECT_VICTIM:
    {
        switch (arg->which)
        {
          case 0:
            if (team->count <1)
              return EINVAL;

            victim->ld = kproc->rsrc.ownplace;
            victim->ident = kproc->rsrc.kid;
          return 0;

          case 1: { /* random select numa node using distance */

            double p;
#if 0
            double lambda = 0.17;
            do {
              double r = erand48(arg->xsubi);
              if (r == 0.0) r = FLT_MIN;
              p = -log(r)/lambda;
            } while (p >=numa_count);
#else
            p = ++arg->peer;
            if (arg->peer >= numa_count)
              p = arg->peer = 0;
#endif
            arg->node_index = arg->numa_distance[(int)p].peer;
#if 0
if (kproc->rsrc.kid == 115)
  printf("%i -> %i\n", numaid, arg->node_index);
#endif
            victim->ident = arg->node_index;
            victim->ld    = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[arg->node_index];
            return 0;
          }

          case 2: { /* select kproc inside numa node */
            if (KAAPI_CPUSET_COUNT(arg->procs) ==0)
            {
              if (++arg->count > 64)
              {
                arg->count = 0;
                arg->which = 3;
              }
              else
                arg->which = 1;
              goto redo;
            }

            victim->ident = next_elem(arg->procs, arg->rand_index);
#if 0
if (kproc->rsrc.kid == 115)
  printf("kproc:%i \n", victim->ident );
#endif
            KAAPI_CPUSET_CLR( victim->ident, arg->procs );
            if (team->all_kprocessors[victim->ident] == 0) return EINVAL;
            victim->ld = team->all_kprocessors[victim->ident]->rsrc.ownplace;
            if (victim->ld ==0) return EINVAL;
            
            arg->rand_index = rand_r(&(kproc->rsrc.seed)) % numa_count;
            return 0;
          }
       
          case 3: {
            arg->node_index = rand_r( &kproc->rsrc.seed ) % team->numaplaces.count;
            victim->ident = arg->node_index;
            victim->ld    = team->numaplaces.places[victim->ident];
            if (victim->ld ==0) return EINVAL;

            *arg->procs = team->node_bitmap[arg->node_index];
            arg->which = 2;
            return 0;
          }
        }

        /*Check remaining procs*/
        int victimid = KAAPI_CPUSET_FIRSTONE(arg->procs) - 1;
        kaapi_assert_debug(victimid >= 0);
        kaapi_processor_t *victim_proc = team->all_kprocessors[victimid];
        if (victim_proc == 0) return EINVAL;
        victim->ld = victim_proc->rsrc.ownplace;
        victim->ident = victimid;
        KAAPI_CPUSET_CLR(victimid, arg->procs);

        return 0;

      break;
    }
    case KAAPI_STEAL_SUCCESS:
    {
      arg->which = 0;
      break;
    }
    case KAAPI_STEAL_FAILED:
    {
      switch (arg->which) {
        case 0:
          arg->which = 2;
          arg->peer  = 0;
          arg->node_index = arg->numa_distance[arg->peer].peer;
          *arg->procs = team->node_bitmap[arg->node_index];
        break;
        case 1:
          arg->which = 2;
          *arg->procs = team->node_bitmap[arg->node_index];
        break;
        case 2:
        break;
      }
      break;
    }

    default:
      break;
  }

  return 0;
}
#endif
