/*
** xkaapi
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/

#include "kaapi_impl.h"

#define KAAPI_REORDER_LIST 1

/* ========================================================================= */
/* This is an implementation of the CC_Sync algorithm                        */
/* ========================================================================= */


/** This is the new implementation of the combining steal request as proposed by Fatourou, CC_sync
    algorithm. The tail is stored in the team structure.
*/
typedef struct kaapi_request_node_t {
  kaapi_request_t              req;
  struct kaapi_request_node_t* volatile next;
  int volatile                 wait;
  int volatile                 completed;
  struct kaapi_request_node_t* volatile nextNode; /* used to pass data between post and commit */
  struct kaapi_request_node_t* pgo_next;
} kaapi_request_node_t __attribute__((aligned(KAAPI_CACHE_LINE)));


/* structure to retreive request object */
typedef struct kaapi_listrequest_t {
  struct kaapi_request_node_t* tail;
} kaapi_listrequest_t __attribute__((aligned (KAAPI_CACHE_LINE)));


/* The most important structure to iterate over the list of requests
*/
typedef struct kaapi_listrequest_iterator_t {
  int                                 count;
  kaapi_request_node_t**              curr;
} kaapi_listrequest_iterator_t;


/* Steal context: no more than a list of requests 
*/
typedef struct kaapi_stealcontext_t {
  struct kaapi_request_node_t* tail;
#if defined(KAAPI_DEBUG)
  kaapi_ressource_t*           owner; /* the combiner kprocessor or 0 */
#endif
} kaapi_stealcontext_t;


/** Per thread steal context
*/
__thread kaapi_request_node_t* _kaapi_sealcontext_key = 0;


/*
*/
int kaapi_request_ccsync_reply
( 
  kaapi_request_t*        request,
  int                     value
)
{
  request->header.status = value;
  kaapi_request_node_t* node = (kaapi_request_node_t*)request;
  node->completed = 1;
  node->wait      = 0;
  return 0;
}


/* return !=0 iff the range is empty
*/
int kaapi_listrequest_ccsync_iterator_empty(
  kaapi_listrequest_iterator_t* lri
)
{ return (lri->count ==0); }


/* get the first request of the range. range iterator should have been initialized by kaapi_listrequest_iterator_init 
*/
kaapi_steal_request_t* kaapi_listrequest_ccsync_iterator_get(
  kaapi_listrequest_iterator_t* lri
)
{ 
  return (kaapi_steal_request_t*)*lri->curr;
}


/* return the next entry in the request. return 0 if the range is empty.
*/
kaapi_steal_request_t* kaapi_listrequest_ccsync_iterator_next(
  kaapi_listrequest_iterator_t* lri
)
{
  if (lri->count <=0) return 0;
  --lri->count;
  ++lri->curr;
  return (kaapi_steal_request_t*)*lri->curr;
}


/*
*/
int kaapi_listrequest_ccsync_iterator_count(
  kaapi_listrequest_iterator_t* lri
)
{ return lri->count; }


#define NANOSLEEP 0
#if NANOSLEEP
#if !defined(_XOPEN_SOURCE)
#  define _XOPEN_SOURCE 600
#endif
#include <time.h>
#endif


/* no concurrency here: always called before starting threads */
int kaapi_sched_ccsync_emitsteal_init(kaapi_stealcontext_t** sc)
{
  kaapi_stealcontext_t* ctxt;
  ctxt = (kaapi_stealcontext_t*)malloc(sizeof(kaapi_stealcontext_t));
  if (ctxt ==0) return ENOMEM;
  /* first non blocked node ! */
  kaapi_request_node_t* tail = (kaapi_request_node_t*)malloc( sizeof(kaapi_request_node_t) );
  tail->wait = 0;
  tail->completed = 0;
  tail->next = 0;
  ctxt->tail = tail;
#if defined(KAAPI_DEBUG)
  ctxt->owner = 0;
#endif
  *sc = ctxt;
  return 0;
}


/* no concurrency here: always called before starting threads 
*/
int kaapi_sched_ccsync_emitsteal_dstor(struct kaapi_stealcontext_t* sc)
{
  kaapi_stealcontext_t* ctxt = (kaapi_stealcontext_t*)sc;
  free(ctxt->tail);
  if (ctxt->tail->next !=0) return EBUSY;
  ctxt->tail = 0;
  return 0;
}


/*
*/
kaapi_request_t* kaapi_sched_ccsync_post_request (
  kaapi_ressource_t* rsrc,
  kaapi_place_t*     ld
)
{
  kaapi_request_node_t* currNode;
  kaapi_request_node_t* nextNode;
  kaapi_stealcontext_t* victim_stealctxt
    = (kaapi_stealcontext_t*)ld->steal_ctxt;

  nextNode = _kaapi_sealcontext_key;
  if (nextNode ==0)
    nextNode = malloc( sizeof(kaapi_request_node_t));
  else
    _kaapi_sealcontext_key = nextNode->pgo_next;

  nextNode->next = 0;
  nextNode->wait = 1;
  nextNode->completed = 0;
  nextNode->pgo_next            = 0;
  nextNode->req.header.ident    = -1;
  nextNode->req.header.mask_arch= 0;
  nextNode->req.header.ld       = 0;
  nextNode->req.header.status   = KAAPI_REQUEST_S_INIT;

  /* on return currNode contains the old value of tail */
  currNode = nextNode;
  KAAPI_ATOMIC_EXCHANGE(currNode, victim_stealctxt->tail);
  currNode->nextNode = nextNode;
  currNode->pgo_next = 0;
  /* do not forget to have this information set before stealing ! */
  currNode->req.header.mask_arch = (1U << rsrc->proc_type);
  currNode->req.header.ld        = ld;
  currNode->req.header.status    = KAAPI_REQUEST_S_POSTED;
  return &currNode->req;
}


/*
*/
#define KAAPI_MAXREQUEST_COUNT 256
int kaapi_sched_ccsync_commit_request( kaapi_place_t* ld, kaapi_request_t* req )
{
  kaapi_request_node_t* currNode = (kaapi_request_node_t*)req;
  currNode->next = currNode->nextNode;
  kaapi_assert( req->header.ld == ld );

  /* link new node (currNode) here, after all request fields are setup */
#if defined(KAAPI_DEBUG)
  kaapi_stealcontext_t* victim_stealctxt
    = (kaapi_stealcontext_t*)ld->steal_ctxt;
#endif

  /* to avoid deadlock with recursive call to shared object methods */
  kaapi_ressource_t* rsrc = kaapi_self_rsrc();

#if defined (KAAPI_USE_PERFCOUNTER)
  KAAPI_EVENT_PUSH1(rsrc->evtkproc, 0, KAAPI_EVT_STEAL_AGGR_BEG, (uintptr_t)ld );
#endif

  /* (2) In case of aggregation, lock after thief has posted its request.
     lock and re-test if they are yet posted requests on victim or not 
     if during tentaive of locking, a reply occurs, then return with reply
  */
  while (currNode->wait == 1)
  {
#  if defined(KAAPI_USE_NETWORK)
    kaapi_network_poll();
#  endif
#if NANOSLEEP
    struct timespec ts = { 0, 10 }; 
    nanosleep( &ts, 0 ); 
#endif
    kaapi_slowdown_cpu();
  }

  if (currNode->completed)
  {
#if defined (KAAPI_USE_PERFCOUNTER)
    KAAPI_EVENT_PUSH1(rsrc->evtkproc, 0, KAAPI_EVT_STEAL_AGGR_END, (uintptr_t)ld );
#endif
    currNode->pgo_next = _kaapi_sealcontext_key;
    _kaapi_sealcontext_key = currNode;
    return currNode->req.header.status;
  }

  /* become combiner: iterates over all requests and process first push, then pop, then steal
  */

#if defined(KAAPI_DEBUG)
  /* update the combiner thread : debug */
  victim_stealctxt->owner = rsrc;
#endif

#if KAAPI_REORDER_LIST
  int i;
  kaapi_request_node_t* popList[KAAPI_MAXREQUEST_COUNT];
  kaapi_request_node_t* stealList[KAAPI_MAXREQUEST_COUNT];
  kaapi_request_node_t* stealinList[KAAPI_MAXREQUEST_COUNT];
  kaapi_request_node_t* extraList[KAAPI_MAXREQUEST_COUNT];
  int count_pop  = 0;
  int count_steal = 0;
  int count_steal_in = 0;
  int count_extra = 0;
#endif
  kaapi_request_node_t* tmpNode = currNode;

  int count = KAAPI_MAXREQUEST_COUNT;
  while (1)
  {
    kaapi_request_node_t* tmpNodeNext = tmpNode->next;
    kaapi_request_t* req = &tmpNode->req;
    kaapi_request_op_t op = req->header.op;
    switch (op)
    {
      case KAAPI_REQUEST_OP_PUSH:
      {
        if (0 == ld->vtable->fs_push(ld, req->push_a.task))
          req->header.status = KAAPI_REQUEST_S_OK;
        else
          req->header.status = KAAPI_REQUEST_S_NOK;

        tmpNode->completed = 1;
        kaapi_writemem_barrier();
        tmpNode->wait = 0;
      } break;

      case KAAPI_REQUEST_OP_PUSH_REMOTE:
      {
        if (0 == ld->vtable->fs_push_remote(ld, req->push_a.task))
          req->header.status = KAAPI_REQUEST_S_OK;
        else
          req->header.status = KAAPI_REQUEST_S_NOK;

        tmpNode->completed = 1;
        kaapi_writemem_barrier();
        tmpNode->wait = 0;
      } break;

      case KAAPI_REQUEST_OP_PUSHLIST:
      {
        if (0 == ld->vtable->fs_pushlist(ld, req->push_l.list))
          req->header.status = KAAPI_REQUEST_S_OK;
        else
          req->header.status = KAAPI_REQUEST_S_NOK;

        tmpNode->completed = 1;
        kaapi_writemem_barrier();
        tmpNode->wait = 0;
      } break;

#if KAAPI_REORDER_LIST
#warning "Reorder"
      case KAAPI_REQUEST_OP_POP:
      {
        kaapi_assert_debug( count_pop < KAAPI_MAXREQUEST_COUNT );
        popList[count_pop++] = tmpNode;
      } break;

      case KAAPI_REQUEST_OP_STEAL:
      {
        kaapi_assert_debug( count_steal < KAAPI_MAXREQUEST_COUNT );
        stealList[count_steal++] = tmpNode;
      } break;

      case KAAPI_REQUEST_OP_STEAL_INPLACE:
      {
        kaapi_assert_debug( count_steal_in < KAAPI_MAXREQUEST_COUNT );
        stealinList[count_steal_in++] = tmpNode;
      } break;

      case KAAPI_REQUEST_OP_EXTRA:
      {
        kaapi_assert_debug( count_extra < KAAPI_MAXREQUEST_COUNT );
        extraList[count_extra++] = tmpNode;
      } break;
#else
#warning "No reorder"
      case KAAPI_REQUEST_OP_POP:
      {
        kaapi_task_t* task = ld->vtable->fs_pop(ld);
        if (task !=0)
        {
          tmpNode->req.header.status = KAAPI_REQUEST_S_OK;
          tmpNode->req.pop_a.task =  task;
        }
        else
          tmpNode->req.header.status = KAAPI_REQUEST_S_NOK;
        tmpNode->completed = 1;
        kaapi_writemem_barrier();
        tmpNode->wait = 0;
      } break;

      case KAAPI_REQUEST_OP_STEAL:
      {
        kaapi_listrequest_iterator_t lri;
        lri.curr          = &tmpNode;
        lri.count         = 1;
        ld->vtable->fs_steal( ld, &lri );
        /* reply failed for all remains request */
        if (lri.count!=0)
        {
          tmpNode->req.header.status = KAAPI_REQUEST_S_NOK;
          tmpNode->completed = 1;
          kaapi_writemem_barrier();
          tmpNode->wait = 0;
        }

      } break;

      case KAAPI_REQUEST_OP_STEAL_INPLACE:
      {
        kaapi_listrequest_iterator_t lri;
        lri.curr          = &tmpNode;
        lri.count         = 1;
        ld->vtable->fs_stealin( ld, &lri );
        /* reply failed for all remains request */
        if (lri.count!=0)
        {
          tmpNode->req.header.status = KAAPI_REQUEST_S_NOK;
          tmpNode->completed = 1;
          kaapi_writemem_barrier();
          tmpNode->wait = 0;
        }

      } break;

      case KAAPI_REQUEST_OP_EXTRA:
      {
        tmpNode->req.extra_a.retval = tmpNode->req.extra_a.func( ld, tmpNode->req.extra_a.arg );
        tmpNode->req.header.status = KAAPI_REQUEST_S_OK;
        tmpNode->completed = 1;
        kaapi_writemem_barrier();
        tmpNode->wait = 0;
      } break;
#endif
      default:
        kaapi_assert(0);
    }
    
    tmpNode = tmpNodeNext;
    if (tmpNode->next ==0) break;
    if (--count ==0) 
      break;
  }
//if (KAAPI_MAXREQUEST_COUNT-count >1)
//  printf("Aggregation: %i\n", KAAPI_MAXREQUEST_COUNT-count);



#if KAAPI_REORDER_LIST
  /* process extra type request before others */
  for (i=0; i<count_extra; ++i)
  {
    kaapi_request_node_t* tmp = extraList[i];
    kaapi_assert_debug( tmp->req.header.op == KAAPI_REQUEST_OP_EXTRA);
    tmp->req.extra_a.retval = tmp->req.extra_a.func( ld, tmp->req.extra_a.arg );
    tmp->req.header.status = KAAPI_REQUEST_S_OK;
    tmp->completed = 1;
    kaapi_writemem_barrier();
    tmp->wait = 0;
  }

  /* process pop request before steal request */
  for (i=0; i<count_pop; ++i)
  {
    kaapi_request_node_t* tmp = popList[i];
    kaapi_assert_debug( tmp->req.header.op == KAAPI_REQUEST_OP_POP);
    kaapi_task_t* task = ld->vtable->fs_pop(ld);
    if (task !=0)
    {
      tmp->req.header.status = KAAPI_REQUEST_S_OK;
      tmp->req.pop_a.task =  task;
    }
    else
      tmp->req.header.status = KAAPI_REQUEST_S_NOK;
    tmp->completed = 1;
    kaapi_writemem_barrier();
    tmp->wait = 0;
  }

  /* process steal not implace request before steal request */
  if (count_steal !=0)
  {
    kaapi_listrequest_iterator_t lri;
    lri.curr          = stealList;
    lri.count         = count_steal;
    ld->vtable->fs_steal( ld, &lri );

    /* reply failed for all remains request */
    for (i=(count_steal-lri.count); i<count_steal; ++i)
    {
      kaapi_request_node_t* tmp = stealList[i];
      kaapi_assert_debug( tmp->req.header.op == KAAPI_REQUEST_OP_STEAL);
      tmp->req.header.status = KAAPI_REQUEST_S_NOK;
      tmp->completed = 1;
      kaapi_writemem_barrier();
      tmp->wait = 0;
    }
  }

  /* process steal request at the end */
  if (count_steal_in != 0)
  {
    kaapi_listrequest_iterator_t lri;
    lri.curr          = stealinList;
    lri.count         = count_steal_in;
    ld->vtable->fs_stealin( ld, &lri );

    /* reply failed for all remains request */
    for (i=(count_steal_in-lri.count); i<count_steal_in; ++i)
    {
      kaapi_request_node_t* tmp = stealinList[i];
      kaapi_assert_debug( tmp->req.header.op == KAAPI_REQUEST_OP_STEAL_INPLACE);
      tmp->req.header.status = KAAPI_REQUEST_S_NOK;
      tmp->completed = 1;
      kaapi_writemem_barrier();
      tmp->wait = 0;
    }
  }
#endif

  /* update the combiner thread */
#if defined(KAAPI_DEBUG)
  victim_stealctxt->owner = 0;
#endif
  kaapi_writemem_barrier();
  tmpNode->wait = 0;

  /* reinsert currNode */
  int status = currNode->req.header.status;
  currNode->pgo_next = _kaapi_sealcontext_key;
  _kaapi_sealcontext_key = currNode;

#if defined (KAAPI_USE_PERFCOUNTER)
  KAAPI_EVENT_PUSH1(rsrc->evtkproc, 0, KAAPI_EVT_STEAL_AGGR_END, (uintptr_t)ld );
#endif

  return status;
}


/* asynchronous versions
*/
int kaapi_sched_ccsync_commit_request_async(
  struct kaapi_place_group_operation_t* kpgo,
  kaapi_place_t* ld,
  kaapi_request_t* req
)
{
  /* link new node (currNode) here, after all request fields are setup: make them visible to the
     rest of the list
  */
  kaapi_request_node_t* currNode = (kaapi_request_node_t*)req;
  currNode->next = currNode->nextNode;

  /* test if completed: return */
  if (!currNode->wait && currNode->completed)
  {
    currNode->pgo_next = _kaapi_sealcontext_key;
    _kaapi_sealcontext_key = currNode;
    return currNode->req.header.status;
  }

  /* push back request in the kpgo's list */
  currNode->pgo_next = 0;
  if (kpgo->tail)
    kpgo->tail->pgo_next = currNode;
  else
    kpgo->head = currNode;
  kpgo->tail = currNode;

  return KAAPI_REQUEST_S_POSTED;
}


/*
*/
int kaapi_sched_ccsync_pgo_init( kaapi_place_group_operation_t* kpgo )
{
  kpgo->head = kpgo->tail = 0;
  return 0;
}

/*
*/
int kaapi_sched_ccsync_pgo_wait( kaapi_place_group_operation_t* kpgo )
{
  kaapi_request_node_t* req_first = kpgo->head;
  while (req_first !=0)
  {
    kaapi_request_node_t* next = req_first->pgo_next;
    kaapi_sched_ccsync_commit_request( req_first->req.header.ld, &req_first->req );
    req_first = next;
  }
  kpgo->tail = kpgo->head = 0;
  return 0;
}

/*
*/
int kaapi_sched_ccsync_pgo_fini( kaapi_place_group_operation_t* kpgo )
{
  kaapi_assert_debug( kpgo->head ==  0);
  kaapi_assert_debug( kpgo->tail ==  0);
  return 0;
}



