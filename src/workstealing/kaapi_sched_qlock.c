/*
** xkaapi
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/

#include "kaapi_impl.h"
#include "kaapi_wsprotocol.h"

/* ========================================================================= */
/* This is an implementation of the queue algorithm using lock to serialize  */
/* access to the queues                                                      */
/* ========================================================================= */


/* Steal context: no more than a list of requests 
*/
typedef struct kaapi_stealcontext_t {
  kaapi_lock_t                 lock;
  kaapi_request_t*             req;
  kaapi_request_t*             free;
#if defined(KAAPI_DEBUG)
  kaapi_ressource_t*           owner; /* the combiner kprocessor or 0 */
#endif
} kaapi_stealcontext_t;

/* view iterator as pointer to the stealcontext */
typedef kaapi_stealcontext_t kaapi_listrequest_iterator_t;

/*
*/
int kaapi_request_qlock_reply
( 
  kaapi_request_t*        request,
  int                     value
)
{
  request->header.status = value;
  return 0;
}


/* return !=0 iff the range is empty
*/
int kaapi_listrequest_qlock_iterator_empty(
  struct kaapi_listrequest_iterator_t* lri
)
{ return ((kaapi_listrequest_iterator_t*)lri)->req ==0; }


/* get the first request of the range. range iterator should have been initialized by kaapi_listrequest_iterator_init 
*/
kaapi_steal_request_t* kaapi_listrequest_qlock_iterator_get(
  struct kaapi_listrequest_iterator_t* lri
)
{ 
  return (kaapi_steal_request_t*)((kaapi_listrequest_iterator_t*)lri)->req;
}


/* return the next entry in the request. return 0 if the range is empty.
*/
kaapi_steal_request_t* kaapi_listrequest_qlock_iterator_next(
  struct kaapi_listrequest_iterator_t* lri
)
{
  ((kaapi_listrequest_iterator_t*)lri)->req = 0;
  return 0;
}


/*
*/
int kaapi_listrequest_qlock_iterator_count(
  struct kaapi_listrequest_iterator_t* lri
)
{ return ((kaapi_listrequest_iterator_t*)lri)->req ==0 ? 0 : 1; }


#define NANOSLEEP 0
#if NANOSLEEP
#if !defined(_XOPEN_SOURCE)
#  define _XOPEN_SOURCE 600
#endif
#include <time.h>
#endif


/* no concurrency here: always called before starting threads */
int kaapi_sched_qlock_emitsteal_init(kaapi_stealcontext_t** sc)
{
  kaapi_stealcontext_t* ctxt;
  ctxt = (kaapi_stealcontext_t*)malloc(sizeof(kaapi_stealcontext_t));
  if (ctxt ==0) return ENOMEM;
  ctxt->req = 0;
  ctxt->free = 0;
  kaapi_atomic_initlock(&ctxt->lock);
#if defined(KAAPI_DEBUG)
  ctxt->owner = 0;
#endif
  *sc = ctxt;
  return 0;
}


/* no concurrency here: always called before starting threads */
int kaapi_sched_qlock_emitsteal_dstor(struct kaapi_stealcontext_t* sc)
{
  kaapi_stealcontext_t* ctxt = (kaapi_stealcontext_t*)sc;
  kaapi_atomic_destroylock(&ctxt->lock);
  return 0;
}


/*
*/
__thread kaapi_request_t* _ownrequest = 0;


/*
*/
kaapi_request_t* kaapi_sched_qlock_post_request (
  kaapi_ressource_t* rsrc,
  kaapi_place_t*     ld
)
{
  kaapi_stealcontext_t* victim_stealctxt
    = (kaapi_stealcontext_t*)ld->steal_ctxt;

  kaapi_atomic_lock( &victim_stealctxt->lock );
  kaapi_request_t* req = _ownrequest;
  if (req ==0)
    req = malloc( sizeof(kaapi_request_t));

  req->header.ident    = -1;
  req->header.mask_arch= 0;
  req->header.status = KAAPI_REQUEST_S_INIT;

  /* do not forget to have this information set before stealing ! */
  req->header.mask_arch = (1U << rsrc->proc_type);
#if defined(KAAPI_DEBUG)
  victim_stealctxt->req = req;
#endif
  return req;
}


/*
*/
int kaapi_sched_qlock_commit_request( kaapi_place_t* ld, kaapi_request_t* req )
{
  /* to avoid deadlock with recursive call to shared object methods */
  kaapi_stealcontext_t* victim_stealctxt
    = (kaapi_stealcontext_t*)ld->steal_ctxt;

  kaapi_assert_debug( req == victim_stealctxt->req );

  /* to avoid deadlock with recursive call to shared object methods */
  kaapi_ressource_t* rsrc = kaapi_self_rsrc();

#if defined (KAAPI_USE_PERFCOUNTER)
  KAAPI_EVENT_PUSH1(rsrc->evtkproc, 0, KAAPI_EVT_STEAL_AGGR_BEG, (uintptr_t)ld );
#endif

#if defined(KAAPI_DEBUG)
  /* update the combiner thread : debug */
  victim_stealctxt->owner = rsrc;
#endif

  kaapi_request_op_t op = req->header.op;
  switch (op)
  {
    case KAAPI_REQUEST_OP_PUSH:
    {
      if (0 == ld->vtable->fs_push(ld, req->push_a.task))
        req->header.status = KAAPI_REQUEST_S_OK;
      else
        req->header.status = KAAPI_REQUEST_S_NOK;
    } break;

    case KAAPI_REQUEST_OP_PUSH_REMOTE:
    {
      if (0 == ld->vtable->fs_push_remote(ld, req->push_a.task))
        req->header.status = KAAPI_REQUEST_S_OK;
      else
        req->header.status = KAAPI_REQUEST_S_NOK;
    } break;

    case KAAPI_REQUEST_OP_PUSHLIST:
    {
      if (0 == ld->vtable->fs_pushlist(ld, req->push_l.list))
        req->header.status = KAAPI_REQUEST_S_OK;
      else
        req->header.status = KAAPI_REQUEST_S_NOK;
    } break;

    case KAAPI_REQUEST_OP_POP:
    {
      kaapi_task_t* task = ld->vtable->fs_pop(ld);
      if (task !=0)
      {
        req->header.status = KAAPI_REQUEST_S_OK;
        req->pop_a.task =  task;
      }
    } break;

    case KAAPI_REQUEST_OP_STEAL:
    {
      ld->vtable->fs_steal(ld, (struct kaapi_listrequest_iterator_t*)victim_stealctxt);
    } break;

    case KAAPI_REQUEST_OP_STEAL_INPLACE:
    {
      ld->vtable->fs_stealin( ld, (struct kaapi_listrequest_iterator_t*)victim_stealctxt );
    } break;

    case KAAPI_REQUEST_OP_EXTRA:
    {
      req->extra_a.retval = req->extra_a.func( ld, req->extra_a.arg );
      req->header.status = KAAPI_REQUEST_S_OK;
    } break;

    default:
      kaapi_assert(0);
  }

  if (req->header.status == KAAPI_REQUEST_S_INIT)
    req->header.status = KAAPI_REQUEST_S_NOK;

  victim_stealctxt->req = 0;

#if defined(KAAPI_DEBUG)
  victim_stealctxt->owner = 0;
#endif
  kaapi_atomic_unlock( &victim_stealctxt->lock );
  _ownrequest = req;
#if defined (KAAPI_USE_PERFCOUNTER)
  KAAPI_EVENT_PUSH1(rsrc->evtkproc, 0, KAAPI_EVT_STEAL_AGGR_END, (uintptr_t)ld );
#endif
  return req->header.status;
}



