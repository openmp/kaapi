/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_SCHED_H_
#define _KAAPI_SCHED_H_ 1

/*
*/
typedef struct {
  const char* name;
  int (*entrypoint)(
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
  );
} kaapi_strategy_selectvictim_t;

/*
*/
typedef struct {
  const char* name;
  void (*init)();
  void (*entrypoint)(
      kaapi_ressource_t* rsrc,
      kaapi_frame_wrdlist_t*  frame_rd
  );
} kaapi_strategy_pushinit_t;


/*
*/
typedef struct {
  const char* name;
  void (*init)();
  int (*entrypoint)(
      kaapi_ressource_t* kpi,
      kaapi_place_t* local,
      kaapi_list_t* list
  );
} kaapi_strategy_push_t;


/* list of strategies. Ended by 0 entrypoint functions
*/
extern kaapi_strategy_selectvictim_t kaapi_default_selectvictim[];
extern kaapi_strategy_pushinit_t     kaapi_default_pushinit[];
extern kaapi_strategy_push_t         kaapi_default_push[];


/** \ingroup WS
    Select a victim for next steal request using uniform random selection over all cores.
*/
extern int kaapi_sched_select_victim_rand( 
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
);

extern int kaapi_sched_select_victim_global(
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
);


#if defined(KAAPI_USE_NUMA)
/** \ingroup WS
    Select a victim for next steal request using uniform random selection over all numa node.
*/
extern int kaapi_sched_select_victim_strict_push(
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
);

/** \ingroup WS
    Select a victim for next steal request using uniform random selection over all numa node.
*/
extern int kaapi_sched_select_victim_rand_numa(
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
);
#endif


#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
/** \ingroup WS
    Select victim using the memory hierarchy
*/
extern int kaapi_sched_select_victim_hierarchy_numa_kproc( 
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
);

/** \ingroup WS
    Select victim using the memory hierarchy
*/
extern int kaapi_sched_select_victim_hierarchy_kproc_numa( 
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
);

/** \ingroup WS
    Select victim using the memory hierarchy
*/
extern int kaapi_sched_select_victim_hierarchy_kproc( 
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
);
/** \ingroup WS
    Select victim using the memory hierarchy
*/
extern int kaapi_sched_select_victim_hierarchy_numa( 
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
);

extern int kaapi_sched_select_victim_hierarchy_distance_numa(
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
);
#endif


/** \ingroup WS
    Select victim using the numa memory hierarchy
*/
extern int kaapi_sched_select_victim_numa(
    kaapi_processor_t* kproc, 
    kaapi_victim_t* victim, 
    kaapi_selecvictim_flag_t flag 
);


extern void kaapi_sched_push_init_queue_wspush(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t*  frame_rd
);

extern void kaapi_sched_push_init_queue_global(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t*  frame_rd
);

extern void kaapi_sched_push_init_queue_cyclickproc(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t* frame_rd
);

extern void kaapi_sched_push_init_queue_randkproc(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t* frame_rd
);


#if defined(KAAPI_USE_NUMA)
extern void kaapi_sched_push_init_queue_cyclicnuma(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t*  frame_rd
);

extern void kaapi_sched_push_init_queue_cyclicnumastrict(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t*  frame_rd
);

extern void kaapi_sched_push_init_queue_randnuma(
    kaapi_ressource_t* rsrc,
    kaapi_frame_wrdlist_t* frame_rd
);
#endif


/*
*/
extern int kaapi_sched_select_queue_local(
    kaapi_ressource_t* kpi,
    kaapi_place_t* local,
    kaapi_list_t* list
);

/*
*/
extern int kaapi_sched_select_queue_global(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* local,
    kaapi_list_t* list
);

#if defined(KAAPI_USE_PERFCOUNTER)
/*
*/
extern int kaapi_sched_select_queue_etf(
    kaapi_ressource_t* kpi,
    kaapi_place_t* local,
    kaapi_list_t* list
);
#endif

#if defined(KAAPI_USE_NUMA)
/*
*/
extern int kaapi_sched_select_queue_localnuma(
    kaapi_ressource_t* kpi,
    kaapi_place_t* local,
    kaapi_list_t* list
);

/*
*/
extern int kaapi_sched_select_queue_Wnuma(
    kaapi_ressource_t* kpi,
    kaapi_place_t* local,
    kaapi_list_t* list
);
#endif


/*
*/
extern int kaapi_sched_select_queue_attr(
    kaapi_ressource_t* kpi,
    kaapi_place_t* local,
    kaapi_list_t* list
);

/*
*/
extern int kaapi_sched_select_queue_cyclic(
    kaapi_ressource_t* kpi,
    kaapi_place_t* local,
    kaapi_list_t* list
);


#if defined(KAAPI_USE_OFFLOAD)
/*
*/
extern int kaapi_sched_select_queue_offload_rand(
    kaapi_ressource_t* kpi,
    kaapi_place_t* local,
    kaapi_list_t* list
);

extern int kaapi_sched_select_queue_offload_data_aware(
    kaapi_ressource_t* kpi,
    kaapi_place_t* local,
    kaapi_list_t* list
);
#endif


/** \ingroup WS
    Enter in the infinite loop of trying to steal work.
    Never return from this function...
    If proc is null pointer, then the function allocate a new kaapi_processor_t and 
    assigns it to the current processor.
    This method may be called by 'host' current thread in order to become an executor thread.
    The method returns only in case of terminaison.
    Count is the number of scheduling loop until the function call returns.
    If count == 0 then == infinite loop.
*/
extern int kaapi_sched_idle (
    kaapi_processor_t* kproc,
    int (*fcondition)(void* ),
    void* arg_fcondition
);


/** \ingroup WS
    Suspend the current context due to unsatisfied condition and do stealing until the condition becomes true.
    The condition is evaluated as fcondition(arg_fcondition) and it is true (!=0) when the thread can continue
    its execution.
    \retval 0 in case of success
    \retval EINTR in case of termination detection
    \TODO reprendre specs
*/
extern int kaapi_sched_suspend (
    kaapi_processor_t* kproc, 
    int (*fcondition)(void* ), 
    void* arg_fcondition
);


//@{
/** \ingroup WS
    Called when reply is received to return to the caller.
*/
extern kaapi_request_status_t kaapi_sched_emitsteal_return (
    kaapi_processor_t*      kproc,
    kaapi_victim_t*         victim,
    kaapi_request_t*        self_request
);


/** \ingroup WS
    The method starts a work stealing operation and return the result of one steal request
    The kprocessor kproc should have its stack ready to receive a work after a steal request.
    If the stack returned is not 0, either it is equal to the stack of the processor or it may
    be different. In the first case, some work has been insert on the top of the stack. On the
    second case, a whole stack has been stolen. It is to the responsability of the caller
    to treat the both case.
    \retval KAAPI_REQUEST_S_NOK in case of failure of stealing something
    \retval KAAPI_REQUEST_S_OK in case of success of the steal operation
*/
extern kaapi_request_status_t kaapi_sched_emitsteal ( kaapi_processor_t* );

/** \ingroup WS
    The method initializes the information required for the flat emitsteal function.
    \retval 0 in case success
    \retval an error code
*/
extern int kaapi_sched_ccsync_emitsteal_init(struct kaapi_stealcontext_t**);
extern int kaapi_sched_qlock_emitsteal_init(struct kaapi_stealcontext_t**);

/** \ingroup WS
    The method destroys the information required for the flat emitsteal function.
    \retval 0 in case success
    \retval an error code
*/
extern int kaapi_sched_ccsync_emitsteal_dstor(struct kaapi_stealcontext_t*);
extern int kaapi_sched_qlock_emitsteal_dstor(struct kaapi_stealcontext_t*);
//@}

//@{
/*
*/
extern int kaapi_sched_ccsync_pgo_init( struct kaapi_place_group_operation_t* kpgo );

/*
*/
extern int kaapi_sched_ccsync_pgo_wait( struct kaapi_place_group_operation_t* kpgo );

/*
*/
extern int kaapi_sched_ccsync_pgo_fini( struct kaapi_place_group_operation_t* kpgo );
//@}

/** \ingroup WS
    Advance polling of request for the current running thread.
    If this method is called from an other running thread than proc,
    the behavious is unexpected.
    \param proc should be the current running thread
*/
extern int kaapi_sched_advance ( kaapi_processor_t* proc );

/** \ingroup WS
*/
extern int kaapi_task_computeready(
  kaapi_task_t*                task,
  const struct kaapi_format_t* task_fmt,
  unsigned int*                war_param,
  unsigned int*                cw_param,
  kaapi_hashmap_t*             map
);


/** \ingroup WS
*/
extern int kaapi_frame_computereadylist(
  kaapi_stack_t* stack,
  kaapi_frame_t* frame
);

/** \ingroup WS
  Activate the successors of the task 'task' and return a next task to execute if popflag !=0
*/
extern kaapi_task_t* kaapi_sched_activate_succ(
    kaapi_ressource_t* kpi,
    kaapi_task_t*          task,
    int                    popflag,
    kaapi_list_t*          readytasks
);

/** \ingroup WS
    The method starts a work stealing operation and return until a sucessfull steal
    operation or 0 if no work may be found.
    The kprocessor kproc should have its stack ready to receive a work after a steal request.
    If the stack returned is not 0, either it is equal to the stack of the processor or it may
    be different. In the first case, some work has been insert on the top of the stack. On the
    second case, a whole stack has been stolen. It is to the responsability of the caller
    to treat the both case.
    \retval 0 in case of not stolen work 
    \retval a pointer to a stack that is the result of one workstealing operation.
*/
extern int kaapi_sched_stealprocessor ( 
  kaapi_processor_t*                   kproc, 
  struct kaapi_listrequest_iterator_t* lrrange
);

/** TODO
*/
extern int kaapi_sched_copy_steal_task
(
  kaapi_task_t*           task,
  unsigned int            war_param,
  unsigned int            cw_param,
  kaapi_frame_wrdlist_t*  masterframe,
  kaapi_steal_request_t*  request
);


extern int kaapi_sched_copy_stolentask_onstack
(
  kaapi_thread_t*         thread,
  kaapi_task_t*           task,
  unsigned int            war_param,
  unsigned int            cw_param,
  kaapi_frame_wrdlist_t*  masterframe
);


#endif /* _KAAPI_SCHED_H_ */
