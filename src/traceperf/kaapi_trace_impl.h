/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_TRACE_H_
#define _KAAPI_TRACE_H_ 1

#include "kaapi_trace.h"

#if defined(__cplusplus)
extern "C" {
#endif

struct kaapi_team_t;

/** Collect and display trace */
extern void kaapi_collect_trace(struct kaapi_team_t* team);
extern void kaapi_end_collect_trace(void);
extern void kaapi_display_rawperf( FILE* file, const kaapi_thread_perfctr_t* counter );

/* ========================================================================= */

/* vector arithmetic over counters */
extern void kaapi_mt_perf_max_counters(
  const kaapi_perf_idset_t* idset,
  kaapi_perf_counter_t* result,
  const kaapi_perf_counter_t* operand
);
extern void kaapi_mt_perf_add_counters(
  const kaapi_perf_idset_t* idset,
  kaapi_perf_counter_t* result,
  const kaapi_perf_counter_t* operand
);
/* accumulate thread performance counters into counter.
*/
extern void kaapi_mt_perf_add_threadcounters(
  kaapi_thread_perfctr_t* counter,
  const kaapi_thread_perfctr_t* v
);
/* accumulate thread performance counters into counter, reset v counters
*/
extern void kaapi_mt_perf_addreset_threadcounters(
  kaapi_thread_perfctr_t* counter,
  kaapi_thread_perfctr_t* v
);
/* accumulated to global counter, the collected counter operand in user or sys mode
*/
extern void kaapi_mt_perf_global_add(
  const kaapi_thread_perfctr_t* accum
);
extern void kaapi_mt_perf_cpy_counters(
  const kaapi_perf_idset_t* idset,
  kaapi_perf_counter_t* result,
  const kaapi_perf_counter_t* operand
);


/* accumulate performance counters into counter.
   size of counter must be the size of the set
*/
extern void kaapi_mt_perf_accum_registers(
  const struct kaapi_perfproc_t* kproc,
  const kaapi_perf_idset_t* idset,
  kaapi_perf_counter_t* counter
);

#if defined(__cplusplus)
}
#endif

#endif /* */
