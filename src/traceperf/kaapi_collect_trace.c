/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com 
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"

#if defined(KAAPI_USE_PERFCOUNTER)
#include <inttypes.h> 
#include <string.h>
#include <stdio.h>

#if 0
/* List of saved perfcounter ordered by apparition
*/
static kaapi_lock_t list_lock = KAAPI_LOCK_INITIALIZER;
static kaapi_teamperfctr_t* saved_headlist = 0;
static kaapi_teamperfctr_t* saved_taillist = 0;

/*
*/
static kaapi_teamperfctr_t* _kaapi_swap_and_link( kaapi_team_t* team )
{
  kaapi_teamperfctr_t* perf = team->perf;
  kaapi_teamperfctr_t* savedperf = (kaapi_teamperfctr_t*)malloc(sizeof(kaapi_teamperfctr_t)
    + perf->count * sizeof(kaapi_thread_perfctr_t)
  );

  savedperf->count = perf->count;
  savedperf->name  = perf->name;
  savedperf->t_start = 0; // must be write elsewhere
  savedperf->t_end = 0; // must be write elsewhere
  savedperf->regs = (kaapi_thread_perfctr_t*)(savedperf+1);
  savedperf->next = 0;
  memcpy(savedperf->regs, perf->regs, sizeof(kaapi_thread_perfctr_t)*perf->count );

  /* accum will be reset before accumulation in collect */
  kaapi_atomic_lock(&list_lock);
  if (saved_taillist ==0)
    saved_headlist = saved_taillist = savedperf;
  else
  {
    saved_taillist->next = savedperf;
    saved_taillist = savedperf;
  }
  kaapi_atomic_unlock(&list_lock);

  return savedperf;
}
#endif

static void display_gplot_header()
{
  unsigned int j;
  for (j=0; j< kaapi_tracelib_count_perfctr(); ++j)
  {
    if (!kaapi_perf_idset_test(&kaapi_tracelib_param.perfctr_idset, j))
      continue;
    printf("%15.15s  ", kaapi_perfctr_info[j].name);
  }
  printf("\n");
}

static void display_gplot( const kaapi_thread_perfctr_t* counter )
{
  unsigned int j;
  for (j=0; j< kaapi_tracelib_count_perfctr(); ++j)
  {
    if (!kaapi_perf_idset_test(&kaapi_tracelib_param.perfctr_idset, j))
      continue;
    if (kaapi_perfctr_info[j].ns2s)
      printf("%15e ",
        (double)((*counter).state[KAAPI_PERF_USR_STATE][j]+(*counter).state[KAAPI_PERF_SYS_STATE][j])/(double)1000000000
      );
    else
      printf("%15"PRIi64 " ",
        (*counter).state[KAAPI_PERF_USR_STATE][j]+(*counter).state[KAAPI_PERF_SYS_STATE][j]
      );
  }
}



static void kaapi_display_trace(
    kaapi_team_perfstat_t* perf,
    const kaapi_thread_perfctr_t* accum
)
{
  if (kaapi_tracelib_param.display_perfcounter == KAAPI_DISPLAY_PERF_FULL)
  {
    unsigned int i;
    /* accumulate per thread counters to team counter */
    for (i=0; i< perf->nthreads; ++i)
    {
      printf("----- Performance counters, core    : %i\n", i);
      kaapi_display_rawperf( stdout, &perf->threads_perfctr[i]->perf_regs );
    }
  }

  /* */
  if  ((kaapi_tracelib_param.display_perfcounter == KAAPI_DISPLAY_PERF_FULL)
    || (kaapi_tracelib_param.display_perfcounter == KAAPI_DISPLAY_PERF_RESUME))
  {
    printf("----- Cumulated Performance counters\n");
    kaapi_display_rawperf( stdout, accum );
  }

  /* Print GPLOT compatible info */
  if  ((kaapi_tracelib_param.display_perfcounter == KAAPI_DISPLAY_PERF_FULL)
    || (kaapi_tracelib_param.display_perfcounter == KAAPI_DISPLAY_PERF_GPLOT))
  {
    unsigned int i;
    /* print header of the file */
    printf("## GPLOT format for each core\n");
    /* print header of the file */
    printf("#%15.15s  ", "core id");
    display_gplot_header();

    for (i=0; i< perf->nthreads; ++i)
    {
      printf("%15i ", i);
      display_gplot( &perf->threads_perfctr[i]->perf_regs );
      printf("\n");
    }
  }

  /* */
  if  ((kaapi_tracelib_param.display_perfcounter == KAAPI_DISPLAY_PERF_FULL)
    || (kaapi_tracelib_param.display_perfcounter == KAAPI_DISPLAY_PERF_RESUME)
    || (kaapi_tracelib_param.display_perfcounter == KAAPI_DISPLAY_PERF_GPLOT))
  {
    printf("## GPLOT format for cumulative (per process) stat\n");
    printf("#");
    display_gplot_header();
    display_gplot( accum );
    printf("\n");
  }
}


/*
*/
void kaapi_collect_trace(kaapi_team_t* team)
{
  kaapi_thread_perfctr_t  accum;
  memset(&accum, 0, sizeof(kaapi_thread_perfctr_t));
  _kaapi_accumulate_trace( &accum, team->perf );

  /* report */
  kaapi_mt_perf_global_add( &accum );

  kaapi_team_perfstat_t* perf = 0; /*= _kaapi_swap_and_link( team );*/

  kaapi_display_trace(perf, &accum);
}



/* Call at the end of the computation to make synthetesis of collected teamperf_ctr
*/
void kaapi_end_collect_trace(void)
{
  unsigned int i;
  kaapi_thread_perfctr_t  accum;
  kaapi_thread_perfctr_t* accum_perkid = 0;
  int                         count_perkid = 0;

  if (kaapi_tracelib_param.display_perfcounter != KAAPI_DISPLAY_PERF_FINAL)
    return;

  memset(&accum, 0, sizeof(kaapi_thread_perfctr_t));
#if 0
  kaapi_teamperfctr_t* perf = saved_headlist;
  while (perf !=0)
  {
    if (count_perkid <perf->count)
    {
      //accum_perkid = realloc( accum_perkid, sizeof(kaapi_thread_perfctr_t)*perf->count );
      accum_perkid = malloc( sizeof(kaapi_thread_perfctr_t)*perf->count );
      memset( &accum_perkid[count_perkid], 0, sizeof(kaapi_thread_perfctr_t)*(perf->count - count_perkid));
      count_perkid = perf->count;
    }
    for (i=0; i<perf->count; ++i)
      kaapi_mt_perf_add_threadcounters( &accum_perkid[i], &perf->regs[i] );

    _kaapi_accumulate_trace( &accum, perf );
    /* */
    perf = perf->next;
  }
#endif

  /* print header of the file */
  printf("## GPLOT format for each core\n");
  /* print header of the file */
  printf("#%15.15s  ", "core id");
  display_gplot_header();

  for (i=0; i<count_perkid; ++i)
  {
    printf("%15i ", i);
    display_gplot( &accum_perkid[i] );
    printf("\n");
  }

  printf("## GPLOT format for cumulative (all threads) stat\n");
  printf("#");
  display_gplot_header();
  display_gplot( &accum );
  printf("\n");
}

#else
void kaapi_collect_trace(kaapi_team_t* team)
{
}

void kaapi_end_collect_trace(void)
{
}
#endif

