/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_EVENT_RECORDER_H_
#define _KAAPI_EVENT_RECORDER_H_ 1

#include "kaapi_event.h"
#include "kaapi_perf.h"
#include "assert.h"

#if defined(__cplusplus)
extern "C" {
#endif


/** Mask of events = kaapi_tracelib_param.eventmask
    The mask is set at ru		ntime to select events that will be registered
    to file.
    The bit i-th in the mask is 1 iff the event number i is registered.
    It means that not more than sizeof(kaapi_event_mask_type_t)*8 events 
    are available in Kaapi.
*/

/** Statup time for event recorder.
    Serve as the epoch for the event recorder sub library.
*/
extern uint64_t kaapi_event_startuptime;

/* the datation used for event */
static inline uint64_t kaapi_event_date(void)
{
#if defined(__i386__) || defined(__pentium__) || defined(__pentiumpro__) || defined(__i586__) || defined(__i686__) || defined(__k6__) || defined(__k7__) || defined(__x86_64__)
#  define KAAPI_GET_TICK(t) __asm__ volatile("rdtsc" : "=a" ((t).sub.low), "=d" ((t).sub.high))
  union tick_t
  {
    uint64_t tick;

    struct
    {
      uint32_t low;
      uint32_t high;
    }
    sub;
  };
  union tick_t t; 
  KAAPI_GET_TICK(t);
  return t.tick;
#else
  return kaapi_get_elapsedns(); 
#endif
}

/* info about timer, must be coherent with kaapi_event_date() */
static inline const char* kaapi_event_date_unit(void)
{
#if defined(__i386__) || defined(__pentium__) || defined(__pentiumpro__) || defined(__i586__) || defined(__i686__) || defined(__k6__) || defined(__k7__) || defined(__x86_64__)
  return "rdtsc";
#else
  return "ns";
#endif
}


/* Information about task in the trace file
*/
typedef struct kaapi_descrformat_t {
  int                         fmtid;
  int                         implicit; /* from open implicit task */
  const char*                 name;
  const char*                 color;
  struct kaapi_descrformat_t* parent;
  struct kaapi_descrformat_t* firstchild;
  struct kaapi_descrformat_t* lastchild;
  struct kaapi_descrformat_t* sibling;
  kaapi_named_perfctr_t*      perfctr;
} kaapi_descrformat_t;


/** Initialize the event recorder sub library.
    Must be called before any other functions.

*/
void kaapi_eventrecorder_init();


/** Destroy the event recorder sub library.
*/
void kaapi_eventrecorder_fini(void);

/** Open a new buffer for kprocessor kid.
    Kid must be a valid kid.
    \param ptype the type (KAAP_PROC_TYPE_XXX) of kid
    \param kid the identifier of a kprocessor or a device. Must be unique between threads.
    \retval a new eventbuffer
*/
extern kaapi_event_buffer_t* kaapi_event_openbuffer(unsigned int ptype, int kid, int numaid);


/** Flush the event buffer evb and return and new buffer.
    \param evb the event buffer to flush
    \retval the new event buffer to use for futur records.
*/
extern kaapi_event_buffer_t* kaapi_event_flushbuffer( kaapi_event_buffer_t* evb );

/** Flush the event buffer and close the associated file descriptor.
*/
extern void kaapi_event_closebuffer( kaapi_event_buffer_t* evb );


/** Fence: write all flushed buffers and return
*/
extern int kaapi_event_fencebuffers(void);

#if defined(__cplusplus)
}
#endif

#endif
