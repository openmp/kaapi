/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** fabien.lementec@gmail.com 
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_event.h"


/** Human readable name of events
*/
const char* kaapi_event_name[]
= {
/* 0 */  "K-ProcStart",
/* 1 */  "K-ProcEnd",
/* 2 */  "TaskBegin",
/* 3 */  "TaskEnd",
/* 4 */  "Dependency",
/* 5 */  "Access",
/* 6 */  "DagCompBegin",
/* 7 */  "DagCompEnd",
/* 8 */  "<undef>",
/* 9 */  "<undef>",
/*10 */  "IdleBeg",
/*11 */  "IdleEnd",
/*12 */  "<undef>",
/*13 */  "<undef>",
/*14 */  "<undef>",
/*15 */  "SuspendBegin",
/*16 */  "SuspendEnd",
/*17 */  "RequestBeg",
/*18 */  "RequestEnd",
/*19 */  "StealOp",
/*20 */  "StealAggrBegin",
/*21 */  "StealAggrBegin",
/*22 */  "H2HBegin",
/*23 */  "H2HEnd",
/*24 */  "H2DBegin",
/*25 */  "H2DEnd",
/*26 */  "D2HBegin",
/*27 */  "D2HEnd",
/*28 */  "D2DBegin",
/*29 */  "D2DEnd",
/*30 */  "KernelBegin",
/*31 */  "KernelEnd",
/*32 */  "ParallelBegin",
/*33 */  "ParallelEnd",
/*34 */  "TaskWaitBegin",
/*35 */  "TaskWaitEnd",
/*36 */  "TaskGroupBegin",
/*37 */  "TaskGroupEnd",
/*38 */  "PerfCounter",
/*39 */  "TaskPerfCntr",
/*40 */  "LockBegin",
/*41 */  "LockEnd",
/*42 */  "YieldBeg",
/*43 */  "YieldEnd",
/*44 */  "BarrierBegin",
/*45 */  "BarrierEnd",
/*46 */  "TaskSteal",
};
