/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** fabien.lementec@gmail.com 
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <errno.h>
#include <inttypes.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <sys/resource.h>


#include "config.h"
#if defined(KAAPI_USE_PAPI)
#include <papi.h>
#endif

#include "kaapi_time.h"
#include "kaapi_trace.h"
#include "kaapi_trace_impl.h"
#include "kaapi_recorder.h"
#include "kaapi_hashmap.h"
#include "kaapi_atomic.h"
#include "kaapi_util.h"

/*
  Global Variable
  */
kaapi_tracelib_param_t kaapi_tracelib_param =
{
  .cpucount = 0,
  .gpucount = 0,
  .numaplacecount = 0,
  .fmt_list = 0,
  .fmt_listsize = 0,
  .display_perfcounter = KAAPI_NO_DISPLAY_PERF,
#if defined(KAAPI_USE_PERFCOUNTER)
  /* to do here: define group and likwid like metric using collected counters */
  .eventmask           = 0 /*KAAPI_EVT_MASK_COMPUTE|KAAPI_EVT_MASK_IDLE|KAAPI_EVT_MASK_PERFCOUNTER*/,
  .perfctr_idset       = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKEXEC) |
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKSTEAL) |
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_WORK) |
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TINF) |
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_PTIME) |
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_DFGBUILD) |
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALREQOK)  |
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALREQ) |
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALOP) |
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALIN)
#if defined(KAAPI_USE_NUMA)
                        | KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_READ)|
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_WRITE) |
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_READ) |
                        KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_WRITE)
#endif
                        ,
  .taskperfctr_idset   = 0,
#else
  .eventmask           = 0,
  .perfctr_idset       = 0,
  .taskperfctr_idset   = 0,
#endif
  .papi_event_count    = 0,
};
static int fmt_listcapacity = 0;

/* Meta data about performance counter 
*/
kaapi_perfctr_info_t kaapi_perfctr_info[64] = {
/* 0 */  { "Work(s)",     "WORK", "", 0, 1, 0, 0, 0, 0 },
/* 1 */  { "Tinf(s)",     "TINF", "", 0, 1, 1, 0, 0, 0 },
/* 2 */  { "Time(s)",     "TIME", "", 0, 1, 1, 0, 0, 0 },
/* 3 */  { "#Task Spawn", "TASKSPAWN", "", 0, 0, 0, 0, 0, 0 },
/* 4 */  { "#Task Exec",  "TASKEXEC", "", 0, 0, 0, 0, 0, 0 },
/* 5 */  { "Conflict Pop", 0, "", 0, 0, 0, 0, 0, 0 },
/* 6 */  { "#Steal req ok", "STEALOK", "", 0, 0, 0, 0, 0, 0 },
/* 7 */  { "#Steal req",    "STEALREQ", "", 0, 0, 0, 0, 0, 0 },
/* 8 */  { "#Steal op",     "STEALOP", "", 0, 0, 0, 0, 0, 0 },
/* 9 */  { "#Steal input",  0, "<not implemented>", 0, 0, 0, 0, 0, 0 },
/* 10 */ { "#Task Steal",   "TASKSTEAL", "", 0, 0, 0, 0, 0, 0 },
/* 11 */ { "#Sync",         "SYNC", "", 0, 0, 0, 0, 0, 0 },
/* 12 */ { "H2H(MByte)",    "H2H", "Host to device communicaiton volume", 0, 0, 0, 0, 0, 0},
/* 13 */ { "H2D(MByte)",    "H2D", "Host to device communicaiton volume", 0, 0, 0, 0, 0, 0},
/* 14 */ { "D2H(MByte)",    "D2H", "Device to host communicaiton volume", 0, 0, 0, 0, 0, 0},
/* 15 */ { "D2D(MByte)",    "D2D", "Device to device communicaiton volume", 0, 0, 0, 0, 0, 0},
/* 16 */ { "#Task offload", 0, "", 0, 0, 0, 0, 0, 0 },
/* 17 */ { "Cache Hit",     "CACHE_HIT", "", 0, 0, 0, 0, 0, 0 },
/* 18 */ { "Cache Miss",    "CACHE_MISS", "", 0, 0, 0, 0, 0, 0 },
/* 19 */ { "Stack size",    "STACKSIZE", "", 0, 0, 0, 0, 0, 0 },
/* 20 */ { "T(dfgbuild)/ns","DFGBUILD",  "", 0, 1, 0, 0, 0 },
/* 21 */ { "T(syncdfg)/ns", 0, "", 0, 1, 0, 0, 0 },
/* 22 */ { "T(initdfg)/ns", 0, "", 0, 1, 0, 0, 0 },
/* 23 */ { "#local read",   "LOCAL_READ", "Number of local read parameters to a task", 0, 0, 0, 0, 0, 0 },
/* 24 */ { "#local write",  "LOCAL_WRITE", "", 0, 0, 0, 0, 0, 0 },
/* 25 */ { "#remote read",  "REMOTE_READ", "", 0, 0, 0, 0, 0, 0 },
/* 26 */ { "#remote write", "REMOTE_WRITE", "", 0, 0, 0, 0, 0, 0 },
/* 27 */ { "#parallel",     "PARALLELREG", "OpenMP support. Count number of parallel regions", 0, 0, 0, 0, 0, 0 },
/* 28 */ { "#lock",         "LOCK", "OpenMP support. Count number of calls to omp_set_lock or omp_set_nested_lock", 0, 0, 0, 0, 0, 0 },
/* 29 */ { "#barrier",      "BARRIER", "OpenMP support. Count number of barrier", 0, 0, 0, 0, 0, 0 },
/* 30 */ { "#taskyield",    "YIELD", "OpenMP support. Count number of taskyield", 0, 0, 0, 0, 0, 0 },
/* 31 */ { "", 0, "", 0, 0, 0, 0, 0, 0 },
/* 32 */ { "", 0, "<PAPI>", 0, 0, 0, 0, 0, 0 },
/* 33 */ { "", 0, "<PAPI>", 0, 0, 0, 0, 0, 0 },
/* 34 */ { "", 0, "<PAPI>", 0, 0, 0, 0, 0, 0 },
/* 35 */ { "", 0, "<PAPI>", 0, 0, 0, 0, 0, 0 },
/* 36 */ { "", 0, "<PAPI>", 0, 0, 0, 0, 0, 0 },
/* 37 */ { "", 0, "<PAPI>", 0, 0, 0, 0, 0, 0 },
/* 38 */ { "", 0, "<PAPI>", 0, 0, 0, 0, 0, 0 },
/* 39 */ { "", 0, "<PAPI>", 0, 0, 0, 0, 0, 0 },
/* 40 */ { "", 0, "<PAPI>", 0, 0, 0, 0, 0, 0 },
/* 41 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 42 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 43 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 44 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 45 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 46 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 47 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 48 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 49 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 50 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 51 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 52 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 53 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 54 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 55 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 56 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 57 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 58 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 59 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 60 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 61 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 62 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
/* 63 */ { "", 0, "<?>", 0, 0, 0, 0, 0, 0 },
};

/* Predefined group of event */
typedef struct  {
  //int countevent;
  kaapi_perf_idset_t mask;
  kaapi_perf_idset_t mask_pertask;
} kaapi_perfctr_group_t;

static kaapi_perfctr_group_t kaapi_perfctr_group[] = {
  { /* KAAPI_PERF_GROUP_TASK */
    .mask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_WORK)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TINF)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_PTIME)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKSPAWN)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKEXEC)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_SYNCINST)
#if defined(KAAPI_USE_NUMA)
          | KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_READ) |
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCAL_WRITE) |
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_READ) |
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_REMOTE_WRITE)
#endif
    ,
    .mask_pertask = 0
  },
  { /* KAAPI_PERF_GROUP_OFFLOAD */
    .mask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_COMM_H2D)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_COMM_D2H)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_COMM_D2D)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_CACHE_HIT)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_CACHE_MISS),
    .mask_pertask = 0
  },
  { /* KAAPI_PERF_GROUP_DFGBUILD */
    .mask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_DFGBUILD)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_RDLISTINIT)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_RDLISTEXEC),
    .mask_pertask = 0
  },
  { /* KAAPI_PERF_GROUP_STEAL */
    .mask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_CONFLICTPOP)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKSTEAL)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALREQOK)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALREQ)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALOP)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_STEALIN),
    .mask_pertask = 0
  },
  { /* KAAPI_PERF_GROUP_OMP */
    .mask = KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_WORK)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_PTIME)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKSPAWN)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKEXEC)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_SYNCINST)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_PARALLEL)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TASKYIELD)|
            KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_LOCK),
    .mask_pertask = 0
  }
};

/*
*/
#define KAAPI_SIZE_DFGCTXT 9


/* Intialize team performance counter
   Global list is maintained for global output.
*/
static kaapi_team_perfstat_t* team_perfctr_head = 0;
static kaapi_team_perfstat_t* team_perfctr_tail = 0;
static kaapi_lock_t team_perfctr_lock = KAAPI_LOCK_INITIALIZER;

/* hash map key/team -> kaapi_team_perfstat_t */
static kaapi_hashmap_t          team_map_routine;
static kaapi_hashentries_t*     team_mapentries[1<<KAAPI_SIZE_DFGCTXT];
static kaapi_hashentries_bloc_t team_mapbloc;
static kaapi_lock_t             team_map_lock = KAAPI_LOCK_INITIALIZER;


/* A global perf counter = accumulation of all thread' performance counter
*/
kaapi_thread_perfctr_t*	global_perf_regs = 0;


/* Hash map for task format descriptor
*/
static kaapi_hashmap_t                fdescr_map_routine;
static kaapi_hashentries_t*           fdescr_mapentries[1<<KAAPI_SIZE_DFGCTXT];
static kaapi_hashentries_bloc_t       fdescr_mapbloc;
static kaapi_lock_t                   fdescr_map_lock = KAAPI_LOCK_INITIALIZER;


static void kaapi_perf_thread_dumpcounter(
  kaapi_evtproc_t*  stream,
  kaapi_perfproc_t* perf
);
static void kaapi_mt_perf_accum_perf_stat(
  kaapi_named_perfctr_t*    perf,
  const kaapi_perf_idset_t* idset
);

/*
*/
static int kaapi_get_events(
  const char* env,
  int task_set
);

/* internal */
static unsigned int user_event_count = 0;
static unsigned int papi_event_count = 0;
static kaapi_perf_idset_t papi_event_mask = 0;

/* a <- a + (b-c) */
static void kaapi_mt_perf_addsub_counters(
  const kaapi_perf_idset_t*   idset,
  kaapi_perf_counter_t*       a,
  const kaapi_perf_counter_t* b,
  const kaapi_perf_counter_t* c
);



/**
*/
int kaapi_tracelib_init(
  int gid,
  int cpucount,
  int gpucount,
  int numaplacecount,
  kaapi_descrformat_t** fmt_descr,
  int fmt_descrcount
)
{
  static int once = 0;
  if (once) return 0;
  once = 1;

  int i, error;

  /* allows to have the delay between init/fini */
  kaapi_timelib_init();

  /* map for format descriptor */
  kaapi_hashmap_init( &fdescr_map_routine,
                      fdescr_mapentries,
                      KAAPI_SIZE_DFGCTXT,
                      &fdescr_mapbloc );

  /* map for team  */
  kaapi_hashmap_init( &team_map_routine,
                      team_mapentries,
                      KAAPI_SIZE_DFGCTXT,
                      &team_mapbloc );

  kaapi_tracelib_param.gid = gid;
  kaapi_tracelib_param.cpucount = cpucount;
  kaapi_tracelib_param.gpucount = gpucount;
  kaapi_tracelib_param.numaplacecount = numaplacecount;

  /* perf counters initialization */
  kaapi_perf_idset_zero( &kaapi_tracelib_param.perfctr_idset ) ;
  kaapi_perf_idset_zero( &kaapi_tracelib_param.taskperfctr_idset ) ;

#if defined(KAAPI_USE_PAPI)
  error = PAPI_library_init(PAPI_VER_CURRENT);
  kaapi_assert(error == PAPI_VER_CURRENT);
  
  error = PAPI_thread_init(pthread_self);
  kaapi_assert(error == PAPI_OK);
#endif

  /* Other counters: undefined code */
  for (i=0; i<KAAPI_PERF_ID_ENDSOFTWARE; ++i)
    kaapi_perfctr_info[i].eventcode = i;

  error = kaapi_get_events("KAAPI_PERF_EVENTS", 0);
  kaapi_assert(0 == error);
  /* always add task counter into the global set of counter */
  /* may be user add counter perf task (e.g. etf WSPUSH strategy) */
  error = kaapi_get_events("KAAPI_TASKPERF_EVENTS", 1);
  kaapi_assert(0 == error);

  global_perf_regs = malloc( sizeof(kaapi_thread_perfctr_t));
  memset( global_perf_regs, 0, sizeof(kaapi_thread_perfctr_t) );

  /* event mask */
  kaapi_tracelib_param.eventmask = 0;
  if ((getenv("KAAPI_RECORD_TRACE") !=0) && !strcasecmp(getenv("KAAPI_RECORD_TRACE"),"1"))
  {
    if (getenv("KAAPI_RECORD_MASK") !=0)
    {
      /* actual grammar:
         eventno[,eventno]*
         eventno is an integer less than 2^sizeof(kaapi_event_mask_type_t)
         grammar must be more complex using predefined set
      */
      uint64_t mask = 0;
      char* name = getenv("KAAPI_RECORD_MASK");
      bool err = kaapi_parse_listkeywords( &mask, &name, ',',
         6,
           "COMPUTE", (uint64_t)KAAPI_EVT_MASK_COMPUTE,
           "IDLE",    (uint64_t)KAAPI_EVT_MASK_IDLE,
           "STEAL",   (uint64_t)KAAPI_EVT_MASK_STEALOP,
           "OFFLOAD", (uint64_t)KAAPI_EVT_MASK_OFFLOAD,
           "PERFCTR", (uint64_t)KAAPI_EVT_MASK_PERFCOUNTER,
           "KOMP",    (uint64_t)KAAPI_EVT_MASK_KOMP

      );
      if (err ==false)
      {
        fprintf(stderr, "*** Kaapi: mal formed mask list 'KAAPI_RECORD_MASK': '%s'\n",
          getenv("KAAPI_RECORD_MASK")
        );
        return EINVAL;
      }
      /* always add startup set */
      kaapi_tracelib_param.eventmask = mask |KAAPI_EVT_MASK_STARTUP;
    }
  }

  kaapi_tracelib_param.fmt_list = fmt_descr;
  kaapi_tracelib_param.fmt_listsize = fmt_descrcount;
  fmt_listcapacity = fmt_descrcount;

  kaapi_tracelib_param.task_event_count
    = __builtin_popcountl(kaapi_tracelib_param.taskperfctr_idset);
  kaapi_eventrecorder_init();

  char* displayperf;
  int displayperf_value;
  displayperf = getenv("KAAPI_DISPLAY_PERF");
  if (displayperf)
  if (!kaapi_parse_perfcounter(&displayperf, &displayperf_value))
  {
    fprintf(stderr, "***Kaapi: bad value for variable KAAPI_DISPLAY_PERF. Use 'false'\n");
    kaapi_tracelib_param.display_perfcounter = KAAPI_NO_DISPLAY_PERF;
  }
  kaapi_tracelib_param.display_perfcounter = displayperf_value;

  return 0;
}


/*
*/
static void _kaapi_print_total(
  char* buffer, const char* title, int count, uint64_t gn[], double gsum[], double gsum2[]
)
{
  /* total info */
  if (count >0)
  {
    int i, pos;
    char* buff = buffer;
    pos = sprintf(buff, " %40s %10llu ", title, (unsigned long long)gn[0]);
    buff += pos;
    for (i = 0; i<count; ++i)
    {
      double n = 1.0/(double)gn[i];
      double sum   = gsum[i];
      double avrg  = sum * n;
      //double avrg2 = gsum2[i];
      //double s = sqrt( avrg2 - avrg*avrg );
      //double d = 1.96 * s * sqrt(n);
      //pos = sprintf(buff, "%12.3e %12.3e %12.3e ", sum, avrg, d);
      pos = sprintf(buff, "%12.3e %12.3e %12s ", sum, avrg, " ");
      buff += pos;
    }
  }
}

/*
*/
static void _kaapi_print_stat_set(
  char* buffer,
  int level,
  kaapi_perf_idset_t set,
  kaapi_named_perfctr_t* perf,
  uint64_t gn[], double gsum[], double gsum2[]
)
{
  char* buff;
  int pos;
  char buffer_name[64];

  /* add indentation */
  buff = buffer_name;
  if (level >0)
  {
    int i;
    for (i=0; i<level-1; ++i)
    {
      pos = sprintf(buff,"| ");
      buff += pos;
    }
    pos = sprintf(buff,"|-");
    buff += pos;
  }
  sprintf(buff, "%s", perf->name);

  buff = buffer;
  pos = sprintf(buff," %-40.40s %10lu ", buffer_name, perf->stats.count);
  buff += pos;
  int gidx = 0;
  while (set !=0)
  {
    unsigned int idx;
    idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    double c;
    /* scale PERF_ID_TIME counter to be in second */
    if (kaapi_perfctr_info[idx].ns2s) c = 1e-9;
    else c = 1.0;
    double n = 1.0/(double)perf->stats.count;
    double sum   = c*(double)perf->stats.sum_counters[idx];
    double avrg  = sum * n;
    double avrg2 = (c*c*(double)perf->stats.sum2_counters[idx]) * n;
    double s = sqrt( avrg2 - avrg*avrg );
    double d = 1.96 * s * sqrt(n);
    pos = sprintf(buff, "%12.3e %12.3e %12.3e ", sum, avrg, d);
    buff += pos;

    gn[gidx] += perf->stats.count;
    gsum[gidx] += c*(double)perf->stats.sum_counters[idx];
    gsum2[gidx] += c*c*(double)perf->stats.sum2_counters[idx];
    ++gidx;
  }
}


/* For each counter reserve space for sum/avrg/sd
*/
static void _kaapi_print_header(
  char* buffer,
  kaapi_perf_idset_t set
)
{
  char* buff = buffer;
  int pos = sprintf(buff,"#%40s %10s ", "<task name>", "<count>");
  buff += pos;

  char buffer_group [64];
  while (set !=0)
  {
    unsigned int idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    sprintf(buffer_group,"    %s (sum/avrg/sd)   ", kaapi_perfctr_info[idx].name);
    pos = sprintf(buff, "%-38.38s ", buffer_group);
    buff += pos;
  }
}


/*
*/
static void _kaapi_compute_child(void)
{
  int i;
  for (i=0; i<kaapi_tracelib_param.fmt_listsize; ++i)
  {
    if (kaapi_tracelib_param.fmt_list[i]->implicit ==0)
      continue;
    kaapi_descrformat_t* fdescr = kaapi_tracelib_param.fmt_list[i];
    kaapi_descrformat_t* fparent = fdescr->parent;
    if (fparent)
    {
      if (fparent->firstchild ==0)
        fparent->firstchild = fdescr;
      else
        fparent->lastchild->sibling = fdescr;
      fparent->lastchild = fdescr;
    }
  }
}


/*
*/
static void _kaapi_print_tree(
    FILE *file,
    char* buffer,
    kaapi_descrformat_t* fdescr,
    int level,
    uint64_t gn[], double gsum[], double gsum2[]
)
{
  kaapi_named_perfctr_t* perf = fdescr->perfctr;
  kaapi_mt_perf_accum_perf_stat( perf, &kaapi_tracelib_param.taskperfctr_idset);

  if (perf->stats.count !=0)
  {
    _kaapi_print_stat_set( buffer, level, kaapi_tracelib_param.taskperfctr_idset, perf, gn, gsum, gsum2 );
    fprintf(file, "%s\n",buffer);
  }
  fdescr = fdescr->firstchild;
  while (fdescr !=0)
  {
    _kaapi_print_tree( file, buffer, fdescr, level+1, gn, gsum, gsum2 );
    fdescr = fdescr->sibling;
  }
}


/* For each counter reserve space for total/self
*/
static void _kaapi_print_team_header(
  char* buffer,
  kaapi_perf_idset_t set
)
{
  char* buff = buffer;
  int pos = sprintf(buff,"#%40s %10s %10s", "<team name>", "<count>", "<#threads>");
  buff += pos;

  char buffer_group [128];
  while (set !=0)
  {
    unsigned int idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    sprintf(buffer_group,"   %s sys/usr (total|self)  ", kaapi_perfctr_info[idx].name);
    pos = sprintf(buff, "%-48.48s ", buffer_group);
    buff += pos;
  }
}


/*
*/
static void _kaapi_print_team_set(
  char* buffer,
  int level,
  kaapi_perf_idset_t set,
  kaapi_team_perfstat_t* perf
)
{
  char* buff;
  int pos;
  char buffer_name[128];

  /* add indentation */
  buff = buffer_name;
  if (level >0)
  {
    int i;
    for (i=0; i<level-1; ++i)
    {
      pos = sprintf(buff,"| ");
      buff += pos;
    }
    pos = sprintf(buff,"|-");
    buff += pos;
  }
  sprintf(buff, "%s", perf->name);

  buff = buffer;
  pos = sprintf(buff," %-40.40s %10i %10i ", buffer_name, perf->count, perf->nthreads);
  buff += pos;

  while (set !=0)
  {
    unsigned int idx;
    idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    double c;

    /* scale PERF_ID_TIME counter to be in second */
    if (kaapi_perfctr_info[idx].ns2s) c = 1e-9;
    else c = 1.0;
    double total_sys = c*((double)(perf->all_perfctr.state[KAAPI_PERF_USR_STATE][idx]));
    double total_usr = c*((double)(perf->all_perfctr.state[KAAPI_PERF_SYS_STATE][idx]));

    double self_sys  = c*((double)(perf->self_perfctr.state[KAAPI_PERF_USR_STATE][idx]));
    double self_usr  = c*((double)(perf->self_perfctr.state[KAAPI_PERF_SYS_STATE][idx]));

    pos = sprintf(buff, "%10.3e | %10.3e / %10.3e | %10.3e ", total_sys, total_usr, self_sys, self_usr);
    buff += pos;
  }
}


/* Debug
*/
static void _kaapi_print_threadctr_set(
  kaapi_perf_idset_t set,
  kaapi_thread_perfctr_t* perf
)
{
  while (set !=0)
  {
    unsigned int idx;
    idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    double c;

    /* scale PERF_ID_TIME counter to be in second */
    if (kaapi_perfctr_info[idx].ns2s) c = 1e-9;
    else c = 1.0;
    double total_sys = c*((double)(perf->state[KAAPI_PERF_USR_STATE][idx]));
    double total_usr = c*((double)(perf->state[KAAPI_PERF_SYS_STATE][idx]));

    printf("%10.3e/%10.3e ", total_sys, total_usr);
  }
}


/*
*/
static int _kaapi_compute_team_tree(void)
{
  int i;
  int cnt = 0;
  kaapi_team_perfstat_t* curr = team_perfctr_head;
  while (curr !=0)
  {
    ++cnt;
    /* accumulate per thread counters to team counter */
    for (i=0; i< curr->nthreads; ++i)
    {
      if (curr->threads_perfctr[i] ==0) continue;
      kaapi_thread_perfctr_t* perf_regs = &curr->threads_perfctr[i]->perf_regs;
      printf("[%i]:",i);
      _kaapi_print_threadctr_set(kaapi_tracelib_param.perfctr_idset, perf_regs );
      printf("\n");
      kaapi_mt_perf_add_threadcounters( &curr->self_perfctr, perf_regs );
    }
    /* compute self and report self to the parent sum */
    kaapi_mt_perf_add_threadcounters( &curr->all_perfctr, &curr->self_perfctr );
    kaapi_team_perfstat_t* tparent = curr->parent;
    if (tparent)
    {
      kaapi_mt_perf_add_threadcounters( &tparent->all_perfctr, &curr->self_perfctr );
      if (tparent->firstchild ==0)
        tparent->firstchild = curr;
      else
        tparent->lastchild->sibling = curr;
      tparent->lastchild = curr;
    }
    curr = curr->next;
  }
  return cnt;
}


/*
*/
static void _kaapi_print_team_tree(
    FILE *file,
    char* buffer,
    int level,
    kaapi_team_perfstat_t* tperfctr
)
{
  _kaapi_print_team_set( buffer, level, kaapi_tracelib_param.perfctr_idset, tperfctr );
  fprintf(file, "%s\n",buffer);
  kaapi_team_perfstat_t* child = tperfctr->firstchild;
  while (child !=0)
  {
    _kaapi_print_team_tree( file, buffer, level+1, child );
    child = child->sibling;
  }
}


/** Finish trace. Assume that threads have reach the barrier and flush
    their event buffers.
*/
void kaapi_tracelib_fini(void)
{
  static int once = 0;
  if (once) return;
  once = 1;

  FILE *file = 0;
  char buffer[16384];
  char filename[128];

#if defined(KAAPI_USE_PAPI)
  PAPI_shutdown();
#endif

  /* Display stat per task */
  if (kaapi_tracelib_param.display_perfcounter != KAAPI_NO_DISPLAY_PERF)
  {
    sprintf(filename, "stat.%i", getpid());
    file = fopen(filename,"w");
    if (file == 0)
      fprintf(stderr,"*** Kaapi: cannot open stat file '%s'\n", filename);
    fprintf(file,"# Statistics about process %i. Duration: %e(s)\n",
        getpid(),
        1e-9*((double)kaapi_get_elapsedns_since_start())
    );
  }

  if (file !=0)
  {
    /* accumulate over all teams */
    int nteam = _kaapi_compute_team_tree();
    if (nteam)
    {
      fprintf(file, "#---------- Teams' Performance Counter\n");

      _kaapi_print_team_header( buffer, kaapi_tracelib_param.perfctr_idset );
      fprintf(file, "%s\n",buffer);
      kaapi_team_perfstat_t* curr = team_perfctr_head;
      while (curr !=0)
      {
        if (curr->parent ==0) /* root */
          _kaapi_print_team_tree(file, buffer, 0, curr );
        curr = curr->next;
      }
    }

    /* Display per task counters informations */
    if (kaapi_tracelib_param.taskperfctr_idset !=0)
    {
      int perfctr_cnt = kaapi_tracelib_param.task_event_count;
      int banner0;
      int banner1;
      size_t i;

      /* display format of the output */
      fprintf(file, "\n");

      uint64_t total_gn[perfctr_cnt];
      double total_gsum[perfctr_cnt];
      double total_gsum2[perfctr_cnt];
      memset( total_gn, 0, sizeof(total_gn));
      memset( total_gsum, 0, sizeof(total_gsum));
      memset( total_gsum2, 0, sizeof(total_gsum2));

      banner0 = 0;
      uint64_t gn[perfctr_cnt];
      double gsum[perfctr_cnt];
      double gsum2[perfctr_cnt];
      memset( gn, 0, sizeof(gn));
      memset( gsum, 0, sizeof(gsum));
      memset( gsum2, 0, sizeof(gsum2));

      for (i=0; i<kaapi_tracelib_param.fmt_listsize; ++i)
      {
        if (kaapi_tracelib_param.fmt_list[i]->implicit)
          continue;
        kaapi_named_perfctr_t* perf = kaapi_tracelib_param.fmt_list[i]->perfctr;
        if (!banner0)
        {
          fprintf(file," # --------------- Explicit task\n");
          _kaapi_print_header( buffer, kaapi_tracelib_param.taskperfctr_idset );
          fprintf(file, "%s\n", buffer);
          banner0 = 1;
        }

        kaapi_mt_perf_accum_perf_stat( perf, &kaapi_tracelib_param.taskperfctr_idset);

        if (perf->stats.count !=0)
        {
          _kaapi_print_stat_set( buffer, 0, kaapi_tracelib_param.taskperfctr_idset, perf, gn, gsum, gsum2 );
          fprintf(file, "%s\n",buffer);
        }
      }
      for (i = 0; i<perfctr_cnt; ++i)
      {
        total_gn[i] += gn[i];
        total_gsum[i] += gsum[i];
        total_gsum2[i] += gsum2[i];
      }
      if (banner0 ==1)
      {
        _kaapi_print_total( buffer, "   Sub total", perfctr_cnt, gn, gsum, gsum2);
        fprintf(file, "%s\n\n",buffer);
      }

      banner1 = 0;
      memset( gn, 0, sizeof(gn));
      memset( gsum, 0, sizeof(gsum));
      memset( gsum2, 0, sizeof(gsum2));

      /* */
      _kaapi_compute_child();
      for (i=0; i<kaapi_tracelib_param.fmt_listsize; ++i)
      {
        if (kaapi_tracelib_param.fmt_list[i]->implicit ==0)
          continue;
        kaapi_descrformat_t* fdescr = kaapi_tracelib_param.fmt_list[i];
        if (fdescr->parent) /* print by child/sibling info */
          continue;
        if (!banner1)
        {
          fprintf(file," # --------------- Implicit task\n");
          _kaapi_print_header( buffer, kaapi_tracelib_param.taskperfctr_idset );
          fprintf(file, "%s\n", buffer);
          banner1 = 1;
        }
        _kaapi_print_tree( file, buffer, fdescr, 0, gn, gsum, gsum2 );
      }

      for (i = 0; i<perfctr_cnt; ++i)
      {
        total_gn[i] += gn[i];
        total_gsum[i] += gsum[i];
        total_gsum2[i] += gsum2[i];
      }
      if (banner1 == 1)
      {
        _kaapi_print_total( buffer, "   Sub total", perfctr_cnt, gn, gsum, gsum2);
        fprintf(file, "%s\n\n",buffer);
      }

      if ((banner0 == 1)||(banner1 == 1))
      {
        _kaapi_print_total( buffer, "   Total", perfctr_cnt, total_gn, total_gsum, total_gsum2);
        fprintf(file, "\n%s\n\n\n",buffer);
      }
    }

//    fprintf(file, "###--- Global performance counters\n");
//    kaapi_display_rawperf(file, (const kaapi_thread_perfctr_t*)global_perf_regs);

    printf("File: '%s' generated\n", filename);
  }

  if (kaapi_tracelib_param.eventmask)
    kaapi_eventrecorder_fini();
}


/*
*/
kaapi_team_perfstat_t* kaapi_tracelib_team_init(
  int nproc,
  void* (*routine)(),
  const char* func_name,
  const char* (*filter_func)(const char*),
  kaapi_team_perfstat_t* parent
)
{
  void* key = (void*)(uintptr_t)routine;

  /* pessimist lock */
  kaapi_atomic_lock(&team_map_lock);

  kaapi_hashentries_t* entry = kaapi_hashmap_findinsert( &team_map_routine, key );
  kaapi_team_perfstat_t* tperfctr = KAAPI_HASHENTRIES_GET(entry, kaapi_team_perfstat_t*);
  if (tperfctr != 0)
  {
    ++tperfctr->count;
    kaapi_atomic_unlock(&team_map_lock);
    kaapi_assert_debug( nproc  == tperfctr->nthreads );
    kaapi_assert_debug( parent == tperfctr->parent );
    return tperfctr;
  }

  tperfctr = malloc( sizeof(kaapi_team_perfstat_t) + nproc* sizeof(kaapi_perfproc_t*));
  tperfctr->nthreads   = nproc;
  tperfctr->count      = 1;
  tperfctr->parent     = parent;
  tperfctr->firstchild = 0;
  tperfctr->lastchild  = 0;
  tperfctr->sibling    = 0;
  memset(&tperfctr->self_perfctr, 0, sizeof(kaapi_thread_perfctr_t));
  memset(&tperfctr->all_perfctr, 0, sizeof(kaapi_thread_perfctr_t));
  tperfctr->threads_perfctr = (kaapi_perfproc_t**)(1+tperfctr);
  memset(tperfctr->threads_perfctr, 0, nproc*sizeof(kaapi_perfproc_t*));
  if (filter_func && func_name)
    tperfctr->name = strdup(filter_func(func_name));
  else if (func_name)
    tperfctr->name  = strdup(func_name);
  else
    tperfctr->name  = "<undefined>";
  KAAPI_HASHENTRIES_SET(entry, tperfctr, kaapi_team_perfstat_t*);
  kaapi_atomic_unlock(&team_map_lock);

  kaapi_atomic_lock( &team_perfctr_lock );
  tperfctr->next  = 0;
  if (team_perfctr_tail !=0)
    team_perfctr_tail->next = tperfctr;
  else
    team_perfctr_head = tperfctr;
  team_perfctr_tail = tperfctr;
  kaapi_atomic_unlock( &team_perfctr_lock );

  return tperfctr;
}


/**
*/
int kaapi_tracelib_team_fini(
  kaapi_team_perfstat_t* team_perfctr
)
{
  /* do nothing: stats will be computed at the end of the library */
  return 0;
}



/**
*/
__thread int papi_init_count = 0;
kaapi_perfproc_t* kaapi_tracelib_thread_init(
    kaapi_evtproc_t**      stream,
    kaapi_team_perfstat_t* team,
    unsigned int           proctype,
    int                    kid,
    int                    numaid,
    int                    isuser
)
{
  kaapi_assert( (isuser ==0)||(isuser==1) );

  kaapi_perfproc_t* perfproc;

  /* reuse team and per thread data. Init or re-init papi in the both case.
  */
  if (team->threads_perfctr[kid] ==0)
  {
    perfproc = malloc( sizeof(kaapi_perfproc_t) );
    memset( perfproc, 0, sizeof( kaapi_perfproc_t) );
    perfproc->start_t[0] = perfproc->start_t[1] = -1;
    perfproc->curr_perf_regs = perfproc->perf_regs.state[isuser];
    perfproc->start_t[isuser] = kaapi_get_elapsedns();
  }
  else
    perfproc = team->threads_perfctr[kid];

  if (kaapi_tracelib_param.eventmask)
  {
    if (stream !=0)
    {
      *stream = (kaapi_evtproc_t*)malloc(sizeof(kaapi_evtproc_t));
      (*stream)->eventbuffer = kaapi_event_openbuffer(proctype, kid, numaid);
      (*stream)->numaid = numaid;
      (*stream)->ident  = kid;
      (*stream)->ptype  = proctype;
    }
    else
      kaapi_abort(__LINE__, __FILE__, "*** Kaapi warning: cannot store event stream\n");
  }

  perfproc->papi_event_count  = 0;

#if defined(KAAPI_USE_PAPI)
  int papi_event_codes[KAAPI_MAX_HWCOUNTERS];
  if ((papi_event_count) && (++papi_init_count == 1))
  {
    int err;
    PAPI_option_t opt;

    /* register the thread */
    err = PAPI_register_thread();
    kaapi_assert(PAPI_OK == err);

    /* create event set */
    perfproc->papi_event_set = PAPI_NULL;
    err = PAPI_create_eventset(&perfproc->papi_event_set);
    kaapi_assert(PAPI_OK == err);

    /* set cpu as the default component. mandatory in newer interfaces. */
    err = PAPI_assign_eventset_component(perfproc->papi_event_set, 0);
    kaapi_assert(PAPI_OK == err);

    /* thread granularity */
    memset(&opt, 0, sizeof(opt));
    opt.granularity.def_cidx = perfproc->papi_event_set;
    opt.granularity.eventset = perfproc->papi_event_set;
    opt.granularity.granularity = PAPI_GRN_THR;
    err = PAPI_set_opt(PAPI_GRANUL, &opt);
    kaapi_assert(PAPI_OK == err);

    /* user domain */
    memset(&opt, 0, sizeof(opt));
    opt.domain.eventset = perfproc->papi_event_set;
    opt.domain.domain = PAPI_DOM_USER;
    err = PAPI_set_opt(PAPI_DOMAIN, &opt);
    kaapi_assert(PAPI_OK == err);

    /* configure the papi event set */
    unsigned int i;
    int count =0;
    for ( i=KAAPI_PERF_ID_PAPI_BASE; i<KAAPI_PERF_ID_MAX; ++i)
    {
      if (kaapi_perf_idset_test( &kaapi_tracelib_param.perfctr_idset, i))
      {
        papi_event_codes[count++] = kaapi_perfctr_info[i].eventcode;
        kaapi_perf_idset_add(&kaapi_tracelib_param.perfctr_idset, i);
      }
    }
    kaapi_assert_debug( count == papi_event_count );
    err = PAPI_add_events
      (perfproc->papi_event_set, papi_event_codes, count);

    if (err != PAPI_OK) 
      fprintf(stderr,"PAPI error code:%i, could not add events in set. Msg: %s\n",err, PAPI_strerror(err));
    kaapi_assert(PAPI_OK == err);

    /* toggle perf and kaapi_tracelib_param as having possibliy papi event */
    perfproc->papi_event_count = papi_event_count;

    err = PAPI_start(perfproc->papi_event_set);
    if (err != PAPI_OK) 
      fprintf(stderr,"PAPI error code:%i, could not start library. Msg: %s\n",err, PAPI_strerror(err));
    
    kaapi_assert(PAPI_OK == err);
  }
#endif

  team->threads_perfctr[kid] = perfproc;
  return perfproc;
}


/*
*/
void kaapi_tracelib_thread_fini( kaapi_perfproc_t* perf, kaapi_evtproc_t* stream )
{
  if (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(KAAPI_EVT_PERFCOUNTER))
    kaapi_perf_thread_dumpcounter( stream, perf );

  if (stream && (stream->eventbuffer !=0))
  {
    kaapi_event_closebuffer(stream->eventbuffer);
    stream->eventbuffer = 0;
  }

#if defined(KAAPI_USE_PAPI)
  if ((perf->papi_event_count) && (--papi_init_count ==0))
  {
    PAPI_stop(perf->papi_event_set,
              (long_long*)(perf->curr_perf_regs + KAAPI_PERF_ID_PAPI_BASE));
    PAPI_cleanup_eventset(perf->papi_event_set);
    PAPI_destroy_eventset(&perf->papi_event_set);
    perf->papi_event_set = PAPI_NULL;
    perf->papi_event_count = 0;
    PAPI_unregister_thread();
  }
#endif
//TODO free(ctxt);
}


/*
*/
void kaapi_tracelib_thread_start( kaapi_perfproc_t* perf, kaapi_evtproc_t* stream )
{
  int mode;

  /* reset all counters in both sys/usr states */
  memset( &perf->perf_regs, 0, sizeof(perf->perf_regs) );

#if defined(KAAPI_USE_PAPI)
  if (perf->papi_event_count)
  {
    /* not that event counts between kaapi_tracelib_thread_init and here represent the
       cost to this thread wait all intialization of other threads, set setconcurrency.
       We do not taking them into account.
       Once the thread starts counting, the counters always are monotonically increasing.
       After this call we assume that we are counting.
    */
    kaapi_assert(PAPI_OK == PAPI_reset(perf->papi_event_set));
  }
#endif
  /* only to start/reset the startup time counter. */
  mode = KAAPI_GET_THREAD_STATE(perf);
  perf->start_t[mode] = (kaapi_perf_counter_t)kaapi_get_elapsedns();
  kaapi_assert_debug( perf->start_t[1-mode] == -1 ); /* else other already started... */
  kaapi_assert_debug( perf->curr_perf_regs == perf->perf_regs.state[mode]);

  KAAPI_EVENT_PUSH2(stream, 0, KAAPI_EVT_KPROC_START, stream->ptype, stream->numaid );
}


/*
*/
void kaapi_tracelib_thread_stop( kaapi_perfproc_t* perf, kaapi_evtproc_t* stream )
{
  int mode;
  kaapi_perf_counter_t delay;

  mode = KAAPI_GET_THREAD_STATE(perf);
  if (perf->start_t[mode] == -1 ) /* already stoped */
    return;

  kaapi_assert_debug( perf->start_t[mode] != -1 ); /* else none started */
  delay = (kaapi_perf_counter_t)kaapi_get_elapsedns() - perf->start_t[mode];
  KAAPI_PERFCTR_INCR(perf, KAAPI_PERF_ID_WORK, delay);
  KAAPI_PERFCTR_INCR(perf, KAAPI_PERF_ID_PTIME, delay);
  perf->start_t[mode] =-1;
#if defined(KAAPI_USE_PAPI)
  if (perf->papi_event_count)
  {
    /* in fact here we accumulate papi counter:
       the thread do not stop counting
    */
    const int err = PAPI_accum
    (
     perf->papi_event_set,
     (long_long*)(perf->curr_perf_regs + KAAPI_PERF_ID_PAPI_BASE)
    );
    kaapi_assert(PAPI_OK == err);
  }
#endif

  KAAPI_EVENT_PUSH0(stream, 0, KAAPI_EVT_KPROC_STOP );
}


/* Switch to mode state
*/
int kaapi_tracelib_thread_switchstate(
    kaapi_perfproc_t*     perfkproc,
    kaapi_evtproc_t*      evtkproc,
    int isuser
)
{
  kaapi_assert_debug( (isuser ==KAAPI_PERF_SYS_STATE)
                   || (isuser ==KAAPI_PERF_USR_STATE) );

  if (perfkproc->curr_perf_regs == perfkproc->perf_regs.state[isuser])
    return isuser;

  kaapi_perf_counter_t date;
#if defined(KAAPI_USE_PAPI)
  if ((perfkproc->papi_event_count) && perfkproc->papi_event_set)
  {
    /* we accumulate papi counter in previous mode
       do not reset as in kaapi_tracelib_thread_start
       because PAPI_accum does that
    */
    const int err = PAPI_accum(
      perfkproc->papi_event_set,
      (long_long*)(perfkproc->curr_perf_regs + KAAPI_PERF_ID_PAPI_BASE)
    );
    kaapi_assert(PAPI_OK == err);
  }
#endif
  /* doit only iff real swap */
  kaapi_assert_debug( perfkproc->start_t[isuser] == -1 );   /* else already started */
  date = (kaapi_perf_counter_t)kaapi_get_elapsedns();
  if (perfkproc->start_t[1-isuser] != -1 ) /* else none started */
  {
    KAAPI_PERF_REG(perfkproc, KAAPI_PERF_ID_WORK) += date - perfkproc->start_t[1-isuser];
    KAAPI_PERF_REG(perfkproc, KAAPI_PERF_ID_PTIME) += date - perfkproc->start_t[1-isuser];
    perfkproc->start_t[1-isuser] = -1;
  }
  perfkproc->start_t[isuser] = date;
  perfkproc->curr_perf_regs  = perfkproc->perf_regs.state[isuser];

  if (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(KAAPI_EVT_PERFCOUNTER))
    kaapi_perf_thread_dumpcounter( evtkproc, perfkproc );

  return 1-isuser;
}


/*
 * Performance counter:
 * -> reference value / kproc, monotone
 * -> reset = 0
 * -> delta = T1 - T0 = read(T1)
*/


/*
*/
__thread uint64_t task_id = 0;
kaapi_task_id_t kaapi_tracelib_newtask_id(void)
{
  kaapi_task_id_t value = (kaapi_task_id_t)(++task_id | (((uint64_t)pthread_self()) << 16));
  return value;
}


/* */
void kaapi_tracelib_task_begin(
    kaapi_perfproc_t*     perfkproc,
    kaapi_evtproc_t*      evtkproc,
    kaapi_perf_counter_t* parent_perfctr0,
    kaapi_perf_counter_t* parent_perfctr,
    kaapi_perf_counter_t* task_perfctr0,
    void* task,
    int typeid,
    int isexplicit
)
{
  if (kaapi_tracelib_param.taskperfctr_idset)
  {
    kaapi_perflib_read_registers( perfkproc,
                                  &kaapi_tracelib_param.taskperfctr_idset,
                                  task_perfctr0);
    if ((parent_perfctr !=0)&&(parent_perfctr0 !=0))
      /* parent_perfctr += (task_perfctr-parent_perfctr0) */
      kaapi_mt_perf_addsub_counters(
        &kaapi_tracelib_param.taskperfctr_idset,
        parent_perfctr,
        task_perfctr0,
        parent_perfctr0
      );
  }

  KAAPI_EVENT_PUSH3(evtkproc, 0, KAAPI_EVT_TASK_BEG, task, typeid, isexplicit );
}

/* */
uint64_t kaapi_tracelib_task_end(
    kaapi_perfproc_t*      perfkproc,
    kaapi_evtproc_t*       evtkproc,
    int                    gtid,
    kaapi_named_perfctr_t* named_perfctr,
    kaapi_perf_counter_t*  parent_perfctr0,
    kaapi_perf_counter_t*  start_perfctr,
    kaapi_perf_counter_t*  task_perfctr,
    void* task,
    uint64_t Tinf
)
{
  if (kaapi_tracelib_param.taskperfctr_idset)
  {
    /* accumulate in task_perfctr += current - start, store current in parent0 if not 0 */
    kaapi_perflib_readsub_registers( perfkproc,
                                     &kaapi_tracelib_param.taskperfctr_idset,
                                     parent_perfctr0,
                                     start_perfctr,
                                     task_perfctr
    );

    /* compute Tinf counter: */
    if (kaapi_tracelib_param.taskperfctr_idset & KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TINF))
    {
      kaapi_perf_idset_t sset = kaapi_tracelib_param.taskperfctr_idset & (KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_TINF)-1);
      int idxtinf = __builtin_popcountl(sset);
      sset = kaapi_tracelib_param.taskperfctr_idset & (KAAPI_PERF_ID_MASK(KAAPI_PERF_ID_WORK)-1);
      int idxtwork = __builtin_popcountl(sset);
      task_perfctr[idxtinf] = Tinf + task_perfctr[idxtwork];
      Tinf = (uint32_t)task_perfctr[idxtinf];
    }
    if (named_perfctr !=0)
      kaapi_perflib_update_perf_stat(
        gtid,
        named_perfctr,
        &kaapi_tracelib_param.taskperfctr_idset,
        task_perfctr
      );
  }
  KAAPI_EVENT_PUSH_PERFCTR(evtkproc, 0, task, &kaapi_tracelib_param.taskperfctr_idset, task_perfctr );
  KAAPI_EVENT_PUSH1(evtkproc, 0, KAAPI_EVT_TASK_END, task );
  KAAPI_PERFCTR_INCR(perfkproc, KAAPI_PERF_ID_TASKEXEC, 1);
#if 0
  printf("%p::end task: #task=%i, #spawn=%i\n",
    ctxt,
    KAAPI_PERF_REG_READALL(perf->perf_regs,KAAPI_PERF_ID_TASKEXEC),
    KAAPI_PERF_REG_READALL(perf->perf_regs,KAAPI_PERF_ID_TASKSPAWN)
  );
#endif
  return Tinf;
}



/*
*/
kaapi_descrformat_t* kaapi_tracelib_register_fmtdescr(
    int implicit,
    void* (*routine)(),
    const char* prefix_name,
    const char* func_name,
    const char* (*filter_func)(const char*)
)
{
  void* key = (void*)(uintptr_t)routine;
  kaapi_hashentries_t* entry = kaapi_hashmap_find( &fdescr_map_routine, key );
  if (entry != 0)
    return KAAPI_HASHENTRIES_GET(entry, kaapi_descrformat_t*);

  kaapi_atomic_lock(&fdescr_map_lock);
  entry = kaapi_hashmap_find( &fdescr_map_routine, key );
  if (entry != 0)
  {
    kaapi_descrformat_t* pdescr =KAAPI_HASHENTRIES_GET(entry, kaapi_descrformat_t*);
    kaapi_atomic_unlock(&fdescr_map_lock);
    return pdescr;
  }

  /* kaapi_tracelib_reserve_perfcounter recopy descr in kaapi_tracelib_param.fmt_list array*/
  kaapi_descrformat_t* fdescr = kaapi_tracelib_reserve_perfcounter();
  fdescr->fmtid = (int)(uintptr_t)routine;
  fdescr->implicit = implicit;
  fdescr->color = "0 0.5 1.0";
  fdescr->parent  = 0;
  fdescr->firstchild = 0; /* compute at the end from parent relation ship */
  fdescr->lastchild = 0; /* idem */
  fdescr->sibling = 0; /* idem */
  char buffer [128];
  if (func_name ==0)
  {
    snprintf(buffer,128,"%s_%i/%p", (prefix_name ? prefix_name : "omp_task"), fdescr->fmtid, (void*)routine );
    fdescr->name = strdup(buffer);
  }
  else
  {
    snprintf(buffer,128,"%s%s/%p", (prefix_name ? prefix_name : ""), 
             (filter_func ? filter_func(func_name) : func_name), (void*)routine);
    fdescr->name = strdup(buffer);
  }
  fdescr->perfctr->name = fdescr->name; /* to avoid */
  entry = kaapi_hashmap_insert( &fdescr_map_routine, key );
  KAAPI_HASHENTRIES_SET(entry, fdescr, kaapi_descrformat_t*);
  kaapi_atomic_unlock(&fdescr_map_lock);
  return fdescr;
}

/*
*/
kaapi_descrformat_t* kaapi_tracelib_reserve_perfcounter(void)
{
  kaapi_named_perfctr_t* perf = 0;
  kaapi_descrformat_t* retval = 0;

  perf = calloc(1,sizeof(kaapi_named_perfctr_t));
  perf->kproc_stats = calloc(256,sizeof(kaapi_perfstat_t*)); /* at most 256 processors ! */

  if (kaapi_tracelib_param.fmt_listsize+1 >= fmt_listcapacity)
  {
    if (fmt_listcapacity ==0) fmt_listcapacity = 2;
    kaapi_tracelib_param.fmt_list = realloc( kaapi_tracelib_param.fmt_list, 2*fmt_listcapacity*sizeof(kaapi_descrformat_t) );
    fmt_listcapacity *= 2;
  }

  retval = (kaapi_descrformat_t*)malloc(sizeof(kaapi_descrformat_t));
  retval->implicit = 0;
  retval->fmtid    = 0;
  retval->name     = 0;
  retval->parent   = 0;
  retval->color    = 0;
  retval->perfctr   = perf;
  kaapi_tracelib_param.fmt_list[kaapi_tracelib_param.fmt_listsize] = retval;
  ++kaapi_tracelib_param.fmt_listsize;

  kaapi_assert(retval != 0);
  return retval;
}



/*
*/
static void kaapi_perf_thread_dumpcounter( kaapi_evtproc_t* evtkproc, kaapi_perfproc_t* perf )
{
  /* dump performance counter at the same date ! */
  if (kaapi_tracelib_param.eventmask)
  {
    if (perf && evtkproc && evtkproc->eventbuffer && (kaapi_tracelib_param.eventmask & KAAPI_EVT_MASK(KAAPI_EVT_PERFCOUNTER)))
    {
      int i;
      uint64_t tclock = kaapi_event_date();        
      for (i=0; i<kaapi_tracelib_count_perfctr(); ++i)
      {
        KAAPI_EVENT_PUSH3_AT(evtkproc, tclock, 0, KAAPI_EVT_PERFCOUNTER,
            (uint64_t)(i), 
            (uint64_t)KAAPI_PERF_REG_USR(perf->perf_regs, i),
            (uint64_t)KAAPI_PERF_REG_SYS(perf->perf_regs, i)
        );
      }
    }
  }
}


/**
*/
void _kaapi_signal_dump_counters(int xxdummy)
{
  kaapi_event_fencebuffers();
  _exit(-1);
}


/* This function should be called first to declare papi event counted before user defined counters
*/
kaapi_perf_id_t kaapi_tracelib_create_perfid( const char* name, kaapi_perf_counter_t (*read)(void*), void* ctxt )
{
  kaapi_perf_id_t id = KAAPI_PERF_ID_PAPI_BASE+user_event_count;
  if (id >= KAAPI_PERF_ID_MAX) return -1;
  kaapi_perfctr_info[id].name = strdup(name);
  kaapi_perfctr_info[id].type = KAAPI_PCTR_USER; /* except if redefined, as for PAPI */
  kaapi_perfctr_info[id].read = read;
  kaapi_perfctr_info[id].ctxt = ctxt;
  ++user_event_count;
  return id;
}


/*
*/
const char* kaapi_tracelib_perfid_to_name(kaapi_perf_id_t id)
{
  kaapi_assert_debug( (0 <= id) && (id <KAAPI_PERF_ID_MAX) );
  return kaapi_perfctr_info[id].name;
}


/* Used to loop over all perfcounters in set
*/
size_t kaapi_tracelib_count_perfctr(void)
{
  return KAAPI_PERF_ID_PAPI_BASE + papi_event_count;
}


/* fwd decl */
static int get_event_code(char* name, int* code, uint32_t* type);

/* initialize global field kaapi_event, kaapi_name, kaapi_event_count
*/
static int kaapi_get_events(
  const char* env,
  int task_set
)
{
  /* todo: [u|k]:EVENT_NAME */
  unsigned int i = 0;
  unsigned int j;
  const char* s = 0;
  char name[64];
  int event_code;

  s = getenv(env);
  if (s == NULL)
    return 0;

  if (task_set)
    kaapi_perf_idset_zero( &kaapi_tracelib_param.taskperfctr_idset );

  while (*s)
  {
    uint32_t type = 0;

    for (j = 0; j < (sizeof(name) - 1) && *s && (*s != ','); ++s, ++j)
      name[j] = *s;
    name[j] = 0;

    if (get_event_code(name, &event_code, &type ) != 0)
      return -1;


    /* Register PAPI counter to be at KAAPI_PERF_ID_PAPI_BASE+cnt in kaapi_perfctr_info
    */
    if (type == KAAPI_PCTR_PAPI)
    {
      kaapi_perf_id_t newid = kaapi_tracelib_create_perfid( strdup(name), 0, 0 );
      if (newid == -1)
      {
        fprintf(stderr,"*** Kaapi: too many hardware counters defined while adding event from '%s'\n", env);
        return -1;
      }
      kaapi_perfctr_info[newid].type = KAAPI_PCTR_PAPI;
      kaapi_perfctr_info[newid].eventcode = event_code;
      kaapi_perf_idset_add( &kaapi_tracelib_param.perfctr_idset, newid );
      if (task_set)
        kaapi_perf_idset_add( &kaapi_tracelib_param.taskperfctr_idset, newid );
      papi_event_mask |= KAAPI_PERF_ID_MASK(newid);
      ++papi_event_count;
      if (papi_event_count >= KAAPI_MAX_HWCOUNTERS)
      {
        fprintf(stderr,"*** Kaapi: too many hardware counters defined while adding event from '%s'\n", env);
        kaapi_abort(__LINE__, __FILE__, "*** aborting");
      }
    }
    else if (type == KAAPI_PCTR_LIBRARY)
    {
      if (event_code <KAAPI_PERF_ID_MAX)
      {
        kaapi_perfctr_info[event_code].type = KAAPI_PCTR_LIBRARY;
        kaapi_perfctr_info[event_code].eventcode = event_code;
        kaapi_perf_idset_add( &kaapi_tracelib_param.perfctr_idset, event_code );
        if (task_set)
          kaapi_perf_idset_add( &kaapi_tracelib_param.taskperfctr_idset, event_code );
      }
      else { /* group event */
        event_code -= KAAPI_PERF_ID_MAX;
        kaapi_perf_idset_addset( &kaapi_tracelib_param.perfctr_idset, kaapi_perfctr_group[event_code].mask);
        if (task_set)
          kaapi_perf_idset_addset( &kaapi_tracelib_param.taskperfctr_idset, kaapi_perfctr_group[event_code].mask_pertask);
      }
    }
    ++i;

    if (*s == 0)
      break;

    ++s;
  }
  return 0;
}

/* Find event name '<name>' or 'KAAPI_<name>' in list of predefined events
   Return -1 if not found
*/
int get_kaapi_code( char* name )
{
  int i;
  /* group of events */
  if ((strcasecmp(name, "KAAPI_TASK") ==0) || (strcasecmp(name, "TASK") ==0)) /* group */
    return KAAPI_PERF_ID_MAX+KAAPI_PERF_GROUP_TASK;
  else if ((strcasecmp(name, "KAAPI_SCHED") ==0) || (strcasecmp(name, "SCHED") ==0)) /* group */
    return KAAPI_PERF_ID_MAX+KAAPI_PERF_GROUP_STEAL;
  else if (strcasecmp(name, "KAAPI_OFFLOAD") ==0) /* group */
    return KAAPI_PERF_ID_MAX+KAAPI_PERF_GROUP_OFFLOAD;
  else if ((strcasecmp(name, "KAAPI_DFG") ==0) || (strcasecmp(name, "DFG") ==0)) /* group */
    return KAAPI_PERF_ID_MAX+KAAPI_PERF_GROUP_DFGBUILD;
  else if ((strcasecmp(name, "KOMP") ==0) || (strcasecmp(name, "OPENMP") ==0)) /* group */
    return KAAPI_PERF_ID_MAX+KAAPI_PERF_GROUP_OMP;

  /* specific event name ? */
  for (i=0; i<sizeof(kaapi_perfctr_info)/sizeof(kaapi_perfctr_info_t); ++i)
  {
    if (kaapi_perfctr_info[i].cmdlinename ==0) continue;
    if (strcasecmp(kaapi_perfctr_info[i].cmdlinename,name) ==0)
      return kaapi_perfctr_info[i].eventcode;
    if (strncasecmp(name, "KAAPI_", 6) ==0)
    {
      if (strcasecmp(kaapi_perfctr_info[i].cmdlinename,name+6) ==0)
        return kaapi_perfctr_info[i].eventcode;
    }
  }
  return -1;
}

/* any update in this list of event => update README.envvars */
static int get_event_code(char* name, int* code, uint32_t* type)
{
  *code = -1;
  if (strncasecmp(name, "PAPI", 4) !=0)
  {
    int c = get_kaapi_code(name);
    if (c != -1)
    {
      *code = c;
      *type = KAAPI_PCTR_LIBRARY;
      return 0;
    }
    /* if name start with KAAPI_ but not known -> warning */
    if (strncasecmp(name, "KAAPI_", 5) ==0)
    {
      fprintf(stderr, "*** KAAPI bad event name:%s\n", name);
      return -1;
    }
  }
  /* else assume that its PAPI counter (included native counter) */
#if defined(KAAPI_USE_PAPI)
  int err = PAPI_event_name_to_code(name, code);
  if (err != PAPI_OK)
  {
    fprintf(stderr, "*** PAPI error code:%i\n", err);
    return -1;
  }
  *type = KAAPI_PCTR_PAPI;
  return 0;
#else
  fprintf(stderr, "*** KAAPI bad event name:%s. May be because PAPI is not configured\n", name);
  return -1;
#endif
}


/* ========================================================================= */
/* Accumulation of counters
*/
/* ========================================================================= */

/*
*/
void kaapi_mt_perf_global_add(
  const kaapi_thread_perfctr_t* accum
)
{
  /* cumulate accumulation */
  kaapi_mt_perf_add_threadcounters( global_perf_regs, accum );
}


#if 0 // unused ?
/*
*/
void kaapi_mt_perf_accum_registers(
  const kaapi_perfproc_t* perf,
  const kaapi_perf_idset_t* idset,
  kaapi_perf_counter_t* counter
)
{
  kaapi_assert_debug( sizeof(*idset) == 64/8 ); /* __builtin functions on 64 bits */

  /* ready first software counters, then other are papi counter if papi_event_mask is !=0 */
  unsigned int i;
  kaapi_perf_idset_t set = *idset & ((1UL << KAAPI_PERF_ID_ENDSOFTWARE) -1);

  while (set !=0)
  {
    i = __builtin_ffsl( set )-1;
    set &= ~(1UL << i);
    if ((i==KAAPI_PERF_ID_WORK) || (i ==KAAPI_PERF_ID_PTIME))
    {
      kaapi_perf_counter_t date = kaapi_get_elapsedns();
      *counter++ += (date - perf->curr_perf_regs[i]);
      perf->curr_perf_regs[i] = date;
    }
    else
      *counter++ += perf->curr_perf_regs[i];
  }

#if defined(KAAPI_USE_PAPI)
  if (*idset & papi_event_mask)
  {
    /* not that event counts between kaapi_tracelib_thread_init and here represent the
       cost to this thread wait all intialization of other threads, set setconcurrency.
       After this call we assume that we are counting.
    */
    kaapi_assert(PAPI_OK == PAPI_accum(perf->papi_event_set, (long_long*)counter));
    counter += papi_event_count;
  }
#endif

  set = *idset & ~(papi_event_mask| ((1UL<<KAAPI_PERF_ID_ENDSOFTWARE)-1));
  while (set !=0)
  {
    i = __builtin_ffsl( set )-1;
    set &= ~(1UL << i);
    *counter++ = kaapi_perfctr_info[i].read( kaapi_perfctr_info[i].ctxt );
  }
}
#endif


/*
*/
void kaapi_perflib_read_registers(
  kaapi_perfproc_t*         perf,
  const kaapi_perf_idset_t* idset,
  kaapi_perf_counter_t*     counter
)
{
  kaapi_assert_debug( sizeof(*idset) == 64/8 ); /* __builtin functions on 64 bits */

  /* ready first software counters, then other are papi counter if papi_event_mask is !=0 */
  unsigned int i;
  kaapi_perf_idset_t set = *idset & ((1UL << KAAPI_PERF_ID_ENDSOFTWARE) -1);

  while (set !=0)
  {
    i = __builtin_ffsl( set )-1;
    set &= ~(1UL << i);
    if ((i==KAAPI_PERF_ID_WORK) || (i ==KAAPI_PERF_ID_PTIME))
      *counter++ = kaapi_get_elapsedns(); 
    else
      *counter++ = perf->curr_perf_regs[i];
  }

#if defined(KAAPI_USE_PAPI)
  if (*idset & papi_event_mask)
  {
    /* not that event counts between kaapi_tracelib_thread_init and here represent the
       cost to this thread wait all intialization of other threads, set setconcurrency.
       After this call we assume that we are counting.
    */
    kaapi_assert(PAPI_OK == PAPI_read(perf->papi_event_set, (long_long*)counter));
    counter += papi_event_count;
  }
#endif
  /* other counter, defined by function */
  set = *idset & ~(papi_event_mask | ((1UL<<KAAPI_PERF_ID_ENDSOFTWARE)-1));
  while (set !=0)
  {
    i = __builtin_ffsl( set )-1;
    set &= ~(1UL << i);
    *counter++ = kaapi_perfctr_info[i].read( kaapi_perfctr_info[i].ctxt );
  }
}



/*
*/
void kaapi_perflib_readsub_registers(
  kaapi_perfproc_t*           perf,
  const kaapi_perf_idset_t*   idset,
  kaapi_perf_counter_t*       read,
  const kaapi_perf_counter_t* start,
  kaapi_perf_counter_t*       result
)
{
  kaapi_assert_debug( sizeof(*idset) == 64/8 ); /* __builtin functions on 64 bits */

  kaapi_assert( start != 0);
  kaapi_assert( result != 0);

  if (read)
  {
    kaapi_perflib_read_registers(perf, idset, read);
    kaapi_mt_perf_addsub_counters(idset, result, read, start);
  }
  else
  {
    kaapi_perf_counter_t read[kaapi_perf_idset_size(idset)];
    kaapi_perflib_read_registers(perf, idset, read);
    kaapi_mt_perf_addsub_counters(idset, result, read, start);
  }
}


/* Update stats perf task type (==same format)
*/
void kaapi_perflib_update_perf_stat(
  int                    kid,
  kaapi_named_perfctr_t* perf,
  const kaapi_perf_idset_t* idset,
  kaapi_perf_counter_t*  counters
)
{
  unsigned int i;
  unsigned int idx;
  unsigned int count = kaapi_perf_idset_size(idset);
  kaapi_perf_idset_t set = *idset;

  kaapi_perfstat_t* stats = perf->kproc_stats[kid];
  if (stats == 0)
  {
    perf->kproc_stats[kid] = stats = malloc( sizeof(kaapi_perfstat_t) );
    memset(perf->kproc_stats[kid], 0, sizeof(kaapi_perfstat_t) );
  }

  for (i=0; i<count; ++i)
  {
    idx = __builtin_ffsl( set )-1;
    set &= ~(1UL << idx);
    if (stats->max_counters[idx] < counters[i])
      stats->max_counters[idx] =  counters[i];
    stats->sum_counters[idx]  += counters[i];
    /* pass to double for sum2 ? */
    stats->sum2_counters[idx] += counters[i] * counters[i];
  }
  ++stats->count;
}

static void kaapi_mt_perf_accum_perf_stat(
  kaapi_named_perfctr_t*    perf,
  const kaapi_perf_idset_t* idset
)
{
  unsigned int i;
  unsigned int idx;
  kaapi_perfstat_t* stats = &perf->stats;
  for (i=0; i<256; ++i)
  {
    kaapi_perfstat_t* a = perf->kproc_stats[i];
    if (a ==0) continue;

    kaapi_perf_idset_t set = *idset;
    while (set !=0)
    {
      idx = __builtin_ffsl( set )-1;
      set &= ~(1UL << idx);
      if (stats->max_counters[idx] < a->max_counters[idx])
        stats->max_counters[idx] =  a->max_counters[idx];
      stats->sum_counters[idx] += a->sum_counters[idx];
      stats->sum2_counters[idx] += a->sum2_counters[idx];
    }
    stats->count += a->count;
  }
}


/* Do: a += (b-c);
*/
static void kaapi_mt_perf_addsub_counters(
  const kaapi_perf_idset_t*       idset,
  kaapi_perf_counter_t*           a,
  const kaapi_perf_counter_t*     b,
  const kaapi_perf_counter_t*     c
)
{
  kaapi_assert( a != 0);
  kaapi_assert( b != 0);
  kaapi_assert( c != 0);

  unsigned int i;
  unsigned int count = kaapi_perf_idset_size(idset);
  for (i=0; i<count; ++i)
    a[i] += (b[i] - c[i]);
}

#if 0 // unused

/*
*/
void kaapi_mt_perf_max_counters(
  const kaapi_perf_idset_t* idset,
  kaapi_perf_counter_t* result,
  const kaapi_perf_counter_t* operand
)
{
  unsigned int i;
  unsigned int count = kaapi_perf_idset_size(idset);
  for (i=0; i<count; ++i)
    if (result[i] < operand[i]) result[i] = operand[i];
}
#endif

#if 0 // unused
/*
*/
void kaapi_mt_perf_add_counters(
  const kaapi_perf_idset_t* idset,
  kaapi_perf_counter_t* result,
  const kaapi_perf_counter_t* operand
)
{
  unsigned int i;
  unsigned int count = kaapi_perf_idset_size(idset);
  for (i=0; i<count; ++i)
    result[i] += operand[i];
}
#endif

/*
*/
void kaapi_mt_perf_add_threadcounters(
  kaapi_thread_perfctr_t* counter,
  const kaapi_thread_perfctr_t* v
)
{
  unsigned int s;
  unsigned int i;
  for (s=0; s<2; ++s) /* 2 states: usr + sys */
  {
    kaapi_perf_counter_t* pc = &counter->state[s][0];
    const kaapi_perf_counter_t* pv = &v->state[s][0];
    for (i=0; i<kaapi_tracelib_count_perfctr(); ++i)
      if (kaapi_perfctr_info[i].opaccum ==0)
        pc[i] += pv[i];
      else if (kaapi_perfctr_info[i].opaccum ==1)
      {
        if (pc[i] < pv[i])
          pc[i] = pv[i];
      }
      else kaapi_assert(0);
  }
}



/*
*/
void kaapi_mt_perf_addreset_threadcounters(
  kaapi_thread_perfctr_t* counter,
  kaapi_thread_perfctr_t* v
)
{
  unsigned int s;
  unsigned int i;
  for (s=0; s<2; ++s) /* 2 states: usr + sys */
  {
    kaapi_perf_counter_t* pc = &counter->state[s][0];
    kaapi_perf_counter_t* pv = &v->state[s][0];
    for (i=0; i<kaapi_tracelib_count_perfctr(); ++i)
    {
      kaapi_perf_counter_t k = pv[i];
      pv[i] = 0;
      pc[i] += k;
    }
  }
}

#if 0

/*
*/
void kaapi_mt_perf_cpy_counters(
  const kaapi_perf_idset_t* idset,
  kaapi_perf_counter_t* result,
  const kaapi_perf_counter_t* operand
)
{
  unsigned int i;
  unsigned int count = kaapi_perf_idset_size(idset);
  for (i=0; i<count; ++i)
    result[i] = operand[i];
}
#endif


/*
*/
void kaapi_display_rawperf( FILE* file, const kaapi_thread_perfctr_t* counter )
{
  unsigned int j;
  for (j=0; j< kaapi_tracelib_count_perfctr(); ++j)
  {
    if (!kaapi_perf_idset_test(&kaapi_tracelib_param.perfctr_idset, j))
      continue;
    if (kaapi_perfctr_info[j].ns2s)
      fprintf(file, "%20.20s     (sys/usr): %e, %e\n",
        kaapi_perfctr_info[j].name,
        (double)(*counter).state[KAAPI_PERF_SYS_STATE][j]*1e-9,
        (double)(*counter).state[KAAPI_PERF_USR_STATE][j]*1e-9
      );
    else
      fprintf(file, "%20.20s     (sys/usr): %"PRIi64 ", %" PRIi64 "\n",
        kaapi_perfctr_info[j].name,
        (*counter).state[KAAPI_PERF_SYS_STATE][j],
        (*counter).state[KAAPI_PERF_USR_STATE][j]
      );
  }
}
