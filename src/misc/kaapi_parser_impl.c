/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** francois.broquedis@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <limits.h>
#include "kaapi_impl.h"

/*
 */
static inline int is_range_delim(const int c)
{
  return (c == ':') || (c == '-');
}

/*
 */
static inline int is_delim_stride(const int c)
{
  return (c == ':');
}

/* end of list
 */
static inline int is_eol(const int c)
{
  /* end of list */
  return (c == ',') || (c == 0);
}

/*
*/
static void eat_space(char** str)
{
  while ((*str !=0) && isspace ((int)**str))
    ++*str;
}


/* parse PROC_BIND value: false,true,master,close,spread and
   return the numerical value komp_proc_bind_t
*/
bool kaapi_parse_procbind (char** str, kaapi_procbind_t* procbind )
{
  if (**str == 0)
    return false;

  eat_space(str);

  if (strncasecmp(*str,"false", 5) ==0)
  {
    *procbind = KAAPI_PROCBIND_FALSE;
    *str += 5;
  }
  else if (strncasecmp(*str,"true", 4) ==0)
  {
    *procbind = KAAPI_PROCBIND_TRUE;
    *str += 4;
  }
  else if (strncasecmp(*str,"master", 6) ==0)
  {
    *procbind = KAAPI_PROCBIND_MASTER;
    *str += 6;
  }
  else if (strncasecmp(*str,"close", 5) ==0)
  {
    *procbind = KAAPI_PROCBIND_CLOSE;
    *str += 5;
  }
  else if (strncasecmp(*str,"spread", 6) ==0)
  {
    *procbind = KAAPI_PROCBIND_SPREAD;
    *str += 6;
  }
  else
    return false;
  return true;
}

/* Parse a list of kaapi_procbind_t.  Return true if one was
   present and it was successfully parsed.  
*/
bool
kaapi_parse_list_procbind (char** str, unsigned int* count, kaapi_procbind_t **pvalue)
{
  char *token, *context;
  const char *sep = ",";
  kaapi_procbind_t value;
  kaapi_procbind_t* list = 0;
  int size = 0;

  if (**str == 0)
    return false;

  eat_space(str);

  for (token = strtok_r(*str, sep, &context);
       token;
       token = strtok_r(NULL, sep, &context))
  {
    *str = token;
    if (!kaapi_parse_procbind(str, &value))
      goto invalid;

    if (*count+2 > size)
    {
      size = 2*size;
      if (size < *count+2) size = (*count+2)*2;
      list = (kaapi_procbind_t*)realloc( list, size*sizeof(kaapi_procbind_t));
    }
    list[*count] = value;
    ++*count;
  }

  return true;

invalid:
  if (list !=0) free(list);
  return false;
}


/* Parse the ccsync|noaggr.
   Initialize the steal protocol in the runtime parameter rt_param + initialization and 
   dstor function.
*/
bool
kaapi_parse_stealprotocol (char** str, kaapi_rtparam_t* rt_param)
{
  if (*str == NULL)
    return false;

  eat_space(str);
  if (strncasecmp (*str, "ccsync", 6) == 0)
  {
    rt_param->emitsteal          = kaapi_sched_emitsteal;
    rt_param->emitsteal_initctxt = kaapi_sched_ccsync_emitsteal_init;
    rt_param->emitsteal_dstorctxt= kaapi_sched_ccsync_emitsteal_dstor;
    rt_param->request_post_fnc   = kaapi_sched_ccsync_post_request;
    rt_param->request_commit_fnc = kaapi_sched_ccsync_commit_request;
    rt_param->request_commit_fnc_async = kaapi_sched_ccsync_commit_request_async;
    rt_param->request_reply_fnc  = kaapi_request_ccsync_reply;
    rt_param->lr_empty_fnc       = kaapi_listrequest_ccsync_iterator_empty;
    rt_param->lr_get_fnc         = kaapi_listrequest_ccsync_iterator_get;
    rt_param->lr_next_fnc        = kaapi_listrequest_ccsync_iterator_next;
    rt_param->lr_count_fnc       = kaapi_listrequest_ccsync_iterator_count;
    rt_param->pgo_init           = kaapi_sched_ccsync_pgo_init;
    rt_param->pgo_wait           = kaapi_sched_ccsync_pgo_wait;
    rt_param->pgo_fini           = kaapi_sched_ccsync_pgo_fini;

    *str += 6;
    return true;
  }
  else if (strncasecmp (*str, "qlock", 5) == 0)
  {
    rt_param->emitsteal          = kaapi_sched_emitsteal;
    rt_param->emitsteal_initctxt = kaapi_sched_qlock_emitsteal_init;
    rt_param->emitsteal_dstorctxt= kaapi_sched_qlock_emitsteal_dstor;
    rt_param->request_post_fnc   = kaapi_sched_qlock_post_request;
    rt_param->request_commit_fnc = kaapi_sched_qlock_commit_request;
    rt_param->request_commit_fnc_async = 0;
    rt_param->request_reply_fnc  = kaapi_request_qlock_reply;
    rt_param->lr_empty_fnc       = kaapi_listrequest_qlock_iterator_empty;
    rt_param->lr_get_fnc         = kaapi_listrequest_qlock_iterator_get;
    rt_param->lr_next_fnc        = kaapi_listrequest_qlock_iterator_next;
    rt_param->lr_count_fnc       = kaapi_listrequest_qlock_iterator_count;
    rt_param->pgo_init           = 0;
    rt_param->pgo_wait           = 0;
    rt_param->pgo_fini           = 0;

    *str += 5;
    return true;
  }
  else if (strncasecmp (*str, "null", 4) == 0)
  {
    rt_param->emitsteal          = 0;
    rt_param->emitsteal_initctxt = 0;
    rt_param->emitsteal_dstorctxt= 0;
    rt_param->request_post_fnc   = 0;
    rt_param->request_commit_fnc = 0;
    rt_param->request_commit_fnc_async = 0;
    rt_param->request_reply_fnc  = 0;
    rt_param->lr_empty_fnc       = 0;
    rt_param->lr_get_fnc         = 0;
    rt_param->lr_next_fnc        = 0;
    rt_param->lr_count_fnc       = 0;
    rt_param->pgo_init           = 0;
    rt_param->pgo_wait           = 0;
    rt_param->pgo_fini           = 0;

    *str += 4;
    return true;
  }

  return false;
}



bool
kaapi_parse_schedule (
  char** str,
  kaapi_foreach_attr_policy_t* prun_sched,
  int* prun_sched_modifier
)
{
  int size;
  int run_sched_modifier = 0;
  if (*str == NULL)
    return false;

  eat_space(str);
  char* policy = strsep(str, ",");
  if (*str == 0)
  { /* no sep */
    run_sched_modifier = 0;
    *str = policy;
  }
  else if (!kaapi_parse_int( str, &run_sched_modifier ))
    return false;

  if (strncasecmp (policy, "static",6) == 0)
  { *prun_sched = KAAPI_FOREACH_SCHED_STATIC; size = 6; }
  else if (strncasecmp (policy, "dynamic", 7) == 0)
  { *prun_sched = KAAPI_FOREACH_SCHED_DYNAMIC; size = 7; }
  else if (strncasecmp (policy, "guided",6) == 0)
  { *prun_sched = KAAPI_FOREACH_SCHED_GUIDED; size = 6; }
  else if (strncasecmp (policy, "adaptive",8) == 0)
  { *prun_sched = KAAPI_FOREACH_SCHED_ADAPTIVE; size = 8; }
  else if (strncasecmp (policy, "steal",5) == 0)
  { *prun_sched = KAAPI_FOREACH_SCHED_STEAL; size = 5; }
  else if (strncasecmp (policy, "auto",4) == 0)
  { *prun_sched = KAAPI_FOREACH_SCHED_AUTO; size = 4; }
  else
    return false;
  *prun_sched_modifier = run_sched_modifier;
  if (policy == *str)
    *str += size;
  return true;
}



/** Parse :
    [!] [ low ] [ -|: [ high ] [: stride] ]

    low -> num | <empty>
    high -> num | <empty>
    if empty detected for low, high then it set value to (unsigned int)-1.
    If stride pointer is not null and no stride is specified, then stride is set to 1.
    If stride pointer is null and a stride is specified, then it is an error
    If low '-' high then, low <= high.
    Else if format is low ':' high, then this is the OpenMP format with low:count.
 */
bool kaapi_parse_range(
    char** str,
    unsigned int* index_low,
    unsigned int* index_high,
    int* stride
)
{
  char range_delim = ' ';
  if (isdigit(**str))
  {
    if (!kaapi_parse_unsigned_int(str, index_low))
      return false;
  }
  else
  {
    if (!is_range_delim(**str)) /* - or : */
      return false;
    *index_low = (unsigned int)-1;
  }

  *index_high = *index_low;
  if (stride !=0)
    *stride = 1;

  if (is_range_delim(**str)) /* - or : */
  {
    range_delim = **str;
    ++*str;

    /* 1 token look ahead */
    if (is_eol(**str))
      *index_high = (unsigned int)-1;
    else
    {
      if (!kaapi_parse_unsigned_int(str, index_high))
        return false;

      if ((stride ==0) && is_delim_stride(**str))
        return false;

      if (is_delim_stride(**str))
      {
        ++*str;
        if (is_eol(**str))
          *stride = 1;
        if (!kaapi_parse_int(str, stride))
          return false;
      }
    }
  }

  /* test if empty set (index_low-index_high:stride)
     or *stride ==0.
  */
  if (range_delim == ':')  
  {
    if (*index_high == -1) *index_high = 1;
    if (*index_high < 1) return false;
    *index_high = *index_low + (*index_high -1) * (stride == 0 ? 1 : *stride);
  }

  if ((*index_low > *index_high) || ((stride !=0) && (*stride ==0)))
    return false;

  return true;
}


/* range or list of number
*/
bool kaapi_parse_oneplace(
    char** str,
    unsigned int* cpu_count,
    kaapi_cpuset_t* place
)
{
  if (*str ==0)
    return false;

  *cpu_count = 0;

  /* parse list */
  int negate;
  unsigned int low;
  unsigned int high;
  int stride;

#if defined(__linux__) && defined(KAAPI_USE_SCHED_AFFINITY)
  kaapi_cpuset_t set;
  kaapi_cpuset_t negateset;
#endif

#if defined(__linux__) && defined(KAAPI_USE_SCHED_AFFINITY)
  KAAPI_CPUSET_ZERO( place );
  KAAPI_CPUSET_ZERO( &set );
  KAAPI_CPUSET_ZERO( &negateset );
#endif
  while (1)
  {
    eat_space(str);
    if (**str == '!')
    {
      negate = 1;
      ++*str;
    }
    else negate = 0;

    /* numa(i), socket(id), core(id) thread(id) extensions to OMP_PLACES
    */
    if (isalpha(**str))
    {
      kaapi_hws_levelid_t hid = KAAPI_HWS_LEVELID_MAX;
      unsigned id;

      if (strncasecmp (*str, "thread", 6) == 0)
      {
        *str += 6;
        hid = KAAPI_HWS_LEVELID_THREAD;
      }
      else if (strncasecmp (*str, "core", 4) == 0)
      {
        *str += 4;
        hid = KAAPI_HWS_LEVELID_CORE;
      }
      else if (strncasecmp (*str, "socket", 6) == 0)
      {
        *str += 6;
        hid = KAAPI_HWS_LEVELID_SOCKET;
      }
      else if (strncasecmp (*str, "numa", 4) == 0)
      {
        *str += 4;
        hid = KAAPI_HWS_LEVELID_NUMA;
      }
      if (hid == KAAPI_HWS_LEVELID_MAX)
        /* not correct */
        return false;

      eat_space(str);
      if (**str != '(') return false;
      ++*str;
      if (!kaapi_parse_unsigned_int(str, &id))
        return false;
      eat_space(str);
      if (**str != ')') return false;
      ++*str;

      /* fill set and negate set */
      if (kaapi_default_param.affinity_set[hid].count < id)
         kaapi_abort(__LINE__, __FILE__, "*** Error while parsing places: invalid ressource index.");
#if defined(__linux__) && defined(KAAPI_USE_SCHED_AFFINITY)
      if (!negate)
        KAAPI_CPUSET_OR(&set, &set, &kaapi_default_param.affinity_set[hid].cpuset[id]);
      else
        KAAPI_CPUSET_OR(&negateset, &negateset, &kaapi_default_param.affinity_set[hid].cpuset[id]);
#endif
    }
    else
    {
      if (!kaapi_parse_range(str, &low, &high, &stride))
        return false;

      eat_space(str);
      /* fill set and negate set */
      for (unsigned int i=low; i<=high; i+= stride)
      {
#if defined(__linux__) && defined(KAAPI_USE_SCHED_AFFINITY)
         if (!negate) KAAPI_CPUSET_SET(i, &set);
         else KAAPI_CPUSET_SET(i, &negateset);
#endif
      }
    }
    if (**str != ',')
      break;
    ++*str;
  } /* place loop */

  /* merge two sets together: finalset = set & ~negateset */
#if defined(__linux__) && defined(KAAPI_USE_SCHED_AFFINITY)
  KAAPI_CPUSET_XOR( place, &set, &negateset );
  KAAPI_CPUSET_AND( place, place, &set );
  /* */
  unsigned int cpu = KAAPI_CPUSET_COUNT(place);
  *cpu_count = cpu;
#endif

  return true;
}



/* Grammar for parsing places as in OpenMP.
[ expr ] is for optional expression.
numas is for taking places from numa nodes that does not correspond
to socket on some machines.
places := threads [ ( num ) ] | cores [ ( num ) ] | sockets [ ( num ) ] | numas [ ( num ) ]
          | places_list;
place_list := { place } [ : num [ : stride ] ] [, place_list ]
place := range , [ place ]
range := [!] [num] [ : [num] [ : num ] ]
*/
bool kaapi_parse_places(
    char** str,
    unsigned int* count,
    unsigned int* cpu_count,
    kaapi_cpuset_t** places
)
{
  kaapi_hws_levelid_t hid = KAAPI_HWS_LEVELID_MAX;
  if (*str ==0)
    return false;

  *places = 0;
  *count = 0;
  *cpu_count = 0;

  if (strncasecmp (*str, "threads", 7) == 0)
  {
    *str += 7;
    hid = KAAPI_HWS_LEVELID_THREAD;
  }
  else if (strncasecmp (*str, "cores", 5) == 0)
  {
    *str += 5;
    hid = KAAPI_HWS_LEVELID_CORE;
  }
  else if (strncasecmp (*str, "sockets", 7) == 0)
  {
    *str += 7;
    hid = KAAPI_HWS_LEVELID_SOCKET;
  }
  else if (strncasecmp (*str, "numas", 5) == 0)
  {
    *str += 5;
    hid = KAAPI_HWS_LEVELID_NUMA;
  }
  if (hid != KAAPI_HWS_LEVELID_MAX)
  {
    kaapi_mt_hwdetect(); /* required ! */

    unsigned int num;
    /* parse remaing '(' num ')' */
    eat_space(str);
    if (**str == '(')
    {
      ++*str;
      if (!kaapi_parse_unsigned_int(str, &num) || (**str != ')'))
        return false;
      ++*str;
    }
    else
      num = kaapi_default_param.affinity_set[hid].count;

    if (kaapi_default_param.affinity_set[hid].count < num)
    {
      fprintf(stderr,"*** Warning while parsing places: Too many ressources in places.\n");
//      num = kaapi_default_param.affinity_set[hid].count;
    }
    *places = malloc( sizeof(kaapi_cpuset_t)*num );

#if defined(__linux__) && defined(KAAPI_USE_SCHED_AFFINITY)
    for (unsigned int i=0; i<num; ++i)
    {
      int index = i % kaapi_default_param.affinity_set[hid].count;
      memcpy( &(*places)[i], &kaapi_default_param.affinity_set[hid].cpuset[index], sizeof(kaapi_cpuset_t));
      *cpu_count += KAAPI_CPUSET_COUNT(&kaapi_default_param.affinity_set[hid].cpuset[index]);
    }
#else
    *cpu_count += num;
#endif

    *count = num;
    return true;
  }

  /* parse list */
  kaapi_cpuset_t oneplace;

  while (1)
  {
    eat_space(str);
    if (**str != '{') return false;
    ++*str;
    unsigned int cpu;
    if (!kaapi_parse_oneplace(str, &cpu, &oneplace)) return false;
    eat_space(str);
    if (**str != '}') return false;
    ++*str;

    /* */
    if (cpu >0)
    {
      *places = realloc(*places, sizeof(kaapi_cpuset_t)*(*count +1));
      memcpy(&(*places)[*count], &oneplace, sizeof(kaapi_cpuset_t));
      ++*count;
      *cpu_count += cpu;
    }

    eat_space(str);
    if (**str == ':')
    {
      unsigned int nrep;
      unsigned int shift;
      ++*str;
      if (!kaapi_parse_unsigned_int(str, &nrep))
        return false;
      eat_space(str);
      if (**str != ':') return false;
      ++*str;
      if (!kaapi_parse_unsigned_int(str, &shift))
        return false;
      eat_space(str);

      /* */
      kaapi_cpuset_t shifted = oneplace;
      memcpy(&shifted, &oneplace, sizeof(kaapi_cpuset_t));
      for (unsigned int i=0; i<nrep-1; ++i)
      {
#if defined(__linux__) && defined(KAAPI_USE_SCHED_AFFINITY)
        KAAPI_CPUSET_SHIFT( &shifted, shift );
        if (cpu >0)
        {
          *places = realloc(*places, sizeof(kaapi_cpuset_t)*(*count +1));
          memcpy(&(*places)[*count], &shifted, sizeof(kaapi_cpuset_t));
          ++*count;
          *cpu_count += cpu;
        }
#endif
      }
    }
    if (**str == 0)
      break;
    if (**str != ',') return false;

    ++*str;
  }
  return true;
}


/*
*/
char* kaapi_unparse_places_r(
    char* buffer, unsigned int buffer_size,
    unsigned int count,
    kaapi_cpuset_t* places
)
{
  size_t size = 0;
  int s;
  char* pos = &buffer[0];
  memset(buffer, 0, buffer_size);

  for (unsigned int i=0; i<count; ++i)
  {
    s = snprintf(pos, buffer_size-size, "{");
    if (s<0) return buffer;
    size += s; pos += s;
#if defined(__linux__) && defined(KAAPI_USE_SCHED_AFFINITY)
    kaapi_cpuset_t* place = places+i;
    unsigned long j, max = KAAPI_CPUSET_SETSIZE, len;
    bool notfirst = false;
    for (j = 0, len = 0; j < max; j++)
      if (KAAPI_CPUSET_ISSET(j, place))
      {
        if (len == 0)
        {
          if (notfirst)
          {
            s = snprintf(pos, buffer_size-size, ",");
            if (s<0) return buffer;
            size += s; pos += s;
          }
          notfirst = true;
          s = snprintf (pos, buffer_size-size, "%lu", j);
          if (s<0) return buffer;
          size += s; pos += s;
        }
        ++len;
      }
      else
      {
        if (len > 1)
        {
          s = snprintf (pos, buffer_size-size, ":%lu", len);
          if (s<0) return buffer;
          size += s; pos += s;
        }
        len = 0;
      }

    if (len > 1)
    {
      s = snprintf (pos, buffer_size-size, ":%lu", len);
      if (s<0) return buffer;
      size += s; pos += s;
    }
#endif
    if (i < count-1)
      s = snprintf(pos, buffer_size-size, "},");
    else
      s = snprintf(pos, buffer_size-size, "}");
    if (s<0) return buffer;
    size += s; pos += s;
  }
  return buffer;
}



/*
*/
char* kaapi_unparse_places(
    unsigned int count,
    kaapi_cpuset_t* places
)
{
  #define MAX_SIZE 4096
  static char buffer[MAX_SIZE];
  return kaapi_unparse_places_r(buffer, MAX_SIZE, count, places);
}


