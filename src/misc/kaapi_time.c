/*
** xkaapi
** 
**
** Copyright 2009-2013 INRIA.
**
** Contributors :
**
** fabien.lementec@imag.fr
** thierry.gautier@inrialpes.fr
** Vincent.Danjean@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include <stdint.h>
#if defined(HAVE_CLOCK_GETTIME)
# include <time.h>
#else
# include <sys/time.h>
#endif

#if defined(HAVE_CLOCK_GETTIME)
typedef struct timespec struct_time;
#  define gettime(t) clock_gettime( CLOCK_REALTIME, t)
#  define get_sub_seconde(t) (1e-9*(double)t.tv_nsec)
#  define get_sub_seconde_ns(t) ((uint64_t)t.tv_nsec)
#else
typedef struct timeval struct_time;
#  define gettime(t) gettimeofday( t, 0)
#  define get_sub_seconde(t) (1e-6*(double)t.tv_usec)
#  define get_sub_seconde_ns(t) (1000*(uint64_t)t.tv_usec)
#endif

/** return time in second
*/
double kaapi_get_elapsedtime(void)
{
  struct_time st;
  int err = gettime(&st);
  if (err !=0) return 0;
  return (double)st.tv_sec + get_sub_seconde(st);
}

/** return time in nanosecond
*/
uint64_t kaapi_get_elapsedns(void)
{
  uint64_t retval;
  struct_time st;
  int err = gettime(&st);
  if (err != 0) return (uint64_t)0UL;
  retval = (uint64_t)st.tv_sec * 1000000000ULL;
  retval += get_sub_seconde_ns(st);
  return retval;
}

/*
*/
static uint64_t kaapi_startup_time = 0;
uint64_t kaapi_get_elapsedns_since_start(void)
{ 
  return (kaapi_get_elapsedns() - kaapi_startup_time);
}

/*
*/
void kaapi_timelib_init()
{
  static int once= 0;
  if (once) return;
  once = 1;
  kaapi_startup_time = kaapi_get_elapsedns();
}

/*
*/
void kaapi_timelib_fini()
{
}
