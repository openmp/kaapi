/*
** kaapi_staticsched.h
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_EVENT_H_
#define _KAAPI_EVENT_H_

#include <stdint.h>
#include "kaapi_perf.h"

#if defined(__cplusplus)
extern "C" {
#endif

/* Version 3 is not upper compatible with version 2.
   Only reader with trace version upper than 3 may have chance to read trace file
   Version 5 is not uppercompatible with older version.
   Version 6 is not uppercompatible with older version.
*/
#define __KAAPI_TRACE_VERSION__     6

/** Definition of internal KAAPI events.
    Not that any extension or modification of the events must
    be reflected in the utility bin/kaapi_event_reader.
*/
#define KAAPI_EVT_KPROC_START        0     /* kproc begins, i0: processor type (0:CPU, 1:GPU) */
#define KAAPI_EVT_KPROC_STOP         1     /* kproc ends */

#define KAAPI_EVT_TASK_BEG           2     /* begin execution of tasks */
#define KAAPI_EVT_TASK_END           3     /* end execution of tasks */

#define KAAPI_EVT_TASK_SUCC          4     /* T0 has successor T1 */
#define KAAPI_EVT_TASK_ACCESS        5     /* d0: task, d1: mode, d2: pointer */


#define KAAPI_EVT_COMP_DAG_BEG       6     /* begin of computing the dag */
#define KAAPI_EVT_COMP_DAG_END       7     /* end of computing the dag */

#define KAAPI_EVT_SCHED_IDLE_BEG     10    /* begin when k-processor begins to be idle and try to steal */
#define KAAPI_EVT_SCHED_IDLE_END     11    /* end when k-processor starts to steal */

#define KAAPI_EVT_SCHED_SUSPWAIT_BEG 15    /* master thread wait */
#define KAAPI_EVT_SCHED_SUSPWAIT_END 16    /* master thread end wait */

#define KAAPI_EVT_REQUESTS_BEG       17    /* when k-processor begin to process requests, data=victim.id */
#define KAAPI_EVT_REQUESTS_END       18    /* when k-processor end to process requests */

#define KAAPI_EVT_STEAL_OP           19    /* when k-processor emit a steal request data=victimid, serial*/
#define KAAPI_EVT_STEAL_AGGR_BEG     20    /* when begin to be a combiner */
#define KAAPI_EVT_STEAL_AGGR_END     21    /* when begin to be a combiner */

#define KAAPI_EVT_OFFLOAD_HTOH_BEG   22 /* offload copy */
#define KAAPI_EVT_OFFLOAD_HTOH_END   23

#define KAAPI_EVT_OFFLOAD_HTOD_BEG   24 /* offload copy */
#define KAAPI_EVT_OFFLOAD_HTOD_END   25

#define KAAPI_EVT_OFFLOAD_DTOH_BEG   26 /* offload copy */
#define KAAPI_EVT_OFFLOAD_DTOH_END   27

#define KAAPI_EVT_OFFLOAD_DTOD_BEG   28 /* offload copy */
#define KAAPI_EVT_OFFLOAD_DTOD_END   29

#define KAAPI_EVT_OFFLOAD_KERNEL_BEG 30
#define KAAPI_EVT_OFFLOAD_KERNEL_END 31

#define KAAPI_EVT_PARALLEL_BEG       32
#define KAAPI_EVT_PARALLEL_END       33

#define KAAPI_EVT_TASKWAIT_BEG       34
#define KAAPI_EVT_TASKWAIT_END       35

#define KAAPI_EVT_TASKGROUP_BEG      36
#define KAAPI_EVT_TASKGROUP_END      37


/* format of perf counter event: <perf id (0, 1, 2..)>, <value> (cumul user + sys) */
#define KAAPI_EVT_PERFCOUNTER        38
#define KAAPI_EVT_TASK_PERFCOUNTER   39 /* d0=task; d1.i8[0..2]: perf counter id; d2, d3: values */
                                        /* several KAAPI_EVT_TASK_PERFCOUNTER may follow KAAPI_EVT_TASK_END */

#define KOMP_EVT_LOCK_BEG            40
#define KOMP_EVT_LOCK_END            41

#define KOMP_EVT_YIELD_BEG           42
#define KOMP_EVT_YIELD_END           43

#define KOMP_EVT_BARRIER_BEG         44
#define KOMP_EVT_BARRIER_END         45

#define KAAPI_EVT_TASK_STEAL         46    /* d0: executed task, d1: original task */
#define KAAPI_EVT_LAST               47

/* Human readable name of events */
extern const char* kaapi_event_name[];

#ifndef KAAPI_FORMAT_MAX
#define KAAPI_FORMAT_MAX             128
#endif

/** Size of the event mask 
*/
typedef uint64_t kaapi_event_mask_type_t;

/** Heler for creating mask from an event
*/
#define KAAPI_EVT_MASK(eventno) \
  (((kaapi_event_mask_type_t)1) << (kaapi_event_mask_type_t)eventno)

/* The following set is always in the mask
*/
#define KAAPI_EVT_MASK_STARTUP \
    (  KAAPI_EVT_MASK(KAAPI_EVT_KPROC_START) \
     | KAAPI_EVT_MASK(KAAPI_EVT_KPROC_STOP) \
    )

#define KAAPI_EVT_MASK_COMPUTE \
    (  KAAPI_EVT_MASK(KAAPI_EVT_TASK_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_SUCC) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_ACCESS) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_STEAL) \
     | KAAPI_EVT_MASK(KAAPI_EVT_COMP_DAG_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_COMP_DAG_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_KERNEL_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_KERNEL_END) \
    )
    
#define KAAPI_EVT_MASK_KOMP \
    (  KAAPI_EVT_MASK(KAAPI_EVT_PARALLEL_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_PARALLEL_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASKWAIT_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASKWAIT_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASKGROUP_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASKGROUP_END) \
    )

#define KAAPI_EVT_MASK_IDLE \
    (  KAAPI_EVT_MASK(KAAPI_EVT_SCHED_IDLE_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_SCHED_IDLE_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_SCHED_SUSPWAIT_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_SCHED_SUSPWAIT_END) \
    )

#define KAAPI_EVT_MASK_STEALOP \
    (  KAAPI_EVT_MASK(KAAPI_EVT_STEAL_OP) \
     | KAAPI_EVT_MASK(KAAPI_EVT_STEAL_AGGR_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_STEAL_AGGR_END) \
    )

#define KAAPI_EVT_MASK_OFFLOAD \
    (  KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_HTOH_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_HTOH_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_HTOD_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_HTOD_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_DTOH_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_DTOH_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_DTOD_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_DTOD_END) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_KERNEL_BEG) \
     | KAAPI_EVT_MASK(KAAPI_EVT_OFFLOAD_KERNEL_END) \
    )

#if 1
#define KAAPI_EVT_MASK_PERFCOUNTER \
    (  KAAPI_EVT_MASK(KAAPI_EVT_TASK_PERFCOUNTER)\
    )
#else
#define KAAPI_EVT_MASK_PERFCOUNTER \
    (  KAAPI_EVT_MASK(KAAPI_EVT_PERFCOUNTER) \
     | KAAPI_EVT_MASK(KAAPI_EVT_TASK_PERFCOUNTER)\
    )
#endif

/* .................................. Implementation notes ..................................*/

/* Event data. at most 3 event data per event.
*/
typedef union {
    void*     p;
    uintptr_t i; 
    double    d;
    uint64_t  u;
    uint64_t  i64[2];
    uint32_t  i32[2];
    uint16_t  i16[4];
    uint8_t   i8[8];
} kaapi_event_data_t;


/* Event 
  - fixed sized data structure. To be extended if required.
*/
typedef struct kaapi_event_t {
  uint8_t     evtno;      /* event number */
  uint8_t     size;       /* size of event: not used, event should have avriable size [futur] */
  uint16_t    kid;        /* kaapi processor identifier or device identifier or other identifier */
  uint64_t    date;       /* nano second */
  union {
    struct {
      kaapi_event_data_t d0;
      kaapi_event_data_t d1;
      kaapi_event_data_t d2;
      kaapi_event_data_t d3;
    } s;
    kaapi_event_data_t data[4];
  } u;
} kaapi_event_t;

#define KAAPI_EVENT_DATA(evt,i,f) (evt)->u.s.d##i.f

/* 
*/
#define KAAPI_EVENT_BUFFER_SIZE 65520
typedef struct kaapi_event_buffer_t {
  kaapi_event_t                         buffer[KAAPI_EVENT_BUFFER_SIZE];
  struct kaapi_event_buffer_t* volatile next;
  uint32_t                              pos;
  uint32_t                              ident;  /* system wide identifier per thread ~KID */
  uint32_t                              ptype;  /* extra information of the thread */
  uint32_t                              numaid; /* thread binding's NUMA node id */
} kaapi_event_buffer_t;


/* 
*/
typedef struct kaapi_evtproc {
  uint32_t              ident;  /* system wide identifier per thread ~KID */
  uint32_t              ptype;  /* extra information of the thread */
  uint32_t              numaid; /* thread binding's NUMA node id */
  kaapi_event_buffer_t* eventbuffer;
} kaapi_evtproc_t;


/* 
*/
typedef struct kaapi_fmttrace_def {
  uint32_t          fmtid;       /* id */
  char              name[64];    /* name */
  char              color[32];   /* color to represent the task */
} kaapi_fmttrace_def;


/** Header of the trace file. First block of each trace file.
    Very static size
*/
typedef struct kaapi_eventfile_header {
  int            version;
  int            minor_version;
  int            trace_version;
  uint32_t       kid;
  uint32_t       numaid;
  uint32_t       ptype;
  unsigned int   cpucount;
  unsigned int   gpucount;
  int            numacount;           /* numa count */
  char           event_date_unit[8];  /* unit for the clock used to take date of event */
  uint64_t       event_mask;
  char           package[128];
  uint64_t       perf_mask;
  uint64_t       task_perf_mask;
  int            perfcounter_count;   /* (idmax & 0xFF) | (base for papi << 8)  */
  int            taskfmt_count;       /* number of task's formats */
  char           perfcounter_name[KAAPI_PERF_ID_MAX][32]; /* name for each perf counter */
  kaapi_fmttrace_def fmtdefs[KAAPI_FORMAT_MAX]; /* of size taskfmt_count */
} kaapi_eventfile_header_t;

#if defined(__cplusplus)
}
#endif

#endif
