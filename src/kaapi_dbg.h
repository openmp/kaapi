/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_DBG_H
#define _KAAPI_DBG_H 1
#include "kaapi.h"
#include <stdio.h>

/* ================= Extra interface for printing internal data structure ======================= */

#if defined(__cplusplus)
extern "C" {
#endif

struct kaapi_thread_t;
struct kaapi_stack_t;
struct kaapi_context_t;
struct kaapi_frame_wrdlist_t;

/** Return 0 if stack will not overflow
*/
extern int kaapi_stack_overflow_check (struct kaapi_thread_t *thread, uint32_t count);

/** \ingroup TASK
    Print the content of a task.
    Only available in libkaapidbg
*/
extern int kaapi_task_print( FILE* file, const struct kaapi_task_t* task );

/** \ingroup TASK
    Print the content of a stack.
    Only available in libkaapidbg
*/
extern int kaapi_stack_print( FILE* file, const struct kaapi_stack_t* stack );

/** \ingroup TASK
    Print the content of a stack. Tasks in frame in reverse order of creation.
    Only available in libkaapidbg
*/
extern int kaapi_stack_printreverse( FILE* file, const struct kaapi_stack_t* stack );

/** \ingroup TASK
    Print the content of a context.
    Only available in libkaapidbg
*/
extern int kaapi_context_print( FILE* file, const struct kaapi_context_t* ctxt );

/** \ingroup TASK
    Print the content of a context. Tasks in frame in reverse order of creation.
    Only available in libkaapidbg
*/
extern int kaapi_context_printreverse( FILE* file, const struct kaapi_context_t* ctxt );

/** \ingroup TASK
    Print the content of the current context
    Only available in libkaapidbg
*/
extern int kaapi_context_print_current(FILE* file );

/** \ingroup TASK
    Print the content of the current context. Tasks in frame in reverse order of creation.
    Only available in libkaapidbg
*/
extern int kaapi_context_printreverse_current(FILE* file );

/** \ingroup TASK
    Print the content of a frame in dot representation.
    DFG must has been computed before.
    Only available in libkaapidbg
*/
extern int kaapi_frame_print_dot(
  FILE* file,
  const struct kaapi_stack_t* stack,
  const struct kaapi_frame_t* frame
);

/** \ingroup TASK
    Print the dot content of a stack.
    Only available in libkaapidbg
*/
extern int kaapi_stack_print_dot( FILE* file, const struct kaapi_stack_t* stack );

/** \ingroup TASK
    Print the dot content of a context.
    Only available in libkaapidbg
*/
extern int kaapi_context_print_dot  ( FILE* file, const struct kaapi_context_t* ctxt );

/** \ingroup TASK
    Print the dot content of the current context.
    Only available in libkaapidbg
*/
extern int kaapi_context_print_dot_current(FILE* file );


/**
*/
extern int kaapi_frame_tasklist_print( FILE* file, const struct kaapi_frame_wrdlist_t* rdlist );

/**
*/
extern int kaapi_frame_tasklist_print_current(FILE* file );


/** Print info about address space
    The format of the output is [<type>, <gid>], the type is the type of the address space
    and gid the global identifier of the process that handles the address space.
    \param file [IN/OUT] the file descriptor where to dump the representation of the address
    space identifier.
    \param kasid [IN] an address space identifier
    \return the return value of the fprintf.
*/
extern int kaapi_memory_asid_fprintf( FILE* file, kaapi_address_space_id_t kasid );

/** Keep it ?
*/
extern size_t __kaapi_get_size_stack(void);

/** Allow to synchronize output on multithreaded program
*/
extern void kaapi_dbg_print_lock(void);

/** Allow to synchronize output on multithreaded program
*/
extern void kaapi_dbg_print_unlock(void);


#if defined(__cplusplus)
}
#endif

#endif
