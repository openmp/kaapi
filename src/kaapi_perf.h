/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_PERF_H
#define _KAAPI_PERF_H 1

#include <stddef.h>

#if defined(__cplusplus)
extern "C" {
#endif

#if !defined(KAAPI_MAX_HWCOUNTERS)
#define KAAPI_MAX_HWCOUNTERS 4
#endif


/* ========================================================================= */
/* Perf counter                                                              */
/* ========================================================================= */
/** \ingroup PERF
    Performace counters.
    The runtime instrumentation allows to compute performance counters at runtime.
    The set of counters is decomposed in two classes:
    - software counters 
    - hardware counters using PAPI support to read and accumulate them.
    Both classes are unified in the Kaapi runtime : selection of events to count
    are specified using KAAPI_PERF_ and KAAPI_TASKPERF_ env. vars.
    
    On specific points in the execution, the runtime read selected event counters and
    accumulated them per thread, per process or per task.
    Some general remarks: all times are in ns, excepted if specified otherwise

    TODO: 
    - constraints some of the counter to no be cumulated per task or per thread but per socket
    - define group to simplify analysis of some code
*/
#define KAAPI_PERF_ID_MASK(ID) (1UL << (ID))


#define KAAPI_PERF_ID_WORK          0   /* computation time (user) or schedule time (sys) = tidle */
#define KAAPI_PERF_ID_TINF          1   /* critical path */
#define KAAPI_PERF_ID_PTIME         2   /* parallel computation time (user) or schedule time (sys) = tidle */
#define KAAPI_PERF_ID_TASKSPAWN     3   /* count number of spawned tasks */
#define KAAPI_PERF_ID_TASKEXEC      4   /* count number of executed tasks */
#define KAAPI_PERF_ID_CONFLICTPOP   5   /*  */
#define KAAPI_PERF_ID_STEALREQOK    6   /* count number of successful steal requests */
#define KAAPI_PERF_ID_STEALREQ      7   /* count number of steal requests emitted */
#define KAAPI_PERF_ID_STEALOP       8   /* count number of steal operation to reply to requests */
#define KAAPI_PERF_ID_STEALIN       9   /* count number of receive steal requests */
#define KAAPI_PERF_ID_TASKSTEAL     10   /* number of stolen task */
#define KAAPI_PERF_ID_SYNCINST      11  /* number of sync instruction */
#define KAAPI_PERF_ID_COMM_H2H	    12 /* host to host transfers. Same order as KAAPI_IO_COPY_H2H */
#define KAAPI_PERF_ID_COMM_H2D	    13 /* host to device transfers */
#define KAAPI_PERF_ID_COMM_D2H      14 /* device to host transfers */
#define KAAPI_PERF_ID_COMM_D2D      15 /* device to device transfers */
#define KAAPI_PERF_ID_OFFLOADTASK   16 /* number of task performed by a device */
#define KAAPI_PERF_ID_CACHE_HIT	    17 /* GPU cache hit */
#define KAAPI_PERF_ID_CACHE_MISS    18 /* GPU cache miss */
#define KAAPI_PERF_ID_STACKSIZEBLOC 19 /* bytes of memory bloc stack allocated */
#define KAAPI_PERF_ID_DFGBUILD      20 /* time to compute task lists in ns */
#define KAAPI_PERF_ID_RDLISTEXEC    21 /* time to execute tasks using ready lists in ns */
#define KAAPI_PERF_ID_RDLISTINIT    22 /* time to push tasks using ready lists in ns */
#define KAAPI_PERF_ID_LOCAL_READ    23 /* software counter of number of read parameter local to a task */
#define KAAPI_PERF_ID_LOCAL_WRITE 	24 /* software counter of number of written parameter local to a task  */
#define KAAPI_PERF_ID_REMOTE_READ   25 /* idem for remote data */
#define KAAPI_PERF_ID_REMOTE_WRITE  26 /* idem for remote data */

#define KAAPI_PERF_ID_PARALLEL      27 /* #parallel region */
#define KAAPI_PERF_ID_LOCK          28 /* #lock */
#define KAAPI_PERF_ID_BARRIER       29 /* #call to barrier */
#define KAAPI_PERF_ID_TASKYIELD     30 /* #call to taskyield */


/* mark end of software counters */
#define KAAPI_PERF_ID_ENDSOFTWARE   32

#define KAAPI_MASK_PERF_ID_SOFTWARE ((1UL << KAAPI_PERF_ID_ENDSOFTWARE) -1)

#define KAAPI_PERF_ID_PAPI_BASE    (KAAPI_PERF_ID_ENDSOFTWARE)
/* All bit set to 1 in eventset after position KAAPI_PERF_ID_PAPI_BASE are papi events 
   The name of the counter at bit i, is papi_names[i-KAAPI_PERF_ID_PAPI_BASE]
*/

/* predefined PAPI event counter : those events exist only if they are defined 
   in KAAPI_PERF_EVENTS or KAAPI_TASKPERF_EVENTS
*/
#define KAAPI_PERF_ID_PAPI_0       (KAAPI_PERF_ID_PAPI_BASE+0)
#define KAAPI_PERF_ID_PAPI_1       (KAAPI_PERF_ID_PAPI_BASE+1)
#define KAAPI_PERF_ID_PAPI_2       (KAAPI_PERF_ID_PAPI_BASE+2)
#define KAAPI_PERF_ID_PAPI_3       (KAAPI_PERF_ID_PAPI_BASE+3)

#define KAAPI_PERF_ID_MAX          64

/* Group of events */
#define KAAPI_PERF_GROUP_TASK      0
#define KAAPI_PERF_GROUP_OFFLOAD   1
#define KAAPI_PERF_GROUP_DFGBUILD  2
#define KAAPI_PERF_GROUP_STEAL     3
#define KAAPI_PERF_GROUP_OMP       4

#if ((KAAPI_PERF_ID_ENDSOFTWARE+KAAPI_MAX_HWCOUNTERS) > KAAPI_PERF_ID_MAX)
#error "The maximal size of the peformance counters handled by Kaapi should be extended. Please contact the authors."
#endif

#if !defined(KAAPI_CACHE_LINE)
#define KAAPI_CACHE_LINE 64
#endif

/* counter type
*/
typedef int64_t kaapi_perf_counter_t;

/* counters per thread
*/
typedef kaapi_perf_counter_t kaapi_threadstate_perfctr_t[KAAPI_PERF_ID_MAX];
typedef struct {
  kaapi_threadstate_perfctr_t state[2]; /* KAAPI_PERF_USR_STATE/KAAPI_PERF_SYS_STATE */
} __attribute__((aligned (KAAPI_CACHE_LINE))) kaapi_thread_perfctr_t;

/* identifier of perf event
*/
typedef uint32_t kaapi_perf_id_t;

/* bit map for a set of events
*/
typedef uint64_t kaapi_perf_idset_t;

/* fwd */
typedef struct kaapi_perfonestat_t {
  /* represent starts of cummulated counters (of the same type)
  */
  kaapi_perf_counter_t max_counters;    /* max of the counters */
  kaapi_perf_counter_t sum_counters;    /* sum of the counters */
  kaapi_perf_counter_t sum2_counters;   /* sum of the square of the counters */
  size_t               count;
} kaapi_perfonestat_t;

/*  */
typedef struct kaapi_perfstat_t {
  /* represent cummulated counters for all tasks of this format.
     counters are taken from KAAPI_TASKPERF_EVENTS or using default in kaapi_default_param
  */
  kaapi_perf_counter_t max_counters[KAAPI_PERF_ID_MAX];    /* max of the counters */
  kaapi_perf_counter_t sum_counters[KAAPI_PERF_ID_MAX];    /* sum of the counters */
  kaapi_perf_counter_t sum2_counters[KAAPI_PERF_ID_MAX];   /* sum of the square of the counters */
  size_t               count;
} __attribute__((aligned (KAAPI_CACHE_LINE))) kaapi_perfstat_t;

/* Named perf counters with accumulation per kproc
*/
typedef struct kaapi_named_perfctr {
   const char*                   name;

  /* represent cummulated counters for all tasks of this format */
  struct kaapi_perfstat_t        stats;

  /* represent cummulated counters for all tasks of this format */
  struct kaapi_perfstat_t**      kproc_stats;
} kaapi_named_perfctr_t;



/*
*/
typedef struct kaapi_perfproc_t {
  kaapi_thread_perfctr_t   perf_regs;
  kaapi_perf_counter_t*	   curr_perf_regs;   /* either perf_regs[0], either perf_regs[1] */
  int                      papi_event_set;
  unsigned int	           papi_event_count;
  kaapi_perf_counter_t     start_t[2];       /* [KAAPI_PERF_SYS_STATE]= T1 else = Tidle */
  kaapi_perfstat_t         stats;            /* elementary statistics about counters */
} kaapi_perfproc_t;


/* for perf_regs access: SHOULD BE 0 and 1 
   All counters have both USER and SYS definition (sys == program that execute the scheduler).
   * KAAPI_PERF_ID_T1 is considered as the T1 (computation time) in the user space
   and as TSCHED, the scheduling time if SYS space. In workstealing litterature it 
   is also named Tidle.
*/
#define KAAPI_PERF_USR_STATE   0
#define KAAPI_PERF_SYS_STATE   1


/* Meta data about performance counter 
*/
typedef struct kaapi_perfctr_info {
  const char*            name;         /* human readable */
  const char*            cmdlinename;  /* command line name */
  const char*            helpstring;
  int                    eventcode;
  int                    ns2s;         /* 1 iff need conversion when displayed */
  int                    opaccum;      /* 0 = add, 1=max */
  uint32_t               type;
  kaapi_perf_counter_t (*read)(void*);
  void*                  ctxt;
} kaapi_perfctr_info_t;

extern kaapi_perfctr_info_t kaapi_perfctr_info[];

/* type of counter */
#define KAAPI_PCTR_LIBRARY    0x1
#define KAAPI_PCTR_PAPI       0x2
#define KAAPI_PCTR_USER       0x3


/* idset */
static inline
void kaapi_perf_idset_zero(kaapi_perf_idset_t* set)
{
  *set = 0;
}
static inline
void kaapi_perf_idset_add(kaapi_perf_idset_t* set, kaapi_perf_id_t id)
{
  *set |= KAAPI_PERF_ID_MASK(id);
}
static inline
void kaapi_perf_idset_addset(kaapi_perf_idset_t* set, const kaapi_perf_idset_t mask)
{
  *set |= mask;
}
static inline
int kaapi_perf_idset_clear(kaapi_perf_idset_t* set, kaapi_perf_id_t pcid)
{
  *set &= (kaapi_perf_idset_t)(~KAAPI_PERF_ID_MASK(pcid));
  return 0;
}

static inline
int kaapi_perf_idset_test(const kaapi_perf_idset_t* set, kaapi_perf_id_t pcid)
{
  return (*set & (kaapi_perf_idset_t)KAAPI_PERF_ID_MASK(pcid)) !=0;
}
static inline
int kaapi_perf_idset_test_mask(const kaapi_perf_idset_t* set, kaapi_perf_idset_t mask)
{
  return (*set & mask) !=0;
}
static inline
unsigned int kaapi_perf_idset_size(const kaapi_perf_idset_t* set)
{
  return __builtin_popcountl( *set );
}
static inline
unsigned int kaapi_perf_idset_empty(const kaapi_perf_idset_t* set)
{
  return *set == 0;
}

#if defined(__cplusplus)
}
#endif

#endif
