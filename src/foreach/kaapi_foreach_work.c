/*
 ** xkaapi
 ** 
 ** Created on Tue Mar 31 15:19:14 2009
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 ** thierry.gautier@inrialpes.fr
 ** fabien.lementec@imag.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
 
/* Note on the implementation.
   The work structure is decomposed in two parts that follows the two
   scheduling level adopted here.
   At the upper level, the work is a kaapi_global_work_t. It represents
   the iteration space distributed among a set of participants.
   At the lower lelve, the work is a kaapi_foreach_work_t which is
   represented as a kaapi_workqueue_t.
   Initialisation of the global work must be made by a unique caller thread.
   Once created, the caller thread has initialized the global work and the
   local work information. The adaptive task publishes the splitter that will
   create new participant.
   
   Once a participant is enrolled to work on the global work instance, i.e. after
   he begins to execute the _kaapi_thief_entrypoint, he never returns until the
   global end of the work.
   This optimization allows to bypass a return to the scheduler. Nevertheless, 
   it can let work inactive until all the enrolled threads complete.
   
   The distribution is not an foreach_attribute, but it must be.
   The current distribution data structure is handle by the wa field of the global
   work data structure. A thread with given tid, correspond to initial slice 
   [startindex[pos]..starindex[pos+1]) where pos = tid2pos[tid].
   The table tid2pos is used to compact information at the begin of the array
   startindex. Only thread with tid such that the tid-th bit is set in the
   map field can steal the initial slice.
   T.G.
*/
 
#include "kaapi_impl.h"
#include "kaapi_foreach_impl.h"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <sched.h>


#define LOG2 0

#define FATAL()						\
do {							\
  printf("fatal error @ %s::%d\n", __FILE__, __LINE__);	\
  exit(-1);						\
} while(0)


#if defined(KAAPI_DEBUG)
# define PRINTF(__s, ...) printf(__s, __VA_ARGS__)
#else
# define PRINTF(__s, ...)
#endif

#define KAAPI_LOCAL_LOG(x)
//#define KAAPI_LOCAL_LOG(x) x

#define LOG 0


/* The task */
typedef struct kaapi_foreach_entrypoint_arg {
  kaapi_foreach_globalwork_t* gwork;
  kaapi_foreach_work_t*       lwork;   /* !=0 when the task start to run on its local work */
  int                         tid;
  int                         ident;
  kaapi_foreach_attr_policy_t policy;
  kaapi_workqueue_index_t     first;   /* in case of steal from split_task. */
  kaapi_workqueue_index_t     last;
} kaapi_foreach_entrypoint_arg_t;

static void _kaapi_foreach_entrypoint(kaapi_task_t* task, kaapi_thread_t* thread);


/* task splitter: interface between scheduler and kaapic work (here foreach loop).
   This function is called when an idle kproc emits a request in order to extract work from
   the current task (foreach adaptive task).
   The method applies a filter to received requests :
   - if request->ident is not in the threadset, then the request is rejected
   - if the number of threads that have steal is greather than gwork->wi.nthreads, the request
   is rejected
   
   Then, depending of the policy attribut pass to the invocation of the foreach :
   - if the policy is KAAPI_FOREACH_SCHED_STEAL: 
     The non rejected requests are aggregated and thieves steal a range of size
     nrequests/(1+nrequests).
     
  If the gwork affinity attribut is MACHINE, any thread can steal work.
  If the gwork affinity attribut if NUMA, only thread in the NUMA location of the victim can
  steal work.
  If the gwork affinity attribut is CORE, only the (hyperthread) running on the same core can
  steal work.

  If the explanation is not clear, please read first the API-C documentation or read
  the online documentation (if you have a premium licence).
*/
static int _kaapi_split_task
(
  struct kaapi_task_t*                 victim_task,
  struct kaapi_listrequest_iterator_t* lri
)
{
  /* call by a thief to steal work */
  kaapi_foreach_entrypoint_arg_t* arg = (kaapi_foreach_entrypoint_arg_t*)victim_task->arg;
  kaapi_foreach_work_t* const  lwork = arg->lwork;
  if (lwork ==0)
    return 0;

  kaapi_foreach_globalwork_t* const  gwork = arg->gwork;
#if defined(__linux__) && defined(KAAPI_USE_SCHED_AFFINITY)
  kaapi_team_t* team = kaapi_self_team();
#endif

  kaapi_steal_request_t** keeprequests; /*  null terminated */
  kaapi_steal_request_t** rootrequests;
  kaapi_steal_request_t** c_kr;
  kaapi_steal_request_t** c_rr;
  kaapi_steal_request_t*  req;
  kaapi_workqueue_index_t first, last;
  kaapi_workqueue_index_t range_size, unit_size;

  /* no attached body: return (typically case of OpenMP) */
  if (gwork->body ==0)
    return 0;
  
  /* filter the requests that can be served.
     arg->ident is the ressource identifier : TREAD/CORE/NUMA or MACHINE dependending of
     the distribution attribut of the foreach (see foreach_workinit).
  */
  size_t cnt_requests = kaapi_listrequest_iterator_count(lri);
  c_kr = keeprequests = (kaapi_steal_request_t**)alloca( (1+cnt_requests) * sizeof(kaapi_steal_request_t*));
  c_rr = rootrequests = (kaapi_steal_request_t**)alloca( (1+cnt_requests) * sizeof(kaapi_steal_request_t*));
  for ( ; !kaapi_listrequest_iterator_empty(lri); )
  {
    kaapi_steal_request_t* req = kaapi_listrequest_iterator_get(lri);
    if ( gwork->affinity == KAAPI_HWS_LEVELID_MACHINE
    // REVOIR LE TEST ICI
//TODO
#if 0 //defined(__linux__) && defined(KAAPI_USE_SCHED_AFFINITY)
      || ((gwork->affinity == KAAPI_HWS_LEVELID_NUMA) &&
          KAAPI_CPUSET_ISSET(arg->ident, &team->all_kprocessors[req->ident]->nodeinfos[KAAPI_HWS_LEVELID_NUMA]->cpuset))
#endif
    )
    {
      *c_kr = req;
      ++c_kr;
    }
    else
    {
      *c_rr = req;
      ++c_rr;
    }
    kaapi_listrequest_iterator_next(lri);
  }
  *c_rr = 0;
  *c_kr = 0;

  if ((c_kr == keeprequests) && (c_rr == rootrequests))
    goto reply_failed;

  /* simple test to refuse requests : no enough work !
  */
  range_size = kaapi_workqueue_size(&lwork->cr);
  if (range_size < gwork->p_grain)
    return 0;

  /* how much per root req */
  int nreq = (int)(c_kr - keeprequests);
  
  /* */
  unit_size = range_size / (nreq + 1);
  if (unit_size < gwork->p_grain)
  {
    nreq = (int)((range_size / gwork->p_grain) - 1);
    unit_size = gwork->p_grain;
    if (nreq ==0)
      return 0;
  }

  kaapi_workqueue_lock(&lwork->cr);
  int ok = (0 != kaapi_workqueue_steal(&lwork->cr, &first, &last, nreq * unit_size));
  kaapi_workqueue_unlock(&lwork->cr);
  if (!ok)
    goto reply_failed;

  kaapi_assert_debug(first < last);
  kaapi_assert_debug(unit_size*nreq == last-first);  

  while ((*keeprequests != 0) && (first<last))
  {
    req = *keeprequests;
    ++keeprequests;
    kaapi_foreach_entrypoint_arg_t* tw = kaapi_request_pushdata(req, sizeof(kaapi_foreach_entrypoint_arg_t));
    tw->gwork       = gwork;
    tw->lwork       = 0;
    tw->tid         = -1;
    tw->ident       = arg->ident; /* inherit TID */
    tw->policy      = KAAPI_FOREACH_SCHED_SPLIT;
    tw->first       = last-unit_size;
    tw->last        = last;

    if (!gwork->strict)
      kaapi_request_pushtask_adaptive(
        req, 
        victim_task,
        _kaapi_foreach_entrypoint,
        _kaapi_split_task,
        tw
      );
    else
    kaapi_request_pushtask(
      req,
      victim_task,
      _kaapi_foreach_entrypoint,
      tw
    );

    kaapi_request_committask(req);
    
    last -= unit_size;
    kaapi_assert_debug(first <= last);
  }
  kaapi_assert_debug( last == first );

reply_failed:
  /* reply failed to keep requests not replied */
  while (*keeprequests != 0)
  {
    req = *keeprequests;
    kaapi_request_reply((kaapi_request_t*)req, KAAPI_REQUEST_S_NOK);
    ++keeprequests;
  }

  /* reply failed to other requests */
  while (*rootrequests != 0)
  {
    req = *rootrequests;
    kaapi_request_reply((kaapi_request_t*)req, KAAPI_REQUEST_S_NOK);
    ++rootrequests;
  }
  
  return 0;
}



/* Return internal_last, internal_first by parameter.
   Return value is the chunk, if modified.
*/
static void _kaapi_init_lwork(
    int                         self_tid,
    kaapi_foreach_attr_policy_t policy,
    kaapi_foreach_globalwork_t* gwork,
    kaapi_foreach_work_t*       lwork,
    kaapi_workqueue_index_t     first,
    kaapi_workqueue_index_t     last
)
{
  kaapi_workqueue_index_t internal_first = 0;
  kaapi_workqueue_index_t internal_last  = 0;
  kaapi_workqueue_index_t remainder;
  kaapi_workqueue_index_t size = (last-first);
  kaapi_workqueue_index_t count = gwork->count;
  kaapi_workqueue_index_t chunk = gwork->s_grain;

  lwork->gwork        = gwork;
  lwork->all_workdone = 0;
  lwork->workdone     = 0;
  lwork->iter         = 0;
  lwork->frame        = 0;
  lwork->self_tid     = self_tid;
  lwork->prev         = 0;

  switch (policy)
  {
    case KAAPI_FOREACH_SCHED_SPLIT:
      internal_first = first;
      internal_last  = last;
    break;

    /* pre-distribute work per count */
    case KAAPI_FOREACH_SCHED_STATIC:
    {
      /* OMP compliant implementation */
      if (chunk == 0)
      {
        chunk = size / count;
        if (chunk == 0) chunk = 1;
      }
      else if (size < chunk * count)
        count = (last-first)/chunk;
      remainder = size - chunk * count;
      internal_first = first + chunk*self_tid + (self_tid < remainder ? self_tid : remainder);
      internal_last  = internal_first + chunk + (self_tid < remainder ? 1 : 0);
    } break;

    case KAAPI_FOREACH_SCHED_GUIDED:
    case KAAPI_FOREACH_SCHED_DYNAMIC:
    case KAAPI_FOREACH_SCHED_AUTO:
    case KAAPI_FOREACH_SCHED_ADAPTIVE:
      /* chunk is the grain of pop, default == 1, always pre-distribute the work
         among count ressource at level 'dist' of the team
      */
      chunk = size / count;
      remainder = size % count;
      internal_first = first+ chunk*self_tid + (self_tid < remainder ? self_tid : remainder);
      internal_last  = internal_first + chunk + (self_tid < remainder ? 1 : 0);
      break;
      
    case KAAPI_FOREACH_SCHED_STEAL:
      /* master thread has all the initial work */
      if (self_tid != 0)
        internal_first = internal_last = 0;
      else
      {
        internal_first = first;
        internal_last  = last;
      }
      break;


    default:
      kaapi_abort(__LINE__,__FILE__, "Invalid case");
  }
  if (internal_first > last )
    internal_first = last;
  if (internal_last > last )
    internal_last = last;

  /* no steal due to policy or empty workqueue */
  if ((policy == KAAPI_FOREACH_SCHED_STATIC) || (internal_last <= internal_first))
    internal_first = internal_last = 0;

  /* always init the workqueue */
  kaapi_workqueue_init(
    &lwork->cr, 
    internal_first,
    internal_last
  );
}



/* entrypoint for tasks generated during non synchronous call to foreach.
 * TODO
  - Implementation strict / non strict pour l'affinité : n'autoriser le vol que si ...
*/
static void _kaapi_foreach_entrypoint(kaapi_task_t* task, kaapi_thread_t* thread)
{
  kaapi_foreach_entrypoint_arg_t* arg = (kaapi_foreach_entrypoint_arg_t*)task->arg;
  kaapi_foreach_globalwork_t* gwork = arg->gwork;
  kaapi_foreach_work_t*       lwork;
  const int                   self_tid = arg->tid;
  kaapi_workqueue_index_t     first, last;
  kaapi_foreach_body_t        body  = gwork->body;
  void*                       args  = gwork->args;

  lwork = kaapi_alloca(thread, sizeof(kaapi_foreach_work_t));
  kaapi_assert_debug(lwork !=0);

  _kaapi_init_lwork(
      self_tid,
      arg->policy,
      gwork,
      lwork,
      arg->first,
      arg->last
  );
  kaapi_writemem_barrier();
  arg->lwork = lwork;
  if (self_tid !=-1)
    gwork->lworkers[self_tid] = lwork;

  /* while there is sequential work to do in local work */
  while (kaapi_foreach_worknext(lwork, &first, &last) ==0)
  {
    /* apply w->f on [i, j[ */
    body(first, last, args);
  }
  kaapi_assert_debug( kaapi_workqueue_isempty(&lwork->cr) );

  kaapi_foreach_workfini( lwork );
}


/* Init work of a foreach construct.
   If sync is not zero, then it correspond to OpenMP loop description vwhere each thread intialize
   the loop. The global work (initial range, incr, number of iterations, ...) are allocated only
   once. The internal representation of the loop is [0...N) where N is the number of iterations
   to execute.
   \param body_f and body_args may be 0 iff sync != 0. Else no threads will participate to 
   work execution.
   \retval returns non zero if there is work to do, else returns 0
*/
kaapi_foreach_work_t* kaapi_foreach_workinit(
  kaapi_context_t*            self_thread,
  int                         sync,
  const kaapi_foreach_attr_t* attr,
  unsigned long long          size,
  kaapi_foreach_body_t        body_f,
  void*                       body_args
)
{
  kaapi_team_t*               team;
  int                         count;
  int                         tid;
  kaapi_foreach_work_t*       lwork = 0;
  kaapi_foreach_globalwork_t* gwork = 0;
  kaapi_frame_t*              frame;


  if (size ==0)
    /* empty range */
    return 0;

  if (!sync && (body_f ==0))
    return 0;
  
  if (attr ==0)
    attr = &kaapi_default_foreach_attr;

  team = self_thread->proc->team;

  /* set maximal number of threads */
  if (team)
    count = team->count;
  else
    count = 1;
  if (sync)
  {
    /* standard OPENMP loop */
    tid = (int)self_thread->proc->rsrc.kid;
  }
  else {
    /* task generating loop */
    tid = 0;
  }

  /* Allocate the global work only once.
     - If sync ==1, then lock the team and use the team->ws.gwork to broadcast the gwork
     - If sync ==0, then only the current running thread will do the initialisation
  */
  frame = kaapi_stack_push_frame( &self_thread->stack );
  int alloc=0;
  if (sync && (team !=0))
  {
//TODO: use ws.lock inplace (the OMP global lock)
    kaapi_atomic_lock(&team->lock);
    gwork = team->ws.gwork;
  }
  if (gwork ==0)
  {
    alloc = 1;
    gwork = kaapi_alloca(kaapi_context2thread(self_thread), sizeof(kaapi_foreach_globalwork_t));
    kaapi_assert_debug(gwork !=0);
    gwork->sync   = sync;
    gwork->size   = size;
    gwork->body   = body_f;
    gwork->args   = body_args;
    KAAPI_ATOMIC_WRITE(&gwork->term, size);
    KAAPI_ATOMIC_WRITE(&gwork->count_term, 0 );
    KAAPI_ATOMIC_WRITE(&gwork->cntworkers, 1 );

    kaapi_foreach_attr_get_policy(attr, &gwork->policy);
    if (attr->init & KAAPI_FOREACH_ATTR_INIT_SGRAIN)
      kaapi_foreach_attr_get_grains(attr, &gwork->s_grain, &gwork->p_grain);
    else
      gwork->s_grain = gwork->p_grain = 0;

    kaapi_foreach_attr_get_localitydomain(attr, &gwork->affinity, &gwork->strict);
    kaapi_foreach_attr_get_distribution(attr, &gwork->dist, &gwork->dist_strict);
    kaapi_foreach_attr_get_nowait(attr, &gwork->nowait);
    gwork->lworkers = (kaapi_foreach_work_t**)kaapi_alloca(kaapi_context2thread(self_thread),
                        count*sizeof(kaapi_foreach_work_t*));
    memset(gwork->lworkers, 0, count*sizeof(kaapi_foreach_work_t*));

    /* update count to spit interval */
    switch (gwork->dist)
    {
      case KAAPI_HWS_LEVELID_THREAD:
      case KAAPI_HWS_LEVELID_CORE:
      {
        if (gwork->affinity == KAAPI_HWS_LEVELID_MACHINE)
          gwork->count = count;
        else {
          kaapi_assert_debug( gwork->affinity = KAAPI_HWS_LEVELID_NUMA);
          gwork->count = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].count;
        }
      } break;

      case KAAPI_HWS_LEVELID_NUMA:
      case KAAPI_HWS_LEVELID_SOCKET:
        gwork->count = (team == 0 ? 1 : kaapi_default_param.places_set[gwork->dist].count);
        break;

      case KAAPI_HWS_LEVELID_BOARD:
        kaapi_abort(__LINE__, __FILE__, "invalid case, not yet implemented");
        break;

      case KAAPI_HWS_LEVELID_MACHINE:
        gwork->count = 1;
        break;
      default:
        kaapi_abort(__LINE__, __FILE__, "internal error: bad case");
    }

    /* update policy / chunk */
    switch (gwork->policy)
    {
      /* pre-distribute work per count */
      case KAAPI_FOREACH_SCHED_STATIC:
#if 0 // ICC_BUG 
        /* OMP compliant implementation */
        if (gwork->s_grain == 0)
        {
          gwork->s_grain = size / gwork->count;
          if (gwork->s_grain == 0) gwork->s_grain = 1;
        }
#endif
        break;

      case KAAPI_FOREACH_SCHED_GUIDED:
      case KAAPI_FOREACH_SCHED_DYNAMIC:
      case KAAPI_FOREACH_SCHED_AUTO:
        gwork->policy = KAAPI_FOREACH_SCHED_ADAPTIVE;
        break;

      case KAAPI_FOREACH_SCHED_STEAL:
      case KAAPI_FOREACH_SCHED_ADAPTIVE:
        /* gwork->chunk is the grain of pop, default == 1, always pre-distribute the work
           among count ressource at level 'dist' of the team
        */
        if (gwork->s_grain ==0) gwork->s_grain = 1;
        break;
        
      default: break;
    }
  }
  else {
    /* assert sync != 0 and team !=0 */
    /* if == count of the current team, then set team->gwork to 0 for next loop */
    if (KAAPI_ATOMIC_INCR(&gwork->cntworkers) == team->count)
      team->ws.gwork = 0;
  }
  if (sync && (team !=0))
  {
    if (alloc)
      team->ws.gwork = gwork;
//TODO: use ws.lock inplace (the OMP global lock)
    kaapi_atomic_unlock(&team->lock);
  }
//printf("%i::#%i workinit: [0,%llu) gwork:%p\n", (int)self_thread->proc->kid, gwork->count, size, (void*)gwork);

  /* local work : always initialized by each thread in case of sync construct */
  lwork = kaapi_alloca(kaapi_context2thread(self_thread), sizeof(kaapi_foreach_work_t));
  kaapi_assert_debug(lwork !=0);

  /* */
  _kaapi_init_lwork(
      tid,
      gwork->policy,
      gwork,
      lwork,
      0,
      size
  );

  /* update fields */
  lwork->self_tid     = (sync ? tid : 0);
  lwork->frame        = frame;

  if (!sync)
  {
    /* once local work is filled, then create adaptive tasks to execute work.
    */
    kaapi_task_t* task;
    kaapi_place_t* ld = 0;
    int child;
    uint64_t bitmap = 0;
//TODO VERIF    uint64_t bitmap = (team == 0 ? 0 : team->ldset[gwork->dist].bitmap); // for numa dist
    if ((gwork->dist == KAAPI_HWS_LEVELID_NUMA)
     || (gwork->dist == KAAPI_HWS_LEVELID_SOCKET))
    {
      /* delete own (master) numa node from list */
//TODO      bitmap &= ~(1<<self_thread->proc->numanode);
    }

    KAAPI_ATOMIC_WRITE(&gwork->count_term, gwork->count);
    /* if team ==0 then body of the loop is not executed by any thread because count==1 */
    for (child=1; child < gwork->count; ++child)
    {
      kaapi_foreach_entrypoint_arg_t* argtask =
          kaapi_alloca(kaapi_context2thread(self_thread), sizeof(kaapi_foreach_entrypoint_arg_t));
      argtask->gwork       = gwork;
      argtask->lwork       = 0;
      argtask->tid         = child;
      argtask->ident       = -1;
      argtask->policy      = gwork->policy;
      argtask->first       = 0;
      argtask->last        = size;

      task = kaapi_alloca(kaapi_context2thread(self_thread), sizeof(kaapi_task_t));
      if ((gwork->strict == 0) && (gwork->dist_strict ==0))
        kaapi_task_create_adaptive_withflags(
          task, kaapi_context2thread(self_thread),
          _kaapi_foreach_entrypoint,
          _kaapi_split_task,
          argtask,
          KAAPI_TASK_FLAG_UNSTEALABLE|KAAPI_TASK_FLAG_INDPENDENT
        );
      else
        kaapi_task_create_withflags(
          task,
          _kaapi_foreach_entrypoint,
          argtask,
          KAAPI_TASK_FLAG_UNSTEALABLE|KAAPI_TASK_FLAG_INDPENDENT
        );

      switch (gwork->dist) {
        case KAAPI_HWS_LEVELID_THREAD:
        case KAAPI_HWS_LEVELID_CORE:
        {
          if (gwork->affinity == KAAPI_HWS_LEVELID_MACHINE)
          {
            ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_THREAD].places[child];
            argtask->ident = child;
          }
          else
          { /* ld of the kproc in the NUMA group */
            ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[child];
            argtask->ident = child;
          }
        } break;

        case KAAPI_HWS_LEVELID_NUMA:
        case KAAPI_HWS_LEVELID_SOCKET:
        {
          int childidx = __builtin_ctzll( bitmap );
          bitmap &= ~(1<<childidx);
          ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[child];
          argtask->ident = childidx;
        } break;

        case KAAPI_HWS_LEVELID_BOARD:
          kaapi_abort(__LINE__, __FILE__, "invalid case, not yet implemented");
          break;

        case KAAPI_HWS_LEVELID_MACHINE:
//TODO verif          ld = team->ldset[KAAPI_HWS_LEVELID_MACHINE].lds[child];
          ld = kaapi_default_param.places_set[KAAPI_HWS_LEVELID_THREAD].places[child];
          argtask->ident = child;
          break;
        default:
          kaapi_abort(__LINE__, __FILE__, "internal error: bad case");
      }

      if (gwork->dist_strict)
        task->u.s.flag |= KAAPI_TASK_FLAG_LD_BOUND;
      kaapi_place_push(&self_thread->proc->rsrc, ld, task );
    }
  }

  kaapi_writemem_barrier();
  gwork->lworkers[tid] = lwork;
  lwork->prev = self_thread->ws.for_data.rt_ctxt;
  self_thread->ws.for_data.rt_ctxt = lwork;
  self_thread->ws.for_data.first   = 0;
  self_thread->ws.for_data.last    = 0;
  self_thread->ws.for_data.incr    = 0;
  self_thread->ws.for_data.curr_start = 0;
  self_thread->ws.for_data.curr_end = 0;
  ++self_thread->ws.count_for;

  return lwork;
}


int kaapi_foreach_workfini(
  kaapi_foreach_work_t* lwork
)
{
  if (lwork ==0)
    return 0;

  kaapi_thread_t* self_thread  = kaapi_self_thread();
  kaapi_foreach_globalwork_t* gwork = lwork->gwork;

  /* cannot access to gwork that may be deleted gwork->lworkers[lwork->self_tid] = 0; */
  kaapi_writemem_barrier();

  /* ok : local work was finished flush may be generated task */
  kaapi_sched_sync( self_thread );

  kaapi_context_t* self_context = kaapi_thread2context(self_thread);

  /* Here: no work, report local number of iteration performed by thread before stealing */
  if (lwork->workdone != 0)
  {
    lwork->all_workdone += lwork->workdone;
    KAAPI_ATOMIC_SUB64( &gwork->term, lwork->workdone );
    lwork->workdone = 0;
  }

  /* if sync==1 then this is OpenMP loop : do a barrier */
  if (gwork->sync)
  {
    /* synchronous, so can access to global work description */
    kaapi_team_t* team = self_context->proc->team;
    if (team !=0)
    {
      /* TODO: implement nowait clause, and thus do dynamic allocation of
         kaapi_workshare_t structuree into the team
         - 1 => should reset the flag for terminaison !
      */
      if (!gwork->nowait)
        kaapi_team_barrier_wait( team, self_context->proc, KAAPI_BARRIER_FLAG_RESET_WORK );
    }
  }
  else 
  { /* master thread wait end of count tasks generated during workinitialisation */
    if (lwork->self_tid == 0)
    {
      KAAPI_ATOMIC_DECR64( &gwork->count_term );
      while (KAAPI_ATOMIC_READ(&gwork->count_term) !=0)
        kaapi_slowdown_cpu();
    }
    else
      KAAPI_ATOMIC_DECR64( &gwork->count_term );
  }
  ++self_context->ws.count_for_end;

#if LOG2
  printf("FINI @%p :: tid=%i global=[0, %llu) ws.term=%i\n", (void*)team, kaapi_self_kid(), lwork->last, KAAPI_ATOMIC_READ(&team->ws.term) );
  fflush(stdout);
#endif 

  self_context->ws.for_data.rt_ctxt = lwork->prev;
  if (lwork->frame)
    kaapi_stack_pop_frame( &self_context->stack, lwork->frame );
  return 0;
}



/* 
  Return ==0 iff first and last have been filled for the next piece
  of work to execute. Else return !=0 in case of termination.
  Depending of the scheduling policy, the function may try to steal from registered lwork
  in the global work.
*/
int kaapi_foreach_worknext(
  kaapi_foreach_work_t*    lwork,
  kaapi_workqueue_index_t* first,
  kaapi_workqueue_index_t* last
)
{
  kaapi_foreach_globalwork_t* gwork = lwork->gwork;
  int                     self_tid = lwork->self_tid;
  kaapi_workqueue_index_t internal_first;
  kaapi_workqueue_index_t internal_last;
  kaapi_workqueue_index_t sizeq;
  kaapi_workqueue_index_t pop_size = 0;
  int isok;


  switch (gwork->policy)
  {
    case KAAPI_FOREACH_SCHED_STEAL:
    case KAAPI_FOREACH_SCHED_ADAPTIVE:
    {
      pop_size = gwork->s_grain;
      kaapi_assert_debug( pop_size != 0);
      /* first pop in the queue */

      int retval;
      isok = (pop_size!=0) && ((retval = kaapi_workqueue_pop(&lwork->cr, first, last, pop_size))==0);
      if (isok)
      {
        kaapi_assert_debug( *first < *last);
        kaapi_assert_debug( *last <= gwork->size);
        sizeq = *last - *first;
        lwork->workdone += sizeq;
        return 0;
      }
      break;
    }
    default:
      break;
  }


  if (gwork->policy == KAAPI_FOREACH_SCHED_STATIC)
  {
    kaapi_workqueue_index_t count = gwork->count;
    kaapi_workqueue_index_t chunk = gwork->s_grain; 
    kaapi_workqueue_index_t remainder;
    /* cannot pop! refill it if possible (only if lwork->chunk !=0) */
    if (lwork->iter == -1)
      return 1;

    if (chunk == 0)
    {
      chunk = gwork->size / count;
      if (chunk ==0) chunk = 1;
      remainder = gwork->size - chunk * count;
      internal_first = chunk*self_tid + (self_tid < remainder ? self_tid : remainder);
      internal_last  = internal_first + chunk + (self_tid < remainder ? 1 : 0);
      lwork->iter = -1;
    }
    else {
      /* return multiple of chunk_iteration */
      internal_first = chunk*(self_tid + count*lwork->iter);
      internal_last  = internal_first + chunk;
      ++lwork->iter;
    }
   
    if (internal_last > gwork->size)
      internal_last = gwork->size;
    if (internal_first > gwork->size) /* or internal_last */
      internal_first = gwork->size;
    *first = internal_first;
    *last  = internal_last;
    kaapi_assert_debug( internal_first <= internal_last );
    if (internal_first == internal_last)
      goto return_fail;
    sizeq = internal_last - internal_first;
    lwork->workdone += sizeq;
//printf("%i::[%i, %i)\n", lwork->self_tid, internal_first, internal_last);
    return 0;
  }

  /* Here: no work, report local number of iteration performed by thread before stealing */
  if (lwork->workdone != 0)
  {
//printf("%i::worknext(1), gwork:%p : wd:%i allwd:%i\n", lwork->self_tid, (void*)gwork, lwork->workdone, lwork->all_workdone );
    lwork->all_workdone += lwork->workdone;
    KAAPI_ATOMIC_SUB64( &gwork->term, lwork->workdone );
    lwork->workdone = 0;
  }

  /* if orphan return false: empty queue,
     if affinity != MACHINE return fail, else the thread will try to steal adaptive task
  */
  if ((gwork->count <2)
   || (gwork->dist != KAAPI_HWS_LEVELID_MACHINE)
  )
    return 1;
  
  /* else dynamic scheduling strategy: steal part with random selection */
  kaapi_foreach_work_t* lw_victim;

  /* here team is !=0, else the gwork->count == 1 */
  kaapi_context_t* self_context = kaapi_self_context();
  int i;
  int victim;
  internal_first = internal_last = 0;
  for (i=0; i<gwork->count; ++i)
  {
    /* test end of workshare construct */
    sizeq = KAAPI_ATOMIC_READ( &gwork->term );
    if (sizeq == 0)
      goto return_fail;

    victim = rand_r(&self_context->proc->rsrc.seed);

    victim %= gwork->count;
    lw_victim = gwork->lworkers[victim];

    if (lw_victim ==0)
      continue;

    sizeq = kaapi_workqueue_size(&lw_victim->cr);
    if (sizeq ==0)
      continue;

    /* steal half strategy: here grain must be computed */
    sizeq = sizeq/2;
    if (sizeq ==0) sizeq = 1;
    kaapi_workqueue_lock( &lw_victim->cr );
    isok = (0 ==kaapi_workqueue_steal(&lw_victim->cr, &internal_first , &internal_last, sizeq));
    kaapi_workqueue_unlock( &lw_victim->cr );

    if (isok)
    {
      kaapi_assert_debug( internal_first < internal_last);
      break;
    }
    internal_first= internal_last= 0;
  }

  if (internal_last != internal_first)
  {
    if (internal_first + pop_size < internal_last)
    {
      /* reset own queue with [*first+pop_size, *last) 
         and return only [*fist, *first+pop_size[
      */
      kaapi_workqueue_lock( &lwork->cr );
      isok = kaapi_workqueue_reset(&lwork->cr, internal_first+pop_size, internal_last );
      kaapi_workqueue_unlock( &lwork->cr );
      kaapi_assert(isok == 0);
      internal_last = internal_first+pop_size;
    }

    kaapi_assert_debug( internal_first < internal_last);
    sizeq = internal_last - internal_first;
    kaapi_assert_debug( sizeq > 0);
    lwork->workdone += sizeq;

    /* Here convert to user level iteration space with incr */
    *first = internal_first;
    *last  = internal_last;
    return 0;
  }

return_fail:
  if (lwork->workdone != 0)
  {
//printf("%i::worknext(2), gwork:%p : wd:%i allwd:%i\n", lwork->self_tid, (void*)gwork, lwork->workdone, lwork->all_workdone );
    lwork->all_workdone += lwork->workdone;
    KAAPI_ATOMIC_SUB64( &gwork->term, lwork->workdone );
    lwork->workdone = 0;
  }
  lwork->iter = -1;
  kaapi_assert(lwork->workdone == 0);
  return 1;
}



/* exported Kaapi foreach interface */
int kaapi_foreach(
    unsigned long long          size,
    const kaapi_foreach_attr_t* attr,
    kaapi_foreach_body_t        body,
    void*                       arg
)
{
  kaapi_foreach_work_t*  lwork;
  unsigned long long first;
  unsigned long long last;
  if (size ==0)
    return 0;

  /* is_format true if called from kaapif_foreach_with_format */
  kaapi_processor_t* const kproc = kaapi_self_processor();
  if (kproc ==0)
  {
    body(0, size, arg);
    return 0;
  }
  kaapi_context_t* const self_thread = kproc->context;


  /* initalize the foreach global and local work, returns the local work pointer or 0 */
  lwork = kaapi_foreach_workinit(
      self_thread,
      0,
      attr,
      size,
      body,
      arg
  );
  if (lwork ==0)
    return EINVAL;

  while (kaapi_foreach_worknext(lwork, &first, &last) == 0)
    body( first, last, arg);

  kaapi_foreach_workfini( lwork );

  return 0;
}
