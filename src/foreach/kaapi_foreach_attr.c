/*
 ** xkaapi
 ** 
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 ** thierry.gautier@inrialpes.fr
 ** fabien.lementec@imag.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include "kaapi_impl.h"
#include "kaapi_foreach_impl.h"

kaapi_foreach_attr_t kaapi_default_foreach_attr = {
  .init = 0
};

/*
*/
int kaapi_foreach_attr_set_policy(
  kaapi_foreach_attr_t* attr, 
  kaapi_foreach_attr_policy_t policy
)
{
  kaapi_assert_debug( (policy == KAAPI_FOREACH_SCHED_STEAL)
                    ||(policy == KAAPI_FOREACH_SCHED_ADAPTIVE)
                    ||(policy == KAAPI_FOREACH_SCHED_DEFAULT)
                    ||(policy == KAAPI_FOREACH_SCHED_STATIC)
                    ||(policy == KAAPI_FOREACH_SCHED_DYNAMIC)
                    ||(policy == KAAPI_FOREACH_SCHED_RUNTIME)
                    ||(policy == KAAPI_FOREACH_SCHED_AUTO)
                    ||(policy == KAAPI_FOREACH_SCHED_GUIDED)
  );
  if ( (policy != KAAPI_FOREACH_SCHED_STEAL)
    &&(policy != KAAPI_FOREACH_SCHED_ADAPTIVE)
    &&(policy != KAAPI_FOREACH_SCHED_DEFAULT)
    &&(policy != KAAPI_FOREACH_SCHED_STATIC)
    &&(policy != KAAPI_FOREACH_SCHED_DYNAMIC)
    &&(policy != KAAPI_FOREACH_SCHED_RUNTIME)
    &&(policy != KAAPI_FOREACH_SCHED_AUTO)
    &&(policy != KAAPI_FOREACH_SCHED_GUIDED)
  )
    return EINVAL;
  if (policy == KAAPI_FOREACH_SCHED_DEFAULT)
    policy = KAAPI_FOREACH_SCHED_ADAPTIVE;
  attr->policy = policy;
  attr->init |= KAAPI_FOREACH_ATTR_INIT_POLICY;
  return 0;
}

/*
*/
int kaapi_foreach_attr_get_policy(
  const kaapi_foreach_attr_t* attr,
  kaapi_foreach_attr_policy_t* policy
)
{
  if (attr->init & KAAPI_FOREACH_ATTR_INIT_POLICY)
    *policy = attr->policy;
  else
    *policy = KAAPI_FOREACH_SCHED_AUTO;
  return 0;
}

/*
*/
int kaapi_foreach_attr_set_grains(
  kaapi_foreach_attr_t* attr, 
  unsigned long long s_grain,
  unsigned long long p_grain
)
{
  attr->s_grain = s_grain;
  attr->p_grain = p_grain;
  attr->init |= KAAPI_FOREACH_ATTR_INIT_SGRAIN | KAAPI_FOREACH_ATTR_INIT_PGRAIN;
  return 0;
}

/*
*/
int kaapi_foreach_attr_get_grains(
  const kaapi_foreach_attr_t* attr,
  unsigned long long* s_grain,
  unsigned long long* p_grain
)
{
  if (s_grain)
  {
    if (attr->init & KAAPI_FOREACH_ATTR_INIT_SGRAIN)
      *s_grain = attr->s_grain;
    else
      *s_grain = 1;
  }

  if (p_grain)
  {
    if (attr->init & KAAPI_FOREACH_ATTR_INIT_PGRAIN)
      *p_grain = attr->p_grain;
    else
      *p_grain = 1;
  }
  return 0;
}


/*
*/
int kaapi_foreach_attr_set_localitydomain(
  kaapi_foreach_attr_t* attr,
  kaapi_hws_levelid_t   level,
  int                   strict
)
{
  attr->affinity = level;
  if (strict) attr->flag |= 0x2;
  else attr->flag &= ~0x2;
  attr->init |= KAAPI_FOREACH_ATTR_INIT_LD;
  return 0;
}


/*
*/
int kaapi_foreach_attr_get_localitydomain(
  const kaapi_foreach_attr_t* attr,
  kaapi_hws_levelid_t*        level,
  int*                        strict
)
{
  if (level)
  {
    if (attr->init & KAAPI_FOREACH_ATTR_INIT_LD)
      *level = attr->affinity;
    else
      *level = KAAPI_HWS_LEVELID_MACHINE;
  }
  if (strict)
  {
    if (attr->init & KAAPI_FOREACH_ATTR_INIT_LD)
      *strict = (attr->flag & 0x2 ? 1 : 0);
    else
      *strict = 0;
  }
  return 0;
}


/*
*/
int kaapi_foreach_attr_set_distribution(
  kaapi_foreach_attr_t* attr,
  kaapi_hws_levelid_t   level,
  int                   strict
)
{
  if ((level != KAAPI_HWS_LEVELID_CORE)
   && (level != KAAPI_HWS_LEVELID_NUMA)
   && (level != KAAPI_HWS_LEVELID_MACHINE)
  )
    return EINVAL;

  attr->dist = level;
  if (strict) attr->flag |= 0x1;
  else attr->flag &= ~0x1;
  attr->init |= KAAPI_FOREACH_ATTR_INIT_DIST;
  return 0;
}

/*
*/
int kaapi_foreach_attr_get_distribution(
  const kaapi_foreach_attr_t* attr,
  kaapi_hws_levelid_t*        level,
  int*                        strict
)
{
  if (level)
  {
    if (attr->init & KAAPI_FOREACH_ATTR_INIT_DIST)
      *level = attr->dist;
    else
      *level = KAAPI_HWS_LEVELID_CORE;
  }
  if (strict)
  {
    if (attr->init & KAAPI_FOREACH_ATTR_INIT_DIST)
      *strict = (attr->flag & 0x1 ? 1 : 0);
    else
      *strict = 0;
  }
  return 0;
}

/*
*/
int kaapi_foreach_attr_set_nowait(
  kaapi_foreach_attr_t* attr,
  int                   nowait
)
{
  if (nowait)
    attr->init |= KAAPI_FOREACH_ATTR_INIT_NOWAIT;
  else
    attr->init &= ~KAAPI_FOREACH_ATTR_INIT_NOWAIT;
  return 0;
}

/*
*/
int kaapi_foreach_attr_get_nowait(
  const kaapi_foreach_attr_t* attr,
  int*                        nowait
)
{
  if (nowait ==0)
    return EINVAL;
  if (attr->init & KAAPI_FOREACH_ATTR_INIT_NOWAIT)
    *nowait = 1;
  else
    *nowait = 0;
  return 0;
}


