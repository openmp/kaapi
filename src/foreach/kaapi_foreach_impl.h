/*
 ** xkaapi
 ** 
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 ** thierry.gautier@inrialpes.fr
 ** fabien.lementec@imag.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#ifndef KAAPIC_HIMPL_INCLUDED
# define KAAPIC_HIMPL_INCLUDED

#include "kaapi_impl.h"
#include "kaapi_workqueue.h"

#if defined(__cplusplus)
extern "C" {
#endif

/* some important macros */
#define KAAPI_FOREACH_USE_KPROC_LOCK 1


/* Flag for lazy initialization of attribute ==1 iff set, else default value
*/
#define  KAAPI_FOREACH_ATTR_INIT_SGRAIN     0x01
#define  KAAPI_FOREACH_ATTR_INIT_PGRAIN     0x02
#define  KAAPI_FOREACH_ATTR_INIT_POLICY     0x04
#define  KAAPI_FOREACH_ATTR_INIT_LD         0x08
#define  KAAPI_FOREACH_ATTR_INIT_DIST       0x10
#define  KAAPI_FOREACH_ATTR_INIT_NOWAIT     0x20
struct kaapi_workshare_t;

/* Default attribut if not specified
*/
extern kaapi_foreach_attr_t kaapi_default_foreach_attr;


/* work array distribution. allow for random access. 
   The thread tid has a reserved slice [first, last)
   iff map[tid] != 0.
   Then it should process work on its slice, where:
     first = startindex[tid2pos[tid]]   (inclusive)
     last  = startindex[tid2pos[tid]+1] (exclusive)
  
   tid2pos is a permutation to specify slice of each tid
*/
typedef struct kaapi_work_distribution_t
{
//  kaapi_cpuset_t                map __attribute__((aligned(KAAPI_CACHE_LINE)));
  uint16_t                      *tid2pos;
  union {
    kaapi_workqueue_index_t     *startindex;
  } u;
} kaapi_work_distribution_t __attribute__((aligned(KAAPI_CACHE_LINE)));


/* work container information */
typedef struct
{
  int                          flags;  /* depending of the setted attribut */
  /* grains */
  union {
    struct {
      unsigned long long par_grain;
      unsigned long long seq_grain;
    } li;
  } rep;
  kaapi_foreach_attr_policy_t policy;    /* choose the policy for initial splitting */
} kaapi_work_info_t;


/* local work: used by each worker to process its work
   cr ranges from 0 to last
   [first,last] represents the initial interval.
   incr is the user increment.
*/
struct kaapi_foreach_work_t;
typedef struct kaapi_foreach_globalwork_t
{
  kaapi_atomic64_t            term;     /* == 0 if no more iteration */
  kaapi_atomic64_t            count_term;/* == 0 if initial tasks (sync ==0) are completed */
  int                         sync;     /* sync: 1==OpenMP loop synchronisation semantic, 0=no */
  int                         nowait;   /* nowait OpenMP semantic */
  kaapi_workqueue_index_t     size;     /* size of iterations  */
  kaapi_workqueue_index_t     p_grain;  /* parallel grain for dynamic scheduling policies */
  kaapi_workqueue_index_t     s_grain;  /* = chunk for static sched */
  kaapi_workqueue_index_t     count;
  kaapi_foreach_attr_policy_t policy;   /* policy */
  kaapi_hws_levelid_t         affinity; /* who is able to steal */
  int                         strict;   /* who is able to steal */
  kaapi_hws_levelid_t         dist;
  int                         dist_strict;
  kaapi_foreach_body_t        body;
  void*                       args;     /* other localwork */
  kaapi_atomic_t              cntworkers;
  struct kaapi_foreach_work_t** lworkers; /* less than the number of threads in the current team */
} kaapi_foreach_globalwork_t;

/* local work: used by each worker to process its work
   cr ranges from 0 to last
   [first,last] represents the initial interval.
   incr is the user increment.
*/
typedef struct kaapi_foreach_work_t
{
  kaapi_workqueue_t           cr;
  kaapi_workqueue_index_t     all_workdone;  /* all number of iterations performed by the thread */
  kaapi_workqueue_index_t     workdone;      /* number of iterations performed by the thread */
  kaapi_workqueue_index_t     iter;          /* #iter for static */
  int                         self_tid;      /* id in gwork->lworkers */
  struct kaapi_foreach_globalwork_t* gwork;
  struct kaapi_foreach_work_t* prev;
  kaapi_frame_t*              frame;   /* current frame to restore */
} kaapi_foreach_work_t __attribute__ ((aligned (KAAPI_CACHE_LINE)));


#if defined(__cplusplus)
}
#endif

#endif /* KAAPIC_HIMPL_INCLUDED */
