/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_MT_MACHINE_H_
#define _KAAPI_MT_MACHINE_H_ 1

#include "config.h"
#include <pthread.h>
#include <stddef.h> // offset of

#ifdef __linux__
#  ifdef HAVE_UCONTEXT_H
#    define KAAPI_USE_UCONTEXT
#  elif HAVE_SETJMP_H
#    error "Not implemented yet"
#    define KAAPI_USE_SETJMP
#  endif
#endif

#if defined(KAAPI_USE_SCHED_AFFINITY)
#include <sched.h>
#endif

#ifdef __APPLE__
#  ifdef HAVE_SETJMP_H
#    define KAAPI_USE_SETJMP
#  elif HAVE_UCONTEXT_H
#    define KAAPI_USE_UCONTEXT
#  endif
#endif


#if defined(KAAPI_USE_SETJMP)
#  include <setjmp.h>
#elif defined(KAAPI_USE_UCONTEXT)
#  include <ucontext.h>
#endif

#if defined(KAAPI_USE_NUMA)
#  include <numa.h>
#  include <numaif.h>
#  define KAAPI_KPROCESSOR_ALIGNMENT_SIZE 4096  /* page size */
#else
#  define KAAPI_KPROCESSOR_ALIGNMENT_SIZE KAAPI_CACHE_LINE
#endif

#include "kaapi_hashmap.h"

struct kaapi_team_t;
struct kaapi_context_t;
struct kaapi_request_node_t;
struct kaapi_cpuset_t;
struct kaapi_foreach_globalwork_t;
struct kaapi_stealcontext_t;

/* ========================================================================== */
/* New cpuset type : in order to get access to first_non_zero asm             */
/* ========================================================================== */
#if defined(KAAPI_USE_SCHED_AFFINITY)
typedef struct {
  uint64_t  bits[4];  /* 256 bits */
} kaapi_cpuset_t;
#define KAAPI_CPUSET_ISSET(i,p) ((p)->bits[(i)/64] & (1UL<< ((i)%64)))
#if defined(KAAPI_DEBUG)
#  define KAAPI_CPUSET_SET(i,p) do {kaapi_assert((i>=0) && (i<256)); (p)->bits[(i)/64] |= (1UL<< ((i)%64));} while(0)
#else
#  define KAAPI_CPUSET_SET(i,p) ((p)->bits[(i)/64] |= (1UL<< ((i)%64)))
#endif
#define KAAPI_CPUSET_ATOMIC_SET(i,p) __sync_or_and_fetch(&((p)->bits[(i)/64]), (1UL<<((i)%64)))
#define KAAPI_CPUSET_CLR(i,p) ((p)->bits[(i)/64] &= ~(1UL<< ((i)%64)))
#define KAAPI_CPUSET_CLR_EXCEPT(i,p) \
    do { KAAPI_CPUSET_ZERO(p); ((p)->bits[(i)/64] |= (1UL<< ((i)%64))); } while(0)
#define KAAPI_CPUSET_SETSIZE sizeof(kaapi_cpuset_t)*8
#define KAAPI_CPUSET_ZERO(p) \
     ((p)->bits[0] = (p)->bits[1] = (p)->bits[2] = (p)->bits[3] =0UL)
static inline int KAAPI_CPUSET_FIRSTONE( kaapi_cpuset_t* p)
{
  if (p->bits[0]) return __builtin_ffsll( p->bits[0] );
  if (p->bits[1]) return 64+__builtin_ffsll( p->bits[1] );
  if (p->bits[2]) return 128+__builtin_ffsll( p->bits[2] );
  if (p->bits[3]) return 192+__builtin_ffsll( p->bits[3] );
  return -1;
}

#define KAAPI_CPUSET_COUNT(p) \
     (__builtin_popcountll((p)->bits[0]) +\
     __builtin_popcountll((p)->bits[1]) +\
     __builtin_popcountll((p)->bits[2]) +\
     __builtin_popcountll((p)->bits[3]))
#define KAAPI_CPUSET_AND(p,s1,s2) \
     do {(p)->bits[0] = (s1)->bits[0] & (s2)->bits[0]; \
      (p)->bits[1] = (s1)->bits[1] & (s2)->bits[1]; \
      (p)->bits[2] = (s1)->bits[2] & (s2)->bits[2]; \
      (p)->bits[3] = (s1)->bits[3] & (s2)->bits[3]; } while(0)
#define KAAPI_CPUSET_OR(p,s1,s2) \
     do {(p)->bits[0] = (s1)->bits[0] | (s2)->bits[0]; \
      (p)->bits[1] = (s1)->bits[1] | (s2)->bits[1]; \
      (p)->bits[2] = (s1)->bits[2] | (s2)->bits[2]; \
      (p)->bits[3] = (s1)->bits[3] | (s2)->bits[3]; } while(0)
#define KAAPI_CPUSET_XOR(p,s1,s2) \
     do {(p)->bits[0] = (s1)->bits[0] ^ (s2)->bits[0]; \
      (p)->bits[1] = (s1)->bits[1] ^ (s2)->bits[1]; \
      (p)->bits[2] = (s1)->bits[2] ^ (s2)->bits[2]; \
      (p)->bits[3] = (s1)->bits[3] ^ (s2)->bits[3]; } while(0)
static inline void KAAPI_CPUSET_SHIFT( kaapi_cpuset_t* shifted, unsigned int shift )
{
   uint64_t old = 0;
   unsigned int i;
   for (i=0; i<4; ++i)
   {
     uint64_t nextold = shifted->bits[i];
     shifted->bits[i] = (shifted->bits[i] << shift) | (old >> (sizeof(uint64_t)*8 - shift));
     old = nextold;
   }
}

static inline void KAAPI_CPUSET_RSHIFT_LOW( kaapi_cpuset_t* shifted, unsigned int shift )
{
    kaapi_assert_debug(shift <= 64);
    /*Does not work for shift > 64, cf RSHIFT*/
    if (shift == 0)
        return;
    if (shift == 64) {
        shifted->bits[0] = shifted->bits[1];
        shifted->bits[1] = shifted->bits[2];
        shifted->bits[2] = shifted->bits[3];
        shifted->bits[3] = 0;
        return;
    }
    uint64_t old = 0;
    uint64_t mask = 0;
    mask = ~mask;
    mask >>= (sizeof(uint64_t)*8 - shift);
    int i;
    for (i = 3; i >= 0; --i) {
        uint64_t nextold = shifted->bits[i];
        shifted->bits[i] = (shifted->bits[i] >> shift) | ((old & mask) << (sizeof(uint64_t)*8 - shift));
        old = nextold;
    }
}

static inline void KAAPI_CPUSET_RSHIFT(kaapi_cpuset_t *shifted, unsigned int shift)
{
    unsigned int times = shift/64;
    unsigned int remains = shift%64;
    while (times--) {
        KAAPI_CPUSET_RSHIFT_LOW(shifted, 64);
    }
    KAAPI_CPUSET_RSHIFT_LOW(shifted, remains);
}

#else
typedef uint64_t      kaapi_cpuset_t;
#define KAAPI_CPUSET_SETSIZE sizeof(kaapi_cpuset_t)*8
#define KAAPI_CPUSET_ISSET(i,p) (*(p) & (1UL<<(i)))
#define KAAPI_CPUSET_SET(i,p) (*(p) |= (1UL<<(i)))
#define KAAPI_CPUSET_ATOMIC_SET(i,p) __sync_or_and_fetch(p, (1UL<<(i)))
#define KAAPI_CPUSET_CLR(i,p) (*(p) &= ~(1UL<<(i)))
#define KAAPI_CPUSET_CLR_EXCEPT(i, p) \
    do { (*(p) &= (1UL<<(i))); (*(p) |= (1UL<<(i))); } while(0)
#define KAAPI_CPUSET_ZERO(p) (*(p) = 0)
#define KAAPI_CPUSET_FIRSTONE(p) __builtin_ffsll(*(p))
#define KAAPI_CPUSET_COUNT(p) __builtin_popcountll(*(p))
#define KAAPI_CPUSET_AND(p,s1,s2) (*(p) = *(s1) & *(s2))
#define KAAPI_CPUSET_OR(p,s1,s2) (*(p) = *(s1) | *(s2))
#define KAAPI_CPUSET_XOR(p,s1,s2) (*(p) = *(s1) ^ *(s2))
#define KAAPI_CPUSET_SHIFT( shifted, shift ) (*(shifted) <<= (shift))
#define KAAPI_CPUSET_RSHIFT( shifted, shift ) (*(shifted) >>= (shift))
#endif

#include "kaapi_numa.h"
struct kaapi_device;

/* ========================================================================== */
typedef struct {
  kaapi_cpuset_t* first;
  unsigned long  len;
} kaapi_places_part_t;

/* ========================================================================== */
/*  ProcInfo struct is used to initialize a set of thread with 
    ressource constrainst such as cpuid.
    This structure is used to start thread after field team was set.
    run_processor is the subroutine start with thre thread. If 0 then Kaapi
    start a default subroutine.
*/ 
/* ========================================================================== */
/* */
typedef struct kaapi_procinfo_t
{
  struct kaapi_procinfo_t* next;
  kaapi_places_part_t  place_part;
  long                 place_off;   /* in place_part */
  unsigned int         proc_type;
  unsigned int         proc_index;
  unsigned int         reuse;
  void                 (*body_runprocessor)(void*);
  void*                arg_runprocessor;
  struct kaapi_team_t* team;
} kaapi_procinfo_t;


/* ========================================================================== */
/* reserved field in kproc structure, only used if lib configured with perfcounter
*/
struct kaapi_event_buffer_t;

/** lfree data structure
*/
typedef struct kaapi_lfree
{
  kaapi_list_t            list;
  int                     _sizelfree;
} kaapi_lfree_t;

#define KAAPI_CACHE_LINE_SIZE 64


/* ========================================================================= */
/* Information about memory hierarchy                                               */
/* ========================================================================= */
/** nodeinfo is the static object attached to a localitydomain at given level
*/
typedef struct kaapi_nodeinfo_t {
  kaapi_ldid_t               lid;
  kaapi_cpuset_t             cpuset;
  kaapi_lock_t               lock __attribute__ ((aligned (KAAPI_CACHE_LINE_SIZE)));
  kaapi_lfree_t              lfree;                     /* queue of free context shared at nodeinfo level */
} kaapi_nodeinfo_t __attribute__ ((aligned (KAAPI_CACHE_LINE_SIZE)));

/** Affinityset. 
    if cpuset[i][j] bit is set, then the i-th places share affinity with PU j.
*/
typedef struct kaapi_affinityset_t {
  int                        count;
  kaapi_cpuset_t            *cpuset;
} kaapi_affinityset_t;

struct kaapi_vtable_place_t;

/** A place : queues with locks and steal context to receive steal request.
    Allocated on page boundary and initialized by one of the threads of the place.
*/
typedef struct kaapi_place_t {
  int                          count;          /* system wide id. From kaapi_default_param */
  kaapi_hws_levelid_t          level;          /* or in current team */
  struct kaapi_vtable_place_t* vtable;
  kaapi_lock_t                 lock;           /* used for locking queue. lock consummer if spsc */
  struct kaapi_stealcontext_t* steal_ctxt;     /* specific to the WS algorithm / victim part */
  struct kaapi_stack_bloc_t*   freebloc;       /* pop/push in front for stack bloc */
  struct kaapi_list_bloc_t*    l_freebloc;       /* pop/push in front for stack bloc */
} kaapi_place_t;


/*
*/
typedef struct kaapi_place_group_operation_t {
  struct kaapi_request_node_t* head;
  struct kaapi_request_node_t* tail;
} kaapi_place_group_operation_t;


/* Virtual table of functions on place
*/
struct kaapi_vtable_place_t {
  /* fc: client side */
  kaapi_task_t*              (*fc_steal)  (struct kaapi_ressource_t* kpi, kaapi_place_t* ld);
  int                        (*fc_stealin)(struct kaapi_processor_t* kproc, kaapi_place_t* ld);
  kaapi_task_t*              (*fc_pop)    (struct kaapi_ressource_t* kpi, kaapi_place_t* ld);
  int                        (*fc_push)   (struct kaapi_ressource_t* kpi, kaapi_place_t* ld, kaapi_task_t* task);
  int                        (*fc_push_remote) (struct kaapi_ressource_t* rsrc, kaapi_place_t* ld, kaapi_task_t* task);
  int                        (*fc_push_async)   (struct kaapi_place_group_operation_t*, struct kaapi_ressource_t* kpi, kaapi_place_t* ld, kaapi_task_t* task);
  int                        (*fc_push_remote_async) (struct kaapi_place_group_operation_t*, struct kaapi_ressource_t* rsrc, kaapi_place_t* ld, kaapi_task_t* task);
  int                        (*fc_pushlist) (struct kaapi_ressource_t* rsrc, kaapi_place_t* ld, kaapi_list_t* list);

  /* client/server side */
  int                        (*f_isempty)(const struct kaapi_place_t* place); /* fast test */
  void                       (*f_destroy) (struct kaapi_place_t* place);

  /* fs: server side */
  void                       (*fs_steal)  (struct kaapi_place_t* place,
                                           struct kaapi_listrequest_iterator_t* lri );
  void                       (*fs_stealin)( struct kaapi_place_t* place,
                                            struct kaapi_listrequest_iterator_t* lri );
  kaapi_task_t*              (*fs_pop)  (struct kaapi_place_t* place);
  int                        (*fs_push) (struct kaapi_place_t* place, kaapi_task_t* task);
  int                        (*fs_push_remote) (struct kaapi_place_t* place, kaapi_task_t* task);
  int                        (*fs_pushlist) (struct kaapi_place_t* place, kaapi_list_t* list);
};


/** Place with private and public queues 
*/
typedef struct kaapi_plainplace_t {
  kaapi_place_t                inherit;
  kaapi_queue_t                queue;          /* queue of tasks */
  kaapi_queue_t                private_queue;  /* private queue of tasks to kproc of the ld */
} kaapi_plainplace_t __attribute__ ((aligned (KAAPI_CACHE_LINE_SIZE)));


/*
*/
typedef struct kaapi_kprocplace_t {
  kaapi_plainplace_t           inherit;
  struct kaapi_processor_t*           kproc;
} kaapi_kprocplace_t;


/** map2affinityset : map cpuid to the affinity sets on which it depends
*/
typedef struct kaapi_map2affinityset_t {
  kaapi_place_t*               affset[KAAPI_HWS_LEVELID_MAX];
} kaapi_map2affinityset_t;


/** allplaces : all places per level
*/
typedef struct kaapi_placeset_t {
  int                          count;
  kaapi_place_t**              places;
} kaapi_placeset_t;


/*
*/
typedef struct kaapi_localitydomainset_t {
  int             count;     /* size of places */
  kaapi_place_t*  places;    /* array of domains */
} kaapi_localitydomainset_t;


/* ========================================================================= */
/* Barrier                                                                   */
/* ========================================================================= */
#define KAAPI_BAR_CYCLES 3
typedef struct kaapi_barrier_t {
  kaapi_atomic_t cycle __attribute__ ((aligned (KAAPI_CACHE_LINE_SIZE)));
  kaapi_atomic_t wait_cycle __attribute__ ((aligned (KAAPI_CACHE_LINE_SIZE)));
  int term __attribute__ ((aligned (KAAPI_CACHE_LINE_SIZE)));
  char __attribute__ ((aligned (KAAPI_CACHE_LINE_SIZE))) count[KAAPI_BAR_CYCLES * KAAPI_CACHE_LINE_SIZE];
} kaapi_barrier_t;




/* ========================================================================= */
/* Team a set of kprocessors                                                 */
/* ========================================================================= */
typedef struct kaapi_team_t {
  kaapi_thread_id_t          tid;                       /* id of the creator thread */
  int                        count;                     /* threads */
  int                        teamid;
  int volatile               isterm;
  int                        flag;                      /* see kaapi_teamflag_t */
  
  kaapi_barrier_t            barrier __attribute__((aligned (KAAPI_CACHE_LINE)));
  kaapi_lock_t               lock;

  struct kaapi_team_t*       parent_team;
  int                        active_levels;             /* active levels in sens of OpenMP */
  int                        levels;                    /* levels in sens of OpenMP */

  struct kaapi_processor_t** all_kprocessors;

  kaapi_placeset_t           numaplaces; /* only NUMA currently used */
  //Bitmap of numa nodes represented in this task
  //FIXME: this is actually *way* to huge if kaapi_use_sched_affinity
  kaapi_cpuset_t numa_bitmap;
  //FIXME: same here
  kaapi_cpuset_t node_bitmap[32];

  /* OMP work share region */
  struct workshare_t {
    kaapi_lock_t             lock;                      /* for omp lock similar construct */
    kaapi_atomic_t           single;                    /* for omp single similar construct */
    void*                    single_data;               /* for libGOMP copyprivate */
    kaapi_atomic_t           forcst;                    /* for omp for similar construct */
    struct kaapi_foreach_globalwork_t* gwork;           /* for omp for construct, terminaison */
    kaapi_atomic64_t         ordered_index;             /* ordered clause management of loop */
    double                   data[8];                   /* used by omp to avoid small allocation */
  } ws __attribute__((aligned (KAAPI_CACHE_LINE)));

  struct kaapi_processor_t*  save_master;               /* current kproc before team creation */
  struct kaapi_thread_register_t*reg0;                  /* state of the thread register before team init */

  kaapi_atomic_t             started;
  int volatile               thread_started;            /* cache for the main team */
  kaapi_procinfo_t*          all_descriptors;           /* to start thread */
#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_team_perfstat_t     *perf;                     /* set of perf counters */
#endif
} kaapi_team_t __attribute__((aligned (KAAPI_CACHE_LINE)));


enum kaapi_teamflag_t {
  KAAPI_TEAM_NOCACHE
};


/* ========================================================================= */
/* Context data structure                                                    */ 
/* ========================================================================= */
/** The context data structure  ~ POSIX user level thread
    This data structure should be extend in case where the C-stack  is required to be suspended 
    and resumed.

    WARNING: stack should be the first field of the data structure in order to be able 
    to recover in the public kaapi_stack from public kaapi_thread_t*
*/
typedef struct kaapi_context_t {
  kaapi_stack_t                    stack;
  struct kaapi_processor_t*        proc;
  struct kaapi_task_t*             task;              /* running task or 0 - unsued by libKOMP*/
  int                              unstealable;       /* !=0 -> cannot be stolen */

  /* OMP support */
  struct thread_specific_ws {                         /* thread specific work share info */
    int                            count_single;      /* for single as omp single construct */
    int                            count_for;         /* for single as omp single construct */
    int                            count_for_end;     /* for single as omp single construct */
    struct {
      struct kaapi_foreach_work_t* volatile rt_ctxt;  /* for GOMP support */
      long long                    first;
      long long                    last;
      long long                    incr;
      long long                    curr_start;        /* for ordered OMP support */
      long long                    curr_end;
    } for_data;
  } ws;
  void*                            komp_icvs;
  void*                            reduce_data;       /* for libOMP reduction */
  void*                            freebloc_fj;       /* for libOMP */
  kaapi_task_t                     stask;             /* task used when context is suspended */
} __attribute__((aligned (KAAPI_CACHE_LINE))) kaapi_context_t;

/* helper function */
#define kaapi_context2thread(ctxt)      (&(ctxt)->stack.thread)
#define kaapi_context2kproc(ctxt)       ((ctxt)->proc)
#define kaapi_stack2kproc(st)           (((kaapi_context_t*)st)->proc)
#define kaapi_stack2kpi(st)             (&((kaapi_context_t*)st)->proc->rsrc)
#define kaapi_stack2perfproc(st)        (((kaapi_context_t*)st)->proc->rsrc.perfkproc)
#define kaapi_thread2kproc(st)          (((kaapi_context_t*)st)->proc)
#define kaapi_thread2rsrc(st)           (&((kaapi_context_t*)st)->proc->rsrc)
#define kaapi_thread2perfproc(st)       (((kaapi_context_t*)st)->proc->rsrc.perfkproc)
#define kaapi_kproc2thread(p)           (kaapi_context2thread((p)->context))

/* ========================================================================= */
/* Kprocessor                                                                */
/* ========================================================================= */

/**/
struct kaapi_tracelib_context_t;

/* ========================================================================= */
/* Kprocessor                                                                */
/* ========================================================================= */
/** \ingroup WS
    This data structure defines a work stealer processor kernel thread.
    A kprocessor is a container of tasks stored by stack.
    A kproc is part of a team (for non orphan processor). It may be bound
    to a ressource. 
    If hasnowork == 1, then it means that thread has no work and thief does
    not need to iterates over data structure to search for work.
A finest information would be that current context has no meaning because it is
used to receive task during work stealing request. TO TEST
*/
#define KAAPI_KPROC_SPECIFIC_SIZE 16
#define KAAPI_KPROC_SPECIFIC_FREE 8
typedef struct kaapi_ressource_t {
  kaapi_place_t*                  ownplace;      /* terminal place to steal in queue stack */
  kaapi_processor_id_t            kid;           /* ressource id in the context of a team */
  unsigned int                    seed;          /* for the kproc own rand_r random generator */
  unsigned int                    proc_type;     /* processor type */
  int                             numaid;        /* numa id in kaapi_default_param. team */
#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_perfproc_t*               perfkproc;     /* per thread perf counter */
  kaapi_evtproc_t*                evtkproc;      /* per thread event buffer stream */
#endif
} kaapi_ressource_t;

typedef struct kaapi_processor_t {
  kaapi_context_t*         context;              /* current thread under execution */
  kaapi_ressource_t        rsrc;
  kaapi_team_t*            team;                 /* the team */

  /* queues of tasks to suspended thread
     task->body  : the entry point to restore the thread execution
     task->steal : signature int (fnc)(kaapi_task_t*, kaapi_thread_t* thief)
     task->arg   : the suspended thread
  */
  kaapi_lock_t             lock_lsuspend;
  kaapi_list_t             lsuspend;                  /* list of suspended context */
  kaapi_lfree_t            lfree;                     /* free list of contexts */

  /* private kproc fields */
  kaapi_places_part_t      place_part __attribute__((aligned(KAAPI_CACHE_LINE)));
  long                     place_off;   /* in place_part */

  /* dynamic and runtime value */
  uintptr_t                arg_selecfn[8];            /* arguments for select victim function */

  void*                    data_specific[KAAPI_KPROC_SPECIFIC_SIZE]; /* thread data specific */
  size_t                   size_specific[KAAPI_KPROC_SPECIFIC_SIZE]; /* size of data specific */

  char*                    sp_data;
  char                     stack __attribute__((aligned(8))); /* begin of stack for small allocation*/  
} kaapi_processor_t;

/* Value for the next data specific internal key
*/
extern kaapi_atomic_t kaapi_nextprocessor_dataspecific_key;

/* Allocate a new key for data_specific
*/
static inline int kaapi_kproc_newkey(void)
{ return KAAPI_ATOMIC_INCR_ORIG(&kaapi_nextprocessor_dataspecific_key); }

/* ========================================================================= */
/* Interface                                                                 */ 
/* ========================================================================= */
#include "kaapi_offload_stream.h"
#include "kaapi_memory.h"
#include "kaapi_offload.h"

/* Barrier are strongly correlated to team. The number of threads in a barrier is
   the number of threads of the team.
*/
extern void kaapi_barrier_init (struct kaapi_barrier_t *barrier, int count);

/*
*/
extern void kaapi_barrier_destroy (struct kaapi_barrier_t *barrier);


/** Initialize a new team:
    - internal data structure is allocate on the kprocessor stack thread
    Return 0 in case of failure.

    * if kpl != 0 then procbind must be equal to default value.
    * if procbind == default value and kpl is not null, then the threads are created
    following information in kpl.
    * if procbind == default value and kpl is not null then the implementation
    will consider procbind as either master, close or spread

    EINVAL: if kpl ==0 and procbind != komp_proc_bind_default

*/
#define KAAPI_TEAM_DEFAULT 0
#define KAAPI_TEAM_NOCACHE 0x1
#define KAAPI_TEAM_LDSET   0x2  /* use topology information */

extern  kaapi_team_t* kaapi_team_init(
  kaapi_processor_t*         kproc,
  const char*                name,
  int                        count,
  kaapi_procbind_t           procbind,
  void                     (*body)(void*),
  void                     (*wrapper)(void*),
  void*                      arg,
  const kaapi_places_part_t* places,
  int                        flag
);

/** Wait end of a team, do not destroy data structure.
    Signal all threads to terminate their work and wait until the thread view the signal.
    After the call kaapi_self_processor() and kaapi_self_thread() return the master processor
    and master thread that exist previously to the call to kaapi_team_create...
*/
extern kaapi_thread_t* kaapi_team_finalize( kaapi_team_t* team );


/** Terminate a team
    Return 0 in case of success, else return an error code:
    EBUSY: one of the thread has remaining work not finished.
*/
extern int kaapi_team_destroy( kaapi_team_t* team, int force );


/** Start a new team
    The current thread is the thread 0 in the created team.
    The function creates a team following informations passed in the call kaapi_team_init

    Return 0 in case of success, else return an error code:
*/
extern int kaapi_team_start( kaapi_team_t* team );

/** \ingroup WS
    Higher level context manipulation.
    Assign context onto the running processor proc.
    This function is machine dependent.
*/
static inline int kaapi_setcontext( kaapi_processor_t* kproc, kaapi_context_t* ctxt )
{
  if (ctxt !=0)
    ctxt->proc    = kproc;
  kproc->context   = ctxt;
#if defined(KAAPI_HAVE_COMPILER_TLS_SUPPORT)
  kaapi_current_thread_key = &ctxt->stack.thread;
#endif
  return 0;
}
/*@}*/

extern void* kaapi_processor_alloc_data( kaapi_processor_t* kproc, size_t size );

extern kaapi_stack_bloc_t* kaapi_processor_alloc_stackbloc(
    kaapi_processor_t* kproc
);

extern void kaapi_processor_dealloc_stackbloc(
    kaapi_processor_t* kproc, 
    kaapi_stack_bloc_t* bloc
);


/* OMP support
*/
extern struct kaapi_processor_t* kaapi_create_orphan_kprocessor(void);

/* OMP support
*/
extern void kaapi_destroy_orphan_kprocessor(void);


/* Default entry point when CPU thread is created 
*/
extern void* kaapi_default_sched_run_processor( void* arg );


/** \ingroup TASK
    The function kaapi_context_alloc() allocates in the heap a context with a stack containing 
    at bytes for tasks and bytes for data.
    If successful, the kaapi_context_alloc() function will return a pointer to a kaapi_context_t.  
    Otherwise, an error number will be returned to indicate the error.
    This function is machine dependent.
    \param kproc IN/OUT the kprocessor that make allocation
    \retval pointer to the context
    \retval 0 if allocation failed
*/
extern kaapi_context_t* kaapi_context_alloc( struct kaapi_processor_t* kproc );

/** \ingroup TASK
    The function kaapi_context_destroy() free the ressource allocated with
    kaapi_context_alloc.
    If successful, the kaapi_context_destroy() function will return zero.
    Otherwise, an error number will be returned to indicate the error.
    This function is machine dependent.
    \param kproc IN/OUT the kprocessor that make allocation
    \param ctxt INOUT a pointer to the kaapi_context_t to allocate.
    \retval EINVAL invalid argument: bad stack pointer.
*/
extern int kaapi_context_destroy(
    kaapi_processor_t* kproc, 
    kaapi_context_t* ctxt 
);

/** \ingroup TASK
    The function kaapi_context_free() free the context successfuly allocated with 
    kaapi_context_alloc.
    If successful, the kaapi_context_free() function will return zero.  
    Otherwise, an error number will be returned to indicate the error.
    This function is machine dependent.
    \param kproc IN/OUT the kprocessor that make allocation
    \param ctxt INOUT a pointer to the kaapi_context_t to allocate.
    \retval EINVAL invalid argument: bad stack pointer.
*/
extern int kaapi_context_free( struct kaapi_processor_t* kproc, kaapi_context_t* ctxt );


/** \ingroup TASK
    The function kaapi_context_clear re-initialize the context data structure
    If successful, the kaapi_context_clear() function returns 0.  
*/
extern int kaapi_context_clear( kaapi_context_t* ctxt );


#if defined(KAAPI_HAVE_COMPILER_TLS_SUPPORT)
extern __thread kaapi_processor_t*      kaapi_current_processor_key;
extern __thread kaapi_ressource_t* kaapi_current_processor_info_key;

/* */
static inline kaapi_processor_t* kaapi_self_processor(void)
{ return kaapi_current_processor_key; }
static inline kaapi_ressource_t* kaapi_self_rsrc(void)
{ return kaapi_current_processor_info_key; }

static inline kaapi_context_t* kaapi_self_context(void)
{ return kaapi_current_processor_key->context;}


static inline void kaapi_set_self_ressource( kaapi_ressource_t* kpi )
{
  kaapi_current_processor_info_key = kpi;
}
static inline void kaapi_set_self_processor( kaapi_processor_t* kproc )
{
  kaapi_current_processor_key      = kproc;
  kaapi_current_processor_info_key = &kproc->rsrc;
  if (kproc ==0)
    kaapi_current_thread_key    = 0;
  else
    kaapi_current_thread_key    = &kproc->context->stack.thread;
}


#else
extern pthread_key_t kaapi_current_processor_key;
extern pthread_key_t kaapi_current_processor_info_key;

static inline kaapi_processor_t* kaapi_self_processor(void)
{ return ((kaapi_processor_t*)pthread_getspecific( kaapi_current_processor_key )); }
static inline kaapi_ressource_t* kaapi_self_rsrc(void)
{ return ((kaapi_ressource_t*)pthread_getspecific( kaapi_current_processor_info_key )); }

static inline kaapi_context_t* kaapi_self_context(void)
{ return kaapi_self_processor()->context;}

static inline void kaapi_set_self_ressource( kaapi_ressource_t* kpi )
{
  kaapi_assert(0 == pthread_setspecific(kaapi_current_processor_info_key, kpi));
}
static inline void kaapi_set_self_processor( kaapi_processor_t* kproc )
{
  kaapi_assert(0 == pthread_setspecific(kaapi_current_processor_key, kproc));
  kaapi_assert(0 == pthread_setspecific(kaapi_current_processor_info_key, &kproc->rsrc));
}
#endif


/*
*/
static inline unsigned int kaapi_processor_get_type(const kaapi_processor_t* kproc)
{
  return kproc->rsrc.proc_type;
}

/** Return true if the kprocess has no work in hierarchy queue
*/
static inline int kaapi_processor_has_nowork_hierarchy( const kaapi_processor_t* kproc )
{
  return 1;
}

/* */
extern void kaapi_init_kprocld( kaapi_processor_t* kproc, kaapi_kprocplace_t* ld );

/* */
extern void kaapi_init_plainplace( kaapi_plainplace_t* ld );

/* */
extern void kaapi_init_place( kaapi_place_t* ld );

/* */
static inline int kaapi_localitydomain_has_nowork( const kaapi_place_t* ld)
{
  return ld->vtable->f_isempty(ld);
}

/* Return 0 iff the place is not empty
*/
static inline int kaapi_place_empty( const kaapi_place_t* ld)
{
  return ld->vtable->f_isempty(ld);
}

/* Steal and return a stolen task.
   By comparizon kaapi_place_stealinplace return the task into the stack of the caller.
   TODO: unification
*/
static inline kaapi_task_t* kaapi_place_steal( kaapi_ressource_t* kpi, kaapi_place_t* ld)
{ return ld->vtable->fc_steal( kpi, ld ); }

/*
*/
static inline int kaapi_place_stealinplace( kaapi_processor_t* kproc, kaapi_place_t* ld)
{ return ld->vtable->fc_stealin( kproc, ld ); }


/* 
*/
static inline kaapi_task_t* kaapi_place_pop(
    kaapi_ressource_t* kpi,
    kaapi_place_t* ld
)
{ return ld->vtable->fc_pop( kpi, ld ); }

/* 
*/
static inline int kaapi_place_push(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* ld,
    kaapi_task_t* task
)
{ return ld->vtable->fc_push( rsrc, ld, task ); }


/* push to remote place, insert in tail
*/
static inline int kaapi_place_push_remote(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* ld,
    kaapi_task_t* task
)
{ return ld->vtable->fc_push_remote( rsrc, ld, task ); }


/* 
*/
static inline int kaapi_place_push_async(
    struct kaapi_place_group_operation_t* kpgo,
    kaapi_ressource_t* rsrc,
    kaapi_place_t* ld,
    kaapi_task_t* task
)
{ return ld->vtable->fc_push_async( kpgo, rsrc, ld, task ); }


/* push to remote place, insert in tail
*/
static inline int kaapi_place_push_remote_async(
    struct kaapi_place_group_operation_t* kpgo,
    kaapi_ressource_t* rsrc,
    kaapi_place_t* ld,
    kaapi_task_t* task
)
{ return ld->vtable->fc_push_remote_async( kpgo, rsrc, ld, task ); }


/*
*/
static inline int kaapi_place_pushlist(
    kaapi_ressource_t* rsrc,
    kaapi_place_t*     ld,
    kaapi_list_t*      list
)
{ return ld->vtable->fc_pushlist( rsrc, ld, list ); }


/* Call any func, on the place: func( ld, arg)
   Return the value of func.
*/
extern int kaapi_place_internalop(
    kaapi_ressource_t* rsrc,
    kaapi_place_t* ld,
    int (*func)(kaapi_place_t*, void* ), void* arg
);

/*
*/
static inline void kaapi_place_destroy( kaapi_place_t* ld)
{
  ld->vtable->f_destroy(ld);
}

#endif /* _KAAPI_MT_MACHINE_H */
