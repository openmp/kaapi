/*
** kaapi_init.c
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com 
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include <stdlib.h>
#include <inttypes.h> 
#include <string.h>
#include <stdio.h>
#include "kaapi_impl.h"
#include "kaapi_util.h"

#if defined(KAAPI_DEBUG)
#  include <unistd.h>
#  include <sys/time.h>
#  include <signal.h>
#endif

#if defined(KAAPI_USE_PERFCOUNTER)
#include <signal.h>
#endif
#if defined(__linux__)
#include <unistd.h>
#endif


/* Fwd decl
*/
static int kaapi_setup_param(void);


/* this function return 1 iff the environement variable is listed in recognized list of var */
static char* list_official_evar[] = {
  "KAAPI_ENV_STRICT",
  "KAAPI_CPUCOUNT",
  "KAAPI_CPUSET",
  "KAAPI_WSSELECT",
  "KAAPI_WSPUSH",
  "KAAPI_WSPUSH_INIT",
  "KAAPI_STRICT_PUSH",
  "KAAPI_BLOCK_CYCLIC",
  "KAAPI_EMITSTEAL",
  "KAAPI_DISPLAY_PERF",
  "KAAPI_GOMP_SHOW_BANNER",
  "KAAPI_DISPLAY_ENV",
#if defined(KAAPI_USE_OFFLOAD)
  "KAAPI_PLUGIN_PATH",
  "KAAPI_DEVICE_TYPE",
#endif
#if defined(KAAPI_USE_PERFCOUNTER)
  "KAAPI_RECORD_MASK",
  "KAAPI_RECORD_TRACE",
  "KAAPI_PERF_EVENTS",
  "KAAPI_TASKPERF_EVENTS",
  "KAAPI_DUMP_GRAPH",
#endif
  0
};

static int kaapi_findenv(const char* evar)
{
  char** curr = list_official_evar;
  char* eq = strchr(evar, '=');
  if (eq == 0)
    kaapi_abort(__LINE__,__FILE__, "Please contact master chief");
  size_t l = eq - evar;
  while (*curr != 0)
  {
    if (strncmp(evar, *curr, l) ==0)
      return 1;
    ++curr;
  }
  return 0;
}



/** \ingroup WS
    Initialize from xkaapi runtime parameters from command line
    \param argc [IN] command line argument count
    \param argv [IN] command line argument vector
    \retval 0 in case of success 
    \retval EINVAL because of error when parsing then KAAPI_CPUSET string
    \retval E2BIG because of a cpu index too high in KAAPI_CPUSET
*/
static int kaapi_setup_param(void)
{
  /* workstealing selection function */
  /* code move to kaapi_mt_init to avoid double parsing with kaapi and omp. Only for experiemnts*/
  
  kaapi_default_param.alarmperiod = 0;
  if (getenv("KAAPI_DUMP_PERIOD") !=0)
  {
    kaapi_default_param.alarmperiod = atoi(getenv("KAAPI_DUMP_PERIOD"));
    if (kaapi_default_param.alarmperiod <0)
      kaapi_default_param.alarmperiod = 0;
  }

  kaapi_default_param.cpuset_str   = getenv("KAAPI_CPUSET");
  kaapi_default_param.cpucount_str = getenv("KAAPI_CPUCOUNT");

  /* verify mistake in name of KAAPI_ environment variable */
  extern char **environ;
  
  char** curr = environ;
  while (*curr != 0)
  {
    if (strncmp(*curr,"KAAPI_", 6) ==0)
    {
      if (!kaapi_findenv(*curr))
      {
        fprintf(stderr,
          "*** warning environment variable '%s' is not recognized by this version of Kaapi\n",
          *curr
        );
        if (getenv("KAAPI_ENV_STRICT") !=0)
          kaapi_abort(__LINE__,__FILE__, "Bad env. variable");
      }
    }
    ++curr;
  }

  return 0;
}


/*
*/
int kaapi_init(int flag, int* argc, char*** argv)
{
  /* set up runtime parameters */
  kaapi_assert( 0 == kaapi_setup_param() );

  /* FIXEME : currently KAAPI should be used with OMP_PLACES defined ... 
  */
  if (kaapi_default_param.cpuset_str ==0)
  {
    kaapi_mt_hwdetect();
    char tmp[32];
    memset(tmp,0,32);
    if (kaapi_default_param.cpucount_str ==0)
      snprintf(tmp, 32, "threads(%i)", kaapi_default_param.syscpucount);
    else
      snprintf(tmp, 32, "threads(%s)", kaapi_default_param.cpucount_str);
    setenv("KAAPI_CPUSET",tmp, 1);
    kaapi_default_param.cpuset_str = getenv("KAAPI_CPUSET");
  }

  return kaapi_mt_init(flag);
}


/*
*/
int kaapi_finalize(void)
{
  return kaapi_mt_finalize();
}

