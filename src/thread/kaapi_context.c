/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>


#define KAAPI_ALIGNUP(s,a) (((s)+(a)-1) & ~((a)-1))

/* lfree list routines
 */
static inline int kaapi_lfree_isempty( const kaapi_lfree_t* list)
{
  return list->_sizelfree == 0;
}

/* return 1 iff the ctxt is pushed into free list else return 0
*/
static inline int kaapi_lfree_push(
  kaapi_lfree_t* list, 
  struct kaapi_context_t* ctxt
)
{
#  define KAAPI_MAXFREECTXT 64
  if (list->_sizelfree >= KAAPI_MAXFREECTXT)
    return 0;

  kaapi_list_push_head(&list->list, &ctxt->stask);
  ++list->_sizelfree;

  return 1;
}


/* Re-design interface between context_free / context_alloc and lfree_push and lfree_pop
*/
static inline kaapi_context_t* kaapi_lfree_pop(kaapi_lfree_t* list)
{
  if (kaapi_list_empty(&list->list)) return 0;
  kaapi_task_t* stask = kaapi_list_pop_head(&list->list);
  kaapi_context_t* context = (kaapi_context_t*)stask->arg;
  --list->_sizelfree;
  return context;
}


/*
*/
kaapi_context_t* kaapi_context_alloc( kaapi_processor_t* kproc )
{
  kaapi_context_t* ctxt;
  size_t k_ctxtsize;
  size_t pagesize;
  
  /* already allocated bloc */
  if (!kaapi_lfree_isempty(&kproc->lfree))
  {
    ctxt = kaapi_lfree_pop(&kproc->lfree);
    kaapi_context_clear(ctxt);
    return ctxt;
  }

  pagesize = getpagesize();
  k_ctxtsize = KAAPI_ALIGNUP(sizeof(kaapi_context_t), pagesize);
  kaapi_assert_debug( k_ctxtsize % pagesize == 0);
  
  ctxt = (kaapi_context_t*) mmap(
    0, k_ctxtsize, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, (off_t)0
  );

  if (ctxt == (kaapi_context_t*)-1) 
  {
#if defined(KAAPI_DEBUG)
    fprintf(stderr,"*** error cannot allocate context\n");
    kaapi_abort(__LINE__,__FILE__, "Invalid case");
#endif
    return 0;
  }

  memset( ctxt, 0, k_ctxtsize );
  
  kaapi_stack_init( kproc, &ctxt->stack );

  /* same as context_clear without calling stack reset (else two times the same op) */
  ctxt->proc           = 0;
  ctxt->task           = 0;
  ctxt->unstealable    = 0;

  ctxt->ws.count_single= 0;
  ctxt->ws.count_for   = 0;
  ctxt->ws.count_for_end    = 0;
  ctxt->ws.for_data.rt_ctxt = 0;
  ctxt->komp_icvs      = 0;

  ctxt->stask.arg      = ctxt;

  return ctxt;
}


/*
*/
int kaapi_context_destroy(
    kaapi_processor_t* kproc, 
    kaapi_context_t* ctxt 
)
{
  if (ctxt ==0) 
    return 0;

  if (kaapi_lfree_push(&kproc->lfree, ctxt))
    return 0;

  return kaapi_context_free(kproc, ctxt);
}


/*
*/
int kaapi_context_free(
    kaapi_processor_t* kproc, 
    kaapi_context_t* ctxt 
)
{
  int err = kaapi_stack_destroy(kproc, &ctxt->stack);
  kaapi_assert_debug(err == 0);

  size_t k_stacksize;
  size_t pagesize;
  pagesize = getpagesize();
  k_stacksize = KAAPI_ALIGNUP(sizeof(kaapi_context_t),pagesize);
  kaapi_assert_debug( k_stacksize % pagesize == 0);

  err = munmap(ctxt, k_stacksize);
  KAAPI_RETURN_ERROR(err, 0);
}


/**
*/
int kaapi_context_clear( kaapi_context_t* ctxt )
{
  kaapi_stack_reset( &ctxt->stack );
  ctxt->proc           = 0;
  ctxt->unstealable    = 0;

  ctxt->ws.count_single= 0;
  ctxt->ws.count_for   = 0;
  ctxt->ws.count_for_end    = 0;
  ctxt->ws.for_data.rt_ctxt = 0;
  ctxt->komp_icvs      = 0;

  return 0;
}

