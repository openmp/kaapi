/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com 
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#if defined(KAAPI_USE_NUMA)
#  include <numaif.h>
#  include <numa.h>
#  ifndef _GNU_SOURCE
#    define _GNU_SOURCE
#  endif
#    include <sched.h>
#endif

#if defined(KAAPI_DEBUG)
#include "kaapi_dbg.h"
#include "kaapi_util.h" /* unparse_places */
#endif


#if defined(__MIC__) || defined(__linux__)
#ifndef _GNU_SOURCE
#  define _GNU_SOURCE         /* See feature_test_macros(7) ??? */
#endif
#include <sched.h>
#include <unistd.h>
#include <stdio.h>
#endif

/* Fwd decl
*/
static int kaapi_team_addthread( kaapi_team_t* team, kaapi_procinfo_t* kpi );
static int kaapi_team_creates_threads(
  kaapi_team_t* team, kaapi_procinfo_t* kpi_start, kaapi_procinfo_t* kpi_end
);

static kaapi_atomic_t kaapi_running_threads = {1};

/*
*/
kaapi_atomic_t kaapi_nextprocessor_dataspecific_key = {0};


/*
*/
int kaapi_get_running_threads(void)
{
  return KAAPI_ATOMIC_READ(&kaapi_running_threads);
}


/** \ingroup WS
    Alloc same data in the same memory region than the kproc data structure itself
*/
void* kaapi_processor_alloc_data( kaapi_processor_t* kproc, size_t size )
{
  char* retval;
  size_t align =16;
  const uintptr_t mask = align - (uintptr_t)1;
  retval = (char*)((uintptr_t)(kproc->sp_data + mask) & ~mask);
  if ( ((retval + size) - (char*)kproc) >= KAAPI_KPROCESSOR_SIZE )
    return 0;
  kproc->sp_data = retval+size;
  return retval;
}



/*
*/
static void _kaapi_processor_register_hierarchy( kaapi_processor_t* kproc )
{
  kaapi_team_t* team = kproc->team;
  if (team ==0) 
    return;
  if ((team->flag & KAAPI_TEAM_LDSET) ==0)
    return;
#if defined(KAAPI_USE_SCHED_AFFINITY) 
  /* all cpuset */
  int index = kproc->place_part.first + kproc->place_off-kaapi_default_param.places.first;
  kaapi_cpuset_t* selfplace = &kaapi_default_param.places.first[ index % kaapi_default_param.places.len ];
  kaapi_assert_debug( selfplace < (kaapi_default_param.places.first+kaapi_default_param.places.len) );
  int i;
  for (i=0; i<kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count; ++i)
  {
    kaapi_cpuset_t p;
    KAAPI_CPUSET_ZERO(&p);
    KAAPI_CPUSET_AND(&p, selfplace, &kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].cpuset[i]);
    if (KAAPI_CPUSET_COUNT(&p) !=0)
    {
      kproc->rsrc.numaid = i;
      /*A kproc can only belong to one numanode*/
      break;
    }
  }
#if defined(KAAPI_USE_NUMA)
  /*Register ourselves to the team*/
  if (!KAAPI_CPUSET_ISSET(i, &(team->numa_bitmap))) {
    KAAPI_CPUSET_ATOMIC_SET(i, &(team->numa_bitmap));
  }
  KAAPI_CPUSET_ATOMIC_SET(kproc->rsrc.kid, &(team->node_bitmap[i]));
#endif
#endif
}


/* never do menset on the kproc. sp_data is initialized on allocation.
   Assume thread is correctly attach to its cpuset.
*/
static int kaapi_processor_init(
    kaapi_processor_t*   kproc,
    kaapi_team_t*        team,
    kaapi_procinfo_t*    kpi
)
{
  kaapi_context_t* ctxt;
  kaapi_processor_id_t kid = kpi->proc_index;
  kproc->context       = 0;  
  kproc->rsrc.kid      = kid;
  kproc->rsrc.proc_type= kpi->proc_type;

  kproc->place_part     = kpi->place_part;
  kproc->place_off      = kpi->place_off;
#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
  sched_yield();
  int rcpu = sched_getcpu();
  if (kproc->place_part.first !=0)
  {
    int index = kpi->place_part.first + kpi->place_off-kaapi_default_param.places.first;
#if defined(KAAPI_DEBUG)
    kaapi_cpuset_t* place = &kaapi_default_param.places.first[ index % kaapi_default_param.places.len ];
    kaapi_assert( place < (kaapi_default_param.places.first+kaapi_default_param.places.len) );
#endif
//TODO: the assertion seems to fail on some machine
    //kaapi_assert( KAAPI_CPUSET_ISSET( rcpu, place ) );
  }
#endif
  kproc->team          = team;

#if defined(KAAPI_USE_PERFCOUNTER)
  /* allocate perfcounter prior to other initialization */
  kproc->rsrc.perfkproc = kaapi_tracelib_thread_init(
      &kproc->rsrc.evtkproc,
      team->perf,
      KAAPI_PROC_TYPE_CPU,
      kproc->rsrc.kid,
      kproc->rsrc.numaid,
      KAAPI_PERF_SYS_STATE);
#endif

  /* local queue */
  kproc->rsrc.ownplace = kaapi_processor_alloc_data(kproc, sizeof(kaapi_kprocplace_t));
  kaapi_init_kprocld(kproc, (kaapi_kprocplace_t*)kproc->rsrc.ownplace);

  kproc->rsrc.seed = (unsigned int)kproc->rsrc.kid;
  for (int i=0; i<8; ++i)
    kproc->arg_selecfn[i] = 0;

  kaapi_atomic_initlock( &kproc->lock_lsuspend );
  kaapi_list_init( &kproc->lsuspend );

  /* register processor NUMA level */
  _kaapi_processor_register_hierarchy( kproc );

  ctxt = kaapi_context_alloc( kproc );
  kaapi_assert(ctxt !=0);

  /* set new context to the kprocessor */
  kaapi_setcontext(kproc, ctxt);
  
  memset(&kproc->data_specific, 0, KAAPI_KPROC_SPECIFIC_SIZE*sizeof(void*));
  memset(&kproc->size_specific, 0, KAAPI_KPROC_SPECIFIC_SIZE*sizeof(size_t));

  return 0;
}


/*
*/
static int kaapi_processor_destroy(kaapi_processor_t* kproc)
{
#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_tracelib_thread_fini( kproc->rsrc.perfkproc, kproc->rsrc.evtkproc );
#endif

  kaapi_assert_debug( kaapi_list_empty(&kproc->lsuspend) );

  if (kproc->context !=0)
  {
    kaapi_context_t* tmp = kproc->context;
    kaapi_setcontext( kproc, 0);
    kaapi_context_free( kproc, tmp );
  }

  kaapi_atomic_destroylock( &kproc->lock_lsuspend );
  kaapi_place_destroy( kproc->rsrc.ownplace );

  for (int i=0; i<KAAPI_KPROC_SPECIFIC_SIZE; ++i)
  {
    if (kproc->data_specific[i] !=0)
    {
      free(kproc->data_specific[i]);
    }
    KAAPI_DEBUG_INST(
      kproc->data_specific[i] = 0;
      kproc->size_specific[i] = 0;
    )
  }
  return 0;
}


#define KAAPI_ALIGNUP(s,a) (((s)+(a)-1) & ~((a)-1))

static inline kaapi_processor_t* kaapi_processor_allocate(void)
{
  size_t size = KAAPI_ALIGNUP(KAAPI_KPROCESSOR_SIZE, getpagesize());
  kaapi_assert_debug( sizeof(kaapi_processor_t) < size );
  kaapi_processor_t* kproc = (kaapi_processor_t*) mmap(
    0, size, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, (off_t)0
  );

  if (kproc == (kaapi_processor_t*)-1)
    return 0;
  
  kproc->sp_data = &kproc->stack;
  return kproc;
}

static inline void kaapi_processor_free(kaapi_processor_t* kproc)
{
  size_t size = getpagesize();
  munmap(kproc, size);
}

kaapi_atomic_t count_teamid = {0};



/*
*/
static inline void _kaapi_team_init_ds( kaapi_team_t* team, int count )
{
  team->isterm           = 0;
  team->reg0             = 0;
  team->barrier.term     = 0;
  team->ws.gwork         = 0;
  KAAPI_ATOMIC_WRITE( &team->ws.single, 0 );
  KAAPI_ATOMIC_WRITE( &team->ws.forcst, 0 );
  KAAPI_ATOMIC_WRITE( &team->ws.ordered_index, 0 );
}


/* Static kaapi_cached_global_team team, the only that is cached
*/
kaapi_team_t* kaapi_cached_global_team = 0;


/*
*/
kaapi_team_t* kaapi_team_init(
  kaapi_processor_t*         kproc,
  const char*                name,
  int                        count,
  kaapi_procbind_t           procbind,
  void                     (*body)(void*),
  void                     (*wrapper)(void*),
  void*                      arg,
  const kaapi_places_part_t* places,
  int                        flag
)
{
  int i;
  kaapi_team_t* team;
  kaapi_thread_t* thread;
  kaapi_thread_register_t reginit0;

  if (kproc !=0)
  {
    thread = kaapi_context2thread(kproc->context);
    kaapi_thread_save( thread, &reginit0 );
  }
  else
    thread = 0;

  if (count == 0)
    count = kaapi_default_param.cpucount;
  if (count <=0)
  {
    errno = EINVAL;
    return 0;
  }

  /* cache only the main team if KAAPI_TEAM_NOCACHE not defined */
  if (   ((flag & KAAPI_TEAM_NOCACHE)==0)
      && (kaapi_cached_global_team !=0)
      && (kaapi_cached_global_team->count == count)
      && ((kproc ==0) || (kproc->team== kaapi_cached_global_team->parent_team))
  )
  {
    team = kaapi_cached_global_team;
    /* reset some team fields even if reused */
    _kaapi_team_init_ds(team, count);
    for (i = 0; i<count; ++i)
    {
      team->all_descriptors[i].proc_type         = KAAPI_PROC_TYPE_HOST;
      team->all_descriptors[i].body_runprocessor = (wrapper ? wrapper : body);
      team->all_descriptors[i].arg_runprocessor  = arg;
    }
  }
  else
  {
    int do_dynamic;
    char* data;

    /* if can be cached, then do dynamic allocation */
    if ((thread !=0) && !(((kaapi_cached_global_team ==0) && ((flag & KAAPI_TEAM_NOCACHE)==0))))
    {
      team = (kaapi_team_t*)kaapi_data_push_align( thread, sizeof(kaapi_team_t), KAAPI_CACHE_LINE_SIZE);
      team->tid            = (kaapi_thread_id_t)kproc->rsrc.kid;
      team->parent_team    = kproc->team;
      do_dynamic = 0;
    }
    else
    {
      do_dynamic = 1;
      size_t s = (size_t)KAAPI_CACHE_LINE_SIZE;
      if (0 != posix_memalign( (void**)&team, s, sizeof(kaapi_team_t)) )
        return 0;
#if defined(KAAPI_DEBUG)
      memset(team, 0, sizeof(kaapi_team_t) );
#endif
      team->tid            = 0;
      team->parent_team    = (kproc ? kproc->team : 0);
    }

    /* clear team fields */
    _kaapi_team_init_ds(team, count);
    KAAPI_ATOMIC_WRITE(&team->started,0);
    team->thread_started   = 0;
    team->count            = 0;
    kaapi_atomic_initlock(&team->lock );
    kaapi_atomic_initlock(&team->ws.lock );
    kaapi_barrier_init(&team->barrier, count);

    if (team->parent_team !=0)
    {
      team->levels = 1+team->parent_team->levels;
      if (team->count >1)
        team->active_levels = 1+team->parent_team->active_levels;
      else
        team->active_levels = team->parent_team->active_levels;
    }
    else
    {
      team->levels = 0;
      team->active_levels = 0;
    }


#if defined(KAAPI_USE_SCHED_AFFINITY)
#endif

    unsigned int size_data = count* (sizeof(kaapi_processor_t*)+sizeof(kaapi_procinfo_t)
#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
    + sizeof(kaapi_place_t*)*kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count
#endif
    )
    ;

    if (!do_dynamic)
      data = kaapi_data_push_align( thread, size_data, 8);
    else {
      if (0 != posix_memalign( (void**)&data, KAAPI_CACHE_LINE_SIZE, size_data) )
        data = 0; // error
    }
    if (data ==0)
    {
      errno = ENOMEM;
      return 0;
    }
    team->all_kprocessors = (kaapi_processor_t**)data;
    data += count*sizeof(kaapi_processor_t*);

    team->all_descriptors = (kaapi_procinfo_t*)data;
    data += count*sizeof(kaapi_procinfo_t);

#if defined(KAAPI_USE_SCHED_AFFINITY) && defined(KAAPI_USE_NUMA)
    team->numaplaces.count  = 0;
    team->numaplaces.places = (kaapi_place_t**)data;
    data += kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count*sizeof(kaapi_place_t*);
    KAAPI_CPUSET_ZERO(&(team->numa_bitmap));
    /*FIXME hardcoded 32 is bad*/
    for (int i = 0; i < 32; i++)
      KAAPI_CPUSET_ZERO(&(team->node_bitmap[i]));
#endif

#if defined(KAAPI_USE_PERFCOUNTER)
    team->perf = kaapi_tracelib_team_init(
      count,
      (void* (*)())body,
      name,
      0,
      team->parent_team ? team->parent_team->perf : 0
    );
#endif

    /* compute team hierarchy */
    if (flag & KAAPI_TEAM_LDSET)
    {
      team->flag = KAAPI_TEAM_LDSET;
#if defined(KAAPI_USE_SCHED_AFFINITY)
      kaapi_assert_debug( (places ==0) || (kproc ==0)
        || (kproc->place_part.len == 0) || (kproc->place_part.len == places->len) );
      kaapi_assert_debug( (places ==0) || (kproc ==0) || (kproc->place_part.len ==0) ||
        ((kproc->place_part.first == places->first) && (kproc->place_off < places->len)) );
      /* all cpuset */
      kaapi_cpuset_t forallplaces;
      KAAPI_CPUSET_ZERO(&forallplaces);
      for (i=0; i<places->len; ++i)
      {
        int index = places->first+i-kaapi_default_param.places.first;
        kaapi_assert_debug( index % kaapi_default_param.places.len < kaapi_default_param.places.len );
        KAAPI_CPUSET_OR(&forallplaces, &forallplaces, &kaapi_default_param.places.first[ index % kaapi_default_param.places.len ] );
      }

#if defined(KAAPI_USE_NUMA)
      for (i=0; i<kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count; ++i)
      {
        kaapi_cpuset_t p;
        KAAPI_CPUSET_AND(&p, &forallplaces, &kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].cpuset[i]);
        if (KAAPI_CPUSET_COUNT(&p) !=0)
        {
          team->numaplaces.places[team->numaplaces.count++] =
            kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].places[i];
        }
      }
#endif
#endif
    }
    else
      team->flag = 0;

    /* compute the team partition */
    long P = (places == 0 ? 1 : places->len);
    long T = count;
    long s=0,rp =0,k,t=0,rt=0;
    long omaster = 0; /* thread/master offsets place in places */

    if (procbind == KAAPI_PROCBIND_DEFAULT)
      procbind = kaapi_default_param.use_affinity ? KAAPI_PROCBIND_SPREAD : KAAPI_PROCBIND_FALSE;

    if (procbind != KAAPI_PROCBIND_FALSE)
    {
      omaster = (kproc ==0 ? 0 : kproc->place_off); /* old: through context */
      if (procbind == KAAPI_PROCBIND_TRUE)
        procbind = KAAPI_PROCBIND_SPREAD;
      switch (procbind)
      {
        case KAAPI_PROCBIND_TRUE:
        case KAAPI_PROCBIND_MASTER:
          break;
        case KAAPI_PROCBIND_CLOSE:
          break;
        case KAAPI_PROCBIND_SPREAD:
          k = (T<=P ? T : P);   /* k = number of partitions */
          s = P/k;              /* s = number of places per partition */
          rp = P-s*k;           /* r = number of partitions with s+1 places */
          t = T/k;              /* t = number of threads per partition */
          rt = T-k*t;           /* t = number of partitions with t+1 threads */
          break;
        default:
          kaapi_assert(0);
      }
    }

    /**/
    memset( (void*)&team->all_kprocessors[0], 0, sizeof(team->all_kprocessors[0])*count );

    /* allows to have first+len greather than global_first+global_len, index is modulo the size */
    for (i = 0; i<count; ++i)
    {
      team->all_descriptors[i].team                = team;
      team->all_descriptors[i].proc_index          = i;
      switch (procbind)
      {
        case KAAPI_PROCBIND_FALSE:
          team->all_descriptors[i].place_off        = 0;
          team->all_descriptors[i].place_part.first = 0;
          team->all_descriptors[i].place_part.len   = 0;
          break;

        case KAAPI_PROCBIND_TRUE:
        case KAAPI_PROCBIND_MASTER:
          team->all_descriptors[i].place_part= *places;
          team->all_descriptors[i].place_off = omaster;
          break;
        case KAAPI_PROCBIND_CLOSE:
          team->all_descriptors[i].place_part = *places;
          team->all_descriptors[i].place_off  = (omaster+i) % P;
          break;
        case KAAPI_PROCBIND_SPREAD:
        {
          long place_first, part_len;
          long part =(i < rt*(t+1)) ? i / (t+1) : rt+(i-rt*(t+1))/t;
          if (part < rp )
          {
            place_first = part*(s+1);
            part_len = s+1;
          }
          else
          {
            place_first =(part-rp)*s + rp*(s+1);
            part_len = s;
          }
          team->all_descriptors[i].place_part.first = places->first+(place_first+omaster) % P;
          team->all_descriptors[i].place_part.len   = part_len;
          team->all_descriptors[i].place_off        = 0;
        } break;
        default:
          kaapi_assert(0);
      }
      team->all_descriptors[i].proc_type         = KAAPI_PROC_TYPE_HOST;
      team->all_descriptors[i].body_runprocessor = (wrapper ? wrapper : body);
      team->all_descriptors[i].arg_runprocessor  = arg;
    }

    /* default processor number */
    team->count = count;
    if ((kaapi_cached_global_team ==0) && ((flag & KAAPI_TEAM_NOCACHE)==0))
      kaapi_cached_global_team = team;
  }

  team->save_master = kproc;
  if (kproc !=0)
  {
    team->reg0           = kaapi_thread_pushdata( thread, sizeof(kaapi_thread_register_t) );
    *team->reg0          = reginit0;
  }
  team->teamid = KAAPI_ATOMIC_INCR(&count_teamid);

  return team;
}

#define RDTSC_FINAL 0
#if RDTSC_FINAL
/*
*/
static inline uint64_t rdtsc(void)
{
  uint32_t lo,hi;
  __asm__ volatile ( "rdtsc" : "=a" ( lo ) , "=d" ( hi ) );
  return (uint64_t)hi << 32UL | lo;
}
#endif

/*
*/
kaapi_thread_t* kaapi_team_finalize( kaapi_team_t* team )
{
  kaapi_assert_debug( kaapi_self_processor() == team->all_kprocessors[0] );
  kaapi_processor_t* kproc0 = team->all_kprocessors[0];

#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_tracelib_thread_switchstate(
    kproc0->rsrc.perfkproc,
    kproc0->rsrc.evtkproc,
    KAAPI_PERF_SYS_STATE
  );

  /* new barrier at the end of parallel region to schedule tasks and returns */
  kaapi_team_barrier_wait( team, kproc0, KAAPI_BARRIER_FLAG_DEFAULT );

//  team->perf->t_end = kaapi_get_elapsedns();
  kaapi_tracelib_thread_stop(
    kproc0->rsrc.perfkproc,
    kproc0->rsrc.evtkproc
  );
#endif

#if RDTSC_FINAL
  double t0 = kaapi_get_elapsedtime();
#endif
  /* normal implicit barrier at the end of parallel region */
  kaapi_team_barrier_wait( team, kproc0, KAAPI_BARRIER_FLAG_DEFAULT );
#if RDTSC_FINAL
  double t1 = kaapi_get_elapsedtime();
#endif

#if defined(KAAPI_USE_PERFCOUNTER)
    kaapi_tracelib_team_fini(team->perf);
#endif

  KAAPI_ATOMIC_SUB(&kaapi_running_threads, team->count-1);

  kaapi_assert_debug( kaapi_self_kid() == 0 );
  kaapi_context_clear( team->all_kprocessors[0]->context );
  kaapi_set_self_processor( team->save_master );
  if (team->save_master !=0)
  {
    kaapi_assert_debug( team->reg0 != 0);
    kaapi_thread_restore( kaapi_context2thread(team->save_master->context), team->reg0 );
  }
#if RDTSC_FINAL
  double t2 = kaapi_get_elapsedtime();
  printf("[kaapi_team_finalize] wait:%f, cleaning:%f\n", t1-t0, t2-t1);
#endif

  if (team->save_master !=0)
    return kaapi_context2thread(team->save_master->context);
  return 0;
}


/*
*/
int kaapi_team_destroy( kaapi_team_t* team, int force)
{
  /* only destroy non initial team */
  if ((team != kaapi_cached_global_team) || force)
  {
    team->isterm = -1;
    kaapi_writemem_barrier();
    kaapi_team_barrier_wait_signal( team, team->all_kprocessors[0] );
    kaapi_team_barrier_wait_term( team, team->all_kprocessors[0] );

    for (int i=0; i<team->count; ++i)
    {
      kaapi_processor_destroy(team->all_kprocessors[i]);
      kaapi_processor_free(team->all_kprocessors[i]);
    }

    /* */
    kaapi_barrier_destroy( &team->barrier);
    kaapi_atomic_destroylock(&team->lock );
    kaapi_atomic_destroylock(&team->ws.lock );

    KAAPI_DEBUG_INST(
      team->count          = 0;
    );

    if (team->save_master == 0)
    {
      free( team->all_kprocessors );
      KAAPI_DEBUG_INST(team->all_kprocessors = 0;)
      free( team );
    }
    else
    {
      kaapi_setcontext( team->save_master, team->save_master->context );
      kaapi_set_self_processor( team->save_master );
#if defined(KAAPI_USE_PERFCOUNTER)
      kaapi_processor_t* kproc = team->save_master;
      kaapi_tracelib_thread_switchstate(
        kproc->rsrc.perfkproc,
        kproc->rsrc.evtkproc,
        KAAPI_PERF_USR_STATE
      );
#endif
      if (team != kaapi_cached_global_team)
      {
        free( team->all_kprocessors );
        KAAPI_DEBUG_INST(team->all_kprocessors = 0;)
        free( team );
      }
    }
  }
  return 0;
}


/*
*/
int kaapi_team_lock (struct kaapi_team_t *team)
{
  if (team ==0) return 0;
  if (team->count ==0) return 0;
  return kaapi_atomic_lock( &team->ws.lock);
}

/*
*/
int kaapi_team_unlock (struct kaapi_team_t *team)
{
  if (team ==0) return 0;
  if (team->count ==0) return 0;
  return kaapi_atomic_unlock( &team->ws.lock);
}


/* Similar to omp single nowait.
*/
int kaapi_team_single ( kaapi_team_t* team, kaapi_processor_t* kproc )
{
  if (team ==0) return 1;
  if (kproc == 0) kproc = kaapi_self_processor();
  kaapi_context_t *ctxt = kproc->context;
  int oldvalue = ctxt->ws.count_single;
  ++ctxt->ws.count_single;
  int retval = KAAPI_ATOMIC_CAS(&team->ws.single, oldvalue, ctxt->ws.count_single);
  return retval;
}


/* Similar to omp master nowait.
*/
int kaapi_team_master ( kaapi_team_t* team, kaapi_processor_t* kproc )
{
  if (team ==0) return 1;
  if (kproc == 0) kproc = kaapi_self_processor();
  if (kproc->rsrc.kid == 0) return 1;
  return 0;
}


/*
*/
int kaapi_team_start(
  kaapi_team_t* team
)
{
  kaapi_processor_t* kproc0;
#if defined(KAAPI_USE_PERFCOUNTER)
  kproc0 = kaapi_self_processor();
  if (kproc0 !=0)
    kaapi_tracelib_thread_switchstate(
      kproc0->rsrc.perfkproc,
      kproc0->rsrc.evtkproc,
      KAAPI_PERF_SYS_STATE
    );
#endif
  KAAPI_ATOMIC_ADD(&kaapi_running_threads, team->count-1);

  if ((team == kaapi_cached_global_team) && (team->thread_started !=0))
  {
    kproc0 = team->all_kprocessors[0];
    /* reset kproc 0, the mast */
    kproc0->team         = team;
    kaapi_setcontext(kproc0, kproc0->context);
    kaapi_set_self_processor(kproc0);

    kaapi_team_barrier_wait_signal( team, kproc0 );
  }
  else
  {
    int alreadystarted = KAAPI_ATOMIC_READ(&team->started);
    if (0 != kaapi_team_creates_threads(
          team,
          &team->all_descriptors[0],
          &team->all_descriptors[team->count]
      ))
        return EINVAL;

    if (1)
    {
      while (alreadystarted+team->count != KAAPI_ATOMIC_READ(&team->started))
      {
        kaapi_slowdown_cpu();
        sched_yield();
      }
    }
    team->thread_started = 1;
  }

#if defined(KAAPI_USE_PERFCOUNTER)
  kproc0 = team->all_kprocessors[0];
  kaapi_tracelib_thread_switchstate(
    kproc0->rsrc.perfkproc,
    kproc0->rsrc.evtkproc,
    KAAPI_PERF_USR_STATE
  );
#endif

  return 0;
}


typedef struct {
  kaapi_team_t* team;
  kaapi_procinfo_t* kpi_start;
  kaapi_procinfo_t* kpi_end;
} kaapi_thread_create_range_t;

/*
*/
static void* helper_kaapi_team_creates_threads( void* arg )
{
  kaapi_thread_create_range_t* range = (kaapi_thread_create_range_t*)arg;
  kaapi_team_creates_threads( range->team, range->kpi_start, range->kpi_end );
  free(range);
  return 0;
}

/*
*/
static int kaapi_team_creates_threads(
    kaapi_team_t* team,
    kaapi_procinfo_t* kpi_start,
    kaapi_procinfo_t* kpi_end   /* past the last */
)
{
  size_t count = kpi_end - kpi_start;
  if (count <= 96)
  {
    int err;
    for (; kpi_start < kpi_end; ++kpi_start)
    {
      if (0 != (err = kaapi_team_addthread(team, kpi_start)))
        return err;
    }
  }
  else
  {
    pthread_t tid;
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate( &attr, PTHREAD_CREATE_DETACHED);

    kaapi_thread_create_range_t* arg = malloc(sizeof(kaapi_thread_create_range_t));
    arg->team = team;
    arg->kpi_start = kpi_start+count/2;
    arg->kpi_end = kpi_end;
    kpi_end = arg->kpi_start;

    if (0 != pthread_create(&tid, &attr, helper_kaapi_team_creates_threads, arg))
      return EAGAIN;
    kaapi_team_creates_threads( team, kpi_start, kpi_end );
  }

  return 0;
}


/* Creates thread on the specified cpuset.
   Allocate the stack (C stack) of the thread on the right numa node.
*/
static int _kaapi_team_addthread( kaapi_procinfo_t* kpi )
{
  pthread_t tid;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate( &attr, PTHREAD_CREATE_DETACHED);
  size_t stacksize;
  void* stackaddr;

  /* reschedule the thread on the local cpu */
  sched_yield();

  if (kaapi_default_param.threadstacksize !=0)
  {
    stacksize = kaapi_default_param.threadstacksize;
    /* roundup to pagesize */
    stacksize = (stacksize + getpagesize()-1) & ~(getpagesize()-1);
    //printf("stack size:%lu\n", stacksize);
    stackaddr = mmap(0, stacksize,PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, (off_t)0);
    kaapi_assert(stackaddr != (void*)-1);
#if defined(__linux__) && defined(MADV_HUGEPAGE)
    kaapi_assert(!madvise(stackaddr, stacksize, MADV_WILLNEED | MADV_UNMERGEABLE | MADV_HUGEPAGE));
#endif
  }
  else
  {
    kaapi_assert( 0 == pthread_attr_getstacksize( &attr, &stacksize));
    stackaddr = 0;
  }

#if defined(KAAPI_USE_SCHED_AFFINITY) 
  if (kpi->place_part.len >0)
  {
    size_t cnt = 0;
    int index = kpi->place_part.first + kpi->place_off-kaapi_default_param.places.first;
    kaapi_cpuset_t* place = &kaapi_default_param.places.first[ index % kaapi_default_param.places.len ];
    kaapi_assert_debug( place < (kaapi_default_param.places.first+kaapi_default_param.places.len) );
    if ((cnt = KAAPI_CPUSET_COUNT(place)) != 0)
    {
      kaapi_assert((!pthread_attr_setaffinity_np(&attr, sizeof(kaapi_cpuset_t), (const cpu_set_t*)place)));
#if 0
      char buffer[1024];
      printf("[thread] %i on cpuset: '%s'\n", kpi->proc_index,
        kaapi_cpuset2string_r( buffer, 1024, 64, place )
      );
#endif

      int r = 1+(int)rand() % cnt;
      int cpu = 0;
      for (cpu=0; cpu<sizeof(kaapi_cpuset_t)*8; ++cpu)
        if (KAAPI_CPUSET_ISSET(cpu, place))
          if (--r ==0)
            break;
      kaapi_assert_debug( cpu != 8*sizeof(kaapi_cpuset_t) );
#if defined(KAAPI_USE_NUMA)
      if (stackaddr !=0)
      {
        int node = numa_node_of_cpu(cpu);
        int maxnode = numa_num_configured_cpus();
        unsigned long nodemask
          [(maxnode + (8 * sizeof(unsigned long) -1))/ (8 * sizeof(unsigned long))];
        memset(nodemask, 0, sizeof(nodemask));
        nodemask[node / (8 * sizeof(unsigned long))] |= 1UL << (node % (8 * sizeof(unsigned long)));
        kaapi_assert (mbind((void*)stackaddr, stacksize, MPOL_BIND, nodemask, maxnode, MPOL_MF_STRICT | MPOL_MF_MOVE) == 0);
      }
#endif
    }
  }
#endif /* KAAPI_USE_SCHED_AFFINITY */

  if (stackaddr !=0)
#if defined(__linux__)
    kaapi_assert( !pthread_attr_setstack(&attr, stackaddr, stacksize));
#else
  /* on mac os x, if allocation throught mmap, it seems that non deterministic bug appears */
  {
    int err;
    kaapi_assert( !(err = pthread_attr_setstacksize(&attr, stacksize)));
    kaapi_assert( !(err = pthread_attr_setstackaddr(&attr, stackaddr)));
  }
#endif
  if (0 == pthread_create(&tid, &attr,kaapi_default_sched_run_processor, kpi))
  {
    pthread_attr_destroy(&attr);
    return 0;
  }
  return EFAULT;
}


/*
*/
static int kaapi_team_addthread( kaapi_team_t* team, kaapi_procinfo_t* kpi )
{
  kaapi_processor_id_t kid = kpi->proc_index;
  kpi->team = team;

  if (kid >= (uint64_t)team->count)
    return EINVAL;

  if (kid !=0)
  {
    kaapi_assert(0 == _kaapi_team_addthread(kpi) );
  }
  else /* if (kid == 0) */
  {
    kaapi_processor_t* kproc;

#if defined(KAAPI_USE_SCHED_AFFINITY)
    if (kpi->place_part.len >0)
    {
      int index = kpi->place_part.first + kpi->place_off-kaapi_default_param.places.first;
      kaapi_cpuset_t* place = &kaapi_default_param.places.first[ index % kaapi_default_param.places.len ];
      kaapi_assert_debug( place < (kaapi_default_param.places.first+kaapi_default_param.places.len) );
      if (KAAPI_CPUSET_COUNT(place) != 0)
      {
        kaapi_assert(!pthread_setaffinity_np(pthread_self(), sizeof(kaapi_cpuset_t), (const cpu_set_t*)place));
#if 0
        char buffer[1024];
        printf("[thread] %i on cpuset: '%s'\n", kid,
          kaapi_cpuset2string_r( buffer, 1024, 64, place )
        );
#endif
      }
    }
#endif /* KAAPI_USE_SCHED_AFFINITY */

    kproc = kaapi_processor_allocate();
    if (kproc == 0)
      return ENOMEM;

    kaapi_assert(0 == kaapi_processor_init( kproc, team, kpi ) );
    kaapi_set_self_processor(kproc);
#if defined(KAAPI_USE_PERFCOUNTER)
    kaapi_tracelib_thread_start( kproc->rsrc.perfkproc, kproc->rsrc.evtkproc );
#endif

    team->all_kprocessors[0] = kproc;
    KAAPI_ATOMIC_INCR(&team->started);
  }

  return 0;
}


/* Create orphan kproc with no team
*/
kaapi_processor_t* kaapi_create_orphan_kprocessor(void)
#if defined(KAAPI_USE_PERFCOUNTER)
#endif
#if 1
{
  /* Create initial team ! */
  kaapi_team_t* team = kaapi_team_init(
      0,
      "main",
      1,
      KAAPI_PROCBIND_MASTER,
      (void (*)(void*))1,
      0,
      0,
      &kaapi_default_param.places, /* places */
      KAAPI_TEAM_NOCACHE
  );

  if (kaapi_team_start(team) !=0) return 0;
  return kaapi_self_processor();
}
#endif
#if 0
{
  kaapi_processor_t* kproc = kaapi_processor_allocate();
  if (kproc ==0) return 0;
  kaapi_procinfo_t kpi = {
    .place_part = kaapi_default_param.places,
    .place_off = 0,
    .proc_type = KAAPI_PROC_TYPE_CPU,
    .proc_index = 0
  };

  kaapi_assert(0 == kaapi_processor_init(kproc, 0, &kpi));
  kaapi_set_self_processor(kproc);
#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_tracelib_thread_start( kproc->rsrc.perfkproc, kproc->rsrc.evtkproc );
#endif
  return kproc;
}
#endif


/**
*/
void* kaapi_default_sched_run_processor( void* arg )
{
  kaapi_procinfo_t*  kpi = (kaapi_procinfo_t*)arg;
  kaapi_processor_t* kproc = 0;
  kaapi_processor_id_t kid;
  kaapi_team_t* team = kpi->team;
  kaapi_team_t* oldteam = 0;

  /* reschedule the thread on the local cpu */
  sched_yield();

#if defined(KAAPI_USE_SCHED_AFFINITY)
  if (kpi->place_part.len >0)
  {
    int index = kpi->place_part.first + kpi->place_off-kaapi_default_param.places.first;
    kaapi_cpuset_t* place = &kaapi_default_param.places.first[ index % kaapi_default_param.places.len ];
    kaapi_assert_debug( place < (kaapi_default_param.places.first+kaapi_default_param.places.len) );
    if (KAAPI_CPUSET_COUNT(place) != 0)
      sched_setaffinity( 0, sizeof(kaapi_cpuset_t), (const cpu_set_t*)place);
  }
#endif /* KAAPI_USE_SCHED_AFFINITY */

  /* processor initialization */
  kid = kpi->proc_index;
  team = kpi->team;
  kproc = kaapi_processor_allocate();
  kaapi_assert( 0 == kaapi_processor_init( kproc, team, kpi ));
  kaapi_assert( kproc->context != 0 );
  kaapi_set_self_processor(kproc);
#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_assert_debug(
      (kproc->rsrc.perfkproc==0) ||
      (KAAPI_GET_THREAD_STATE(kproc->rsrc.perfkproc) == KAAPI_PERF_SYS_STATE));
  kaapi_tracelib_thread_start(kproc->rsrc.perfkproc, kproc->rsrc.evtkproc);
#endif

  KAAPI_ATOMIC_INCR(&team->started);

  /* wait all threads have finish their initialization */
  while (team->thread_started ==0)
    kaapi_slowdown_cpu();

#if 0 //defined(KAAPI_USE_PERFCOUNTER)
int counter_loop = 0;
#endif

  /* infinite loop 
  */
  do {
    //sched_yield();
#if defined(KAAPI_USE_PERFCOUNTER)
  kaapi_assert_debug(
      (kproc->rsrc.perfkproc==0) ||
      (KAAPI_GET_THREAD_STATE(kproc->rsrc.perfkproc) == KAAPI_PERF_SYS_STATE));
#endif

    if (oldteam != team)
    {
      team->all_kprocessors[kid] = kproc;
      if (oldteam !=0) {
        _kaapi_processor_register_hierarchy(kproc);
#if defined(KAAPI_USE_PERFCOUNTER)
        kproc->rsrc.perfkproc = kaapi_tracelib_thread_init(
            &kproc->rsrc.evtkproc,
            team->perf,
            KAAPI_PROC_TYPE_CPU,
            kproc->rsrc.kid,
            kproc->rsrc.numaid,
            KAAPI_PERF_SYS_STATE);
#endif
      }
    }
    kaapi_assert( kproc->context != 0 );
    kaapi_setcontext(kproc, kproc->context);
    kaapi_set_self_processor(kproc);

    /* worker code. Sync is done in team_barier_wait */
    if (kpi->body_runprocessor)
      kpi->body_runprocessor( kpi->arg_runprocessor );

#if defined(KAAPI_USE_PERFCOUNTER)
    /* new barrier at the end of parallel region to schedule tasks and returns */
    kaapi_team_barrier_wait( team, kproc, KAAPI_BARRIER_FLAG_DEFAULT );

    /*  */
    kaapi_tracelib_thread_stop(kproc->rsrc.perfkproc, kproc->rsrc.evtkproc);
#endif

    /* implicit barrier at the end of parallel region */
    kaapi_team_barrier_wait( team, kproc, KAAPI_BARRIER_FLAG_WAITEXIT );

    if (team->isterm == -1)
      break;

#if defined(KAAPI_USE_PERFCOUNTER)
    kaapi_tracelib_thread_start(kproc->rsrc.perfkproc, kproc->rsrc.evtkproc);
    kaapi_assert_debug(
        (kproc->rsrc.perfkproc==0) ||
        (KAAPI_GET_THREAD_STATE(kproc->rsrc.perfkproc) == KAAPI_PERF_SYS_STATE));
#endif

    kaapi_context_clear( kproc->context );

    oldteam = team;
    kid  = kpi->proc_index;
    team = kpi->team;
    kaapi_assert_debug(kproc->team == team); /* may have changed ? */
  } while (1);

  kaapi_team_barrier_wait_signal_term(team, kproc );

  kaapi_assert_debug( kaapi_self_processor() == kproc );

  return 0;
}


#if defined(KAAPI_USE_SCHED_AFFINITY)
static int get_cpuid(kaapi_processor_t* kproc)
{
  int cpu = -1; //kproc->cpuid;
#if defined(__MIC__) || defined(__linux__)
  if (cpu == -1)
    cpu = sched_getcpu();
#elif defined(__APPLE__)
  cpu = (int)kproc->kid;
#else
#error "Undefined OS"
#endif
  return cpu;
}


#if 0//UNUSED TODO
static int get_physicalproc(kaapi_processor_t* kproc)
{
  int processor;
  int cpu = get_cpuid(kproc);
#if defined(__MIC__)
  processor = cpu -1;
  if (processor <0) processor += kaapi_default_param.syscpucount;
  processor /= 4; /* for series 5100, 3100, 7100 */
#elif defined(__linux__)
  char filename[256];
  FILE* file;
  int err;
  sprintf(filename, "/sys/devices/system/cpu/cpu%i/topology/core_id", cpu);
  file = fopen(filename, "rt");
  if (file ==0)
    err = -EINVAL;
  else
    err = fscanf(file,"%d",
                 &processor
    );

  /* test if processor has been read */
  if (err < 1)
    return -1;
#elif defined(__APPLE__)
  processor = cpu;
#else
#error "Undefined OS"
#endif
  return processor;
}
#endif


/*
*/
int _kaapi_processor_getcpuid(
    kaapi_processor_t*   kproc
)
{
  exit(1);
  return -1;
}
#endif // KAAPI_USE_SCHED_AFFINITY

#if 0
/*
*/
int kaapi_processor_getpu( kaapi_team_t* team, kaapi_processor_t* kproc,  int n, int* kids )
{
  if (n<0) return -1;
#if defined(__MIC__)
  int* kids0 = kids;
  int cpu = get_cpuid(kproc);
  int processor = cpu -1;
  if (processor <0) processor += kaapi_default_param.syscpucount;
  processor /= 4; /* for series 5100, 3100, 7100 */
  if (n >4) n = 4; /* 4 Pus per core */
  for (int i=0; i<n; ++i)
  {
    int cpuid = processor*4+1+i;   /** with respect to the team-> core/pu mapping */
    if (cpuid >= kaapi_default_param.syscpucount)
      cpuid -= kaapi_default_param.syscpucount;
    if (*kids != -1) ++kids;
  }
  return kids-kids0;
#else
  return -1;
#endif
}
#endif

