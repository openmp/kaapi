/*
** kaapi_init.c
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com 
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"
#include "kaapi_util.h"

#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#if defined(KAAPI_DEBUG)
#  include <sys/time.h>
#  include <signal.h>
#endif

#if defined(KAAPI_USE_PERFCOUNTER)
#include <stdio.h>
#include <signal.h>
#endif
#if defined(__linux__)
#include <unistd.h>
#endif
#if defined(__APPLE__)
#include <sys/sysctl.h>
#endif

#if defined(KAAPI_DEBUG)
#include <float.h>
double kaapi_debug_threshold_task_duration = DBL_MAX;
#endif


/** The initial team
*/
kaapi_team_t* kaapi_global_team = 0;

extern kaapi_team_t* kaapi_cached_global_team;

/** The default parameters for Kaapi
*/
kaapi_rtparam_t kaapi_default_param = {
#if defined(KAAPI_USE_CUPTI)
   .cudastartuptime    = 0,
#endif
   .stackblocsize      = KAAPI_STACKBLOCSIZE, /* the kaapi STACK for tasks and args */
   .threadstacksize    = 0,  /* the pthread stack size */
   .syscpucount        = 0,
   .cpucount           = 0,
   .cpuset_str         = 0,
   .cpucount_str       = 0,
   .gpucount           = 0,
   .alarmperiod        = 0,

   /* selection of the victim processor */
#if defined(KAAPI_USE_NUMA)
   .wspush             = &kaapi_sched_select_queue_Wnuma,
   .wspush_init        = &kaapi_sched_push_init_queue_cyclicnumastrict,
   .wspush_init_distrib= &kaapi_sched_push_init_queue_cyclicnumastrict,
   .wsselect           = &kaapi_sched_select_victim_global,
#else
   .wspush             = &kaapi_sched_select_queue_local,
   .wspush_init        = &kaapi_sched_push_init_queue_wspush,
   .wspush_init_distrib= &kaapi_sched_push_init_queue_wspush,
   .wsselect           = &kaapi_sched_select_victim_rand,
#endif

   /* init in priority CC_SYNC, bitmap and no aggr */
   .emitsteal          = kaapi_sched_emitsteal,
   .emitsteal_initctxt = kaapi_sched_ccsync_emitsteal_init,
   .emitsteal_dstorctxt= kaapi_sched_ccsync_emitsteal_dstor,
   .request_post_fnc   = kaapi_sched_ccsync_post_request,
   .request_commit_fnc = kaapi_sched_ccsync_commit_request,
   .request_commit_fnc_async = kaapi_sched_ccsync_commit_request_async,
   .request_reply_fnc  = kaapi_request_ccsync_reply,
   .lr_empty_fnc       = kaapi_listrequest_ccsync_iterator_empty,
   .lr_get_fnc         = kaapi_listrequest_ccsync_iterator_get,
   .lr_next_fnc        = kaapi_listrequest_ccsync_iterator_next,
   .lr_count_fnc       = kaapi_listrequest_ccsync_iterator_count,

   .pgo_init           = kaapi_sched_ccsync_pgo_init,
   .pgo_wait           = kaapi_sched_ccsync_pgo_wait,
   .pgo_fini           = kaapi_sched_ccsync_pgo_fini,

#if defined(KAAPI_USE_PERFMODEL)
   .perfmodel           = 0,
   .perfmodel_calibrate = 0,
   .noremotesteal       = 0,
#endif
   .use_affinity        = 0,
   .affinity_set        = {{0,0},  /* threads */
                           {0,0},  /* cores */
                           {0,0},  /* numas */
                           {0,0},  /* sockets */
                           {0,0},  /* board */
                           {0,0}}, /* machine */
   .places_set          = {{0,0},  /* threads */
                           {0,0},  /* cores */
                           {0,0},  /* numas */
                           {0,0},  /* sockets */
                           {0,0},  /* board */
                           {0,0}}, /* machine */
   .map2affset          = 0,
   .procbind            = KAAPI_PROCBIND_DEFAULT,
   .places              = {0,0},
   /*FIXME investigate if this is an actual duplicate, as it's only
    * referenced in kaapi_dfgbuild and it mainly concerns "date" in critical path */
   .ctpriority          = 0,
   .use_priority        = 0,
   .strict_push         = 0,
   .block_cyclic        = 1,
   .enable_push_local   = 0,

   .display_env         = 0,
   .dump_graph          = 0,
};


/* == 0 if lib is not initialized
*/
static kaapi_atomic_t kaapi_count_init = {0};

/*
*/
#if defined(KAAPI_HAVE_COMPILER_TLS_SUPPORT)
__thread kaapi_thread_t*        kaapi_current_thread_key;
__thread kaapi_processor_t*     kaapi_current_processor_key;
__thread kaapi_ressource_t*     kaapi_current_processor_info_key;
#else
pthread_key_t kaapi_current_processor_key;
pthread_key_t kaapi_current_processor_info_key;

/**
*/
kaapi_thread_t* kaapi_self_thread(void)
{
  kaapi_processor_t* kproc = kaapi_self_processor();
  if (kproc ==0) return 0;
  return &kproc->context->stack.thread;
}
#endif

/**
*/
unsigned int kaapi_self_kid(void)
{
  kaapi_processor_t* kproc = kaapi_self_processor();
  if (kproc ==0) return -1;
  return (unsigned int)kproc->rsrc.kid;
}

/**
*/
kaapi_team_t* kaapi_self_team(void)
{
  kaapi_processor_t* kproc = kaapi_self_processor();
  if (kproc ==0) return kaapi_global_team;
  return kproc->team;
}


/**
*/
int kaapi_get_concurrency(void )
{
  kaapi_processor_t* kproc = kaapi_self_processor();
  if (kproc ==0) 
    return 1; // kaapi not initialized
  if (kproc->team ==0)
    return 1; // kproc not in a team
  return kproc->team->count;
}

/**
*/
#if !defined(KAAPI_USE_OFFLOAD)
extern void* kaapi_get_cublas_handle(void);
void* kaapi_get_cublas_handle(void)
{
  return 0;
}
#endif


/**
*/
int kaapi_mt_registerproc(void)
{
  int err = kaapi_mt_hwdetect();
  if ((err !=0) && (err != EALREADY))  return err;

  /* WARNING: here should allocate at lest cpucount+gpucount descriptor
   Currently allocate maximal number of Kprocessor */
  int use_cpu = 0;
  kaapi_default_param.cpucount = 0;

  /* KAAPI_CPUCOUNT */
  if (kaapi_default_param.cpucount_str != 0)
    use_cpu = atoi(kaapi_default_param.cpucount_str);

  /* KAAPI_CPUSET */
  if (kaapi_default_param.cpuset_str != 0)
  {
    unsigned int count = 0;
    unsigned int cpu_count = 0;
    kaapi_cpuset_t* places = 0;
    char* toparse = strdup(kaapi_default_param.cpuset_str);
    char* save = toparse;
    bool err = kaapi_parse_places( &toparse, &count, &cpu_count, &places );
    kaapi_assert(err);
    free(save);

#if 0 //defined(KAAPI_USE_SCHED_AFFINITY)
char buffer[1024];
char buffer2[1024];
    printf("Places: #count=%i, #cpus=%i\n", count, cpu_count );
    for (int i=0; i<count; ++i)
    {
      printf("place='%s', cpuset='%s'\n", kaapi_unparse_places_r(buffer, 256, 1, &places[i]), kaapi_cpuset2string_r(buffer2, 256, 48, &places[i]));
    }
#endif

    /* ok : use affinity */
    kaapi_default_param.use_affinity = 1;
    kaapi_default_param.places.first = places;
    kaapi_default_param.places.len = count;
    if (use_cpu ==0) use_cpu = cpu_count;
  }
  else
    if (use_cpu ==0) use_cpu = kaapi_default_param.syscpucount;

  kaapi_default_param.cpucount = use_cpu;

  //printf("[kaapi] use %i cpus\n", use_cpu );

  return 0;
}


/*
*/
static void _kaapi_init_reversemap(
    kaapi_hws_levelid_t level,
    kaapi_map2affinityset_t* map2affset,
    const kaapi_cpuset_t* place_i)
{
  size_t cpusize = KAAPI_CPUSET_SETSIZE;
  size_t pagesize = getpagesize();

  int count = kaapi_default_param.places_set[level].count;
  kaapi_assert( 0==posix_memalign((void**)&kaapi_default_param.places_set[level].places[count],
                                  pagesize, sizeof(kaapi_plainplace_t)));

  kaapi_init_plainplace( (kaapi_plainplace_t*)kaapi_default_param.places_set[level].places[count] );

  for (size_t i=0; i<cpusize; ++i)
  {
    if (KAAPI_CPUSET_ISSET(i,place_i))
    {
      if (map2affset[i].affset[level] == 0)
      {
        map2affset[i].affset[level] = kaapi_default_param.places_set[level].places[count];
      }
    }
  }
  kaapi_default_param.places_set[level].count = count+1;
}


/**
*/
static kaapi_atomic_t is_hwdetect_called = {0};
int kaapi_mt_hwdetect(void)
{
  if (KAAPI_ATOMIC_INCR(&is_hwdetect_called) >1)
    return EALREADY;

  /* parse for displaying info */
  if (kaapi_default_param.display_env ==0)
  {
    char *env = getenv ("KAAPI_DISPLAY_ENV");
    if (env != 0)
      if (!kaapi_parse_display_env(&env, &kaapi_default_param.display_env) || (*env != 0))
        return EINVAL;
  }

  const char* volatile version __attribute__((unused));
  version = get_kaapi_version();

  /* compute the number of cpu of the system */
#if defined(__linux__)
  kaapi_default_param.syscpucount = sysconf(_SC_NPROCESSORS_CONF);
#elif defined(__APPLE__)
  {
    kaapi_default_param.syscpucount = 0;
    size_t len;
    len = sizeof(kaapi_default_param.syscpucount);
    sysctlbyname("hw.physicalcpu", &kaapi_default_param.syscpucount, &len, 0, 0 );
  }
#else
#  warning "Could not compute number of physical cpu of the system. Default value==1"
  kaapi_default_param.syscpucount = 1;
#endif

  kaapi_default_param.use_affinity = 0;
  kaapi_affinityset_t* affset = &kaapi_default_param.affinity_set[0];
  kaapi_placeset_t* placeset = &kaapi_default_param.places_set[0];
  kaapi_default_param.map2affset =
    (kaapi_map2affinityset_t*)calloc( kaapi_default_param.syscpucount, sizeof(kaapi_map2affinityset_t) );

  for (int i=0; i<KAAPI_HWS_LEVELID_MAX; ++i)
  {
    affset[i].count   = 0;
    affset[i].cpuset  = 0;
    placeset[i].count = 0;
    placeset[i].places= 0;
  }

  /* Initialize the MACHINE affinityset */
  affset[KAAPI_HWS_LEVELID_MACHINE].count = 1;
  affset[KAAPI_HWS_LEVELID_MACHINE].cpuset = calloc( 1, sizeof(kaapi_cpuset_t) );
  kaapi_assert(affset[KAAPI_HWS_LEVELID_MACHINE].cpuset !=0);
  KAAPI_CPUSET_ZERO(&affset[KAAPI_HWS_LEVELID_MACHINE].cpuset[0]);
  for (int k=0; k<kaapi_default_param.syscpucount; ++k)
    KAAPI_CPUSET_SET(k, &affset[KAAPI_HWS_LEVELID_MACHINE].cpuset[0]);

  /* init machine level place and related informations */
  placeset[KAAPI_HWS_LEVELID_MACHINE].count  = 0;
  placeset[KAAPI_HWS_LEVELID_MACHINE].places = calloc(1, sizeof(kaapi_place_t*));
  placeset[KAAPI_HWS_LEVELID_MACHINE].places[0] =calloc(1, sizeof(kaapi_place_t));
  kaapi_cpuset_t* place_i = &affset[KAAPI_HWS_LEVELID_MACHINE].cpuset[0];
  _kaapi_init_reversemap(KAAPI_HWS_LEVELID_MACHINE, kaapi_default_param.map2affset, place_i);

  /* detect the topology */
#if defined(__linux__)
  char filename[sizeof("/sys/devices/system/cpu/cpu1024/topology/thread_siblings_list") + 16];
  FILE *file;

#if !defined(__MIC__)
  /* 1/ parse numa node informations - MIC does not have it (KNC). */
  memcpy(filename,
      "/sys/devices/system/node/online",
      sizeof("/sys/devices/system/node/online"));
  file = fopen(filename,"r");
  if (file !=0)
  {
    char *line = 0;
    char *str = 0;
    size_t len = 0;
    kaapi_assert( getline (&line, &len, file) > 0);
    str = line;

    /* scan num '-' num */
    unsigned int start, end;
    kaapi_assert(kaapi_parse_range(&str, &start, &end, 0));
    fclose( file );
    free(line);

    /* [OFFLOAD TG]extend count by the number of device memory view as the NUMA domain */
    affset[KAAPI_HWS_LEVELID_NUMA].count  = 1+end-start + kaapi_offload_get_num_devices();
    affset[KAAPI_HWS_LEVELID_NUMA].cpuset = calloc( (1+end-start), sizeof(kaapi_cpuset_t) );
    kaapi_assert(affset[KAAPI_HWS_LEVELID_NUMA].cpuset !=0);

    placeset[KAAPI_HWS_LEVELID_NUMA].count  = 0; /* incr in _kaapi_allocplace_reversemap */
    placeset[KAAPI_HWS_LEVELID_NUMA].places = calloc(affset[KAAPI_HWS_LEVELID_NUMA].count, sizeof(kaapi_place_t*));
    kaapi_assert(placeset[KAAPI_HWS_LEVELID_NUMA].places !=0);

    for (int i=0; i<affset[KAAPI_HWS_LEVELID_NUMA].count; ++i)
    {
      sprintf(filename, "/sys/devices/system/node/node%i/cpulist", i);
      file = fopen(filename,"r");
      kaapi_assert( file != 0 );
      line = 0;
      kaapi_assert( getline (&line, &len, file) > 0);
      str = line;
      fclose(file);

      kaapi_cpuset_t* place_i = &affset[KAAPI_HWS_LEVELID_NUMA].cpuset[i];
      KAAPI_CPUSET_ZERO(place_i);

      unsigned int cpu;
      kaapi_assert( kaapi_parse_oneplace(&str, &cpu, place_i) );
      _kaapi_init_reversemap(KAAPI_HWS_LEVELID_NUMA, kaapi_default_param.map2affset, place_i);
      free(line);
    }
  }
  else
    printf("[kaapi] Cannot access to topology information\n");
#else
  /* mic has one numa node with all cores inside */
  unsigned int start, end;
  start=0;
  end=1;
  /* [OFFLOAD TG]extend count by the number of device memory view as the NUMA domain */
  affset[KAAPI_HWS_LEVELID_NUMA].count = 1+end-start + kaapi_offload_get_num_devices();
  affset[KAAPI_HWS_LEVELID_NUMA].cpuset= calloc( (1+end-start), sizeof(kaapi_cpuset_t) );
  kaapi_assert(affset[KAAPI_HWS_LEVELID_NUMA].cpuset !=0);

  kaapi_cpuset_t* place_i = &affset[KAAPI_HWS_LEVELID_NUMA].cpuset[0];
  char str[1024];
  char* sstr = &str[0]; 
  char** ssstr = &sstr;
  sprintf(sstr,"%i-%i", (int)0, (int)kaapi_default_param.syscpucount);

  KAAPI_CPUSET_ZERO(place_i);
  unsigned int cpu;
  kaapi_assert( kaapi_parse_oneplace(&sstr, &cpu, place_i) );
#endif


  /* 1/ parse thread/core/sockets informations 
     socket is assumed to be sibling threads of a threads
  */
  memcpy(filename,
      "/sys/devices/system/cpu/online",
      sizeof("/sys/devices/system/cpu/online"));
  file = fopen(filename,"r");
  if (file !=0)
  {
    char *line = 0;
    char *str = 0;
    size_t len = 0;
    kaapi_assert( getline (&line, &len, file) > 0);
    str = line;
    fclose( file );

    /* scan num '-' num */
    unsigned int start, end;
    kaapi_assert(kaapi_parse_range(&str, &start, &end, 0));
    free(line);

    kaapi_cpuset_t core_allcpu, socket_allcpu;
    KAAPI_CPUSET_ZERO(&core_allcpu);
    KAAPI_CPUSET_ZERO(&socket_allcpu);
    for (int i=start; i<=end; ++i)
    {
      KAAPI_CPUSET_SET(i, &core_allcpu);
      KAAPI_CPUSET_SET(i, &socket_allcpu);
    }

    /* init thread level : list of PU */
    affset[KAAPI_HWS_LEVELID_THREAD].count = 1+end-start;
    affset[KAAPI_HWS_LEVELID_THREAD].cpuset= calloc( (1+end-start), sizeof(kaapi_cpuset_t) );
    kaapi_assert(affset[KAAPI_HWS_LEVELID_THREAD].cpuset !=0);

    placeset[KAAPI_HWS_LEVELID_THREAD].count  = 0; /* incr in reversemap */
    placeset[KAAPI_HWS_LEVELID_THREAD].places = calloc(affset[KAAPI_HWS_LEVELID_THREAD].count, sizeof(kaapi_place_t*));
    kaapi_assert(placeset[KAAPI_HWS_LEVELID_THREAD].places !=0);

    for (int i=0; i<affset[KAAPI_HWS_LEVELID_THREAD].count; ++i)
    {
      kaapi_cpuset_t* place_i = &affset[KAAPI_HWS_LEVELID_THREAD].cpuset[i];
      KAAPI_CPUSET_SET(i, place_i);
      _kaapi_init_reversemap(KAAPI_HWS_LEVELID_THREAD, kaapi_default_param.map2affset, place_i);
    }

    /* Init core and socket levels */
    affset[KAAPI_HWS_LEVELID_CORE].count = 0;
    affset[KAAPI_HWS_LEVELID_SOCKET].count = 0;
    /* reserve up to end-start places then set correct size at the end */
    affset[KAAPI_HWS_LEVELID_CORE].cpuset= calloc( affset[KAAPI_HWS_LEVELID_THREAD].count, sizeof(kaapi_cpuset_t) );
    kaapi_assert(affset[KAAPI_HWS_LEVELID_CORE].cpuset !=0);
    affset[KAAPI_HWS_LEVELID_SOCKET].cpuset= calloc( affset[KAAPI_HWS_LEVELID_THREAD].count, sizeof(kaapi_cpuset_t) );
    kaapi_assert(affset[KAAPI_HWS_LEVELID_SOCKET].cpuset !=0);

    /* allocate upperbound */
    placeset[KAAPI_HWS_LEVELID_CORE].places = calloc(affset[KAAPI_HWS_LEVELID_THREAD].count, sizeof(kaapi_place_t*));
    kaapi_assert(placeset[KAAPI_HWS_LEVELID_CORE].places !=0);
    placeset[KAAPI_HWS_LEVELID_SOCKET].places = calloc(affset[KAAPI_HWS_LEVELID_THREAD].count, sizeof(kaapi_place_t*));
    kaapi_assert(placeset[KAAPI_HWS_LEVELID_SOCKET].places !=0);

//    memcpy( &core_allcpu, &socket_allcpu, KAAPI_CPUSET_SETSIZE);
    for (int i=0; i<affset[KAAPI_HWS_LEVELID_THREAD].count; ++i)
    {
      for (kaapi_hws_levelid_t hws =KAAPI_HWS_LEVELID_THREAD;
           hws != KAAPI_HWS_LEVELID_NUMA;
           ++hws
      )
      {
        kaapi_cpuset_t* allcpu = (hws == KAAPI_HWS_LEVELID_CORE ? &core_allcpu : &socket_allcpu);
        if (KAAPI_CPUSET_ISSET(i, allcpu))
        {
          sprintf(filename, "/sys/devices/system/cpu/cpu%i/topology/%s_siblings_list",
              i,
              (hws == KAAPI_HWS_LEVELID_CORE ? "thread" : "core" )
          );
          file = fopen(filename,"r");
          kaapi_assert( file != 0 );
          line = 0;
          kaapi_assert( getline (&line, &len, file) > 0);
          str = line;
          fclose( file );

          int alloc_place =0;
          unsigned int cpu;
          kaapi_cpuset_t place;
          kaapi_assert( kaapi_parse_oneplace(&str, &cpu, &place) );
          free(line);
          for (int k=0; k<KAAPI_CPUSET_SETSIZE; ++k)
          {
            if (KAAPI_CPUSET_ISSET(k, &place) && KAAPI_CPUSET_ISSET( k, allcpu ))
            {
              KAAPI_CPUSET_CLR(k, allcpu);
              if (k == i) alloc_place = 1;
            }
          }
          if (alloc_place)
          {
            kaapi_hws_levelid_t thws = (hws == KAAPI_HWS_LEVELID_CORE ? KAAPI_HWS_LEVELID_CORE : KAAPI_HWS_LEVELID_SOCKET);
            kaapi_cpuset_t* place_new = &affset[thws].cpuset[ affset[thws].count ];
            memcpy(place_new, &place, sizeof(kaapi_cpuset_t));
            ++affset[thws].count;
            _kaapi_init_reversemap(thws, kaapi_default_param.map2affset, place_new);
          }
        }
      }
    }

  }

  if (kaapi_default_param.display_env ==2)
  {
    printf("#places 'KAAPI_HWS_LEVELID_THREAD' : %i\n",affset[KAAPI_HWS_LEVELID_THREAD].count);

    printf("#places 'KAAPI_HWS_LEVELID_CORE'   : %i\n",affset[KAAPI_HWS_LEVELID_CORE].count);
    for (int i=0; i<affset[KAAPI_HWS_LEVELID_CORE].count; ++i)
      printf("[%i]: '%s'\n", i, kaapi_unparse_places(1,&affset[KAAPI_HWS_LEVELID_CORE].cpuset[i]));

    printf("#places 'KAAPI_HWS_LEVELID_NUMA'   : %i\n",affset[KAAPI_HWS_LEVELID_NUMA].count);
    for (int i=0; i<affset[KAAPI_HWS_LEVELID_NUMA].count; ++i)
      printf("[%i]: '%s'\n", i, kaapi_unparse_places(1,&affset[KAAPI_HWS_LEVELID_NUMA].cpuset[i]));

    printf("#places 'KAAPI_HWS_LEVELID_SOCKET' : %i\n",affset[KAAPI_HWS_LEVELID_SOCKET].count);
    for (int i=0; i<affset[KAAPI_HWS_LEVELID_SOCKET].count; ++i)
      printf("[%i]: '%s'\n", i, kaapi_unparse_places(1,&affset[KAAPI_HWS_LEVELID_SOCKET].cpuset[i]));

    printf("#places 'KAAPI_HWS_LEVELID_MACHINE': %i\n",affset[KAAPI_HWS_LEVELID_MACHINE].count);
    for (int i=0; i<affset[KAAPI_HWS_LEVELID_MACHINE].count; ++i)
      printf("[%i]: '%s'\n", i, kaapi_unparse_places(1,&affset[KAAPI_HWS_LEVELID_MACHINE].cpuset[i]));
  }

#else // not if defined(__linux__)
  unsigned int syscpucount = kaapi_default_param.syscpucount;

  /* Assume one NUMA node with one socket and syscpucount core and threads */
  affset[KAAPI_HWS_LEVELID_NUMA].count = 1 + kaapi_offload_get_num_devices();
  affset[KAAPI_HWS_LEVELID_NUMA].cpuset =
      calloc( affset[KAAPI_HWS_LEVELID_NUMA].count, sizeof(kaapi_cpuset_t) );
  kaapi_assert(affset[KAAPI_HWS_LEVELID_NUMA].cpuset !=0);

  placeset[KAAPI_HWS_LEVELID_NUMA].count  = 0; /* incr in _kaapi_allocplace_reversemap */
  placeset[KAAPI_HWS_LEVELID_NUMA].places = calloc(affset[KAAPI_HWS_LEVELID_NUMA].count, sizeof(kaapi_place_t*));
  kaapi_assert(placeset[KAAPI_HWS_LEVELID_NUMA].places !=0);

  KAAPI_CPUSET_ZERO(&affset[KAAPI_HWS_LEVELID_NUMA].cpuset[0]);
  for (int k=0; k<kaapi_default_param.syscpucount; ++k)
    KAAPI_CPUSET_SET(k, &affset[KAAPI_HWS_LEVELID_NUMA].cpuset[0]);

  _kaapi_init_reversemap( KAAPI_HWS_LEVELID_NUMA,
                          kaapi_default_param.map2affset,
                          &affset[KAAPI_HWS_LEVELID_NUMA].cpuset[0]);


  affset[KAAPI_HWS_LEVELID_SOCKET].count = 1;
  affset[KAAPI_HWS_LEVELID_SOCKET].cpuset = calloc( 1, sizeof(kaapi_cpuset_t) );
  kaapi_assert(affset[KAAPI_HWS_LEVELID_SOCKET].cpuset !=0);

  placeset[KAAPI_HWS_LEVELID_SOCKET].count  = 0; /* incr in _kaapi_allocplace_reversemap */
  placeset[KAAPI_HWS_LEVELID_SOCKET].places = calloc(affset[KAAPI_HWS_LEVELID_SOCKET].count, sizeof(kaapi_place_t*));
  kaapi_assert(placeset[KAAPI_HWS_LEVELID_SOCKET].places !=0);

  KAAPI_CPUSET_ZERO(&affset[KAAPI_HWS_LEVELID_SOCKET].cpuset[0]);
  for (int k=0; k<kaapi_default_param.syscpucount; ++k)
    KAAPI_CPUSET_SET(k, &affset[KAAPI_HWS_LEVELID_SOCKET].cpuset[0]);
  _kaapi_init_reversemap( KAAPI_HWS_LEVELID_SOCKET,
                          kaapi_default_param.map2affset,
                          &affset[KAAPI_HWS_LEVELID_NUMA].cpuset[0]);

  /* init thread level : list of PU */
  affset[KAAPI_HWS_LEVELID_THREAD].count = syscpucount;
  affset[KAAPI_HWS_LEVELID_THREAD].cpuset = calloc( syscpucount, sizeof(kaapi_cpuset_t) );
  kaapi_assert(affset[KAAPI_HWS_LEVELID_THREAD].cpuset !=0);

  placeset[KAAPI_HWS_LEVELID_THREAD].count  = 0; /* incr in _kaapi_allocplace_reversemap */
  placeset[KAAPI_HWS_LEVELID_THREAD].places = calloc(affset[KAAPI_HWS_LEVELID_THREAD].count, sizeof(kaapi_place_t*));
  kaapi_assert(placeset[KAAPI_HWS_LEVELID_THREAD].places !=0);

  for (int i=0; i<affset[KAAPI_HWS_LEVELID_THREAD].count; ++i)
  {
    KAAPI_CPUSET_ZERO(&affset[KAAPI_HWS_LEVELID_THREAD].cpuset[i]);
    KAAPI_CPUSET_SET(i, &affset[KAAPI_HWS_LEVELID_THREAD].cpuset[i]);
    _kaapi_init_reversemap( KAAPI_HWS_LEVELID_THREAD,
                            kaapi_default_param.map2affset,
                            &affset[KAAPI_HWS_LEVELID_NUMA].cpuset[i]);
  }

  /* init core level : list of PU */
  affset[KAAPI_HWS_LEVELID_CORE].count = syscpucount;
  affset[KAAPI_HWS_LEVELID_CORE].cpuset = calloc( syscpucount, sizeof(kaapi_cpuset_t) );
  kaapi_assert(affset[KAAPI_HWS_LEVELID_CORE].cpuset !=0);
  placeset[KAAPI_HWS_LEVELID_CORE].count  = 0; /* incr in _kaapi_allocplace_reversemap */
  placeset[KAAPI_HWS_LEVELID_CORE].places = calloc(affset[KAAPI_HWS_LEVELID_CORE].count, sizeof(kaapi_place_t*));
  kaapi_assert(placeset[KAAPI_HWS_LEVELID_CORE].places !=0);

  for (int i=0; i<affset[KAAPI_HWS_LEVELID_CORE].count; ++i)
  {
    KAAPI_CPUSET_ZERO(&affset[KAAPI_HWS_LEVELID_CORE].cpuset[i]);
    KAAPI_CPUSET_SET(i, &affset[KAAPI_HWS_LEVELID_CORE].cpuset[i]);
    _kaapi_init_reversemap( KAAPI_HWS_LEVELID_CORE,
                            kaapi_default_param.map2affset,
                            &affset[KAAPI_HWS_LEVELID_NUMA].cpuset[i]);
  }
#endif

  return 0;
}


/**
*/
int kaapi_mt_init(int flag)
{
  const char* volatile version __attribute__((unused));
  int                  err;
  const char*          str;

  if (KAAPI_ATOMIC_INCR(&kaapi_count_init) >1)
    return EALREADY;

  /* set param cpucount, places */
  err = kaapi_mt_registerproc();
  if (err !=0) return err;

  /* set flag to dump graph */
  if (getenv("KAAPI_DUMP_GRAPH"))
    kaapi_default_param.dump_graph = 1;

  str = getenv("KAAPI_USE_TASKPRIORITY");
  if (str != 0)
  {
    if (!strcasecmp(str, "1") || !strcasecmp(str, "true"))
      kaapi_default_param.use_priority = 1;
    else if (!strcasecmp(str, "0") || !strcasecmp(str, "false"))
      kaapi_default_param.use_priority = 0;
    else {
      fprintf(stderr, "***Kaapi: bad value for variable KAAPI_USE_TASKPRIORITY: '%s'\n", str);
      return EINVAL;
    }
  }

  str = getenv("KAAPI_BLOCK_CYCLIC");
  if (str !=0)
  {
    kaapi_default_param.block_cyclic = atol(str);
    if (kaapi_default_param.block_cyclic <=0) kaapi_default_param.block_cyclic = 1;
    printf("[kaapi] blocking factor: %li\n", kaapi_default_param.block_cyclic );
  }


  /* code to move again to kaapi_init */
  str = getenv("KAAPI_WSSELECT");
  if (str !=0)
  {
    int i;
    int found=0;
    for (i=0; ; ++i)
    {
      if (kaapi_default_selectvictim[i].entrypoint ==0) break; /* end of list */
      if (strcasecmp(str, kaapi_default_selectvictim[i].name) ==0)
      {
        kaapi_default_param.wsselect = kaapi_default_selectvictim[i].entrypoint;
        found = 1;
        break;
      }
    }
    if (!found)
    {
      fprintf(stderr, "***Kaapi: bad value for variable KAAPI_WSSELECT: '%s'\n", str);
      return EINVAL;
    }
  }
  str = getenv("KAAPI_WSPUSH");
  if (str !=0)
  {
    int i;
    int found=0;
    for (i=0; ; ++i)
    {
      if (kaapi_default_push[i].entrypoint ==0) break; /* end of list */
      if (strcasecmp(str, kaapi_default_push[i].name) ==0)
      {
        kaapi_default_param.wspush = kaapi_default_push[i].entrypoint;
        if (kaapi_default_push[i].init !=0)
          (*kaapi_default_push[i].init)();
        found = 1;
        break;
      }
    }
    if (!found)
    {
      fprintf(stderr, "***Kaapi: bad value for variable KAAPI_WSPUSH: '%s'\n", str);
      return EINVAL;
    }
  }

  str = getenv("KAAPI_WSPUSH_INIT");
  if (str !=0)
  {
    int i;
    int found=0;
    for (i=0; ; ++i)
    {
      if (kaapi_default_pushinit[i].entrypoint ==0) break; /* end of list */
      if (strcasecmp(str, kaapi_default_pushinit[i].name) ==0)
      {
        kaapi_default_param.wspush_init = kaapi_default_pushinit[i].entrypoint;
        if (kaapi_default_pushinit[i].init !=0)
          (*kaapi_default_pushinit[i].init)();
        found = 1;
        break;
      }
    }
    if (!found)
    {
      fprintf(stderr, "***Kaapi: bad value for variable KAAPI_WSPUSH: '%s'\n", str);
      return EINVAL;
    }
  }

  str = getenv("KAAPI_WSPUSH_INIT_DISTRIB");
  if (str !=0)
  {
    int i;
    int found=0;
    for (i=0; ; ++i)
    {
      if (kaapi_default_pushinit[i].entrypoint ==0) break; /* end of list */
      if (strcasecmp(str, kaapi_default_pushinit[i].name) ==0)
      {
        kaapi_default_param.wspush_init_distrib = kaapi_default_pushinit[i].entrypoint;
        if (kaapi_default_pushinit[i].init !=0)
          (*kaapi_default_pushinit[i].init)();
        found = 1;
        break;
      }
    }
    if (!found)
    {
      fprintf(stderr, "***Kaapi: bad value for variable KAAPI_WSPUSH: '%s'\n", str);
      return EINVAL;
    }
  }

  str = getenv("KAAPI_STRICT_PUSH");
  if (str != 0)
  {
    if (!strcasecmp(str, "1") || !strcasecmp(str, "true"))
      kaapi_default_param.strict_push = 1;
    else if (!strcasecmp(str, "0") || !strcasecmp(str, "false"))
      kaapi_default_param.strict_push = 0;
    else {
      fprintf(stderr, "***Kaapi: bad value for variable KAAPI_STRICT_PUSH: '%s'\n", str);
      return EINVAL;
    }
  }

  /* initialize the kprocessor key */
#if defined(KAAPI_HAVE_COMPILER_TLS_SUPPORT)
  kaapi_current_thread_key = 0;
#else
  kaapi_assert( 0 == pthread_key_create( &kaapi_current_processor_key, 0 ) );
  kaapi_assert( 0 == pthread_key_create( &kaapi_current_processor_info_key, 0 ) );
#endif

  /* init numa allocation functions */
  err = kaapi_numa_initialize();
  if (err !=0) return err;

  /* init memory subsystem (DSM) */
  err = kaapi_memory_init(0);
  if (err !=0) return err;

#if defined(KAAPI_USE_OFFLOAD)
  /* offload device */
  err = kaapi_offload_init(0);
  if ((err !=0) && (err != EALREADY))  return err;
#endif

#if defined(KAAPI_USE_OFFLOAD)
  /* init device places */
  err = kaapi_offload_init_places();
  if (err !=0) return err;

  /* start offload part */
  err = kaapi_offload_start();
  if (err !=0) return err;
#endif

   /* start up time */
  kaapi_timelib_init();

#if defined(KAAPI_USE_PERFCOUNTER)
#if 0
  kaapi_descrformat_t** fmt_descr = 0;
  int i, count = 0;

  for (i=0; i<KAAPI_FORMAT_MAX; ++i)
  {
    if (kaapi_all_format_byfmtid[i] !=0)
    {
      const kaapi_format_t* fmt = kaapi_all_format_byfmtid[i];
      while (fmt !=0)
      {
        if (fmt->default_body!=0)
          ++count;
        fmt = fmt->next_byfmtid;
      }
    }
  }
  fmt_descr = malloc( sizeof(kaapi_descrformat_t*)*count );
  count = 0;
  for (i=0; i<KAAPI_FORMAT_MAX; ++i)
  {
    if (kaapi_all_format_byfmtid[i] !=0)
    {
      const kaapi_format_t* fmt = kaapi_all_format_byfmtid[i];
      while (fmt !=0)
      {
        if (fmt->default_body!=0)
        {
          fmt_descr[count] = kaapi_tracelib_reserve_perfcounter();
          fmt_descr[count]->fmtid = fmt->fmtid;
          if (fmt->name !=0)
            fmt_descr[count]->name = fmt->name;
          else
            fmt_descr[count]->name = "no name";
          if (fmt->color_dot !=0)
            fmt_descr[count]->color = fmt->color_dot;
          else
            fmt_descr[count]->color = "0.0 0.0 1.0";
          ++count;
        }
        fmt = fmt->next_byfmtid;
      }
    }
  }
#endif

  /* call prior setconcurrency */
  kaapi_tracelib_init(
    0,
    kaapi_default_param.cpucount,
    kaapi_default_param.gpucount,
    kaapi_default_param.places_set[KAAPI_HWS_LEVELID_NUMA].count,
    0,
    0
  );
#endif

  /* format init */
  err = kaapi_format_init();
  if (err !=0) return err;

  /* */
  if (flag == KAAPI_START_ALL_THREADS)
  {
    kaapi_global_team = kaapi_team_init( 0,
      "main",
      (int)kaapi_default_param.cpucount,
      kaapi_default_param.procbind,
      (void (*)(void*))kaapi_mt_init, 0, 0,
      &kaapi_default_param.places,
      KAAPI_TEAM_DEFAULT | KAAPI_TEAM_LDSET
    );
    err = kaapi_team_start( kaapi_global_team );
    kaapi_assert(err ==0);
  }
  else
  {
    kaapi_global_team = 0;
  }
  
#if defined(KAAPI_USE_PERFCOUNTER)
  /* dump output information */
  if (kaapi_tracelib_param.display_perfcounter != KAAPI_NO_DISPLAY_PERF)
  {
    printf("[KAAPI::INIT] use #physical cpu:%u, start time:%15f\n", 
          kaapi_default_param.cpucount,kaapi_get_elapsedtime()
    );
    fflush( stdout );
  }
  if (kaapi_global_team !=0)
  {
    kaapi_context_t* ctxt = kaapi_self_context();
    if (ctxt->proc !=0)
      kaapi_tracelib_thread_switchstate(
        ctxt->proc->rsrc.perfkproc,
        ctxt->proc->rsrc.evtkproc,
        KAAPI_PERF_USR_STATE
      );
  }
#endif
  

#if defined(KAAPI_USE_PERFCOUNTER)
  if (kaapi_tracelib_param.eventmask)
  {
    /* set signal handler to flush event */
    struct sigaction sa;
    sa.sa_handler = _kaapi_signal_dump_counters;
    sa.sa_flags = SA_RESTART;
    sigemptyset (&sa.sa_mask);
    sigaction(SIGINT,  &sa, NULL);
    sigaction(SIGQUIT, &sa, NULL);
    sigaction(SIGTSTP, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGTSTP, &sa, NULL);
  }
#endif
  
  /* */
#if defined(KAAPI_DEBUG)
  /* set alarm */
  if (kaapi_default_param.alarmperiod >0)
  {
    /* set signal handler on SIGALRM + set alarm */
    struct sigaction sa;
    sa.sa_handler = _kaapi_signal_dump_state;
    sa.sa_flags = SA_RESTART;
    sigemptyset (&sa.sa_mask);
    sigaction(SIGALRM , &sa, NULL);

    /* set periodic alarm */
    alarm( kaapi_default_param.alarmperiod );
  }
#endif
  
  return 0;
}


/**
*/
int kaapi_mt_finalize(void)
{
  int i;

  if (KAAPI_ATOMIC_DECR(&kaapi_count_init) !=0) 
    return EAGAIN;

  if (kaapi_global_team !=0)
  {
    kaapi_processor_t* kproc = kaapi_self_processor();
    kaapi_assert(kaapi_global_team->count >0);
    kaapi_assert( kaapi_global_team->all_kprocessors[0] == kproc);
    kaapi_sched_sync( kaapi_context2thread(kproc->context) );
  }


#if defined(KAAPI_USE_PERFCOUNTER)
  if (kaapi_tracelib_param.display_perfcounter != KAAPI_NO_DISPLAY_PERF)
  {  
    printf("[KAAPI::TERM] end time:%15f, delta: %15f(s)\n", kaapi_get_elapsedtime(),
          kaapi_get_elapsedns_since_start()*1e-9 );
    fflush( stdout );
  }
#endif
  
  if (kaapi_global_team !=0)
  {
    /* implicit barrier in finalize */
    kaapi_team_finalize( kaapi_global_team );

    /* force destruction */
    kaapi_team_destroy( kaapi_global_team, 1 );

    if (kaapi_global_team == kaapi_cached_global_team)
      kaapi_cached_global_team = 0;
    kaapi_global_team = 0;
  }
  if (kaapi_cached_global_team !=0)
  {
    kaapi_team_destroy( kaapi_cached_global_team, 1 );
    kaapi_cached_global_team = 0;
  }


#if defined(KAAPI_USE_PERFCOUNTER)
kaapi_processor_t* kproc = kaapi_self_processor();

   kaapi_tracelib_fini();
#endif

  kaapi_memory_finalize();

#if defined(KAAPI_USE_OFFLOAD)
  /* */
  kaapi_offload_finalize();
#endif
  
  /* init numa allocation functions */
  kaapi_numa_finalize();
  

  /* reclaim format' names allocations */
  kaapi_format_finalize();

  /* reclaim hwdetect allocation */
  kaapi_affinityset_t* affset = &kaapi_default_param.affinity_set[0];
  for (i=0; i<KAAPI_HWS_LEVELID_MAX; ++i)
    free(affset[i].cpuset);

  free(kaapi_default_param.places.first);
  kaapi_default_param.places.first = 0;
  kaapi_default_param.places.len = 0;
  KAAPI_ATOMIC_WRITE(&is_hwdetect_called,0);

  return 0;
}


/*
*/
int kaapi_locality_domain_getcount(void)
{
#if defined(KAAPI_USE_NUMA)
  return kaapi_default_param.affinity_set[KAAPI_HWS_LEVELID_NUMA].count;
#else
  return 1;
#endif
}
