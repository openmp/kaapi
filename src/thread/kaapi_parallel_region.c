/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
**
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi_impl.h"
#if defined(KAAPI_DEBUG)
#include <unistd.h>
#include <sys/mman.h>
#endif


/**
*/
kaapi_team_t* kaapi_begin_parallel(
  unsigned int num_threads,
  kaapi_procbind_t procbind,
  void (*body)(void*),
  void (*wrapper)(void*),
  void* arg
)
{
  kaapi_processor_t* kproc = kaapi_self_processor();

  kaapi_team_t* team = kaapi_team_init(
      kproc,
      "region",
      num_threads,
      procbind,
      body, wrapper,
      arg,
      kproc == 0 ? &kaapi_default_param.places : &kproc->place_part,
      KAAPI_TEAM_DEFAULT | KAAPI_TEAM_LDSET
  );
  return team;
}


/**
*/
int kaapi_start_parallel(
  kaapi_team_t* team,
  int startmaster
)
{
  int err = kaapi_team_start(team);
  if (err !=0) return err;

  /* call body(arg) */
  if (startmaster)
    team->all_descriptors[0].body_runprocessor( team->all_descriptors[0].arg_runprocessor );

  return 0;
}


/**
*/
void kaapi_end_parallel(kaapi_team_t* team)
{
#if defined(KAAPI_DEBUG)
  kaapi_processor_t* kproc = kaapi_self_processor();
  kaapi_assert_debug_m(kproc == team->all_kprocessors[0], "bad kproc");
#endif

  /* implicit barrier in finalize */
  kaapi_team_finalize( team );
  kaapi_team_destroy( team, 0 );
}

