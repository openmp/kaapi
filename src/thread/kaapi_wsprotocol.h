/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** vincent.danjean@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#ifndef _KAAPI_WSPROTOCOL_H
#define _KAAPI_WSPROTOCOL_H 1

#include <string.h> // memset

/* ============================= REQUEST ============================ */

/** \ingroup WS
    A pop request
*/
typedef struct kaapi_pop_request_t {
  kaapi_request_op_t            op;
  struct kaapi_place_t*         ld;             /* victim place */
  int                           ident;          /* system wide id who is emetting the request */
  int                           mask_arch;      /* accepted arch */
  int                           status;         /* request status */
  kaapi_task_t*                 task;            /* to push */
} kaapi_pop_request_t;


/** \ingroup WS
    Arg for push request
*/
typedef struct kaapi_push_request_t {
  kaapi_request_op_t            op;
  struct kaapi_place_t*         ld;             /* victim place */
  int                           ident;          /* system wide id who is emetting the request */
  int                           mask_arch;      /* accepted arch */
  int                           status;         /* request status */
  kaapi_task_t*                 task;            /* to push */
} kaapi_push_request_t;


/** \ingroup WS
    Arg for push request
*/
typedef struct kaapi_pushlist_request_t {
  kaapi_request_op_t            op;
  struct kaapi_place_t*         ld;             /* victim place */
  int                           ident;          /* system wide id who is emetting the request */
  int                           mask_arch;      /* accepted arch */
  int                           status;         /* request status */
  kaapi_list_t*                 list;           /* to push */
} kaapi_pushlist_request_t;

/** \ingroup WS
    Arg for extra op request
*/
typedef struct kaapi_extra_request_t {
  kaapi_request_op_t            op;
  struct kaapi_place_t*         ld;             /* victim place */
  int                           ident;          /* system wide id who is emetting the request */
  int                           mask_arch;      /* accepted arch */
  int                           status;         /* request status */
  void*                         arg;            /* arg for func */
  int                         (*func)( struct kaapi_place_t*, void* );  /* func */
  int                           retval;         /* return value for func */
} kaapi_extra_request_t;


/** \ingroup WS
    Request emitted to get work.
    This data structure is pass in parameter of the splitter function.
    On return, the receiver that emits the request will retreive task(s) in the frame.
*/
typedef union kaapi_request_t {
    kaapi_header_request_t   header;
    kaapi_steal_request_t    steal_a;
    kaapi_pop_request_t      pop_a;
    kaapi_push_request_t     push_a;
    kaapi_pushlist_request_t push_l;
    kaapi_extra_request_t    extra_a;
} kaapi_request_t;

/** Private status of request
    \ingroup WS
    
    The protocol to steal work is very simple:
    1- the thief that want to post a request should provide:
      - an status to indicate the completion of the request (kaapi_atomic_t)
      - a frame where to store theft tasks
    2. The post method initialize fields, do a write barrier and set the status to
    KAAPI_REQUEST_S_POSTED
    3. A victim thread that replies must fill the kaapi_tasksteal_arg_t and
    call kaapi_request_replytask with the status of the steal request: 
    either KAAPI_REQUEST_S_OK in case of success, else KAAPI_REQUEST_S_NOK.
    If the victim want to change kind of reply, it can change the body of the
    task. The pre-allocated arguments for the task is currently of size 
    sizeof(kaapi_tasksteal_arg_t).
*/
/* Request status 
*/
typedef enum kaapi_request_status_t {
  KAAPI_REQUEST_S_INIT     = 0,
  KAAPI_REQUEST_S_NOK      = 1,
  KAAPI_REQUEST_S_OK       = 2,
  KAAPI_REQUEST_S_STOP     = 3,  /* stop to steal, return to upper level */
  KAAPI_REQUEST_S_ERROR    = 4,
  KAAPI_REQUEST_S_POSTED   = 5   /* asynchronous request */
} kaapi_request_status_t;


struct kaapi_place_t;
struct kaapi_place_group_operation_t;
struct kaapi_list_t;
struct kaapi_stealcontext_t;


/** \ingroup WS
    This data structure should contains all necessary informations to post a request 
    to a selected node. It should be extended in case of remote work stealing.
*/
typedef struct kaapi_victim_t {
  struct kaapi_place_t* ld; /** the victim processor */
  kaapi_ldid_t                   ident;
} kaapi_victim_t;


/** Flag to ask generation of a new victim or to report an error
*/
typedef enum kaapi_selecvictim_flag_t {
  KAAPI_SELECT_VICTIM,       /* ask to the selector to return a new victim */
  KAAPI_STEAL_SUCCESS,       /* indicate that previous steal was a success */
  KAAPI_STEAL_FAILED,        /* indicate that previous steal has failed (no work) */   
  KAAPI_STEAL_ERROR          /* indicate that previous steal encounter an error */   
} kaapi_selecvictim_flag_t;


/** \ingroup WS
    Select a victim for next steal request
    \param kproc [IN] the kaapi_processor_t that want to emit a request
    \param victim [OUT] the selection of the victim
    \param victim [IN] a flag to give feedback about the steal operation
    \retval 0 in case of success 
    \retval EINTR in case of detection of the termination 
    \retval else error code    
*/
typedef int (*kaapi_selectvictim_fnc_t)( 
    struct kaapi_processor_t*, 
    struct kaapi_victim_t*,
    kaapi_selecvictim_flag_t flag );

/** \ingroup WS
    Used to select queue where to push newly activated task by processor.
    \retval 0 in case of success of the steal
    \retval else error code
*/
typedef int (*kaapi_push_fnc_t)(
    struct kaapi_ressource_t* kproc,
    struct kaapi_place_t* local_place,
    struct kaapi_list_t* list
);

/** \ingroup WS
    Used to push initial set of tasks into queues
    \retval 0 in case of success of the steal
    \retval else error code
*/
typedef void (*kaapi_push_init_fnc_t)(
    struct kaapi_ressource_t* rsrc,
    struct kaapi_frame_wrdlist_t*  frame_rd
);



/** \ingroup WS
    Reply to a steal request.
    Return !=0 if the request cannot be replied.
*/
extern int kaapi_request_reply
( 
  kaapi_request_t* request,
  int              value
);
typedef int (*kaapi_request_reply_fnc_t)(
  kaapi_request_t* request,
  int              value
);


/** Return the request status
  \param pksr kaapi_request_t
  \retval KAAPI_REQUEST_S_OK sucessfull steal operation
  \retval KAAPI_REQUEST_S_NOK steal request has failed
  \retval KAAPI_REQUEST_S_ERROR error during steal request processing
*/
static inline int kaapi_request_get_status( kaapi_request_t* kr )
{ return kr->header.status; }

/** Return true iff the request has been posted
  \param kr kaapi_request_t
  \retval KAAPI_REQUEST_S_OK sucessfull steal operation
  \retval KAAPI_REQUEST_S_NOK steal request has failed
  \retval KAAPI_REQUEST_S_ERROR steal request has failed to be posted because the victim refused request
*/
static inline int kaapi_request_test( kaapi_request_t* kr )
{ return kaapi_request_get_status(kr) != KAAPI_REQUEST_S_POSTED; }

/** Return the data associated with the reply
  \param kr kaapi_request_t
*/
static inline void kaapi_request_syncdata( kaapi_request_t* kr ) 
{ 
  kaapi_mem_barrier();
}


/** \ingroup WS
    Post a request
    \param kproc [IN] the kaapi_processor_t that want to emit a request
    \retval the stolen thread
*/
typedef kaapi_request_t* (*kaapi_sched_postrequest_fnc_t) (
  struct kaapi_ressource_t* kpi,
  struct kaapi_place_t* ld
);

//@{
typedef int (*kaapi_listrequest_iterator_empty_fnc_t)(
  struct kaapi_listrequest_iterator_t* lri
);
typedef kaapi_steal_request_t* (*kaapi_listrequest_iterator_get_fnc_t)(
  struct kaapi_listrequest_iterator_t* lri
);
typedef kaapi_steal_request_t* (*kaapi_listrequest_iterator_next_fnc_t)(
  struct kaapi_listrequest_iterator_t* lri
);
typedef int (*kaapi_listrequest_iterator_count_fnc_t)(
  struct kaapi_listrequest_iterator_t* lri
);
//@}

typedef int (*kaapi_sched_commit_request_fnc_t)(
  struct kaapi_place_t* ld, kaapi_request_t* req
);

typedef int (*kaapi_sched_commit_request_fnc_async_t)(
  struct kaapi_place_group_operation_t* kpgo, struct kaapi_place_t* ld, kaapi_request_t* req
);


/** \ingroup WS
    Emit a steal request
    \param kproc [IN] the kaapi_processor_t that want to emit a request
    \retval the stolen thread
*/
typedef kaapi_request_status_t (*kaapi_emitsteal_fnc_t)( struct kaapi_processor_t* );

/** \ingroup WS
    Called to initialize the emit a steal context
    \param kproc [IN] the kaapi_processor_t that want to emit a request
    \retval the stolen thread
*/
typedef int (*kaapi_emitsteal_init_t)(struct kaapi_stealcontext_t**);
typedef int (*kaapi_emitsteal_dstor_t)(struct kaapi_stealcontext_t*);

//@{
extern int kaapi_request_ccsync_reply
( 
  kaapi_request_t*        request,
  int                     value
);
extern int kaapi_sched_ccsync_commit_request
(
  struct kaapi_place_t* ld,
  kaapi_request_t* req
);
extern int kaapi_sched_ccsync_commit_request_async
(
  struct kaapi_place_group_operation_t* kpgo,
  struct kaapi_place_t* ld,
  kaapi_request_t* req
);

extern int kaapi_listrequest_ccsync_iterator_empty(
  struct kaapi_listrequest_iterator_t* lri
);
extern kaapi_steal_request_t* kaapi_listrequest_ccsync_iterator_get(
  struct kaapi_listrequest_iterator_t* lri
);
extern kaapi_steal_request_t* kaapi_listrequest_ccsync_iterator_next(
  struct kaapi_listrequest_iterator_t* lri
);
extern int kaapi_listrequest_ccsync_iterator_count(
  struct kaapi_listrequest_iterator_t* lri
);
//@}

//@{
extern int kaapi_request_qlock_reply
( 
  kaapi_request_t*        request,
  int                     value
);
extern int kaapi_sched_qlock_commit_request
(
  struct kaapi_place_t* ld,
  kaapi_request_t* req
);
extern int kaapi_listrequest_qlock_iterator_empty(
  struct kaapi_listrequest_iterator_t* lri
);
extern kaapi_steal_request_t* kaapi_listrequest_qlock_iterator_get(
  struct kaapi_listrequest_iterator_t* lri
);
extern kaapi_steal_request_t* kaapi_listrequest_qlock_iterator_next(
  struct kaapi_listrequest_iterator_t* lri
);
extern int kaapi_listrequest_qlock_iterator_count(
  struct kaapi_listrequest_iterator_t* lri
);
//@}


#endif
