/*
 * Code specialisation for using CUBLAS dgemm building bloc
 */
#include <stdlib.h>
#include <stdio.h>

extern "C" {
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <cublas_v2.h>
}
#include "kaapi++.h"
#include "../common.h"

extern "C" {
/* normally loaded with the same module that make _gpu_dgemm_bloc called by the runtime */
__attribute__((weak)) cublasHandle_t kaapi_get_cublas_handle(void);
}

template<class T>
struct CUBLAS {
  typedef T value_type;
  static cublasStatus_t axpy(cublasHandle_t handle, const int n, const value_type *alpha,
                   const value_type *x, const int incx, value_type *y, const int incy);
};
/* specialization for float */
template<>
struct CUBLAS<float> {
  typedef float value_type;
  static cublasStatus_t axpy(cublasHandle_t handle, const int n, const value_type *alpha,
                   const value_type *x, const int incx, value_type *y, const int incy)
  { return cublasSaxpy(handle, n, alpha, x, incx, y, incy); }
};
/* specialization for double */
template<>
struct CUBLAS<double> {
  typedef double value_type;
  static cublasStatus_t axpy(cublasHandle_t handle, const int n, const value_type *alpha,
                   const value_type *x, const int incx, value_type *y, const int incy)
  { return cublasDaxpy(handle, n, alpha, x, incx, y, incy); }
};

template<typename T>
struct TaskAXPYBlas: public ka::Task<3>::Signature<
  T,                          /* alpha */
  ka::R<ka::range1d<T> >,    /* X */
  ka::RW<ka::range1d<T> >   /* Y */
>{};

template<typename T>
struct TaskBodyGPU<TaskAXPYBlas<T> > {
  void operator()( 
         T alpha,
         ka::range1d_r<T> X,
         ka::range1d_rw<T> Y
  );
};

template<>
void TaskBodyGPU<TaskAXPYBlas<float> >::operator() (
         float alpha,
         ka::range1d_r<float> X,
         ka::range1d_rw<float> Y
  )
  {
    cublasStatus_t res;
    cudaStream_t stream;

    double t0 = get_elapsetime();
    cublasHandle_t handle = kaapi_get_cublas_handle();
    res = CUBLAS<float>::axpy(handle, X->size(), &alpha, X->ptr(), 1, Y->ptr(), 1);
    if (res != CUBLAS_STATUS_SUCCESS)
      abort();
    res = cublasGetStream( kaapi_get_cublas_handle(), &stream);
    if (res != CUBLAS_STATUS_SUCCESS)
      abort();
#if 0 /* used to time execution... */
    cudaError_t err = cudaStreamSynchronize(stream);
    if (err != CUDA_SUCCESS)
      abort();
#endif
    double t1 = get_elapsetime();
    printf("Time Launch KERNEL GPU: %f on stream: %p\n", t1-t0, (void*)stream);
  }
