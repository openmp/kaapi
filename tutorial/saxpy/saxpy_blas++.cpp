/*
 * This example presents an scalar product axpy Y = alpha * X + Y
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <cblas.h>
#include <kaapi++.h>

#include "../common.h"

extern "C" {
#include <cuda_runtime_api.h>
#include <cuda.h>
}

template<typename T>
void do_init( ka::range1d<T> A )
{
  for (int i= 0; i < A.size(); ++i)
    A(i) = 1.0f;
   // A(i) = rand()/(T)RAND_MAX;
}

template<typename T>
void do_copy( ka::range1d<T> A, ka::range1d<T> A_copy )
{
  for (int i= 0; i < A.size(); ++i)
    A_copy(i) = A(i);
}

template<typename T>
void do_print( const ka::range1d<T> A )
{
  printf("@:%p\n", (void*)A.ptr());
  for (int i= 0; i < A.size(); ++i)
      printf("%f ", A(i));
  printf("\n");
}

template<class T>
struct CBLAS {
  typedef T value_type;
  static void axpy(const int N, const value_type alpha, const value_type *X,
                   const int incX, value_type *Y, const int incY);
};
/* specialization for float */
template<>
struct CBLAS<float> {
  typedef float value_type;
  static void axpy(const int N, const value_type alpha, const value_type *X,
                   const int incX, value_type *Y, const int incY)
  { cblas_saxpy(N, alpha, X, incX, Y, incY); }
};
/* specialization for double */
template<>
struct CBLAS<double> {
  typedef double value_type;
  static void axpy(const int N, const value_type alpha, const value_type *X,
                   const int incX, value_type *Y, const int incY)
  { cblas_daxpy(N, alpha, X, incX, Y, incY); }
};

template<typename T>
struct TaskAXPYBlas: public ka::Task<3>::Signature<
  T,                          /* alpha */
  ka::R<ka::range1d<T> >,    /* X */
  ka::RW<ka::range1d<T> >   /* Y */
>{};

template<typename T>
struct TaskBodyCPU<TaskAXPYBlas<T> > {
  void operator()( 
         T alpha,
         ka::range1d_r<T> X,
         ka::range1d_rw<T> Y
  )
  {
//    std::cout << "alpha " << alpha << " X " << X->ptr()
//      << " Y " << Y->ptr() << " N " << X->size() << std::endl;
    CBLAS<T>::axpy(X->size(), alpha, X->ptr(), 1, Y->ptr(), 1);
  }
};
/* GPU version */
template<typename T>
struct TaskBodyGPU<TaskAXPYBlas<T> > {
  void operator()( 
         T alpha,
         ka::range1d_r<T> X,
         ka::range1d_rw<T> Y
  );
};

template<typename T>
struct TaskAXPY: public ka::Task<4>::Signature<
  int,                        /* BS */
  T,                          /* alpha */
  ka::RP<ka::range1d<T> >,    /* X */
  ka::RPWP<ka::range1d<T> >   /* Y */
>{};

template<typename T>
struct TaskBodyCPU<TaskAXPY<T> > {
  void operator()( 
         int BS,
         T alpha,
         ka::range1d_rp<T> X,
         ka::range1d_rpwp<T> Y
  )
  {
    int N = X->size();
    for (int i = 0; i < N; i+=BS)
    {
      ka::rangeindex ri( i, i+BS );
      ka::Spawn<TaskAXPYBlas<T> >() ( alpha, X(ri), Y(ri) );
    }
    ka::Sync();
  }
};

#define CONFIG_DO_CHECK 1
#if CONFIG_DO_CHECK
template<typename T>
static int do_check(
          T alpha,
          ka::range1d<T> X,
          ka::range1d<T> Y,
          ka::range1d<T> Y_copy
  )
{
  int N = X.size();
   
  CBLAS<T>::axpy(X.size(), alpha, X.ptr(), 1, Y_copy.ptr(), 1);
  
  if (N <= 16)
  {
    {
       printf("Check C\n");
       ka::rangeindex r( ka::rangeindex::full );
       do_print( Y_copy(r) );
    }
  }
  int res = -1;
  
  for (int i = 0; i < Y.size(); ++i) {
    if (fabs(Y(i) - Y_copy(i)) >= 0.01)
    {
      fprintf(stderr, "ERROR invalid @%lx,%u %f != %f\n", (uintptr_t)Y.ptr(), i, Y(i), Y_copy(i) );
      goto on_error;
    }
  }
  res = 0;
  std::cout << "Results Ok." << std::endl;
  
on_error:
  return res;
}
#endif // CONFIG_DO_CHECK

template<typename T>
void do_run(int NB, int BS)
{
  int N = NB*BS;
  T* dX = (T*)malloc( N*sizeof( T ) );
  T* dY = (T*)malloc( N*sizeof( T ) );
#if defined(CONFIG_DO_CHECK)
  T* dY_copy = (T*)malloc( N*sizeof( T ) );
#endif

  cuMemHostRegister(dX, N*sizeof( T ), CU_MEMHOSTREGISTER_PORTABLE );
  cuMemHostRegister(dY, N*sizeof( T ), CU_MEMHOSTREGISTER_PORTABLE );

  ka::range1d<T> X( dX, N );
  ka::range1d<T> Y( dY, N );
#if defined(CONFIG_DO_CHECK)
  ka::range1d<T> Y_copy( dY_copy, N );
#endif

  ka::rangeindex r( ka::rangeindex::full );
  {
    do_init( X(r) );
    do_init( Y(r) );
#if defined(CONFIG_DO_CHECK)
    do_copy( Y(r), Y_copy(r) );
#endif
  }

  if (N <= 16)
  {
    printf("X\n");
    do_print( X(r) );
    printf("Y\n");
    do_print( Y(r) );
  }

  double t0 = get_elapsetime();
  {
    ka::Spawn<TaskAXPY<T> >() ( BS, 1.0f, X, Y );
    ka::MemorySync();

  }
  double t1 = get_elapsetime();

  if (N <= 16)
  {
    {
       printf("Result Y\n");
       do_print( Y(r) );
    }
  }

#if defined(CONFIG_DO_CHECK)
  {
    do_check(1.0f, X(r), Y(r), Y_copy(r));
  }
#endif
  printf("Time: %f\n", t1-t0);
}


/* In fact, main for XKaapi C++ API
*/
void doit::operator()(int argc, char** argv )
{
  int N = 1024;
  int BS= 256;

  if (argc >1)
    N = atoi(argv[1]);
  if (argc >2)
    BS = atoi(argv[2]);

  N = (N/BS)*BS;

  printf("N=%i\n", N);

  do_run<float>( N/BS, BS );
}
