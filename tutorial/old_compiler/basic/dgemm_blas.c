/*
 * This example presents the well known matrix product.
 * This code is the same as dgemm_omp.c where the matrix is not viewed as a matrix of bloc
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <cblas.h>

#include "../common.h"


#pragma omp declare target
/* C <- C+ A*B, where A, B, C are N x N matrices. */
extern  void dgemm_bloc(int N, int ldC, double* C, int ldA, const double* A, int ldB, const double* B );
#pragma omp end declare target


void do_init(int N, double A[N][N])
{
  int i,j;
  for (i=0; i<N; ++i)
    for (j=0; j<N; ++j)
      A[i][j] = rand()/(double)RAND_MAX;
}


void do_zero(int N, double A[N][N])
{
  int i,j;
  for (i=0; i<N; ++i)
    for (j=0; j<N; ++j)
      A[i][j] = 0;
}


void do_print(int N, double A[N][N])
{
  int i,j;
  printf("@:%p\n", (void*)A);
  for (i=0; i<N; ++i)
  {
    for (j=0; j<N; ++j)
      printf("%f ", A[j][i]);
    printf("\n");
  }
}


void dgemm_bloc(int N, int ldC, double* C, int ldA, const double* A, int ldB, const double *B )
{
  double t0 = get_elapsetime();
  cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans, N, N, N, 1.0, A, ldA, B, ldB, 1.0, (double*)C, ldC );
  double t1 = get_elapsetime();
  printf("Time KERNEL CPU: %f\n", t1-t0);
}


void do_run(int N)
{
  typedef double matrix[N][N];
  matrix* A = (matrix*)malloc( sizeof( matrix ) );
  matrix* B = (matrix*)malloc( sizeof( matrix ) );
  matrix* C = (matrix*)malloc( sizeof( matrix ) );

#pragma omp parallel
#pragma omp master
  {
    #pragma omp task firstprivate(N)
    do_init( N, *A);
    #pragma omp task firstprivate(N)
    do_init( N, *B);
    #pragma omp task firstprivate(N)
    do_zero( N, *C);
  }

  if (N <= 16)
  {
    printf("A @:%p, size=%li\n", (void*)A, sizeof(matrix));
    do_print( N, *A);
#pragma omp taskwait
    printf("B @:%p\n", (void*)B);
    do_print( N, *B);
#pragma omp taskwait
    printf("C @:%p\n", (void*)C);
    do_print( N, *C);
#pragma omp taskwait
  }


  double t0 = get_elapsetime();
#pragma omp parallel
#pragma omp master
  {
#pragma omp task firstprivate(N) \
                 depend( in: A, B )\
                 depend( inout: C[0:N][0:N] )
    dgemm_bloc( N, N, &(*C)[0][0], N, &(*A)[0][0], N, &(*B)[0][0] );

// depend( in: A[0:N][0:N], B[0:N][0:N] )

    if (N <= 16)
    {
      {
         printf("C @:%p\n", (void*)C);
         do_print( N, *C);
      }
    }
  }
  double t1 = get_elapsetime();
  printf("Time: %f\n", t1-t0);
}


int main(int argc, char** argv)
{
  int N= 1024;

  if (argc >1)
    N = atoi(argv[1]);

  printf("N=%i\n", N);

#pragma omp parallel 
#pragma omp master
  do_run( N );

  return 0;
}

