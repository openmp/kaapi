/*
 * Code specialisation for using CUBLAS dgemm building bloc
 */
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <cublas_v2.h>

#include "../common.h"

/* normally loaded with the same module that make _gpu_dgemm_bloc called by the runtime */
__attribute__((weak)) cublasHandle_t kaapi_get_cublas_handle(void);

/* The name of the GPU entry point is _gpu_dgemm_bloc */
extern void _gpu_dgemm_bloc(
  int BS, int ldC, double* C, int ldA, const double* A , int ldB, const double* B
)
{
  double alpha ;
  double beta;
  cublasStatus_t res;
  cudaStream_t stream;

  double t0 = get_elapsetime();
  
  /* A, B, C are pointer to the device. Assuming col major storage. */ 
  alpha = 1.0;
  beta = 0.0;
  res  = cublasDgemm( kaapi_get_cublas_handle(),  
      CUBLAS_OP_N, CUBLAS_OP_N,
      BS, BS, BS, &alpha, A, BS, B, BS, &beta, C, BS 
    );
  if (res != CUBLAS_STATUS_SUCCESS)
    abort();
  res = cublasGetStream( kaapi_get_cublas_handle(), &stream);
  if (res != CUBLAS_STATUS_SUCCESS)
    abort();
#if 0 /* used to time execution... */
  cudaError_t err = cudaStreamSynchronize(stream);
  if (err != CUDA_SUCCESS)
    abort();
#endif
  double t1 = get_elapsetime();
  printf("Time KERNEL GPU: %f\n", t1-t0);
}
