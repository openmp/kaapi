/*
 * Code specialisation for using CUBLAS dgemm building bloc
 */
#include <cublas_v2.h>

/* normally loaded with the same module that make _gpu_dgemm_bloc called by the runtime */
__attribute__((weak)) cublasHandle_t kaapi_get_cublas_handle(void);

/* The name of the GPU entry point is _gpu_dgemm_bloc */
extern void _gpu_dgemm_bloc(int BS, int ldC, double* C, int ldA, const double* A , int ldB, const double* B )
{
  /* A, B, C are pointer to the device. Assuming col major storage. */ 
  double alpha = 1.0;
  double beta = 0.0;
  cublasDgemm( kaapi_get_cublas_handle(),  
    CUBLAS_OP_N, CUBLAS_OP_N,
    BS, BS, BS, &alpha, A, BS, B, BS, &beta, C, BS 
  );
}
