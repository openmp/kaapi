/*
 * This example presents the well known matrix product.
 * This code is the same as dgemm_omp.c where the matrix is not viewed as a matrix of bloc
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <cblas.h>



#pragma omp declare target
/* C <- C+ A*B, where A, B, C are BS x BS matrices. */
extern  void dgemm_bloc(int BS, int ldC, double* C, int ldA, const double* A, int ldB, const double* B );
#pragma omp end declare target


void do_init(int NB, int BS, double A[NB*BS][NB*BS])
{
  int i,j;
  for (i=0; i<NB*BS; i+=BS)
    for (j=0; j<NB*BS; j+=BS)
#pragma omp task firstprivate(i,j,NB,BS)
    {
      int ii, jj;
      for (ii=0; ii<BS; ++ii)
        for (jj=0; jj<BS; ++jj)
          A[i+ii][j+jj] = rand()/(double)RAND_MAX;
    }
}


void do_zero(int NB, int BS, double A[NB*BS][NB*BS])
{
  int i,j;
  for (i=0; i<NB*BS; i+=BS)
    for (j=0; j<NB*BS; j+=BS)
#pragma omp task firstprivate(i,j,NB,BS)
    {
      int ii,jj;
      for (ii=0; ii<BS; ++ii)
        for (jj=0; jj<BS; ++jj)
          A[i+ii][j+jj] = 0;
    }
}


void do_print(int NB, int BS, double A[NB*BS][NB*BS])
{
  int i,j;
  printf("@:%p\n", (void*)A);
  for (i=0; i<NB*BS; ++i)
  {
    for (j=0; j<NB*BS; ++j)
      printf("%f ", A[i][j]);
    printf("\n");
  }
}


void dgemm_bloc(int BS, int ldC, double* C, int ldA, const double* A, int ldB, const double *B )
{
  cblas_dgemm( CblasRowMajor, CblasNoTrans, CblasNoTrans, BS, BS, BS, 1.0, A, ldA, B, ldB, 1.0, (double*)C, ldC );
}

void do_dgemm(int NB, int BS, double C[NB*BS][NB*BS], double A[NB*BS][NB*BS], double B[NB*BS][NB*BS] )
{
  int i, j, k;
  for (i=0; i<NB*BS; i+=BS)
    for (j=0; j<NB*BS; j+=BS)
      for (k=0; k<NB*BS; k+=BS)
#pragma omp task firstprivate(i,j,k,NB,BS) \
                 depend( in: A[i:BS][j:BS], B[i:BS][j:BS] )\
                 depend( inout: C[i:BS:NB*BS][j:BS:NB*BS] )
        dgemm_bloc( BS, NB*BS, &C[i][j], NB*BS, &A[i][k], NB*BS, &B[k][j] );
}

void do_run(int NB, int BS)
{
  typedef double matrix[NB*BS][NB*BS];
  matrix* A = (matrix*)malloc( sizeof( matrix ) );
  matrix* B = (matrix*)malloc( sizeof( matrix ) );
  matrix* C = (matrix*)malloc( sizeof( matrix ) );

#pragma omp taskgroup
  {
    #pragma omp task firstprivate(NB,BS)
    do_init( NB, BS, *A);
    #pragma omp task firstprivate(NB,BS)
    do_init( NB, BS, *B);
    #pragma omp task firstprivate(NB,BS)
    do_zero( NB, BS, *C);
  }

  if (NB*BS <= 16)
  {
    printf("A @:%p, size=%li\n", (void*)A, sizeof(matrix));
    do_print( NB, BS, *A);
    printf("B @:%p\n", (void*)B);
    do_print( NB, BS, *B);
    printf("C @:%p\n", (void*)C);
    do_print( NB, BS, *C);
  }

#pragma omp taskgroup
  {
    do_dgemm( NB, BS, *C, *A, *B);
  }

  if (NB*BS <= 16)
  {
    printf("C @:%p\n", (void*)C);
    do_print( NB, BS, *C);
  }
}


int main(int argc, char** argv)
{
  int N= 1024;
  int BS = 128;

  if (argc >1)
    N = atoi(argv[1]);
  if (argc >2)
    BS = atoi(argv[2]);

  N = (N/BS)*BS;

  printf("N=%i\n NB=%i x BS=%i\n", N, N/BS, BS);

#pragma omp parallel 
#pragma omp master
  do_run( N/BS, BS );

  return 0;
}

