/*
 * This example presents the well known Cholesky factorization.
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <kaapi++.h>

#include "../common.h"

extern "C" {
#if defined(__APPLE__)
  #include <clapack.h>
#else
  /* OpenBLAS */
  #include <lapacke.h>
#endif
#include <cblas.h>
}
//extern "C" {
//#include <cuda_runtime_api.h>
//#include <cuda.h>
//}

void do_init_chol( ka::range2d<double,ColMajor> A )
{
  int i,j;
  for (i=0; i<A.dim(0); ++i)
    for (j=0; j<A.dim(1); ++j)
      A(i,j) = rand()/(double)RAND_MAX;
  
  for (i = 0; i< A.dim(0); i++) {
    A(i,i) = A(i,i) + 1.0*A.dim(0); 
    for (j = 0; j < i; j++)
      A(i,j) = A(j,i);
  }
}

struct TaskPrint: public ka::Task<2>::Signature<
  std::string,                             /* msg */ 
  ka::R<ka::range2d<double,ColMajor> >     /* A */
>
{};

template<>
struct TaskBodyCPU<TaskPrint> {
  void operator()(
         const std::string& msg,
         ka::range2d_r<double,ColMajor> A
  )
  {
    std::cout << msg;
    ka::rangeindex r( ka::rangeindex::full );
    do_print( *(A(r,r)) );
  }
};


/* DPOTRFBlas */
struct TaskDPOTRFBlas: public ka::Task<1>::Signature<
  ka::RW<ka::range2d<double,ColMajor> >    /* A */
>{};
/**/
template<>
struct TaskBodyCPU<TaskDPOTRFBlas> {
  void operator()( 
         ka::range2d_rw<double,ColMajor> A
  )
  {
    //std::cout << "DPOTRFBlas" << std::endl;
#if defined(__APPLE__)
    char uplo = 'l';
    int info= 0;
    int N = A->dim(0);
    int lda = A->lda();
    dpotrf_( &uplo, &N, A->ptr(), &lda, &info );
#else
    LAPACKE_dpotrf( LAPACK_COL_MAJOR, 'l', A->dim(0), A->ptr(), A->lda() );
#endif
  }
};

/* DTRSMBlas */
struct TaskDTRSMBlas: public ka::Task<2>::Signature
<
  ka::R<ka::range2d<double,ColMajor> >, /* A */
  ka::RW<ka::range2d<double,ColMajor> > /* B */
>{};
/**/
template<>
struct TaskBodyCPU<TaskDTRSMBlas> {
  void operator()( 
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_rw<double,ColMajor> B
  )
  {
    //std::cout << "DTRSMBlas" << std::endl;
    cblas_dtrsm( CblasColMajor, CblasRight, CblasLower, CblasTrans, CblasNonUnit,
        B->dim(0), A->dim(0), 1.0, A->ptr(), A->lda(), B->ptr(), B->lda());
  }
};
/* GPU version */
template<>
struct TaskBodyGPU<TaskDTRSMBlas> {
  void operator()(
         ka::range2d_r<double,ColMajor>  A,
         ka::range2d_rw<double,ColMajor> B
  );
};

/* DSYRKBlas */
struct TaskDSYRKBlas: public ka::Task<2>::Signature
<
  ka::R<ka::range2d<double,ColMajor> >, /* A */
  ka::RW<ka::range2d<double,ColMajor> > /* C */
>{};
/**/
template<>
struct TaskBodyCPU<TaskDSYRKBlas> {
  void operator()( 
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_rw<double,ColMajor> C
  )
  {
    //std::cout << "DSYRKBlas" << std::endl;
    cblas_dsyrk( CblasColMajor, CblasLower, CblasNoTrans,
      C->dim(0), A->dim(0), -1.0, A->ptr(), A->lda(), 1.0, C->ptr(), C->lda() );
  }
};
/* GPU version */
template<>
struct TaskBodyGPU<TaskDSYRKBlas> {
  void operator()(
         ka::range2d_r<double,ColMajor>  A,
         ka::range2d_rw<double,ColMajor> C
  );
};

/* DGEMMBlas */
struct TaskDGEMMBlas: public ka::Task<3>::Signature
<
  ka::R<ka::range2d<double,ColMajor> >, /* A */
  ka::R<ka::range2d<double,ColMajor> >, /* B */
  ka::RW<ka::range2d<double,ColMajor> > /* C */
>{};
/**/
template<>
struct TaskBodyCPU<TaskDGEMMBlas> {
  void operator()( 
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_r<double,ColMajor> B,
         ka::range2d_rw<double,ColMajor> C
  )
  {
    //std::cout << "DGEMMBlas" << std::endl;
    cblas_dgemm( CblasColMajor, CblasNoTrans, CblasTrans,
      A->dim(0), A->dim(1), B->dim(1), -1.0, A->ptr(), A->ld(), B->ptr(), B->ld(), 1.0, C->ptr(), C->ld() );
  }
};
/* GPU version */
template<>
struct TaskBodyGPU<TaskDGEMMBlas> {
  void operator()(
         ka::range2d_r<double,ColMajor>  A,
         ka::range2d_r<double,ColMajor>  B,
         ka::range2d_rw<double,ColMajor> C
  );
};

struct TaskCholesky: public ka::Task<2>::Signature<
  int,                                      /* BS */
  ka::RPWP<ka::range2d<double,ColMajor> >    /* A */
>{};

template<>
struct TaskBodyCPU<TaskCholesky> {
  void operator()( 
         int BS,
         ka::range2d_rpwp<double,ColMajor> A
  )
  {
    int N = A->dim(0);
    for (int k= 0; k < N; k+=BS)
    {
      ka::rangeindex rk( k, k+BS );
      ka::Spawn<TaskDPOTRFBlas>()( A(rk,rk) );
      for (int m=k+BS; m < N; m += BS)
      {
        ka::rangeindex rm(m, m+BS);
        ka::Spawn<TaskDTRSMBlas>() ( A(rk,rk), A(rm,rk) );
      }
      for (int m=k+BS; m < N; m += BS)
      {
        ka::rangeindex rm(m, m+BS);
        ka::Spawn<TaskDSYRKBlas>()( A(rm,rk), A(rm,rm) );

        for (int n=k+BS; n < m; n += BS) {
          ka::rangeindex rn(n, n+BS);
          ka::Spawn<TaskDGEMMBlas>()( A(rm,rk), A(rn,rk), A(rm,rn) );
        }
      }
    }
    ka::Sync();
  }
};

void do_run(int NB, int BS, int docheck)
{
  int N = NB*BS;
  double* dA = (double*)malloc( N*N*sizeof( double ) );

  //cuMemHostRegister(dA, N*N*sizeof( double ), CU_MEMHOSTREGISTER_PORTABLE );

  ka::range2d<double,ColMajor> A( dA, N, N, N);

  ka::rangeindex r( ka::rangeindex::full );
  {
    do_init_chol( A(r,r) );
  }

  if (N <= 16)
  {
    printf("A\n");
    do_print( A(r,r) );
  }

  int iter = 1;
  double dtime = 0.0;
  for (int i=0; i<iter; ++i)
  {
    double t0 = get_elapsetime();
    ka::Spawn<TaskCholesky>() ( BS, A(r,r) );
    ka::MemorySync();
    double t1 = get_elapsetime();
    dtime += t1-t0;
    printf("---------------\n");
  }

  if (N <= 16)
  {
    for (int i=0; i<N; i+=BS)
    {
      ka::rangeindex ri( i, i+BS );
      for (int j=0; j<N; j+=BS)
      {
        ka::rangeindex rj( j, j+BS );
        printf("@A(%i,%i)=%p\n", i, j, A(ri,rj).ptr() );
      }
    }

    {
       printf("Result A\n");
       do_print( A(r,r) );
    }
  }

  dtime /= iter;
  /* formula used by PLASMA in time_dpotrf.c */
  double fp_per_mul = 1;
  double fp_per_add = 1;
#define FMULS_POTRF(n) ((n) * (((1. / 6.) * (n) + 0.5) * (n) + (1. / 3.)))
#define FADDS_POTRF(n) ((n) * (((1. / 6.) * (n)      ) * (n) - (1. / 6.)))
  double fmuls = FMULS_POTRF(N);
  double fadds = FADDS_POTRF(N);
  double gflops = 1e-9 * (fmuls * fp_per_mul + fadds * fp_per_add) / dtime;
  //Can compute time to memory transfer back to cpu iff 1 memsync outside the computaiton part
  //double bwds   = 1.9e-9 * (N*N * sizeof(double)) / (t2-t1);
  printf("Time  : %f\n", dtime);
  printf("Gflops: %f\n", gflops);
  //printf("Bwd/s : %f\n", bwds);
}


/* In fact, main for XKaapi C++ API
*/
void doit::operator()(int argc, char** argv )
{
  int N = 1024;
  int BS= 256;
  int docheck = 0;

  if (argc >1)
    N = atoi(argv[1]);
  if (argc >2)
    BS = atoi(argv[2]);
  if (argc >3)
    docheck = (atoi(argv[3]) !=0);


  N = (N/BS)*BS;

  printf("N=%i\nBS=%i\n", N, BS);

  do_run( N/BS, BS, docheck );
}
