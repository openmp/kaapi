/*
 * Code specialisation for using CUBLAS dgemm building bloc
 */
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

extern "C" {
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <cublas_v2.h>
}
#include "kaapi++.h"
#include "../common.h"

extern "C" {
/* normally loaded with the same module that make _gpu_dgemm_bloc called by the runtime */
__attribute__((weak)) cublasHandle_t kaapi_get_cublas_handle(void);
}

struct TaskDGEMMBlas: public ka::Task<3>::Signature<
  ka::R<ka::range2d<double,ColMajor> >,     /* A */
  ka::R<ka::range2d<double,ColMajor> >,     /* B */
  ka::RW<ka::range2d<double,ColMajor> >     /* C */
>{};


template<>
struct TaskBodyGPU<TaskDGEMMBlas> {
  void operator() (
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_r<double,ColMajor> B, 
         ka::range2d_rw<double,ColMajor> C
  );
};

void TaskBodyGPU<TaskDGEMMBlas>::operator() (
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_r<double,ColMajor> B, 
         ka::range2d_rw<double,ColMajor> C
  )
  {
    cublasStatus_t res;
    cudaStream_t stream;

//    double t0 = get_elapsetime();
    int M = A->dim(0);
    int N = A->dim(1);
    int K = B->dim(1);

//    std::cout << "DGEMMBlas GPU" << std::endl;
    /* A, B, C are pointer to the device. Assuming col major storage. */ 
    cublasHandle_t handle = kaapi_get_cublas_handle();
    double alpha = -1.0;
    double beta = 1.0;
    res  = cublasDgemm( handle,
        CUBLAS_OP_N, CUBLAS_OP_T,
        M, N, K, &alpha, A->ptr(), N /*A->ld()*/, B->ptr(), K /*B->ld()*/, &beta, C->ptr(), N/*C->ld()*/
      );
    if (res != CUBLAS_STATUS_SUCCESS)
      abort();

//    double t1 = get_elapsetime();
//    printf("Time Launch KERNEL GPU: %f on stream: %p\n", t1-t0, (void*)stream);
  }

struct TaskDTRSMBlas: public ka::Task<2>::Signature
<
  ka::R<ka::range2d<double,ColMajor> >, /* A */
  ka::RW<ka::range2d<double,ColMajor> > /* B */
>{};

template<>
struct TaskBodyGPU<TaskDTRSMBlas> {
  void operator()(
         ka::range2d_r<double,ColMajor>  A,
         ka::range2d_rw<double,ColMajor> B
  );
};

void TaskBodyGPU<TaskDTRSMBlas>::operator() (
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_rw<double,ColMajor> B
  )
  {
//    std::cout << "DTRSMBlas GPU" << std::endl;
    cublasStatus_t res;
    cublasHandle_t handle = kaapi_get_cublas_handle();
    const double alpha = 1.0;
    res = cublasDtrsm_v2(
        handle, CUBLAS_SIDE_RIGHT, CUBLAS_FILL_MODE_LOWER,
        CUBLAS_OP_T, CUBLAS_DIAG_NON_UNIT,
        B->dim(0), A->dim(0), &alpha, A->ptr(), A->lda(), B->ptr(), B->lda()
      );
    if (res != CUBLAS_STATUS_SUCCESS)
      abort();
  }

struct TaskDSYRKBlas: public ka::Task<2>::Signature
<
  ka::R<ka::range2d<double,ColMajor> >, /* A */
  ka::RW<ka::range2d<double,ColMajor> > /* C */
>{};

template<>
struct TaskBodyGPU<TaskDSYRKBlas> {
  void operator()(
         ka::range2d_r<double,ColMajor>  A,
         ka::range2d_rw<double,ColMajor> C
  );
};

void TaskBodyGPU<TaskDSYRKBlas>::operator() (
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_rw<double,ColMajor> C
  )
  {
//    std::cout << "DSYRKBlas GPU" << std::endl;
    cublasStatus_t res;
    cublasHandle_t handle = kaapi_get_cublas_handle();
    const double alpha = -1.0;
    const double beta = 1.0;
    res = cublasDsyrk_v2(
        handle, CUBLAS_FILL_MODE_LOWER, CUBLAS_OP_N,
        C->dim(0), A->dim(0), &alpha, A->ptr(), A->lda(), &beta, C->ptr(), C->lda()
    );
    if (res != CUBLAS_STATUS_SUCCESS)
      abort();
  }

