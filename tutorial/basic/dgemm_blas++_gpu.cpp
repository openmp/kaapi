/*
 * Code specialisation for using CUBLAS dgemm building bloc
 */
#include <stdlib.h>
#include <stdio.h>

extern "C" {
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <cublas_v2.h>
}
#include "kaapi++.h"
#include "../common.h"

extern "C" {
/* normally loaded with the same module that make _gpu_dgemm_bloc called by the runtime */
__attribute__((weak)) cublasHandle_t kaapi_get_cublas_handle(void);
}


struct TaskDGEMMBlas: public ka::Task<3>::Signature<
  ka::R<ka::range2d<double,ColMajor> >,     /* A */
  ka::R<ka::range2d<double,ColMajor> >,     /* B */
  ka::RW<ka::range2d<double,ColMajor> >     /* C */
>{};


template<>
struct TaskBodyGPU<TaskDGEMMBlas> {
  void operator() (
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_r<double,ColMajor> B, 
         ka::range2d_rw<double,ColMajor> C
  );
};

void TaskBodyGPU<TaskDGEMMBlas>::operator() (
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_r<double,ColMajor> B, 
         ka::range2d_rw<double,ColMajor> C
  )
  {
    cublasStatus_t res;
    cudaStream_t stream;

    double t0 = get_elapsetime();
    int M = A->dim(0);
    int N = A->dim(1);
    int K = B->dim(1);

    /* A, B, C are pointer to the device. Assuming col major storage. */ 
    cublasHandle_t handle = kaapi_get_cublas_handle();
    double alpha = 1.0;
    double beta = 1.0;
    res  = cublasDgemm( handle,
        CUBLAS_OP_N, CUBLAS_OP_N,
        M, N, K, &alpha, A->ptr(), A->ld(), B->ptr(), B->ld(), &beta, C->ptr(), C->ld()
      );
    if (res != CUBLAS_STATUS_SUCCESS)
      abort();
#if 0 /* used to time execution... */
    res = cublasGetStream( kaapi_get_cublas_handle(), &stream);
    if (res != CUBLAS_STATUS_SUCCESS)
      abort();
    cudaError_t err = cudaStreamSynchronize(stream);
    if (err != CUDA_SUCCESS)
      abort();
#endif
    double t1 = get_elapsetime();
    printf("Time Launch KERNEL GPU: %f on stream: %p\n", t1-t0, (void*)stream);
  }
