/*
 * This example presents the well known matrix product.
 * This code is the same as dgemm_omp.c where the matrix is not viewed as a matrix of bloc
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <cblas.h>
#include <kaapi++.h>

#include "../common.h"


struct TaskDGEMMBlas: public ka::Task<3>::Signature<
  ka::R<ka::range2d<double,ColMajor> >,     /* A */
  ka::R<ka::range2d<double,ColMajor> >,     /* B */
  ka::RW<ka::range2d<double,ColMajor> >     /* C */
>{};


template<>
struct TaskBodyCPU<TaskDGEMMBlas> {
  void operator()( 
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_r<double,ColMajor> B, 
         ka::range2d_rw<double,ColMajor> C
  )
  {
    int M = A->dim(0);
    int N = A->dim(1);
    int K = B->dim(1);
    cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans,
      M, N, N, 1.0, A->ptr(), A->ld(), B->ptr(), B->ld(), 1.0, C->ptr(), C->ld()
    );
  }
};

template<>
struct TaskBodyGPU<TaskDGEMMBlas> {
  void operator()(
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_r<double,ColMajor> B, 
         ka::range2d_rw<double,ColMajor> C
  );
};

#define CONFIG_DO_CHECK 1
#if CONFIG_DO_CHECK
static int do_check(
          double alpha,
          ka::range2d<double,ColMajor> A,
          ka::range2d<double,ColMajor> B,
          double beta,
          ka::range2d<double,ColMajor> C,
          ka::range2d<double,ColMajor> C_old
  )
{
  unsigned int i, j, k;
  int M = A.dim(0);
  int N = A.dim(1);
  int K = B.dim(1);
   
  cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans,
    M, N, N, alpha, A.ptr(), A.ld(), B.ptr(), B.ld(), beta, C_old.ptr(), C_old.ld() 
  );
  
  int res = -1;
  
  for (i = 0; i < A.dim(0); ++i) {
    for (j = 0; j < A.dim(1); ++j) {
      if (fabs(C(i,j) - C_old(i,j)) >= 0.01)
      {
        fprintf(stderr, "ERROR invalid @%lx,%u,%u %f != %f\n", (uintptr_t)C.ptr(), i, j, C(i,j), C_old(i,j));
        goto on_error;
      }
    }
  }
  
  res = 0;
  
on_error:
  return res;
}
#endif // CONFIG_DO_CHECK

void do_run(int N)
{
  double* dA = (double*)malloc( N*N*sizeof( double ) );
  double* dB = (double*)malloc( N*N*sizeof( double ) );
  double* dC = (double*)malloc( N*N*sizeof( double ) );
#if defined(CONFIG_DO_CHECK)
  double* dC_copy = (double*)malloc( N*N*sizeof( double ) );
#endif

  ka::range2d<double,ColMajor> A( dA, N, N, N);
  ka::range2d<double,ColMajor> B( dB, N, N, N);
  ka::range2d<double,ColMajor> C( dC, N, N, N);
#if defined(CONFIG_DO_CHECK)
  ka::range2d<double,ColMajor> C_copy( dC_copy, N, N, N);
#endif

  ka::rangeindex r( ka::rangeindex::full );
  {
    do_init( A(r,r) );
    do_init( B(r,r) );
    do_zero( C(r,r) );
#if defined(CONFIG_DO_CHECK)
    do_zero( C_copy(r,r) );
#endif
  }

  if (N <= 16)
  {
    printf("A\n");
    do_print( A(r,r) );
    printf("B\n");
    do_print( B(r,r) );
    printf("C\n");
    do_print( C(r,r) );
  }


  double t0 = get_elapsetime();
  {
    ka::Spawn<TaskDGEMMBlas>() ( A(r,r), B(r,r), C(r,r) );
    ka::MemorySync();
  }
  double t1 = get_elapsetime();

  if (N <= 16)
  {
    {
       printf("Result C\n");
       do_print( C(r,r) );
    }
  }

#if defined(CONFIG_DO_CHECK)
  {
    do_check(1.0, A(r,r), B(r,r), 1.0, C(r,r), C_copy(r,r) );
  }
#endif
  printf("Time: %f\n", t1-t0);
  double gflops = 1.0e-9 * ((2.0 * N * N * N)/(t1-t0));
  printf("GFlop/s: %f\n", gflops);
}


/* In fact, main for XKaapi C++ API
*/
void doit::operator()(int argc, char** argv )
{
  int N= 1024;

  if (argc >1)
    N = atoi(argv[1]);

  printf("N=%i\n", N);

  do_run( N );
}
