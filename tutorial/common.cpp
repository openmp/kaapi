/*
 * This example presents the well known matrix product.
 * This code is the same as dgemm_omp.c where the matrix is not viewed as a matrix of bloc
 */
#include <kaapi++.h>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"

void do_init( ka::range2d<double,ColMajor> A )
{
  int i,j;
  for (i=0; i<A.dim(0); ++i)
    for (j=0; j<A.dim(1); ++j)
      A(i,j) = rand()/(double)RAND_MAX;
}


void do_zero( ka::range2d<double,ColMajor> A )
{
  int i,j;
  for (i=0; i<A.dim(0); ++i)
    for (j=0; j<A.dim(1); ++j)
      A(i,j) = 0;
}


void do_print( const ka::range2d<double,ColMajor> A )
{
  int i,j;
  printf("@:%p\nMatrix([", (void*)A.ptr());
  for (i=0; i<A.dim(0); ++i)
  {
    printf("[");
    for (j=0; j<A.dim(1); ++j)
      printf("%f%c ", A(i,j), (j==A.dim(1)-1 ? ' ': ','));
    printf("]%c\n", (i==A.dim(0)-1 ? ' ': ','));
  }
  printf("])\n");
}


/* C++ main
*/
int main(int argc, char** argv)
{
  try {
    /* Join the initial group of computation : it is defining
       when launching the program by a1run.
    */
    ka::Community com = ka::System::join_community( argc, argv );
    
    /* Start computation by forking the main task */
    ka::SpawnMain<doit>()(argc, argv); 
    
    /* Leave the community: at return to this call no more athapascan
       tasks or shared could be created.
    */
    com.leave();

    /* */
    ka::System::terminate();
  }
  catch (const std::exception& E) {
    ka::logfile() << "Catch : " << E.what() << std::endl;
  }
  catch (...) {
    ka::logfile() << "Catch unknown exception: " << std::endl;
  }
}
