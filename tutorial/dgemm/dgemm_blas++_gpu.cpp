/*
 * Code specialisation for using CUBLAS dgemm building bloc
 */
#include <stdlib.h>
#include <stdio.h>

extern "C" {
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <cublas_v2.h>
}
#include "kaapi++.h"
#include "../common.h"

extern "C" {
/* normally loaded with the same module that make _gpu_dgemm_bloc called by the runtime */
__attribute__((weak)) cublasHandle_t kaapi_get_cublas_handle(void);
}

void print_mat( double* A, int N, int M, int ld)
{
  printf("Matrix:=\n");
  int i,j;
  for (i=0; i<N; ++i)
  {
    for (j=0; j<M; ++j)
      printf(" %15f", A[j*ld+i]);
    printf("\n");
  }
}

void copy_mat( double* hA, double* dA, int M, int N, int ld)
{
  CUresult res;
  CUDA_MEMCPY2D pcopy;
  pcopy.dstXInBytes = 0;
  pcopy.dstY        = 0;
  pcopy.dstPitch    = ld * sizeof(double);
  pcopy.srcXInBytes = 0;
  pcopy.srcY        = 0;
  pcopy.srcPitch    = ld * sizeof(double);
  pcopy.srcMemoryType = CU_MEMORYTYPE_DEVICE;
  pcopy.srcDevice     = (CUdeviceptr)dA;
  pcopy.dstMemoryType = CU_MEMORYTYPE_HOST;
  pcopy.dstHost       = hA;
  pcopy.WidthInBytes = M*sizeof(double);
  pcopy.Height       = N;
  res = cuMemcpy2D( &pcopy );
  if (res != CUDA_SUCCESS)
  {
    printf("ERROR: cuMemcpy2D error: %i\n", res);
  }
}

struct TaskDGEMMBlas: public ka::Task<3>::Signature<
  ka::R<ka::range2d<double,ColMajor> >,     /* A */
  ka::R<ka::range2d<double,ColMajor> >,     /* B */
  ka::RW<ka::range2d<double,ColMajor> >     /* C */
>{};


template<>
struct TaskBodyGPU<TaskDGEMMBlas> {
  void operator() (
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_r<double,ColMajor> B, 
         ka::range2d_rw<double,ColMajor> C
  );
};

void TaskBodyGPU<TaskDGEMMBlas>::operator() (
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_r<double,ColMajor> B, 
         ka::range2d_rw<double,ColMajor> C
  )
  {
    cublasStatus_t res;
    cudaStream_t stream;

    double t0 = get_elapsetime();
    int M = A->dim(0);
    int N = A->dim(1);
    int K = B->dim(1);

    /* A, B, C are pointer to the device. Assuming col major storage. */ 
    cublasHandle_t handle = kaapi_get_cublas_handle();
    double alpha = 1.0;
    double beta = 1.0;
    res  = cublasDgemm( handle,
        CUBLAS_OP_N, CUBLAS_OP_N,
        M, N, K, &alpha, A->ptr(), N /*A->ld()*/, B->ptr(), K /*B->ld()*/, &beta, C->ptr(), N/*C->ld()*/
      );
    if (res != CUBLAS_STATUS_SUCCESS)
      abort();
#if 0 /* used to time execution... */
    res = cublasGetStream( kaapi_get_cublas_handle(), &stream);
    if (res != CUBLAS_STATUS_SUCCESS)
      abort();
    CUresult err = cuStreamSynchronize(stream);
    if (err != CUDA_SUCCESS)
      abort();
    double hA[M*K];
    double hB[K*N];
    double hC[M*N];
    copy_mat(&hA[0], A->ptr(), M, K, M); //A->ld());
    print_mat( hA, K, N, K);
    copy_mat(&hB[0], B->ptr(), K, N, K);//B->ld());
    print_mat( hB, K, M, K);
    copy_mat(&hC[0], C->ptr(), M, N, M);//C->ld());
    print_mat( hC, M, N, M);
#endif

    double t1 = get_elapsetime();
    //printf("Time Launch KERNEL GPU: %f on stream: %p\n", t1-t0, (void*)stream);
  }
