/*
 * This example presents the well known matrix product.
 * This code is the same as dgemm_omp.c where the matrix is not viewed as a matrix of bloc
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <cblas.h>
#include <kaapi++.h>

#include "../common.h"


#if 0
extern "C" {
#include <cuda_runtime_api.h>
#include <cuda.h>
}
#endif

struct TaskPrint: public ka::Task<2>::Signature<
  std::string,                             /* msg */ 
  ka::R<ka::range2d<double,ColMajor> >     /* A */
>
{};

template<>
struct TaskBodyCPU<TaskPrint> {
  void operator()(
         const std::string& msg,
         ka::range2d_r<double,ColMajor> A
  )
  {
    std::cout << msg;
    ka::rangeindex r( ka::rangeindex::full );
    do_print( *(A(r,r)) );
  }
};


struct TaskDGEMMBlas: public ka::Task<3>::Signature<
  ka::R<ka::range2d<double,ColMajor> >,     /* A */
  ka::R<ka::range2d<double,ColMajor> >,     /* B */
  ka::RW<ka::range2d<double,ColMajor> >     /* C */
>{};


template<>
struct TaskBodyCPU<TaskDGEMMBlas> {
  void operator()( 
         ka::range2d_r<double,ColMajor> A,
         ka::range2d_r<double,ColMajor> B, 
         ka::range2d_rw<double,ColMajor> C
  )
  {
    int M = A->dim(0);
    int N = A->dim(1);
    int K = B->dim(1);
    cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans,
      M, N, N, 1.0, A->ptr(), A->ld(), B->ptr(), B->ld(), 1.0, C->ptr(), C->ld()
    );
  }
};

template<>
struct TaskBodyGPU<TaskDGEMMBlas> {
  void operator()(
         ka::range2d_r<double,ColMajor>  A,
         ka::range2d_r<double,ColMajor>  B,
         ka::range2d_rw<double,ColMajor> C
  );
};


struct TaskDGEMM: public ka::Task<4>::Signature<
  int,                                      /* BS */
  ka::RP<ka::range2d<double,ColMajor> >,    /* A */
  ka::RP<ka::range2d<double,ColMajor> >,    /* B */
  ka::RPWP<ka::range2d<double,ColMajor> >   /* C */
>{};

template<>
struct TaskBodyCPU<TaskDGEMM> {
  void operator()( 
         int BS,
         ka::range2d_rp<double,ColMajor> A,
         ka::range2d_rp<double,ColMajor> B,
         ka::range2d_rpwp<double,ColMajor> C
  )
  {
    int M = A->dim(0);
    int N = A->dim(1);
    int K = B->dim(1);
    int i, j, k;
    for (i=0; i<M; i+=BS)
    {
      ka::rangeindex ri( i, i+BS );
      for (j=0; j<N; j+=BS)
      {
        ka::rangeindex rj( j, j+BS );
        for (k=0; k<K; k+=BS)
        {
          ka::rangeindex rk( k, k+BS );
          ka::Spawn<TaskDGEMMBlas>() ( A(ri,rk), B(rk,rj), C(ri,rj) );
        }
      }
    }
    ka::Sync();
  }
};

#define CONFIG_DO_CHECK 1
#if CONFIG_DO_CHECK
static int do_check(
          ka::range2d<double,ColMajor> A,
          ka::range2d<double,ColMajor> B,
          ka::range2d<double,ColMajor> C,
          ka::range2d<double,ColMajor> C_old
  )
{
  unsigned int i, j, k;
  int M = A.dim(0);
  int N = A.dim(1);
  int K = B.dim(1);
   
  cblas_dgemm( CblasColMajor, CblasNoTrans, CblasNoTrans,
    M, N, N, 1.0, A->ptr(), A->ld(), B->ptr(), B->ld(), 0.0, C_old->ptr(), C_old->ld()
  );
  
  if (N <= 16)
  {
    {
       printf("Check C\n");
       ka::rangeindex r( ka::rangeindex::full );
       do_print( C_old(r,r) );
    }
  }
  int res = -1;
  
  for (i = 0; i < A.dim(0); ++i) {
    for (j = 0; j < A.dim(1); ++j) {
      if (fabs(C(i,j) - C_old(i,j)) >= 1e-6)
      {
        fprintf(stderr, "ERROR invalid @%lx,%u,%u %f != %f\n", (uintptr_t)C.ptr(), i, j, C(i,j), C_old(i,j));
        goto on_error;
      }
    }
  }
  
  res = 0;
  
on_error:
  return res;
}
#endif // CONFIG_DO_CHECK

void do_run(int NB, int BS, int docheck)
{
  int N = NB*BS;
  double* dA = (double*)malloc( N*N*sizeof( double ) );
  double* dB = (double*)malloc( N*N*sizeof( double ) );
  double* dC = (double*)malloc( N*N*sizeof( double ) );


#if 0
  cuMemHostRegister(dA, N*N*sizeof( double ), CU_MEMHOSTREGISTER_PORTABLE );
  cuMemHostRegister(dB, N*N*sizeof( double ), CU_MEMHOSTREGISTER_PORTABLE );
  cuMemHostRegister(dC, N*N*sizeof( double ), CU_MEMHOSTREGISTER_PORTABLE );
#endif

  ka::range2d<double,ColMajor> A( dA, N, N, N);
  ka::range2d<double,ColMajor> B( dB, N, N, N);
  ka::range2d<double,ColMajor> C( dC, N, N, N);

  ka::rangeindex r( ka::rangeindex::full );

  printf("--------------- Init\n");
  {
    do_init( A(r,r) );
    do_init( B(r,r) );
  }

  if (N <= 16)
  {
    printf("A\n");
    do_print( A(r,r) );
    printf("B\n");
    do_print( B(r,r) );
    printf("C\n");
    do_print( C(r,r) );
  }

  int iter = 2;
  double dtime = 0.0;
  printf("--------------- compute\n");
  for (int i=0; i<iter; ++i)
  {
    do_zero( C(r,r) );
    double t0 = get_elapsetime();
    ka::Spawn<TaskDGEMM>() ( BS, A(r,r), B(r,r), C(r,r) );
    ka::Sync();
    ka::MemorySync();
    double t1 = get_elapsetime();
    dtime += t1-t0;
    printf("---------------\n");
  }

  if (N <= 16)
  {
    for (int i=0; i<N; i+=BS)
    {
      ka::rangeindex ri( i, i+BS );
      for (int j=0; j<N; j+=BS)
      {
        ka::rangeindex rj( j, j+BS );
        printf("@C(%i,%i)=%p\n", i, j, C(ri,rj).ptr() );
      }
    }

    {
       printf("Result C\n");
       do_print( C(r,r) );
    }
  }

#if defined(CONFIG_DO_CHECK)
  if (docheck)
  {
    double* dC_copy = 0;
    dC_copy = (double*)malloc( N*N*sizeof( double ) );
    ka::range2d<double,ColMajor> C_copy(dC_copy, N, N, N);
    do_zero( C_copy(r,r) );
    do_check( A(r,r), B(r,r), C(r,r), C_copy(r,r) );
  }
#endif
  dtime /= iter;
  double gflops = 1.0e-9 * ((2.0 * N * N * N )/dtime);
  //Can compute time to memory transfer back to cpu iff 1 memsync outside the computaiton part
  //double bwds   = 1.9e-9 * (N*N * sizeof(double)) / (t2-t1);
  printf("Time  : %f\n", dtime);
  printf("Gflops: %f\n", gflops);
  //printf("Bwd/s : %f\n", bwds);
}


/* In fact, main for XKaapi C++ API
*/
void doit::operator()(int argc, char** argv )
{
  int N = 1024;
  int BS= 256;
  int docheck = 0;

  if (argc >1)
    N = atoi(argv[1]);
  if (argc >2)
    BS = atoi(argv[2]);
  if (argc >3)
    docheck = (atoi(argv[3]) !=0);


  N = (N/BS)*BS;

  printf("N=%i\nBS=%i\n", N, BS);

  do_run( N/BS, BS, docheck );
}
