/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
**
 *
 *
 *
**/
#ifndef __COMMON_H_
#define __COMMON_H_
#include <sys/time.h>
#include "kaapi++.h"

#ifndef __cplusplus
extern "C" {
#endif
static inline double get_elapsetime(void)
{
   struct timeval tmp ;
   gettimeofday (&tmp, 0) ;
   return (double) tmp.tv_sec + ((double) tmp.tv_usec) * 1e-6;
}

/* utility functions to initialize matrix
*/
extern void do_init( ka::range2d<double,ColMajor> A );
extern void do_zero( ka::range2d<double,ColMajor> A );
extern void do_print( const ka::range2d<double,ColMajor> A );

/* In fact, main for XKaapi C++ API
   Redefined in each example of this tutorial.
*/
struct doit {
  void operator()(int argc, char** argv );
};


#ifndef __cplusplus
}
#endif

#endif

