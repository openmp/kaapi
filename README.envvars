
* Available environment variables:

*** Number of resources
-----------------
KAAPI_CPUSET:
  CPUSET for mapping kprocessors to CPU.
  The format to define CPUSET is the same as OMP_PLACES.
  For instance, KAAPI_CPUSET="cores(8)" or =“{7:3},{16}"

KAAPI_CPUCOUNT:
  Number of CPU to used into the CPUSET.
  If CPUSET is not defined, then the OS maps 
  kprocessors to CPU.


*** Scheduling algorithms
-----------------
KAAPI_WSPUSH=local|numa|wnuma|whws|cyclic

KAAPI_WSPUSH_INIT=wspush|cyclic|rand|cyclicnuma|randnuma|cyclicnumastrict

KAAPI_STRICT_PUSH=true|false
  Default is false.
  If true then a task is pushed to the private queue of the resource. Only core that shared the
  resource can pop task.

KAAPI_WSSELECT=rand|local|numa|dnuma



*** Performance counters
-----------------
KAAPI_DISPLAY_PERF: 1, full, resume, gplot

KAAPI_RECORD_TRACE: defined to generate trace of events at runtime.
  Only available if the library was configured with --with-perfcounter
KAAPI_RECORD_MASK: name[,name]* where name is either compute, idle, steal or the event number.
  Only record event in the mask.
  
KAAPI_PERF_EVENTS: allows to define performance counters collected at runtime.
  These performance counters are software performance counters or hardware (PAPI) counters.
  The list of software counters is defined in get_event_code in kaapi_perf.c:
    - KAAPI_OFFLOAD
    - KAAPI_TASK
    - KAAPI_TIMES
    - KAAPI_DFGBUILD
    - KAAPI_STEALREQOK
    - KAAPI_STEALREQ
    - KAAPI_STEALREQOP
    - KAAPI_STEALIN
    - KAAPI_CONFLICTPOP
    - KAAPI_STACKSIZE
  (please refer to get_event_code for uptodate version).

  List of PAPI event depends on PAPI (see papi_avail). For instance:
  PAPI_L1_DCM, PAPI_TOT_CYC or PAPI_FP_OPS.

  Usage: KAAPI_PERF_EVENTS=PAPI_TOT_CYC,KAAPI_TIMES,KAAPI_CONFLICTPOP,PAPI_FP_OPS ./a.out
  The display of performance counters is controlled by KAAPI_DISPLAY_PERF.

KAAPI_TASKPERF_EVENTS: as for KAAPI_PERF_EVENTS but for counting per task.
  The set of events is more restrictive (typically at most 4 counters).
  Note: the runtime also add those perf counters into KAAPI_PERF_EVENTS such
  that KAAPI_TASKPERF_EVENTS is a subset of KAAPI_PERF_EVENTS.


*** GRAPH
-----------------
KAAPI_DUMP_GRAPH:
  Dump the graph after spawning task with SetStaticSched attribut
  is used. Only operational if XKaapi library is compiled with
  '--with-perfcounter'.

KAAPI_DOT_NODATA_LINK:
  Do not dump data nodes in the DFG dot graph.
  
KAAPI_DOT_NOACTIVATION_LINK:
  Do not print activation link between tasks when graph is dump into 
  the dot output.

KAAPI_DOT_NOVERSION_LINK:
  Do not print link between version of data when graph is dump into the dot output.
  
KAAPI_DOT_NOLABEL:
  Do not print information inside each task node or data node of the dot output.

KAAPI_DUMP_PERIOD:
  Dump the internal state at given period (in second).
  Only work with debug library.


*** CUDA support [NOT yet implemented]
-----------------

KAAPI_CUDA_WINDOW_SIZE: (number)
  Set the sliding window size of GPU tasks to overlap (default is 2).

KAAPI_CUDA_PEER: (0 or 1)
  Enable peer-to-peer memory copy (default disabled).

KAAPI_CUDA_TRACE_BUFFER: (number)
  Set the buffer size for CUDA traces in MBytes (default is 1 MB).

KAAPI_CUDA_CACHE_POLICY: (string)
  Set the software cache policy for GPUs. Options are lru (least recently used) or lru_double 
  (least recently used based on data access). Default is lru_double. 

