\select@language {french}
\setcounter {tocdepth}{1}
\contentsline {section}{\numberline {1}Motivation et objectif}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}OpenMP~: des boucles parall\IeC {\`e}les vers les t\IeC {\^a}ches}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}\textsc {X-Kaapi}\xspace ~: des t\IeC {\^a}ches vers les boucles parall\IeC {\`e}les}{4}{subsection.1.2}
\contentsline {section}{\numberline {2}D\IeC {\'e}marrage de l'environnement parall\IeC {\`e}le}{5}{section.2}
\contentsline {section}{\numberline {3}Boucle parall\IeC {\`e}le}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Boucle parall\IeC {\`e}le \IeC {\'e}quivalente en OpenMP}{7}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}S\IeC {\'e}quence de boucles parall\IeC {\`e}les}{8}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Boucles imbriqu\IeC {\'e}s}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}S\IeC {\'e}lection de la politique d'ordonnancement}{9}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Portage de la gestion de l'affinit\IeC {\'e} \textsc {X-Kaapi}\xspace }{11}{subsection.3.5}
\contentsline {section}{\numberline {4}T\IeC {\^a}ches}{12}{section.4}
\contentsline {section}{\numberline {5}Perspectives}{13}{section.5}
