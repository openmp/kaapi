\documentclass[a4paper, 11pt]{article}

\usepackage{graphicx}
\usepackage{graphics}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{color}
%\usepackage[utf8]{inputenc}
\usepackage{RR}
\usepackage{hyperref}

\usepackage[latin1]{inputenc} % ou \usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc} % ou \usepackage[OT1]{fontenc}

%%% A METTRE
%%%
\RRNo{xxxx}

%%
%% date de publication du rapport
\RRdate{November 2011}
%%
%% Cas d'une version deux
%% \RRversion{2}
%% date de publication de la version 2
%% \RRdater{Novembre  2006}
\usepackage{listings}

\usepackage{amssymb}
\usepackage{xspace} 
\usepackage{array} 
\usepackage{multirow}
\newcommand\hyph{\nobreak\hskip0pt-\nobreak\hskip0pt\relax}
\newlength\savedwidth
\newcommand\whline{\noalign{\global\savedwidth
  \arrayrulewidth\global\arrayrulewidth 1.5pt}
  \hline \noalign{\global\arrayrulewidth
  \savedwidth}
}
\newcolumntype{I}{!{\vrule width 1.5pt}}
\renewcommand{\arraystretch}{1.5}

\newcommand{\kaapi}{\textsc{X-Kaapi}\xspace}

%%% For all listing figures
\definecolor{MyDarkBlue}{rgb}{0.254901960784314, 0.411764705882353, 0.882352941176471}

\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\lstset{stringstyle=\ttfamily}
\lstset{ classoffset=1
           }
\lstset{ classoffset=2 
           }
\lstset{ classoffset=3
           }
           
\lstset{classoffset=0, showstringspaces=false}

\RRdate{November 2011}

%%
\RRauthor{
Fabien Le Mentec\thanks{INRIA}
  \and
Vincent Faucher\thanks{CEA, DEN, DANS, DM2S, SEMT, DYN, F-91191 Gif-sur-Yvette, France}
  \and
Thierry Gautier\thanks{INRIA}
}

\authorhead{Gautier \& Faucher \& Le Mentec}
\RRtitle{\kaapi Fortran programming interface\\ \hfill version 3.0}
\RRetitle{\kaapi Fortran programming interface\\ \hfill version 3.0}
\titlehead{\kaapi Fortran programming interface}

\RRresume{Ce rapport décrit l'interface Fortran du logical \kaapi.}
\RRabstract{This report defines the \kaapi Fortran programming interface.}

\RRmotcle{environment de programmation parallèle, \kaapi, Fortran}
\RRkeyword{parallel computing, \kaapi, Fortran}
\RRprojets{MOAIS}
\RCGrenoble % Grenoble - Rh\^one-Alpes


\begin{document}

% \makeRR % cas d'un rapport de recherche
\makeRT % cas d'un rapport technique.

\tableofcontents
\addtocontents{toc}{\protect\setcounter{tocdepth}{1}}

\newpage
\section{Software installation}\label{sec:userinstall}

\kaapi is both a programming model and a runtime for high performance parallelism targeting multicore and distributed architectures. 
It relies on the work stealing paradigm.
\kaapi was developed in the MOAIS INRIA project by Thierry Gautier, Fabien Le Mentec, Vincent Danjean and Christophe Laferri\`{e}re in the early stage of a previous library.

In this report, only the programming model based on the Fortran API is presented.
The runtime library comes also with a full set of complementary programming interfaces: C, C++ and STL-like interfaces. The C++ and STL interfaces, at a higher level than the C interface, may be directly used for developing parallel programs or libraries.

\subsection*{Installation}
\kaapi uses the GNU auto-tools, so you can use the classic scheme
\begin{verbatim}
> ../xkaapi-3.0/configure  --prefix=my_prefix_xkaapi --with-numa
> make install
\end{verbatim}
Numa option allows to build optimized version for numa architecture. It requires libnuma installed on your system.
With NUMA option, the subroutines \verb+kaapif_set_distribution+ and \verb+kaapif_set_affinity+ allow to control
loop scheduling over the NUMA architecture.
The configure script has a lot of options (run it with \verb+--help+). More documentation can be found of the website.

\kaapi has been tested with success on:
Linux with gcc/g++ 4.1, 4.3, 4.4 and 4.5, 4.7, 4.8, 4.9
It is currently developed with gcc 4.9.1, 4.7, 4.8 and 4.9 and clang 3.5.

\subsection*{Linking with \kaapi}
Once installed, the \kaapi offer a set of libraries that need to be linked against your program. A typical compilation with gfortran will be:
\begin{verbatim}
> gfortran -O3 -o myprog myprog.f -L<your prefix>/lib \
    -lkaapif -lkomp -lkaapi
\end{verbatim}
where \verb+kaapif+ is the Fortran subroutines definition, \verb+komp+ is the \kaapi's OpenMP runtime functions and \verb+kaapi+ the \kaapi  low level runtime functions.

Note that the set of libraries required for Fortran interface can be obtained with pkg-config tool:
\begin{verbatim}
> export PKG_CONFIG_PATH=my_prefix_xkaapi/lib/pkgconfig:PKG_CONFIG_PATH
> pkg-config --libs kaapif
-Lmy_prefix_xkaapi/lib -lkaapif -lkomp -lkaapi 
>
\end{verbatim}	

\subsection*{Supported Platforms}
\kaapi targets essentially SMP and NUMA platforms. The runtime should run
on any system providing:
\begin{itemize}
\item a GNU toolchain (4.3),
\item the pthread library,
\item Unix based environment.
\end{itemize}
It has been extensively tested on the following operating systems:
\begin{itemize}
\item GNU-Linux with x86\_64 architectures,
\item MacOSX/Intel processor.
\end{itemize}

There is no version for Windows yet.

\subsection*{\kaapi Contacts}
If you wish to contact the XKaapi team, please visite the web site at:
\begin{center}
\url{http://kaapi.gforge.inria.fr}
\end{center}

\newpage
\section{Initialization and termination}

\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
INTEGER*4 FUNCTION KAAPIF_INIT(INTEGER*4 FLAGS)
INTEGER*4 FUNCTION KAAPIF_FINALIZE()
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
\textit{KAAPIF\_INIT} initializes the runtime. It must be called once per program
before using any of the other routines. If successful, there must be a
corresponding \textit{KAAPIF\_FINALIZE} at the end of the program.

\subsection{Parameters}
\begin{itemize}
\item \textit{FLAGS}: if not zero, start only the main thread to avoid disturbing
the execution until tasks are actually scheduled. The other threads are suspended
waiting for a parallel region to be entered (refer to KAAPIF\_BEGIN\_PARALLEL).
\end{itemize}

\subsection{Return value}
\paragraph{}
Refer to the \textit{Error codes} section.

\subsection{Example}
\paragraph{}
Refer to examples/kaapif/foreach\\
\begin{small}
\begin{lstlisting}[frame=tb]
  PROGRAM MAIN
    INTEGER*4 E

    E = KAAPIF_INIT(1)
    ...
    E = KAAPIF_FINALIZE()

  END PROGRAM MAIN
\end{lstlisting}
\end{small}


\newpage
\section{Library versioning}

\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
SUBROUTINE KAAPIF_GET_VERSION(CHARACTER(40) VERSION)
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
\textit{KAAPIF\_GET\_VERSION} retrieves a string describing the runtime version.
Currently, the string is the git last commit hash in base64.

\subsection{Parameters}
\begin{itemize}
\item \textit{VERSION}: An array large enough to hold the version.
\end{itemize}

\subsection{Return value}
\paragraph{}
None.

\subsection{Example}
\paragraph{}
Refer to examples/kaapif/get\_version\\
\begin{small}
\begin{lstlisting}[frame=tb]
   PROGRAM MAIN

      CHARACTER(40) VERSION

      CALL KAAPIF_INIT(1)
      CALL KAAPIF_GET_VERSION(VERSION)
      CALL KAAPIF_FINALIZE()

      WRITE(*, *) 'version: ', VERSION(1:40)

  END PROGRAM MAIN
\end{lstlisting}
\end{small}


\newpage
\section{Concurrency}

\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
INTEGER*4 FUNCTION KAAPIF_GET_CONCURRENCY()
INTEGER*4 FUNCTION KAAPIF_GET_THREAD_NUM()
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
Concurrency related routines.

\subsection{Return value}
\paragraph{}
\textit{KAAPIF\_GET\_CONCURRENCY} returns the number of parallel thread available
to the \kaapi runtime.
\paragraph{}
\textit{KAAPIF\_GET\_THREAD\_NUM} returns the current thread identifier. Note it
should only be called in the context of a \kaapi thread.
\paragraph{}
For both functions, a negative value means an error occured. Refer to the
\textit{Error codes} section.

\subsection{Example}
\paragraph{}
Refer to examples/kaapif/foreach


\newpage
\section{Performance}

\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
REAL*8 FUNCTION KAAPIF_GET_TIME()
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
Capture the current time. Used to measure the time spent in a code region.

\subsection{Parameters}
\paragraph{}
None.

\subsection{Return value}
\paragraph{}
The current time, in microseconds.

\subsection{Example}
\paragraph{}
Refer to examples/kaapif/foreach\\

\begin{small}
\begin{lstlisting}[frame=tb]
  PROGRAM MAIN
    REAL*8 START
    REAL*8 STOP
    INTEGER*4 E

    E = KAAPIF_INIT(1)
    START = KAAPIF_GET_TIME()
    ...
    STOP = KAAPIF_GET_TIME()
    E = KAAPIF_FINALIZE()

    WRITE(*, *) STOP - START

  END PROGRAM MAIN
\end{lstlisting}
\end{small}


\newpage
\section{Independent loops}

\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
INTEGER*4 FUNCTION KAAPIF_FOREACH
(
  INTEGER*4 FIRST, INTEGER*4 LAST,
  INTEGER*4 NARGS,
  BODY,
  ...
)

INTEGER*4 FUNCTION KAAPIF_FOREACH_WITH_FORMAT
(
  INTEGER*4 FIRST, INTEGER*4 LAST,
  INTEGER*4 NARGS,
  BODY,
  ...
)
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
Those routines run a parallel loop over the range [\textit{FIRST}, \textit{LAST}]
(note this is an \textbf{inclusive} interval). In order to reach peak performance, the reader would refer
sections to control the scheduling policy, the grain size, the distribution of iterations and may be how
to control affinity during execution.
The loop body is defined by \textit{BODY} whose arguments are given
in parameters. It must have the following prototype:\\
\begin{small}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
SUBROUTINE BODY(I, J, TID, ...)
\end{lstlisting}
\end{small}
\begin{itemize}
\item $[I, J]$ the subrange to process (note that interval is inclusive)
\item \textit{TID} the thread identifier
\end{itemize}

\subsection{Parameters}
\begin{itemize}
\item \textit{FIRST}, \textit{LAST}: the iteration range indices, inclusive.
\item \textit{NARGS}: the argument count
\item \textit{BODY}: the function body to be called at each iteration
\item \textit{...}: the arguments passed to \textit{BODY}. For\\
\textit{KAAPIF\_FOREACH\_WITH\_FORMAT},\\
 refer to the \textit{KAAPIF\_SPAWN}
documentation.
\end{itemize}

\subsection{Return value}
\paragraph{}
Refer to the \textit{Error codes} section.

\subsection{Example}
Refer to examples/kaapif/foreach\\
Refer to examples/kaapif/foreach\_with\_format\\

\begin{small}
\begin{lstlisting}[frame=tb]
  ! computation task entry point
  SUBROUTINE OP(I, J, TID, ARRAY)
    DO K = I, J
      ! process ARRAY(K)
      ...
    END DO
    RETURN
  END

  PROGRAM MAIN
    INTEGER*4 E

    ...
    ! apply the OP routine on ARRAY[1:SIZE]
    E = KAAPIF_FOREACH(1, SIZE, 1, OP, ARRAY)

    ...
    ! version with format
    ! as above, ARRAY is the only argument
    ! 
    E = KAAPIF_FOREACH_WITH_FORMAT
    (
    ! iterated range and argument count are the same as above
    1, SIZE, 1, OP,
    ! ARRAY an array of SIZE double
    ! ARRAY elments are read and written by the task
    ARRAY, KAAPIF_TYPE_DOUBLE, SIZE, KAAPIF_MODE_RW
    )
    ...

  END PROGRAM MAIN
\end{lstlisting}
\end{small}


\newpage
\section{Setting the loop scheduler policy}

\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
INTEGER*4 FUNCTION KAAPIF_SET_POLICY
(
  INTEGER*4 SCHEDULER
)
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
\textit{KAAPIF\_SET\_POLICY} permit to select the loop scheduler policy.
The scheduler policy impact the initial distribution of work as well as the runtime load balancing.
The general framework of Kaapi loop scheduler is composed by 2 steps:
\begin{enumerate}
\item distribution of loop iterations among a set of threads,
\item work balancing between threads.
\end{enumerate}

The function  \textit{KAAPIF\_SET\_DISTRIBUTION} and \textit{KAAPIF\_SET\_AFFINITY} may restrict the set of threads on which iterations are initially distributed and also which threads may be involved in the dynamic load balancing step. 
\paragraph{}
Note that this routine sets global variables used by the next subsequent call to
\textit{KAAPIF\_FOREACH} family functions. 

\subsection{Parameters}
\begin{itemize}
\item \textit{SCHEDULER}: one of the value: 
\begin{itemize}
\item 0 or \textit{KAAPIF\_POLICY\_DEFAULT} (defined in kaapif.inc, kaapif.inc.f90 or kaapif.inc.f95), the default policy which is implementation dependent. Currently it is the same as  \textit{KAAPIF\_POLICY\_ADAPTIVE}.
\item 1 or  \textit{KAAPIF\_POLICY\_STATIC} it corresponds to the OpenMP static scheduling policy without dynamic load balancing after the initial distribution work.
\item 7 or  \textit{KAAPIF\_POLICY\_ADAPTIVE} corresponds to an initial work distribution identical to the OpenMP static scheduling policy but with dynamic load balancing.
\end{itemize}
\end{itemize}

\subsection{Return value}
\paragraph{}
Refer to the \textit{Error codes} section.

\subsection{Example}
\paragraph{}
\begin{small}
\begin{lstlisting}[frame=tb]
  PROGRAM MAIN
    INTEGER*4 E

    ...
    E = KAAPIF_INIT(1)
    E = KAAPIF_SET_POLICY(KAAPIF_POLICY_ADAPTIVE)
    E = KAAPIF_FOREACH(...)
    ...

  END PROGRAM MAIN
\end{lstlisting}
\end{small}


\newpage
\section{Setting grains}

\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
INTEGER*4 FUNCTION KAAPIF_SET_GRAINS
(
  INTEGER*4 PAR_GRAIN,
  INTEGER*4 SEQ_GRAIN
)
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
\textit{KAAPIF\_SET\_GRAINS} sets the adaptive loop grains. Grains are used
to amortize the act of extracting work for both the parallel (ie. during a
steal) and sequential (ie. during a pop) executions. Guessing those grains
is problem specific. The general intuition is that they should be inversely
propotionnal to a single task processing time (given a task processing time
is constant across execution). The parallel grain should be greater than the
sequential grain, since a steal operation requires a bigger task to be amortized.

\paragraph{}
Note that this routine sets global variables used by the next subsequent call to
\textit{KAAPIF\_FOREACH} family functions. 

\subsection{Parameters}
\begin{itemize}
\item \textit{PAR\_GRAIN}: below this size, a task cannot be split for
subsequent parallel execution. default to 32.
\item \textit{SEQ\_GRAIN}: the size used by the sequential execution to
extract work from its local workqueue. default to 16.
\end{itemize}

\subsection{Return value}
\paragraph{}
Refer to the \textit{Error codes} section.

\subsection{Example}
\paragraph{}
\begin{small}
\begin{lstlisting}[frame=tb]
  PROGRAM MAIN
    INTEGER*4 E

    ...
    E = KAAPIF_INIT(1)
    E = KAAPIF_SET_GRAINS(32, 32)
    E = KAAPIF_FOREACH(...)
    ...

  END PROGRAM MAIN
\end{lstlisting}
\end{small}


\newpage
\section{Control of the distribution of iterations}

\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
INTEGER*4 FUNCTION KAAPIF_SET_DISTRIBUTION
(
  INTEGER*4 LEVEL,
  INTEGER*4 STRICT
)
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
\textit{KAAPIF\_SET\_DISTRIBUTION} sets the distribution for the next independent loop among a "hierarchical level".

The hierarchical levels defined in the fortran interface are :
\begin{description}
\item [MACHINE]: the loop iterations are distributed among all the threads of the enclosed parallel region.
\item [NUMA]: the loop iterations are distributed among the NUMA node of the enclosed parallel region.
\end{description}
If STRICT is non zero then the distribution will strictly respect to description above. Else some threads outside the set of threads defined by the distribution may steal some work, according to the scheduling policy.

\paragraph{}
Note that this routine sets global variables used by the next subsequent call to
\textit{KAAPIF\_FOREACH} family functions. 

\subsection{Parameters}
\begin{itemize}
\item \textit{LEVEL}: one of the value: 
\begin{itemize}
\item 2 or \textit{KAAPIF\_LD\_NUMA} (defined in kaapif.inc, kaapif.inc.f90 or kaapif.inc.f95) 
\item 5 or  \textit{KAAPIF\_LD\_MACHINE}
\end{itemize}
\item \textit{STRICT}: 0 if the distribution is not strict, else the distribution is strict among the set of threads defined by the hierarchical level.
\end{itemize}

\subsection{Return value}
\paragraph{}
Refer to the \textit{Error codes} section.

\subsection{Example}
\paragraph{}
\begin{small}
\begin{lstlisting}[frame=tb]
  PROGRAM MAIN
    INTEGER*4 E

    ...
    E = KAAPIF_INIT(1)
    E = KAAPIF_SET_DISTRIBUTION(KAAPIF_LD_NUMA, 1)
    E = KAAPIF_FOREACH(...)
    ...

  END PROGRAM MAIN
\end{lstlisting}
\end{small}


\newpage
\section{Control of the affinity of threads/iterations}

\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
INTEGER*4 FUNCTION KAAPIF_SET_AFFINITY
(
  INTEGER*4 LEVEL,
  INTEGER*4 STRICT
)
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
\textit{KAAPIF\_SET\_AFFINITY} allows to control which threads in the parallel  region are allowed to perform the iterations of the loop.

The level correspond to a hierarchical levels defined in the fortran interface:
\begin{description}
\item [MACHINE]: any threads of the machine are able to perform iterations,
\item [NUMA]:  all the threads that share the same NUMA node of the running thread may perform iterations.
\end{description}
If STRICT is non zero then the affinity will strictly respect to above description. Else some threads outside the set of threads defined by the affinity may steal some work, according to the scheduling policy.

\paragraph{}
Note that this routine sets global variables used by the next subsequent call to
\textit{KAAPIF\_FOREACH} family functions. 

\subsection{Parameters}
\begin{itemize}
\item \textit{LEVEL}: one of the value: 
\begin{itemize}
\item 2 or \textit{KAAPIF\_LD\_NUMA} (defined in kaapif.inc, kaapif.inc.f90 or kaapif.inc.f95) 
\item 5 or  \textit{KAAPIF\_LD\_MACHINE}
\end{itemize}
\item \textit{STRICT}: 0 if the affinity is not strict, else the affinity is strict among the set of threads.
\end{itemize}

\subsection{Return value}
\paragraph{}
Refer to the \textit{Error codes} section.

\subsection{Example}
\paragraph{}
\begin{small}
\begin{lstlisting}[frame=tb]
  PROGRAM MAIN
    INTEGER*4 E

    ...
    E = KAAPIF_INIT(1)
    E = KAAPIF_SET_AFFINITY(KAAPIF_LD_NUMA, 1)
    E = KAAPIF_FOREACH(...)
    ...

  END PROGRAM MAIN
\end{lstlisting}
\end{small}


\newpage
\section{Dataflow programming}
\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
INTEGER*4 FUNCTION KAAPIF_SPAWN
(
  INTEGER*4 NARGS,
  BODY,
  ...
)
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
Create a new computation task implemented by the function \textit{BODY}.

\paragraph{}
\textit{BODY} is called with the user specified arguments, there is no
argument added by XKAAPI:\\
\begin{small}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
SUBROUTINE BODY(ARG0, ARG1, ...)
\end{lstlisting}
\end{small}

\paragraph{}
Each task parameter is described by 4 successive arguments including:
\begin{itemize}
\item the argument \textit{VALUE},
\item the parameter \textit{TYPE},
\item the element \textit{COUNT},
\item the access \textit{MODE}.
\end{itemize}

\paragraph{}
\textit{TYPE} is one of the following:
\begin{itemize}
\item KAAPIF\_TYPE\_CHAR=0,
\item KAAPIF\_TYPE\_INT=1,
\item KAAPIF\_TYPE\_REAL=2,
\item KAAPIF\_TYPE\_DOUBLE=3.
\end{itemize}

\paragraph{}
If a parameter is an array, \textit{COUNT} must be set to the array size.
For a scalar value, it must be set to 1.

\paragraph{}
\textit{MODE} is one of the following:
\begin{itemize}
\item KAAPIF\_MODE\_R=0 for a read access,
\item KAAPIF\_MODE\_W=1 for a write access,
\item KAAPIF\_MODE\_RW=2 for a read write access,
\item KAAPIF\_MODE\_V=3 for a parameter passed by value.
\end{itemize}

\subsection{Parameters}
\begin{itemize}
\item \textit{NARGS}: the argument count.
\item \textit{BODY}: the task body.
\item \textit{...}: the \textit{VALUE}, \textit{TYPE}, \textit{COUNT}, \textit{MODE} tuple list.
\end{itemize}

\subsection{Return value}
\paragraph{}
Refer to the \textit{Error codes} section.

\subsection{Example}
\paragraph{}
Refer to examples/kaapif/dfg

\begin{small}
\begin{lstlisting}[frame=tb]
  ! computation task entry point
  SUBROUTINE OP(A, B)
    ! task user specific code
    ...
    RETURN
  END

  PROGRAM MAIN
    INTEGER*4 E
    ...
    ! spawn a task implemented by the OP routine
    E = KAAPIF_SPAWN(2, OP,
    ! argument[0]
     &                42
     &                KAAPIF_TYPE_DOUBLE,
     &                1,
     &                KAAPIF_MODE_V,
    ! argument[1]
     &                42,
     &                KAAPIF_TYPE_DOUBLE,
     &                1,
     &                KAAPIF_MODE_V)
    ...
  END PROGRAM MAIN
\end{lstlisting}
\end{small}


\newpage
\section{Parallel regions}

\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
INTEGER*4 FUNCTION KAAPIF_BEGIN_PARALLEL()
INTEGER*4 FUNCTION KAAPIF_END_PARALLEL(INTEGER*4 FLAGS)
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
\textit{KAAPIF\_BEGIN\_PARALLEL} and \textit{KAAPIF\_END\_PARALLEL} mark the
start and the end of a parallel region. Regions are used to wakeup and suspend
the \kaapi system threads so they avoid disturbing the application when idle.
This is important if another parallel library is being used. Wether threads
are suspendable or not is controlled according by the KAAPIF\_INIT parameter.

\subsection{Parameters}
\begin{itemize}
\item \textit{FLAGS}: if zero, an implicit synchronization is inserted before
leaving the region.
\end{itemize}

\subsection{Return value}
\paragraph{}
Refer to the \textit{Error codes} section.

\subsection{Example}
\paragraph{}
Refer to examples/kaapif/dfg\\
\begin{small}
\begin{lstlisting}[frame=tb]
  PROGRAM MAIN
    INTEGER*4 E

    ...
    E = KAAPIF_BEGIN_PARALLEL()
    ...
    E = KAAPIF_END_PARALLEL(1)
    ...

  END PROGRAM MAIN
\end{lstlisting}
\end{small}


\newpage
\section{Synchronization}

\subsection{Synopsis}
\begin{small}
\lstset{commentstyle=\color{blue}}
\lstset{language=C}
\begin{lstlisting}[frame=tb]
INTEGER*4 FUNCTION KAAPIF_SYNC()
\end{lstlisting}
\end{small}

\subsection{Description}
\paragraph{}
Synchronize the sequential with the parallel execution flow. When this routine
returns, every computation task has been executed and memory is consistent for
the processor executing the sequential flow.

\subsection{Return value}
\paragraph{}
Refer to the \textit{Error codes} section.

\subsection{Example}
\paragraph{}
Refer to examples/kaapif/dfg\\
\begin{small}
\begin{lstlisting}[frame=tb]
  PROGRAM MAIN
    INTEGER*4 E

    ...
    E = KAAPIF_SYNC()
    ...

  END PROGRAM MAIN
\end{lstlisting}
\end{small}


\newpage
\section{Error codes}
\paragraph{}
When indicated, a routine may return one of the following error code:
\begin{itemize}
\item KAAPIF\_SUCCESS=0: success
\item KAAPIF\_ERR\_FAILURE=-1: generic error code
\item KAAPIF\_ERR\_EINVAL=-2: invalid argument
\item KAAPIF\_ERR\_UNIMPL=-3: feature not implemented
\end{itemize}


\end{document}
