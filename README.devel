Note to run (OpenMP) program in order to generate trace files from
OpenMP program.
***********************

I. Compilation
==============

This project must be configured with --with-perfcounter in order to capture events at runtime
to generate trace of the execution.
The trace contains:
- scheduling events,
- computing events : begin, end of task execution, memory access made by the task, successors task
- performance events : counter per task may be recorded after the end of execution.
- specific events for OpenMP execution (begn/end) of parallel regions, barriers, etc

Typical command ligne to configure the library is:
  > configure --prefix=<your installation prefix> --with-perfcounter --enable-mode=release

It could be interesting to capture PAPI events related to some point or state in the execution.
  > configure --prefix=<your installation prefix> --with-perfcounter --enable-mode=release \
      --with-papi=<papi installation prefix>

See INSTALL for instruction about how to compile and install X-Kaapi.


II. Trace generation:
=============

Once xkaapi is correctly configured and installed, they are several environement variables that
helps to specify which events are captured or which performance counters are recorded :

* KAAPI_RECORD_TRACE=1 (in fact not 0): activate the record of events in the repository /tmp.
Once file is created per thread and it is named /tmp/events.<username>.0.<threadid>.evt.
If KAAPI_RECORD_TRACE=0 then the record of events is deactivated.

* KAAPI_RECORD_MASK= : list of events or group of events.
Typical interesting groups for the purpose of generating trace for simulation is :
      KAAPI_RECORD_MASK=compute,komp,perfcounter
Individual event numbers can be given to KAAPI_RECORD_MASK. Please read kaapi_event.h

* KAAPI_TASKPERF_EVENTS= : list of events or groups of events for capturing execution caracteristic
of each tasks. The most usefull events are :
    - work: the total number seconds required to execute a task. Summed among the sub tasks.
    - time: the parallel time in seconds to execute a task. This event is required to capture
    the execution time of task which is the metric for generating the 'cost' field in the
    simulation trace.
    - any papy related events name, e.g. PAPI_TOTAL_CYC, PAPI_TOT_INS, ...

For instance:
    KAAPI_TASKPERF_EVENTS=TIME,PAPI_LD_INS,PAPI_SR_INS,PAPI_TOT_INS

Launching the KASTORS dpotrf_taskdep on the first 8 cores of a machine :
 > N=8; KAAPI_DUMP_GRAPH=1 KAAPI_RECORD_TRACE=1 KAAPI_RECORD_MASK=komp,compute  \
    KAAPI_TASKPERF_EVENTS=TIME,PAPI_LD_INS,PAPI_SR_INS,PAPI_TOT_INS\
    OMP_DISPLAY_ENV=true OMP_NUM_THREADS=$N OMP_PLACES="cores($N)" \
  ~/xkaapi/install/bin/komp-run  -d ./dpotrf_taskdep -n 4096 -b 256 -i 1


III. Trace conversion
=============

During set 1 of the library compilation and installation, the katracereader utility is installed
in repository <prefix>/bin.
The purpose of katracereader is to convert trace fron internal binary representation
to a human readable version.
From the set of files /tmp/events.<username>.0.<threadid>.evt it is possible to :
  - have access to some information about the trace, including the number of cores,
  the name of the tasks.
    > katracereader [-h] /tmp/events.<username>.0.*
  - generate a human readable dump of the file:
    > katracereader --display-data /tmp/events.<username>.0.*
    Note that each events are timestamped by the date of its generation (clock tick).
  - generate that Vite application may display :
    > katracereader --vite /tmp/events.<username>.0.*
  - generate a .c files for the CORSE team simulation tool.
    > katracereader --rastello /tmp/events.<username>.0.*



