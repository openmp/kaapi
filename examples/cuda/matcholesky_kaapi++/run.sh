#!/bin/bash

export LD_LIBRARY_PATH=$HOME/install/xkaapi/default/lib:$LD_LIBRARY_PATH

function run_test {
    export KAAPI_CPUSET="0"
#    export KAAPI_CPUCOUNT=2
    export KAAPI_GPUSET="0~4"

#    export KAAPI_CPUSET="0,5,6,11"
#    export KAAPI_GPUSET="0~1,1~2,2~3,3~4"
#    export KAAPI_GPUSET="0~1,1~2,2~3,3~4,4~7,5~8,6~9,7~10"

    # beagle
#    export KAAPI_CPUSET="0:1"
#    export KAAPI_CPUCOUNT=2
#    export KAAPI_GPUSET="0~6,1~7"

#    export KAAPI_CPUSET="0:1"
#    export KAAPI_GPUSET="0~2,1~3"

#    export KAAPI_RECORD_TRACE=1
#    export KAAPI_RECORD_MASK="COMPUTE,IDLE"
#    export KAAPI_RECORD_MASK="COMPUTE,IDLE,STEAL"

#    export KAAPI_WSSELECT="hwsg"
#    export KAAPI_CUDA_PEER=1

#    export KAAPI_DUMP_GRAPH=1
#    export KAAPI_DOT_NOVERSION_LINK=1
#    export KAAPI_DOT_NODATA_LINK=1
#    export KAAPI_DOT_NOLABEL_INFO=1
#    export KAAPI_DOT_NOACTIVATION_LINK=1
#    export KAAPI_DOT_NOLABEL=1

#    export KAAPI_DISPLAY_PERF=1

#     export KAAPI_PERFMODEL_CALIBRATE=1
#     export KAAPI_PERFMODEL_TRANSFER="idgraf"
#     export KAAPI_PERFMODEL_TRANSFER="history"
#     export KAAPI_PERFMODEL_TRANSFER="null"
#    export KAAPI_SCHED="gdual"
#    export KAAPI_GDUAL_STEAL=1
#    export KAAPI_SCHED="heft"
#    export KAAPI_HEFT_STEAL=1

#    export KAAPI_SCHED="daws"
#    export KAAPI_DAWS_ALPHA=1.0
#    export KAAPI_DAWS_BETA=20.0

#    export KAAPI_SCHED="comm"
#    export KAAPI_SCHED="writer"
#    export KAAPI_SCHED="locality"


    execfile="./matcholesky_kaapi++"
    msizes="4096"
    bsizes="512"
    niter=1
#    verif=1
    export KAAPI_CUDA_WINDOW_SIZE=2
    for m in $msizes ; do
	    for b in $bsizes; do
	    for i in `seq 1 $niter`
	    do
				echo "$KAAPI_CPUSET $KAAPI_GPUSET ($KAAPI_SCHED) $execfile $m $b $verif"
#	   KAAPI_STACKSIZE_MASTER=536870912 gdb $execfile 
	   KAAPI_STACKSIZE_MASTER=536870912 $execfile $m $b $verif 
	    done
	done
    done
}

run_test
exit 0
