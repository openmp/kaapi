
#include <stdio.h>
#include <unistd.h>
#include "kaapic.h"

#include <cuda_runtime_api.h>

void body(int n, int* result)
{
  fprintf(stdout, "TaskCPU\n");
  fflush(stdout);
  *result = n;
}

__global__ void setone(int *res, int n)
{
  *res = n;
}

void cuda_body(int n, int* result)
{
  fprintf(stdout, "TaskGPU ptr=%p\n", result);
  fflush(stdout);
  setone<<<1,1>>>(result, n);
//  cudaMemset(result, 0, sizeof(int));
}

int main()
{
  int result;
  int result2;
  kaapic_spawn_attr_t attr;
  
  kaapic_init(KAAPIC_START_ONLY_MAIN);

  kaapic_begin_parallel(KAAPIC_FLAG_STATIC_SCHED);

  /* initialize default attribut */
  kaapic_spawn_attr_init(&attr);
  
  kaapic_spawn_attr_set_alternative_body(&attr, (void (*)())cuda_body);
  kaapic_spawn_attr_set_arch(&attr, KAAPIC_ARCH_GPU_ONLY);
  
  /* spawn the task */
  kaapic_spawn(&attr, 
      2,     /* number of arguments */
      (void (*)())body,  /* the entry point for the task */
      KAAPIC_MODE_V, KAAPIC_TYPE_INT, 1, (int)129,
      KAAPIC_MODE_W, KAAPIC_TYPE_INT, 1, &result
  );
  kaapic_spawn(&attr,
               2,     /* number of arguments */
               (void (*)())body,  /* the entry point for the task */
               KAAPIC_MODE_V, KAAPIC_TYPE_INT, 1, (int)129,
               KAAPIC_MODE_W, KAAPIC_TYPE_INT, 1, &result2
               );

  kaapic_end_parallel(KAAPIC_FLAG_STATIC_SCHED);

  printf("The result is : %i and %i\n", result, result2 );
  
  kaapic_finalize();
  return 0;
}
