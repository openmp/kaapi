#!/bin/bash

export LD_LIBRARY_PATH=$HOME/install/xkaapi/default/lib:$LD_LIBRARY_PATH

function run_test {
#    export KAAPI_CPUSET="0"
#    export KAAPI_GPUSET="0~1"

    export KAAPI_CPUSET="0,5,6,11"
    #    export KAAPI_GPUSET="0~1,1~2,2~3,3~4"
    export KAAPI_GPUSET="0~1,1~2,2~3,3~4,4~7,5~8,6~9,7~10"

#    export KAAPI_RECORD_TRACE=1
#    export KAAPI_RECORD_MASK="COMPUTE,IDLE"

#    export KAAPI_DUMP_GRAPH=1
#    export KAAPI_DOT_NOVERSION_LINK=1
#    export KAAPI_DOT_NODATA_LINK=1
#    export KAAPI_DOT_NOLABEL_INFO=1
#    export KAAPI_DOT_NOACTIVATION_LINK=1
#    export KAAPI_DOT_NOLABEL=1

#    export KAAPI_DISPLAY_PERF=1
#
    export KAAPI_SCHED="writer"
#    export KAAPI_SCHED="comm"
#    export KAAPI_SCHED="locality"

#  export KAAPI_WSSELECT="hwsg"
    export KAAPI_CUDA_PEER=1

    execfile="./matcholesky_rec_bloc_kaapi++"
    msizes="12288"
    bsizes="1024"
    subblock="128"
    niter=2
#    verif=1
    export KAAPI_CUDA_WINDOW_SIZE=2
    for m in $msizes ; do
	    for b in $bsizes; do
	    for i in `seq 1 $niter`
	    do
	    echo "$KAAPI_CPUSET $KAAPI_GPUSET \
		    $execfile $m $b  $subblock $verif"
	    KAAPI_STACKSIZE_MASTER=536870912 $execfile $m $b $subblock $verif 
	    done
	done
    done
}

run_test
exit 0
