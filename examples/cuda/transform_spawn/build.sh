#!/bin/bash

XKAAPIDIR=$HOME/install/xkaapi/default
#ARCH_GPU="sm_20" # Fermi GPUs
ARCH_GPU="sm_30" # Kepler GPUs

CFLAGS="-I$XKAAPIDIR/include"
LDFLAGS="-L$XKAAPIDIR/lib -lkaapi -lkaapi++"

nvcc -w -g --machine 64 -arch $ARCH_GPU --compiler-options "$CFLAGS" \
    transform.cu \
    -o transform \
    $LDFLAGS 
