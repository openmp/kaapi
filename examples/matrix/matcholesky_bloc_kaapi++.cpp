/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** fabien.lementec@gmail.com / fabien.lementec@imag.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include <iostream>
#include <iomanip>
#include <string>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <stdlib.h>

extern "C" {
#if defined(MKL)
#include <mkl_cblas.h>   // 
#include <mkl_lapacke.h> // assume MKL/ATLAS clapack version
#else
#include <cblas.h>   // 
#include <clapack.h> // assume MKL/ATLAS clapack version
#endif
}

#include "kaapi++" // this is the new C++ interface for Kaapi

/* Compute inplace LLt factorization of A, ie L such that A = L * Lt
   with L lower triangular.
*/
struct TaskDPOTRF: public ka::Task<3>::Signature<
  CBLAS_UPLO,                  /* upper / lower */
  ka::RW<double>,   /* A */
  size_t /* NB */
>{};
template<>
struct TaskBodyCPU<TaskDPOTRF> {
  void operator()( 
    CBLAS_UPLO uplo, ka::pointer_rw<double> A, int NB
  )
  {
    const size_t n     = NB;
    const size_t lda   = NB;
    double* const a = A;
#if defined(MKL)
    LAPACKE_dpotrf( LAPACK_ROW_MAJOR, (uplo==CblasUpper ? 'u' : 'l'), n, a, lda );
#else
    clapack_dpotrf(
      CblasRowMajor, uplo, n, a, lda
    );
#endif
  }
};


/* DTRSM
*/
struct TaskDTRSM: public ka::Task<8>::Signature<
      CBLAS_SIDE,                  /* side */
      CBLAS_UPLO,                  /* uplo */
      CBLAS_TRANSPOSE,             /* transA */
      CBLAS_DIAG,                  /* diag */
      double,                      /* alpha */
      ka::R<double>, /* A */
      ka::RW<double>, /* B */
      size_t /* NB */
>{};
template<>
struct TaskBodyCPU<TaskDTRSM> {
  void operator()( 
    CBLAS_SIDE             side,
    CBLAS_UPLO             uplo,
    CBLAS_TRANSPOSE        transA,
    CBLAS_DIAG             diag,
    double                 alpha,
    ka::pointer_r<double> A, 
    ka::pointer_rw<double> C,
    size_t NB
  )
  {
    const double* const a = A;
    const size_t lda = NB;

    double* const c = C;
    const size_t ldc = NB;

    const size_t n = NB;
    const size_t k = NB;

    cblas_dtrsm
    (
      CblasRowMajor, side, uplo, transA, diag,
      n, k, alpha, a, lda, c, ldc
    );
  }
};


/* Rank k update
*/
struct TaskDSYRK: public ka::Task<7>::Signature<
  CBLAS_UPLO,                  /* CBLAS Upper / Lower */
  CBLAS_TRANSPOSE,             /* transpose flag */
  double,                      /* alpha */
  ka::R<double>, /* A */
  double,                      /* beta */
  ka::RW<double>, /* C */
  size_t /* NB */
>{};
template<>
struct TaskBodyCPU<TaskDSYRK> {
  void operator()(
    CBLAS_UPLO uplo,
    CBLAS_TRANSPOSE trans,
    double alpha,
    ka::pointer_r <double>  A, 
    double beta,
    ka::pointer_rw<double> C,
    size_t NB
  )
  {
    const size_t n     = NB;
    const size_t k     = NB;
    const size_t lda   = NB;
    const double* const a = A;

    const size_t ldc   = NB;
    double* const c = C;

    cblas_dsyrk
    (
      CblasRowMajor, uplo, trans,
      n, k, alpha, a, lda, beta, c, ldc
    );

  }
};


/* DGEMM rountine to compute
    Aij <- alpha* Aik * Akj + beta Aij
*/
struct TaskDGEMM: public ka::Task<8>::Signature<
      CBLAS_TRANSPOSE,             /* NoTrans/Trans for A */
      CBLAS_TRANSPOSE,             /* NoTrans/Trans for B */
      double,                      /* alpha */
      ka::R<double>,               /* Aik   */
      ka::R<double>,               /* Akj   */
      double,                      /* beta */
      ka::RW<double>,              /* Aij   */
      size_t                          /* NB */
>{};
template<>
struct TaskBodyCPU<TaskDGEMM> {
  void operator()
  (
    CBLAS_TRANSPOSE transA,
    CBLAS_TRANSPOSE transB,
    double alpha,
    ka::pointer_r<double> Aik,
    ka::pointer_r<double> Akj,
    double beta,
    ka::pointer_rw<double> Aij,
    size_t NB
  )
  {
    const double* const a = Aik;
    const double* const b = Akj;
    double* const c       = Aij;

    const size_t m = NB;
    const size_t n = NB;
    const size_t k = NB;

    const size_t lda = NB;
    const size_t ldb = NB;
    const size_t ldc = NB;

    cblas_dgemm
    (
      CblasRowMajor, transA, transB,
      m, n, k, alpha, a, lda, b, ldb, beta, c, ldc
    );
  }
};


/* Generate a random matrix symetric definite positive matrix of size m x m 
   - it will be also interesting to generate symetric diagonally dominant 
   matrices which are known to be definite postive.
*/
#define DIM 128

static void generate_bloc_seq(double* A, size_t ig, size_t jg, int bc, size_t NB, size_t m)
{
  for (size_t i = ig*NB ; i< (ig+1)*NB-1; ++i)
  {
    for (size_t j = jg*NB; j< (jg+1)*NB-1; ++j)
      A[(i%NB)*NB+j%NB] = 1.0 / (1.0+i+j);
    if (ig == jg)
      A[(i%NB)*NB+i%NB] = m*1.0;
  }
}


/* DGEMM rountine to compute
    Aij <- alpha* Aik * Akj + beta Aij
*/
struct TaskGenMat: public ka::Task<6>::Signature<
      ka::RW<double>,              /* Aij   */
      size_t,                      /* ig */
      size_t,                      /* jg */
      size_t,                      /* bc */
      size_t,                      /* NB */
      size_t                       /* m  */
>{};
template<>
struct TaskBodyCPU<TaskGenMat> {
  void operator()
  (
    ka::pointer_rw<double> Aij,
    size_t ig,
    size_t jg,
    size_t bc,
    size_t NB,
    size_t m
  )
  {
    double* const A  = Aij;
    generate_bloc_seq( A, ig, jg, bc, NB, m);
  }
};


static void generate_matrix(double* A[DIM][DIM], size_t bc, size_t NB, size_t m)
{
#if 0
  for (size_t i = 0; i< m; ++i)
  {
    for (size_t j = 0; j< m; ++j)
      A[j/NB][i/NB][(i%NB)*NB+j%NB] = 1.0 / (1.0+i+j);
    A[i/NB][i/NB][(i%NB)*NB+i%NB] = m*1.0;
  }
#else
  for (size_t ig = 0; ig< bc; ++ig)
    for (size_t jg = 0; jg< bc; ++jg)
      ka::Spawn<TaskGenMat>()( A[ig][jg], ig, jg, bc, NB, m );
  ka::Sync();
#endif
}


/*
*/
#if 0
static void convert_to_blocks(size_t NB, size_t N, double *Alin, double* A[DIM][DIM] )
{
  for (size_t i = 0; i < N; i++)
  {
    for (size_t j = 0; j < N; j++)
    {
      A[j/NB][i/NB][(i%NB)*NB+j%NB] = Alin[i*N+j];
    }
  }
}
#endif

/* Block Cholesky factorization A <- L * L^t
   Lower triangular matrix, with the diagonal, stores the Cholesky factor.
*/
struct TaskCholesky: public ka::Task<3>::Signature<
      int,       /* N */
      int,       /* NB */
      uintptr_t  /* A */
>{};
template<>
struct TaskBodyCPU<TaskCholesky> {
  void operator()( /*const ka::StaticSchedInfo* schedinfo,*/ size_t N, size_t NB, uintptr_t ptr )
  {
    typedef double* TYPE[DIM][DIM]; 
    TYPE* A = (TYPE*)ptr;
#define USE_DIST 0
#if (USE_DIST ==1)    
    /* assume a square grid of ressources composed by CPU and GPU */
    size_t nressources = (schedinfo->count_cpu()+schedinfo->count_gpu());
    size_t sqrtnressources = sqrt(nressources);
    
#if 1
    /* for the block version of Cholesky, the matrix is a NxN matrix of blocks of size NBxNB,
       thus we assume to distribute the block using a blocksize of =1 in each direction
    */
    ka::Distribution2D<ka::BlockCyclic2D> dist( 
          ka::SetnCPU(schedinfo->count_cpu()) | ka::SetnGPU(schedinfo->count_gpu()), 
          ka::BlockCyclic2D(sqrtnressources, 1, sqrtnressources, 1 )
    );
#else
    ka::Distribution2D<ka::BlockCyclic2D> dist( 
          ka::SetnCPU(schedinfo->count_cpu()) | ka::SetnGPU(schedinfo->count_gpu()), 
          ka::BlockCyclic2D(1, 8, nressources, 1 )
    );
#endif

#  define SITE(x) ka::SetSite(x)
#else
#  define SITE(x) 
#endif


#if (USE_DIST ==1)
    std::cout << std::endl << "Distribution: " << std::endl;
    for (size_t i=0; i < N; ++i)
    {
      for (size_t j=0; j < N; ++j)
      {
        std::cout << dist.map_ressource(i,j) << " ";
      }
      std::cout << std::endl;
    }
#endif

    for (size_t k=0; k < N; ++k)
    {
      ka::Spawn<TaskDPOTRF>( /* SITE(dist(k,k))*/ )( CblasLower, (*A)[k][k], NB );

      for (size_t m=k+1; m < N; ++m)
      {
        //ka::Spawn<TaskDTRSM>(SITE(dist(m,k)))(CblasRight, CblasLower, CblasTrans, CblasNonUnit, 1.0, (*A)[k][k], (*A)[m][k], NB);
        ka::Spawn<TaskDTRSM>()(CblasRight, CblasLower, CblasTrans, CblasNonUnit, 1.0, (*A)[k][k], (*A)[m][k], NB);
      }

      for (size_t m=k+1; m < N; ++m)
      {
        //ka::Spawn<TaskDSYRK>(SITE(dist(m,m)))( CblasLower, CblasNoTrans, -1.0, (*A)[m][k], 1.0, (*A)[m][m], NB);
        ka::Spawn<TaskDSYRK>()( CblasLower, CblasNoTrans, -1.0, (*A)[m][k], 1.0, (*A)[m][m], NB);
        for (size_t n=k+1; n < m; ++n)
        {
          //ka::Spawn<TaskDGEMM>(SITE(dist(m,n)))(CblasNoTrans, CblasTrans, -1.0, (*A)[m][k], (*A)[n][k], 1.0, (*A)[m][n], NB);
          ka::Spawn<TaskDGEMM>()(CblasNoTrans, CblasTrans, -1.0, (*A)[m][k], (*A)[n][k], 1.0, (*A)[m][n], NB);
        }
      }
    }
  }
};



extern "C" {
int kaapi_numa_bind(uint32_t node, const void* addr, size_t size);
uint32_t kaapi_numa_getpage_id(const void* addr);
}

int fmap( int i, int j)
{
  return 6*(i%2) + (j%6);
}

int fmap2( int i, int j)
{
  return 3*((i/3)%4) + (j/4)%3;
}

/* Main of the program
*/
struct doit {
  void doone_exp( size_t n, size_t block_count, int niter, int verif )
  {
    size_t NB = n / block_count;
    
    //n = block_count * global_blocsize;

    double t0, t1;

    // blocked matrix
    double* dAbloc[DIM][DIM];
    for (size_t i = 0; i < block_count; i++)
    {
      for (size_t j = 0; j < block_count; j++)
      {
       dAbloc[i][j] = 0;
       if (posix_memalign( (void**)&(dAbloc[i][j]), 4096, NB*NB*sizeof(double)) !=0)
       { std::cout << "cannot allocate memory" << std::endl; return; }
       uint32_t node = fmap2(i,j);
       if (kaapi_numa_bind( node, dAbloc[i][j], NB*NB*sizeof(double)) != 0)
       {
         std::cout << "Error cannot bind addr: " << dAbloc[i][j] << " on node " << (i+j)%8 << std::endl;
         return;
       }
       std::cout << std::setw(3) << node;
       dAbloc[i][j][0] = 0; 
      }
      std::cout << std::endl;
    }
    std::cout << "Mapping:" << std::endl;
    for (size_t i = 0; i < block_count; i++)
    {
      for (size_t j = 0; j < block_count; j++)
        std::cout << std::setw(3) << kaapi_numa_getpage_id(dAbloc[i][j]);
      std::cout << std::endl;
    }


    // Cholesky factorization of A 
    double sumt = 0.0;
    double sumgf = 0.0;
    double sumgf2 = 0.0;
    double sd;
    double sumt_exec = 0.0;
    double sumgf_exec = 0.0;
    double sumgf2_exec = 0.0;
    double sd_exec;
    double gflops;
    double gflops_max = 0.0;
    double gflops_exec;
    double gflops_exec_max = 0.0;

    /* formula used by plasma in time_dpotrf.c */
    double fp_per_mul = 1;
    double fp_per_add = 1;
    double fmuls = (n * (1.0 / 6.0 * n + 0.5 ) * n);
    double fadds = (n * (1.0 / 6.0 * n ) * n);
        

    generate_matrix( dAbloc, block_count, NB, n );
    std::cout << "Mapping after generate:" << std::endl;
    for (size_t i = 0; i < block_count; i++)
    {
      for (size_t j = 0; j < block_count; j++)
        std::cout << std::setw(3) << kaapi_numa_getpage_id(dAbloc[i][j]);
      std::cout << std::endl;
    }

    printf("%6d %5d %5d ", (int)n, (int)kaapi_get_concurrency(), (int)(n/NB) );


    for (int i=0; i<niter; ++i)
    {
#if 0
{ // display the memory mapping
      std::cout << "Memory mapping for A:" << std::endl;
      for (size_t i=0; i<DIM; ++i)
      {
        for (size_t j=0; j<DIM; ++j)
        {
           double* addr = dAbloc[i][j];
           int node = kaapi_numa_get_page_node( addr );
           //std::cout << " @:" << addr  << " " << node << ",  ";
           std::cout << node << ",  ";
        }
        std::cout << std::endl;
      }
}
#endif
      t0 = kaapi_get_elapsedtime();
      ka::Spawn<TaskCholesky>( ka::SetStaticSched() )(block_count, NB, (uintptr_t)&dAbloc);
      ka::Sync();
      t1 = kaapi_get_elapsedtime();

      gflops = 1e-9 * (fmuls * fp_per_mul + fadds * fp_per_add) / (t1-t0);
      if (gflops > gflops_max) gflops_max = gflops;
//printf("Gflops= %9.3f\n", gflops );

      sumt += double(t1-t0);
      sumgf += gflops;
      sumgf2 += gflops*gflops;

      gflops_exec = 1e-9 * (fmuls * fp_per_mul + fadds * fp_per_add) / 1.0;
      if (gflops_exec > gflops_exec_max) gflops_exec_max = gflops_exec;
      
      sumt_exec += double(1.0);
      sumgf_exec += gflops_exec;
      sumgf2_exec += gflops_exec*gflops_exec;
    }

    gflops = sumgf/niter;
    gflops_exec = sumgf_exec/niter;
    if (niter ==1) 
    {
      sd = 0.0;
      sd_exec = 0.0;
    } else {
      sd = sqrt((sumgf2 - (sumgf*sumgf)/niter)/niter);
      sd_exec = sqrt((sumgf2_exec - (sumgf_exec*sumgf_exec)/niter)/niter);
    }
    
    printf("%9.3f %9.3f %9.3f %9.3f %9.3f %9.3f %9.3f %9.3f\n", 
        sumt/niter, 
        gflops, 
        sd, 
        sumt_exec/niter, 
        gflops_exec, 
        sd_exec, 
        gflops_max, 
        gflops_exec_max );

    for (size_t i = 0; i < block_count; i++)
      for (size_t j = 0; j < block_count; j++)
       free(dAbloc[i][j]);
  }

  void operator()(int argc, char** argv )
  {
    // matrix dimension
    size_t n = 32;
    if (argc > 1)
      n = atoi(argv[1]);

    // block count
    size_t block_count = 2;
    if (argc > 2)
      block_count = atoi(argv[2]);
      
    // Number of iterations
    int niter = 1;
    if (argc >3)
      niter = atoi(argv[3]);

    // Make verification ?
    int verif = 0;
    if (argc >4)
      verif = atoi(argv[4]);
    
    //size_t nmax = n+16*incr;
    printf("size   #threads #bs    time      GFlop/s   Deviation\n");
    for (int k=0; k<niter; ++k )
    {
      //doone_exp( 1<<k, block_count, niter, verif );
      doone_exp( n, block_count, niter, verif );
    }
  }

};


/* main entry point : Kaapi initialization
*/
int main(int argc, char** argv)
{
  try {
    /* Join the initial group of computation : it is defining
       when launching the program by a1run.
    */
    ka::Community com = ka::System::join_community( argc, argv );
    
    /* Start computation by forking the main task */
    ka::SpawnMain<doit>()(argc, argv); 
    
    /* Leave the community: at return to this call no more athapascan
       tasks or shared could be created.
    */
    com.leave();

    /* */
    ka::System::terminate();
  }
  catch (const std::exception& E) {
    ka::logfile() << "Catch : " << E.what() << std::endl;
  }
  catch (...) {
    ka::logfile() << "Catch unknown exception: " << std::endl;
  }
  
  return 0;
}
