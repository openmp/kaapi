/*
 ** xkaapi
 ** 
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 ** thierry.gautier@inrialpes.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
 
#define TEST_MOVE 1
//#define TEST_WRITE 1
//#define TEST_READ 1

//#define USE_MBIND  /* for kaapi : use mbind in allocation */

#define USE_KAAPI
//#define USE_OMP        /* loop using parallel OpenMP loop */
//#define USE_SEQ        /* loop using sequential memcpy-move, else C loop */
 
#if defined(USE_OMP)
#include <omp.h>
#endif
#if defined(USE_KAAPI)
#include "kaapic.h"
#endif

#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>
#include <assert.h>

#if defined(HAVE_CLOCK_GETTIME)
# include <time.h>
#else
# include <sys/time.h>
#endif

/** Description of the example.
 
 Overview of the execution.
 
 What is shown in this example.
 
 Next example(s) to read.
 */

double my_gettime(void)
{
#if defined(HAVE_CLOCK_GETTIME)
  struct timespec ts;
  int err = clock_gettime( CLOCK_REALTIME, &ts );
  if (err !=0) return 0;
  return (double)tv.tv_sec + 1e-9*(double)tv.tv_nsec;
#else
  struct timeval tv;
  int err = gettimeofday( &tv, 0);
  if (err  !=0) return 0;
  return (double)tv.tv_sec + 1e-6*(double)tv.tv_usec;
#endif
}

static void apply_init(
  int32_t i, int32_t j, int32_t tid, double* a1, double* a2
)
{
  int32_t ssize = j-i;
  a1 += i;
  a2 += i;
  for (i = 0; i < ssize; ++i)
  {
    a1[i] = 0.0f;
    a2[i] = 0.0f;
  }
}

#if defined(USE_KAAPI)
#  if defined(TEST_MOVE)
static void apply_move(
  int32_t i, int32_t j, int32_t tid, const double* src, double* dest
)
{
  int32_t ssize = j-i;
  src += i;
  dest += i;
  for (i = 0; i < ssize; ++i)
    dest[i] = src[i];
}
#  endif

#  if defined(TEST_WRITE)
static void apply_write(
  int32_t i, int32_t j, int32_t tid, double* dest
)
{
  int k;
  size_t ssize = j-i;
  dest += i;
  for (k=0; k<ssize; ++k)
    dest[k] = 3.1415678;
}
#  endif

#  if defined(TEST_READ)
static void apply_read(
  int32_t i, int32_t j, int32_t tid, const double* src, double* dest
)
{
  int k;
  double d = 0;
  size_t ssize = j-i;
  src += i;
  for (k=0; k<ssize; ++k)
    d += src[i];
  dest[0] = d;
}
#  endif
#endif


void my_memcpy( void* dest, const void* src, size_t size)
{
#if defined(USE_OMP) || defined(USE_SEQ)
  double* dd = (double*)dest;
  const double* ss = (const double*)src;
#endif
  size_t ssize = size / sizeof(double);
#if defined(TEST_MOVE)
#  if defined(USE_KAAPI)
  kaapic_foreach_attr_t attr;
  kaapic_foreach_attr_init( &attr );
  kaapic_foreach_attr_set_grains( &attr, 8192, 8192 );
  kaapic_foreach( 0, ssize, &attr, 2, apply_move, src, dest );
  kaapic_foreach_attr_destroy( &attr );
#  elif defined(USE_OMP)
  size_t i;
#pragma omp parallel for 
  for (i=0; i<ssize; ++i)
    dd[i] = ss[i];

#  elif defined(USE_SEQ)
  size_t i;
  for (i=0; i<ssize; ++i)
    dd[i] = ss[i];
#  endif

#elif defined(TEST_WRITE)

#  if defined(USE_KAAPI)
  kaapic_foreach_attr_t attr;
  kaapic_foreach_attr_init( &attr );
  kaapic_foreach_attr_set_grains( &attr, 8192, 8192 );
  kaapic_foreach( 0, ssize, &attr, 1, apply_write, dd );
  kaapic_foreach_attr_destroy( &attr );
#  elif defined(USE_SEQ) 
  size_t i;
  for (i=0; i<ssize; ++i)
    dd[i] = 3.1415678;
#  elif defined(USE_OMP)
  int i;
#pragma omp parallel for 
  for (i=0; i<ssize; ++i)
    dd[i] = 3.1415678;
#  endif

#elif defined(TEST_READ)

#  if defined(USE_KAAPI)
  kaapic_foreach_attr_t attr;
  kaapic_foreach_attr_init( &attr );
  kaapic_foreach_attr_set_grains( &attr, 8192, 8192 );
  kaapic_foreach( 0, ssize, &attr, 1, apply_read, ss );
  kaapic_foreach_attr_destroy( &attr );
#  elif defined(USE_SEQ) 
  size_t i;
  double d = 0.0;
  for (i=0; i<ssize; ++i)
    d += ss[i];
  dd[0] = d;
#  elif defined(USE_OMP)
  size_t i;
  double d = 0.0;
#pragma omp parallel for reduction(+:d)
  for (i=0; i<ssize; ++i)
    d += ss[i];
  dd[0] = d;
#  endif

#endif
}

/**
 */
int main(int argc, char** argv)
{
  double t0,t1;
  double sum = 0.f;
  double tbind = 0.f;
  size_t k;
  size_t iter;
  size_t ITEM_COUNT;
  int blocsize;
#if defined(USE_KAAPI) && defined(USE_MBIND)
  kaapi_numaset_t numaset_dest;
  kaapi_numaset_t numaset_src;
#endif
 
  ITEM_COUNT = 134217728; /* 1 GByte of double */
  if (argc >1)
    ITEM_COUNT = atoi(argv[1]); 

#if defined(USE_KAAPI)
  /* initialize the runtime : start all the worker threads */
  kaapic_init(KAAPIC_START_ALL);
  blocsize = ITEM_COUNT*sizeof(double)/kaapi_getconcurrency();
#elif defined(USE_OMP)
  blocsize = ITEM_COUNT*sizeof(double)/omp_get_num_threads();
#else
#  error "here"
#endif
  if (argc >2)
    blocsize = atoi(argv[2]); 


#if defined(USE_KAAPI) && defined(USE_MBIND)
  numaset_src = kaapi_all_numanodeset;
  if (argc >3)
    numaset_src= atoi(argv[3]); 

  numaset_dest = kaapi_all_numanodeset;
  if (argc >4)
    numaset_dest= atoi(argv[4]); 
#endif

  t0 = my_gettime();
#if defined(USE_KAAPI) && defined(USE_MBIND)
  double* array_src = kaapi_numa_allocate_bloc1dcyclic(numaset_src, ITEM_COUNT*sizeof(double), blocsize );
#else
  double* array_src = malloc(ITEM_COUNT*sizeof(double) );
#endif
  t1 = my_gettime();
  tbind += t1-t0;
  assert( array_src != 0 );

  t0 = my_gettime();
#if defined(USE_KAAPI) && defined(USE_MBIND)
  double* array_dest = kaapi_numa_allocate_bloc1dcyclic(numaset_dest, ITEM_COUNT*sizeof(double), blocsize );
#else
  double* array_dest = malloc(ITEM_COUNT*sizeof(double));
#endif
  t1 = my_gettime();
  tbind += t1-t0;
  assert( array_dest != 0 );
  printf("#-- time alloc/bind (s)%li, %i\n    %lf\n", ITEM_COUNT, blocsize, tbind/2.0 );

#  if defined(USE_KAAPI)
  kaapic_foreach_attr_t attr;
  kaapic_foreach_attr_init( &attr );
  kaapic_foreach_attr_set_grains( &attr, 8192, 8192 );
  kaapic_foreach( 0, ITEM_COUNT, &attr, 2, apply_init, array_src, array_dest );
  kaapic_foreach_attr_destroy( &attr );
#  elif defined(USE_SEQ) 
  int i;
  for (i=0; i<ITEM_COUNT; ++i) 
  {
    array_src[i]  = 0.0f;
    array_dest[i] = 0.0f;
  }
#  elif defined(USE_OMP)
  int i;
#pragma omp parallel for
  for (i=0; i<ITEM_COUNT; ++i) 
  {
    array_src[i]  = 0.0f;
    array_dest[i] = 0.0f;
  }
#endif


#if 0 /* uncomment to display mapping of arrays */
#if defined(USE_KAAPI)
  t1 = my_gettime();
  tbind += t1-t0;
  long size = ITEM_COUNT*sizeof(double);
  char* tmp = (char*)array_src; 
  printf("Mapping: src set=%lu\n", numaset_src);
  while (size >0)
  {
    int nodeid = kaapi_numa_getpage_nodeid( tmp );
    printf("%i ", nodeid);
    tmp  += getpagesize();;
    size -= getpagesize();;
  }
  printf("\n");
  fflush(stdout);

  size = ITEM_COUNT*sizeof(double);
  tmp = (char*)array_dest; 
  printf("Mapping: dest set=%lu\n", numaset_dest);
  while (size >0)
  {
    int nodeid = kaapi_numa_getpage_nodeid( tmp );
    printf("%i ", nodeid);
    tmp  += getpagesize();;
    size -= getpagesize();;
  }
  printf("\n");
  fflush(stdout);
#endif
#endif
  
  /* wormloop */
  t0 = my_gettime();
  for (k=0; k<4; ++k)
    my_memcpy( array_dest, array_src, ITEM_COUNT*sizeof(double) );
  t1 = my_gettime();
  printf("#-- time worm steps: %lf\n", (t1-t0)/4.0);

  t0 = my_gettime();
  for (iter = 0; iter < 100; ++iter)
    my_memcpy( array_dest, array_src, ITEM_COUNT*sizeof(double) );
  t1 = my_gettime();
  sum = t1-t0;

  double bytes = (double)ITEM_COUNT * (double)sizeof(double);
#if defined(TEST_MOVE)
  bytes *= 2.0;
#endif

  printf("#-- time (s)%li, %i\n    %lf\n", ITEM_COUNT, blocsize, sum / 100.0);
  printf("#-- bandwidth (MByte/s)%li, %i\n  %lf\n", ITEM_COUNT, blocsize, 
    1.0E-06*bytes*100.0 / sum );

#if defined(USE_KAAPI) && defined(USE_MBIND)
  kaapi_numa_free( array_src, blocsize );
  kaapi_numa_free( array_dest, blocsize );
#endif

#if defined(USE_KAAPI) && defined(USE_MBIND)
  /* finalize the runtime */
  kaapic_finalize();
#endif
  return 0;
}
