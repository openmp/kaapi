/*
 ** xkaapi
 ** 
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 ** thierry.gautier@inrialpes.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include "kaapic.h"
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>


/** Description of the example.
 
 Overview of the execution.
 
 What is shown in this example.
 
 Next example(s) to read.
 */

#if 0
static void apply_init(
  int32_t i, int32_t j, int32_t tid, double* array
)
{
  /* process array[i, j[, inclusive */
  int32_t k;

  for (k = i; k < j; ++k)
    array[k] = (i*j+k)/32.0;
}
#endif

#if 1
static void apply(
  int32_t i, int32_t j, int32_t tid, const double* src, double* dest
)
{
  memcpy(dest+i, src+i, sizeof(double)*(j-i));
}

#endif

void my_memcpy( void* dest, const void* src, size_t size)
{
#if 1
  kaapic_foreach_attr_t attr;
  kaapic_foreach_attr_init( &attr );
  kaapic_foreach_attr_set_grains( &attr, 8192, 8192 );
  kaapic_foreach( 0, size/sizeof(double), &attr, 2, apply, src, dest );
  kaapic_foreach_attr_destroy( &attr );
#elif 0
  memcpy( dest, src, size );
#elif 0
  double* dd = (double*)dest;
  const double* ss = (const double*)src;
  size_t i;
  size_t ssize = size / sizeof(double);
  for (i=0; i<ssize; ++i)
    dd[i] = ss[i];
#elif 0 // WRITE
  double* dd = (double*)dest;
  size_t i;
  size_t ssize = size / sizeof(double);
  for (i=0; i<ssize; ++i)
    dd[i] = 3.1415678;
#elif 0 // READ
  double* dd = (double*)dest;
  const double* ss = (const double*)src;
  size_t i;
  size_t ssize = size / sizeof(double);
  double d = 0.0;
  for (i=0; i<ssize; ++i)
    d += ss[i];
  dd[0] = d;
#endif
}

/**
 */
int main(int argc, char** argv)
{
  double t0,t1;
  double sum = 0.f;
  double tbind = 0.f;
  size_t i,k;
  size_t iter;
  size_t ITEM_COUNT;
  int blocsize;
  kaapi_ldset_t ldset_dest;
  kaapi_ldset_t ldset_src;
 
  ITEM_COUNT = 134217728; /* 1 GByte of double */
  if (argc >1)
    ITEM_COUNT = atoi(argv[1]); 

  blocsize = getpagesize();
  if (argc >2)
    blocsize = atoi(argv[2]); 

  /* initialize the runtime : start all the worker threads */
  kaapic_init(KAAPIC_START_ALL);

  ldset_src = kaapi_all_lds(KAAPI_HWS_LEVELID_NUMA);
  if (argc >3)
    ldset_src= atoi(argv[3]); 

  ldset_dest = kaapi_all_lds(KAAPI_HWS_LEVELID_NUMA);
  if (argc >4)
    ldset_dest= atoi(argv[4]); 

  t0 = kaapic_get_time();
  double* array_src = kaapi_ldset_allocate_bloc1dcyclic(ldset_src, ITEM_COUNT*sizeof(double), blocsize );
  t1 = kaapic_get_time();
  tbind += t1-t0;
  kaapi_assert( array_src != 0 );

  t0 = kaapic_get_time();
  double* array_dest = kaapi_ldset_allocate_bloc1dcyclic(ldset_dest, ITEM_COUNT*sizeof(double), blocsize );
  t1 = kaapic_get_time();
  tbind += t1-t0;
  kaapi_assert( array_dest != 0 );

  bzero( array_src, sizeof(double)*ITEM_COUNT );
  bzero( array_dest, sizeof(double)*ITEM_COUNT );


#if 1
  t1 = kaapic_get_time();
  tbind += t1-t0;
  long size = ITEM_COUNT*sizeof(double);
  char* tmp = (char*)array_src; 
  printf("Mapping: src set=%lu\n", ldset_src);
  while (size >0)
  {
    int nodeid = kaapi_ldset_getpage_id( tmp );
    printf("%i ", nodeid);
    tmp  += getpagesize();;
    size -= getpagesize();;
  }
  printf("\n");
  fflush(stdout);

  size = ITEM_COUNT*sizeof(double);
  tmp = (char*)array_dest; 
  printf("Mapping: dest set=%lu\n", ldset_dest);
  while (size >0)
  {
    int nodeid = kaapi_ldset_getpage_id( tmp );
    printf("%i ", nodeid);
    tmp  += getpagesize();;
    size -= getpagesize();;
  }
  printf("\n");
  fflush(stdout);
#endif
  
  /* initialize, apply, check */
  for (i = 0; i < ITEM_COUNT; ++i)
    array_src[i] = 0.f;

  /* wormloop */
  t0 = kaapic_get_time();
  for (k=0; k<4; ++k)
    my_memcpy( array_dest, array_src, ITEM_COUNT*sizeof(double) );
  t1 = kaapic_get_time();
  printf("#-- time worm steps: %lf\n", t1-t0);

  t0 = kaapic_get_time();
  for (iter = 0; iter < 100; ++iter)
    my_memcpy( array_dest, array_src, ITEM_COUNT*sizeof(double) );
  t1 = kaapic_get_time();
  sum = t1-t0;

  printf("#-- time bind (s) %li, %i\n    %lf\n", ITEM_COUNT, blocsize, tbind/2.0 );
  printf("#-- time (s) %li, %i\n    %lf\n", ITEM_COUNT, blocsize, sum / 100.0);
  printf("#-- bandwidth (MByte/s) %li, %i\n  %lf\n", ITEM_COUNT, blocsize, 
    (double)(ITEM_COUNT*sizeof(double)*1.0) / sum / 1024.0/1024.0 );

  kaapi_ldset_free( array_src, blocsize );
  kaapi_ldset_free( array_dest, blocsize );

  /* finalize the runtime */
  kaapic_finalize();
  return 0;
}
