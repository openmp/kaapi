/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi.h"
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>


typedef struct fibo_arg_t {
  long  n;
  long* res;
} fibo_arg_t __attribute__((aligned(8)));

void fibo_body( kaapi_task_t* task, kaapi_thread_t* thread );

static inline void fibo_compute( kaapi_thread_t* thread, long* res, long n )
{
  if (n < 2)
  {
    *res = n;
  }
  else {
    long r1;
    long r2; 

    fibo_arg_t* task1 = (fibo_arg_t*)kaapi_thread_pushdata(thread, sizeof(fibo_arg_t));
    task1->n = n - 1;
    task1->res = &r1;
    kaapi_task_push(thread, fibo_body, task1);

    fibo_arg_t* task2 = (fibo_arg_t*)kaapi_thread_pushdata(thread, sizeof(fibo_arg_t));
    task2->n = n - 2;
    task2->res = &r2;
    kaapi_task_push(thread, fibo_body, task2);

    kaapi_sched_sync( thread );

    *res = r1+r2;
 }
}

void fibo_body( kaapi_task_t* task, kaapi_thread_t* thread )
{
  fibo_arg_t* arg = (fibo_arg_t*)kaapi_task_getargs(task);
  fibo_compute( thread, arg->res, arg->n);
}

int main(int argc, char** argv)
{
  double delay = 0;
  double t0, t1;
  long value_result;
  long n;
  long niter;
  long i;
  fibo_arg_t* task;
  kaapi_thread_t* thread;
  
  if (argc >1)
    n = atoi(argv[1]);
  else 
    n = 30;
  if (argc >2)
    niter =  atoi(argv[2]);
  else 
    niter = 1;

  kaapi_init(1, &argc, &argv);
  thread = kaapi_self_thread();

  for ( i=0; i<niter; ++i)
  {
    t0 = kaapi_get_elapsedtime();
    task = (fibo_arg_t*)kaapi_thread_pushdata(thread, sizeof(fibo_arg_t));
    task->n      = n;
    task->res    = &value_result;
    kaapi_task_push(thread, fibo_body, task);
    kaapi_sched_sync( thread );
    t1 = kaapi_get_elapsedtime();
    delay += t1-t0;
  }

  printf("Res = %li\n", value_result );
  printf("Time Fibo(%li): %f\n", n, delay/niter);

  kaapi_finalize();
  
  return 0;
}
