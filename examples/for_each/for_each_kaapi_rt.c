/*
 ** xkaapi
 ** 
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 ** thierry.gautier@inrialpes.fr
 ** fabien.lementec@imag.fr
 ** 
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 ** 
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 ** 
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 ** 
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 ** 
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include "kaapi.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <inttypes.h>
#include <unistd.h>

/** Description of the example.
 
 Overview of the execution.
 
 What is shown in this example.
 
 Next example(s) to read.
 */


static void apply_cos(
  unsigned long long i,
  unsigned long long j,
  void* arg
)
{
  double* array = (double*)arg;
  /* process array[i, j[, inclusive */
  unsigned long long k;

  for (k = i; k < j; ++k)
    array[k] += array[k]+2.0;
}


/**
 */
int main(int argc, char** argv)
{
  double t0,t1;
  double sum = 0.f;
  size_t iter;
  kaapi_foreach_attr_t attr;
  int sg;
  
  sg = 1;
  if (argc >1)
    sg = atoi(argv[1]); 
  
  /* initialize the runtime : start all the worker threads */
  kaapi_init(KAAPI_START_ONLY_MAIN, &argc, &argv );

#define ITEM_COUNT ((10000000/getpagesize())*getpagesize())
  size_t sz = ITEM_COUNT * sizeof(double);

#if 1
  double* array = (double*)malloc(sz);
#endif
  if (array ==0)
  {
    fprintf(stderr,"Error in allocation\n");
    exit(1);
  }
  
  kaapi_foreach_attr_init( &attr );
  kaapi_foreach_attr_set_grains( &attr, sg, sg );
  
  for (iter = 0; iter < 100; ++iter)
  {
    t0 = kaapi_get_elapsedtime();
    kaapi_foreach( ITEM_COUNT, &attr, apply_cos, array );
    t1 = kaapi_get_elapsedtime();
    sum += (t1-t0)*1000; /* ms */
  }

  printf("#-- Grain  time (ms)\n%i  %i  %lf\n", kaapi_get_concurrency(), sg, sum / 100);

  kaapi_foreach_attr_destroy( &attr );

  /* finalize the runtime */
  kaapi_finalize();
  
  return 0;
}
