/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/

#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <inttypes.h> 
#include <errno.h> 

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>

/* to have clock_gettime depending of the OS */
//#define HAVE_CLOCK_GETTIME 1

/* exclusive compilation flags:
   KAAPI_FOREACH -> kaapic_foreach version
   KAAPI_TASK    -> kaapic_task version
   OMP_FOREACH   -> openmp foreach version
   OMP_TASK      -> openmp task version
*/
#define KAAPI_FOREACH 1
//#define OMP_FOREACH 1
//#define KAAPI_TASK 1


#if defined(KAAPI_FOREACH) || defined(KAAPI_TASK)
#include "kaapic.h"
#endif

#if defined(OMP_FOREACH) || defined(OMP_TASK)
#include <numa.h>
#include <numaif.h>
#include <sys/mman.h>
#include "omp.h"
uint64_t all_ldset = 0;

int getcpu( unsigned* cpu, unsigned* node, void* a)
{
  int err = syscall(__NR_getcpu, cpu, node, a );
  if (err !=0)
    printf( "Error... %s\n", strerror(errno) );
  return 0;
}

#endif


/* function for allocation + binding. Take from Kaapi, put copied for OpenMP.. */
static void* mini_pma_allocate_bloc1dcyclic(size_t size, size_t blocsize);
static int mini_pma_getpage_id(const void* addr);
static double get_elapsedtime(void);
static int mini_pma_numa_init(void);
int count_ldset =0;

/* CHECK: define if check the mapping */
#define CHECK 1

/* size of particules in term of cache line (assumed cache line == 64 bytes) */
#define CACHE_LINE 3
#define MAX_HISTORY (64*CACHE_LINE)/sizeof(double)


/* threshold : if bigger then copy particles to the destination array */
#define THRESHOLD 0.8

typedef struct {
  double history[MAX_HISTORY];
} particule;

#if defined(KAAPI_FOREACH) || defined(KAAPI_TASK)
#include "kaapic.h"
#endif

#if defined(OMP_FOREACH) || defined(OMP_TASK)
#include "omp.h"
#endif


#if defined(KAAPI_FOREACH) 
void whatever_function(int i, int j, int tid, int  iter, particule* src, particule* dest, int* atom)
{
  for ( ; i<j; ++i)
  {
    if (src[i].history[0]+src[i].history[iter] > THRESHOLD)
    {
      int pos = KAAPI_ATOMIC_INCR((kaapi_atomic_t*)atom);
      dest[pos] = src[i];
    }
  }
}
#endif
#if defined(KAAPI_TASK) 
void whatever_function(int i, int j, int iter, particule* src, particule* dest, int* atom)
{
  for ( ; i<j; ++i)
  {
    if (src[i].history[0]+src[i].history[iter] > THRESHOLD)
    {
      int pos = KAAPI_ATOMIC_INCR((kaapi_atomic_t*)atom);
      dest[pos] = src[i];
    }
  }
}
#endif
#if defined(OMP_FOREACH) 
void whatever_function(int n, int  iter, particule* src, particule* dest, int* atom)
{
  int i;
  #pragma omp parallel for firstprivate(n) 
  for ( i=0; i<n; ++i)
  {
    if (src[i].history[0]+src[i].history[iter] > THRESHOLD)
    {
      int pos= __sync_add_and_fetch(atom, 1);
      dest[pos] = src[i];
    }
  }
}
#endif



/* init array source array with random number */
void init_function(int i, int j, int tid, particule* dest, unsigned int seed)
{
  size_t iter;
  for ( ; i<j; ++i)
  {
    for (iter=0; iter<MAX_HISTORY; ++iter)
    {
      double r = ((double)(rand_r(&seed) % 10000)) / 10000.0;
      dest[i].history[iter] = r;
    }
  }
}

/* */
void verif_mapping_bloc(char* base, size_t size, size_t blocsize )
{
  size_t i;
  size_t pagesize = getpagesize();
  assert( blocsize % pagesize == 0);
  
  for (i=0; i<size; i+=pagesize)
  {
    int id = mini_pma_getpage_id(base+i);
    if (id != (i/blocsize))
    {
      printf("*** bad mapping: page: %"PRIu64" mapped on node: %i, should be on:%"PRIu64"\n",
             (uint64_t)i/pagesize, id, (uint64_t)(i/blocsize) );
      exit(1);
    }
  }
}

/* main entry point : Kaapi initialization
*/
int main(int argc, char** argv)
{ 
  size_t n, p;
  size_t size, blocsize;
  int iter, i, k, j;
  unsigned int seed;
  int* atom;
  double t0, t1;
  double d;
  
  /* start all the thread */
#if defined(KAAPI_FOREACH) || defined(KAAPI_TASK)
  kaapic_init(KAAPIC_START_ONLY_MAIN);
#endif
  
  if (argc <2)
  {
    printf("Usage: %s <n> <seed>\n",argv[0]);
    exit(22);
  }
  n = atoi(argv[1]);
  seed = atoi(argv[2]);

  atom = malloc( sizeof(int) );
#if defined(OMP_FOREACH) || defined(OMP_TASK)
  #pragma omp parallel 
  {
#pragma omp single
    p = omp_get_num_threads();

    {
      unsigned cpu; 
      unsigned node;
      getcpu( &cpu, &node, 0);
      printf("#Thread: %i run on cpuid: %i, on nodeid: %i\n", omp_get_thread_num(), cpu, node );
    }
  }
#elif defined(KAAPI_FOREACH) || defined(KAAPI_TASK)
  p = kaapic_get_concurrency();
#endif
  assert( n % p == 0);
  assert( n*sizeof(particule) % getpagesize() == 0);

  mini_pma_numa_init();

  iter = MAX_HISTORY;
  
  size = n*sizeof(particule);
  blocsize = size/count_ldset;
  particule* src  = mini_pma_allocate_bloc1dcyclic(size, blocsize );
  assert(src !=0);
  particule* dest = mini_pma_allocate_bloc1dcyclic( size, blocsize );
  assert(dest !=0);

  init_function(0, n, 0, src, seed);
  init_function(0, n, 0, dest, seed);
  
#if defined(CHECK)
  {
    /* verif mapping is bloc, i.e. page i-ith is on numanode i/((n*sizepart)/count_ldset) */
    verif_mapping_bloc((char*)src, size, blocsize );
    verif_mapping_bloc((char*)dest, size, blocsize );
  }
#endif

  /* print numa node for var numa: */
  int id_atom = mini_pma_getpage_id( (void*)(0xfff & (getpagesize()-1+(uintptr_t)atom)) );
  printf("#Node info atom : nodeid: %i, @var:%p, error: %s\n", id_atom, (void*)atom, strerror(errno));

  printf("#MINI PMA FOR EACH, n=%i, p=%i\n", (int)n, (int)p);
  for (j=0; j<1; ++j)
  for (i=0; i<iter; ++i)
  {
    t0 = get_elapsedtime();
    *atom = 0;

#if defined(KAAPI_FOREACH)
    kaapic_foreach( 
      0, n, /* range */
      0,    /* attribut */
      4,    /* #function args */
      6, whatever_function,
      i,
      src,
      dest,
      atom
    );
#endif
#if defined(OMP_FOREACH)
    /* parallel loop inside the function */
    whatever_function( n, i, src, dest, atom );
#endif
#if defined(KAAPI_TASK)
    {
      int l, threadid;
      size_t size = n*sizeof(particule);
      size_t thread_blocsize = size/p;
      size_t count_ld = kaapi_ldset_count(KAAPI_HWS_LEVELID_NUMA);
      size_t thread_perld = p / count_ld;
      /* assume same numer of thread per locality domain */
      assert( thread_perld * count_ld == p );
printf("#ld:%i, thread per ld:%i, thread_blocsize:%i\n", (int)count_ld, (int)thread_perld, (int)thread_blocsize);
int depth;
int c = kaapi_getconcurrency_level(KAAPI_HWS_LEVELMASK_NUMA, &depth);
printf("Concurrency at NUMA level:%i, [depth:%i]\n", c, depth );
      
      for (l=0; l<p; ++l)
      {
        int ld;
        size_t begin = l*thread_blocsize;
        size_t end   = begin + thread_blocsize;
        
        ld = begin / blocsize;
        threadid = (begin / (blocsize/thread_perld)) % thread_perld;
printf("Map [%"PRIu64", %"PRIu64") on ld=%i, thread id:%i\n", (uint64_t)begin, (uint64_t)end, ld, threadid );
        
        kaapic_spawn_attr_t attr;
        kaapic_spawn_attr_init(&attr);
        kaapic_spawn_attr_set_ld(&attr, threadid );
        kaapic_spawn( &attr, 
          KAAPIC_MODE_V, KAAPIC_TYPE_INT, 1, begin, 
          KAAPIC_MODE_V, KAAPIC_TYPE_INT, 1, end, 
          KAAPIC_MODE_V, KAAPIC_TYPE_INT, 1, i, 
          KAAPIC_MODE_V, KAAPIC_TYPE_PTR, 1, src, 
          KAAPIC_MODE_V, KAAPIC_TYPE_PTR, 1, dest, 
          KAAPIC_MODE_V, KAAPIC_TYPE_PTR, 1, atom
        );
      }
      kaapic_sync();
    }
#endif
    t1 = get_elapsedtime();
    printf("%f  %i  %e  %i\n", t0, (int)i, t1-t0, *atom);

    d = 0;
    for (k=0; k< *atom; ++k)
      d += dest[k].history[i];
  }
  
#if defined(KAAPI_FOREACH) || defined(KAAPI_TASK)
  kaapic_finalize();
#endif
  
  return 0;
}


#if defined(HAVE_CLOCK_GETTIME)
# include <time.h>
#else
# include <sys/time.h>
#endif

/**
*/
static double get_elapsedtime(void)
{
#if defined(HAVE_CLOCK_GETTIME)
  struct timespec ts;
  int err = clock_gettime( CLOCK_REALTIME, &ts );
  if (err !=0) return 0;
  return (double)ts.tv_sec + 1e-9*(double)ts.tv_nsec;
#else
  struct timeval tv;
  int err = gettimeofday( &tv, 0);
  if (err  !=0) return 0;
  return (double)tv.tv_sec + 1e-6*(double)tv.tv_usec;
#endif
}


#if defined(KAAPI_FOREACH) || defined(KAAPI_TASK)
int mini_pma_numa_init(void)
{
  count_ldset = kaapi_ldset_count(KAAPI_HWS_LEVELID_NUMA);
  return 0;
}

static int mini_pma_getpage_id(const void* addr)
{
  return kaapi_ldset_getpage_id(addr);
}

static void* mini_pma_allocate_bloc1dcyclic(size_t size, size_t blocsize)
{
  return kaapi_ldset_allocate_bloc1dcyclic( kaapi_all_lds(KAAPI_HWS_LEVELID_NUMA), size, blocsize );
}
#endif

#if defined(OMP_FOREACH) || defined(OMP_TASK)
static inline int bitmap_value_first1_and_zero_64( uint64_t* b )
{
  /* Note: for WIN32, to have a look at _BitScanForward */
  int fb = __builtin_ffsl( *b );
  if (fb ==0) return 0;
  *b &= ~( ((uint64_t)1) << (fb-1) );
  return fb;
}

/*
*/
static int mini_pma_getpage_id(const void* addr)
{
  const unsigned long flags = MPOL_F_NODE | MPOL_F_ADDR;
  int mode;
  uint64_t nodemask;
  nodemask =0;
  const int err = get_mempolicy
    (&mode, (unsigned long*)&nodemask, 64UL, (void* )addr, flags);

  if (err || (nodemask==0))
  {
    errno = EINVAL;
    return -1;
  }
  return mode;
}

/*
*/
static void* mini_pma_allocate_bloc1dcyclic(size_t size, size_t blocsize)
{
  void* retval;
  size_t pagesize, size_mmap;
  const int mode = MPOL_BIND;
  const unsigned int flags = MPOL_MF_STRICT | MPOL_MF_MOVE;
  const char* base;
  struct bitmask *nodemask;
  uint64_t numaset;

  if ((blocsize & 0xfff) !=0) /* should be divisible by 4096 */
  {
    errno = EINVAL;
    return (void*)0;
  }

  
  pagesize = getpagesize();
  size_mmap = (( size + pagesize -1 ) / pagesize) * pagesize;
  retval = mmap( 0, size_mmap, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANON, -1, (off_t)0 );
  if (retval == (void*)-1)
  {
    fprintf(stderr, "*** error: %s", strerror(errno));
    return 0;
  }
  if (((uintptr_t)retval & 0xfff) !=0) /* should aligned on page boundary */
  {
    errno = EFAULT;
    goto return_error_after_mmap;
  }

  base = (const char*)retval;
  
  nodemask = numa_allocate_nodemask();
  numaset = all_ldset;
  int node = 0;
  while (size >0)
  {
    /* get the OS numanode id */
    node = bitmap_value_first1_and_zero_64( &numaset );
    if (node == -1)
    {
      numaset = all_ldset;
      continue;
    }
    --node;
    numa_bitmask_setbit(nodemask, node);
    if (mbind((void*)base, blocsize, mode, nodemask->maskp, nodemask->size, flags))
      goto return_error_after_mmap;
printf("Bind on node:%i\n", node);
    numa_bitmask_clearbit(nodemask, node);

    base += blocsize;
    if (size > blocsize)
      size -= blocsize;
    else
      size = 0;
  }
  numa_bitmask_free( nodemask );
  
  return retval;

return_error_after_mmap:
    fprintf(stderr, "*** error: %s", strerror(errno));
    munmap( retval, size_mmap );
    return 0;
}

int mini_pma_numa_init(void)
{
  unsigned int i;
  unsigned int j;
  struct bitmask* allowed_numanode;
  uint64_t ldset_all = 0;
  numa_set_bind_policy(1);
  numa_set_strict(1);
  
  /* mapping : Kaapi numa node id to OS identifier */
  allowed_numanode = numa_get_mems_allowed();
  for (j=0, i=0; i< 8*numa_bitmask_nbytes(allowed_numanode); ++i)
  {
    if (numa_bitmask_isbitset(allowed_numanode, i))
    {
      ldset_all |= (1<<j);
      j++;
    }
  }

#if 0
  count_ldset = j;
  all_ldset = ldset_all;
#else
  count_ldset = 4;
  all_ldset = (1<<0) | (1<<2) | (1<<4) | (1<<6);
#endif
  return 0;
}

#endif
