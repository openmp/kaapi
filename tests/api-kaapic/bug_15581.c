#include <kaapic.h>
#include <stdio.h>
#include <stdlib.h>

static inline void doFor(int32_t b, int32_t e, int32_t tid) 
{
}

void sum(int* result1, int* result2, int* result)
{
*result = *result1 + *result2;
}

void fibonacci(int n, int* result)
{
/* task user specific code */
if (n<2)
{
  *result = n;
  //printf("[%i] beg for\n", kaapi_self_kid());
  kaapic_foreach(0,10000,NULL,0,doFor);
  //printf("[%i] end for\n", kaapi_self_kid());
}
else
{
  int* result1 = kaapic_alloca(sizeof(int));
  int* result2 = kaapic_alloca(sizeof(int));
  kaapic_spawn(0, 2, fibonacci, 
    KAAPIC_MODE_V, KAAPIC_TYPE_INT, 1, n-1,
    KAAPIC_MODE_W, KAAPIC_TYPE_INT, 1, result1
  );

  kaapic_spawn(0, 2, fibonacci, 
    KAAPIC_MODE_V, KAAPIC_TYPE_INT, 1, n-2,
    KAAPIC_MODE_W, KAAPIC_TYPE_INT, 1, result2
  );

  kaapic_spawn(0, 3, sum,
    KAAPIC_MODE_R, KAAPIC_TYPE_INT, 1, result1,
    KAAPIC_MODE_R, KAAPIC_TYPE_INT, 1, result2,
    KAAPIC_MODE_W, KAAPIC_TYPE_INT, 1, result
  );
}
}

int main(int argc, char *argv[])
{
int n = 30;
int result = 0;
int err = kaapic_init( KAAPIC_START_ONLY_MAIN );

fprintf(stdout, "err %d\n", err);

if (argc > 1)
{
  char *e;
  n = (int)strtol(argv[1], &e,0);
}

/* example of attribut for spawn, not used */
kaapic_spawn_attr_t attr;
kaapic_spawn_attr_init(&attr);

kaapic_begin_parallel (KAAPIC_FLAG_DEFAULT);

double start = kaapic_get_time();

kaapic_spawn(&attr, 2, fibonacci, 
KAAPIC_MODE_V, KAAPIC_TYPE_INT, 1, n,
KAAPIC_MODE_W, KAAPIC_TYPE_INT, 1, &result );

kaapic_sync();

double stop = kaapic_get_time();
  
kaapic_end_parallel (KAAPIC_FLAG_DEFAULT);

fprintf(stdout, "Fibo(%d) = %d\n", n, result);

fprintf(stdout, "Time : %f (s)\n", stop-start);

kaapic_finalize();
return 0;
}

