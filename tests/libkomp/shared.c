#include <stdio.h>
#include <omp.h>

int
check_results (int *res, int nthr, int value) {
  int ret = 0;
  int i = 0;

  for (i = 1; i < nthr; i++) {
    if (res[i - 1] != res[i]) {
      res = 1;
      break;
    }
  }

  return ret;
}

int
main (int argc, char **argv)
{
  int cpt = 123321;
  int nthr = 0;

#pragma omp parallel shared (nthr)
  {
#pragma omp single
    nthr = omp_get_num_threads ();
  }
  
  int res[nthr];

#pragma omp parallel shared (cpt, res) num_threads (nthr)
  res[omp_get_thread_num ()] = cpt;
  
  return check_results (res, nthr, cpt);
}
