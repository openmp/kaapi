/* { dg-options "-std=gnu99 -fopenmp" } */

extern void abort (void);

#define M(x, y, z) O(x, y, z)
#define O(x, y, z) x ## _ ## y ## _ ## z

#define F parallel for
#define G pf
#include "for-1.h"
#undef F
#undef G

#define F for
#define G f
#include "for-1.h"
#undef F
#undef G

int
main ()
{
/*
  if (test_pf_static ())
    abort ();
  if (test_pf_static32 ())
    abort ();
  if (test_pf_auto ())
    abort ();
*/
  if (test_pf_guided32 ())
    abort ();
  if (test_pf_runtime ())
    abort ();
  if (test_f_static ())
    abort ();
  if (test_f_static32 ())
    abort ();
  if (test_f_auto ())
    abort ();
  if (test_f_guided32 ())
    abort ();
  if (test_f_runtime ())
    abort ();
  return 0;
}
