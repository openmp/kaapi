#include <stdlib.h>
#include <stdio.h>

#include <omp.h>

int
main (int argc, char **argv)
{
  int ret = 0;

  if (omp_get_ancestor_thread_num (0) != 0)
    return 1;

  if (omp_get_ancestor_thread_num (1) != -1)
    return 2;

#pragma omp parallel shared (ret) num_threads(8)
  {
    int tid = omp_get_thread_num ();

    if (omp_get_ancestor_thread_num (0) != 0)
      ret = 3;

    if (omp_get_ancestor_thread_num (1) != tid)
      ret = 4;
  }

  omp_set_nested(1);
  omp_set_dynamic(0);

#pragma omp parallel shared (ret) num_threads(8) 
  {
    int tid = omp_get_thread_num ();
#pragma omp parallel firstprivate (tid) num_threads(4)  shared (ret)
    {
      int tid2 = omp_get_thread_num ();
      if (omp_get_ancestor_thread_num (1) != tid)
        ret = 5;

      if (omp_get_ancestor_thread_num (2) != tid2)
        ret = 6;
#pragma omp parallel firstprivate (tid) num_threads(2)  shared (ret)
      {
        int tid3 = omp_get_thread_num ();
        if (omp_get_ancestor_thread_num (1) != tid)
          ret = 7;

        if (omp_get_ancestor_thread_num (2) != tid2)
          ret = 8;

        if (omp_get_ancestor_thread_num (3) != tid3)
          ret = 9;
      }
    }
  }
  
  return ret;
}
