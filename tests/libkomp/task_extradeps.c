#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "omp.h"
#warning "Use libKOMP extension"
#include <omp_ext.h>

int dummy(int size, double* array)
{
  int err = 0;
  int i;
  void*  deps[size];
  
  /* address of data to compute dependencies */
  for (i=0; i<size; ++i)
    deps[i] = &array[i];

  omp_task_declare_dependencies( OMPEXT_MODE_WRITE, size, deps );
#pragma omp task private(i) firstprivate(size)
{
  sleep(1);
  for (i=0; i<size; ++i)
    array[i] = (double)i;
}

  omp_task_declare_dependencies( OMPEXT_MODE_READ, size/2, deps );
#pragma omp task private(i) firstprivate(size) shared(err)
  for (i=0; i<size/2; ++i)
  {
    if (array[i] != (double)i) 
      #pragma omp atomic write
      err = 1;
  }

  omp_task_declare_dependencies( OMPEXT_MODE_READ, size-size/2, size/2+deps );
#pragma omp task private(i) firstprivate(size) shared(err)
  for (i=size/2; i<size; ++i)
  {
    if (array[i] != (double)i) 
      #pragma omp atomic write
      err = 1;
  }

  return err;
}



int
main (int argc, char **argv)
{
  int err;
  int size = 10;
  double array[size];

#pragma omp parallel
  {
#pragma omp master
    err = dummy( size, &array[0] );
  }
  return err;
}

