#include <stdio.h>
#include <omp.h>

int
main (int argc, char **argv)
{
  int num_threads = 2;
  int cpt = 0;
#pragma omp parallel shared (cpt, num_threads)
  {

#pragma omp master
    num_threads = omp_get_num_threads ();

#pragma omp critical
    cpt++;
  }

  return !(cpt == num_threads);
}
