#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

extern int kaapi_context_print_current(FILE* file );
extern int kaapi_context_printreverse_current(FILE* file );

long spawn= 0;
long count= 0;
static void dpotrf(int BS, double* dA)
{ ++count; }
static void dtrsm(int BS, double* dA, double* dB)
{ ++count; }
static void dsyrk(int BS, double* dA, double* dB)
{ ++count; }
static void dgemm(int BS, double* dA, double* dB, double* dC)
{ ++count; }

#define A(m,n) (A+n*BS+m)
int
main (int argc, char **argv)
{
  int N = atoi(argv[1]);
  int BS = 4;
  double* A;
#pragma omp parallel
#pragma omp master
  {
    int k;
    for (k = 0; k < N; k++)
    {
      double *dA = A(k, k);
      ++spawn;
#pragma omp task depend(inout:dA[0:BS*BS])
      dpotrf(BS, dA);

      int m;
      for (m = k+1; m < N; m++)
      {
        double *dA = A(k, k);
        double *dB = A(k, m);
        ++spawn;
#pragma omp task depend(in:dA[0:BS*BS]) depend(inout:dB[0:BS*BS])
        dtrsm(BS, dB, dA);
      }

      for (m = k+1; m < N; m++)
      {
        double *dA = A(k, m);
        double *dB = A(m, m);
        ++spawn;
#pragma omp task depend(in:dA[0:BS*BS]) depend(inout:dB[0:BS*BS])
        dsyrk(BS, dB, dA);

        int n;
        for (n = k+1; n < m; n++)
        {
          double *dA = A(k , n);
          double *dB = A(k , m);
          double *dC = A(n , m);
          ++spawn;
#pragma omp task depend(in:dA[0:BS*BS], dB[0:BS*BS]) depend(inout:dC[0:BS*BS])
          dgemm(BS, dC, dB, dA);
        }
      }
    }
    //kaapi_context_print_current(stdout);
    //kaapi_context_printreverse_current(stdout);
  }
  printf("#exec: %li, #spawn: %li\n", count, spawn);
  return 0;
}
