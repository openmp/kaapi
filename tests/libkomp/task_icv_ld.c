#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "omp.h"
#if defined(KOMP_VERSION_MAJOR)
#warning "Use libKOMP extension"
#include <omp_ext.h>

void do_subunit_compute_inherited(int tid, int ld_set_by_parent)
{
#if defined(KOMP_VERSION_MAJOR)
  if (ld_set_by_parent != omp_get_locality_domain_num())
  {
    printf("[do_subunit_compute_inherited] Error:  task: %i, bad icv ldset:%i, required:%i\n", tid, omp_get_locality_domain_num(), ld_set_by_parent );
    abort();
  }  
#endif
}

void do_unit_compute(int tid, int ld_set_by_parent)
{
#if defined(KOMP_VERSION_MAJOR)
  int ldlocal = omp_get_locality_domain_num();
  if (ld_set_by_parent != ldlocal)
  {
    printf("[do_unit_compute] Error:  task: %i, bad icv ldset:%i, required:%i\n", tid, ldlocal, ld_set_by_parent );
    abort();
  } 
#endif
#pragma omp task
  do_subunit_compute_inherited(tid+100, ld_set_by_parent);
}

int
main (int argc, char **argv)
{
  int i;
  
  size_t size = 32;

#if defined(KOMP_VERSION_MAJOR)
  int nbldset = omp_get_num_locality_domains();
#endif

#pragma omp parallel
  {
#pragma omp master
    for (i = 0; i < size; ++i)
    {
#if defined(KOMP_VERSION_MAJOR)
       omp_child_task_affinity( i % nbldset, OMP_AFFINITY_FLAG_STRICT );
#endif
#pragma omp task firstprivate(i)
       do_unit_compute( i, i% nbldset );
    }
  }
  return 0;
}

#else
int
main (int argc, char **argv)
{
  return 0;
}
#endif
