#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
#if defined(KOMP_VERSION_MAJOR)
#include <omp_ext.h>
#endif


void do_unit_compute(double* array, int i, int blocsize)
{
}

int
main (int argc, char **argv)
{
  int tab[123] = { 0 };
  int i;
  int nb_threads;
  
  size_t size = 2048;

#pragma omp parallel
#pragma omp single
  nb_threads = omp_get_num_threads();
  
#if defined(KOMP_VERSION_MAJOR)
  int blocsize = 2048 / omp_get_num_locality_domains();
  double* array = omp_locality_domain_allocate_bloc1dcyclic( size*getpagesize(), blocsize );
#else
  double* array = malloc( size*getpagesize() );
#endif
  
#pragma omp parallel
  {
#pragma omp single
    for (i = 0; i < i/nb_threads; ++i)
    {
#if defined(KOMP_VERSION_MAJOR)
       omp_child_task_affinity( i / blocsize, OMP_AFFINITY_FLAG_STRICT );
#endif
#pragma omp task
       do_unit_compute( array, i, size/nb_threads);
    }
  }
    
  return 0;
}
