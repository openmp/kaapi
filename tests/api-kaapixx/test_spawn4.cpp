/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "test_main.h"
#include "test_task.h"
#include "kaapi++"

/* Main of the program
*/
void doit::operator()(int argc, char** argv )
{
  int N = 1;
  if (argc >1)
    N = atoi(argv[1]);
  if (N<1) N=1;

  /* rpwp -> all other modes */
  ka::pointer<int> p1 = ka::Alloca<int>(1);
  ka::pointer<int> p2 = ka::Alloca<int>(1);
  std::cout << "Begin iteration 0.." << N << std::endl;

  for (int t=0; t<5; ++t)
  {
    kaapi_thread_register_t regs;
    kaapi_thread_save(kaapi_self_thread(), &regs);
    for (int i=0; i<N; ++i)
    {
      ka::Spawn<TaskW<int> >()(p1);
      ka::Spawn<TaskR<int> >()(p1);
      ka::Spawn<TaskW<int> >()(p2);
      ka::Spawn<TaskR<int> >()(p2);
      ka::Spawn<TaskR<int> >()(p1);
      ka::Spawn<TaskR<int> >()(p2);
    }
    ka::Sync();
    kaapi_thread_restore(kaapi_self_thread(), &regs);
    std::cout << "Time:" << t << std::endl;
  }
  std::cout << "End iteration 0.." << N << std::endl;
}
