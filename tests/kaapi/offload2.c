/*
 ** xkaapi
 **
 **
 ** Copyright 2009,2010,2011,2012 INRIA.
 **
 ** Contributors :
 **
 ** thierry.gautier@inrialpes.fr
 **
 ** This software is a computer program whose purpose is to execute
 ** multithreaded computation with data flow synchronization between
 ** threads.
 **
 ** This software is governed by the CeCILL-C license under French law
 ** and abiding by the rules of distribution of free software.  You can
 ** use, modify and/ or redistribute the software under the terms of
 ** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
 ** following URL "http://www.cecill.info".
 **
 ** As a counterpart to the access to the source code and rights to
 ** copy, modify and redistribute granted by the license, users are
 ** provided only with a limited warranty and the software's author,
 ** the holder of the economic rights, and the successive licensors
 ** have only limited liability.
 **
 ** In this respect, the user's attention is drawn to the risks
 ** associated with loading, using, modifying and/or developing or
 ** reproducing the software by the user in light of its specific
 ** status of free software, that may mean that it is complicated to
 ** manipulate, and that also therefore means that it is reserved for
 ** developers and experienced professionals having in-depth computer
 ** knowledge. Users are therefore encouraged to load and test the
 ** software's suitability as regards their requirements in conditions
 ** enabling the security of their systems and/or data to be ensured
 ** and, more generally, to use and operate it in the same conditions
 ** as regards security.
 **
 ** The fact that you are presently reading this means that you have
 ** had knowledge of the CeCILL-C license and that you accept its
 ** terms.
 ** 
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "kaapi.h"

/*
 * Execute with:
KAAPI_CPUSET=threads  KAAPI_PLUGIN_PATH=../../../src/offload/plugin ./offload2
 */

extern int	kaapi_offload_init(int flag);
extern void	kaapi_offload_finalize(void);
#if defined(__linux__)
extern __thread struct kaapi_processor_t*      kaapi_current_processor_key;
static inline struct kaapi_processor_t* kaapi_self_processor(void)
{ return kaapi_current_processor_key; }
#elif defined(__APPLE__)
extern pthread_key_t kaapi_current_processor_key;
static inline struct kaapi_processor_t* kaapi_self_processor(void)
{ return ((struct kaapi_processor_t*)pthread_getspecific( kaapi_current_processor_key )); }
#endif

typedef struct foo_arg_t {
	int		n;
}		foo_arg_t;

void 
foo_body(kaapi_task_t * task, kaapi_thread_t * thread)
{
	foo_arg_t      *arg = (foo_arg_t *) kaapi_task_getargs(task);
	unsigned int	kid = kaapi_self_kid();
	struct kaapi_team_t *team = kaapi_self_team();
	printf("foo number %d kid=%u team=%p\n", arg->n, kid, (void*)team);
}

static		size_t
foo_task_format_get_size(const struct kaapi_format_t *fmt, const void *sp)
{
	return sizeof(foo_arg_t);
}

static void
foo_task_format_task_copy(const struct kaapi_format_t *fmt, void *sp_dest, const void *sp_src)
{
	memcpy(sp_dest, sp_src, sizeof(foo_arg_t));
}

static
unsigned int 
foo_task_format_get_count_params(const struct kaapi_format_t *fmt, const void *sp)
{
	return 1;
}

static
kaapi_access_mode_t 
foo_task_format_get_mode_param(
	    const struct kaapi_format_t *fmt, unsigned int i, const void *sp
)
{
	return KAAPI_ACCESS_MODE_V;
}


static
void           *
foo_task_format_get_data_param(
	    const struct kaapi_format_t *fmt, unsigned int i, const void *sp
)
{
	//foo_arg_t * arg = (foo_arg_t *) sp;
	//void         *retval;
	//retval = (uintptr_t) arg->n;
	//return retval;
	return 0;
}

static
kaapi_access_t *
foo_task_format_get_access_param(
	    const struct kaapi_format_t *fmt, unsigned int i, const void *sp
)
{
	return 0;
}

static
void 
foo_task_format_set_access_param(
				 const struct kaapi_format_t *fmt, unsigned int i, void *sp, const kaapi_access_t * a
)
{
}

static
const struct kaapi_format_t *
foo_task_format_get_fmt_param(
	    const struct kaapi_format_t *fmt, unsigned int i, const void *sp
)
{
	foo_arg_t      *arg __attribute__((unused)) = (foo_arg_t *) sp;
	return kaapi_char_format;
}

static void 
foo_task_format_get_view_param(
	   const struct kaapi_format_t *fmt, unsigned int i, const void *sp,
			       kaapi_memory_view_t * view
)
{
	kaapi_memory_view_make1d(view, 0, 1, 1);
}

static
void 
foo_task_format_set_view_param(
			       const struct kaapi_format_t *fmt, unsigned int i, void *sp, const kaapi_memory_view_t * view
)
{
	kaapi_abort(__LINE__, __FILE__, "invalid case");
}

static
void 
foo_task_format_reducor(
   const struct kaapi_format_t *fmt, unsigned int i, void *sp, const void *v
)
{
	foo_arg_t      *arg __attribute__((unused)) = (foo_arg_t *) sp;
}

static
void 
foo_task_format_redinit(
   const struct kaapi_format_t *fmt, unsigned int i, const void *sp, void *v
)
{
	foo_arg_t      *arg __attribute__((unused)) = (foo_arg_t *) sp;
}

static void body_parallel_region1(void* arg)
{
    kaapi_thread_t *thread;
    unsigned int kid = kaapi_self_kid();
//    int ret;
    
    printf("I'm thread region1 self_tid:%i\n", kid );
    thread = kaapi_self_thread();    
    if(kid == 0){
        foo_arg_t      *task;
        int		i;
        int		n = 20;
        
        for (i = 0; i < n; i++) {
            task = (foo_arg_t *) kaapi_thread_pushdata(thread, sizeof(foo_arg_t));
            task->n = i;
            kaapi_task_push_withflags(thread, foo_body, task, KAAPI_TASK_FLAG_INDPENDENT | KAAPI_TASK_FLAG_DEFAULT);
        }
    }
//    kaapi_sched_sync(thread);
  kaapi_team_barrier_wait( kaapi_self_team(), kaapi_self_processor(), KAAPI_BARRIER_FLAG_DEFAULT );
}

struct kaapi_format_t *foo_task_format = 0;

/*
 * main entry point : Kaapi initialization
 */
int 
main(int argc, char **argv)
{
    struct kaapi_team_t* team;

    /* start only main thread */
    kaapi_init(0, &argc, &argv);

    team = kaapi_begin_parallel( 4, 0 );
    foo_task_format = kaapi_format_allocate();
    kaapi_format_taskregister_func(
           foo_task_format,
           (void*) (uintptr_t)foo_body,
           (kaapi_task_body_t) foo_body,
           "foo_task_format",
           0,	/* get name */
           foo_task_format_get_size,
           foo_task_format_task_copy,
           foo_task_format_get_count_params,
           foo_task_format_get_mode_param,
           foo_task_format_get_data_param,
           foo_task_format_get_access_param,
           foo_task_format_set_access_param,
           foo_task_format_get_fmt_param,
           foo_task_format_get_view_param,
           foo_task_format_set_view_param,
           foo_task_format_reducor,
           foo_task_format_redinit,
	   0,	/* splitter */
	   0    /* affinity */
           );
    kaapi_start_parallel( team, body_parallel_region1, 0, 1 );
    kaapi_end_parallel( team );

    kaapi_finalize();
    
	printf("Success\n");
	return 0;
}
