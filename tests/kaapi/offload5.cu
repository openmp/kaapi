
extern "C" 
__global__ void offload5_kernel(int* out, const int* in, int n)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	if(idx < n)
		out[idx] = in[idx];
}

