/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include "kaapi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>

#include <sys/mman.h>

extern void kaapi_dump_numa_info();

/* main entry point : Kaapi initialization
*/
int main(int argc, char** argv)
{  
  int j;
  int ldcount;
  kaapi_ldset_t ldset;
  char* result;
  char* tmp;
  size_t blocsize;

  /* start only main thread */
  kaapi_init(0, &argc, &argv);

  ldcount = kaapi_ldset_count(KAAPI_HWS_LEVELID_NUMA);
  ldset = kaapi_all_lds( KAAPI_HWS_LEVELID_NUMA );

  blocsize= getpagesize() *8;

  result = (char*)kaapi_ldset_allocate_bloc1dcyclic( 
      ldset, 
      ldcount * blocsize,
      blocsize
  );
  if (result ==0) 
  {
    printf("Error: '%s'\n", strerror(errno) );
    exit(-1);
  }

  /* test alignment */
  if (( (uintptr_t)result & (getpagesize()-1)) !=0)
  {
    printf("*** Error: bad alignment: %p\n", (void*)result);
    exit(-1);
  }

#if 0
  if (mprotect(result, ldcount*blocsize, PROT_READ|PROT_WRITE))
    printf("***Cannot set protection\n");
#endif
  for (j=0; j<ldcount*blocsize; j+=blocsize)
    result[j] = 10;
#if 0
  if (mprotect(result, ldcount*blocsize, PROT_READ))
    printf("***Cannot set protection\n");
#endif
#if 0
  /* touch page for allocation */
  bzero( result, ldcount*blocsize);
#endif
  
  //kaapi_dump_numa_info();

#if 0
  /* display mapping */
  tmp = result;
  for (j=0; j<ldcount; ++j, tmp += blocsize )
  {
    kaapi_ldid_t ldid = kaapi_ldset_getpage_id(tmp );
    if (ldid == (kaapi_ldid_t)-1)
    {
      printf("*** kaapi_ldset_getpage_id error: '%s'\n", strerror(errno) );
      exit(-1);
    }
    printf("*** Mapping bloc %i, mapped on LD: %i, should be on LD: %i\n", j, ldid, j);
  }
#endif

  /* verify mapping */
  tmp = result;
  for (j=0; j<ldcount; ++j, tmp += blocsize )
  {
    kaapi_ldid_t ldid = kaapi_ldset_getpage_id(tmp );
    if (ldid != j)
    {
      printf("*** Error: Mapping bloc %i, mapped on LD: %i, should be on LD: %i\n", j, ldid, j);
      exit(-1);
    }
  }

  kaapi_finalize();
  printf("Success\n");  
  return 0;
}

