
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define USE_CUDA 1

#if defined(USE_CUDA)
#include <cuda.h>
#endif

#include "kaapi.h"

/*
 * Execute with:
 */

#if defined(__linux__)
extern __thread struct kaapi_processor_t*      kaapi_current_processor_key;
static inline struct kaapi_processor_t* kaapi_self_processor(void)
{ return kaapi_current_processor_key; }
#elif defined(__APPLE__)
extern pthread_key_t kaapi_current_processor_key;
static inline struct kaapi_processor_t* kaapi_self_processor(void)
{ return ((struct kaapi_processor_t*)pthread_getspecific( kaapi_current_processor_key )); }
#endif

typedef struct {
    kaapi_access_t addr;
    size_t         size;
} foo_param_t ;

typedef struct foo_arg_t {
    foo_param_t param[3];
} foo_arg_t;

void foo_body(kaapi_task_t * task, kaapi_thread_t * thread)
{
    int i;
	foo_arg_t      *arg = (foo_arg_t *) kaapi_task_getargs(task);
	unsigned int	kid = kaapi_self_kid();
	struct kaapi_team_t *team = kaapi_self_team();
    int* in = (int*)arg->param[1].addr.data;
    int* out = (int*)arg->param[2].addr.data;
    int n = *((int*)arg->param[0].addr.data);
    for(i = 0; i < n; i++)
        out[i] = in[i];
    
	printf("HOST foo number %d (in=%p out=%p) kid=%u team=%p arg=%p\n", n, (void*)in, (void*)out,
           kid, (void*)team, (void*)arg);
}

#if defined(USE_CUDA)
void foo_body_device(kaapi_task_t * task, kaapi_thread_t * thread)
{
    foo_arg_t      *arg = (foo_arg_t *) kaapi_task_getargs(task);
    unsigned int	kid = kaapi_self_kid();
    struct kaapi_team_t *team = kaapi_self_team();
    int* in = (int*)arg->param[1].addr.data;
    int* out = (int*)arg->param[2].addr.data;
    int n = *((int*)arg->param[0].addr.data);
    CUmodule module;
    CUfunction offload5_kernel;
    CUresult res;
    
    cuModuleLoad(&module, "offload5.ptx");
    cuModuleGetFunction(&offload5_kernel, module, "offload5_kernel");
    int threads_per_block = 64;
    int blocks_per_grid = (n + threads_per_block - 1)/threads_per_block;
    void* args[] = { &out, &in, &n };
    res = cuLaunchKernel(offload5_kernel,
                         blocks_per_grid /* gridDimX */, 1 /* gridDimY */, 1 /* gridDimZ */,
                         threads_per_block /* blockDimX */, 1 /* blockDimY */, 1 /* blockDimZ */,
                         0 /* sharedMem */, 0 /* stream */, args, NULL /* extra */
                         );
    printf("DEVICE foo number %d (in=%p out=%p) kid=%u team=%p arg=%p code=%d\n", n, (void*)in, (void*)out,
           kid, (void*)team, (void*)arg, (int)res);
}
#endif

static	size_t
foo_task_format_get_size(const struct kaapi_format_t *fmt, const void *sp)
{
	return sizeof(foo_arg_t);
}

static void
foo_task_format_task_copy(const struct kaapi_format_t *fmt, void *sp_dest, const void *sp_src)
{
	memcpy(sp_dest, sp_src, sizeof(foo_arg_t));
}

static
unsigned int 
foo_task_format_get_count_params(const struct kaapi_format_t *fmt, const void *sp)
{
	return 3;
}

static
kaapi_access_mode_t 
foo_task_format_get_mode_param(
	    const struct kaapi_format_t *fmt, unsigned int i, const void *sp
)
{
    foo_arg_t* arg = (foo_arg_t*)sp;
    return arg->param[i].addr.mode;
}


static
void*
foo_task_format_get_data_param(
	    const struct kaapi_format_t *fmt, unsigned int i, const void *sp
)
{
    foo_arg_t* arg = (foo_arg_t*)sp;
    kaapi_assert_debug((arg->param[i].addr.mode & KAAPI_ACCESS_MODE_V));
//    fprintf(stdout, "%s access %d data %p\n", __FUNCTION__, i, arg->param[i].addr.data);
    return arg->param[i].addr.data;
}

static
kaapi_access_t *
foo_task_format_get_access_param(
	    const struct kaapi_format_t *fmt, unsigned int i, const void *sp
)
{
    foo_arg_t *arg = (foo_arg_t *) sp;
    kaapi_access_t* retval = 0;
    kaapi_assert_debug((arg->param[i].addr.mode & KAAPI_ACCESS_MODE_V) == 0);
    retval = &arg->param[i].addr;
//    fprintf(stdout, "%s access %d mode %d data %p\n", __FUNCTION__, i, retval->mode, retval->data);
    return retval;
}

static
void 
foo_task_format_set_access_param(
				 const struct kaapi_format_t *fmt, unsigned int i, void *sp, const kaapi_access_t * a
)
{
	foo_arg_t *arg = (foo_arg_t *) sp;
    kaapi_assert_debug((arg->param[i].addr.mode & KAAPI_ACCESS_MODE_V) == 0);
    arg->param[i].addr = *a;
//    fprintf(stdout, "%s access %d mode %d data %p\n", __FUNCTION__, i, a->mode, a->data);
}

static
const struct kaapi_format_t *
foo_task_format_get_fmt_param(
	    const struct kaapi_format_t *fmt, unsigned int i, const void *sp
)
{
	foo_arg_t      *arg __attribute__((unused)) = (foo_arg_t *) sp;
	return kaapi_char_format;
}

static void 
foo_task_format_get_view_param(
	   const struct kaapi_format_t *fmt, unsigned int i, const void *sp,
			       kaapi_memory_view_t * view
)
{
	foo_arg_t *arg = (foo_arg_t *) sp;
    kaapi_memory_view_make1d(view, 0, arg->param[i].size, 1);
}

static
void 
foo_task_format_set_view_param(
			       const struct kaapi_format_t *fmt, unsigned int i, void *sp, const kaapi_memory_view_t * view
)
{
	kaapi_abort(__LINE__, __FILE__, "invalid case");
}

static
void 
foo_task_format_reducor(
   const struct kaapi_format_t *fmt, unsigned int i, void *sp, const void *v
)
{
	foo_arg_t      *arg __attribute__((unused)) = (foo_arg_t *) sp;
}

static
void 
foo_task_format_redinit(
   const struct kaapi_format_t *fmt, unsigned int i, const void *sp, void *v
)
{
	foo_arg_t      *arg __attribute__((unused)) = (foo_arg_t *) sp;
}

static void body_parallel_region1(void* arg)
{
    kaapi_thread_t *thread;
    int		i;
    int		n = 20;
//    int ret;
    int* input;
    int* output;
    unsigned int kid = kaapi_self_kid();

    printf("I'm thread region1 self_tid:%i\n", kid );
    if(kid == 0){
        input = (int*)malloc(100*sizeof(int));
        output = (int*)malloc(100*sizeof(int));
        for(i= 0; i < 100; i++){
            input[i] = 1;
            output[i] = 0;
        }
    }
    
    thread = kaapi_self_thread();
    if(kid == 0){
        for (i = 0; i < n; i++) {
            foo_arg_t *task = (foo_arg_t *) kaapi_thread_pushdata(thread, sizeof(foo_arg_t));
            kaapi_access_init(&task->param[0].addr, 0);
            task->param[0].addr.mode = KAAPI_ACCESS_MODE_V;
            task->param[0].addr.data = malloc(sizeof(int));
            *((int*)task->param[0].addr.data) = (int)(100/n);
            task->param[0].size = 1;
            
            kaapi_access_init(&task->param[1].addr, 0);
            task->param[1].addr.mode = KAAPI_ACCESS_MODE_R;
            task->param[1].addr.data = input+i*(100/n);
            task->param[1].size = (100/n)*sizeof(int);
            
            kaapi_access_init(&task->param[2].addr, 0);
            task->param[2].addr.mode = KAAPI_ACCESS_MODE_W;
            task->param[2].addr.data = output+i*(100/n);
            task->param[2].size = (100/n)*sizeof(int);
            
//            fprintf(stdout, "TASK %p size=%d input=%p output=%p\n", (void*)task, (int)(100/n),
//                    (void*)task->param[1].addr.data, (void*)task->param[2].addr.data);
//            fflush(stdout);
            
            kaapi_task_push_withflags(thread, foo_body, task, KAAPI_TASK_FLAG_DEFAULT);
        }
    }
    kaapi_team_barrier_wait( kaapi_self_team(), kaapi_self_processor(), KAAPI_BARRIER_FLAG_DEFAULT );
    if(kid == 0)
      kaapi_memory_sync();
    
    if(kid == 0){
        for(i = 0; i < 100; i++)
            if(input[i] != output[i]){
                printf("Error: result error\n");
                exit(-1);
            }
        free(input);
        free(output);        
    }
}

static void task_register_foo1(void)
{
    struct kaapi_format_t *foo_task_format = kaapi_format_allocate();
    kaapi_format_taskregister_func(
                                   foo_task_format,
                                   (void*) foo_body,
                                   (kaapi_task_body_t) foo_body,
                                   "foo_task_format",
                                   0,	/* get name */
                                   foo_task_format_get_size,
                                   foo_task_format_task_copy,
                                   foo_task_format_get_count_params,
                                   foo_task_format_get_mode_param,
                                   foo_task_format_get_data_param,
                                   foo_task_format_get_access_param,
                                   foo_task_format_set_access_param,
                                   foo_task_format_get_fmt_param,
                                   foo_task_format_get_view_param,
                                   foo_task_format_set_view_param,
                                   foo_task_format_reducor,
                                   foo_task_format_redinit,
                                   0,	/* splitter */
                                   0    /* affinity */
                                   );
#if defined(USE_CUDA)
    kaapi_format_taskregister_body(foo_task_format, foo_body_device, KAAPI_PROC_TYPE_CUDA);
#endif
}

/*
 * main entry point : Kaapi initialization
 */
int
main(int argc, char **argv)
{
    struct kaapi_team_t* team;
    
    /* start only main thread */
    kaapi_init(0, &argc, &argv);
    
    task_register_foo1();
    
    team = kaapi_begin_parallel( 10, 0 );
    kaapi_start_parallel( team, body_parallel_region1, 0, 1 );
    kaapi_end_parallel( team );
    
    kaapi_finalize();
    
    printf("Success\n");
    return 0;
}
