/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/

#include "kaapi.h"
#include <stdio.h>
#include <stdlib.h>

static void body_parallel_region(void* arg)
{
    unsigned int kid = kaapi_self_kid();
    struct kaapi_team_t* team = kaapi_self_team();
    kaapi_memory_view_t view;
    char* ptr = (char*)malloc(sizeof(char)*10);
    kaapi_memory_view_make1d(&view, 0, 10, sizeof(char));
    struct kaapi_metadata_info_t* mdi = kaapi_memory_bind(0, ptr, view, 0);
    
    fprintf(stdout, "Thread region1 tid=%i team=%p ptr=%p mdi=%p\n", kid, (void*)team, (void*)ptr, (void*)mdi);
    struct kaapi_metadata_info_t* tmp = kaapi_memory_bind(0, ptr, view, 0);
    if( (tmp == 0) || (tmp != mdi) )
        fprintf(stderr, "Error: pointer not found tid=%i team=%p ptr=%p mdi=%p\n", kid, (void*)team, (void*)ptr, (void*)mdi);
 
    kaapi_memory_unbind(0, ptr, view);
    free(ptr);
}


/* main entry point : Kaapi initialization
*/
int main(int argc, char** argv)
{  
  struct kaapi_team_t* team;

  /* start only main thread */
  kaapi_init(0, &argc, &argv);
    
  team = kaapi_begin_parallel( 10, 0 );
  kaapi_start_parallel( team, body_parallel_region, 0, 1 );
  kaapi_end_parallel( team );

  kaapi_finalize();
  printf("Success\n");  
  return 0;
}

