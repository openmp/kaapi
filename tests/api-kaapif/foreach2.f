      PROGRAM FOREACH
      IMPLICIT NONE

      INCLUDE 'kaapif.inc.f90'

      CHARACTER*40 :: KAAPIVERS
      INTEGER :: P=100, S=500, I, J, err
      INTEGER, ALLOCATABLE ::PROD(:),SOM(:)
      ALLOCATE(PROD(P),SOM(S))

      err = KAAPIF_INIT(1)

      KAAPIVERS=' '
      WRITE(*,*) 'Nb threads : ',KAAPIF_GET_CONCURRENCY()
      CALL KAAPIF_GET_VERSION(KAAPIVERS)
      WRITE(*,*) 'KAAPI_VERSION : ',TRIM(KAAPIVERS)
!----- Pre-distribution among core, without dynamic load balancing scheme
      err = KAAPIF_SET_POLICY( KAAPIF_POLICY_STATIC )
!----- Distribution over numa node used by the runing parallel region
      err = KAAPIF_SET_DISTRIBUTION( KAAPIF_LD_NUMA, 1) 
      err = KAAPIF_FOREACH(1, P, 0, PROD_TASK)

      err = KAAPIF_FINALIZE()
      
      CONTAINS
      
!------Boucle externe      
      SUBROUTINE PROD_TASK(DEB, FIN, ITHR)
      INTEGER :: DEB, FIN, ITHR, P
      WRITE(10+ITHR,*) 'NTH-PROD',ITHR,'I',DEB,'J',FIN
      
      END SUBROUTINE PROD_TASK
      
      END PROGRAM FOREACH         

