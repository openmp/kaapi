      PROGRAM FOREACH
!    Ce petit code permet de calculer une somme d'un produit calcul� dans une 
!    boucle externe et de l'indice courant d'une boucle interne. 
!    (dans une boucle imbriqu�e, pour chaque prod(i),on incr�mente 
!    la somme SOM(j)).     
      IMPLICIT NONE
      CHARACTER*40 :: KAAPIVERS
      INTEGER :: P=100, S=500
      INTEGER, ALLOCATABLE ::PROD(:),SOM(:)
      INTEGER, EXTERNAL :: KAAPIF_GET_CONCURRENCY
      ALLOCATE(PROD(P),SOM(S))
      CALL KAAPIF_INIT(1)
      KAAPIVERS=' '
      WRITE(*,*) 'Nb threads : ',KAAPIF_GET_CONCURRENCY()
      CALL KAAPIF_GET_VERSION(KAAPIVERS)
      WRITE(*,*) 'KAAPI_VERSION : ',TRIM(KAAPIVERS)
      
      
      CALL KAAPIF_FOREACH(1, P, 0, PROD_TASK)

      CALL KAAPIF_FINALIZE()
      
      
      
      CONTAINS
      
!------Boucle externe      
      SUBROUTINE PROD_TASK(DEB, FIN, ITHR)
       INTEGER :: DEB, FIN, ITHR
       WRITE(10+ITHR,*) 'NTH-PROD',ITHR,'I',DEB,'J',FIN
      
      END SUBROUTINE PROD_TASK
      
      END PROGRAM FOREACH         
          
