      PROGRAM FOREACH
      IMPLICIT NONE

      INCLUDE 'kaapif.inc.f95'

      CHARACTER*40 :: KAAPIVERS
      INTEGER :: P=100, S=500, I, J, err

      err = KAAPIF_INIT(1)

      KAAPIVERS=' '
      WRITE(*,*) 'Nb threads : ',KAAPIF_GET_CONCURRENCY()
      CALL KAAPIF_GET_VERSION(KAAPIVERS)
      WRITE(*,*) 'KAAPI_VERSION : ',TRIM(KAAPIVERS)

!----- Pre-distribution among core, without dynamic load balancing scheme
      err = KAAPIF_SET_POLICY( KAAPIF_POLICY_STATIC )
!----- Distribution over numa node used by the runing parallel region
      err = KAAPIF_SET_DISTRIBUTION( KAAPIF_LD_NUMA, 1)
      err = KAAPIF_FOREACH(1, P, 0, PROD_TASK)

      err = KAAPIF_FINALIZE()

      CONTAINS

!------Boucle externe
      SUBROUTINE PROD_TASK(DEB, FIN, ITHR)
       INTEGER :: DEB, FIN, ITHR, err
       INTEGER :: KAAPIF_SET_AFFINITY
       WRITE(10+ITHR,*) 'NTH-PROD',ITHR,'I',DEB,'J',FIN
!----- Iterations scheduled on cores with NUMA affinity/running core
       err = KAAPIF_SET_AFFINITY(KAAPIF_LD_NUMA, 1)

       err = KAAPIF_FOREACH(DEB, FIN, 1, SOM_TASK, ITHR)

      END SUBROUTINE PROD_TASK


!------Boucle interne
      SUBROUTINE SOM_TASK(IELOOP,IFLOOP,NTH,ITHR)
       INTEGER :: IELOOP, IFLOOP, NTH, ITHR

       WRITE(60+NTH,*) 'SUM-',NTH,'I',IELOOP,'J',IFLOOP,' THR',ITHR

      END SUBROUTINE SOM_TASK


      END PROGRAM FOREACH
