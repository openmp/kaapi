#What is XKAAPI?

XKAAPI is a runtime for scheduling irregular fine grain tasks with data flow dependencies. It could be used through OpenMP-4.0 compliant applications using GNU C or C++ compiler, Intel compilers or our the research C/C++ source-to-source compiler [KSTAR](http://kstar.gforge.inria.fr)

It is a C library that allows to execute multithreaded computation with data flow synchronization between threads.
The library is able to schedule fine/medium size grain program on distributed machine. The data flow graph is dynamic (unfold at runtime). Target architectures are clusters of SMP machines.

Extension to OpenMP-4.0 are provided through new OpenMP runtime functions or by using other APIs. This extensions deals with:

* memory bindings on NUMA system
* task affinity
* new loop scheduler
* adaptive task model 

XKaapi is a [MOAIS software](http://moais.imag.fr)

