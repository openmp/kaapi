#Publications
Here a list of selected publications about Kaapi which have been published by [MOAIS team](http://moais.imag.fr).
A complete list of MOAIS team publication can be found [here](http://pistou.imag.fr/site/team/19?tri=date%20type).

##Citing XKaapi
MultiGPUs implementation with recent version have been described in:
\*Thierry Gautier, Joao Vicente Ferreira Lima, Nicolas Maillard, Bruno Raffin. **XKaapi: A Runtime System for Data-Flow Task Programming on Heterogeneous Architectures.** In Proc. of the 27-th IEEE International Parallel and Distributed Processing Symposium (IPDPS), Boston, USA, jun 2013.

Older implementation with support for cluster and grid computing is described in:
\*Thierry Gautier, Xavier Besseron, Laurent Pigeon. **KAAPI: A Thread Scheduling Runtime System for Data Flow Computations on Cluster of Multi-Processors.** Parallel Symbolic Computation'07 (PASCO'07), (15–23), London, Ontario, Canada, 2007.

##OpenMP & Kaapi
* Philippe Virouleau, François Broquedis, Thierry Gautier, Fabrice Rastello. Using Data Dependencies to Improve Task-Based Scheduling Strategies on NUMA Architectures. Euro-Par 2016: 531-544
* Philippe Virouleau, Adrien Roussel, François Broquedis, Thierry Gautier, Fabrice Rastello, Jean-Marc Gratien: Description, Implementation and Evaluation of an Affinity Clause for Task Directives.  Proceedings of the 12th International Conference on OpenMP in a Heterogeneous World (IWOMP), Osaka, Japan, oct 2016
* Philippe Virouleau , Pierrick Brunet, Francois Broquedis, Nathalie Furmento, Samuel Thibault, Olivier Aumage, Thierry Gautier. Evaluation of the OpenMP Dependent Tasks with the KASTORS Benchmarks Suite. Proceedings of the 10th International Conference on OpenMP in a Heterogeneous World (IWOMP), Bahia, Brazil, sep 2014.	
* Marie Durand, Francois Broquedis, Thierry Gautier, Bruno Raffin. An Efficient OpenMP Loop Scheduler for Irregular Applications on Large-Scale NUMA Machines. Proceedings of the 9th International Conference on OpenMP in a Heterogeneous World (IWOMP), 8122:141-155, Lecture Notes in Computer Science, Canberra, Australia, sep 2013.	
* Francois Broquedis, Thierry Gautier, Vincent Danjean. libKOMP, an Efficient OpenMP Runtime System for Both Fork-Join and Data Flow Paradigms. IWOMP, :102-115, Rome, Italy, 2012.

##Data flow programming language
* Thierry Gautier, Fabien Le Mentec, Vincent Faucher, Bruno Raffin. X-kaapi: A Multi Paradigm Runtime for Multicore Architectures. ICPP 2013: 728-735
* Marc Tchiboukdjian, Nicolas Gast, Denis Trystram: Decentralized List Scheduling CoRR abs/1107.3734: (2011). To appear.
* Thierry Gautier, Xavier Besseron, Laurent Pigeon. KAAPI: A Thread Scheduling Runtime System for Data Flow Computations on Cluster of Multi-Processors. Parallel Symbolic Computation'07 (PASCO'07), (15–23), London, Ontario, Canada, 2007.
* François Galilée, Jean-Louis Roch, Gerson Cavalheiro, Mathias Doreille. Athapascan-1: On-line Building Data Flow Graph in a Parallel Language. International Conference on Parallel Architectures and Compilation Techniques, PACT'98, :88–95, Paris, France, oct 1998.
* Gerson Cavalheiro, François Galilée, Jean-Louis Roch. Athapascan-1: Parallel Programming with Asynchronous Tasks. Proceedings of the Yale Multithreaded Programming Workshop, Yale, USA, jun 1998.
* Thierry Gautier, Jean-Louis Roch, Frédéric Wagner. Fine Grain Distributed Implementation of a Dataflow Language with Provable Performances. Workshop PAPP 2007 - Practical Aspects of High-Level Parallel Programming in International Conference on Computational Science 2007 (ICCS2007), Beijing, China, may 2007.
* Parallel applications
* Laurent Pigeon, Pascal Roux, Bertrand Braunschweig, Thierry Gautier. Dynamic CAPE-OPEN Simulation Approach on Cluster Oriented Architecture. Proceedings de AIChE 2006 Annual Meeting, San Francisco, USA, 2006.
* Bogdan Dumitrescu, Mathias Doreille, Jean-Louis Roch, Denis Trystram. Two-dimensional block partitionings for the parallel sparse Cholesky factorization. Numerical Algorithms, 16:17–38, 1997.

##CPU and GPU
* João V. F. Lima, Thierry Gautier, Vincent Danjean, Bruno Raffin, Nicolas Maillard. Design and analysis of scheduling strategies for multi-CPU and multi-GPU architectures. Parallel Computing 44: 37-52 (2015)
* Raphaël Bleuse, Thierry Gautier, João V. F. Lima, Grégory Mounié, Denis Trystram. Scheduling Data Flow Program in XKaapi: A New Affinity Based Algorithm for Heterogeneous Architectures. Euro-Par 2014: 560-571
* Thierry Gautier, Joao Vicente Ferreira Lima, Nicolas Maillard, Bruno Raffin. XKaapi: A Runtime System for Data-Flow Task Programming on Heterogeneous Architectures. In Proc. of the 27-th IEEE International Parallel and Distributed Processing Symposium (IPDPS), Boston, USA, jun 2013.
* Thierry Gautier, Joao Vicente Ferreira Lima, Nicolas Maillard, Bruno Raffin. Locality-Aware Work Stealing on Multi-CPU and Multi-GPU Architectures. 6th Workshop on Programmability Issues for Heterogeneous Multicores (MULTIPROG), Berlin, Allemagne, jan 2013.
* Julio Toss, Thierry Gautier. A New Programming Paradigm for GPGPU. EUROPAR 2012, Rhodes Island, Greece, aug 2012.
* J.V.F. Lima, Thierry Gautier, Nicolas Maillard, Vincent Danjean. Exploiting Concurrent GPU Operations for Efficient Work Stealing on Multi-GPUs. 24rd International Symposium on Computer Architecture and High Performance Computing (SBAC-PAD), Columbia University, New York, USA, oct 2012.
* Everton Hermann, Bruno Raffin, François Faure, Thierry Gautier, Jérémie Allard. Multi-GPU and Multi-CPU Parallelization for Interactive Physics Simulations. EUROPAR 2010, Ischia Naples, Italy, aug 2010.
* Everton Hermann, Bruno Raffin, François Faure. Interactive Physical Simulation on Multicore Architectures. Eurographics 2009 Symposium on Parallel Graphics and Visualization (EGPGV'09), :1–8, Munich, Germany, mar 2009.
* Everton Hermann, François Faure, Bruno Raffin. Ray-traced Collision Detection for Deformable Bodies. 3rd International Conference on Computer Graphics Theory and Applications (GRAPP), :293–299, Madeira, Portugal, jan 2008.

##Algorithms & Adaptive algorithms
* Jean-Guillaume Dumas, Thierry Gautier, Clément Pernet, Jean-Louis Roch, Ziad Sultan. Recursion based parallelization of exact dense linear algebra routines for Gaussian elimination. Parallel Computing 57: 235-249 (2016)
* Thierry Gautier, Jean-Louis Roch, Ziad Sultan, Bastien Vialla. Par.llel algebraic linear algebra dedicated interface. PASCO 2015: 34-43
* Jean-Guillaume Dumas, Thierry Gautier, Clément Pernet, Ziad Sultan. Parallel Computation of Echelon Forms. Euro-Par 2014: 499-510
* Mathias Ettinger, François Broquedis, Thierry Gautier, Stéphane Ploix, Bruno Raffin. VtkSMP: Task-based Parallel Operators for VTK Filters. EGPGV 2013: 41-48
* Daouda Traore, Jean-Louis Roch, Nicolas Maillard, Thierry Gautier, Julien Bernard. Deque-free work-optimal parallel STL algorithms. EUROPAR 2008, Las Palmas, Spain, aug 2008.
* Jean-Louis Roch, Daouda Traore, Julien Bernard. On-line adaptive parallel prefix computation. EUROPAR 2006, :843–850, Dresden, Germany, aug 2006.
* Van-Dat Cung, Vincent Danjean, Jean-Guillaume Dumas, Thierry Gautier, Guillaume Huard, Bruno Raffin, Christophe Rapine, Jean-Louis Roch, Denis Trystram. Jean-Guillaume Dumas. Adaptive and Hybrid Algorithms: classification and illustration on triangular system solving. Transgressive Computing (TC'2006) Proceedings, :131–148, Granada, Spain, apr 2006.
* Jean-Guillaume Dumas, Thierry Gautier, Jean-Louis Roch. Generic design of Chinese remaindering schemes. PASCO 2010, Grenoble, France, jul 2010.

## Data flow and Fault tolerance, certification
* Xavier Besseron, Thierry Gautier. Impact of over-decomposition on coordinated checkpoint/rollback protocol. Workshop on Resiliency in High-Performance Computing, 17-th International European Conference On Parallel and Distributed Computing, Bordeaux, France, aug 2011.
* Xavier Besseron, Thierry Gautier. Optimised recovery with a coordinated checkpoint/rollback protocol for domain decomposition applications. Modelling, Computation and Optimization in Information Systems and Management Sciences (MCO'08), :497–506, Metz, France, sep 2008.
* Samir Jafar, Axel W. Krings, Thierry Gautier. Flexible Rollback Recovery in Dynamic Heterogeneous Grid Computing. IEEE Transactions on Dependable and Secure Computing, 6(1), 2009.
* Samir Jafar, Thierry Gautier, Axel W. Krings, Jean-Louis Roch. A Checkpoint/Recovery Model for Heterogeneous Dataflow Computations Using Work-Stealing (http://europar05.di.fct.unl.pt/). EUROPAR'2005, Lisboa, Portogal, aug 2005.
* Samir Jafar, Axel W. Krings, Thierry Gautier, Jean-Louis Roch. Theft-Induced Checkpointing for Reconfigurable Dataflow Applications. IEEE Electro/Information Technology Conference , (EIT 2005), Lincoln, Nebraska, may 2005.
* Axel W. Krings, Jean-Louis Roch, Samir Jafar. Certification of Large Distributed Computations with Task Dependencies in Hostile Environments. IEEE Electro/Information Technology Conference , (EIT 2005), Lincoln, Nebraska, may 2005.
