##Licenes

X-Kaapi libraries and runtime are distributed under a [CeCILL-C license](http://www.cecill.info/index.en.html).

##Retrieving the sources
There are 2 ways to retrieve the sources:

* By downloading a release [tarball](https://gforge.inria.fr/frs/?group_id=94). Last release is [3.1.1rc01](https://gforge.inria.fr/frs/download.php/latestzip/3493/xkaapi-latest.zip)
* Or by Cloning the project git repository

The tarball of the last stable version of XKaapi is available in the [download page](https://gforge.inria.fr/frs/?group_id=94).
Please visit the gforge Kaapi project at [https://gforge.inria.fr/projects/kaapi](https://gforge.inria.fr/projects/kaapi).

##Git Repository
You can also clone KAAPI git repository as follows.
````Bash
> git clone git://scm.gforge.inria.fr/kaapi/xkaapi.git
```
The stable version is branch `public/xkaapi-3.0`.
