
#Features
XKaapi is a runtime high performance parallelism targeting multicore and distributed architectures. It relies on workstealing paradigms. The core library comes with a full set of complementary programming interfaces, allowing for different abstraction levels. The following documents the install process, runtime options, as well as a description of APIs lying on top of the runtime and a set of examples.
X-Kaapi targets essentially SMP and NUMA platforms. The runtime should run on every system providing:

* GNU toolchain
* the pthread library.

It has been extensively tested on the following operating systems:

* GNU-Linux/x86 64

There is no version for Windows yet.
XKaapi is decribed more in depth in the following documents:

##How to use it ?

##Slides
* Introduction [pdf](http://kaapi.gforge.inria.fr/XKaapi/htdocs/doc_files/0-xkaapi-intro.pdf)
* DFG programming [pdf](http://kaapi.gforge.inria.fr/XKaapi/htdocs/doc_files/1-xkaapi-dfg.pdf) 


##Technical reports
* [X-Kaapi's Application Programming Interface. Part I: Data Flow Programming](http://hal.inria.fr/hal-00648245/fr)
* [X-Kaapi C programming interface](http://hal.inria.fr/hal-00647474/fr)
