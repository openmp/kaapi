#Performance counter

The X-Kaapi library is able to record performance counter of various events that occur during execution of your program (using C++ library, Fotran API or [libKOMP](libkomp.md) for OpenMP). In order to select counters to collect and display, the X-Kaapi should has been configured with the right options.

##Configuring X-Kaapi for performance counter 
The configuration is quite simple. Add the ````--with-perfcounter```` to your command line during configuration:  
```Bash
> ../xkaapi/configure  --prefix=<your prefix> --with-numa --with-perfcounter
> make ; make install
```
This option activates code to collect counters. It also defines software counters for internal runtime events.

In order to exploit hardware counters, you need to have [PAPI](http://icl.cs.utk.edu/papi) installed on your machine. Please read [PAPI documentation](http://icl.cs.utk.edu/projects/papi) for this point.

If you want to use software and hardware counters, the full command line is: 
```Bash
> ../xkaapi/configure  --prefix=<your prefix> --with-numa --with-perfcounter --with-papi=<install-dir papi>
> make ; make install
```
It was tested over PAPI versions 5.1.x, 5.2.x and 5.4.x.


##List of performance counters

Once configured, you may have to select events you want to count. The set of event, assuming X-Kaapi is configured with PAPI are: 

* Software counters managed by the X-Kaapi library
* Hardware counters managed by the PAPI library


The set of available software counters is discribed in the following table.

| Event Name   | Group | Description |
| ----------- |--------|---|
| KAAPI_TASKSPAWN| KAAPI_TASK  | Number of created tasks |
| KAAPI_TASKEXEC| KAAPI_TASK  |  Number of executed tasks |
| KAAPI_TASKSTEAL| KAAPI_TASK  | Number of theft tasks |
| KAAPI_SYNC| KAAPI_TASK  | Number of times sync keyword has been called|
| KAAPI_TIMES| KAAPI_TASK | Times during tasks' execution or duing calls of runtime functions|
| KAAPI_DFGBUILD| KAAPI_DFG  | Times required to build the DFG |
| KAAPI_PERF_ID_RDLISTINIT| KAAPI_DFG  | Times required to initialize computation with DFG |
| KAAPI_PERF_ID_RDLISTEXEC| KAAPI_DFG  | Times passed to execution DFG |
| KAAPI_STEALREQOK| KAAPI_STEAL   | Number of successfull steal requests |
| KAAPI_STEALREQ|   KAAPI_STEAL | Number of steal requests |
| KAAPI_STEALREQOP|  KAAPI_STEAL | Number of steal operations to process steal requests |
| KAAPI_STEALIN|  KAAPI_STEAL | Number of received steal request |
| KAAPI_CONFLICTPOP|  KAAPI_STEAL | Number of conflictual steal/pop |
| KAAPI_STACKSIZE|  KAAPI_STEAL | Size of allocated stack block (in bytes) |
| KAAPI_H2D| KAAPI_OFFLOAD  | Volume of communication from host to device |
| KAAPI_D2H| KAAPI_OFFLOAD  | Volume of communication from device to host |
| KAAPI_D2D| KAAPI_OFFLOAD  | Volume of communication from device to an other device |
| KAAPI_CACHE_HIT| KAAPI_OFFLOAD  | Number of hit in the DSM |
| KAAPI_CACHE_MISS| KAAPI_OFFLOAD  | Number of miss in the DSM |


The set of hardware counters relies on your hardware. List may be displayed by the command [papi_avail](http://icl.cs.utk.edu/projects/papi/wiki/PAPIC:papi_avail.1). Note that native papi events [papi_native_avail](http://icl.cs.utk.edu/projects/papi/wiki/PAPIC:papi_native_avail.1) could also be used with X-Kaapi.

##Execution to capture selected performance counters

Two environment variables control how to record the performance counters:

* KAAPI_PERF_EVENTS: a list of event names to be captured per processus.  
* KAAPI_TASKPERF_EVENTS: a list of event names to be captured per kind of tasks.

Event name could be either X-Kaapi event name or Papi event name.  For instance:
```bash
> KAAPI_DISPLAY_PERF=final \
  KAAPI_PERF_EVENTS=KAAPI_TASK,KAAPI_TIMES,PAPI_TOT_CYC ./fibo_kaapi++ 30
```
collect and display select events for all the worker threads of the processus.
Typicall output is:
```R
## GPLOT format for cumulative (per process) stat
#      Time(s)   #Task Spawn   #Task Exec    #Task Steal    #Sync     PAPI_TOT_CYC  
   2.326794e+01    13462881    13462885         101       2692643     15285372450 
###--- Global performance counters
             Time(s)     (sys/usr): 1.096436e+01, 1.230357e+01
         #Task Spawn     (sys/usr): 44, 13462837
          #Task Exec     (sys/usr): 0, 13462885
         #Task Steal     (sys/usr): 101, 0
               #Sync     (sys/usr): 2692643, 0
        PAPI_TOT_CYC     (sys/usr): 5916764031, 9368608419
``` 

The variabel ```KAAPI_DISPLAY_PERF``` controls the way to display counters (see above). It could force to output counters per worker threads.


##Recording performance counter per task
Usage of ```KAAPI_TASKPERF_EVENTS``` follows the same syntax and behaviours of ```KAAPI_PERF_EVENTS```:
```bash
> KAAPI_DISPLAY_PERF=final \
  KAAPI_PERF_EVENTS=KAAPI_TIMES,PAPI_TOT_CYC ./fibo_kaapi++ 30
```
The counters are collected per task. At the end of the execution (or at the end of parallel region, see ```KAAPI_DISPLAY_PERF``` values above), you have typically such output where for each counter (here times and PAPI_TOT_CYC) you have the cumulated values for all tasks of specific type, the average of these values and the standard deviation. The first column is the name of the tasks, the second the number of instances.
```R
# <perf name>  <count>              Time(s)                         PAPI_TOT_CYC 
   7TaskSum  2692536 1.4879e+00 5.5261e-07 1.4779e-10 1.5559e+09 5.7787e+02 5.4833e-02
0TaskDelete  5384958 3.2225e+00 5.9843e-07 7.8853e-11 3.6753e+09 6.8251e+02 8.9709e-02
  8TaskFibo  5385188 5.3867e+00 1.0003e-06 2.0388e-07 5.0255e+09 9.3320e+02 2.9370e-01
```




##How to display recorded counter ?
To display these counters at the end of the execution, the user defined:
```Bash
> KAAPI_DISPLAY_PERF=full   ./myapp
```
or
```Bash
> KOMP_DISPLAY_PERF=full   ./myapp
```
will display some statistics at the end of the execution.

The possible names for KAAPI_DISPLAY_PERF or KOMP_DISPLAY_PERF are resumed in the following table:

| Name     | Description |
|--------- |-------------|
| no or 0  | (default value) do not display counters |
| yes or 1 | display counters per core at the end of parallel region as well as cumulative counters for the region |
| resume | only display resume at the end of parallel region |
| full | display counters per core at the end of parallel region as well as cumulative counters for the region |
| gplot | same as full but output gnuplot format |
| final | display cumulative counters per cores over all parallel regions as well as resume for the processus | 


