#Authors

XKaapi has been written by the members of the INRIA MOAIS project, namely :

* [Thierry Gautier](http://moais.imag.fr/membres/thierry.gautier), INRIA, Researcher, Main contributor
* Fabien Lementec, INRIA, Ingeneer, developper
* François Broquedis, INP, Assistant Professor, libKOMP the OpenMP runtime on top of XKaapi
* Vincent Danjean, UJF, Assistant Professor, build system

Please use the following mailing-list to contact the KAAPI team: [kaapi-dev-info](http://lists.gforge.inria.fr/cgi-bin/mailman/listinfo/kaapi-dev-info)
