##What is KAAPI and XKAAPI?
KAAPI is the first prototype written in C++ for distributed architecture. KAAPI is able to run program on large cluster and grid using a network interface based on TCP/IP and TakTuk?. KAAPI suffers of large to medium overhead on multicore machine. The high level interface of KAAPI the C++ Athapascan API. KAAPI can yet be downloaded here.
KAAPI is no more supported. Most of the KAAPI feature will be integrated into XKAAPI.
XKAAPI is a much more fine grain implementation based on C. As for KAAPI it is based on a variation of work stealing algorithm with aggregation capability. Current release of XKAAPI is for multicore only. The C++ interface is called Kaapi++. For the first releases of XKaapi, we will support Athapascan interface restricted to multicore.

##What is the release model of XKAAPI?
XKAAPI is released as [compressed tarballs](https://gforge.inria.fr/frs/?group_id=94) that contains the latest released version of XKAAPI sources. It should be fairly stable but does not always contains the latest features we develop.
You can get the [current development version using GIT](https://gforge.inria.fr/scm/?group_id=94). You will need recent auto-tools to build the compilation environment.

##How to compile and install XKAAPI ?
XKAAPI uses the GNU auto-tools, so you can use the classic scheme
```Bash
> ../xkaapi/configure  --prefix=<your prefix> --with-numa
> make ; make install
```
Numa option allows to build optimized version for numa architecture. It requires libnuma installed on your system.
The configure script has a lot of options (run it with –help). More documentation can be found of the website.

XKAAPI has been tested with success on:

* Linux with gcc/g++ 4.1, 4.3, 4.4 and 4.5, 4.7, 4.8, 4.9

It is currently developed with gcc 4.9.1, 4.7, 4.8 and 4.9 and clang 3.5.

##Where can I find more documentation
We strongly encourage users to migrate their development to OpenMP-4.0 that contains task with dependencies. Documentation could be find on [OpenMP.org](http://openmp.org/wp/openmp-specifications).
XKaapi contains a binary compliant library called libkomp that could replace libgomp without recompilation. It support all environment variable including OMP_PLACES syntax.



##Tips & tricks

###How to run kaapi on several machines ?
Kaapi version 1.XX only runs on multi-core machines. The version 2.XX is restricted to one machine with multi-CPUs - multi-GPUs.
The version 3.XX (current development) should integrate full support for OpenMP-4.0, multi-CPUs and multi-GPUs from 2.X version.
We plan to re-introduce network support and fault toleant protocols from 1.XX. If you have any suggestion, please send mail to [kaapi-dev-info](http://lists.gforge.inria.fr/cgi-bin/mailman/listinfo/kaapi-dev-info).

###How to run X-Kaapi applications on P cores ?
XKaapi applications developped with OpenMP and compiled with GCC, Intel compiler or [KSTAR compiler](http://kstar.gforge.inria.fr) are OpenMP applications and following environement variables could be used:

* OMP_NUM_THREADS
* OMP_PLACES

```Bash
> OMP_NUM_THREADS=192 ./dgetrf_taskdep -n 8192 -b 256
> OMP_PLACES="{1,2,4}" ./dgetrf_taskdep -n 8192 -b 256
```
Please refer to [OpenMP-4.0 specification](http://openmp.org/wp/openmp-specifications) of OMP_PLACES.


XKaapi application developped with C or C++ APIs may use equivalent variables:

* KAAPI_CPUCOUNT : equiv to OMP_NUM_THREADS
* KAAPI_CPUSET : equiv to OMP_PLACES


###Yes... but how to run OpenMP application with XKaapi?
Once your OpenMP application is ready, you can:

* compile with GCC/GFortran and use libkomp.so. We provide in (install prefix)/bin the script ***komp-run*** that replace libgomp by libkomp:
```Bash
> gcc -fopenmp -o my_binary my_code.c
> OMP_PLACES="{1,2,4}" (install prefix)/bin/komp-run ./my_binary
```

* compile your code with [KSTAR](http://kstar.gforge.inria.fr) and run it directly:
```Bash
> klang --runtime=kaapi -fopenmp -o my_binary my_code.c
> OMP_PLACES="{1,2,4}" ./my_binary
```

###How to trace X-Kaapi applications ?
If configured with --with-perfcounter, the X-Kaapi library may record at runtime events or counters, such as number of executed tasks, time to execute tasks, time thread being idle… The configure of the X-Kaapi library must follow the explanation ”Overview of the installation steps”. Once configured, X-Kaapi library maintains light set of counters. 

####New! 
This [page is dedicated to trace and performance counters](perfcounter.md): How to configure the X-Kaapi library? How to select and display performance counters?

